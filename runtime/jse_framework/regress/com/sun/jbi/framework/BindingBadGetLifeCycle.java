/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingBadGetLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;

/**
 * This is an implementation of a Binding Component that is purely for
 * unit testing. This implementation always throws an exception from its
 * getLifeCycle() method.
 *
 * @author Sun Microsystems, Inc.
 */
public class BindingBadGetLifeCycle
    extends AbstractComponent
{
    /**
     * Public constructor.
     */
    public BindingBadGetLifeCycle()
    {
        mLog = Logger.getLogger("com.sun.jbi.framework.test.BindingBadGetLifeCycle");
        mComponentType = "Binding";
    }

    //
    // Overridden Component methods
    //

    /**
     * Get the ComponentLifeCycle implementation instance for this Binding
     * Component. This method always throws an exception.
     * @return the Life Cycle implementation instance.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        Object o = new String("test");
        mLog.info(mComponentType +
            " getLifeCycle called, causing ClassCastException");
        return (ComponentLifeCycle) o;
    }

}
