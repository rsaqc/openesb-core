/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CLUtils.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework ;

import java.io.File ;
import java.net.URL ;
import java.lang.ClassLoader ;
import java.net.URLClassLoader ;
import java.util.Iterator ;
import java.util.List ;
import java.lang.StackTraceElement;

public class CLUtils
{
    /*  
     * location of the path to the unit test data JAR files
     */ 
    public static String getTestJarsPath (String srcRoot)
    {
        return (srcRoot + File.separator +
            "runtime/framework/regress/data/classloader") ;
    }

    /*  
     * location of the path to the unit test data class files
     */ 
    public static String getTestClassPath (String srcRoot)
    {
        return (srcRoot + File.separator +
            "runtime/framework/regress/data/classloader/classes") ;
    }

    /*  
     * location of the path to the unit test data resource files
     */ 
    public static String getTestResourcePath (String srcRoot)
    {
        return (srcRoot + File.separator +
            "runtime/framework/regress/data/classloader/resources") ;
    }

    /*  
     * creates a generic java.net.URLClassLoader
     * based on data provided
     */ 
    public static ClassLoader createClassLoader (URL[] urls , ClassLoader parent)
    {
        return URLClassLoader.newInstance(urls, parent) ;
    }


    /**
     * method to convert a List into a URL array.
     *
     * @param paths list of String elements representing paths and JAR file
     * names
     * @return java.net.URL[] array representing the URLs corresponding to the

     * paths, or null if the list is null or if there is an exception creating
     * the array.
     * @throws Exception If the array creation is unsucessful.
     */
    public static URL[] list2URLArray (List paths) throws Exception
    {
        File f;
        URL[] urls = new URL[paths.size()];
        int i = 0;
        for (Iterator itr = paths.listIterator(); itr.hasNext();)
        {
            String nextCPElement = (String) itr.next();
            f = new File(nextCPElement);
            urls[i++] = f.toURL();
        }

        return urls;
    }

    /**  
     * logs a message to the system console 
     */ 
    public static void log (String msg)
    {
        System.out.println(msg) ;
    }


    /*  
     * method to print out the stack trace elements of an exception
     */ 
    public static void dumpStackTrace( Throwable t )
    {
        StackTraceElement[] steArr = t.getStackTrace();
	log ("-----------------------------------");
	log ("Stack Trace for :" + t.getMessage());

	String prefix = "-" ;
	for (int i = 0 ; i < steArr.length ; i++)
	{
            StackTraceElement ste = steArr[i] ;
	    prefix += "-" ;
	    log (prefix + ste.getClassName() + ":" + ste.getMethodName() + ":" + ste.getLineNumber()) ;
	}
	log ("-----------------------------------");
    }
}
