/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEventNotifier.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework;

import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.openmbean.CompositeData;

/**
 * Tests for EventNotifier.
 *
 * @author Mark S White
 */
public class TestEventNotifier
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Local handle to the EnvironmentContext
     */
    private EnvironmentContext mContext;

    /**
     * The EnvironmentSetup helper class
     */
    private EnvironmentSetup mSetup;

    /**
     * Instance of EventNotifier.
     */
    private EventNotifier mEventNotifier;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestEventNotifier(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the EventNotifier instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);

        // Create EnvironmentContext

        mSetup = new EnvironmentSetup();
        mContext = mSetup.getEnvironmentContext();
        mEventNotifier = (EventNotifier) mContext.getNotifier();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        System.err.println("***** END of test " + mTestName);
    }

// =============================  test methods ================================

    /**
     * Tests getNotificationInfo.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetNotificationInfo()
    {
        // Get the notification info for this MBean

        MBeanNotificationInfo[] info = mEventNotifier.getNotificationInfo();
        String[] types = info[0].getNotifTypes();
        String name = info[0].getName();
        String desc = info[0].getDescription();

        // Verify that the notification info is correct

        assertEquals("Got more than one MBeanNotificationInfo, expected one: ",
            1, info.length);
        assertEquals("Got wrong number of notification types: ",
            mEventNotifier.NOTIFICATION_TYPES.length, types.length);
        assertEquals("Got wrong class name: ",
            mEventNotifier.NOTIFICATION_CLASS_NAME, name);
        assertEquals("Got wrong description: ",
            mEventNotifier.NOTIFICATION_DESCRIPTION, desc);
    }

    /**
     * Tests emitRuntimeNotification for startup/shutdown of the JBI framework.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEmitRuntimeNotification()
    {
        // Emit a notification for startup of the JBI Framework

        String msg = "JBI Framework has been started";
        Notification not = mEventNotifier.emitRuntimeNotification(
            EventNotifier.EventType.Started, msg);

        // Verify that the notification content is correct

        assertEquals("Notification type is incorrect: ",
            not.getType(), "com.sun.jbi." +
            EventNotifier.EventType.Started + "." +
            EventNotifier.SourceType.JBIRuntime);

        assertEquals("Message in notification is incorrect: ",
            not.getMessage(), msg);

        CompositeData cd = (CompositeData) not.getUserData();

        assertEquals("UserData in notification has wrong event type: ",
            cd.get(EventNotifier.EVENT_TYPE_KEY),
            EventNotifier.EventType.Started.toString());
        assertEquals("UserData in notification has wrong source type: ",
            cd.get(EventNotifier.SOURCE_TYPE_KEY),
            EventNotifier.SourceType.JBIRuntime.toString());
        assertEquals("UserData in notification has wrong source name: ",
            cd.get(EventNotifier.SOURCE_NAME_KEY),
            mEventNotifier.JBI_FRAMEWORK);

        // Now emit another notification

        msg = "JBI Framework has been stopped";
        Notification not2 = mEventNotifier.emitRuntimeNotification(
            EventNotifier.EventType.Stopped, msg);

        // Verify that the notification sequence number is correct. It should
        // be the sequence number from the first notification plus one.

        assertEquals("Notification sequence number incorrect: ",
            not2.getSequenceNumber(), not.getSequenceNumber() + 1);
    }

    /**
     * Tests emitComponentNotification for a component install/uninstall.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEmitComponentNotification()
    {
        // Emit a notification for installation of a component

        String compName = "TestBinding";
        String compMsg = "TestBinding has been installed";
        Notification not = mEventNotifier.emitComponentNotification(
            EventNotifier.EventType.Installed,
            EventNotifier.SourceType.BindingComponent,
            compName, compMsg);

        // Verify that the notification content is correct

        assertEquals("Notification type is incorrect: ",
            not.getType(), "com.sun.jbi." +
            EventNotifier.EventType.Installed + "." +
            EventNotifier.SourceType.BindingComponent);

        assertEquals("Message in notification is incorrect: ",
            not.getMessage(), compMsg);

        CompositeData cd = (CompositeData) not.getUserData();

        assertEquals("UserData in notification has wrong event type: ",
            cd.get(EventNotifier.EVENT_TYPE_KEY),
            EventNotifier.EventType.Installed.toString());
        assertEquals("UserData in notification has wrong source type: ",
            cd.get(EventNotifier.SOURCE_TYPE_KEY),
            EventNotifier.SourceType.BindingComponent.toString());
        assertEquals("UserData in notification has wrong source name: ",
            cd.get(EventNotifier.SOURCE_NAME_KEY), compName);

        // Now emit another notification for uninstallation
    
        Notification not2 = mEventNotifier.emitComponentNotification(
            EventNotifier.EventType.Uninstalled,
            EventNotifier.SourceType.BindingComponent,
            compName, compMsg);

        // Verify that the notification sequence number is correct. It should
        // be the sequence number from the first notification plus one.

        assertEquals("Notification sequence number incorrect: ",
            not2.getSequenceNumber(), not.getSequenceNumber() + 1);
    }

    /**
     * Tests emitSharedLibraryNotification for a shared library install/
     * uninstall.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEmitSharedLibraryNotification()
    {
        String slName = "TestSharedLibrary";
        String slMsg = "TestSharedLibrary has been installed";
        Notification not = mEventNotifier.emitSharedLibraryNotification(
            EventNotifier.EventType.Installed,
            slName, slMsg);

        // Verification

        assertEquals("Notification type is incorrect: ",
            not.getType(), "com.sun.jbi." +
            EventNotifier.EventType.Installed + "." +
            EventNotifier.SourceType.SharedLibrary);

        assertEquals("Message in notification is incorrect: ",
            not.getMessage(), slMsg);

        CompositeData cd = (CompositeData) not.getUserData();

        assertEquals("UserData in notification has wrong event type: ",
            cd.get(EventNotifier.EVENT_TYPE_KEY),
            EventNotifier.EventType.Installed.toString());
        assertEquals("UserData in notification has wrong source type: ",
            cd.get(EventNotifier.SOURCE_TYPE_KEY),
            EventNotifier.SourceType.SharedLibrary.toString());
        assertEquals("UserData in notification has wrong source name: ",
            cd.get(EventNotifier.SOURCE_NAME_KEY), slName);

        // Now emit another notification for uninstallation
    
        slMsg = "TestSharedLibrary has been uninstalled";
        Notification not2 = mEventNotifier.emitSharedLibraryNotification(
            EventNotifier.EventType.Uninstalled,
            slName, slMsg);

        // Verify that the notification sequence number is correct. It should
        // be the sequence number from the first notification plus one.

        assertEquals("Notification sequence number incorrect: ",
            not2.getSequenceNumber(), not.getSequenceNumber() + 1);
    }

    /**
     * Tests emitServiceAssemblyNotification for a service assembly deploy/
     * undeploy.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEmitServiceAssemblyNotification()
    {
        String saName = "TestServiceAssembly";
        String saMsg = "TestServiceAssembly has been deployed";
        Notification not = mEventNotifier.emitServiceAssemblyNotification(
            EventNotifier.EventType.Deployed,
            saName, saMsg);

        // Verification

        assertEquals("Notification type is incorrect: ",
            not.getType(), "com.sun.jbi." +
            EventNotifier.EventType.Deployed + "." +
            EventNotifier.SourceType.ServiceAssembly);

        assertEquals("Message in notification is incorrect: ",
            not.getMessage(), saMsg);

        CompositeData cd = (CompositeData) not.getUserData();

        assertEquals("UserData in notification has wrong event type: ",
            cd.get(EventNotifier.EVENT_TYPE_KEY),
            EventNotifier.EventType.Deployed.toString());
        assertEquals("UserData in notification has wrong source type: ",
            cd.get(EventNotifier.SOURCE_TYPE_KEY),
            EventNotifier.SourceType.ServiceAssembly.toString());
        assertEquals("UserData in notification has wrong source name: ",
            cd.get(EventNotifier.SOURCE_NAME_KEY), saName);

        // Now emit another notification for undeployment

        saMsg = "TestServiceAssembly has been undeployed";
        Notification not2 = mEventNotifier.emitServiceAssemblyNotification(
            EventNotifier.EventType.Undeployed,
            saName, saMsg);

        // Verify that the notification sequence number is correct. It should
        // be the sequence number from the first notification plus one.

        assertEquals("Notification sequence number incorrect: ",
            not2.getSequenceNumber(), not.getSequenceNumber() + 1);
    }

    /**
     * Tests emitServiceUnitNotification for a service unit start/shutdown.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEmitServiceUnitNotification()
    {
        String suName = "TestServiceUnit";
        String suMsg = "TestServiceUnit has been started";
        String saName = "TestServiceAssembly";
        String compName = "TestEngine";
        Notification not = mEventNotifier.emitServiceUnitNotification(
            EventNotifier.EventType.Started,
            suName, saName, compName, suMsg);

        // Verification

        assertEquals("Notification type is incorrect: ",
            not.getType(), "com.sun.jbi." +
            EventNotifier.EventType.Started + "." +
            EventNotifier.SourceType.ServiceUnit);

        assertEquals("Message in notification is incorrect: ",
            not.getMessage(), suMsg);

        CompositeData cd = (CompositeData) not.getUserData();

        assertEquals("UserData in notification has wrong event type: ",
            cd.get(EventNotifier.EVENT_TYPE_KEY),
            EventNotifier.EventType.Started.toString());
        assertEquals("UserData in notification has wrong source type: ",
            cd.get(EventNotifier.SOURCE_TYPE_KEY),
            EventNotifier.SourceType.ServiceUnit.toString());
        assertEquals("UserData in notification has wrong source name: ",
            cd.get(EventNotifier.SOURCE_NAME_KEY), suName);
        assertEquals("UserData in notification has wrong service assembly name: ",
            cd.get(EventNotifier.SERVICE_ASSEMBLY_NAME_KEY), saName);
        assertEquals("UserData in notification has wrong component name: ",
            cd.get(EventNotifier.COMPONENT_NAME_KEY), compName);

        // Now emit another notification for shutdown

        suMsg = "TestServiceUnit has been shut down";
        Notification not2 = mEventNotifier.emitServiceUnitNotification(
            EventNotifier.EventType.ShutDown,
            suName, saName, compName, suMsg);

        // Verify that the notification sequence number is correct. It should
        // be the sequence number from the first notification plus one.

        assertEquals("Notification sequence number incorrect: ",
            not2.getSequenceNumber(), not.getSequenceNumber() + 1);
    }

    /**
     * Tests enableNotifications/disableNotifications.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEnableDisableNotifications()
    {
        // First emit a runtime notification

        String msg = "JBI Framework has been started";
        Notification not = mEventNotifier.emitRuntimeNotification(
            EventNotifier.EventType.Started, msg);

        // Verify that the notification was created

        assertNotNull("Notification not created: ", not);
        assertEquals("Notification not correct: ", msg, not.getMessage());

        // Now disable notifications

        mEventNotifier.disableNotifications();

        // Attempt to emit a runtime notification

        not = mEventNotifier.emitRuntimeNotification(
            EventNotifier.EventType.Started, msg);

        // Verify that the notification was not created

        assertNull("Notification should not have been created: ", not);

        // Now enable notifications

        mEventNotifier.enableNotifications();

        // Emit a runtime notification

        not = mEventNotifier.emitRuntimeNotification(
            EventNotifier.EventType.Started, msg);

        // Verify that the notification was created

        assertNotNull("Notification not created: ", not);
        assertEquals("Notification not correct: ", msg, not.getMessage());
    }

}
