/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.JBIProvider;

import java.io.ByteArrayInputStream;
import java.io.File;

import java.net.URL;
import java.net.URLClassLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import javax.management.ObjectName;

/**
 * Tests for the Component class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponent
    extends junit.framework.TestCase
{
    /**
     * Value of the $SRCROOT environment variable
     */
    private String mSrcroot;

    /**
     * EnvironmentContext
     */
    private EnvironmentContext mEnvironmentContext;

    /**
     * Component
     */
    private Component mComponent;

    /**
     * Component Description
     */
    private String mComponentDesc;

    /**
     * Component Name
     */
    private String mComponentName;

    /**
     * Component Root directory
     */
    private String mInstallRoot;

    /**
     * Workspace Root directory
     */
    private String mWorkspaceRoot;

    /**
     * Bootstrap class instance
     */
    private Object mBootstrapClass;

    /**
     * Bootstrap class loader
     */
    private ClassLoader mBootstrapClassLoader;

    /**
     * Bootstrap class name
     */
    private String mBootstrapClassName;

    /**
     * Bootstrap class path element list
     */
    private ArrayList mBootstrapClassPath;

    /**
     * Life cycle class instance
     */
    private Object mLifeCycleClass;

    /**
     * Life cycle class loader
     */
    private ClassLoader mLifeCycleClassLoader;

    /**
     * Life cycle class name
     */
    private String mComponentClassName;

    /**
     * Component runtime class path element list
     */
    private ArrayList mClassPathElements;

    /**
     * Component runtime class path
     */
    private String mClassPath;

    /**
     * Shared Library list
     */
    private ArrayList mSharedLibList;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponent(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mSrcroot = System.getProperty("junit.srcroot") + "/";
        mEnvironmentContext = new EnvironmentContext(new ScaffoldPlatformContext(), 
                new JBIFramework(), new Properties());
        mEnvironmentContext.setRegistry(new ScaffoldRegistry(mEnvironmentContext));
        mComponent = new Component();
        ArrayList cp;
        File dir;
        URL[] urls;
        Class c;

        mBootstrapClassName = "com.sun.jbi.framework.BindingBootstrap";
        cp = new ArrayList();
        cp.add(mSrcroot + "runtime/framework/bld/com/sun/jbi/framework");
        mBootstrapClassPath = cp;
        dir = new File((String) cp.get(0));
        urls = new URL[1];
        urls[0] = dir.toURL();
        mBootstrapClassLoader = new URLClassLoader(urls);
        c = mBootstrapClassLoader.loadClass(mBootstrapClassName);
        mBootstrapClass = c.newInstance();

        mComponentDesc = "Test Component 1";
        mComponentName = "Comp1";
        mInstallRoot = mSrcroot + "runtime/framework/bld/Comp1/install_root";
        mWorkspaceRoot = mSrcroot + "runtime/framework/bld/Comp1/install_root/work";

        mComponentClassName = "com.sun.jbi.framework.Binding";
        cp = new ArrayList();
        cp.add(mSrcroot + "runtime/framework/bld/com/sun/jbi/framework");
        cp.add(mSrcroot + "runtime/framework/regress/com/sun/jbi/framework");
        mClassPathElements = cp;
        mClassPath = (String) cp.get(0) + File.pathSeparator + (String) cp.get(1);
        dir = new File((String) cp.get(0));
        urls = new URL[1];
        urls[0] = dir.toURL();
        mLifeCycleClassLoader = new URLClassLoader(urls);
        c = mLifeCycleClassLoader.loadClass(mComponentClassName);
        mLifeCycleClass = c.newInstance();

        mSharedLibList = new ArrayList();
        mSharedLibList.add("SharedLibrary1");
        mSharedLibList.add("SharedLibrary2");
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Test the addServiceUnit/getServiceUnit methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testAddGetServiceUnit()
        throws Exception
    {
        ServiceUnit su1 =
            new ServiceUnit("TestSA1", "TestSU1", "jbi/deployment/su1");

        mComponent.addServiceUnit(su1);
        assertSame("Failure adding/getting ServiceUnit: ",
                   mComponent.getServiceUnit("TestSU1"), su1);
    }

    /**
     * Test the isServiceUnitRegistered method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsServiceUnitRegistered()
        throws Exception
    {
        assertFalse("Incorrect result from isServiceUnitRegistered: " +
            "got true, expected false",
            mComponent.isServiceUnitRegistered("TestSU1"));

        ServiceUnit su1 =
            new ServiceUnit("TestSA1", "TestSU1", "jbi/deployment/su1");

        mComponent.addServiceUnit(su1);
        assertTrue("Incorrect result from isServiceUnitRegistered: " +
            "got false, expected true",
            mComponent.isServiceUnitRegistered("TestSU1"));
    }

    /**
     * Test the addServiceUnit method with a duplicate Service Unit name.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testAddServiceUnitBadDuplicate()
        throws Exception
    {
        ServiceUnit su1 =
            new ServiceUnit("TestSA1", "TestSU1", "jbi/deployment/su1");
        ServiceUnit su2 =
            new ServiceUnit("TestSA2", "TestSU1", "jbi/deployment/su2");

        mComponent.addServiceUnit(su1);
        try
        {
            mComponent.addServiceUnit(su2);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("already exists for")));
        }
    }

    /**
     * Test the addServiceUnit method with a null Service Unit. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testAddServiceUnitBadNull()
        throws Exception
    {
        try
        {
            mComponent.addServiceUnit(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Test the get and set methods for the Bootstrap Class Name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetBootstrapClassName()
        throws Exception
    {
        mComponent.setBootstrapClassName(mBootstrapClassName);
        assertEquals("Failure setting/getting BootstrapClassName: ",
                     mBootstrapClassName, mComponent.getBootstrapClassName());
    }

    /**
     * Test the get and set methods for the Bootstrap Class Path Elements.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetBootstrapClassPathElements()
        throws Exception
    {
        mComponent.setBootstrapClassPathElements(mBootstrapClassPath);
        assertEquals("Failure getting/setting BootstrapClassPathElements: ",
                     mBootstrapClassPath, mComponent.getBootstrapClassPathElements());
    }

    /**
     * Test the get and clear methods for the Bootstrap Instance.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetClearBootstrapInstance()
        throws Exception
    {
        // Set bootstrap class name and class path elements for test.
        mComponent.setBootstrapClassName(mBootstrapClassName);
        mComponent.setBootstrapClassPathElements(mBootstrapClassPath);
        Object instance;

        // This should return a null value.
        instance = mComponent.getBootstrapInstance(false);
        assertEquals("Failure on getBootstrapInstance(false): ",
                     null, instance);

        // This should return a non-null value.
        instance = mComponent.getBootstrapInstance(true);
        assertNotNull("Failure on getBootstrapInstance(true): " +
                      "expected a non-null value, got a null",
                      instance);
        assertTrue("Failure on getBootstrapInstance(true): " +
                   "expected an instance of " + mBootstrapClassName +
                   ", got an instance of " + instance.getClass().getName(),
                   instance instanceof com.sun.jbi.framework.BindingBootstrap);

        // This should clear the value that was set.
        mComponent.clearBootstrapInstance();
        instance = mComponent.getBootstrapInstance(false);
        assertNull("Failure on clearBootstrapInstance(): " +
                   "instance pointer was not cleared, value is still " +
                   instance, instance);
    }

    /**
     * Test the getBootstrapInstance with a bad class name to cause a
     * class loading failure.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetBootstrapInstanceBadNotFound()
        throws Exception
    {
        // Set bootstrap class name and class path elements for test.
        mComponent.setBootstrapClassName("com.sun.jbi.framework.nonexistent");
        mComponent.setBootstrapClassPathElements(mBootstrapClassPath);
        Object instance;

        // This should fail with a JBIException.
        try
        {
            instance = mComponent.getBootstrapInstance(true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       ((-1 < ex.getMessage().indexOf("was not found"))) &&
                       ((-1 < ex.getMessage().indexOf("Required class"))));
        }
    }

    /**
     * Test the getBootstrapInstance with a class that does not implement
     * the required interfaces to cause a class loading failure.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetBootstrapInstanceBadClassInvalid()
        throws Exception
    {
        // Set bootstrap class name and class path elements for test.
        mComponent.setBootstrapClassName("com.sun.jbi.framework.Component");
        mComponent.setBootstrapClassPathElements(mBootstrapClassPath);
        Object instance;

        // This should fail with a JBIException.
        try
        {
            instance = mComponent.getBootstrapInstance(true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       ((-1 < ex.getMessage().indexOf("does not implement"))) &&
                       ((-1 < ex.getMessage().indexOf("required interface"))));
        }
    }

    /**
     * Test the get and set methods for the component class name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetComponentClassName()
        throws Exception
    {
        mComponent.setComponentClassName(mComponentClassName);
        assertEquals("Failure getting/setting LifeCycleClassName: ",
                     mComponentClassName, mComponent.getComponentClassName());
    }

    /**
     * Test the get and set methods for the component class path elements.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetComponentClassPathElements()
        throws Exception
    {
        mComponent.setComponentClassPathElements(mClassPathElements);
        assertEquals("Failure getting/setting ComponentClassPathElements: ",
                     mClassPathElements, mComponent.getComponentClassPathElements());
    }

    /**
     * Test the getComponentClassPathAsString method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentClassPathAsString()
        throws Exception
    {
        mComponent.setComponentClassPathElements(mClassPathElements);
        assertEquals("Failure getting ComponentClassPath: ",
                     mClassPath, mComponent.getComponentClassPathAsString());
    }

    /**
     * Test the getComponentInstance method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentInstance()
        throws Exception
    {
        // Set component name.
        mComponent.setName(mComponentName);
        // Set lifecycle class name and class path elements for test.
        mComponent.setComponentClassName(mComponentClassName);
        mComponent.setComponentClassPathElements(mClassPathElements);
        Object instance;

        // This should return a null value.
        instance = mComponent.getLifeCycleInstance(false);
        assertNull("Failure on getLifeCycleInstance(false): " +
                   "expected a null value, got " + instance,
                   instance);
        instance = mComponent.getComponentInstance();
        assertNull("Failure on getComponentInstance(): " +
                   "expected a null value, got " + instance,
                   instance);

        // This should return a non-null value.
        instance = mComponent.getLifeCycleInstance(true);
        assertNotNull("Failure on getLifeCycleInstance(true): " +
                      "expected a non-null value, got a null",
                      instance);
        assertTrue("Failure on getLifeCycleInstance(true): " +
                   "expected an instance of javax.jbi.component.ComponentLifeCycle" +
                   ", got an instance of " + instance.getClass().getName(),
                   instance instanceof javax.jbi.component.ComponentLifeCycle);

        instance = mComponent.getComponentInstance();
        assertNotNull("Failure on getComponentInstance(): " +
                      "expected a non-null value, got a null",
                      instance);
        assertTrue("Failure on getComponentInstance(): " +
                   "expected an instance of javax.jbi.component.Component" +
                   ", got an instance of " + instance.getClass().getName(),
                   instance instanceof javax.jbi.component.Component);

        // This should clear the value that was set.
        mComponent.clearLifeCycleInstance();
        assertNull("Failure on clearLifeCycleInstance(): " +
                   "instance pointer was not cleared, value is still " +
                   mComponent.getComponentInstance(),
                   mComponent.getComponentInstance());
    }

    /**
     * Test the get and set methods for the component type.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetComponentType()
        throws Exception
    {
        mComponent.setComponentType(ComponentType.BINDING);
        assertEquals("Failure getting/setting ComponentType: ",
                     ComponentType.BINDING, mComponent.getComponentType());
        mComponent.setComponentType(ComponentType.ENGINE);
        assertEquals("Failure getting/setting ComponentType: ",
                     ComponentType.ENGINE, mComponent.getComponentType());
    }

    /**
     * Test the getComponentTypeAsString method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentTypeAsString()
        throws Exception
    {
        mComponent.setComponentType(ComponentType.BINDING);
        assertEquals("Failure getting ComponentType: ",
                     "Binding", mComponent.getComponentTypeAsString());
        mComponent.setComponentType(ComponentType.ENGINE);
        assertEquals("Failure getting/setting ComponentType: ",
                     "Engine", mComponent.getComponentTypeAsString());
    }

    /**
     * Test the get and set methods for the context.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetContext()
        throws Exception
    {
        ComponentContext context =
            new ComponentContext(mComponent, mEnvironmentContext);
        mComponent.setContext(context);
        assertEquals("Failure getting/setting Context: ",
                     context, mComponent.getContext());
    }

    /**
     * Test the get and set methods for the component description.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetDescription()
        throws Exception
    {
        mComponent.setDescription(mComponentDesc);
        assertEquals("Failure getting/setting Description: ",
                     mComponentDesc, mComponent.getDescription());
    }

    /**
     * Test the get and set methods for the Deployer MBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetDeployerMBeanName()
        throws Exception
    {
        javax.management.ObjectName mbn =
            new javax.management.ObjectName("jbi:type=deployer");
        mComponent.setDeployerMBeanName(mbn);
        assertEquals("Failure getting/setting Deployer MBean Name: ",
                     mbn, mComponent.getDeployerMBeanName());
    }

    /**
     * Test the get and set methods for the Deployer instance.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetDeployerInstance()
        throws Exception
    {
        // Set component name.
        mComponent.setName(mComponentName);
        // Set component class name and class path elements for test.
        mComponent.setComponentClassName(mComponentClassName);
        mComponent.setComponentClassPathElements(mClassPathElements);
        // Get a component instance for the test.
        Object instance = mComponent.getLifeCycleInstance(true);

        // Now the real test.
        Deployer dep = new Deployer(mComponent);
        mComponent.setDeployerInstance(dep);
        assertSame("Failure getting/setting Deployer instance: ",
                   dep, mComponent.getDeployerInstance());
    }

    /**
     * Test the get and set methods for the desired state.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetDesiredState()
        throws Exception
    {
        assertEquals("Initial state incorrect: ",
                     ComponentState.UNKNOWN, mComponent.getDesiredState());
        mComponent.setDesiredState(ComponentState.STOPPED);
        assertEquals("Failure getting/setting stopped state: ",
                     ComponentState.STOPPED, mComponent.getDesiredState());
        mComponent.setDesiredState(ComponentState.STARTED);
        assertEquals("Failure getting/setting started state: ",
                     ComponentState.STARTED, mComponent.getDesiredState());
        mComponent.setDesiredState(ComponentState.SHUTDOWN);
        assertEquals("Failure getting/setting shutdown state: ",
                     ComponentState.SHUTDOWN, mComponent.getDesiredState());

        // This should fail with a runtime exception
        try
        {
            mComponent.setDesiredState(ComponentState.UNKNOWN);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state")));
        }
    } 
    
    /**
     * Test the get and set methods for the Extension MBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetExtensionMBeanName()
        throws Exception
    {
        javax.management.ObjectName mbn =
            new javax.management.ObjectName("jbi:type=extension");
        mComponent.setExtensionMBeanName(mbn);
        assertEquals("Failure getting/setting extension MBean Name: ",
                     mbn, mComponent.getExtensionMBeanName());
    }

    /**
     * Test the get and set methods for the Installation Descriptor.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetInstallationDescriptor()
        throws Exception
    {
        String id = "<bogus installation descriptor/>";
        mComponent.setInstallationDescriptor(id);
        assertEquals("Failure getting/setting Installer MBean Name: ",
            id, mComponent.getInstallationDescriptor());
    }

    /**
     * Test the get and set methods for the Installer MBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetInstallerMBeanName()
        throws Exception
    {
        javax.management.ObjectName mbn =
            new javax.management.ObjectName("jbi:type=installer");
        mComponent.setInstallerMBeanName(mbn);
        assertEquals("Failure getting/setting Installer MBean Name: ",
                     mbn, mComponent.getInstallerMBeanName());
    }

    /**
     * Test the get and set methods for the component install root directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetInstallRoot()
        throws Exception
    {
        mComponent.setInstallRoot(mInstallRoot);
        // convert backslashes to slashes, so Cygwin is happy
        String expected = mInstallRoot.replace('\\', '/');
        String actual = mComponent.getInstallRoot(). replace('\\', '/');
        assertEquals("Failure getting/setting ComponentRoot: ", expected, actual);
    }

    /**
     * Test the getLifeCycleInstance and clearLifeCycleInstance methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetClearLifeCycleInstance()
        throws Exception
    {
        // Set component name.
        mComponent.setName(mComponentName);
        // Set lifecycle class name and class path elements for test.
        mComponent.setComponentClassName(mComponentClassName);
        mComponent.setComponentClassPathElements(mClassPathElements);
        Object instance;

        // This should return a null value.
        instance = mComponent.getLifeCycleInstance(false);
        assertNull("Failure on getLifeCycleInstance(false): " +
                   "expected a null value, got " + instance,
                   instance);

        // This should return a non-null value.
        instance = mComponent.getLifeCycleInstance(true);
        assertNotNull("Failure on getLifeCycleInstance(true): " +
                      "expected a non-null value, got a null",
                      instance);
        assertTrue("Failure on getLifeCycleInstance(true): " +
                   "expected an instance of " + mComponentClassName +
                   ", got an instance of " + instance.getClass().getName(),
                   instance instanceof com.sun.jbi.framework.Binding);

        // This should clear the value that was set.
        mComponent.clearLifeCycleInstance();
        instance = mComponent.getLifeCycleInstance(false);
        assertNull("Failure on clearLifeCycleInstance(): " +
                   "instance pointer was not cleared, value is still " +
                   instance, instance);
    }

    /**
     * Test the getLifeCycleInstance with a bad classpath to cause a
     * class loading failure.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLifeCycleInstanceBadNotFound()
        throws Exception
    {
        // Set lifecycle class name and class path elements for test.
        mComponent.setComponentClassName("com.sun.jbi.framework.nonexistent");
        mComponent.setComponentClassPathElements(mClassPathElements);
        Object instance;

        // This should fail with a JBIException.
        try
        {
            instance = mComponent.getLifeCycleInstance(true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       ((-1 < ex.getMessage().indexOf("was not found"))) &&
                       ((-1 < ex.getMessage().indexOf("Required class"))));
        }
    }

    /**
     * Test the getLifeCycleInstance with a class that does not implement
     * the required interfaces to cause a class loading failure.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLifeCycleInstanceBadClassInvalid()
        throws Exception
    {
        // Set bootstrap class name and class path elements for test.
        mComponent.setComponentClassName("com.sun.jbi.framework.Component");
        mComponent.setComponentClassPathElements(mClassPathElements);
        Object instance;

        // This should fail with a JBIException.
        try
        {
            instance = mComponent.getLifeCycleInstance(true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       ((-1 < ex.getMessage().indexOf("does not implement"))) &&
                       ((-1 < ex.getMessage().indexOf("required interface"))));
        }
    }

    /**
     * Test the get and set methods for the LifeCycle MBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetLifeCycleMBeanName()
        throws Exception
    {
        javax.management.ObjectName mbn =
            new javax.management.ObjectName("jbi:type=lifecycle");
        mComponent.setLifeCycleMBeanName(mbn);
        assertEquals("Failure getting/setting LifeCycle MBean Name: ",
                     mbn, mComponent.getLifeCycleMBeanName());
    }
    
    /**
     * Test the get and set methods for the Configuration MBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetConfigurationMBeanName()
        throws Exception
    {
        javax.management.ObjectName mbn =
            new javax.management.ObjectName("jbi:type=configuration");
        mComponent.setConfigurationMBeanName(mbn);
        assertEquals("Failure getting/setting Configuration MBean Name: ",
                     mbn, mComponent.getConfigurationMBeanName());
    }

    /**
     * Test the getLoggerInstance/setLoggerInstance methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetLoggerInstance()
        throws Exception
    {
        mComponent.setInstallRoot(mInstallRoot);
        mComponent.setName(mComponentName);
        ComponentLogger cl = new ComponentLogger(mComponent);
        mComponent.setLoggerInstance(cl);
        assertSame("Failure getting/setting Logger MBean Instance: ",
            cl, mComponent.getLoggerInstance());
        mComponent.setLoggerInstance(null);
        assertNull("Failure clearing Logger MBean Instance: ",
            mComponent.getLoggerInstance());
    }

    /**
     * Test the getLoggerMBeanName/setLoggerMBeanName methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetLoggerMBeanName()
        throws Exception
    {
        javax.management.ObjectName mbn =
            new javax.management.ObjectName("jbi:type=logger");
        mComponent.setLoggerMBeanName(mbn);
        assertEquals("Failure getting/setting Logger MBean Name: ",
                     mbn, mComponent.getLoggerMBeanName());
    }

    /**
     * Test the get and set methods for the component name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetName()
        throws Exception
    {
        mComponent.setName(mComponentName);
        assertEquals("Failure getting/setting Name: ",
                     mComponentName, mComponent.getName());
    }

    /**
     * Test the getServiceUnitList method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetServiceUnitList()
        throws Exception
    {
        ServiceUnit su1 =
            new ServiceUnit("TestSA1", "TestSU1", "jbi/deployment/su1");
        ServiceUnit su2 =
            new ServiceUnit("TestSA2", "TestSU2", "jbi/deployment/su2");
        ArrayList suList = new ArrayList();

        assertEquals("Failure getting ServiceUnitList: ",
                   suList.size(), mComponent.getServiceUnitList().size());

        suList.add(su1);
        mComponent.addServiceUnit(su1);
        assertTrue("Failure getting ServiceUnitList: expected true",
                   mComponent.getServiceUnitList().contains(su1));
        assertEquals("Failure getting ServiceUnitList: ",
                   suList.size(), mComponent.getServiceUnitList().size());

        suList.add(su2);
        mComponent.addServiceUnit(su2);
        assertTrue("Failure getting ServiceUnitList: expected true",
                   mComponent.getServiceUnitList().contains(su2));
        assertEquals("Failure getting ServiceUnitList: ",
                   suList.size(), mComponent.getServiceUnitList().size());
    }

    /**
     * Test the getServiceUnitManager method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetServiceUnitManager()
        throws Exception
    {
        Object instance;

        // This should return a null value.
        instance = mComponent.getServiceUnitManager();
        assertNull("Failure on getServiceUnitManager(): " +
            "expected a null value, got a non-null",
            instance);

        // Now load the life cycle class.
        mComponent.setName(mComponentName);
        mComponent.setComponentClassName(mComponentClassName);
        mComponent.setComponentClassPathElements(mClassPathElements);
        mComponent.getLifeCycleInstance(true);

        // This should return a non-null value.
        instance = mComponent.getServiceUnitManager();
        assertNotNull("Failure on getServiceUnitManager(): " +
            "expected a non-null value, got a null",
            instance);
        String sumName = javax.jbi.component.ServiceUnitManager.class.getName();
        assertTrue("Failure on getServiceUnitManager(): " +
                   "expected an instance of " + sumName +
                   ", got an instance of " + instance.getClass().getName(),
                   instance instanceof javax.jbi.component.ServiceUnitManager);
    }

    /**
     * Test the get and set methods for the Shared Library ID list.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetSharedLibraryNames()
        throws Exception
    {
        mComponent.setSharedLibraryNames(mSharedLibList);
        assertEquals("Failure getting/setting Shared Library ID list: ",
                     mSharedLibList, mComponent.getSharedLibraryNames());
    }

    /**
     * Test the get and set methods for the Statistics instance. 
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetClearStatisticsInstance()
        throws Exception
    {
        ComponentStatistics cs = new ComponentStatistics(mComponent.getName());
        mComponent.setStatisticsInstance(cs);
        assertSame("Failure getting/setting Statistics Instance: ",
            cs, mComponent.getStatisticsInstance());
        mComponent.clearStatisticsInstance();
        assertNull("Failure clearing Statistics Instance: ",
            mComponent.getStatisticsInstance());
    }

    /**
     * Test the get and set methods for the Statistics MBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetStatisticsMBeanName()
        throws Exception
    {
        javax.management.ObjectName mbn =
            new javax.management.ObjectName("jbi:type=statistics");
        mComponent.setStatisticsMBeanName(mbn);
        assertEquals("Failure getting/setting Statistics MBean Name: ",
                     mbn, mComponent.getStatisticsMBeanName());
    }

    /**
     * Test the get and set methods for the component status.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetStatus()
        throws Exception
    {
        mComponent.setStatus(ComponentState.LOADED);
        assertEquals("Failure getting/setting loaded status: ",
                     ComponentState.LOADED, mComponent.getStatus());
        mComponent.setStatus(ComponentState.SHUTDOWN);
        assertEquals("Failure getting/setting installed status: ",
                     ComponentState.SHUTDOWN, mComponent.getStatus());
        mComponent.setStatus(ComponentState.STOPPED);
        assertEquals("Failure getting/setting stopped status: ",
                     ComponentState.STOPPED, mComponent.getStatus());
        mComponent.setStatus(ComponentState.STARTED);
        assertEquals("Failure getting/setting started status: ",
                     ComponentState.STARTED, mComponent.getStatus());
    } 
    
    /**
     * Test the getStatusAsString method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStatusAsString()
        throws Exception
    {
        mComponent.setLoaded();
        assertEquals("Failure getting loaded status: ",
                     ComponentState.LOADED.toString(), mComponent.getStatusAsString());
        mComponent.setShutdown();
        assertEquals("Failure getting shutdown status: ",
                     ComponentState.SHUTDOWN.toString(), mComponent.getStatusAsString());
        mComponent.setStopped();
        assertEquals("Failure getting stopped status: ",
                     ComponentState.STOPPED.toString(), mComponent.getStatusAsString());
        mComponent.setStarted();
        assertEquals("Failure getting started status: ",
                     ComponentState.STARTED.toString(), mComponent.getStatusAsString());
    }

    /**
     * Test the get and set methods for the workspace root directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetWorkspaceRoot()
        throws Exception
    {
        mComponent.setWorkspaceRoot(mWorkspaceRoot);
        // convert backslashes to slashes, so Cygwin is happy
        String expected = mWorkspaceRoot.replace('\\', '/');
        String actual = mComponent.getWorkspaceRoot(). replace('\\', '/');
        assertEquals("Failure getting/setting WorkspaceRoot: ", expected, actual);
    }

    /**
     * Test the isBinding and setComponentTypeBinding methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsBinding()
        throws Exception
    {
        mComponent.setComponentTypeBinding();
        assertTrue("Failure in isBinding: expected true, got false",
                   mComponent.isBinding());
        mComponent.setComponentTypeEngine();
        assertFalse("Failure in isBinding: expected false, got true",
                    mComponent.isBinding());
    }

    /**
     * Test the isBootstrapClassLoaderSelfFirst and
     * setBootstrapClassLoaderSelfFirst methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsBootstrapClassLoaderSelfFirst()
        throws Exception
    {
        mComponent.setBootstrapClassLoaderSelfFirst(true);
        assertTrue("Failure in isBootstrapClassLoaderSelfFirst: " +
                   "expected true, got false",
                   mComponent.isBootstrapClassLoaderSelfFirst());
        mComponent.setBootstrapClassLoaderSelfFirst(false);
        assertFalse("Failure in isBootstrapClassLoaderSelfFirst: " +
                    "expected false, got true",
                    mComponent.isBootstrapClassLoaderSelfFirst());
    }

    /**
     * Test the isBootstrapCleanUpNeeded and setBootstrapCleanUpNeeded methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsBootstrapCleanUpNeeded()
        throws Exception
    {
        mComponent.setBootstrapCleanUpNeeded(true);
        assertTrue("Failure in isBootstrapCleanUpNeeded: " +
                   "expected true, got false",
                   mComponent.isBootstrapCleanUpNeeded());
        mComponent.setBootstrapCleanUpNeeded(false);
        assertFalse("Failure in isBootstrapCleanUpNeeded: " +
                    "expected false, got true",
                    mComponent.isBootstrapCleanUpNeeded());
    }

    /**
     * Test the isBusy/setBusy/setBusyForce/clearBusy methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsSetClearBusy()
        throws Exception
    {
        // The component is not busy, this will return false
        assertFalse("Failure in isBusy: expected false, got true",
                    mComponent.isBusy());

        // Set the busy flag, this will succeed
        mComponent.setBusy();
        assertTrue("Failure in setBusy: expected true, got false",
                   mComponent.isBusy());

        // Try to set the busy flag again. This will fail.
        try
        {
            mComponent.setBusy();
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2011")));
        }

        // Forcefully set the busy flag. This will succeed.
        mComponent.setBusyForce();
        assertTrue("Failure in setBusyForce: expected true, got false",
                   mComponent.isBusy());

        // Clear the busy flag, this will succeed.
        mComponent.clearBusy();
        assertFalse("Failure in clearBusy: expected false, got true",
                    mComponent.isBusy());
    }

    /**
     * Test the isComponentClassLoaderSelfFirst and
     * setComponentClassLoaderSelfFirst methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsComponentClassLoaderSelfFirst()
        throws Exception
    {
        mComponent.setComponentClassLoaderSelfFirst(true);
        assertTrue("Failure in isComponentClassLoaderSelfFirst: " +
                   "expected true, got false",
                   mComponent.isComponentClassLoaderSelfFirst());
        assertTrue("Failure in isClassLoaderSelfFirst: " +
                   "expected true, got false",
                   mComponent.isClassLoaderSelfFirst());
        mComponent.setComponentClassLoaderSelfFirst(false);
        assertFalse("Failure in isComponentClassLoaderSelfFirst: " +
                    "expected false, got true",
                    mComponent.isComponentClassLoaderSelfFirst());
        assertFalse("Failure in isClassLoaderSelfFirst: " +
                    "expected false, got true",
                    mComponent.isClassLoaderSelfFirst());
    }

    /**
     * Test the isEngine method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsEngine()
        throws Exception
    {
        mComponent.setComponentTypeEngine();
        assertTrue("Failure in isEngine: expected true, got false",
                   mComponent.isEngine());
        mComponent.setComponentTypeBinding();
        assertFalse("Failure in isEngine: expected false, got true",
                    mComponent.isEngine());
    }

    /**
     * Test the isInitialized method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsInitialized()
        throws Exception
    {
        mComponent.setStatus(ComponentState.LOADED);
        assertFalse("Failure in isInitialized: expected false, got true",
                    mComponent.isInitialized());
        mComponent.setStatus(ComponentState.SHUTDOWN);
        assertFalse("Failure in isInitialized: expected false, got true",
                    mComponent.isInitialized());
        mComponent.setStatus(ComponentState.STOPPED);
        assertTrue("Failure in isInitialized: expected true, got false",
                   mComponent.isInitialized());
        mComponent.setStatus(ComponentState.STARTED);
        assertFalse("Failure in isInitialized: expected false, got true",
                    mComponent.isInitialized());
    }

    /**
     * Test the isInstalled method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsInstalled()
        throws Exception
    {
        mComponent.setStatus(ComponentState.LOADED);
        assertFalse("Failure in isInstalled: expected false, got true",
                    mComponent.isInstalled());
        mComponent.setStatus(ComponentState.SHUTDOWN);
        assertTrue("Failure in isInstalled: expected true, got false",
                   mComponent.isInstalled());
        mComponent.setStatus(ComponentState.STOPPED);
        assertFalse("Failure in isInstalled: expected false, got true",
                    mComponent.isInstalled());
        mComponent.setStatus(ComponentState.STARTED);
        assertFalse("Failure in isInstalled: expected false, got true",
                    mComponent.isInstalled());
    }

    /**
     * Test the isLoaded method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsLoaded()
        throws Exception
    {
        mComponent.setStatus(ComponentState.LOADED);
        assertTrue("Failure in isLoaded: expected true, got false",
                   mComponent.isLoaded());
        mComponent.setStatus(ComponentState.SHUTDOWN);
        assertFalse("Failure in isLoaded: expected false, got true",
                    mComponent.isLoaded());
        mComponent.setStatus(ComponentState.STOPPED);
        assertFalse("Failure in isLoaded: expected false, got true",
                    mComponent.isLoaded());
        mComponent.setStatus(ComponentState.STARTED);
        assertFalse("Failure in isLoaded: expected false, got true",
                    mComponent.isLoaded());
    }

    /**
     * Test the isShutDown method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsShutDown()
        throws Exception
    {
        mComponent.setStatus(ComponentState.LOADED);
        assertFalse("Failure in isShutDown: expected false, got true",
                    mComponent.isShutDown());
        mComponent.setStatus(ComponentState.SHUTDOWN);
        assertTrue("Failure in isShutDown: expected true, got false",
                   mComponent.isShutDown());
        mComponent.setStatus(ComponentState.STOPPED);
        assertFalse("Failure in isShutDown: expected false, got true",
                    mComponent.isShutDown());
        mComponent.setStatus(ComponentState.STARTED);
        assertFalse("Failure in isShutDown: expected false, got true",
                    mComponent.isShutDown());
    }

    /**
     * Test the isStarted method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsStarted()
        throws Exception
    {
        mComponent.setStatus(ComponentState.LOADED);
        assertFalse("Failure in isStarted: expected false, got true",
                    mComponent.isStarted());
        mComponent.setStatus(ComponentState.SHUTDOWN);
        assertFalse("Failure in isStarted: expected false, got true",
                    mComponent.isStarted());
        mComponent.setStatus(ComponentState.STOPPED);
        assertFalse("Failure in isStarted: expected false, got true",
                    mComponent.isStarted());
        mComponent.setStatus(ComponentState.STARTED);
        assertTrue("Failure in isStarted: expected true, got false",
                   mComponent.isStarted());
    }

    /**
     * Test the isStopped method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsStopped()
        throws Exception
    {
        mComponent.setStatus(ComponentState.LOADED);
        assertFalse("Failure in isStopped: expected false, got true",
                    mComponent.isStopped());
        mComponent.setStatus(ComponentState.SHUTDOWN);
        assertFalse("Failure in isStopped: expected false, got true",
                    mComponent.isStopped());
        mComponent.setStatus(ComponentState.STOPPED);
        assertTrue("Failure in isStopped: expected true, got false",
                   mComponent.isStopped());
        mComponent.setStatus(ComponentState.STARTED);
        assertFalse("Failure in isStopped: expected false, got true",
                    mComponent.isStopped());
    }

    /**
     * Test the isObserver/setObserver methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsSetObserver()
        throws Exception
    {
        // The first time, this will check for Observer in the installation
        // desriptor. However, in test mode, this should always fail.
        assertFalse("Failure in isObserver: expected false, got true",
                    mComponent.isObserver());

        // Set the Observer flag to true, this will succeed
        mComponent.setObserver(true);
        assertTrue("Failure in setObserver: expected true, got false",
                   mComponent.isObserver());

        // Set the Observer flag to false, this will succeed
        mComponent.setObserver(false);
        assertFalse("Failure in setObserver: expected false, got true",
                    mComponent.isObserver());
    }

    /**
     * Test the isUpdating/setUpdating/clearUpdating methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsSetClearUpdating()
        throws Exception
    {
        // The updating flag has not been set.
        assertFalse("Failure in isUpdating: expected false, got true",
                    mComponent.isUpdating());

        // Set the updating flag. This will succeed.
        mComponent.setUpdating();
        assertTrue("Failure in setUpdating: expected true, got false",
                   mComponent.isUpdating());

        // Clear the updating flag. This will succeed.
        mComponent.clearUpdating();
        assertFalse("Failure in clearUpdating: expected false, got true",
                    mComponent.isUpdating());

        // Now set the busy flag before trying to set the updating flag. This
        // should fail.
        mComponent.setBusy();
        try
        {
            mComponent.setUpdating();
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2011")));
        }
    }

    /**
     * Test the removeServiceUnit method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRemoveServiceUnit()
        throws Exception
    {
        ServiceUnit su1 =
            new ServiceUnit("TestSA1", "TestSU1", "jbi/deployment/su1");
        ServiceUnit su2 =
            new ServiceUnit("TestSA2", "TestSU2", "jbi/deployment/su2");
        ArrayList suList = new ArrayList();

        suList.add(su2);
        suList.add(su1);
        mComponent.addServiceUnit(su1);
        mComponent.addServiceUnit(su2);

        mComponent.removeServiceUnit("TestSU2");
        assertFalse("Failure removing ServiceUnit: expected false",
                    mComponent.getServiceUnitList().contains(su2));
        assertEquals("Failure adding/getting ServiceUnit: ",
                     1, mComponent.getServiceUnitList().size());

        mComponent.removeServiceUnit("TestSU1");
        assertFalse("Failure removing ServiceUnit: expected false",
                   mComponent.getServiceUnitList().contains(su1));
        assertEquals("Failure adding/getting ServiceUnit: ",
                     0, mComponent.getServiceUnitList().size());
    }

    /**
     * Test the removeServiceUnit method with a non-existent Service Unit name.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRemoveServiceUnitBadNotFound()
        throws Exception
    {
        try
        {
            mComponent.removeServiceUnit("noname");
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("not found for")));
        }
    }

    /**
     * Test the setShutdown method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetShutdown()
        throws Exception
    {
        // Set LOADED component to SHUTDOWN. This is allowed.
        mComponent.setStatus(ComponentState.LOADED);
        mComponent.setShutdown();
        assertEquals("Failure in setShutdown: state set to " +
                     mComponent.getStatusAsString(),
                     mComponent.getStatus(), ComponentState.SHUTDOWN);

        // Set STOPPED component to SHUTDOWN. This is allowed.
        mComponent.setStatus(ComponentState.STOPPED);
        mComponent.setShutdown();
        assertEquals("Failure in setShutdown: state set to " +
                     mComponent.getStatusAsString(),
                     mComponent.getStatus(), ComponentState.SHUTDOWN);

        // Set STARTED component to SHUTDOWN. This is not allowed.
        try
        {
            mComponent.setStatus(ComponentState.STARTED);
            mComponent.setShutdown();
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state cannot change")));
        }
    }

    /**
     * Test the setLoaded method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetLoaded()
        throws Exception
    {
        // Set unset state to LOADED. This is allowed.
        mComponent.setLoaded();
        assertEquals("Failure in setLoaded: state set to " +
                    mComponent.getStatusAsString(),
                    mComponent.getStatus(), ComponentState.LOADED);

        // Set SHUTDOWN component to LOADED. This is allowed.
        mComponent.setStatus(ComponentState.SHUTDOWN);
        mComponent.setLoaded();
        assertEquals("Failure in setLoaded: state set to " +
                    mComponent.getStatusAsString(),
                    mComponent.getStatus(), ComponentState.LOADED);


        // Set STOPPED component to LOADED. This is not allowed.
        try
        {
            mComponent.setStatus(ComponentState.STOPPED);
            mComponent.setLoaded();
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state cannot change")));
        }

        // Set STARTED component to LOADED. This is not allowed.
        try
        {
            mComponent.setStatus(ComponentState.STARTED);
            mComponent.setLoaded();
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state cannot change")));
        }
    }

    /**
     * Test the setStarted method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetStarted()
        throws Exception
    {
        // Set LOADED component to STARTED. This is not allowed.
        try
        {
            mComponent.setStatus(ComponentState.LOADED);
            mComponent.setStarted();
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state cannot change")));
        }

        // Set SHUTDOWN component to STARTED. This is not allowed.
        try
        {
            mComponent.setStatus(ComponentState.SHUTDOWN);
            mComponent.setStarted();
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state cannot change")));
        }

        // Set STOPPED component to STARTED. This is allowed.
        mComponent.setStatus(ComponentState.STOPPED);
        mComponent.setStarted();
        assertEquals("Failure in setStarted: state set to " +
                     mComponent.getStatusAsString(),
                     mComponent.getStatus(), ComponentState.STARTED);
    }

    /**
     * Test the setStopped method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetStopped()
        throws Exception
    {
        // Set LOADED component to STOPPED. This is not allowed.
        try
        {
            mComponent.setStatus(ComponentState.LOADED);
            mComponent.setStopped();
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state cannot change")));
        }

        // Set SHUTDOWN component to STOPPED. This is allowed.
        mComponent.setStatus(ComponentState.SHUTDOWN);
        mComponent.setStopped();
        assertEquals("Failure in setStopped: state set to " +
                     mComponent.getStatusAsString(),
                     mComponent.getStatus(), ComponentState.STOPPED);

        // Set STARTED component to STOPPED. This is allowed.
        mComponent.setStatus(ComponentState.STARTED);
        mComponent.setStopped();
        assertEquals("Failure in setStopped: state set to " +
                     mComponent.getStatusAsString(),
                     mComponent.getStatus(), ComponentState.STOPPED);
    }

}
