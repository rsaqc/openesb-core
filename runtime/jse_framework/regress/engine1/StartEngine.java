/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StartEngine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package engine1;

import com.sun.jbi.framework.AbstractComponent;
import java.util.logging.Logger;
import javax.jbi.component.ComponentContext;

/**
 * This is an implementation of a Engine life cycle which starts a 
 * thread.  The thread is the main implementation of the engine.
 *
 * @author Sun Microsystems, Inc.
 */
public class StartEngine extends AbstractComponent
{
    /**
     *  Actual Engine implementation
     */
    private EngineImpl mEngine;

    /**
     * Public constructor.
     */
    public StartEngine()
    {
        mLog = Logger.getLogger("com.sun.jbi.framework.test");
        mComponentType = "Engine";
    }

    //
    // Overridden ComponentLifeCycle methods
    //

    /**
     * Initialize the Service Engine.
     * @param context the JBI component context created by the JBI framework.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void init(ComponentContext context)
        throws javax.jbi.JBIException
    {
        if ( null != context )
        {
            mComponentName = context.getComponentName();
            mLog.info("Engine " + mComponentName + " initialized");
        }
        else
        {
            throw new javax.jbi.JBIException("Null argument received for " +
                                             "ComponentContext");
        }

        mEngine = new EngineImpl(context, context.getDeliveryChannel(), mLog);
        
        try
        {
            mEngine.init();
        }
        catch (Exception ex)
        {
            throw new javax.jbi.JBIException(ex);
        }
    }

    /**
     * Start the Service Engine.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.info("Engine " + mComponentName + " started");
        mEngine.start();
    }
 
}
