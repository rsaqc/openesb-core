/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PlatformContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.platform;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.security.KeyStoreUtil;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.naming.InitialContext;

/**
 * This interface defines a generic way to obtain application server platform-
 * specific information at runtime. Each platform must provide an implementation
 * of this interface to the JBI framework.
 *
 * @author Sun Microsystems Inc.
 */
public interface PlatformContext
{
    /**
     * Token used in the JBI registry to account for instance-local root path.
     */
    public static String INSTANCE_ROOT_TOKEN = 
            "com.sun.jbi.platform.instanceRoot";
    /**
     * Token used in the JBI registry to account for instance-local install
     * root path.
     */
    public static String INSTALL_ROOT_TOKEN  = 
            "com.sun.jbi.platform.installRoot";
    
    /**
     * The JBI Parent Logger
     */
    public static String JBI_LOGGER_NAME = "com.sun.jbi";
    
    /**
     * Get the TransactionManager for this implementation. The instance
     * returned is an implementation of the standard JTS interface. If none
     * is available, returns <CODE>null</CODE>.
     * @return a <CODE>TransactionManager</CODE> instance.
     * @throws Exception if a <CODE>TransactionManager</CODE> cannot be obtained.
     */
    javax.transaction.TransactionManager getTransactionManager()
        throws Exception;

    /**
     * Get the MBean server connection for a particular instance.
     * @return the <CODE>MBeanServerConnection</CODE> for the specified instance.
     * @throws Exception if a <CODE>MBeanServerConnection</CODE> cannot be
     * obtained.
     */
    MBeanServerConnection getMBeanServerConnection(String instanceName)
        throws Exception;
   
    /**
     * Get the system class loader for this implementation.
     * @return the <CODE>ClassLoader</CODE> that is the "system" class loader
     * from a JBI runtime perspective.
     */
    ClassLoader getSystemClassLoader();

    /**
     * Get the instance name of the platform's administration server.  If the
     * platform does not provide a separate administration server, then this 
     * method returns the name of the local instance.
     * @return instance name of the administration server
     */
    String getAdminServerName();
    
    /**
     * Determine whether this instance is the administration server instance.
     * @return <CODE>true</CODE> if this instance is the administration server,
     * <CODE>false</CODE> if not.
     */
    boolean isAdminServer();
    
    /**
     * Get the name of this instance.
     * @return the name of this server instance.
     */
    String getInstanceName();
        
    /**
     * Determine if the specified instance is up.
     * @return true if the instance is up and running, false otherwise
     */
    boolean isInstanceUp(String instanceName);
    
    /**
     * Determine whether multiple servers are permitted within this AS
     * installation.
     * @return true if multiple servers are permitted.
     */
    boolean supportsMultipleServers();
    
    /**
     * Get the Target Name. If the instance is not a clustered instance then
     * the target name is the instance name. If the instance is part of a
     * cluster then the target name is the cluster name.
     *
     * @return the target name. 
     */
    String getTargetName();

    /**
     * Get the Target Name for a specified instance. If the instance is not
     * clustered the instance name is returned. This operation is invoked by
     * the JBI instance MBeans only.
     *
     * @return the target name. 
     */
    String getTargetName(String instanceName);
    
    /**
     * Get a set of the names of all the standalone servers in the domain.
     * @return a set of names of standalone servers in the domain.
     */
    Set<String> getStandaloneServerNames();
    
    /**
     * Get a set of the names of all the clustered servers in the domain.
     * @return a set of names of clustered servers in the domain.
     */
    Set<String> getClusteredServerNames();

    /**
     * Get a set of the names of all the clusters in the domain.
     * @return a set of names of clusters in the domain.
     */
    Set<String> getClusterNames();
    
    /**
     * Get a set of the names of all the servers in the specified cluster.
     * @return a set of names of servers in the cluster.
     */
    Set<String> getServersInCluster(String clusterName);
    
    /**
     * Determine whether a target is a valid server or cluster name.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a valid
     * standalone server name or cluster name, <CODE>false</CODE> if not.
     */
    boolean isValidTarget(String targetName);
    
    /**
     * Determine whether a target is a cluster.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a cluster,
     * <CODE>false</CODE> if not.
     */
    boolean isCluster(String targetName);
    
    /**
     * Determine whether a target is a standalone server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a standalone
     * server, <CODE>false</CODE> if not.
     */
    boolean isStandaloneServer(String targetName);
    
    /**
     * Determine whether the target is a clustered server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a clustered
     * server, <CODE>false</CODE> if not.
     */
    boolean isClusteredServer(String targetName);
    
    /**
     * Determine whether or not an instance is clustered.
     * @return <CODE>true</CODE> if the instance is clustered,
     * <CODE>false</CODE> if not.
     */
    boolean isInstanceClustered(String instanceName);

    /**
     * Get a string representation of the DAS JMX RMI connector port.
     * @return the JMX RMI connector port as a (CODE>String</CODE>.
     */
    String getJmxRmiPort();
    
    /**
     * Provides access to the platform's MBean server.
     * @return platform MBean server.
     */
    MBeanServer getMBeanServer();
    
    /**
     * Get the full path to the platform's instance root directory.
     * @return platform instance root
     */
    String getInstanceRoot();
    
    /**
     * Get the full path to the platform's instaall root directory.
     * @return platform install root
     */
    String getInstallRoot();
    
    /**
     * Returns the provider type for this platform.
     * @return enum value corresponding to this platform implementation.
     */
    JBIProvider getProvider();
    
    /**
     *  Retrieves the naming context that should be used to locate platform
     *  resources (e.g. TransactionManager).
     *  @return naming context
     */
    InitialContext getNamingContext();
    
    /**
     * Register a listener for platform events.
     * @param listener listener implementation
     */
    void addListener(PlatformEventListener listener);
    
    
    /**
     * Remove a listener for platform events.
     * @param listener listener implementation
     */
    void removeListener(PlatformEventListener listener);

    /**
     * Returns a platform-specific implementation of KeyStoreUtil.
     *
     * @return  an instance of KeyStoreUtil or null if KeyStoreUtil
     * is not supported as part of this platform.
     */
    KeyStoreUtil getKeyStoreUtil();
    
    /**
     * Get the "com.sun.jbi" log level for a target
     *
     * @param target - target name
     * @return the default platform log level
     */
    java.util.logging.Level getJbiLogLevel(String target);
    
    
    /**
     * Set the "com.sun.jbi" log level for a target.
     *
     * @param target - target name
     * @param level the default platform log level
     */
    void setJbiLogLevel(String target, java.util.logging.Level level);
}
