/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Context.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.security;

/**
 * The Context is an interface to name-value pair context.
 *
 * @author Sun Microsystems, Inc.
 */
public interface Context
{
    
    /* ------------------------------------------------------------------------------- *\
     *                                Context                                          *
    \* ------------------------------------------------------------------------------- */ 
    
    /**
     * @param key - paramter key
     * @return true if the Context contains a particular Key
     */
    boolean containsKey(String key);
    
    /**
     * @param key whose associated value is to be returned.
     * @return the Object value associated with a particular Key
     */  
    Object getValue(String key);
    
    /**
     * @param key is the Key whose value is to be set.
     * @param value is the value for the Key.
     */   
    void setValue(String key, Object value);
    
    /**
     * Enumerate all the Keys in the Context.
     *
     * @return the List of Keys (Strings) in the Context.
     */
    java.util.Set enumerateKeys();
    
    /**
     * Remove a entry.
     *
     * @param key is the Key identifying the entry to be deleted.
     */
    void removeValue(String key);
    
    /**
     * Print the Contents of the Context to the Logger.
     *
     * @param logger is the java.util.Logger to use for printing out the contents.
     * @param level is the logging level
     */
    void print(java.util.logging.Logger logger, java.util.logging.Level level);
}
