/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentListener.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  DeploymentListener.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */
package com.sun.jbi.binding.security;


/**
 * The Deployment Listener is notified of Endpoint deployments.
 * @author Sun Microsystems, Inc.
 */
public interface DeploymentListener
{
    /**
     * Notification for a new Endpoint Deployment.
     *
     * @param endpoint is the Endpoint being deployed.
     * @throws Exception on Errors
     */
    void addDeployment (Endpoint endpoint) 
        throws Exception;
    
    /**
     * Notification for removal of an Endpoint Deployment.
     *
     * @param endpoint is the Endpoint being undeployed.
     */
    void removeDeployment(Endpoint endpoint);  
}
