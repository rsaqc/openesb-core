/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Interceptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Interceptor.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */

package com.sun.jbi.binding.security;

import javax.security.auth.Subject;

/**
 * The Interceptor used by clients to delegate processing requests.
 * @author Sun Microsystems, Inc.
 */
public interface Interceptor
{
    /**
     * Process an incoming message. For an Inbound endpoint the incoming message is 
     * a request being sent to invoke a particular operation in a Service. For an 
     * Outbound endpoint the incoming message is a Response to a earlier message.
     *
     * @param endpoint is the deployed Endpoint which is the sink of the Message
     * @param operation is the operation being invoked
     * @param subject is the Subject to be updated with the Senders Identity
     * @param msgCtx is the MessageContext which is a wrapper around the message.
     * @throws MessageHandlerException on Errors
     */
    void processIncomingMessage(Endpoint endpoint, String operation, 
        MessageContext msgCtx, Subject subject)
        throws MessageHandlerException;
    
    /**
     * Process an outbound message. For an Inbound endpoint the outgoind message is 
     * a response being sent to an earlier request to the Endpoint to invoke a particular
     * operation in a Service. For an Outbound endpoint the outbound message is a Request
     * being sent to a remote Service to invoke a operation.
     *
     * @param endpoint is the deployed Endpoint which is the source of the Message
     * @param operation is the operation being invoked
     * param subject is the Subject which identifies the Sender.
     * @param msgCtx is the MessageContext which is a wrapper around the message.
     * @throws MessageHandlerException on Errors
     */
    void processOutgoingMessage(Endpoint endpoint, String operation, 
        MessageContext msgCtx, Subject subject)
        throws MessageHandlerException;
}
