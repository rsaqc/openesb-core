/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PasswordCredential.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  PasswordCredential.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 23, 2005, 6:36 PM
 */

package com.sun.jbi.binding.security;

/**
 * PrivateCredential for a Subject to encapsulate Username and Password information.
 * Currently used only on outbound
 *
 * @author Sun Microsystems, Inc.
 */
public class PasswordCredential
{
    /** Username. */
    private String mUsername;
    
    /** Password. */
    private String mPassword;
    
    /** 
     * Creates a new instance of PasswordCredential.
     *
     * @param username - user name
     * @param password - password
     */
    public PasswordCredential (String username, String password)
    {
        mUsername = username;
        mPassword = password;
    }
    
    /**
     * @return username string
     */
    public String getUsername()
    {
        return mUsername;
    }
    
    /**
     * @return password string
     */
    public String getPassword()
    {
        return mPassword;
    }
    
    
    /**
     * @param obj Object to check for equality
     * @return true if obj is equal to this instance.
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof PasswordCredential)
        {
            PasswordCredential otherCredential = (PasswordCredential) obj;
            
            return ( getUsername().equals(otherCredential.getUsername()) &&
                getPassword().equals(otherCredential.getPassword()));
        }
        return false;
    
    }
    
    /**
     * @return the hash code.
     */
    public int hashCode()
    {
        return getUsername().hashCode() + getPassword().hashCode();
    }
    
}
