/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EnvironmentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

/**
 * This context contains data needed by all components and services running in
 * the JBI environment. This interface defines only methods that are not part
 * of the JBI specification.
 *
 * @author Sun Microsystems, Inc.
 */
public interface EnvironmentContext
{
    /**
     * Get the AppServer installation root directory.
     * @return The AppServer install root.
     */
    String getAppServerInstallRoot();

    /**
     * Get the AppServer instance root directory.
     * @return The AppServer instance root.
     */
    String getAppServerInstanceRoot();

    /**
     * Get the ComponentManager handle.
     * @return The ComponentManager instance.
     */
    ComponentManager getComponentManager();

    /**
     * Get the ComponentQuery handle.
     * @return The ComponentQuery instance.
     */
    ComponentQuery getComponentQuery();
    
    /**
     * Get the ComponentQuery for a specified target.
     * @param targetName - either "domain" or a valid server / cluster name
     * @return The ComponentQuery instance.
     */
    ComponentQuery getComponentQuery(String targetName);
    
    /**
     * Get the Event Notifier for emitting event notifications.
     * @return The EventNotifier instance.
     */
    com.sun.jbi.framework.EventNotifierCommon getNotifier();
    
    /**
     * Get a reference to the persisted JBI registry.
     * @return Registry instance
     */
    com.sun.jbi.registry.Registry getRegistry();
    
    /**
     * Get a read-only reference to the persisted JBI Registry. A DOM registry document 
     * object is returned. 
     * @return the registry document
     */
    org.w3c.dom.Document getReadOnlyRegistry();
    
    /**
     * Indicates whether or not the JBI framework has been fully started.  This
     * method provides clients with a way of determining if the JBI framework
     * started up in passive mode as a result of on-demand initialization.  
     * The 'start' parameter instructs the framework to
     * start completely if it has not already done so.  If the framework has
     * already been started, the request to start again is ignored.
     * @param start requests that the framework start completely before
     *  returning.
     * @return true if the framework is completely started, false otherwise.
     */
    public boolean isFrameworkReady(boolean start);
    
    /**
     * Get the ConnectionManager handle.
     * @return The ConnectionManager instance.
     */
     com.sun.jbi.messaging.ConnectionManager getConnectionManager();

    /**
     * Gets the initial properties specified in the domain.xml file.
     * @return The initial properties from the AppServer.
     */
    java.util.Properties getInitialProperties();

    /**
     * Get the JBI install root directory path.
     * @return The JBI install root directory path.
     */
    String getJbiInstallRoot();

    /**
     * Get the JBI instance root directory path.
     * @return The JBI instance root directory path.
     */
    String getJbiInstanceRoot();

    /**
     * Get a handle to the class implementing management for the named
     * JBI system service.
     * @param aServiceName - the name of the JBI system service.
     * @return The instance implementing management for the service.
     */
    Object getManagementClass(String aServiceName);

    /**
     * Get the management message factory which enables JBI components
     * to construct status and exception messages.
     * @return An instance of ManagementMessageFactory.
     */
    com.sun.jbi.management.ManagementMessageFactory getManagementMessageFactory();

    /**
     * Get a handle to the MBeanHelper implementation.
     * @return The MBeanHelper instance.
     */
    com.sun.jbi.management.MBeanHelper getMBeanHelper();

    /**
     * Get a handle to the MBeanNames service for use in creating MBean
     * names.
     * @return Handle to the MBeanNames service.
     */
    com.sun.jbi.management.MBeanNames getMBeanNames();

    /**
     * Get the JMX MBean server used to register all MBeans in the JBI
     * framework.
     * @return The MBean server handle.
     */
    javax.management.MBeanServer getMBeanServer();

    /**
     * Get the JNDI naming context for this implementation. This context
     * is a standard JNDI InitialContext but its content will vary based
     * on the environment in which the JBI implementation is running.
     * @return The JNDI naming context.
     */
    javax.naming.InitialContext getNamingContext();

    /**
     * Get the platform-specific context for this implementation.
     * @return The PlatformContext.
     */
    com.sun.jbi.platform.PlatformContext getPlatformContext();

    /**
     * Get the ServiceUnitRegistration handle.
     * @return The ServiceUnitRegistration instance.
     */
    ServiceUnitRegistration getServiceUnitRegistration();

    /**
     * Get a StringTranslator for a specific package name.
     * @param packageName - the name of the package for which a StringTranslator
     * is being requested.
     * @return The StringTranslator for the named package.
     */
    StringTranslator getStringTranslator(String packageName);

    /**
     * Get a StringTranslator for a specific object.
     * @param object - the object for which a StringTranslator is being
     * requested, using the name of the package containing the object.
     * @return The StringTranslator for the object's package.
     */
    StringTranslator getStringTranslatorFor(Object object);

    /**
     * Get the TransactionManager for this implementation. The instance
     * returned is an implementation of the standard JTS interface. If none
     * is available, returns null.
     * @return A TransactionManager instance.
     */
    javax.transaction.TransactionManager getTransactionManager();

    /**
     * Get a handle to the VersionInfo implementation.
     * @return The VersionInfo instance.
     */
    com.sun.jbi.VersionInfo getVersionInfo();
    
    /**
     * Gets the provider typeof JBI.
     * @return The JBI provider.
     */
    JBIProvider getProvider();

    /**
     * Get the JBI Framework initialization timestamp in milliseconds.
     * @return the time, in milliseconds, when the JBI Framework was initialized.
     */
    long getJbiInitTime();

    /**
     * Get a copy of the WSDL factory. This needs to be done before
     * any reading, writing, or manipulation of WSDL documents can
     * be performed using the WSDL API.
     *
     * @return An instance of the WSDL factory.
     * @exception WsdlException If the factory cannot be instantiated.
     */
    com.sun.jbi.wsdl2.WsdlFactory getWsdlFactory() 
            throws com.sun.jbi.wsdl2.WsdlException; 
    
    /**
     * This method is used to find out if start-on-deploy is enabled.
     * When this is enabled components are started automatically when 
     * there is deployment for them. 
     * This is controlled by the property com.sun.jbi.startOnDeploy.
     * By default start-on-deploy is enabled. 
     * It is disabled only if com.sun.jbi.startOnDeploy=false.
     */
    public boolean isStartOnDeployEnabled();
    
    /**
     * This method is used to find out if start-on-verify is enabled.
     * When this is enabled components are started automatically when 
     * an application has to be verified for that component. 
     * This is controlled by the property com.sun.jbi.startOnVerify.
     * By default start-on-verify is enabled. 
     * It is disabled only if com.sun.jbi.startOnVerify=false.
     */
    public boolean isStartOnVerifyEnabled();    

}
