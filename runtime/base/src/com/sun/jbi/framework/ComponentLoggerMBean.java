/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLoggerMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.logging.Logger;
import java.util.logging.Level;
import javax.management.ObjectName;


/**
 * This interface defines the LoggerMBean for a Component (BC or SE). This
 * MBean acts as an agent between the JMX management service and the UI runtime
 * to provide operations for displaying and modifying the logger levels for
 * all loggers created by a component.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentLoggerMBean
{
    /**
     * Get the localized display name of the specified logger.
     * @return String representing the localized display name.
     */
    String getDisplayName(String logName);

    /**
     * Get the log level of the specified logger. If the level is not set,
     * search the parent logger chain until a logger is found with a level set.
     * @param logName the name of the logger.
     * @return String representing log level or null if no level is set.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    String getLevel(String logName)
        throws javax.jbi.JBIException;

    /**
     * Get the names of all loggers controlled by this MBean.
     * @return List a list of logger names as String objects.
     */
    String[] getLoggerNames();

    /**
     * Set the log level of the specified logger to ALL.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setAll(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to CONFIG.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setConfig(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to the default, which is
     * null to inherit the parent logger's level.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setDefault(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to FINE.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setFine(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to FINER.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setFiner(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to FINEST.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setFinest(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to INFO.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setInfo(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to OFF.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setOff(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to SEVERE.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setSevere(String logName)
        throws javax.jbi.JBIException;

    /**
     * Set the log level of the specified logger to WARNING.
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setWarning(String logName)
        throws javax.jbi.JBIException;
}
