/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Import.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * API for  WSDL 2.0 Import component.
 *
 * @author ApiGen AX.00
 */
public interface Import extends ExtensibleDocumentedComponent
{
    /**
     * Get target namespace of import (must be different that this
     * Description TNS).
     *
     * @return Target namespace of import (must be different that this
     * Description TNS)
     */
    String getNamespace();

    /**
     * Set target namespace of import (must be different that this
     * Description TNS).
     *
     * @param theNamespace Target namespace of import (must be different that
     * this Description TNS)
     */
    void setNamespace(String theNamespace);

    /**
     * Get location hint, if any.
     *
     * @return Location hint, if any
     */
    String getLocation();

    /**
     * Set location hint, if any.
     *
     * @param theLocation Location hint, if any
     */
    void setLocation(String theLocation);

    /**
     * Get the description from this import component.
     *
     * @return Description from this import component, if any
     */
    Description getDescription();

    /**
     * Get the definitions from this import component.
     *
     * @deprecated - replaced by getDescription
     * @return Definitions n from this import component, if any
     */
    Definitions getDefinitions();

}

// End-of-file: Import.java
