/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestLocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.sun;

import com.sun.jbi.framework.StringTranslator;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.ResourceBundle;

/**
 * Tests for the LocalStringKeys class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestLocalStringKeys
    extends junit.framework.TestCase
{
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestLocalStringKeys(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Test the message keys to make sure the class and the properties file
     * have the same set of keys.
     * @throws java.util.MissingResourceException when
     * ResourceBundle.getBundle fails
     */
    public void testKeys()
        throws java.util.MissingResourceException
    {
        System.out.println("TestLocalStringKeys.testKeys(): " +
                           "Testing that LocalStringKeys interface and " +
                           "LocalStrings.properties have " +
                           "the same set of keys.");

        // Load resource bundle

        String packageName = this.getClass().getPackage().getName();
        String bundleName = packageName + "." +
                            StringTranslator.RESOURCE_BUNDLE_NAME;
        ResourceBundle bundle = null;
        bundle = ResourceBundle.getBundle(bundleName);

        boolean mismatch = false;

        // Get a list of all the fields defined in the LocalStringKeys class

        Field[] keyFields = (LocalStringKeys.class).getDeclaredFields();
        Field keyField;
        int numFields = keyFields.length;

        // Get a list of all the keys defined in the resource bundle

        Enumeration rbKeys = bundle.getKeys();
        String rbKey = null;
        int numKeys = 0;

        // Verify that all the keys in the resource bundle are defined in the
        // fields in the LocalStringKeys class

        while (rbKeys.hasMoreElements())
        {
            rbKey = (String) rbKeys.nextElement();
            numKeys++;
            try
            {
                keyField = LocalStringKeys.class.getDeclaredField(rbKey);
            }
            catch ( NoSuchFieldException nsfEx )
            {
                System.out.println("Resource bundle key " + rbKey +
                    " is not defined in LocalStringKeys");
                mismatch = true;
            }
        }

        // Verify that all the fields defined in the LocalStringKeys class
        // are defined as keys in the resource bundle

        int i = 0;
        String str;
        while ( i < numFields )
        {
            try
            {
                rbKey = keyFields[i].getName();
                str = bundle.getString(rbKey);
            }
            catch ( java.util.MissingResourceException mrEx )
            {
                System.out.println("LocalStringKeys field " + rbKey +
                    " is not defined in the resource bundle");
                mismatch = true;
            }
            i++;
        }

        // Ensure that the bundle and the interface have the same number
        // of keys and they all match

        assertFalse("Resource bundle has " + numKeys + " keys; " +
                    "LocalStringKeys interface has " + numFields + " fields",
                    mismatch);
    }

    /**
     * Test the content of the properties file for syntax errors.
     * @throws java.util.MissingResourceException when
     * ResourceBundle.getBundle fails
     */
    public void testProperties()
        throws java.util.MissingResourceException
    {
        System.out.println("TestLocalStringKeys.testProperties(): " +
                           "Testing that LocalStrings.properties has " +
                           "no syntax errors.");

        // Load resource bundle

        String packageName = this.getClass().getPackage().getName();
        String bundleName = packageName + "." +
                            StringTranslator.RESOURCE_BUNDLE_NAME;
        ResourceBundle bundle = null;
        bundle = ResourceBundle.getBundle(bundleName);

        boolean errors = false;

        // Get a list of all the keys defined in the resource bundle

        Enumeration rbKeys = bundle.getKeys();

        String rbKey = null;
        String rbMsg = null;
        String msg = null;
        int numBad = 0;

        // Check each property in the resource bundle for syntax errors.
        // Note: if a MissingResourceException is thrown here, something is
        // seriously wrong!

        while (rbKeys.hasMoreElements())
        {
            rbKey = (String) rbKeys.nextElement();
            try
            {   rbMsg = bundle.getString(rbKey);
                msg = MessageFormat.format(rbMsg,
                    "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH");
            }
            catch ( IllegalArgumentException iaEx )
            {
                System.out.println("---");
                System.out.println("Resource bundle property " + rbKey +
                    " has invalid syntax: " + rbMsg);
                System.out.println("Error message: " + iaEx.getMessage());
                errors = true;
                ++numBad;
            }
        }

        // Ensure that there were no syntax errors.

        assertFalse("Resource bundle has " + numBad + " syntax errors.",
                    errors);
    }
}
