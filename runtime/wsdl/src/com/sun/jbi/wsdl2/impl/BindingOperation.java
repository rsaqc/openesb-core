/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingOperation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import javax.xml.namespace.QName;
import org.w3.ns.wsdl.BindingOperationType;

/**
 * Abstract implementation of
 * WSDL 2.0 Binding Operation Component.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class BindingOperation extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.BindingOperation
{
    /**
     * Get the BindingOperationType XML bean behind this BindingOperation component.
     *
     * @return The BindingOperationType XML bean behind this component.
     */
    protected final BindingOperationType getBean()
    {
        return (BindingOperationType) this.mXmlObject;
    }

    /**
     * Construct an abstract Binding operation implementation base component.
     * 
     * @param bean      The XML bean for this Binding Operation component
     */
    BindingOperation(BindingOperationType bean)
    {
        super(bean);
    }

    /**
     * Get the target namespace of this operation
     *
     * @return Target namespace of this operation
     */
    public abstract String getTargetNamespace();

    /**
     * Create a new message reference for this operation, appending it to
     * this operation's input list.
     *
     * @param messageLabel NC role name in the {message exchange pattern} of
     * the message for which binding details are provided
     * @return The newly created input message reference, appended to this
     * operation.
     */
    public abstract com.sun.jbi.wsdl2.BindingMessageReference addNewInput(
        String messageLabel);

    /**
     * Create a new message reference for this operation, appending it this
     * operation's output list.
     *
     * @param messageLabel NC name of the message for which binding details
     * are provided.
     * @return The newly created output message reference, appended to this
     * operation.
     */
    public abstract com.sun.jbi.wsdl2.BindingMessageReference addNewOutput(
        String messageLabel);

}

// End-of-file: BindingOperation.java
