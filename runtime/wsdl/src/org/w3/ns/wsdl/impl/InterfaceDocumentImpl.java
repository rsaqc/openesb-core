/*
 * An XML document type.
 * Localname: interface
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.InterfaceDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one interface(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class InterfaceDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.InterfaceDocument
{
    
    public InterfaceDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTERFACE$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "interface");
    
    
    /**
     * Gets the "interface" element
     */
    public org.w3.ns.wsdl.InterfaceType getInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType target = null;
            target = (org.w3.ns.wsdl.InterfaceType)get_store().find_element_user(INTERFACE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "interface" element
     */
    public void setInterface(org.w3.ns.wsdl.InterfaceType xinterface)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType target = null;
            target = (org.w3.ns.wsdl.InterfaceType)get_store().find_element_user(INTERFACE$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.InterfaceType)get_store().add_element_user(INTERFACE$0);
            }
            target.set(xinterface);
        }
    }
    
    /**
     * Appends and returns a new empty "interface" element
     */
    public org.w3.ns.wsdl.InterfaceType addNewInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType target = null;
            target = (org.w3.ns.wsdl.InterfaceType)get_store().add_element_user(INTERFACE$0);
            return target;
        }
    }
}
