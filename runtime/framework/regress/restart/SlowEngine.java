/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SlowEngine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package restart;

import java.util.logging.Logger;
import javax.xml.namespace.QName;

/**
 * This engine is used to test framework restart processing.  The init()
 * method of ServiceUnitManager sleeps for a bit before activating
 * an endpoint.  This should not result in endpoint resolution errors from
 * client components, since all service units should init() before any are 
 * started.
 *
 * @author Sun Microsystems, Inc.
 */
public class SlowEngine extends BaseComponent
{
    /** How long we sleep in init() */
    private static final int SLEEP_TIMER = 10000;
        
    /** Create a new instance of FastBinding */
    public SlowEngine()
    {
        mComponentName = "test-slow-engine";
    }
    
    /**
     * Initialize the deployment.
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state.
     */
    @Override
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        try
        {
            mLog.info("Sleeping for " + SLEEP_TIMER + "ms before initializing " + 
                    serviceUnitName);
            Thread.sleep(SLEEP_TIMER);
            mLog.info("Activating endpoint for " + serviceUnitName);
            mContext.activateEndpoint(new QName(serviceUnitName), serviceUnitName);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new javax.jbi.management.DeploymentException(ex.toString());
        }
    }
}
