/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Binding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;

/**
 * This is an implementation of a Binding Component that is purely for
 * unit testing. It does nothing other than log messages when its methods
 * are called.
 *
 * @author Sun Microsystems, Inc.
 */
public class Binding
    extends AbstractComponent
{
    /**
     * Special flag for causing failure on second call to init().
     */
    private boolean mFirst = true;

    /**
     * Special flag for timeout testing to prevent a timeout after the first
     * call that times out. Without this, junit tests fail because the
     * Component Framework doesn't complete its shutdown processing before
     * the next junit test starts.
     */
    private boolean mTimeoutDone = false;

    /**
     * Constant for slow tests - 1 second sleep interval (in milliseconds).
     */
    private static final long SLOW_INTERVAL = 1000;

    /**
     * Constant for timeout tests - 1 minute sleep interval (in milliseconds).
     */
    private static final long TIMEOUT_INTERVAL = 60000;

    /**
     * Public constructor.
     */
    public Binding()
    {
        mLog = Logger.getLogger("com.sun.jbi.framework.test.Binding");
        mComponentType = "Binding";
    }

    //
    // Overridden ComponentLifeCycle methods
    //

    /**
     * Initialize the Binding Component.
     * @param context the JBI component context created by the JBI framework.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void init(ComponentContext context)
        throws javax.jbi.JBIException
    {
        if ( null != context )
        {
            mContext = context;
            mComponentName = context.getComponentName();
            if ( mComponentName.equals(Constants.BC_NAME_BAD_INIT) )
            {
                throw new javax.jbi.JBIException("BindingBadInit");
            }
            else if ( mComponentName.equals(Constants.BC_NAME_SLOW_INIT) )
            {
                try
                {
                    Thread.sleep(SLOW_INTERVAL);
                }
                catch ( java.lang.InterruptedException iEx )
                {
                    throw new javax.jbi.JBIException("BindingInterrupted");
                }
            }
            else if ( mComponentName.equals(Constants.BC_NAME_TIMEOUT_INIT) )
            {
                try
                {
                    mLog.info("Binding " + mComponentName + " sleeping for " +
                        TIMEOUT_INTERVAL + " milliseconds");
                    Thread.sleep(TIMEOUT_INTERVAL);
                    mLog.info("Binding " + mComponentName + " completed sleep");
                }
                catch ( java.lang.InterruptedException iEx )
                {
                    throw new javax.jbi.JBIException("BindingInterrupted");
                }
            }
            mLog.info("Binding " + mComponentName + " initialized");
        }
        else
        {
            throw new javax.jbi.JBIException("Null argument received for " +
                                             "ComponentContext");
        }

        // This code is here to allow a special test case to run, where the
        // binding is being started after having been shut down, and the
        // init() method throws an exception. The second time init() is
        // called, it throws an exception.

        if ( mFirst )
        {
            mFirst = false;
        }
        else
        {
            throw new javax.jbi.JBIException("Binding initialization failed");
        }
    }

    /**
     * Start the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        if ( mComponentName.equals(Constants.BC_NAME_BAD_START) )
        {
            throw new javax.jbi.JBIException("BindingBadStart");
        }
        else if ( mComponentName.equals(Constants.BC_NAME_SLOW_START) )
        {
            try
            {
                Thread.sleep(SLOW_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("BindingInterrupted");
            }
        }
        else if ( mComponentName.equals(Constants.BC_NAME_TIMEOUT_START) )
        {
            try
            {
                mLog.info("Binding " + mComponentName + " sleeping for " +
                    TIMEOUT_INTERVAL + " milliseconds");
                Thread.sleep(TIMEOUT_INTERVAL);
                mLog.info("Binding " + mComponentName + " completed sleep");
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("BindingInterrupted");
            }
        }
        mLog.info("Binding " + mComponentName + " started");
    }

    /**
     * Stop the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        if ( mComponentName.equals(Constants.BC_NAME_BAD_STOP) )
        {
            throw new javax.jbi.JBIException("BindingBadStop");
        }
        else if ( mComponentName.equals(Constants.BC_NAME_TIMEOUT_STOP)
            && !mTimeoutDone )
        {
            mTimeoutDone = true;
            try
            {
                mLog.info("Binding " + mComponentName + " sleeping for " +
                    TIMEOUT_INTERVAL + " milliseconds");
                Thread.sleep(TIMEOUT_INTERVAL);
                mLog.info("Binding " + mComponentName + " completed sleep");
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("BindingInterrupted");
            }
        }
        else if ( mComponentName.equals(Constants.BC_NAME_SLOW_STOP) )
        {
            try
            {
                Thread.sleep(SLOW_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("BindingInterrupted");
            }
        }
        mLog.info("Binding " + mComponentName + " stopped");
    }

    /**
     * Shut down the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        if ( mComponentName.equals(Constants.BC_NAME_BAD_SHUTDOWN) )
        {
            throw new javax.jbi.JBIException("BindingBadShutdown");
        }
        else if ( mComponentName.equals(Constants.BC_NAME_TIMEOUT_SHUTDOWN)
            && !mTimeoutDone )
        {
            mTimeoutDone = true;
            try
            {
                mLog.info("Binding " + mComponentName + " sleeping for " +
                    TIMEOUT_INTERVAL + " milliseconds");
                Thread.sleep(TIMEOUT_INTERVAL);
                mLog.info("Binding " + mComponentName + " completed sleep");
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("BindingInterrupted");
            }
        }
        else if ( mComponentName.equals(Constants.BC_NAME_SLOW_SHUTDOWN) )
        {
            try
            {
                Thread.sleep(SLOW_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("BindingInterrupted");
            }
        }
        mLog.info("Binding " + mComponentName + " shut down");
    }

    //
    // ServiceUnitManager methods
    //
 
    /**
     * Deploy a Service Unit.
     * @param serviceUnitName the name of the Service Unit being deployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return a deployment status message.
     * @throws javax.jbi.management.DeploymentException if the deployment
     * operation is unsuccessful.
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        if ( serviceUnitName.equals(Constants.SU_NAME_DEPLOY_EXCEPTION) )
        {
            throw new javax.jbi.management.DeploymentException(
                "failed to deploy");
        }
        else if ( serviceUnitName.equals(Constants.SU_NAME_DEPLOY_TIMEOUT)
            && !mTimeoutDone )
        {
            mTimeoutDone = true;
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }

        mLog.info("Binding " + mComponentName + " deployed Service Unit " +
            serviceUnitName);
        return "Successfully deployed " + serviceUnitName;
    }

    /**
     * Initialize the deployment.
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state.
     */
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        if ( serviceUnitName.equals(Constants.SU_NAME_INIT_EXCEPTION) )
        {
            throw new javax.jbi.management.DeploymentException(
                "failed to initialize");
        }
        else if ( serviceUnitName.equals(Constants.SU_NAME_INIT_TIMEOUT)
            && !mTimeoutDone )
        {
            mTimeoutDone = true;
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }
        else if ( serviceUnitName.startsWith(Constants.SU_NAME_INIT_TIMEOUT) )
        {
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }
        mLog.info("Binding " + mComponentName + " initialized Service Unit " +
            serviceUnitName);
    }

    /**
     * Shut down the deployment.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void shutDown(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        if ( serviceUnitName.equals(Constants.SU_NAME_SHUTDOWN_EXCEPTION) )
        {
            throw new javax.jbi.management.DeploymentException(
                "failed to shut down");
        }
        else if ( serviceUnitName.equals(Constants.SU_NAME_SHUTDOWN_TIMEOUT)
            && !mTimeoutDone )
        {
            mTimeoutDone = true;
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }
        else if ( serviceUnitName.startsWith(Constants.SU_NAME_SHUTDOWN_TIMEOUT)
            && !mTimeoutDone )
        {
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }
        mLog.info("Binding " + mComponentName + " shut down Service Unit " +
            serviceUnitName);
    }

    /**
     * Start the deployment.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        if ( serviceUnitName.equals(Constants.SU_NAME_START_EXCEPTION) )
        {
            throw new javax.jbi.management.DeploymentException(
                "failed to start");
        }
        else if ( serviceUnitName.equals(Constants.SU_NAME_START_TIMEOUT)
            && !mTimeoutDone )
        {
            mTimeoutDone = true;
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }
        else if ( serviceUnitName.startsWith(Constants.SU_NAME_START_TIMEOUT)
            && !mTimeoutDone )
        {
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }
        mLog.info("Binding " + mComponentName + " started Service Unit " +
            serviceUnitName);
    }

    /**
     * Stop the deployment.
     * @param serviceUnitName the name of the Service Unit being stopped.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void stop(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        if ( serviceUnitName.equals(Constants.SU_NAME_STOP_EXCEPTION) )
        {
            throw new javax.jbi.management.DeploymentException(
                "failed to stop");
        }
        else if ( serviceUnitName.equals(Constants.SU_NAME_STOP_TIMEOUT)
            && !mTimeoutDone )
        {
            mTimeoutDone = true;
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }
        else if ( serviceUnitName.startsWith(Constants.SU_NAME_STOP_TIMEOUT)
            && !mTimeoutDone )
        {
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }
        mLog.info("Binding " + mComponentName + " stopped Service Unit " +
            serviceUnitName);
    }

    /**
     * Undeploy a Service Unit from the component.
     * @param serviceUnitName the name of the Service Unit being undeployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return an undeployment status message.
     * @throws javax.jbi.management.DeploymentException if the undeployment
     * operation is unsuccessful.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        if ( serviceUnitName.equals(Constants.SU_NAME_UNDEPLOY_EXCEPTION) )
        {
            throw new javax.jbi.management.DeploymentException(
                "failed to undeploy");
        }
        else if ( serviceUnitName.equals(Constants.SU_NAME_UNDEPLOY_TIMEOUT)
            && !mTimeoutDone )
        {
            mTimeoutDone = true;
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL);
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.management.DeploymentException(
                    "BindingInterrupted");
            }
        }

        mLog.info("Binding " + mComponentName + " undeployed Service Unit " +
            serviceUnitName);
        return "Successfully undeployed " + serviceUnitName;
    }
}
