/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * This interface contains the property keys used for looking up message
 * text in the LocalStrings resource bundle.
 *
 * IMPORTANT: The number of property keys defined in this file must always
 *            match the number of properties defined LocalStrings_en.properties.
 *            The keys defined in this file must all match the keys for the
 *            properties defined in LocalStrings_en.properties. The junit test
 *            for this class checks all of this and fails with detailed
 *            messages if any mismatches are found.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{
    //
    // Message keys for common words or phrases
    //

    /**
     * Binding.
     */
    String BINDING =
          "BINDING";
    /**
     * Component.
     */
    String COMPONENT =
          "COMPONENT";
    /**
     * Engine.
     */
    String ENGINE =
          "ENGINE";
    /**
     * Exception message.
     */
    String EXCEPTION_MSG =
          "EXCEPTION_MSG";
    /**
     * No exception message.
     */
    String NO_MESSAGE =
          "NO_MESSAGE";
    /**
     * Shared Unit.
     */
    String SERVICE_UNIT =
          "SERVICE_UNIT";
    /**
     * Shared Library.
     */
    String SHARED_LIBRARY =
          "SHARED_LIBRARY";
    /**
     * Unable to print stack trace.
     */
    String STACK_TRACE_PRINT_FAILED =
          "STACK_TRACE_PRINT_FAILED";
    /**
     * Unknown.
     */
    String UNKNOWN =
          "UNKNOWN";

    //
    // Message keys for common messages
    //

    /**
     * Error message - a required list argument is empty.
     */
    String EMPTY_LIST_ARGUMENT =
          "EMPTY_LIST_ARGUMENT";
    /**
     * Error message - a required string argument is empty.
     */
    String EMPTY_STRING_ARGUMENT =
          "EMPTY_STRING_ARGUMENT";
    /**
     * Error message - an argument is invalid.
     */
    String INVALID_ARGUMENT =
          "INVALID_ARGUMENT";
    /**
     * Error message - a required argument is null.
     */
    String NULL_ARGUMENT =
          "NULL_ARGUMENT";
    /**
     * Warning message - service already initialized.
     */
    String SERVICE_ALREADY_INITIALIZED =
          "SERVICE_ALREADY_INITIALIZED";
    /**
     * Warning message - service already started.
     */
    String SERVICE_ALREADY_STARTED =
          "SERVICE_ALREADY_STARTED";
    /**
     * Warning message - service already stopped.
     */
    String SERVICE_ALREADY_STOPPED =
          "SERVICE_ALREADY_STOPPED";
    /**
     * Info message - service initialized.
     */
    String SERVICE_INITIALIZED =
          "SERVICE_INITIALIZED";
    /**
     * Info message - service started.
     */
    String SERVICE_STARTED =
          "SERVICE_STARTED";
    /**
     * Info message - service stopped.
     */
    String SERVICE_STOPPED =
          "SERVICE_STOPPED";

    //
    // Message keys for JBIFramework
    //

    // service names

    /**
     * Component Framework.
     */
    String CF_NAME =
          "CF_NAME";
    /**
     * Component Registry.
     */
    String CR_NAME =
          "CR_NAME";
    /**
     * Configuration Service .
     */
    String CS_NAME =
          "CS_NAME";
    
    /**
     * Management Service.
     */
    String MS_NAME =
          "MS_NAME";
    /**
     * Normalized Message Service.
     */
    String NMS_NAME =
          "NMS_NAME";
    /**
     * Registry Service.
     */
    String RS_NAME =
          "RS_NAME";
    /**
     * Security Service.
     */
    String SS_NAME =
          "SS_NAME";
    /**
     * Management Runtime Service.
     */
    String MRS_NAME =
          "MRS_NAME";

    // messages

    /**
     * Error message - framework initialization failed.
     */
    String INIT_FRAMEWORK_FAILED =
          "INIT_FRAMEWORK_FAILED";
    /**
     * Error message - service initialization failed.
     */
    String INIT_SERVICE_FAILED =
          "INIT_SERVICE_FAILED";
    /**
     * Logging message - framework initialized.
     */
    String JBI_INITIALIZED =
          "JBI_INITIALIZED";
    /**
     * Logging message - framework initializing.
     */
    String JBI_INITIALIZING =
          "JBI_INITIALIZING";
    /**
     * Logging message - framework ready for requests.
     */
    String JBI_READY =
          "JBI_READY";
    /**
     * Warning message - repository timestamp file creation failed.
     */
    String JBI_REPOSITORY_TIMESTAMP_WARNING =
          "JBI_REPOSITORY_TIMESTAMP_WARNING";
    /**
     * Logging message - framework shutdown elapsed time.
     */
    String JBI_SHUTDOWN_TIME =
          "JBI_SHUTDOWN_TIME";
    /**
     * Logging message - framework shutting down.
     */
    String JBI_SHUTTING_DOWN =
          "JBI_SHUTTING_DOWN";
    /**
     * Logging message - framework shut down.
     */
    String JBI_SHUT_DOWN =
          "JBI_SHUT_DOWN";
    /**
     * Logging message - framework started.
     */
    String JBI_STARTED =
          "JBI_STARTED";
    /**
     * Logging message - framework starting.
     */
    String JBI_STARTING =
          "JBI_STARTING";
    /**
     * Logging message - framework startup elapsed time.
     */
    String JBI_STARTUP_TIME =
          "JBI_STARTUP_TIME";
    /**
     * Logging message - framework terminated.
     */
    String JBI_TERMINATED =
          "JBI_TERMINATED";
    /**
     * Logging message - framework terminating.
     */
    String JBI_TERMINATING =
          "JBI_TERMINATING";
    /**
     * Error message - framework start failed.
     */
    String START_FRAMEWORK_FAILED =
          "START_FRAMEWORK_FAILED";
    /**
     * Error message - service start failed.
     */
    String START_SERVICE_FAILED =
          "START_SERVICE_FAILED";
    /**
     * Error message - framework stop failed.
     */
    String STOP_FRAMEWORK_FAILED =
          "STOP_FRAMEWORK_FAILED";
    /**
     * Error message - service stop failed.
     */
    String STOP_SERVICE_FAILED =
          "STOP_SERVICE_FAILED";

    /**
     * Error message - failed to create jbi admin state file.
     */
    String JBI_STATE_FILE_CREATION_FAILED =
          "JBI_STATE_FILE_CREATION_FAILED";
    //
    // Message keys for ComponentFramework
    //

    /**
     * Error message - shutdown interrupted.
     */
    String CF_SHUTDOWN_INTERRUPTED =
          "CF_SHUTDOWN_INTERRUPTED";
    /**
     * Error message - startup interrupted.
     */
    String CF_STARTUP_INTERRUPTED =
          "CF_STARTUP_INTERRUPTED";
    /**
     * Info message - stopping all components.
     */
    String CF_STOPPING_ALL =
          "CF_STOPPING_ALL";

    // bootstrap messages

    /**
      Error message - bootstrap init() method failed.
     */
    String CF_BOOTSTRAP_GET_EXTENSION_FAILED =
          "CF_BOOTSTRAP_GET_EXTENSION_FAILED";
    /**
     * Error message - bootstrap cleanUp() method failed.
     */
    String CF_BOOTSTRAP_CLEANUP_FAILED =
          "CF_BOOTSTRAP_CLEANUP_FAILED";
    /**
      Error message - bootstrap init() method failed.
     */
    String CF_BOOTSTRAP_INIT_FAILED =
          "CF_BOOTSTRAP_INIT_FAILED";
    /**
      Error message - bootstrap cleanUp() method failed after onInstall().
     */
    String CF_BOOTSTRAP_INSTALL_CLEANUP_FAILED =
          "CF_BOOTSTRAP_INSTALL_CLEANUP_FAILED";
    /**
     * Error message - bootstrap not loaded.
     */
    String CF_BOOTSTRAP_NOT_LOADED =
          "CF_BOOTSTRAP_NOT_LOADED";
    /**
     * Error message - bootstrap onInstall() method failed.
     */
    String CF_BOOTSTRAP_ONINSTALL_FAILED =
          "CF_BOOTSTRAP_ONINSTALL_FAILED";
    /**
     * Error message - bootstrap onUninstall() method failed.
     */
    String CF_BOOTSTRAP_ONUNINSTALL_FAILED =
          "CF_BOOTSTRAP_ONUNINSTALL_FAILED";
    /**
      Error message - bootstrap cleanUp() method failed after onUninstall().
     */
    String CF_BOOTSTRAP_UNINSTALL_CLEANUP_FAILED =
          "CF_BOOTSTRAP_UNINSTALL_CLEANUP_FAILED";
    /**
      Error message - bootstrap cleanUp() method failed during unload.
     */
    String CF_BOOTSTRAP_UNLOAD_CLEANUP_FAILED =
          "CF_BOOTSTRAP_UNLOAD_CLEANUP_FAILED";

    // component-related messages

    /**
     * Error message - component already installed.
     */
    String CF_COMP_ALREADY_INSTALLED =
          "CF_COMP_ALREADY_INSTALLED";
    /**
     * Error message - component ID already registered.
     */
    String CF_COMP_ALREADY_REGISTERED =
          "CF_COMP_ALREADY_REGISTERED";
    /**
     * Error message - component cannot be shut down.
     */
    String CF_COMP_CANNOT_SHUTDOWN =
          "CF_COMP_CANNOT_SHUTDOWN";
    /**
     * Error message - component cannot be started.
     */
    String CF_COMP_CANNOT_START =
          "CF_COMP_CANNOT_START";
    /**
     * Error message - component cannot be stopped.
     */
    String CF_COMP_CANNOT_STOP =
          "CF_COMP_CANNOT_STOP";
    /**
     * Error message - initialization of component timed out.
     */
    String CF_COMP_INIT_TIMEOUT =
          "CF_COMP_INIT_TIMEOUT";
    /**
     * Error message - component initialization failed due to an exception.
     */
    String CF_COMP_INITIALIZATION_EXCEPTION =
          "CF_COMP_INITIALIZATION_EXCEPTION";
    /**
     * Error message - component initialization failed due to an exception.
     */
    String CF_COMP_INITIALIZATION_FAILED_EXCEPTION =
          "CF_COMP_INITIALIZATION_FAILED_EXCEPTION";
    /**
     * Error message - installation failed.
     */
    String CF_COMP_INSTALL_FAILED =
          "CF_COMP_INSTALL_FAILED";
    /**
     * Info message - installation succeeded.
     */
    String CF_COMP_INSTALL_SUCCESSFUL =
          "CF_COMP_INSTALL_SUCCESSFUL";
    /**
     * Error message - installer MBean registration failed.
     */
    String CF_COMP_INSTALLER_MBEAN_REGISTRATION_FAILED =
          "CF_COMP_INSTALLER_MBEAN_REGISTRATION_FAILED";
    /**
     * Error message - required shared library not installed.
     */
    String CF_COMP_MISSING_SHARED_LIBRARY =
          "CF_COMP_MISSING_SHARED_LIBRARY";
    /**
     * Error message - required shared libraries not installed.
     */
    String CF_COMP_MISSING_SHARED_LIBRARY_LIST =
          "CF_COMP_MISSING_SHARED_LIBRARY_LIST";
    /**
     * Error message - required shared libraries not installed.
     */
    String CF_COMP_MISSING_SHARED_LIBRARIES_START =
          "CF_COMP_MISSING_SHARED_LIBRARIES_START";
    /**
     * Error message - component not installed.
     */
    String CF_COMP_NO_COMP_INSTALLED =
          "CF_COMP_NO_COMP_INSTALLED";
    /**
     * Error message - component not found.
     */
    String CF_COMP_NOT_FOUND =
          "CF_COMP_NOT_FOUND";
    /**
     * Error message - component not installed.
     */
    String CF_COMP_NOT_INSTALLED =
          "CF_COMP_NOT_INSTALLED";
    /**
     * Error message - component not loaded.
     */
    String CF_COMP_NOT_LOADED =
          "CF_COMP_NOT_LOADED";
    /**
     * Info message - shutdown forced.
     */
    String CF_COMP_SHUTDOWN_FORCED =
          "CF_COMP_SHUTDOWN_FORCED";
    /**
     * Info message - shutdown component.
     */
    String CF_COMP_SHUTDOWN_SUCCESSFUL =
          "CF_COMP_SHUTDOWN_SUCCESSFUL";
    /**
     * Error message - shutdown component timed out.
     */
    String CF_COMP_SHUTDOWN_TIMEOUT =
          "CF_COMP_SHUTDOWN_TIMEOUT";
    /**
     * Info message - started component.
     */
    String CF_COMP_START_SUCCESSFUL =
          "CF_COMP_START_SUCCESSFUL";
    /**
     * Error message - start of component timed out.
     */
    String CF_COMP_START_TIMEOUT =
          "CF_COMP_START_TIMEOUT";
    /**
     * Info message - inactive component.
     */
    String CF_COMP_STOP_NOT_ACTIVE =
          "CF_COMP_STOP_NOT_ACTIVE";
    /**
     * Info message - stopped component.
     */
    String CF_COMP_STOP_SUCCESSFUL =
          "CF_COMP_STOP_SUCCESSFUL";
    /**
     * Error message - uninstallation failed.
     */
    String CF_COMP_UNINSTALL_FAILED =
          "CF_COMP_UNINSTALL_FAILED";
    /**
     * Error message - uninstallation failed due to deployed SUs.
     */
    String CF_COMP_UNINSTALL_FAILED_HAS_SUS =
          "CF_COMP_UNINSTALL_FAILED_HAS_SUS";
    /**
     * Error message - uninstalling a running component.
     */
    String CF_COMP_UNINSTALL_RUNNING =
          "CF_COMP_UNINSTALL_RUNNING";
    /**
     * Error message - uninstalling a stopped component.
     */
    String CF_COMP_UNINSTALL_STOPPED =
          "CF_COMP_UNINSTALL_STOPPED";
    /**
     * Error message - uninstallation succeeded.
     */
    String CF_COMP_UNINSTALL_SUCCESSFUL =
          "CF_COMP_UNINSTALL_SUCCESSFUL";
    /**
     * Info message - update completed.
     */
    String CF_COMP_UPDATE_COMPLETE =
          "CF_COMP_UPDATE_COMPLETE";
    /**
     * Error message - cancel upgrade not allowed, component not found.
     */
    String CF_COMP_UPGRADE_CANCEL_NOT_FOUND =
          "CF_COMP_UPGRADE_CANCEL_NOT_FOUND";
    /**
     * Info message - upgrade completed.
     */
    String CF_COMP_UPGRADE_COMPLETE =
          "CF_COMP_UPGRADE_COMPLETE";
    /**
     * Error message - upgrade failed.
     */
    String CF_COMP_UPGRADE_FAILED =
          "CF_COMP_UPGRADE_FAILED";
    /**
     * Error message - IllegalAccessException on upgrade.
     */
    String CF_COMP_UPGRADE_ILLEGAL_ACCESS =
          "CF_COMP_UPGRADE_ILLEGAL_ACCESS";
    /**
     * Error message - upgrade load failed.
     */
    String CF_COMP_UPGRADE_LOAD_FAILED =
          "CF_COMP_UPGRADE_LOAD_FAILED";
    /**
     * Error message - missing shared libraries on upgrade.
     */
    String CF_COMP_UPGRADE_MISSING_SHARED_LIBRARIES =
          "CF_COMP_UPGRADE_MISSING_SHARED_LIBRARIES";
    /**
     * Error message - upgrade not allowed, component not found.
     */
    String CF_COMP_UPGRADE_NOT_FOUND =
          "CF_COMP_UPGRADE_NOT_FOUND";
    /**
     * Error message - upgrade not allowed, component still active.
     */
    String CF_COMP_UPGRADE_NOT_SHUT_DOWN =
          "CF_COMP_UPGRADE_NOT_SHUT_DOWN";
    /**
     * Error message - upgrade not allowed, validate not done.
     */
    String CF_COMP_UPGRADE_NOT_VALIDATED =
          "CF_COMP_UPGRADE_NOT_VALIDATED";
    /**
     * Error message - validation for upgrade failed.
     */
    String CF_COMP_UPGRADE_VALIDATE_FAILED =
          "CF_COMP_UPGRADE_VALIDATE_FAILED";
    /**
     * Warning message - unrecognized component type.
     */
    String CF_COMP_UNRECOGNIZED_TYPE =
          "CF_COMP_UNRECOGNIZED_TYPE";

    // ServiceUnitManager-related messages

    /**
     * Error message - failed to register deployer MBean.
     */
    String CF_DEPLOYER_MBEAN_REGISTRATION_FAILED =
          "CF_DEPLOYER_MBEAN_REGISTRATION_FAILED";
    /**
     * Error message - failed to unregister deployer MBean.
     */
    String CF_DEPLOYER_MBEAN_UNREGISTRATION_FAILED =
          "CF_DEPLOYER_MBEAN_UNREGISTRATION_FAILED";

    // lifecycle messages

    /**
     * Error message - failed to register life cycle MBean during restart.
     */
    String CF_LIFECYCLE_MBEAN_NOT_REGISTERED =
          "CF_LIFECYCLE_MBEAN_NOT_REGISTERED";
    /**
     * Error message - failed to register life cycle MBean.
     */
    String CF_LIFECYCLE_MBEAN_REGISTRATION_FAILED =
          "CF_LIFECYCLE_MBEAN_REGISTRATION_FAILED";
    /**
     * Error message - failed to unregister life cycle MBean.
     */
    String CF_LIFECYCLE_MBEAN_UNREGISTRATION_FAILED =
          "CF_LIFECYCLE_MBEAN_UNREGISTRATION_FAILED";
    /**
     * Error message - LifeCycle shutdown() failed.
     */
    String CF_LIFECYCLE_SHUTDOWN_EXCEPTION =
          "CF_LIFECYCLE_SHUTDOWN_EXCEPTION";
    /**
     * Error message - LifeCycle shutdown() failed.
     */
    String CF_LIFECYCLE_SHUTDOWN_FAILED =
          "CF_LIFECYCLE_SHUTDOWN_FAILED";
    /**
     * Error message - LifeCycle start() failed.
     */
    String CF_LIFECYCLE_START_EXCEPTION =
          "CF_LIFECYCLE_START_EXCEPTION";
    /**
     * Error message - LifeCycle start() failed.
     */
    String CF_LIFECYCLE_START_FAILED =
          "CF_LIFECYCLE_START_FAILED";
    /**
     * Error message - LifeCycle stop() failed.
     */
    String CF_LIFECYCLE_STOP_EXCEPTION =
          "CF_LIFECYCLE_STOP_EXCEPTION";
    /**
     * Error message - LifeCycle stop() failed.
     */
    String CF_LIFECYCLE_STOP_FAILED =
          "CF_LIFECYCLE_STOP_FAILED";

    // MBean-related messages

    /**
     * Warning message - logger MBean registration failed.
     */
    String CF_LOGGER_MBEAN_REGISTRATION_FAILED =
          "CF_LOGGER_MBEAN_REGISTRATION_FAILED";

    /**
     * Warning message - logger MBean unregistration failed.
     */
    String CF_LOGGER_MBEAN_UNREGISTRATION_FAILED =
          "CF_LOGGER_MBEAN_UNREGISTRATION_FAILED";
    /**
     * Error message - MBean creation failed.
     */
    String CF_MBEAN_CREATION_FAILED =
          "CF_MBEAN_CREATION_FAILED";
    /**
     * Error message - MBean registration failed.
     */
    String CF_MBEAN_REGISTRATION_FAILED =
          "CF_MBEAN_REGISTRATION_FAILED";
    /**
     * Warning message - statistics MBean registration failed.
     */
    String CF_STATISTICS_MBEAN_REGISTRATION_FAILED =
          "CF_STATISTICS_MBEAN_REGISTRATION_FAILED";
    /**
     * Warning message - statistics MBean unregistration failed.
     */
    String CF_STATISTICS_MBEAN_UNREGISTRATION_FAILED =
          "CF_STATISTICS_MBEAN_UNREGISTRATION_FAILED";
    
    /**
     * Warning message - configuration MBean registration failed.
     */
    String CF_CONFIGURATION_MBEAN_REGISTRATION_FAILED =
          "CF_CONFIGURATION_MBEAN_REGISTRATION_FAILED";
    /**
     * Warning message - configuration MBean unregistration failed.
     */
    String CF_CONFIGURATION_MBEAN_UNREGISTRATION_FAILED =
          "CF_CONFIGURATION_MBEAN_UNREGISTRATION_FAILED";
    
    // shared library related messages

    /**
     * Error message - shared library already registered.
     */
    String CF_SL_ALREADY_REGISTERED =
          "CF_SL_ALREADY_REGISTERED";
    /**
     * Error message - shared classloader creation failed.
     */
    String CF_SL_CLASSLOADER_CREATION_FAILED =
          "CF_SL_CLASSLOADER_CREATION_FAILED";
    /**
     * Error message - installation failed.
     */
    String CF_SL_INSTALL_FAILED =
          "CF_SL_INSTALL_FAILED";
    /**
     * Info message - installation succeeded.
     */
    String CF_SL_INSTALL_SUCCESSFUL =
          "CF_SL_INSTALL_SUCCESSFUL";
    /**
     * Error message - namespace referenced by component.
     */
    String CF_SL_REFERENCED_BY =
          "CF_SL_REFERENCED_BY";
    /**
     * Error message - namespace referenced by components.
     */
    String CF_SL_REFERENCED_BY_LIST =
          "CF_SL_REFERENCED_BY_LIST";
    /**
     * Info message - uninstall could not remove class loader.
     */
    String CF_SL_UNINSTALL_CLASSLOADER =
          "CF_SL_UNINSTALL_CLASSLOADER";
    /**
     * Error message - uninstall failed, dependent components exist.
     */
    String CF_SL_UNINSTALL_FAILED_DEPENDENTS =
          "CF_SL_UNINSTALL_FAILED_DEPENDENTS";
    /**
     * Error message - uninstall failed, no namespace.
     */
    String CF_SL_UNINSTALL_NONEXISTENT =
          "CF_SL_UNINSTALL_NONEXISTENT";
    /**
     * Error message - uninstall succeeded, dependent components exist.
     */
    String CF_SL_UNINSTALL_OK_DEPENDENTS =
          "CF_SL_UNINSTALL_OK_DEPENDENTS";
    /**
     * Info message - uninstall successful.
     */
    String CF_SL_UNINSTALL_SUCCESSFUL =
          "CF_SL_UNINSTALL_SUCCESSFUL";

    //
    // Message keys for ClassLoaderFactory and DelegatingClassLoader
    //

    /**
     * Error message - could not create bootstrap classloader.
     */
    String CL_BOOTSTRAP_CREATE_FAILED =
          "CL_BOOTSTRAP_CREATE_FAILED";
    /**
     * Error message - could not find the component classloader.
     */
    String CL_COMPONENT_CLASSLOADER_NOT_FOUND =
          "CL_COMPONENT_CLASSLOADER_NOT_FOUND";
    /**
     * Error message - could not create component classloader.
     */
    String CL_COMPONENT_CREATE_FAILED =
          "CL_COMPONENT_CREATE_FAILED";
    /**
     * Error message - could not create component classloader.
     */
    String CL_COMPONENT_EXTENSION_ERROR =
          "CL_COMPONENT_EXTENSION_ERROR";
    /**
     * Error message - could not find the shared class.
     */
    String CL_SHARED_CLASS_NOT_FOUND =
          "CL_SHARED_CLASS_NOT_FOUND";
    /**
     * Error message - could not find the shared classloader.
     */
    String CL_SHARED_CLASSLOADER_NOT_FOUND =
          "CL_SHARED_CLASSLOADER_NOT_FOUND";
    /**
     * Error message - could not create the shared classloader.
     */
    String CL_SHARED_CREATE_FAILED =
          "CL_SHARED_CREATE_FAILED";
    /**
     * Error Message - class was not loaded with any class loader.
     */
    String CU_CL_LOAD_CLASS_FAILURE =
          "CU_CL_LOAD_CLASS_FAILURE";
    /**
     * Error Message - I/O error creating class loader.
     */
    String CU_CL_CREATE_IO_ERROR =
          "CU_CL_CREATE_IO_ERROR";
    /**
     * Error Message - security exception trying to load a class.
     */
    String CU_CL_LOAD_CLASS_SECURITY_EXCEPTION =
          "CU_CL_LOAD_CLASS_SECURITY_EXCEPTION";

    //
    // Message keys for Component
    //

    /**
     * Bootstrap load failed.
     */
    String COMP_BOOTSTRAP_LOAD_FAILED =
          "COMP_BOOTSTRAP_LOAD_FAILED";
    /**
     * Component is busy.
     */
    String COMP_BUSY =
          "COMP_BUSY";
    /**
     * Class not found.
     */
    String COMP_CLASS_NOT_FOUND =
          "COMP_CLASS_NOT_FOUND";
    /**
     * Class not valid.
     */
    String COMP_CLASS_NOT_VALID =
          "COMP_CLASS_NOT_VALID";
    /**
     * Exception creating instance.
     */
    String COMP_EXCEPTION =
          "COMP_EXCEPTION";
    /**
     * Component returned null value for instance.
     */
    String COMP_INSTANCE_NOT_PROVIDED =
          "COMP_INSTANCE_NOT_PROVIDED";
    /**
     * Invalid state change.
     */
    String COMP_INVALID_STATE_CHANGE =
          "COMP_INVALID_STATE_CHANGE";
    /**
     * LifeCycle load failed.
     */
    String COMP_LIFECYCLE_LOAD_FAILED =
          "COMP_LIFECYCLE_LOAD_FAILED";
    /**
     * Component logger not found.
     */
    String COMP_LOGGER_NOT_FOUND =
          "COMP_LOGGER_NOT_FOUND";
    /**
     * Component logger settings file close failed.
     */
    String COMP_LOGGER_SETTINGS_CLOSE_FAILED =
          "COMP_LOGGER_SETTINGS_CLOSE_FAILED";
    /**
     * Component logger settings directory create failed.
     */
    String COMP_LOGGER_SETTINGS_CREATE_FAILED =
          "COMP_LOGGER_SETTINGS_CREATE_FAILED";
    /**
     * Component logger settings file read failed.
     */
    String COMP_LOGGER_SETTINGS_LOAD_FAILED =
          "COMP_LOGGER_SETTINGS_LOAD_FAILED";
    /**
     * Component logger settings file create failed.
     */
    String COMP_LOGGER_SETTINGS_SAVE_FAILED =
          "COMP_LOGGER_SETTINGS_SAVE_FAILED";
    /**
     * Component state installed.
     */
    String COMP_STATE_SHUTDOWN =
          "COMP_STATE_SHUTDOWN";
    /**
     * Component state loaded.
     */
    String COMP_STATE_LOADED =
          "COMP_STATE_LOADED";
    /**
     * Component state started.
     */
    String COMP_STATE_STARTED =
          "COMP_STATE_STARTED";
    /**
     * Component state stopped.
     */
    String COMP_STATE_STOPPED =
          "COMP_STATE_STOPPED";

    //
    // Message keys for ComponentContext
    //

    /**
     * Illegal getLogger request with different resource bundle.
     */
    String CC_LOGGER_DIFFERENT_RESOURCE =
          "CC_LOGGER_DIFFERENT_RESOURCE";

    /**
     * Missing resource bundle.
     */
    String CC_LOGGER_MISSING_RESOURCE =
          "CC_LOGGER_MISSING_RESOURCE";

    /**
     * Delivery channel not available.
     */
    String CC_NO_DC_AVAILABLE =
          "CC_NO_DC_AVAILABLE";

    /**
     * Null suffix not allowed.
     */
    String CC_LOGGER_NULL_SUFFIX =
          "CC_LOGGER_NULL_SUFFIX";

    //
    // Message keys for ComponentRegistry
    //

    /**
     * Exception message used when a component cannot be registered because
     * its ID is already in use.
     */
    String CR_COMPONENT_ALREADY_REGISTERED =
          "CR_COMPONENT_ALREADY_REGISTERED";
    /**
     * Exception message used when a component cannot be unregistered because
     * the name is not registered.
     */
    String CR_COMPONENT_NOT_REGISTERED =
          "CR_COMPONENT_NOT_REGISTERED";
    /**
     * Exception message used when a permanent registration fails.
     */
    String CR_PERMANENT_REGISTRATION_NOT_FOUND =
          "CR_PERMANENT_REGISTRATION_NOT_FOUND";
   /**
    * Warning message issued when an unrecognized entry is found.
    */
    String CR_RELOAD_BAD_COMPONENT_TYPE =
          "CR_RELOAD_BAD_COMPONENT_TYPE";
    /**
     * Exception message used when a shared library cannot be registered because
     * its ID is already in use.
     */
    String CR_SL_ALREADY_REGISTERED =
          "CR_SL_ALREADY_REGISTERED";
   /**
    * Exception message used when a shared library cannot be unregistered
    * because the name is not registered.
    */
    String CR_SL_NOT_REGISTERED =
          "CR_SL_NOT_REGISTERED";

    //
    // Message keys for Deployer
    //

    /**
     * Error - Service Unit already deployed.
     */
    String DMB_SU_ALREADY_DEPLOYED =
          "DMB_SU_ALREADY_DEPLOYED";
    /**
     * Error - Service Unit cannot be initialized, it is started.
     */
    String DMB_SU_CANNOT_INIT_STARTED =
          "DMB_SU_CANNOT_INIT_STARTED";
    /**
     * Error - Service Unit cannot be shut down, it is not stopped.
     */
    String DMB_SU_CANNOT_SHUT_DOWN_NOT_STOPPED =
          "DMB_SU_CANNOT_SHUT_DOWN_NOT_STOPPED";
    /**
     * Error - Service Unit cannot be started, it is not initialized.
     */
    String DMB_SU_CANNOT_START_NOT_INITIALIZED =
          "DMB_SU_CANNOT_START_NOT_INITIALIZED";
    /**
     * Error - Service Unit cannot be stopped, it is not started.
     */
    String DMB_SU_CANNOT_STOP_SHUT_DOWN =
          "DMB_SU_CANNOT_STOP_SHUT_DOWN";
    /**
     * Error - Service Unit cannot be undeployed, not shut down.
     */
    String DMB_SU_CANNOT_UNDEPLOY_NOT_SHUT_DOWN =
          "DMB_SU_CANNOT_UNDEPLOY_NOT_SHUT_DOWN";
    /**
     * Error - Service Unit operation cannot be performed, component is not
     * started.
     */
    String DMB_SU_COMP_NOT_STARTED =
          "DMB_SU_COMP_NOT_STARTED";
    /**
     * Error - Service Unit not found.
     */
    String DMB_SU_NOT_FOUND =
          "DMB_SU_NOT_FOUND";
    /**
     * Error - Service Unit operation threw exception.
     */
    String DMB_SU_OPERATION_EXCEPTION =
          "DMB_SU_OPERATION_EXCEPTION";
    /**
     * Error - Service Unit operation threw exception with no message text.
     */
    String DMB_SU_OPERATION_EXCEPTION_NO_MSG =
          "DMB_SU_OPERATION_EXCEPTION_NO_MSG";
    /**
     * Error - Service Unit operation was interrupted.
     */
    String DMB_SU_OPERATION_INTERRUPTED =
          "DMB_SU_OPERATION_INTERRUPTED";
    /**
     * Error - Service Unit operation was interrupted.
     */
    String DMB_SU_OPERATION_NOT_AVAILABLE =
          "DMB_SU_OPERATION_NOT_AVAILABLE";
    /**
     * Error - Service Unit operation timed out.
     */
    String DMB_SU_OPERATION_TIMEOUT =
          "DMB_SU_OPERATION_TIMEOUT";
    /**
     * Error - Service Unit state could not be persisted.
     */
    String DMB_SU_STATE_PERSIST_FAILURE =
          "DMB_SU_STATE_PERSIST_FAILURE";

    //
    // Message keys for EnvironmentContext
    //

    /**
     * Logging - Initial properties.
     */
    String EC_INITIAL_PROPERTIES_LIST =
          "EC_INITIAL_PROPERTIES_LIST";
    /**
     * Error - Invalid log level.
     */
    String EC_INVALID_LOG_LEVEL =
          "EC_INVALID_LOG_LEVEL";
    /**
     * Error - MBean creation failed.
     */
    String EC_MBEAN_CREATION_FAILED =
          "EC_MBEAN_CREATION_FAILED";
    /**
     * Error - MBean name creation failed.
     */
    String EC_MBEAN_NAME_CREATION_FAILED =
          "EC_MBEAN_NAME_CREATION_FAILED";
    /**
     * Error - MBean registration failed.
     */
    String EC_MBEAN_REGISTRATION_FAILED =
          "EC_MBEAN_REGISTRATION_FAILED";
    /**
     * Warning - MBean unregistration failed.
     */
    String EC_MBEAN_UNREGISTRATION_FAILED =
          "EC_MBEAN_UNREGISTRATION_FAILED";
    /**
     * Warning - Cannot create logger MBean.
     */
    String EC_NO_LOGGER_MBEAN =
          "EC_NO_LOGGER_MBEAN";
    /**
     * Warning - Cannot create notification MBean.
     */
    String EC_NO_NOTIFICATION_MBEAN =
          "EC_NO_NOTIFICATION_MBEAN";
    /**
     * Warning - Cannot create statistics MBean.
     */
    String EC_NO_STATISTICS_MBEAN =
          "EC_NO_STATISTICS_MBEAN";
    /**
     * Logging - StringTranslator class loader not found.
     */
    String EC_STRING_TRANSLATOR_CLASSLOADER_NOT_FOUND =
          "EC_STRING_TRANSLATOR_CLASSLOADER_NOT_FOUND";
    /**
     * Logging - TransactionManager not found.
     */
    String EC_TRANSACTION_MANAGER_NOT_FOUND =
          "EC_TRANSACTION_MANAGER_NOT_FOUND";
    /**
     * Registry initialization failed.
     */
    String EC_JBI_REGISTRY_INIT_FAILED =
        "EC_JBI_REGISTRY_INIT_FAILED";

    //
    // Message keys for FrameworkNotification
    //

    /**
     * Error - failed to create user data for notification.
     */
    String FN_USER_DATA_CREATE_FAILED =
          "FN_USER_DATA_CREATE_FAILED";
    /**
     * Error - exception sending notification.
     */
    String FN_SEND_NOTIFICATION_FAILED =
          "FN_SEND_NOTIFICATION_FAILED";

    //
    // Message keys for ServiceUnit
    //

    /**
     * Error - already exists.
     */
    String SU_ALREADY_EXISTS =
          "SU_ALREADY_EXISTS";
    /**
     * Error - invalid state change.
     */
    String SU_INVALID_STATE_CHANGE =
          "SU_INVALID_STATE_CHANGE";
    /**
     * Error - not found.
     */
    String SU_NOT_FOUND =
          "SU_NOT_FOUND";
    /**
     * Service Unit state deployed (never initialized).
     */
    String SU_STATE_DEPLOYED =
          "SU_STATE_DEPLOYED";
    /**
     * Service Unit state shutdown.
     */
    String SU_STATE_SHUTDOWN =
          "SU_STATE_SHUTDOWN";
    /**
     * Service Unit state started.
     */
    String SU_STATE_STARTED =
          "SU_STATE_STARTED";
    /**
     * Service Unit state stopped.
     */
    String SU_STATE_STOPPED =
          "SU_STATE_STOPPED";

    //
    // Message keys for ServiceUnitFramework
    //

    /**
     * Info - Service Unit deployed.
     */
    String SUF_DEPLOYED =
          "SUF_DEPLOYED";
    /**
     * Info - Service Unit started.
     */
    String SUF_STARTED =
          "SUF_STARTED";
    /**
     * Info - Service Unit stopped.
     */
    String SUF_STOPPED =
          "SUF_STOPPED";
    /**
     * Info - Service Unit shut down.
     */
    String SUF_SHUT_DOWN =
          "SUF_SHUT_DOWN";
    /**
     * Info - Service Unit undeployed.
     */
    String SUF_UNDEPLOYED =
          "SUF_UNDEPLOYED";

    //
    // Message keys for ServiceUnitManager
    //

    /**
     * Error - error in ServiceUnitManager.
     */
    String SUM_EXCEPTION =
          "SUM_EXCEPTION";
    /**
     * Error - init() timeout limit reached.
     */
    String SUM_INIT_TIMEOUT_LIMIT =
          "SUM_INIT_TIMEOUT_LIMIT";
    /**
     * Error - stack trace print.
     */
    String SUM_STACK_TRACE =
          "SUM_STACK_TRACE";
    /**
     * Error - start() timeout limit reached.
     */
    String SUM_START_TIMEOUT_LIMIT =
          "SUM_START_TIMEOUT_LIMIT";
    /**
     * Error - stop() timeout limit reached.
     */
    String SUM_STOP_TIMEOUT_LIMIT =
          "SUM_STOP_TIMEOUT_LIMIT";
    /**
     * Error - shutDown() timeout limit reached.
     */
    String SUM_SHUTDOWN_TIMEOUT_LIMIT =
          "SUM_SHUTDOWN_TIMEOUT_LIMIT";
    
    //
    // ComponentConfiguration messages
    //
    /** 
     * Error - Missing Component Configuration MBean.
     */
    String CCFG_MISSING_COMPONENT_CONFIG_MBEAN =
        "CCFG_MISSING_COMPONENT_CONFIG_MBEAN";
    
    /**  
     * Error - Could not persist attribute change.
     */
     String CCFG_PERSIST_ATTRIBUTE_FAILURE =
        "CCFG_PERSIST_ATTRIBUTE_FAILURE";
     
     /**
      * Error - Could not add an application variable in the registry.
      */
     String CCFG_FAILED_ADD_APP_VAR_REG =
        "CCFG_FAILED_ADD_APP_VAR_REG";

     /**
      * Error - Could not update an application variable in the registry.
      */     
     String CCFG_FAILED_UPDATE_APP_VAR_REG =
        "CCFG_FAILED_UPDATE_APP_VAR_REG";
     
     /**
      * Error - Could not deleted an application variable from the registry.
      */
     String CCFG_FAILED_DELETE_APP_VAR_REG =
        "CCFG_FAILED_DELETE_APP_VAR_REG";
     
     /**
      * Unsupported Operation.
      */
     String CCFG_UNSUPPORTED_OPERATION =
        "CCFG_UNSUPPORTED_OPERATION";
     
     /**
      * Error - Could not add an application configuration to the registry.
      */
     String CCFG_FAILED_ADD_APP_CFG_REG =
        "CCFG_FAILED_ADD_APP_CFG_REG";

     /**
      * Error - Could not update an application configuration in the registry.
      */     
     String CCFG_FAILED_UPDATE_APP_CFG_REG =
        "CCFG_FAILED_UPDATE_APP_CFG_REG";
     
     /**
      * Error - Could not deleted an application configuration from the registry.
      */
     String CCFG_FAILED_DELETE_APP_CFG_REG =
        "CCFG_FAILED_DELETE_APP_CFG_REG";
     
    //
    // XML Utility messages
    //
    /** 
     * Error - Missing required element.
     */
    String XML_MISSING_ELEMENT =
        "XML_MISSING_ELEMENT";
    /** 
     * Error - Extra elements found when not expected.
     */
    String XML_EXTRA_ELEMENTS =
        "XML_EXTRA_ELEMENTS";
    /** 
     * Error - Empty element found when not expected.
     */
    String XML_EMPTY_ELEMENT =
        "XML_EMPTY_ELEMENT";
    /** 
     * Error - Invalid element found.
     */
    String XML_INVALID_ELEMENT =
        "XML_INVALID_ELEMENT";
    /** 
     * Error - DOM exception while processing XML.
     */
    String XML_DOM_EXCEPTION =
        "XML_DOM_EXCEPTION";
}
