/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.management.ComponentInfo.Variable;
import com.sun.jbi.management.descriptor.ComponentDescriptor;
import com.sun.jbi.management.system.ManagementException;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.xml.RegistryDocument;
import com.sun.jbi.management.repository.Archive;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.util.ComponentConfigurationHelper;

import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import java.lang.reflect.Method;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.ObjectName;
import javax.management.AttributeNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.OpenDataException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

/**
 * The ComponentConfiguration is a dynamic facade MBean for component configuration.
 * This MBean provides a facade for managing component static, variable and
 * named configuration.
 *
 * This ComponentExtension MBean is registered for each installation of the component
 * on a target i.e it is target specific. 
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentConfiguration
    implements com.sun.jbi.management.ComponentConfiguration
{
   /** The component name */
    private String mComponentName;
    
    /** The context */
    private EnvironmentContext mEnvCtx;
    
    /** The MessageBuilder */
    private MessageBuilder mMsgBuilder;
    
    /** String Translator */
    private StringTranslator mTranslator;
    
    /** The Logger */
    private Logger mLog;
    
    /** "*****" */
    private static final String MASKED_FIELD_VALUE = "*****";
    
    /** PASSWORD Field */
    private static final String PASSWORD = "PASSWORD";
    
    /** Component Configuration Helper */
    private ComponentConfigurationHelper mCfgHlpr;

    public ComponentConfiguration(EnvironmentContext ctx, String compName)
    {   
        mComponentName = compName;
        mEnvCtx = ctx;
        mTranslator        = (StringTranslator) mEnvCtx.getStringTranslatorFor(this);
        mLog               = mEnvCtx.getLogger();
        mCfgHlpr           = new ComponentConfigurationHelper();
        try
        {
            mMsgBuilder        = new MessageBuilder(mTranslator);
        }
        catch ( com.sun.jbi.management.system.ManagementException mex )
        {
            // rare
            mLog.warning(mex.getMessage());
        }
    }
       
    /*----------------------------------------------------------------------------------*\
     *                   javax.management.DynamicMBean Methods                          *
    \*----------------------------------------------------------------------------------*/

    /**
     * Obtain the value of a specific attribute of the Dynamic MBean.
     *
     * @param attribute The name of the attribute to be retrieved
     * @return The value of the attribute retrieved.
     * @exception AttributeNotFoundException
     * @exception MBeanException  Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's getter.
     * @exception ReflectionException  Wraps a <CODE>java.lang.Exception</CODE>
     *            thrown while trying to invoke the getter.
     * @see #setAttribute
     */
    public Object getAttribute(String attribute) 
        throws AttributeNotFoundException, MBeanException, ReflectionException 
    {
        /**
         * This gets the attribute from the registry, converts the String to
         * the attribute type and then returns it. 
         * 
         * Note : Special case handling is required for Environment Variables, since
         *        those are TabularData. For Milestone 2 ignore getEnvironmentVariables
         *
         */
           
        if ( "ApplicationVariables".equals(attribute) ) 
        {
            return getApplicationVariables();
        }
        
        if ( "ApplicationConfigurations".equals(attribute) ) 
        {
            return getApplicationConfigurations();
        }
        
        AttributeList attribList = getAttributes(new String[]{attribute});
        
        if ( !attribList.isEmpty() )
        {
            return ( (Attribute) attribList.get(0)).getValue();
        }
        else
        {
            throw new AttributeNotFoundException(attribute);
        }
        
    }

    /**
     * Set the value of a specific attribute of the Dynamic MBean.
     * 
     * @param attribute The identification of the attribute to
     *        be set and  the value it is to be set to.
     * @exception AttributeNotFoundException
     * @exception InvalidAttributeValueException
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's setter.
     * @exception ReflectionException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown while trying to invoke the MBean's setter.
     * @see #getAttribute
     */
    public void setAttribute(Attribute attribute) 
        throws AttributeNotFoundException, InvalidAttributeValueException, 
            MBeanException, ReflectionException 
    {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        /**
         * This first invokes setAttribute on the original configuration MBean and
         * then updates the registry for the attributes set successfully.
         */
        try
        {
            mEnvCtx.getMBeanServer().setAttribute(configMBeanName, attribute);
        }
        catch ( javax.management.InstanceNotFoundException iex )
        {
            throw new MBeanException(iex);
        }
        
        // Update the registry
        persistAttribute(attribute);
        
    }

    /**
     * Allows an action to be invoked on the Dynamic MBean.
     * 
     * @param actionName The name of the action to be invoked.
     * @param params An array containing the parameters to be set when the 
     *        action is invoked.
     * @param signature An array containing the signature of the action. The 
     *        class objects will be loaded through the same class loader as the
     *        one used for loading the MBean on which the action is invoked.
     * @return The object returned by the action, which represents the result of
     *         invoking the action on the MBean specified.
     * @exception MBeanException  Wraps a <CODE>java.lang.Exception</CODE> thrown
     *            by the MBean's invoked method.
     * @exception ReflectionException  Wraps a <CODE>java.lang.Exception</CODE> 
     *            thrown while trying to invoke the method
     */
    public Object invoke(String actionName, Object[] params, String[] signature) 
        throws MBeanException, ReflectionException 
    { 
        
        if ( actionName.equals("addApplicationVariable") )
        {
            return addApplicationVariable((String) params[0],
                (CompositeData) params[1]);
        }
        if ( actionName.equals("setApplicationVariable") )
        {
            return setApplicationVariable((String) params[0],
                (CompositeData) params[1]);
        }

        if ( actionName.equals("deleteApplicationVariable") )
        {
            return deleteApplicationVariable((String) params[0]);
        }

        if ( actionName.equals("getApplicationVariables") )
        {
            return getApplicationVariables();
        }

        if ( actionName.equals("getApplicationConfigurations") )
        {
            return getApplicationConfigurations();
        }

        if ( actionName.equals("addApplicationConfiguration") )
        {
            if ( params[1] instanceof Properties )
            {
                return addApplicationConfiguration((String) params[0],
                    (Properties) params[1]);
            }
            else
            {
                return addApplicationConfiguration((String) params[0],
                    (CompositeData) params[1]);
            }
        }

        if ( actionName.equals("setApplicationConfiguration") )
        {
            if ( params[1] instanceof Properties )
            {
                return setApplicationConfiguration((String) params[0],
                    (Properties) params[1]);
            }
            else
            {
                return setApplicationConfiguration((String) params[0],
                    (CompositeData) params[1]);
            }
        }

        if ( actionName.equals("deleteApplicationConfiguration") )
        {
            return deleteApplicationConfiguration((String) params[0]);
        }

        if ( actionName.equals("queryApplicationConfigurationType") )
        {
            return queryApplicationConfigurationType();
        }
        
        if ( actionName.equals("retrieveConfigurationDisplaySchema"))
        {
            return retrieveConfigurationDisplaySchema();
        }

        if ( actionName.equals("retrieveConfigurationDisplayData"))
        {
            return retrieveConfigurationDisplayData();
        }
        
        if ( actionName.equals("isAppVarsSupported"))
        {
            return isAppVarsSupported();
        }

        if ( actionName.equals("isAppConfigSupported"))
        {
            return isAppConfigSupported();
        }

        if ( actionName.equals("isComponentConfigSupported"))
        {
            return isComponentConfigSupported();
        }        
        
        throw new UnsupportedOperationException(actionName);   
    }

    /**
     * Sets the values of several attributes of the Dynamic MBean.
     * 
     * @param attributes A list of attributes: The identification of the
     *        attributes to be set and  the values they are to be set to.
     * @return The list of attributes that were set, with their new values.
     * @see #getAttributes
     */
    public AttributeList setAttributes(AttributeList attributes) 
    {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        /**
         * This first invokes setAttribute on the original configuration MBean and
         * then updates the registry for the attributes set successfully.
         */
        AttributeList attribs = new AttributeList();
        try
        {
            attribs = (AttributeList) mEnvCtx.getMBeanServer().setAttributes(configMBeanName,
                attributes);
        }
        catch ( Exception iex )
        {
            throw new RuntimeException(iex);
        }
        
        // Parse the attribute list and update the registry with the set attributes
        persistAttributes(attribs);
        
        return attribs;
    }

    /**
     * Provides the exposed attributes and actions of the Dynamic MBean using 
     * an MBeanInfo object.
     * 
     * @return An instance of <CODE>MBeanInfo</CODE> allowing all attributes 
     *         and actions exposed by this Dynamic MBean to be retrieved.
     */
    public MBeanInfo getMBeanInfo() 
    {
        // Get the MbeanInfo from the actual component configuration MBean and
        // augment it with the facade configuration operations
        MBeanInfo mbeanInfo = getComponentMBeanInfo();
        MBeanInfo augInfo = mbeanInfo;
        
        try
        {
            Method[] methods = Class.forName("com.sun.jbi.management.ComponentConfiguration").getDeclaredMethods();
            MBeanOperationInfo[] opInfos = new MBeanOperationInfo[methods.length];
            int i = 0;
            for ( Method mtd : methods )
            {
                opInfos[i++] = new MBeanOperationInfo(mtd.getName(), mtd);
            }
            
            
            augInfo = new MBeanInfo(
                mbeanInfo.getClassName(),
                mbeanInfo.getDescription(),
                mbeanInfo.getAttributes(),
                mbeanInfo.getConstructors(),
                merge(mbeanInfo.getOperations(), opInfos),
                mbeanInfo.getNotifications());
        }
        catch (Exception ex)
        {
            // log exception
            mLog.log(java.util.logging.Level.FINEST, ex.getMessage(), ex);
        }
        

        
        return augInfo;
    }

    /**
     * Get the values of several attributes of the Dynamic MBean.
     * 
     * @param attributes A list of the attributes to be retrieved.
     * @return The list of attributes retrieved.
     * @see #setAttributes
     */
    public AttributeList getAttributes(String[] attributes) 
    {
        /**
         * If the framework is ready ( i.e. JAXB initialized ) get the attribute from
         * the JAXB model
         *
         * If framework not ready then get the attribute from the registry using DOM/XPath
         *
         * If the attribute is not defined in the registry get the attribute values from
         * the actual MBean and persist it.
         */

        /** Issue 780 Fix
        Properties props = retrieveAttributes();
        
        if ( !props.isEmpty() )
        { 
            AttributeList list = new AttributeList(props.size());

            for (String key : attributes)
            {
                MBeanAttributeInfo attribInfo = getAttributeInfo(key);
                String strValue = props.getProperty(key);
                if (strValue != null )
                {
                    if ( attribInfo != null)
                    {
                        try
                        {
                            Object value = (Object) com.sun.jbi.management.util.StringHelper.
                                convertStringToType( attribInfo.getType(), strValue );
                            list.add(new Attribute( key, value));
                        }
                        catch (Exception ex)
                        {
                            mLog.warning(MessageHelper.getMsgString(ex));
                            list.add( new Attribute(key, strValue));
                        }
                        
                    }
                    else
                    {
                        list.add( new Attribute(key, strValue));
                    }
                }
            }
            return list;
        }
        */
        // -- Call getAttributes on the actual MBean
        ObjectName configMBeanName = getComponentConfigurationMBeanName();

        if ( configMBeanName != null )
        {
            if (mEnvCtx.getMBeanServer().isRegistered(configMBeanName))
            {
                try
                {
                    AttributeList attribList =  mEnvCtx.getMBeanServer().getAttributes(configMBeanName,
                        attributes); 

                    /** Issue 780 fix
                    // persist all the attributes
                    persistMBeanAttributes(configMBeanName); */
                    return attribList;
                }
                catch ( javax.management.JMException iex)
                {
                    mLog.warning(iex.getMessage());
                    // -- should never occur
                }
                
            }
        }

        // Instance config MBean not available and attributes not persisted
        return  new AttributeList();
    }
    
    /*---------------------------------------------------------------------------------*\
     *          Operations for Static Configuration Management                         *
    \*---------------------------------------------------------------------------------*/
    /**
     * Set a single configuration attribute on a target.
     *
     * @param attrib - the configuration attribute to be set.
     * @return a management message string with the status of the operation.
     */
    public String setConfigurationAttribute(Attribute attrib)
        throws javax.jbi.JBIException
    {
        throw new UnsupportedOperationException();
    }
    
    /**
     *
     */
    public String setConfigurationAttributes(AttributeList attribList)
            throws javax.jbi.JBIException
    {
        throw new UnsupportedOperationException();
    }
    
    /**
     * Detect the components support for component configuration. This method
     * returns true if the component has a configuration MBean with
     * configuration attributes.
     *
     * @return true if the components configuration MBean has configuration
     *         attributes.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
    public boolean isComponentConfigSupported()  
        throws MBeanException
    {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        if ( configMBeanName == null )
        {
            // Component does not register a component configuration MBean
           return false;
        }
        
        MBeanInfo mbnInfo = getComponentMBeanInfo();
        
        MBeanAttributeInfo[] attrInfos = mbnInfo.getAttributes();
        
        int realAttrCount = 0;
        
        for ( MBeanAttributeInfo attrInfo : attrInfos )
        {
            if ( !"ApplicationVariables".equals(attrInfo.getName()) &&
                 !"ApplicationConfigurations".equals(attrInfo.getName())  &&
                 attrInfo.isReadable() && attrInfo.isWritable() )
            {
                realAttrCount++;
            }
        }
        
        return ( realAttrCount > 0 );
        
    }
    
    /*---------------------------------------------------------------------------------*\
     *          Operations for Application Variables Management                        *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application variables. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application variable management.
     *
     * @return true if the components configuration MBean implements all the
     *         operations for application variables.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
    public boolean isAppVarsSupported()  
       throws MBeanException            
    {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        if ( configMBeanName == null )
        {
            // Component does not register a component configuration MBean
           return false;
        }
        
        MBeanInfo mbnInfo = getComponentMBeanInfo();
        
        MBeanOperationInfo[] opInfos = mbnInfo.getOperations();
         
        int numOpsSupported = 0;
        for ( MBeanOperationInfo opInfo : opInfos )
        {
            if ( opInfo.getName().equals("addApplicationVariable"))
            {
                MBeanParameterInfo[] params = opInfo.getSignature();
                
                if ( params.length == 2 )
                {
                    if ( params[0].getType().equals("java.lang.String") &&
                         params[1].getType().equals("javax.management.openmbean.CompositeData") )
                    {
                        numOpsSupported++;
                    }
                }
            }
            else if ( opInfo.getName().equals("setApplicationVariable"))
            {
                MBeanParameterInfo[] params = opInfo.getSignature();
                
                if ( params.length == 2 )
                {
                    if ( params[0].getType().equals("java.lang.String") &&
                         params[1].getType().equals("javax.management.openmbean.CompositeData") )
                    {
                        numOpsSupported++;
                    }
                }
            }
            else if ( opInfo.getName().equals("deleteApplicationVariable"))
            {
                MBeanParameterInfo[] params = opInfo.getSignature();
                
                if ( params.length == 1 )
                {
                    if ( params[0].getType().equals("java.lang.String") )
                    {
                        numOpsSupported++;
                    }
                }
            }
        }
        
        boolean isAppVarsAttribSupported = false;
        MBeanAttributeInfo[] attrInfos = mbnInfo.getAttributes();

        for ( MBeanAttributeInfo attrInfo : attrInfos )
        {
            // -- Check if "ApplicationConfigurations" attribute is present
            if ( attrInfo.getName().equals("ApplicationVariables"))
            {
                isAppVarsAttribSupported = true;
            }
        } 
         
        return ( numOpsSupported == 3 && isAppVarsAttribSupported);     
     }
     
    /**
     * This operation adds a new application variable. If a variable already exists with 
     * the same name as that specified then the operation fails.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste
     * @throws MBeanException if an error occurs in adding the application variables to the 
     *         component. 
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String addApplicationVariable(String name, CompositeData appVar) 
        throws MBeanException
    {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        String response = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        /** Issue 780 Fix
        // If the application variables element in the registry is empty
        // get the application variables from the actual MBean and persist them.
        persistApplicationVariables(configMBeanName);
        */
        
        /**
         * This first invokes addApplicationVariable on the original configuration MBean and
         * then updates the registry for the attributes set successfully.
         */
        try
        {
            Object[] params = new Object[]{name, appVar};
            String[] sign = new String[] {
                    "java.lang.String", "javax.management.openmbean.CompositeData"};
            operationSupportedCheck(configMBeanName, "addApplicationVariable", sign );

            mEnvCtx.getMBeanServer().invoke(configMBeanName, "addApplicationVariable",
                params, sign);

            // Update the registry
            addApplicationVariable(appVar);

            response =  mMsgBuilder.buildFrameworkMessage("addApplicationVariable",
                MessageBuilder.TaskResult.SUCCESS);

        }
        catch ( Exception iex )
        {
            try
            {
                String errMsg = 
                    mMsgBuilder.buildExceptionMessage("addApplicationVariable",
                        iex);
                throw new MBeanException( new javax.jbi.JBIException(errMsg));
            }
            catch ( ManagementException mex )
            {
                mLog.warning(MessageHelper.getMsgString(mex));
                throw new MBeanException(new Exception(mex.getMessage()));
            }
        }        
        return response;
    }
     
    /**
     * This operation sets an application variable. If a variable does not exist with 
     * the same name, its an error.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste to be updated.
     * @throws MBeanException if one or more application variables cannot be deleted
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String setApplicationVariable(String name, CompositeData appVar) 
        throws MBeanException
    {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        String response = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        /** Issue 780 Fix
        // If the application variables element in the registry is empty
        // get the application variables from the actual MBean and persist them.
        persistApplicationVariables(configMBeanName); */
        
        /**
         * This first invokes setApplicationVariable on the original configuration MBean and
         * then updates the registry for the attributes set successfully.
         */
        try
        {
            Object[] params = new Object[]{name, appVar};
            String[] sign = new String[] {
                    "java.lang.String", "javax.management.openmbean.CompositeData"};
            operationSupportedCheck(configMBeanName, "setApplicationVariable", sign );

            mEnvCtx.getMBeanServer().invoke(configMBeanName, "setApplicationVariable", 
                params, sign);

            // Update the registry
            updateApplicationVariable(appVar);

            response =  mMsgBuilder.buildFrameworkMessage("setApplicationVariable",
                MessageBuilder.TaskResult.SUCCESS);

        }
        catch ( Exception iex )
        {
            try
            {
                String errMsg = mMsgBuilder.buildExceptionMessage("setApplicationVariable",
                    iex);
                throw new MBeanException( new javax.jbi.JBIException(errMsg));
            }
            catch ( ManagementException mex )
            {
                mLog.warning(MessageHelper.getMsgString(mex));
                throw new MBeanException(new Exception(mex.getMessage()));
            }
        }        
        return response;

    }
     
    /**
     * This operation deletes an application variable, if a variable with the specified name does
     * not exist, it's an error.
     *
     * @param name - name of the application variable
     * @throws MBeanException on errors.
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String deleteApplicationVariable(String name) throws MBeanException
    {
        
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        String response = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        /** Issue 780 Fix
        // If the application variables element in the registry is empty
        // get the application variables from the actual MBean and persist them.
        persistApplicationVariables(configMBeanName); */
        
        /**
         * This first invokes deleteApplicationVariable on the original configuration 
         * MBean and then updates the registry for the attributes set successfully.
         */
        try
        {
            Object[] params = new Object[]{name};
            String[] sign = new String[] {
                    "java.lang.String"};
            operationSupportedCheck(configMBeanName, "deleteApplicationVariable", sign );

            mEnvCtx.getMBeanServer().invoke(configMBeanName, "deleteApplicationVariable", 
                params, sign);

            // Update the registry
            removeApplicationVariable(name);

            response =  mMsgBuilder.buildFrameworkMessage("deleteApplicationVariable",
                MessageBuilder.TaskResult.SUCCESS);

        }
        catch ( Exception iex )
        {
            try
            {
                String errMsg = mMsgBuilder.buildExceptionMessage("deleteApplicationVariable",
                    iex);
                
                throw new MBeanException( new javax.jbi.JBIException(errMsg));
            }
            catch ( ManagementException mex )
            {
                mLog.warning(MessageHelper.getMsgString(mex));
                throw new MBeanException(new Exception(mex.getMessage()));
            }
        }        
        return response;
    }
     
     /**
      * Get the Application Variable set for a component.
      *
      * @return  a TabularData which has all the applicationvariables set on the component. 
      */
     public TabularData getApplicationVariables()
     {
         
        /**
         * If framework not ready then get the application variable from the registry 
         * using DOM.
         *
         * If the framework is ready ( i.e. JAXB initialized ) get the application variable 
         *  from the JAXB model
         *
         * If the registry component configuration / application variables is empty get the 
         * actual values from the MBean.
         */
        /** Issue 780 Fix 
        TabularData appVars = retrieveApplicationVariables();*/
        TabularData appVars = null;
        
        /** Issue 780 fix if ( appVars.isEmpty() )
        {   */
            // -- Call getAttributes on the actual MBean, the application variables might not
            //    have been persisted
            
            ObjectName configMBeanName = getComponentConfigurationMBeanName();

            if ( configMBeanName != null )
            {
                if (mEnvCtx.getMBeanServer().isRegistered(configMBeanName))
                {
                    try
                    {
                        appVars =  (TabularData) mEnvCtx.getMBeanServer().getAttribute(configMBeanName,
                            "ApplicationVariables"); 
                    }
                    catch ( javax.management.JMException iex)
                    {
                        mLog.warning(iex.toString());
                        // -- should never occur
                    }

                }
            }
            if ( appVars == null )
            {
                try
                {
                    appVars = createEmptyAppVarTable();
                }
                catch ( javax.management.openmbean.OpenDataException oex )
                {   
                    throw new RuntimeException(oex);
                }
            }
        /** } */
        return appVars;
     }

    /*---------------------------------------------------------------------------------*\
     *            Operations for Application Configuration Management                  *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application configuration. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application configuration management.
     *
     * @return true if the components configuration MBean implements all the
     *         operations for application configuration.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppConfigSupported()  
        throws MBeanException
     {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        if ( configMBeanName == null )
        {
            return false;
        }
         
        MBeanInfo mbnInfo = getComponentMBeanInfo();

        MBeanOperationInfo[] opInfos = mbnInfo.getOperations();

        int numOpsSupported = 0;
        for ( MBeanOperationInfo opInfo : opInfos )
        {
            if ( opInfo.getName().equals("addApplicationConfiguration"))
            {
                MBeanParameterInfo[] params = opInfo.getSignature();
                
                if ( params.length == 2 )
                {
                    if ( params[0].getType().equals("java.lang.String") &&
                         params[1].getType().equals("javax.management.openmbean.CompositeData") )
                    {
                        numOpsSupported++;
                    }
                }
            }
            else if ( opInfo.getName().equals("setApplicationConfiguration"))
            {
                MBeanParameterInfo[] params = opInfo.getSignature();
                
                if ( params.length == 2 )
                {
                    if ( params[0].getType().equals("java.lang.String") &&
                         params[1].getType().equals("javax.management.openmbean.CompositeData") )
                    {
                        numOpsSupported++;
                    }
                }
            }
            else if ( opInfo.getName().equals("deleteApplicationConfiguration"))
            {
                MBeanParameterInfo[] params = opInfo.getSignature();
                
                if ( params.length == 1 )
                {
                    if ( params[0].getType().equals("java.lang.String") )
                    {
                        numOpsSupported++;
                    }
                }
            }
            else if ( opInfo.getName().equals("queryApplicationConfigurationType"))
            {
                MBeanParameterInfo[] params = opInfo.getSignature();
                 
                if ( params.length == 0 )
                {
                    String returnType = opInfo.getReturnType();
                    
                    if ( returnType.equals("javax.management.openmbean.CompositeType"))
                    {
                        numOpsSupported++;
                    }
                }
            }           
        }
         
        boolean isAppConfigAttribSupported = false;
        MBeanAttributeInfo[] attrInfos = mbnInfo.getAttributes();

        for ( MBeanAttributeInfo attrInfo : attrInfos )
        {
            // -- Check if "ApplicationConfigurations" attribute is present
            if ( attrInfo.getName().equals("ApplicationConfigurations"))
            {
                isAppConfigAttribSupported = true;
            }
        }
         
        return ( numOpsSupported == 4 && isAppConfigAttribSupported);            
     }
     
     /**
      * Get the CompositeType definition for the components application configuration 
      *
      * @return the CompositeType for the components application configuration.
      */
     public CompositeType queryApplicationConfigurationType()
     {
         
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        CompositeType ct = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        try
        {
            Object[] params = new Object[]{};
            String[] sign = new String[] {};
            operationSupportedCheck(configMBeanName, "queryApplicationConfigurationType", sign );

            ct = (CompositeType) mEnvCtx.getMBeanServer().invoke(configMBeanName, "queryApplicationConfigurationType",
                params, sign);

        }
        catch ( Exception iex )
        {
           mLog.warning(MessageHelper.getMsgString(iex));     
        }        
        return ct;
     }
     
      /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appCfgProps - application configuration properties
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String addApplicationConfiguration(String name, Properties appCfgProps) throws MBeanException
     {
         throw new UnsupportedOperationException("addApplicationConfiguration");
     }
     
     /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appConfig - application configuration composite 
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String addApplicationConfiguration(String name, CompositeData appConfig) 
        throws MBeanException
     {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        String response = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        /** Issue 780 Fix
        // If the application configuration elements in the registry is missing
        // get the application configuration from the actual MBean and persist them.
        persistApplicationConfiguration(configMBeanName);
        */
        
        /**
         * This first invokes addApplicationConfiguration on the original configuration 
         * MBean and then updates the registry.
         */
        try
        {
            Object[] params = new Object[]{name, appConfig};
            String[] sign = new String[] {
                    "java.lang.String", "javax.management.openmbean.CompositeData"};
            operationSupportedCheck(configMBeanName, "addApplicationConfiguration", sign );

            mEnvCtx.getMBeanServer().invoke(configMBeanName, "addApplicationConfiguration",
                params, sign);

            // Update the registry
            addApplicationConfiguration(appConfig);

            response =  mMsgBuilder.buildFrameworkMessage("addApplicationConfiguration",
                MessageBuilder.TaskResult.SUCCESS);

        }
        catch ( Exception iex )
        {
            try
            {
                String errMsg = mMsgBuilder.buildExceptionMessage("addApplicationConfiguration",
                    iex);
                throw new MBeanException( new javax.jbi.JBIException(errMsg));
            }
            catch ( ManagementException mex )
            {
                mLog.warning(MessageHelper.getMsgString(mex));
                throw new MBeanException(new Exception(mex.getMessage()));
            }
        }        
        return response;
     }
    
     /**
      * Delete an application configuration. 
      *
      * @param name - identification of the application configuration to be deleted
      * @throws MBeanException if the configuration cannot be deleted.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String deleteApplicationConfiguration(String name) throws MBeanException
     {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        String response = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        /** Issue 780 Fix
        // If the application configuration element in the registry is empty
        // get the application configuration from the actual MBean and persist them.
        persistApplicationConfiguration(configMBeanName); */
        
        /**
         * This first invokes deleteApplicationConfiguration on the original configuration 
         * MBean and then updates the registry.
         */
        try
        {
            Object[] params = new Object[]{name};
            String[] sign = new String[] {
                    "java.lang.String"};
            operationSupportedCheck(configMBeanName, "deleteApplicationConfiguration", sign );

            mEnvCtx.getMBeanServer().invoke(configMBeanName, "deleteApplicationConfiguration", 
                params, sign);

            // Update the registry
            removeApplicationConfiguration(name);

            response =  mMsgBuilder.buildFrameworkMessage("deleteApplicationConfiguration",
                MessageBuilder.TaskResult.SUCCESS);

        }
        catch ( Exception iex )
        {
            try
            {
                String errMsg = mMsgBuilder.buildExceptionMessage("deleteApplicationConfiguration",
                    iex);
                
                throw new MBeanException( new javax.jbi.JBIException(errMsg));
            }
            catch ( ManagementException mex )
            {
                mLog.warning(MessageHelper.getMsgString(mex));
                throw new MBeanException(new Exception(mex.getMessage()));
            }
        }        
        return response;
     }
     
     
     /**
      * Update a application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "configurationName" in the appConfig
      * @param appCfgProps - application configuration properties.
      * @throws MBeanException if there are errors encountered when updating the configuration.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String setApplicationConfiguration(String name, Properties appCfgProps) throws MBeanException
     {
         throw new UnsupportedOperationException("setApplicationConfiguration");
     }
     
     /**
      * Update a application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "configurationName" in the appConfig
      * @param appConfig - application configuration composite
      * @throws MBeanException if there are errors encountered when updating the configuration.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String setApplicationConfiguration(String name, CompositeData appConfig) 
        throws MBeanException
     {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        String response = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        /** Issue 780 fix
        // If the application configuration elements in the registry is missing
        // get the application configuration from the actual MBean and persist them.
        persistApplicationConfiguration(configMBeanName); */
        
        /**
         * This first invokes setApplicationConfiguration on the original configuration 
         * MBean and then updates the registry.
         */
        try
        {
            Object[] params = new Object[]{name, appConfig};
            String[] sign = new String[] {
                    "java.lang.String", "javax.management.openmbean.CompositeData"};
            operationSupportedCheck(configMBeanName, "setApplicationConfiguration", sign );

            mEnvCtx.getMBeanServer().invoke(configMBeanName, "setApplicationConfiguration",
                params, sign);

            // Update the registry
            updateApplicationConfiguration(appConfig);

            response =  mMsgBuilder.buildFrameworkMessage("setApplicationConfiguration",
                MessageBuilder.TaskResult.SUCCESS);

        }
        catch ( Exception iex )
        {
            try
            {
                String errMsg = mMsgBuilder.buildExceptionMessage("updateApplicationConfiguration",
                    iex);
                throw new MBeanException( new javax.jbi.JBIException(errMsg));
            }
            catch ( ManagementException mex )
            {
                mLog.warning(MessageHelper.getMsgString(mex));
                throw new MBeanException(new Exception(mex.getMessage()));
            }
        }        
        return response;
     }
    
    /**
     * Get a Map of all application configurations for the component.
     *
     * @return a TabularData of all the application configurations for a 
     *         component keyed by the configuration name. 
     */
    public TabularData getApplicationConfigurations()
    {
        
        /**
         * If framework not ready then get the application configurations from the registry 
         * using DOM.
         *
         * If the framework is ready ( i.e. JAXB initialized ) get the application configuration 
         *  from the JAXB model
         *
         * If the registry component configuration / application configuration is empty get the 
         * actual values from the MBean.
         */
         
        TabularData appConfigs = null;
        try
        {
            appConfigs = createEmptyAppConfigTable();
            /** Issue 780 fix
            appConfigs = retrieveApplicationConfiguration();
             */
        }
        catch(OpenDataException oex)
        {
            throw new RuntimeException(oex);
        }
        
        if ( appConfigs.isEmpty() )
        {   
            // -- Call getAttributes on the actual MBean, the application variables might not
            //    have been persisted
            
            ObjectName configMBeanName = getComponentConfigurationMBeanName();

            if ( configMBeanName != null )
            {
                if (mEnvCtx.getMBeanServer().isRegistered(configMBeanName))
                {
                    try
                    {
                        appConfigs =  (TabularData) mEnvCtx.getMBeanServer().getAttribute(configMBeanName,
                            "ApplicationConfigurations"); 
                    }
                    catch ( javax.management.JMException iex)
                    {
                        mLog.warning(iex.toString());
                        // -- should never occur
                    }

                }
            }
        }
        return appConfigs;
    }
    
    /** 
     * Retrieves the component specific configuration schema.
     *
     * @return a String containing the configuration schema.
     */
    public String retrieveConfigurationDisplaySchema()
    {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        String response = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        try
        {
            operationSupportedCheck(configMBeanName, 
                    "retrieveConfigurationDisplaySchema", new String[]{});
            response = (String) mEnvCtx.getMBeanServer().invoke(configMBeanName, 
                        "retrieveConfigurationDisplaySchema",
                        new Object[]{}, new String[]{});
        
        }
        catch (Exception ex)
        {
            mLog.warning(MessageHelper.getMsgString(ex));
            throw new RuntimeException(ex.getMessage());
        }
        return response;
    }
    
    /** 
     * Retrieves the component configuration metadata.
     * The XML data conforms to the component 
     * configuration schema.
     *
     * @return a String containing the configuration metadata.
     */
    public String retrieveConfigurationDisplayData()
    {
        ObjectName configMBeanName = getComponentConfigurationMBeanName();
        String response = null;
        
        if ( configMBeanName == null )
        {
            throw new RuntimeException(mTranslator.getString(
                LocalStringKeys.CCFG_MISSING_COMPONENT_CONFIG_MBEAN,
                mComponentName));
        }
        
        try
        {
            String ns = getComponentConfigurationNS(mComponentName);
        
            if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
            {
                // New configuration schema
                response = getComponentConfigurationData(mComponentName); 

            }
            else
            {
                operationSupportedCheck(configMBeanName, 
                        "retrieveConfigurationDisplayData", new String[]{});
                response = (String) mEnvCtx.getMBeanServer().invoke(configMBeanName, 
                            "retrieveConfigurationDisplayData",
                            new Object[]{}, new String[]{});
            }
        
        }
        catch (Exception ex)
        {
            mLog.warning(MessageHelper.getMsgString(ex));
            throw new RuntimeException(ex.getMessage());
        }
        return response;
    }
   

    /*---------------------------------------------------------------------------------*\
     *                                Private Helpers                                  *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Get the MBeanInfo from the actual component configuration MBean. If the component
     * is off-line, this information is not available and a blank MBeanInfo is returned.
     */
    private MBeanInfo getComponentMBeanInfo()
    {
         ObjectName configMBeanName = getComponentConfigurationMBeanName();
         
         if ( configMBeanName != null )
         {
             try
             {
                return mEnvCtx.getMBeanServer().getMBeanInfo(configMBeanName);
             }
             catch ( javax.management.JMException jmx )
             {
                 // This should never be the case
                 mLog.finest(jmx.getMessage());
             }
             
        }
        return new MBeanInfo(this.getClass().getName(), 
            "Dynamic Component Configuration instance MBean",
            new MBeanAttributeInfo[]{},
            new MBeanConstructorInfo[]{},
            new MBeanOperationInfo[]{},
            new MBeanNotificationInfo[]{});  
    }
    
    /** 
     * Get the JMX ObjectName of the actual Component Configuration MBean. 
     * 
     * @return the JMX ObjectName of the actual Component Configuration MBean, if 
     *         a configuration MBean is not registered by the component a null value
     *         is returned.
     */
    private ObjectName getComponentConfigurationMBeanName()
    {
        ObjectName configMBeanName = null;
        /**
         * First check if there is a match for a custom component MBean which uses the
         * following ObjectName pattern : 
         *    com.sun.jbi:ControlType=Configuration,ComponentName=<component-name>
         */
        ObjectName standardPattern = mEnvCtx.getMBeanNames().
            getCustomComponentMBeanNameFilter(
                mEnvCtx.getPlatformContext().getInstanceName(), 
                // CustomControlName="Configuration" 
                "Configuration", 
                mComponentName);
        
        Set<ObjectName> names = mEnvCtx.getMBeanServer().queryNames(standardPattern, null);
        
        if ( !names.isEmpty() )
        {
            if ( names.size() > 1 )
            {
                mLog.finest("More than one MBean matches ObjectName pattern "
                    + standardPattern + ".  " + convertToString(names));
            }
            
            configMBeanName = (ObjectName) names.iterator().next();
        }
        else
        {
            /**
             * If a custom component configuration MBean does not follow the standard 
             * naming convention ( which the open-jbi components don't right now ) then
             * look for the specific MBean for the component :
             *   com.sun.ebi:ServiceType=Configuration,IdentificationName=<component-name>,* 
             */
            ObjectName ebiPattern = null;
            try
            {
                ebiPattern = new ObjectName(
                    "com.sun.ebi:ServiceType=Configuration,IdentificationName="+ mComponentName + ",*");
            }
            catch ( Exception ex)
            {
                mLog.warning(ex.getMessage());
            }
            
            Set<ObjectName> ebiNames = mEnvCtx.getMBeanServer().queryNames(ebiPattern, null);
        
            if ( !ebiNames.isEmpty() )
            {
                if ( ebiNames.size() > 1 )
                {
                    mLog.finest("More than one MBean matches ObjectName pattern "
                        + standardPattern + ".  " + convertToString(ebiNames));
                }

                configMBeanName = (ObjectName) ebiNames.iterator().next();
            }
            
        }
        return configMBeanName;
    }
    
    /**
     * Convert the objects in a set to string.
     *
     * @param set a non-empty set
     */
    private String convertToString(Set set)
    {
        StringBuffer strBuf = new StringBuffer("[ ");
        if ( !set.isEmpty() )
        {
            java.util.Iterator itr = set.iterator();
            while( itr.hasNext() )
            {
                strBuf.append(itr.next().toString());
                if ( itr.hasNext() )
                {
                    strBuf.append(",  ");
                }
                else
                {
                    strBuf.append("  ");
                }
            }
        }
        strBuf.append(" ]");
        return strBuf.toString();
    }
    
    /**
     * Merge two MBeanOperationInfo arrays.
     *
     * @param array1 - 
     * @param array2
     * @return an array containing the OperationInfos from both the arrays.
     */
    private MBeanOperationInfo[] merge(MBeanOperationInfo[] a1, MBeanOperationInfo[] a2)
    {   	
		MBeanOperationInfo[] array = new MBeanOperationInfo[a1.length + a2.length];
        
        // Add the operation infos from the first set
        for (int x=0; x < a1.length; x++) 
        {
            array[x] = a1[x];
        }
        // Add the operation infos from the second set
        for (int x=0; x < a2.length; x++) 
        {
            array[x + a1.length] = a2[x];
        }
        
        return array;
	}
    
    /**
     * Persist component configuration static attribute to the registry
     *
     * @param attrib - the attribute to persist
     */
    private void persistAttribute(Attribute attrib)
    {
        try
        {
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            String attribValueToPersist = encryptCompConfigPasswordField(attrib.getName(),
                        attrib.getValue().toString());
            updater.setComponentAttribute(mComponentName, attrib.getName(), 
                attribValueToPersist);
        }
        catch(Exception rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CCFG_PERSIST_ATTRIBUTE_FAILURE,
                mComponentName, attrib.getName(), rex.getMessage());
            mLog.warning(msg);
        }
    }
    
    /**
     * Persist component configuration static attributes to the registry
     *
     * @param attribs - list of attributes to persist
     */
    private void persistAttributes(AttributeList attribs)
    {
        try
        {
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            
            Properties props = new Properties();
            for ( Object attrib : attribs )
            {
                Attribute attribute = (Attribute) attrib;
                String value = ( attribute.getValue() == null ? "" : attribute.getValue().toString());
                String attribValueToPersist = 
                            encryptCompConfigPasswordField(attribute.getName(),
                                    value);
                
                props.put(attribute.getName(), attribValueToPersist);
            }
            
            updater.setComponentAttributes(mComponentName, props);
        }
        catch(Exception rex)
        {
            StringBuffer strBuf = new StringBuffer();
            strBuf.append("[ ");
            int i = 0;
            for ( Object attrib : attribs )
            {
                i++;
                strBuf.append( ( (Attribute) attrib).getName() );
                if ( i != attribs.size() )
                {
                    strBuf.append( ", ");
                }
            }
            strBuf.append(" ]");
            String msg = mTranslator.getString(
                LocalStringKeys.CCFG_PERSIST_ATTRIBUTE_FAILURE,
                mComponentName, strBuf.toString(), rex.getMessage());
            mLog.warning(msg);
        }
    }
    
    
    /** Issue 780 Fix
     * @return all the properties set on the component and persisted
     *
    private Properties retrieveAttributes()
    {
            Properties props = new Properties();
            
            try
            {
            
                if ( mEnvCtx.isFrameworkReady(false))
                {
                    // Initialization complete can use registry
                    com.sun.jbi.management.ComponentInfo compInfo
                        = (com.sun.jbi.management.ComponentInfo) 
                            getRegistry().getComponentQuery().getComponentInfo(mComponentName);

                    props = compInfo.getConfiguration();
                }
                else
                {
                    // Get the attribute from the registry using DOM
                    RegistryDocument regDoc = new RegistryDocument(
                        mEnvCtx.getReadOnlyRegistry());

                    com.sun.jbi.platform.PlatformContext ctx = mEnvCtx.getPlatformContext();
                    String target = ctx.getTargetName(ctx.getInstanceName());
                    props = regDoc.getComponentConfigurationAttributes( target, 
                        ctx.isStandaloneServer(target), mComponentName);
                }
                props = maskCompConfigPasswordFields(props);
            }
            catch ( Exception ex )
            {
                mLog.warning(MessageHelper.getMsgString(ex));
            }
            
            return props;
     }*/
            
            

    /**
     * @param attribute identification of the attribute
     * @return the MBeanAttributeInfo for the specified attribute
     */
    protected MBeanAttributeInfo getAttributeInfo(String attribute)
    {
        MBeanInfo  mbeanInfo =  this.getMBeanInfo();
        MBeanAttributeInfo[] attribInfos = null;
        MBeanAttributeInfo aInfo = null;
        if ( mbeanInfo != null )
        {
            attribInfos = mbeanInfo.getAttributes();
            for(MBeanAttributeInfo attribInfo : attribInfos )
            {
                if ( attribInfo.getName().equals(attribute))
                {
                    aInfo = attribInfo;
                }
            }
        }
        return aInfo;
    }           
    
    /**
     * Persist the attributes of the component configuration MBean. Skip the 
     * "EnvironmentVariables" attribute
     *
    private void persistMBeanAttributes(ObjectName configMBeanName)
    {
        MBeanAttributeInfo[] attribInfos = getMBeanInfo().getAttributes();
        String[] attributes = new String[attribInfos.length];
                    int i = 0;
        for ( MBeanAttributeInfo attribInfo : attribInfos )
        {
            if ( !attribInfo.getName().equals("ApplicationVariables") &&
                 !attribInfo.getName().equals("ApplicationConfigurations"))
            {
                attributes[i++] = attribInfo.getName();
            }
        }
        
        if ( mEnvCtx.getMBeanServer().isRegistered(configMBeanName))
        {
            try
            {
                AttributeList attribList = mEnvCtx.getMBeanServer().getAttributes(configMBeanName,
                    attributes);
                persistAttributes(attribList);
            } 
            catch ( javax.management.JMException iex)
            {
                mLog.warning(iex.getMessage());
            }
        }        
    }*/
    
     /**
      * Add the application variable to the registry.
      *
      * @param appVar - the application variable to be added to the registry.
      */
     private void addApplicationVariable(CompositeData appVar)
        throws com.sun.jbi.management.registry.RegistryException
     {
        com.sun.jbi.management.ComponentInfo.Variable[] vars =
                new com.sun.jbi.management.ComponentInfo.Variable[1];

        try
        {
            String appVarValue = (String) appVar.get("value");
            if ( PASSWORD.equals((String) appVar.get("type")) )
            {
                appVarValue = mEnvCtx.getPlatformContext().getKeyStoreUtil().
                        encrypt(appVarValue);
            }
            vars[0] = new com.sun.jbi.management.ComponentInfo.Variable(
                        (String) appVar.get("name"),
                        appVarValue,
                        (String) appVar.get("type"));
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            updater.addComponentApplicationVariables(mComponentName, vars);
        }
        catch(Exception rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CCFG_FAILED_ADD_APP_VAR_REG,
                vars[0].getName(), mComponentName, rex.getMessage());
            mLog.warning(msg);
        }
     }
    
     
     /**
      * Update the application variable in the registry.
      *
      * @param appVar - the application variable to be updated in the registry.
      */
     private void updateApplicationVariable(CompositeData appVar)
        throws com.sun.jbi.management.registry.RegistryException
     {
        com.sun.jbi.management.ComponentInfo.Variable[] vars =
                new com.sun.jbi.management.ComponentInfo.Variable[1];

        try
        {
            String appVarValue = (String) appVar.get("value");
            if ( PASSWORD.equals((String) appVar.get("type")) )
            {
                appVarValue = mEnvCtx.getPlatformContext().getKeyStoreUtil().
                        encrypt(appVarValue);
            }            
            
            vars[0] = new com.sun.jbi.management.ComponentInfo.Variable(
                        (String) appVar.get("name"),
                        appVarValue,
                        (String) appVar.get("type"));
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            updater.updateComponentApplicationVariables(mComponentName, vars);
        }
        catch(Exception rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CCFG_FAILED_UPDATE_APP_VAR_REG,
                vars[0].getName(), mComponentName, rex.getMessage());
            mLog.warning(msg);
        }
     }
     
     
     /**
      * Delete the application variable from the registry.
      *
      * @param name - identification for the application variable to be deleted.
      */
     private void removeApplicationVariable(String name)
        throws com.sun.jbi.management.registry.RegistryException
     {

        try
        {
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            updater.deleteComponentApplicationVariables(mComponentName, new String[]{name});
        }
        catch(com.sun.jbi.management.registry.RegistryException rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CCFG_FAILED_DELETE_APP_VAR_REG,
                name, mComponentName, rex.getMessage());
            mLog.warning(msg);
        }
     }
     
    /**
     * Get a handle to the registry
     *
     * @return the persisted registry instance
     */
    private com.sun.jbi.management.registry.Registry getRegistry()
    {
        return (com.sun.jbi.management.registry.Registry) mEnvCtx.getRegistry();
    }
    
    /**
     * @return true if the MBean implements the operation.
     *
     * @param mbean - MBean ObjectName
     * @param op - name of the operation
     * @param sign = operation parameter signature
     */
    private boolean operationSupportedCheck(ObjectName mbean, String op, String[] sign )
    {
        MBeanInfo mbeanInfo = null;
        boolean found = false;
        
        try
        {
            if ( mEnvCtx.getMBeanServer().isRegistered(mbean) )
            {
                mbeanInfo = mEnvCtx.getMBeanServer().getMBeanInfo(mbean);
            }
        }
        catch(Exception ex )
        {
            mLog.warning(ex.getMessage());
            return found;
        }
        
        MBeanOperationInfo[] opInfos = mbeanInfo.getOperations();
        
        
        for ( MBeanOperationInfo opInfo : opInfos )
        {
            if ( opInfo.getName().equals(op) )
            {
                found = true;
                break;
            }
        }
        if ( !found )
        {
            String errMsg = mTranslator.getString(LocalStringKeys.CCFG_UNSUPPORTED_OPERATION,
                op, mbean.toString());
            throw new UnsupportedOperationException(errMsg);
        }
        return found;
    }
    
    
    /** Issue 780 Fix
     * @return all the application variables set on the component and persisted
     *
    private TabularData retrieveApplicationVariables()
    {
            TabularData td = new TabularDataSupport(mCfgHlpr.getApplicationVariableTabularType());
            Variable[] vars = new Variable[0];
            try
            {
            
                if ( mEnvCtx.isFrameworkReady(false))
                {
                    // Initialization complete can use registry
                    com.sun.jbi.management.ComponentInfo compInfo
                        = (com.sun.jbi.management.ComponentInfo) 
                            getRegistry().getComponentQuery().getComponentInfo(mComponentName);

                    vars = compInfo.getVariables();
                }
                else
                {
                    // Get the attribute from the registry using DOM
                    RegistryDocument regDoc = new RegistryDocument(
                        mEnvCtx.getReadOnlyRegistry());

                    com.sun.jbi.platform.PlatformContext ctx = mEnvCtx.getPlatformContext();
                    String target = ctx.getTargetName(ctx.getInstanceName());
                    vars = regDoc.getComponentApplicationVariables( target, 
                        ctx.isStandaloneServer(target), mComponentName);
                }
                
                for ( Variable var : vars )
                {
                    String[] itemNames = mCfgHlpr.getApplicationVariableItemNames();
                        
                    String appVarValue = var.getValue();
                    if ( PASSWORD.equalsIgnoreCase(var.getType()) ) 
                    {
                        appVarValue = MASKED_FIELD_VALUE;
                    }                    
                    CompositeDataSupport cd = new CompositeDataSupport(
                        mCfgHlpr.getApplicationVariablesType(),
                        itemNames,
                        new String[]{var.getName(), appVarValue, var.getType()});

                    td.put(cd);
                }
                
            }
            catch ( Exception ex )
            {
                mLog.warning(MessageHelper.getMsgString(ex));
            }
            // return the retrived values 
            return td;
     }
    
    /**
     * If the application variables element in the registry is empty
     * get the application variables from the actual MBean and persist them.
     *
     * @param configMBeanName - the ConfigurationMBean Object Name
     *
    private void persistApplicationVariables(ObjectName configMBeanName)
    {
        TabularData appVars = null;
        com.sun.jbi.management.ComponentInfo compInfo = null;
        try
        {
            // Get the application variables persisted in the registry
            compInfo
                = (com.sun.jbi.management.ComponentInfo) 
                    getRegistry().getComponentQuery().getComponentInfo(mComponentName);
        }
        catch ( com.sun.jbi.management.registry.RegistryException rex)
        {
            mLog.warning(MessageHelper.getMsgString(rex));
        }
        
        Variable[] vars = compInfo.getVariables();
                    
        if ( vars.length == 0 )
        {
            if (mEnvCtx.getMBeanServer().isRegistered(configMBeanName))
            {
                try
                {
                    appVars =  (TabularData) mEnvCtx.getMBeanServer().getAttribute(configMBeanName,
                        "ApplicationVariables"); 
                }
                catch ( javax.management.JMException iex)
                {
                    mLog.warning(iex.getMessage());
                    // -- should never occur
                }
                
                vars = new Variable[appVars.size()];
                java.util.Collection cds = appVars.values();
                int i=0;
                  try
                {
                    for ( java.util.Iterator itr = cds.iterator(); itr.hasNext(); )
                    {
                        CompositeData cd = (CompositeData) itr.next();

                        String appVarValue = (String) cd.get("value");
                        if ( PASSWORD.equals((String) cd.get("type")) )
                        {
                            appVarValue = mEnvCtx.getPlatformContext().getKeyStoreUtil().
                                    encrypt(appVarValue);
                        }

                        vars[i++] = new Variable(
                                (String) cd.get("name"), 
                                appVarValue, 
                                (String) cd.get("type"));
                    }
                
                    // persist them
                    com.sun.jbi.management.registry.Updater
                    updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                    updater.addComponentApplicationVariables(mComponentName, vars);
                }
                catch(Exception rex)
                {
                    String msg = mTranslator.getString(
                        LocalStringKeys.CCFG_FAILED_ADD_APP_VAR_REG,
                        vars[0].getName(), mComponentName, rex.getMessage());
                    mLog.warning(msg);
                }
            }
        }
    }*/
    
    /** Issue 780 fix
     * If the component is offline this returns a null value, since the components
     * application configuration composite type is not known.
     *
     * @return all the application configurations set on the component and persisted
     *
    private TabularData retrieveApplicationConfiguration()
        throws OpenDataException
    {
        CompositeType appConfigCompositeType = queryApplicationConfigurationType();

        TabularData td = null;

        if ( appConfigCompositeType != null )
        {
            td = createEmptyAppConfigTable();
            try
            {
                Map<String, Properties> appConfigMap = new java.util.HashMap();
                if ( mEnvCtx.isFrameworkReady(false))
                {
                    // Initialization complete can use registry
                    com.sun.jbi.management.ComponentInfo compInfo
                        = (com.sun.jbi.management.ComponentInfo) 
                            getRegistry().getComponentQuery().getComponentInfo(mComponentName);

                    String[] configNames = compInfo.getApplicationConfigurationNames();
                    for ( String configName : configNames )
                    {
                        Properties appCfgProps = compInfo.getApplicationConfiguration(configName);
                        appCfgProps = maskPasswordFields(appCfgProps);
                        appConfigMap.put(configName, appCfgProps);
                    }
                }
                else
                {
                    // Get the attribute from the registry using DOM
                    RegistryDocument regDoc = new RegistryDocument(
                        mEnvCtx.getReadOnlyRegistry());

                    com.sun.jbi.platform.PlatformContext ctx = mEnvCtx.getPlatformContext();
                    String target = ctx.getTargetName(ctx.getInstanceName());
                    appConfigMap = regDoc.getComponentApplicationConfiguration( target, 
                        ctx.isStandaloneServer(target), mComponentName);
                }

                Set<String> configNames = appConfigMap.keySet();
                for ( String configName : configNames )
                {
                    Properties appCfgProps = appConfigMap.get(configName);
                    appCfgProps = maskPasswordFields(appCfgProps);
                    CompositeData cd = mCfgHlpr.convertPropertiesToCompositeData(
                        appCfgProps, appConfigCompositeType);

                    td.put(cd);
                }
            }
            catch ( Exception ex )
            {
                mLog.warning(MessageHelper.getMsgString(ex));
            }
        }
        // return the retrieved values 
        return td;
     }
    
    /**
     * If the application configuration element in the registry is empty
     * get the application configuration from the actual MBean and persist them.
     *
     * @param configMBeanName - the ConfigurationMBean Object Name
     *
    private void persistApplicationConfiguration(ObjectName configMBeanName)
    {
        TabularData appConfigs = null;
        com.sun.jbi.management.ComponentInfo compInfo = null;
        try
        {
            compInfo
                = (com.sun.jbi.management.ComponentInfo) 
                    getRegistry().getComponentQuery().getComponentInfo(mComponentName);
        }
        catch ( com.sun.jbi.management.registry.RegistryException rex)
        {
            mLog.warning(MessageHelper.getMsgString(rex));
        }
        
        String[] configNames = compInfo.getApplicationConfigurationNames();
                    
        if ( configNames.length == 0 )
        {
            if (mEnvCtx.getMBeanServer().isRegistered(configMBeanName))
            {
                try
                {
                    appConfigs =  (TabularData) mEnvCtx.getMBeanServer().getAttribute(configMBeanName,
                        "ApplicationConfigurations"); 
                }
                catch ( javax.management.JMException iex)
                {
                    mLog.warning(iex.getMessage());
                    // -- should never occur
                }
                
                if ( appConfigs != null )
                {
                    java.util.Collection cds = appConfigs.values();
                    int i=0;
                    for ( java.util.Iterator itr = cds.iterator(); itr.hasNext(); )
                    {
                        CompositeData cd = (CompositeData) itr.next();
                        Properties props = mCfgHlpr.convertCompositeDataToProperties(cd);

                        // persist them
                        try
                        {
                            props = encryptPasswordFields(props);
                            com.sun.jbi.management.registry.Updater
                            updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                            updater.addComponentApplicationConfiguration(mComponentName, props);
                        }
                        catch(Exception rex)
                        {
                            String msg = mTranslator.getString(
                                LocalStringKeys.CCFG_FAILED_ADD_APP_CFG_REG,
                                cd.get("configurationName"), mComponentName, rex.getMessage());
                            mLog.warning(msg);
                        }
                    }
                }
            }
        }
    }*/
    
     /**
      * Add the application configuration to the registry.
      *
      * @param appConfig - the application configuration to be added to the registry.
      */
     private void addApplicationConfiguration(CompositeData appConfig)
        throws com.sun.jbi.management.registry.RegistryException
     {
        try
        {
            Properties cfgProps = mCfgHlpr.convertCompositeDataToProperties(appConfig);
            cfgProps = encryptPasswordFields(cfgProps);
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            updater.addComponentApplicationConfiguration(mComponentName, cfgProps);
        }
        catch(Exception rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CCFG_FAILED_ADD_APP_CFG_REG,
                appConfig.get("configurationName"), mComponentName, rex.getMessage());
            mLog.warning(msg);
        }
     }
    
     
     /**
      * Update the application configuration in the registry.
      *
      * @param appVar - the application configuration to be updated in the registry.
      */
     private void updateApplicationConfiguration(CompositeData appConfig)
        throws com.sun.jbi.management.registry.RegistryException
     {

        try
        {
            Properties cfgProps = mCfgHlpr.convertCompositeDataToProperties(appConfig);
            cfgProps = encryptPasswordFields(cfgProps);
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            updater.updateComponentApplicationConfiguration(mComponentName, cfgProps);
        }
        catch(Exception rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CCFG_FAILED_UPDATE_APP_CFG_REG,
                appConfig.get("configurationName"), mComponentName, rex.getMessage());
            mLog.warning(msg);
        }
     }
     
     
     /**
      * Delete the application configuration from the registry.
      *
      * @param name - identification for the application configuration to be deleted.
      */
     private void removeApplicationConfiguration(String name)
        throws com.sun.jbi.management.registry.RegistryException
     {

        try
        {
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            updater.deleteComponentApplicationConfiguration(mComponentName, name);
        }
        catch(com.sun.jbi.management.registry.RegistryException rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CCFG_FAILED_DELETE_APP_CFG_REG,
                name, mComponentName, rex.getMessage());
            mLog.warning(msg);
        }
     }   
     
     /**
      * @return an empty TabularData instance based on the components application
      *         configuration Type.
      */    
     private TabularData createEmptyAppConfigTable()
        throws javax.management.openmbean.OpenDataException
     {
        CompositeType appConfigCompositeType = queryApplicationConfigurationType();

        TabularData td = null;
        if ( appConfigCompositeType != null )
        {
            TabularType tt = new TabularType(
                "Applcation Configuration",
                "Table of Application configurations",
                appConfigCompositeType,
                new String[]{"configurationName"});
            td = new TabularDataSupport(tt);
        }
        return td;
    }
     
     /**
      * @return an empty TabularData instance based on the components application
      *         variable Type.
      */     
    protected TabularData createEmptyAppVarTable()
        throws javax.management.openmbean.OpenDataException
    {
        return new TabularDataSupport(mCfgHlpr.getApplicationVariableTabularType());
    }
    
    private Properties encryptPasswordFields(Properties cfgProps)
        throws Exception
    {
        Document configDoc = getConfigDoc();
        
        Set cfgNames = cfgProps.keySet();
        
        for ( Object cfgName : cfgNames )
        {
            if ( mCfgHlpr.isPassword((String)cfgName, configDoc) )
            {
                String clearTxt = cfgProps.getProperty((String)cfgName);
                String encrTxt = mEnvCtx.getPlatformContext().getKeyStoreUtil().encrypt(clearTxt);
                cfgProps.setProperty((String)cfgName, encrTxt);
            }
        }
        
        return cfgProps;
    }
    
    private Properties maskPasswordFields(Properties cfgProps)
        throws Exception
    {
        Document configDoc = getConfigDoc();
        
        Set cfgNames = cfgProps.keySet();
        
        for ( Object cfgName : cfgNames )
        {
            if ( mCfgHlpr.isPassword((String)cfgName, configDoc) )
            {
                cfgProps.setProperty((String)cfgName, MASKED_FIELD_VALUE);
            }
        }
        
        return cfgProps;
    }
    
    /**
     * Encrypt component configuration attribute value if it is a password field
     *
     * @param cfgName  - component configuration attribute name
     * @param cfgValue - component configuration attribute value
     */
    private String encryptCompConfigPasswordField(String cfgName, String cfgValue)
        throws Exception
    {
        Document configDoc = getConfigDoc();

        if ( mCfgHlpr.isPassword(cfgName, configDoc) )
        {
            String encrTxt = mEnvCtx.getPlatformContext().
                    getKeyStoreUtil().encrypt(cfgValue);
            return encrTxt;
        }
        return cfgValue;
    }
    
    /**
     * If a component configuration attribute is a password field, mask it 
     * i.e. replace value with MASKED_FIELD_VALUE.
     *
     * @param cfgProps - component configuration attributes
     */
    private Properties maskCompConfigPasswordFields(Properties cfgProps)
        throws Exception
    {
        Document configDoc = getConfigDoc();
        
        Set cfgNames = cfgProps.keySet();
        
        for ( Object cfgName : cfgNames )
        {
            if ( mCfgHlpr.isPassword((String)cfgName, configDoc) )
            {
                cfgProps.setProperty((String)cfgName, MASKED_FIELD_VALUE);
            }
        }
        
        return cfgProps;
    }   
    
    private Document getConfigDoc()
        throws Exception
    {
        String xmlConfigData = retrieveConfigurationDisplayData();
        Document doc = null;
        if (xmlConfigData != null)
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db  = dbf.newDocumentBuilder();
            java.io.StringReader reader = new java.io.StringReader(xmlConfigData);
            org.xml.sax.InputSource ipsrc = new org.xml.sax.InputSource(reader); 

            doc = db.parse(ipsrc);
        }
        return doc;
    }
    
        /**
     * Get the namespace of the "Configuration" element in the jbi.xml, if one 
     * exists. Return an empty string if a "Configuration" emenet is not 
     * defined.
     *
     * @param compName - component name
     * @return the configuration element namespace
     */
    protected String getComponentConfigurationNS(String compName)
    {
        String ns = "";
        
        Archive compArchive = getRegistry().getRepository().
                getArchive(ArchiveType.COMPONENT, compName);
        if ( compArchive != null )
        {
            ComponentDescriptor descr = new ComponentDescriptor(
                    compArchive.getJbiXml(true));
            ns = descr.getComponentConfigurationNS();
        }
        
        return ns;
    }
    
    /**
     * Get the the "Configuration" element in the jbi.xml. If the Configuration 
     * element is missing in the jbi.xml a null value is returned
     *
     * @param compName - component name
     * @return the configuration xml string
     */
    protected String getComponentConfigurationData(String compName)
    {
        String xmlStr = null;
        
        Archive compArchive = getRegistry().getRepository().
                getArchive(ArchiveType.COMPONENT, compName);
        if ( compArchive != null )
        {
            ComponentDescriptor descr = new ComponentDescriptor(
                    compArchive.getJbiXml(true));
            xmlStr = descr.getComponentConfigurationXml();
        }
        
        return xmlStr;
    }
}


    
