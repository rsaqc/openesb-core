/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.registry.Updater;

/**
 * This is the implementation of the Component Registry, which provides
 * for registration of Shared Libraries, Binding Components, and Service
 * Engines within the JBI environment.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentRegistry
    implements com.sun.jbi.ServiceLifecycle,
    com.sun.jbi.ServiceUnitRegistration
{
    /**
     * Local handle to the EnvironmentContext
     */
    private EnvironmentContext mContext;

    /**
     * Local flag for start/stop checking
     */
    private Boolean mStarted = null;
   
    /**
     * Cache for Binding Components and Service Engines.
     */
    private HashMap mComponentCache;

    /**
     * Cache for Shared Libraries.
     */
    private HashMap mSharedLibraryCache;

    /**
     * Logger for the component registry service.
     */
    private Logger mLog;

    /**
     * Framework Statistics.
     */
    private FrameworkStatistics mStats;

    /**
     * Local handle to the StringTranslator for the package
     */
    private StringTranslator mTranslator;

    /**
     * Local handle to the registry's query implementation.
     */
    private ComponentQuery mComponentQuery;

    /**
     * Local handle to the registry's updater implementation.
     */
    private Updater mRegistryUpdater;


//------------------------- ServiceLifecycle methods --------------------------

    /**
     * Initialize the Component Registry service.
     * @param context The JBI environment context created by the JBI framework.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void initService(com.sun.jbi.EnvironmentContext context)
        throws javax.jbi.JBIException
    {
        mContext = (EnvironmentContext) context;
        mTranslator = (StringTranslator) context.getStringTranslatorFor(this);

        if ( null != mStarted )
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.SERVICE_ALREADY_INITIALIZED,
                    mTranslator.getString(LocalStringKeys.CR_NAME)));
        }
        mStarted = mStarted.FALSE;
        mComponentCache = new HashMap();
        mSharedLibraryCache = new HashMap();
        mStats = mContext.getFrameworkStatistics();
        mLog = mContext.getLogger();
        mLog.fine(mTranslator.getString(LocalStringKeys.SERVICE_INITIALIZED,
            mTranslator.getString(LocalStringKeys.CR_NAME)));
    }

    /**
     * Start the Component Registry service.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void startService()
        throws javax.jbi.JBIException
    {
        if ( mStarted.booleanValue() )
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.SERVICE_ALREADY_STARTED,
                    mTranslator.getString(LocalStringKeys.CR_NAME)));
        }

        // Get instances of the persistent registry query and updater.

        mComponentQuery = ((Registry) mContext.getJustRegistry()).getComponentQuery();
        mRegistryUpdater = ((Registry) mContext.getJustRegistry()).getUpdater();

        // Reload the component registry cache from the persistent registry
        // information saved by the previous execution of the JBI framework.

        reloadRegistry();
 
        // Indicate that the Component Registry is now stared.

        mStarted = mStarted.TRUE;
        mLog.fine(mTranslator.getString(LocalStringKeys.SERVICE_STARTED,
            mTranslator.getString(LocalStringKeys.CR_NAME)));
    }

    /**
     * Stop the Component Registry service.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void stopService()
        throws javax.jbi.JBIException
    {
        if ( !mStarted.booleanValue() )
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.SERVICE_ALREADY_STOPPED,
                    mTranslator.getString(LocalStringKeys.CR_NAME)));
        }

        mStarted = null;
        mContext = null;
        mComponentCache = null;
        mSharedLibraryCache = null;
        mStats = null;
        mComponentQuery = null;
        mRegistryUpdater = null;

        mLog.fine(mTranslator.getString(LocalStringKeys.SERVICE_STOPPED,
            mTranslator.getString(LocalStringKeys.CR_NAME)));

        mLog = null;
        mTranslator = null;
    }

//--------------------- ServiceUnitRegistration methods -----------------------

    /**
     * Check to see if a Service Unit with the specified name is registered to
     * the specified BC or SE.
     * @param componentName is the unique name of the BC or SE to check.
     * @param serviceUnitName is the unique name of the Service Unit to check
     * for.
     * @return boolean is true if the Service Unit is registered, false if not.
     * @throws javax.jbi.JBIException if no component exists with the specified
     * component name.
     */
    public boolean isServiceUnitRegistered(String componentName,
                                           String serviceUnitName)
        throws javax.jbi.JBIException
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }
        if ( null == componentName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }
        Component comp = getComponent(componentName);
        if ( null != comp )
        {
            return comp.isServiceUnitRegistered(serviceUnitName);
        }
        else
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CR_COMPONENT_NOT_REGISTERED, componentName);
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg);
        }
    }

    /**
     * Register a Service Unit that has been deployed to a particular BC or
     * SE. This is done after a successful deployment operation.
     * @param componentName The unique name of the BC or SE.
     * @param serviceAssemblyName The unique name of the Service Assembly.
     * @param serviceUnitName The unique name of the Service Unit.
     * @param serviceUnitFilePath The fully-qualified path to the Service Unit
     * deployment descriptor.
     * @throws javax.jbi.JBIException if no component exists with the specified
     * component name or if there is already a Service Unit with the specified
     * service unit name registered to the component.
     */
    public synchronized void registerServiceUnit(
        String componentName,
        String serviceAssemblyName,
        String serviceUnitName,
        String serviceUnitFilePath)
        throws javax.jbi.JBIException
    {
        if ( null == componentName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }
        if ( null == serviceAssemblyName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "serviceAssemblyName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }
        if ( null == serviceUnitFilePath )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "serviceUnitFilePath");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        Component comp = getComponent(componentName);
        if ( null != comp )
        {
            ServiceUnit su = new ServiceUnit(serviceAssemblyName,
                serviceUnitName, serviceUnitFilePath, componentName);
            mRegistryUpdater.addServiceUnitToComponent(componentName, su);
            comp.addServiceUnit(su);
        }
        else
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CR_COMPONENT_NOT_REGISTERED, componentName);
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg);
        }
    }

    /**
     * Unregister a Service Unit that has been undeployed from a particular BC
     * or SE. This is done after a successful undeployment operation.
     * @param componentName The unique name of the BC or SE.
     * @param serviceUnitName The unique name of the Service Unit.
     * @throws javax.jbi.JBIException if no component exists with the specified
     * component name or if there is no Service Unit with the specified
     * service unit name registered to the component.
     */
    public void unregisterServiceUnit(
        String componentName,
        String serviceUnitName)
        throws javax.jbi.JBIException
    {
        if ( null == componentName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }
        Component comp = getComponent(componentName);
        if ( null != comp )
        {
            mRegistryUpdater.removeServiceUnitFromComponent(
                componentName, serviceUnitName);
            comp.removeServiceUnit(serviceUnitName);
        }
        else
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CR_COMPONENT_NOT_REGISTERED, componentName);
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg);
        }
    }

//--------------------------- Package-only methods ----------------------------

   /**
    * Commit a Binding Component or a Service Engine. This causes the component
    * to be permanently registered in the persistent registry after installation
    * is complete.
    * @param componentName the unique component name.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    void commitComponent(String componentName)
        throws javax.jbi.JBIException
    {
        if ( null == componentName )
        {
            String msg = mTranslator.getString(
                 LocalStringKeys.NULL_ARGUMENT, "componentName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        mLog.finer("Permanently registering component " + componentName);
        Component component = getComponent(componentName);

        if (null == component)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CR_PERMANENT_REGISTRATION_NOT_FOUND,
                componentName);
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg);
        }
        
        List<String> registeredComps = mComponentQuery.getComponentIds(
            ComponentType.BINDINGS_AND_ENGINES);
        if ( !registeredComps.contains(component.getName()) )
        {
            mRegistryUpdater.addComponent((ComponentInfo) component);
            mLog.fine("Component " + component.getName() + " registered");
        }
        else
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CR_COMPONENT_ALREADY_REGISTERED,
                component.getName());
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg);
        }
    }

   /**
    * Get a list of all installed Components (Binding Components and Service
    * Engines).
    * @return A list of Component instances.
    */
    List getAllComponents()
    {
        return new ArrayList(mComponentCache.values());
    }

   /**
    * Get a list of all installed Shared Libraries.
    * @return A list of SharedLibrary instances.
    */
    List getAllSharedLibraries()
    {
        return new ArrayList(mSharedLibraryCache.values());
    }

   /**
    * Look up a component (Binding Component or Service Engine) using its
    * unique name. 
    * @param componentName is the unique name of the BC or SE.
    * @return Component contains all runtime information about the component
    * or null if no BC or SE was found with the specified name.
    * @throws java.lang.IllegalArgumentException if the componentName parameter
    * is null.
    */
    Component getComponent(String componentName)
    {
        if ( null == componentName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        mLog.finest("Looking up component " + componentName + " in cache");

        // Get the Component from the Cache which is loaded at startup time.

        Component theComponent = (Component) mComponentCache.get(componentName);
       
        if ( null != theComponent )
        {
            mLog.finest("Component " + componentName + " found in cache");
        }
        else
        {
            mLog.finest("Component " + componentName + " not found in cache");
        }
        return theComponent;
    }

   /**
    * Get a list of component IDs of the specified type, with the
    * specified status.
    * @param type The type of component for which the IDs are requested.
    * @param status The status for which the IDs are requested.
    * @return A list of Strings containing the component IDs. If no components
    * were found, the list is empty.
    */
    List<String> getComponentIdsFromCache(ComponentType type, ComponentState status)
    {

        List<String> ids = new ArrayList();

        // First search the Component cache if this call is to list anything
        // other than just Shared Libraries.
 
        if ( ComponentType.SHARED_LIBRARY != type )
        {
            Collection values = mComponentCache.values();
            Iterator i = values.iterator();
            Component comp;
            ComponentType ct;

            while ( i.hasNext() )
            {
                comp = (Component) i.next();
                ct = comp.getComponentType();
                switch ( type )
                {
                case ALL:
                    break;
                case BINDINGS_AND_ENGINES:
                    if ( ct != ComponentType.BINDING
                        && ct != ComponentType.ENGINE )
                    {
                        continue;
                    }
                    break;
                case BINDING:
                case ENGINE:
                    if ( ct != type )
                    {
                        continue;
                    }
                    break;
                default:
                    break;
                }
                if ( null == status || status == comp.getStatus() )
                {
                    ids.add(comp.getName());
                }
            }
        }
        
        
        // Now search the SharedLibrary cache if this call is to list everything
        // or just Shared Libraries.
 
        if ( ComponentType.ALL == type || ComponentType.SHARED_LIBRARY == type )
        {
            Collection values = mSharedLibraryCache.values();
            Iterator i = values.iterator();
            SharedLibrary sl;
            while ( i.hasNext() )
            {
                sl = (SharedLibrary) i.next();
                if ( null == status || status == sl.getStatus() )
                {
                    ids.add(sl.getName());
                }
            }
        }

        return ids;
    }

   /**
    * Return a list of all components dependent upon the specified Shared
    * Library.
    * @param sharedLibraryName - the name of the Shared Library.
    * @return A list containing the Component instances of all components
    * that depend upon the Shared Library.
    */
    List getDependents(String sharedLibraryName)
    {
        boolean isDependent = false;
        List depList = getAllComponents();
        Iterator d = depList.iterator();
        Component comp = null;
        while ( d.hasNext() )
        {
            comp = (Component) d.next();
            List sharedLibraryList = comp.getSharedLibraryNames();
            if ( null == sharedLibraryList )
            {
                d.remove();
            }
            else
            {
                Iterator s = sharedLibraryList.iterator();
                String sl = null;
                while ( s.hasNext() )
                {
                    sl = (String) s.next();
                    if ( sl.equals(sharedLibraryName) )
                    {
                        isDependent = true;
                        break;
                    }
                }
                if ( !isDependent )
                {
                    d.remove();
                }
                else
                {
                    isDependent = false;
                }
            }
        }
        return depList;
    }

   /**
    * Look up a SharedLibrary object using its assigned name.
    * @param sharedLibraryName The unique name of the Shared Library.
    * @return The SharedLibrary instance, or null if no Shared Library was
    * found with the specified name.
    */
    SharedLibrary getSharedLibrary(String sharedLibraryName)
    {
        if ( null == sharedLibraryName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "sharedLibraryName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        mLog.finer("Looking up Shared Library " + sharedLibraryName +
            " in cache");

        SharedLibrary sl =
            (SharedLibrary) mSharedLibraryCache.get(sharedLibraryName);

        if ( null != sl )
        {
            mLog.finer("Shared Library " + sharedLibraryName +
                " found in cache");
        }
        else
        {
            mLog.finer("Shared Library " + sharedLibraryName +
                " not found in cache");
        }
        return sl;
    }

   /**
    * Get the current state of a Binding Component or Service Engine. The
    * returned value is one of the component state values defined in the
    * com.sun.jbi.ComponentState enumerator: LOADED, SHUTDOWN, STOPPED, or
    * or STARTED.
    * @param componentName the unique component name assigned to the BC or SE.
    * @return the current state of the BC or SE.
    * @throws javax.jbi.JBIException if the component name is not found.
    */
    public ComponentState getStatus(String componentName)
        throws javax.jbi.JBIException
    {
        Component comp = getComponent(componentName);
        if ( null != comp )
        {
            return comp.getStatus();
        }
        else
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CR_COMPONENT_NOT_REGISTERED, componentName);
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg);
        }
    }

   /**
    * Check to see if a component with the specified name is registered.
    * @param componentName is the component name to check for.
    * @return boolean is true if the component is registered, false if not.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    boolean isComponentRegistered(String componentName)
        throws javax.jbi.JBIException
    {
        if ( null == componentName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        if ( mComponentCache.containsKey(componentName) )
        {
            mLog.finest("Component " + componentName + " is registered");
            return true;
        }
        else
        {
            mLog.finest("Component " + componentName + " is not registered");
            return false;
        }
    }

   /**
    * Check to see if a Shared Library with the specified name is registered.
    * @param sharedLibraryName is the Shared Library to check for.
    * @return boolean is true if the Shared Library is registered, false if not.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    boolean isSharedLibraryRegistered(String sharedLibraryName)
        throws javax.jbi.JBIException
    {
        if ( null == sharedLibraryName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "sharedLibraryName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        if ( mSharedLibraryCache.containsKey(sharedLibraryName) )
        {
            mLog.finest("Shared Library " + sharedLibraryName +
                " is registered");
            return true;
        }
        else
        {
            mLog.finest("Shared Library " + sharedLibraryName +
                " is not registered");
            return false;
        }
    }

   /**
    * Register a Binding Component or a Service Engine.
    * @param component contains the component runtime information.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    void registerComponent(Component component)
        throws javax.jbi.JBIException
    {
        if ( null == component )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "component");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        mLog.finer("Registering new component " + component.getName() +
            " in cache");
 
        synchronized ( mComponentCache )
        {
            if ( !mComponentCache.containsKey(component.getName()) )
            {
                mComponentCache.put(component.getName(), component);
                mStats.incrementRegistryAdds();
                mLog.fine("Component " + component.getName() +
                    " registered in cache");
            }
            else
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.CR_COMPONENT_ALREADY_REGISTERED,
                    component.getName());
                mLog.warning(msg);
                throw new javax.jbi.JBIException(msg);
            }
        }
    }

   /**
    * Register the unique name and class path for a Shared Library.
    * @param sharedLib The Shared Library instance.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    void registerSharedLibrary(SharedLibrary sharedLib)
        throws javax.jbi.JBIException
    {
        if ( null == sharedLib )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "sharedLib");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        mLog.finer("Registering new Shared Library " + sharedLib.getName() +
            " in cache");

        List<String> installedSLs = mComponentQuery.getComponentIds(
            ComponentType.SHARED_LIBRARY);
        if ( !installedSLs.contains(sharedLib.getName()) )
        {
            synchronized (mSharedLibraryCache)
            {
                if ( !mSharedLibraryCache.containsKey(sharedLib.getName()) )
                {
                    mSharedLibraryCache.put(sharedLib.getName(), sharedLib);
                }
                else
                {
                    String msg = mTranslator.getString(
                        LocalStringKeys.CR_SL_ALREADY_REGISTERED,
                        sharedLib.getName());
                    mLog.warning(msg);
                    throw new javax.jbi.JBIException(msg);
                }
            }
            mRegistryUpdater.addSharedLibrary( (ComponentInfo) sharedLib);
            mStats.incrementRegistryAdds();
            mLog.fine("Shared Library " + sharedLib.getName() +
                " registered in cache");
        }
        else
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CR_SL_ALREADY_REGISTERED,
                sharedLib.getName());
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg);
        }
    }

   /**
    * Remove a Binding Component or a Service Engine from the registry after
    * a failed installation This is used when the registry entry has not yet
    * been propagated to the persistent registry.
    * @param componentName the unique name of the BC or SE.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    void removeComponent(String componentName)
        throws javax.jbi.JBIException
    {
        if ( null == componentName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        synchronized (mComponentCache)
        {
            mComponentCache.remove(componentName);
        }
        mRegistryUpdater.removeComponent(componentName);
        mStats.incrementRegistryDeletes();
            
    }

   /**
    * Unregister a Binding Component or a Service Engine.
    * @param componentName the unique name of the BC or SE.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    void unregisterComponent(String componentName)
        throws javax.jbi.JBIException
    {
        if ( null == componentName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }

        mLog.finer("Unregistering component " + componentName);

        synchronized (mComponentCache)
        {
            if ( mComponentCache.containsKey(componentName) )
            {
                mComponentCache.remove(componentName);
                mRegistryUpdater.removeComponent(componentName);
                mStats.incrementRegistryDeletes();
                mLog.fine("Component " + componentName + " unregistered");
            }
            else
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.CR_COMPONENT_NOT_REGISTERED, componentName);
                mLog.warning(msg);
                throw new javax.jbi.JBIException(msg);
            }
        }
    }

   /**
    * Unregister a Shared Library.
    * @param sharedLibraryName is the name of the Shared Library.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    void unregisterSharedLibrary(String sharedLibraryName)
        throws javax.jbi.JBIException
    {
        if ( null == sharedLibraryName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "sharedLibraryName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(msg);
        }
 
        mLog.fine("Unregistering Shared Library " + sharedLibraryName);

        synchronized (mSharedLibraryCache)
        {
            if ( mSharedLibraryCache.containsKey(sharedLibraryName) )
            {
                mSharedLibraryCache.remove(sharedLibraryName);
                mRegistryUpdater.removeSharedLibrary(sharedLibraryName);
                mStats.incrementRegistryDeletes();
                mLog.fine("Shared Library " + sharedLibraryName +
                    " unregistered");
            }
            else
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.CR_SL_NOT_REGISTERED, sharedLibraryName);
                mLog.warning(msg);
                throw new javax.jbi.JBIException(msg);
            }
        }
    }

//----------------------------- Private methods -------------------------------

   /**
    * Reload the registry cache from the persistent registry.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    private synchronized void reloadRegistry()
        throws javax.jbi.JBIException
    {
        // Get the persisted registry.
        // Load the Components from jbi-registry.xml
        List<String> compList = mComponentQuery.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);

        for ( String compName : compList )
        {
            ComponentInfo info = mComponentQuery.getComponentInfo(compName);

            String id = info.getName();
            switch ( info.getComponentType())
            {
                case BINDING:
                case ENGINE:
                {
                    Component comp = new Component (info);
                    mComponentCache.put(id, comp);
                    mStats.incrementRegistryAdds();
                    mLog.finer(comp.getComponentTypeAsString() + " " +
                        id + " restored to cache");
                    break;
                }
                default :
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CR_RELOAD_BAD_COMPONENT_TYPE,
                        info.getComponentType().toString(), id));
                    continue;
                }   
            }
        }
        
        List<String> slList = mComponentQuery.getComponentIds(ComponentType.SHARED_LIBRARY);

        for ( String slName : slList )
        {
            ComponentInfo slInfo = mComponentQuery.getSharedLibraryInfo(slName);

            String id = slInfo.getName();
            switch ( slInfo.getComponentType())
            {
                case SHARED_LIBRARY :
                {
                    SharedLibrary sl = new SharedLibrary(slInfo);
                    mSharedLibraryCache.put(id, sl);
                    mStats.incrementRegistryAdds();
                    mLog.finer("Shared Library " + id + " restored to cache");
                    break;
                }
                default :
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CR_RELOAD_BAD_COMPONENT_TYPE,
                        slInfo.getComponentType().toString(), id));
                    continue;
                }   
            }
        }
    }

}
