/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStartup.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * This class is used to run a thread that performs component-related tasks
 * at the beginning and end of JBI framework startup. At the beginning of the
 * startup, a thread is created to run this class with a PREPARE command, which
 * causes all shared libraries and components to be initialized. At the end of
 * the startup, a thread is created to run this class with a START command,
 * which causes all components to be brought to their desired states.
 *
 * The reason for operating this way is to prevent the initialization and
 * startup of shared libraries and components from slowing down the startup of
 * the appserver itself. This is especially important because the startup time
 * for components is completely out of the control of JBI other than the timeout
 * used to prevent a runaway component.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentStartup
    implements Runnable
{
    /**
     * ComponentFramework instance provided by the constructor.
     */
    private ComponentFramework mCompFW;

    /**
     * Startup command.
     */
    private int             mCommand;
     
    /**
     * Creates a new instance of ComponentStartup.
     * @param compFW is the ComponentFramework instance.
     * @param command is either <code>ComponentFramework.PREPARE</code> or
     * <code>ComponentFramework.STARTUP</code>.
     */
    public ComponentStartup(ComponentFramework compFW, int command)
    {
        mCompFW = compFW;
        mCommand = command;
    }

    /**
     * The run method yields control to allow the appserver thread to run;
     * when this thread regains control it will call the Component Framework
     * to do shared library/component initialization and startup processing.
     */
    public void run()
    {
        Thread.yield();
        mCompFW.startupAll(mCommand);
    }
}
