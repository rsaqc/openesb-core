/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XmlUtil.java - ver 1.1 - 01/04/2006
 *
 * Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
 */

package com.sun.jbi.framework;

import java.net.URLDecoder;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * This class provides utility methods for accessing data from XML documents.
 *
 * @author Sun Microsystems, Inc.
 */
public class XmlUtil
{
    /**
     * The StringTranslator to be used for constructing messages.
     */
    private static StringTranslator sTranslator;

    /**
     * Constant for character set for URLDecoder.
     */
    private static final String VALUE_UTF8 = "UTF-8";

    /**
     * Static initializer.
     */
    static
    {
        EnvironmentContext ec = EnvironmentContext.getInstance();
        sTranslator = (StringTranslator) ec.getStringTranslatorFor(ec);
    }

    /**
     * Get the named element from the specified node. If isRequired is true,
     * there must be exactly one element of the specified type; if no element
     * is found, an exception is thrown. If isRequired is false, and there
     * is no element of the specified type, null is returned. In either case,
     * if multiple elements are found, an exception is thrown.
     * @param node the node to be processed.
     * @param tag the type of the element to be returned.
     * @param isRequired true if the element is required, false if not.
     * @return A Node representing the requested element, or null if no element
     * was found and isRequired is false.
     * @throws com.sun.jbi.framework.XmlParseException if a required element is
     * not found or if a parse error occurs.
     */
    static Node getElement(Node node, String tag, boolean isRequired)
        throws com.sun.jbi.framework.XmlParseException
    {
        Node retNode = null;
        NodeList nodeList = getElements(node, tag, isRequired);
        int n = nodeList.getLength();
        if ( n > 1 )
        {
            throw new com.sun.jbi.framework.XmlParseException(
                sTranslator.getString(
                    LocalStringKeys.XML_EXTRA_ELEMENTS,
                    new Integer(n), tag));
        }
        if ( n > 0 )
        {
            retNode = nodeList.item(0);
        }
        return retNode;
    }

    /**
     * Get the named elements from the specified node. If isRequired is true,
     * there must be at least one element of the specified type; if no element
     * is found, an exception is thrown. If isRequired is false, and there
     * are no elements of the specified type, an empty NodeList is returned.
     *
     * @param node the node to be processed.
     * @param tag the type of the elements to be returned.
     * @param isRequired true if the element is required, false if not.
     * @return A NodeList representing the requested elements, or null if the
     * input node is null.
     * @throws com.sun.jbi.framework.XmlParseException if a required element is
     * not found or if a parse error occurs.
     */
    static NodeList getElements(Node node, String tag, boolean isRequired)
        throws com.sun.jbi.framework.XmlParseException
    {
        NodeList nodeList = null;
        if ( null != node )
        {
            try
            {
                node = node.cloneNode(true);  // Make this non-destructive
                nodeList = node.getChildNodes();
                for ( int n = 0; n < nodeList.getLength(); n++ )
                {
                    if ( !nodeList.item(n).getNodeName().equals(tag) )
                    {
                        node.removeChild(nodeList.item(n));
                        n--;  // to account for the removal of the node
                    }
                }
                nodeList = node.getChildNodes();
                if ( isRequired && 0 == nodeList.getLength() )
                {
                    throw new com.sun.jbi.framework.XmlParseException(
                        sTranslator.getString(
                            LocalStringKeys.XML_MISSING_ELEMENT,
                            tag, node.getNodeName()));
                }
            }
            catch (org.w3c.dom.DOMException domEx)
            {
                throw new com.sun.jbi.framework.XmlParseException(
                    sTranslator.getString(
                        LocalStringKeys.XML_DOM_EXCEPTION,
                        domEx.getMessage()));
            }
        }
        return nodeList;
    }

    /**
     * Get a single attribute value from a node. If the attribute is not present
     * an empty string value is returned.
     *
     * @param node the Node to be processed.
     * @param attrName the name of the attribute to be retrieved.
     * @return A String representing the value of the attribute. If the
     * attribute is not present, its value is set to an empty string.
     */
    static String getAttribute(Node node, String attrName)
    {
        if ( null == node )
        {
            throw new java.lang.IllegalArgumentException(
                sTranslator.getString(
                    LocalStringKeys.NULL_ARGUMENT, "node"));
        }
        if ( null == attrName )
        {
            throw new java.lang.IllegalArgumentException(
                sTranslator.getString(
                    LocalStringKeys.NULL_ARGUMENT, "attrName"));
        }

        String value;
        Element elem = (Element) node;
        try
        {
            value = URLDecoder.decode(elem.getAttribute(attrName), VALUE_UTF8).trim();
        }
        catch ( java.io.UnsupportedEncodingException ueEx )
        {
            value = elem.getAttribute(attrName);
        }
        return value;
    }

    /**
     * Get a boolean value from a named element in the specified node.
     * @param node the node to be processed.
     * @param tag the element to be processed.
     * @param isRequired true if the element is required, false if not.
     * @return A boolean set based on the text value from the specified element:
     * "true" returns true; "false" returns false. If the element is not present
     * and isRequired is false, returns a null.
     * @throws com.sun.jbi.framework.XmlParseException if the node list is
     * null or if any node is missing its text node.
     */
    static Boolean getBooleanValue(Node node, String tag, boolean isRequired)
        throws com.sun.jbi.framework.XmlParseException
    {
        Boolean value = null;
        NodeList nl = getElements(node, tag, isRequired);
        if ( 0 != nl.getLength() )
        {
            String s = getStringValue(nl);
            if ( null != s )
            {
                value = Boolean.valueOf(s);
            }
        }
        return value;
    }

    /**
     * Get a String value from the specified node. The node can have child nodes
     * but must have one and only one child node that is a text node containing
     * the value.
     * @param node the node to be processed.
     * @return A String containing the text value from the specified node.
     * @throws com.sun.jbi.framework.XmlParseException if the node is missing
     * its text node.
     */
    static String getStringValue(Node node)
        throws com.sun.jbi.framework.XmlParseException
    {
        // Check for null argument
        if ( null == node ) 
        {
            throw new java.lang.IllegalArgumentException(
                sTranslator.getString(
                    LocalStringKeys.NULL_ARGUMENT, "node"));
        }

        // Check for empty node
        NodeList nodes = node.getChildNodes();
        int nodeCount = nodes.getLength();
        if ( 0 == nodeCount )
        {
            throw new com.sun.jbi.framework.XmlParseException(
                sTranslator.getString(
                    LocalStringKeys.XML_EMPTY_ELEMENT,
                    node.getNodeName()));
        }

        // Get the text node
        Node textNode = null;
        for ( int n = 0; n < nodeCount; n++ )
        {
            if ( nodes.item(n).getNodeType() == Node.TEXT_NODE )
            {
                textNode = nodes.item(n);
                break;
            }
        }
        if ( null == textNode )
        {
            throw new com.sun.jbi.framework.XmlParseException(
                sTranslator.getString(
                    LocalStringKeys.XML_MISSING_ELEMENT,
                    "TEXT", node.getNodeName()));
        }
        String value;
        try
        {
            value = URLDecoder.decode(((Text) textNode).getData(), VALUE_UTF8).trim();
        }
        catch ( java.io.UnsupportedEncodingException ueEx )
        {
            value = ((Text) textNode).getData();
        }
        return value;
    }

    /**
     * Get a String value from a named element in the specified node.
     * @param node the node to be processed.
     * @param tag the element to be processed.
     * @param isRequired true if the element is required, false if not.
     * @return A String containing the text value from the specified element,
     * or null if the element is not present and isRequired is false.
     * @throws com.sun.jbi.framework.XmlParseException if the node list is
     * null or if any node is missing its text node.
     */
    static String getStringValue(Node node, String tag, boolean isRequired)
        throws com.sun.jbi.framework.XmlParseException
    {
        String value = null;
        NodeList nl = getElements(node, tag, isRequired);
        if ( 0 != nl.getLength() )
        {
            value = getStringValue(nl);
        }
        return value;
    }

    /**
     * Get a single String value from a node list. The node list is expected
     * to contain exactly one node, which in turn is expected to contain
     * exactly one text node. If the node list is null or has multiple elements,
     * an exception is thrown.
     * @param nodeList the node list to be processed.
     * @return The String value from the node.
     * @throws com.sun.jbi.framework.XmlParseException if the node list is
     * null or has multiple elements, or if the node is missing its text
     * element.
     */
    static String getStringValue(NodeList nodeList)
        throws com.sun.jbi.framework.XmlParseException
    {
        if ( null == nodeList ) 
        {
            throw new java.lang.IllegalArgumentException(
                sTranslator.getString(
                    LocalStringKeys.NULL_ARGUMENT, "nodeList"));
        }
        if ( 0 == nodeList.getLength() ) 
        {
            throw new java.lang.IllegalArgumentException(
                sTranslator.getString(
                    LocalStringKeys.EMPTY_LIST_ARGUMENT, "nodeList"));
        }
        Node node = nodeList.item(0);
        if ( 1 != nodeList.getLength() ) 
        {
            throw new com.sun.jbi.framework.XmlParseException(
                sTranslator.getString(
                    LocalStringKeys.XML_EXTRA_ELEMENTS,
                    new Integer(nodeList.getLength()), node.getNodeName()));
        }
        Node textNode = node.getFirstChild();
        if ( null == textNode )
        {
            throw new com.sun.jbi.framework.XmlParseException(
                sTranslator.getString(
                    LocalStringKeys.XML_EMPTY_ELEMENT,
                    node.getNodeName()));
        }
        if ( !(textNode instanceof Text) )
        {
            throw new com.sun.jbi.framework.XmlParseException(
                sTranslator.getString(
                    LocalStringKeys.XML_INVALID_ELEMENT,
                    node.getNodeName()));
        }
        String value;
        try
        {
            value = URLDecoder.decode(((Text) textNode).getData(), VALUE_UTF8).trim();
        }
        catch ( java.io.UnsupportedEncodingException ueEx )
        {
            value = ((Text) textNode).getData();
        }
        return value;
    }

    /**
     * Get a list of String values from a named element in the specified node.
     * @param node the node to be processed.
     * @param tag the element to be processed.
     * @param isRequired true if the element is required, false if not.
     * @return A list of Strings containing the text values from the specified
     * elements. The list is empty if no elements were found and isRequired is
     * false.
     * @throws com.sun.jbi.framework.XmlParseException if the node list is
     * null or if any node is missing its text node, or if a required element
     * is not found.
     */
    static List<String> getStringValueArray(Node node, String tag,
        boolean isRequired)
        throws com.sun.jbi.framework.XmlParseException
    {
        NodeList nl = getElements(node, tag, isRequired);
        if ( (null == nl) && !isRequired )
        {
            return new ArrayList();
        }
        else
        {
            return getStringValueArray(nl);
        }
    }

    /**
     * Get a list of String values from a named element in the specified node.
     * @param node the node to be processed.
     * @param tag1 the parent element to be processed.
     * @param tag2 the child element to be processed.
     * @param isRequired true if the element is required, false if not.
     * @return A list of Strings containing the text values from the specified
     * elements. The list is empty if no elements were found and isRequired is
     * false.
     * @throws com.sun.jbi.framework.XmlParseException if the node list is
     * null or if any node is missing its text node, or if a required element
     * is not found.
     */
    static List<String> getStringValueArray(Node node, String tag1, String tag2,
        boolean isRequired)
        throws com.sun.jbi.framework.XmlParseException
    {
        NodeList nl = getElements(node, tag1, isRequired);
        if ( nl.getLength() > 1 )
        {
            throw new com.sun.jbi.framework.XmlParseException(
                sTranslator.getString(
                    LocalStringKeys.XML_EXTRA_ELEMENTS,
                    new Integer(nl.getLength()), tag1));
        }
        NodeList nl2 = getElements(nl.item(0), tag2, isRequired);
        if ( (null == nl2) && !isRequired )
        {
            return new ArrayList();
        }
        else
        {
            return getStringValueArray(nl2);
        }
    }

    /**
     * Get a list of String values from a node list. The node list can have
     * any number of nodes, each of which must have a single Text node. For
     * example, given the following XML fragment defined by a node list:
     *
     * <PRE><CODE>
     *  &lt;sharedNamespaceId&gt;shared-namespace-1&lt;/sharedNamespaceId&gt;
     *  &lt;sharedNamespaceId&gt;shared-namespace-2&lt;/sharedNamespaceId&gt;
     *  &lt;sharedNamespaceId&gt;shared-namespace-3&lt;/sharedNamespaceId&gt;
     * </CODE></PRE>
     *
     * The returned ArrayList for this node list would contain three String
     * instances with values "shared-namespace-1", "shared-namespace-2", and
     * "shared-namespace-3".
     *
     * @param nodeList the node list to be processed.
     * @return An array list containing String values from the Text nodes, or
     * null if there were none.
     * @throws java.lang.IllegalArgumentException if the node list is null.
     * @throws com.sun.jbi.framework.XmlParseException if any node in the
     * list is missing a text node.
     */
    static List<String> getStringValueArray(NodeList nodeList)
        throws com.sun.jbi.framework.XmlParseException
    {
        if ( null == nodeList ) 
        {
            throw new java.lang.IllegalArgumentException(
                sTranslator.getString(
                    LocalStringKeys.NULL_ARGUMENT, "nodeList"));
        }

        ArrayList elements = new ArrayList();
        for ( int i = 0; i < nodeList.getLength(); ++i )
        {
            Node node = nodeList.item(i);
            Node textNode = node.getFirstChild();
            if ( null == textNode )
            {
                throw new com.sun.jbi.framework.XmlParseException(
                    sTranslator.getString(
                        LocalStringKeys.XML_EMPTY_ELEMENT,
                        node.getNodeName()));
            }
            if ( !(textNode instanceof Text) )
            {
                throw new com.sun.jbi.framework.XmlParseException(
                    sTranslator.getString(
                        LocalStringKeys.XML_INVALID_ELEMENT,
                        node.getNodeName()));
            }
            String value;
            try
            {
                value = URLDecoder.decode(((Text) textNode).getData(), VALUE_UTF8).trim();
            }
            catch ( java.io.UnsupportedEncodingException ueEx )
            {
                value = ((Text) textNode).getData();
            }
            elements.add(value);
        }
        return elements;
    }
}
