/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitOperation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import javax.jbi.component.ServiceUnitManager;

/**
 * This class extends the Operation abstract class, and is designed to run on a
 * separate thread. The main purpose of this class is to provide a way to run
 * operations on Service Units with a timeout to prevent a hang if the call to
 * the component's ServiceUnitManager fails to return 
 * 
 * To use this class, create an instance for the Service Unit, providing the
 * appropriate Service Unit Manager, operation type, and operation arguments to
 * the constructor. Then create a thread for the instance and call its start()
 * method.
 *
 * If the parent thread needs to wait for the operation to complete, it can call
 * wait() on the instance of this class. Optionally, the wait() call can have a
 * a timeout parameter to limit the amount of time to wait for completion of the
 * operation.
 *
 * @author Sun Microsystems, Inc.
 */
class ServiceUnitOperation extends Operation
{
    /**
     * The JBI environment context.
     */
    private EnvironmentContext mEnv;

    /**
     * The type of operation to be performed.
     */
    private int mOperation;

    /**
     * The name of the component.
     */
    private String mComponentName;

    /**
     * The ServiceUnitManager for the component.
     */
    private ServiceUnitManager mSuManager;

    /**
     * The message string translator.
     */
    private StringTranslator mTranslator;

    /**
     * Value for mOperation for a deploy operation.
     */
    public static final int DEPLOY = 1;

    /**
     * Value for mOperation for an init operation.
     */
    public static final int INIT = 2;

    /**
     * Value for mOperation for a start operation.
     */
    public static final int START = 3;

    /**
     * Value for mOperation for a stop operation.
     */
    public static final int STOP = 4;

    /**
     * Value for mOperation for a shutdown operation.
     */
    public static final int SHUTDOWN = 5;

    /**
     * Value for mOperation for an undeploy operation.
     */
    public static final int UNDEPLOY = 6;

    /**
     * Constructor.
     *
     * @param counter the OperationCounter for synchronization of the operation.
     * @param compName the name of the component.
     * @param suManager the ServiceUnitManager instance for the component.
     * @param operation the type of operation to be performed, either DEPLOY,
     * INIT, START, STOP, SHUTDOWN, or UNDEPLOY.
     * @param args array of arguments for the operation.
     */
    ServiceUnitOperation(OperationCounter counter,
                         String compName,
                         ServiceUnitManager suManager,
                         int operation,
                         Object args[])
    {
        super(counter, args);

        mComponentName = compName;
        mSuManager = suManager;
        mEnv = EnvironmentContext.getInstance();
        mTranslator = (StringTranslator) mEnv.getStringTranslatorFor(this);

        if ( null == suManager )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "suManager"));
        }

        if ( DEPLOY == operation
            || INIT == operation
            || START == operation
            || STOP == operation
            || SHUTDOWN == operation
            || UNDEPLOY == operation )
        {
            mOperation = operation;
        }
        else
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.INVALID_ARGUMENT,
                "operation", new Integer(operation)));
        }
    }

    /**
     * Call the ServiceUnitManager to perform the operation on the Service Unit.
     *
     * @param argumentList - the arguments for the operation provided to the
     * constructor.
     * @return the returned string from operations that return a string or null
     * for operations that have no return value.
     * @throws Throwable if any error occurs.
     */
    Object process(Object argumentList[]) throws Throwable
    {
        String returnValue = null;

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try
        {
            Thread.currentThread().setContextClassLoader(
                ClassLoaderFactory.getInstance().getComponentClassLoader(
                    mComponentName));
            switch ( mOperation )
            {
            case DEPLOY:
                returnValue = mSuManager.deploy((String) argumentList[0],
                    (String) argumentList[1]);
                break;
            case INIT:
                mSuManager.init((String) argumentList[0],
                    (String) argumentList[1]);
                break;
            case START:
                mSuManager.start((String) argumentList[0]);
                break;
            case STOP:
                mSuManager.stop((String) argumentList[0]);
                break;
            case SHUTDOWN:
                mSuManager.shutDown((String) argumentList[0]);
                break;
            case UNDEPLOY:
                returnValue = mSuManager.undeploy((String) argumentList[0],
                    (String) argumentList[1]);
                break;
            default:
                break;
            }
        }
        finally
        {
            Thread.currentThread().setContextClassLoader(cl);
        }

        return (Object) returnValue;
    }

    /**
     * Reset the instance with a new operation. Values must be either DEPLOY,
     * INIT, START, STOP, SHUTDOWN, or UNDEPLOY. This method reinitializes the
     * instance to reuse for another operation.
     *
     * @param operation the type of operation to be performed, either DEPLOY,
     * INIT, START, STOP, SHUTDOWN, or UNDEPLOY.
     */
    void resetOperation(int operation)
    {
        if ( DEPLOY == operation
            || INIT == operation
            || START == operation
            || STOP == operation
            || SHUTDOWN == operation
            || UNDEPLOY == operation )
        {
            mOperation = operation;
            super.reset();
        }
        else
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.INVALID_ARGUMENT,
                "operation", new Integer(operation)));
        }
    }

}
