/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentFramework.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.framework.DeployerMBean;
import com.sun.jbi.component.InstallationContext;
import com.sun.jbi.framework.ComponentLifeCycleMBean;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.messaging.MessageService;

import java.io.File;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.component.Bootstrap;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;

/**
 * This is the implementation of the Component Framework, which provides
 * for the installation, uninstallation, and runtime management of Binding
 * Components, Service Engines, and Shared Libraries within the JBI framework.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentFramework
    implements com.sun.jbi.ComponentManager, com.sun.jbi.ServiceLifecycle
{
    /**
     * Local handle to EnvironmentContext
     */
    private EnvironmentContext mContext;

    /**
     * Local handle to ClassLoaderFactory
     */
    private ClassLoaderFactory mClassLoaderFactory;

    /**
     * Local handle to ComponentRegistry
     */
    private ComponentRegistry mCompReg;

    /**
     * Local handle to ServiceUnitFramework
     */
    private ServiceUnitFramework mSUFramework;

    /**
     * Local handle to Logger
     */
    private Logger mLog;

    /**
     * Local flag for start/stop checking
     */
    private Boolean mStarted = null;

    /**
     * Local handle to StringTranslator
     */
    private StringTranslator mTranslator;

    /**
     * Local handle to registry updater.
     */
    private Updater mUpdater;

    /**
     * Localized string "Binding"
     */
    private String mBinding;

    /**
     * Localized string "component"
     */
    private String mComponent;

    /**
     * Localized string "Engine"
     */
    private String mEngine;

    /**
     * Last known state.
     */
    private int mStartupState;
    
    /**
     * Commands for startupAll()
     */
    public static final int PREPARE = 1;
    public static final int START = 2;
    
    /**
     * Default timeout interval for component startup and shutdown. The
     * value is in milliseconds and is set to 4 minutes.
     */
    private static final long TIMEOUT_DEFAULT = 240000;
    
// ------------------------- ServiceLifecycle methods -------------------------

    /**
     * Initialize the Component Framework service.
     * @param context The JBI environment context created by the JBI framework.
     * @throws javax.jbi.JBIException If an error occurs during initialization.
     */
    public void initService(com.sun.jbi.EnvironmentContext context)
        throws javax.jbi.JBIException
    {
        if ( null != mStarted )
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.SERVICE_ALREADY_INITIALIZED,
                    mTranslator.getString(LocalStringKeys.CF_NAME)));
        }
        mContext = (EnvironmentContext) context;
        mCompReg = (ComponentRegistry) mContext.getComponentRegistry();
        mClassLoaderFactory = ClassLoaderFactory.getInstance();
        mSUFramework = new ServiceUnitFramework(mContext);
        mLog = mContext.getLogger();
        mTranslator = (StringTranslator) mContext.getStringTranslatorFor(this);
        mBinding = mTranslator.getString(LocalStringKeys.BINDING);
        mComponent = mTranslator.getString(LocalStringKeys.COMPONENT);
        mEngine = mTranslator.getString(LocalStringKeys.ENGINE);

        mStarted = mStarted.FALSE;
        mLog.fine(mTranslator.getString(LocalStringKeys.SERVICE_INITIALIZED,
            mTranslator.getString(LocalStringKeys.CF_NAME)));
    }

    /**
     * Start the Component Framework service.
     * @throws javax.jbi.JBIException If an error occurs starting the service.
     */
    public void startService()
        throws javax.jbi.JBIException
    {
        if ( mStarted.booleanValue() )
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.SERVICE_ALREADY_STARTED,
                    mTranslator.getString(LocalStringKeys.CR_NAME)));
        }

        // Indicate that the Component Framework is now stared.

        mStarted = mStarted.TRUE;

        mLog.fine(mTranslator.getString(LocalStringKeys.SERVICE_STARTED,
            mTranslator.getString(LocalStringKeys.CF_NAME)));
    }

    /**
     * Stop the Component Framework service. All BCs and SEs that are still
     * started are stopped and shut down here.
     * @throws javax.jbi.JBIException If an error occurs stopping the service.
     */
    public void stopService()
        throws javax.jbi.JBIException
    {
        if ( !mStarted.booleanValue() )
        {
            mLog.warning(mTranslator.getString(
                LocalStringKeys.SERVICE_ALREADY_STOPPED,
                    mTranslator.getString(LocalStringKeys.CF_NAME)));
            return;
        }
        mStarted = mStarted.FALSE;
        long timeOut = mContext.getComponentTimeout();

        // Get a list of all installed bindings and engines and shut them down.

        List comps = mCompReg.getAllComponents();
        ListIterator c = comps.listIterator();
        Component comp = null;
        mLog.fine(mTranslator.getString(LocalStringKeys.CF_STOPPING_ALL));
        ArrayList shutdowns = new ArrayList();
        ComponentOperation shutdown;
        OperationCounter oc = new OperationCounter();
        while ( c.hasNext() )
        {
            comp = (Component) c.next();
            if ( comp.isStarted() || comp.isStopped() )
            {
                shutdown =
                    new ComponentOperation(oc, comp, ComponentOperation.SHUTDOWN);
                shutdowns.add(shutdown);
                new Thread(shutdown, comp.getName()).start();
            }
        }
        if ( 0 < shutdowns.size() )
        {
            synchronized (oc)
            {
                try
                {
                    if ( 0 < oc.getValue() )
                    {
                        oc.wait(timeOut);
                    }
                }
                catch ( java.lang.InterruptedException iEx )
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_SHUTDOWN_INTERRUPTED));
                }
            }
            Iterator shutdownIter = shutdowns.iterator();
            while ( shutdownIter.hasNext() )
            {
                shutdown = (ComponentOperation) shutdownIter.next();
                if ( !shutdown.completed() )
                {
                    comp = shutdown.getComponent();
                    shutdown.getThread().interrupt();
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_COMP_SHUTDOWN_TIMEOUT,
                        comp.getComponentTypeAsString(),
                        comp.getName(), new Long(timeOut)));
                }
            }
        }

        mLog.fine(mTranslator.getString(LocalStringKeys.SERVICE_STOPPED,
            mTranslator.getString(LocalStringKeys.CF_NAME)));
    }

// ---------------------------- Public IPI methods ----------------------------

   /**
    * Get the instance of javax.jbi.component.Component for the specified
    * component, if it exists.
    * @param componentName the unique name of the component.
    * @return the javax.jbi.component.Component instance or null if no such
    * component exists or if the component is inactive.
    */
    public javax.jbi.component.Component getComponentInstance(String componentName)
    {
        Component comp = mCompReg.getComponent(componentName);
        if ( null != comp )
        {
            return comp.getComponentInstance();
        }
        else
        {
            return null;
        }
    }

   /**
    * Get the instance of com.sun.jbi.framework.DeployerMBean for the specified
    * component, if it exists.
    * @param componentName the unique name of the component.
    * @return the com.sun.jbi.framework.DeployerMBean instance or null if no
    * such component exists or if the component has no Deployer instance.
    */
    public DeployerMBean getDeployerInstance(String componentName)
    {
        DeployerMBean deployer = null;
        Component comp = mCompReg.getComponent(componentName);
        if ( null != comp )
        {
            deployer = comp.getDeployerInstance();
        }
        return deployer;
    }

    /**
     * Install a Shared Library into the JBI framework.
     * @param name The unique name assigned to this Shared Library.
     * @param description The description of the Shared Library.
     * @param componentRoot The root directory for the Shared Library.
     * @param isSelfFirst Set to true to force the class loader for this Shared
     * Library to use a self-first hierarchy, or false for parent-first.
     * @param elements The list of jar and class files included in this
     * Shared Library, in a List of Strings in the order in which they
     * should appear in the class path.
     * @throws javax.jbi.JBIException If the installation fails.
     */
    public synchronized void installSharedLibrary(
        String name,
        String description,
        String componentRoot,
        boolean isSelfFirst,
        List elements)
        throws javax.jbi.JBIException
    {
        // Check the parameters for validity. All parameters are required
        // and values must not be null.

        if ( null == name )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "name"));
        }

        if ( 0 == name.length() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "name"));
        }

        if ( null == componentRoot )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentRoot"));
        }

        if ( 0 == componentRoot.length() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "componentRoot"));
        }

        if ( null == elements )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "elements"));
        }

        if ( 0 == elements.size() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_LIST_ARGUMENT, "elements"));
        }

        // Be sure that there is not already a Shared Library registered with
        // the same name.

        if ( mCompReg.isSharedLibraryRegistered(name) )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_SL_ALREADY_REGISTERED, name));
        }

        // Create a new SharedLibrary instance.

        SharedLibrary sl = new SharedLibrary(name,
                                             description,
                                             componentRoot,
                                             isSelfFirst,
                                             elements);

        // Create the shared class loader for this Shared Library
        try
        {
            mClassLoaderFactory.createSharedClassLoader(sl);
            mLog.fine("Created class loader for Shared Library " + name +
                " with class path " + sl.getClassPathAsString());
        }
        catch ( javax.jbi.JBIException jbiEx )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_SL_INSTALL_FAILED,
                name), jbiEx);
        }

        // Register the Shared Library
        mCompReg.registerSharedLibrary(sl);

        String installMsg = mTranslator.getString(
            LocalStringKeys.CF_SL_INSTALL_SUCCESSFUL, name);
        mLog.info(installMsg);

        // Send an MBean notification that the install has completed.
        mContext.getNotifier().emitComponentNotification(
            EventNotifier.EventType.Installed, 
            EventNotifier.SourceType.SharedLibrary,
            name, installMsg);
    }

    /**
     * Load a component's bootstrap into the JBI framework. This is the first
     * step in the installation and uninstallation processes. It creates and
     * registers the ComponentInstaller MBean that is used to complete the
     * installation or uninstallation.
     * @param installContext The InstallationContext created by the Installer
     * Service.
     * @param bootClassName The name of the bootstrap class for the component.
     * @param bootClassPathElements A List of elements comprising the
     * class path for loading the bootstrap class. Each element is a String
     * containing the full path to either a jar file or a directory containing
     * class files.
     * @param sharedLibraryNames A List of String objects containing the names
     * of the Shared Libraries required by the component.
     * @param force Set to <code>true</code> if this is for an uninstall force.
     * In this case, a failure to load the bootstrap does not prevent the
     * creation of the installer MBean.
     * @return The MBean object name of an installation configuration MBean or
     * null if the component didn't provide one.
     * @throws javax.jbi.JBIException If the component's bootstrap cannot be
     * loaded.
     */
    public synchronized ObjectName loadBootstrap(
        InstallationContext installContext,
        String bootClassName,
        List bootClassPathElements,
        List sharedLibraryNames,
        boolean force)
        throws javax.jbi.JBIException
    {
        // Check the parameters for validity. All parameters are required
        // and values must not be null. The installContext must contain
        // valid information.

        if ( null == installContext )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "installContext"));
        }
        if ( null == bootClassName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "bootClassName"));
        }
        if ( null == bootClassPathElements )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "bootClassPathElements"));
        }
        if ( 0 == bootClassName.length() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "bootClassName"));
        }
        if ( 0 == bootClassPathElements.size() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_LIST_ARGUMENT, "bootClassPathElements"));
        }

        String msg = null;
        String componentName = installContext.getComponentName();
        String componentType = installContext.isBinding() ? mBinding : mEngine;

        mLog.fine("Loading bootstrap for " + componentType +
            " " + componentName + " with force=" + force);

        // Get the Component represented by the component name from the registry
        // if it exists. The registry returns null if there is none.

        Component comp = mCompReg.getComponent(componentName);
        boolean existed = false;

        if ( installContext.isInstall() )
        {
            msg = mTranslator.getString(
                LocalStringKeys.CF_COMP_INSTALL_FAILED,
                componentType, componentName) + " ";

            // This is an installation. If no Component instance exists, create
            // a new one. If a Component instance exists and is in any state
            // other than LOADED, abort the installation.

            if ( null == comp )
            {
                comp = new Component();
            }
            else if ( !comp.isLoaded() )
            {
                throw new javax.jbi.JBIException(msg + mTranslator.getString(
                    LocalStringKeys.CF_COMP_ALREADY_REGISTERED,
                    comp.getComponentTypeAsString()));
            }
            else
            {
                existed = true;
                comp.clearBootstrapInstance();
                comp.clearLifeCycleInstance();
            }

            // Initialize the Component instance for this component.

            initComponent(comp, installContext, bootClassName,
                bootClassPathElements, sharedLibraryNames);

            // Check for required Shared Libraries. If any of them are not
            // installed, abort the installation.

            checkSharedLibraries(comp);
        }
        else
        {
            msg = mTranslator.getString(
                LocalStringKeys.CF_COMP_UNINSTALL_FAILED,
                componentType, componentName) + " ";

            // This is an uninstall. Verify that the component is installed.

            if ( null == comp || comp.isLoaded() )
            {
                throw new javax.jbi.JBIException(msg + mTranslator.getString(
                    LocalStringKeys.CF_COMP_NOT_INSTALLED));
            }
        }

        // Load and initialize the bootstrap instance, and register the
        // InstallerMBean for it.

        ObjectName instMBeanName = initBootstrap(comp, msg, installContext, force);

        // For an installation, the component must be registered to prevent
        // another installation from attempting to install another component
        // with the same component name.

        if ( installContext.isInstall() )
        {
            if ( !existed )
            {
                mCompReg.registerComponent(comp);
            }
        }

        mLog.fine("Bootstrap for " + componentType + " " + componentName +
            " has been loaded");
        return instMBeanName;
    }

    /**
     * Uninstall a Shared Library from the JBI framework. A Shared Library
     * cannot be uninstalled until all dependent components have been shut down.
     * If any running dependent components are found, the uninstall of the
     * Shared Library is aborted.
     * @param sharedLibraryName The unique name of the Shared Library.
     * @throws javax.jbi.JBIException If the uninstall fails or is denied.
     */
    public void uninstallSharedLibrary(String sharedLibraryName)
        throws javax.jbi.JBIException
    {
        // Be sure that there is a Shared Library registered with this name.

        SharedLibrary sl = mCompReg.getSharedLibrary(sharedLibraryName);
        if ( null == sl )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_SL_UNINSTALL_NONEXISTENT,
                sharedLibraryName));
        }

        // First check to be sure there are no running components that
        // reference this Shared Library. If any are found, abort the
        // uninstall. The string returned by checkForActiveDependents()
        // is used only when there are no running components that reference
        // this Shared Library, for providing a list of components that
        // cannot be started until this Shared Library is reinstalled.

        String dependents;
        try
        {
            dependents = checkForActiveDependents(sharedLibraryName);
        }
        catch ( javax.jbi.JBIException ex )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_SL_UNINSTALL_FAILED_DEPENDENTS,
                sharedLibraryName), ex);
        }

        // Destroy the class loader for this Shared Library.

        try
        {
            mClassLoaderFactory.removeSharedClassLoader(sharedLibraryName);
        }
        catch ( javax.jbi.JBIException ex )
        {
            // This only happens if the class loader did not exist, and
            // can be ignored during uninstall. Just log the error.

            mLog.info(mTranslator.getString(
                LocalStringKeys.CF_SL_UNINSTALL_CLASSLOADER, ex.toString()));
        }

        // Remove this Shared Library from the registry.

        mCompReg.unregisterSharedLibrary(sharedLibraryName);

        // Construct an appropriate messaage based on whether or not there are
        // dependent components. Log the message and send an MBean notification
        // of the event.
        String uninstallMsg;
        if ( dependents.length() > 0 )
        {
            uninstallMsg = mTranslator.getString(
                LocalStringKeys.CF_SL_UNINSTALL_OK_DEPENDENTS,
                sharedLibraryName, dependents);
        }
        else
        {
            uninstallMsg = mTranslator.getString(
                LocalStringKeys.CF_SL_UNINSTALL_SUCCESSFUL, sharedLibraryName);
        }
        mLog.info(uninstallMsg);
        mContext.getNotifier().emitComponentNotification(
            EventNotifier.EventType.Uninstalled, 
            EventNotifier.SourceType.SharedLibrary,
            sharedLibraryName, uninstallMsg);

        // If there are any components installed that require this Shared
        // Library, throw a warning exception to inform the user.

        if ( dependents.length() > 0 )
        {
            throw new com.sun.jbi.framework.FrameworkWarningException(
                uninstallMsg);
        }
    }

   /**
    * Unload a component's bootstrap from the JBI framework. This is the last
    * step in the installation and uninstallation processes. It unregisters
    * the InstallerMBean that was used to complete the installation or
    * uninstallation, and calls the bootstrap cleanUp() method if it has not
    * yet been called. Note that in the case where this method is called
    * after loadBootstrap() for a new component, without an intervening call to
    * installComponent(), the Component instance is removed here.
    * @param componentName The unique name of the component.
    * @throws javax.jbi.JBIException if the bootstrap cannot be unloaded.
    */
    public void unloadBootstrap(
        String componentName)
        throws javax.jbi.JBIException
    {
        if ( null == componentName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName"));
        }
        if ( 0 == componentName.length() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "componentName"));
        }

        // Get the Component instance for this component. If none exists,
        // there is nothing to do here, as this means that uninstallComponent()
        // completed and cleaned up everything.

        Component comp = mCompReg.getComponent(componentName);
        if ( null == comp )
        {
            mLog.finer("Component " + componentName +
                " is not present in the component cache, nothing to unload");
            return;
        }

        // Unregister the InstallerMBean for this component, if one is still
        // registered. Any failure is logged but doesn't throw an exception.
        
        MBeanServer mbs = mContext.getMBeanServer();
        ObjectName instMBeanName = comp.getInstallerMBeanName();
        if ( null != mbs && null != instMBeanName )
        {
            try
            {
                if ( mbs.isRegistered(instMBeanName) )
                {
                    mLog.fine("Unregistering installer MBean" + instMBeanName);
                    mbs.unregisterMBean(instMBeanName);
                    comp.setInstallerMBeanName(null);
                }
            }
            catch ( javax.management.InstanceNotFoundException infEx )
            {
                mLog.warning(infEx.getMessage());
                ; // This can be ignored.
            }
            catch ( javax.management.MBeanRegistrationException mbrEx )
            {
                mLog.warning(mbrEx.getMessage());
                ; // This can be ignored.
            }
        }

        // Get the bootstrap class instance that was loaded by the
        // loadBootstrap() method, if present. If found, and its cleanUp()
        // method has not yet been called, call it here.

        Bootstrap bootInstance = comp.getBootstrapInstance(false);
                          
        if ( null != bootInstance )
        {
            try
            {
                if ( comp.isBootstrapCleanUpNeeded() )
                {
                    bootInstance.cleanUp();
                }
            }
            catch ( Throwable ex )
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.CF_BOOTSTRAP_UNLOAD_CLEANUP_FAILED,
                    comp.getComponentTypeAsString(), componentName,
                    ex.getClass().getName()) + getExMsg(ex);
                mLog.warning(msg);
                mLog.warning(mTranslator.stackTraceToString(ex));
                throw new javax.jbi.JBIException(msg, ex);
            }
            finally
            {
                comp.clearBootstrapInstance();
                comp.setBootstrapCleanUpNeeded(false);
            }
        }

        // If the component is in LOADED state, this means that the component
        // is not installed, so the Component instance can be removed from
        // the Component Registry.

        if ( comp.isLoaded() )
        {
            mCompReg.removeComponent(componentName);
        }
    }
    
    /**
     * Cancel a pending component upgrade. This is called when some failure
     * has occurred after <code>validateComponentForUpgrade()</code> has already
     * been called but before <code>upgradeComponent()</code> is called. All
     * this method does is return the component to the <code>SHUTDOWN</code>
     * state and remove the busy indicator from the component entry in the
     * runtime registry cache.
     * @param componentName The name of the component.
     * @throws javax.jbi.JBIException if the component does not exist.
     */
    public synchronized void cancelComponentUpgrade(
        String componentName)
        throws javax.jbi.JBIException
    {
        if ( null == componentName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentName"));
        }
        if ( 0 == componentName.length() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "componentName"));
        }

        Component comp = mCompReg.getComponent(componentName);
        if ( null == comp )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_CANCEL_NOT_FOUND,
                componentName));
        }

        // Clear the component busy and component updating indicators.
        comp.clearUpdating();
    }
    
    /**
     * Upgrade an installed component. This is used to upgrade a component to a
     * newer version without requiring the component to be uninstalled (which
     * requires undeployment of all Service Assemblies that have Service Units
     * deployed to the component). There are two ways a component can be updated.
     * If the component does not provide an <code>upgrade()</code> method in its
     * bootstrap class, then the runtime jar files are updated and any changes
     * to the component's installation descriptor in jbi.xml are propagated. If
     * the component provides an <code>upgrade()</code> method, that method is
     * called to give the component the opportunity to upgrade its workspace
     * and all SUs deployed to it to a new version. In this case, it is the
     * responsibility of the component to perform all version verification, and
     * to provide any recovery processing required in the event of a failed
     * upgrade.
     *
     * @param installContext The InstallationContext created by the Installer
     * Service.
     * @param bootClassName The name of the bootstrap class for the component.
     * @param bootClassPathElements A List of elements comprising the
     * class path for loading the bootstrap class. Each element is a String
     * containing the full path to either a jar file or a directory containing
     * class files.
     * @param sharedLibraryNames A List of String objects containing the names
     * of the Shared Libraries required by the component.
     * @throws javax.jbi.JBIException if the component cannot be updated for
     * any reason.
     * @throws javax.jbi.JBIException if the update fails for some reason.
     */
    public synchronized void upgradeComponent(
        InstallationContext installContext,
        String bootClassName,
        List<String> bootClassPathElements,
        List<String> sharedLibraryNames)
        throws javax.jbi.JBIException
    {
        // Check the parameters for validity. All parameters are required
        // and values must not be null.

        if ( null == installContext )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "installContext"));
        }
        if ( null == bootClassName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "bootClassName"));
        }
        if ( null == bootClassPathElements )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "bootClassPathElements"));
        }
        if ( 0 == bootClassName.length() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "bootClassName"));
        }
        if ( 0 == bootClassPathElements.size() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_LIST_ARGUMENT, "bootClassPathElements"));
        }

        String componentName = installContext.getComponentName();
        String componentType = installContext.isBinding() ? mBinding : mEngine;

        // Get the Component represented by the component name from the registry
        // if it exists. The component must already exist in the registry. If it
        // is not found, abort the request.

        Component comp = mCompReg.getComponent(componentName);
        if ( null == comp )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_NOT_FOUND,
                componentType, componentName));
        }

        // Destroy any existing class loader for this component to allow the
        // upgrade to proceed.

        comp.clearLifeCycleInstance();

        // Make sure the component updating flag is set. If it is not, abort
        // the update, as this means the component has not been validated for
        // update.

        if ( !comp.isUpdating() )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_NOT_VALIDATED,
                componentType, componentName));
        }

        // Create a new Component instance for the upgrade so that if the
        // upgrade fails, the existing Component instance can remain unchanged.

        Component newComp = new Component();
        initComponent(newComp, installContext, bootClassName,
            bootClassPathElements, sharedLibraryNames);
        newComp.setUpdating();

        // Verify that all required Shared Libraries are installed.

        checkSharedLibraries(newComp);

        // Load the bootstrap class and call the component's upgrade method
        // if it provided one.

        javax.jbi.component.Bootstrap bootInstance;
        try
        {
            bootInstance = newComp.getBootstrapInstance(true);
        }
        catch ( javax.jbi.JBIException ex )
        {
            comp.clearUpdating();
            String msg = mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_LOAD_FAILED,
                componentType, componentName, ex.getMessage());
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg, ex);
        }
        String msgKey = null;
        try
        {
            java.lang.reflect.Method upgrade = getUpgradeMethod(bootInstance);
            if ( null != upgrade )
            {
                List<ServiceUnitInfo> suList = comp.getServiceUnitList();
                ArrayList<String> suRoots = new ArrayList(0);
                for ( ServiceUnitInfo su : suList )
                {
                    suRoots.add(su.getFilePath());
                }
                Object[] params = { newComp.getWorkspaceRoot(), suRoots };
                upgrade.invoke(bootInstance, params);
                msgKey = LocalStringKeys.CF_COMP_UPGRADE_COMPLETE;
            }
            else
            {
                msgKey = LocalStringKeys.CF_COMP_UPDATE_COMPLETE;
            }
        }
        catch ( java.lang.reflect.InvocationTargetException itEx )
        {
            Throwable compEx = itEx.getTargetException();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_FAILED,
                componentType, componentName, compEx.getClass().getName(),
                compEx.getMessage()), compEx);
        }
        catch ( java.lang.IllegalAccessException iaEx )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_ILLEGAL_ACCESS,
                componentType, componentName);
            mLog.log(Level.WARNING, msg, iaEx);
            throw new javax.jbi.JBIException(msg, iaEx);
        }
        finally
        {
            comp.clearUpdating();
            newComp.clearBootstrapInstance();
        }

        // Now that the upgrade has completed, update the Component instance
        // with the information for the new version.

        initComponent(comp, installContext, bootClassName,
            bootClassPathElements, sharedLibraryNames);
        comp.setShutdown();
        comp.clearLifeCycleInstance();

        // Check for any new pre-defined loggers and add them to the Logger
        // MBean for the component.
        comp.getLoggerInstance().getLoggerDefs(true);

        // Construct an appropriate message an log it, then send an MBean
        // notification of the upgrade.
        String installMsg = mTranslator.getString(msgKey, componentType,
            componentName);
        mLog.info(installMsg);
        mContext.getNotifier().emitComponentNotification(
            EventNotifier.EventType.Upgraded, 
            (comp.isBinding() ?
                EventNotifier.SourceType.BindingComponent :
                EventNotifier.SourceType.ServiceEngine),
            componentName, installMsg);
    }
    
    /**
     * Validate a component for upgrade. This validates that the runtime jar
     * files provided for the upgrade contain valid implementations of the
     * required interfaces and the component classes will load correctly. This
     * also verifies that all Shared Library dependencies are met. It also
     * determines whether or not the component has provided an implementation
     * of the <code>upgrade()</code> method and returns an indicator of that to
     * the caller.
     *
     * @param installContext The InstallationContext created by the Installer
     * Service.
     * @param bootClassName The name of the bootstrap class for the component.
     * @param bootClassPathElements A List of elements comprising the
     * class path for loading the bootstrap class. Each element is a String
     * containing the full path to either a jar file or a directory containing
     * class files.
     * @param sharedLibraryNames A List of String objects containing the names
     * of the Shared Libraries required by the component.
     * @return <code>true</code> if the component provided an <code>upgrade()
     * </code> method in its bootstrap implementation, <code>false</code> if it
     * did not.
     * @throws javax.jbi.JBIException if the component cannot be updated for
     * any reason.
     */
    public synchronized boolean validateComponentForUpgrade(
        InstallationContext installContext,
        String bootClassName,
        List bootClassPathElements,
        List sharedLibraryNames)
        throws javax.jbi.JBIException
    {
        // Check the parameters for validity. All parameters are required
        // and values must not be null.

        if ( null == installContext )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "installContext"));
        }
        if ( null == bootClassName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "bootClassName"));
        }
        if ( 0 == bootClassName.length() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "bootClassName"));
        }
        if ( null == bootClassPathElements )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "bootClassPathElements"));
        }
        if ( 0 == bootClassPathElements.size() )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_LIST_ARGUMENT, "bootClassPathElements"));
        }

        String componentName = installContext.getComponentName();
        String componentType = installContext.isBinding() ? mBinding : mEngine;

        // Get the Component represented by the component name from the registry
        // if it exists. The component must already exist in the registry. If it
        // is not found, abort the request.

        Component comp = mCompReg.getComponent(componentName);
        if ( null == comp )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_NOT_FOUND,
                componentType, componentName));
        }

        // Set the component updating flag to indicate that the validation call
        // has been made. This in turn marks the component busy so that no life
        // cycle operations can be performed on it. If the component is already
        // busy, the call to set the updating flag will throw an exception and
        // the validation will fail.

        comp.setUpdating();

        mLog.fine("Validating " + componentType + " " + componentName +
            " for upgrade");

        // The component must be in shutdown state to be updated. If it is not,
        // abort the request.

        if ( !comp.isShutDown() )
        {
            comp.clearUpdating();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_NOT_SHUT_DOWN,
                componentType, componentName));
        }

        // Validate the component using the temporary install root where it has
        // been unzipped by the Installation Service. Create a new Component
        // instance for performing the validation, and update all fields that
        // could have been changed in the component's installation descriptor
        // (jbi.xml). Set the "updating" flag in this instance so that the
        // Shared Library dependency check knows this is for an upgrade.

        Component newComp = new Component();
        initComponent(newComp, installContext, bootClassName,
            bootClassPathElements, sharedLibraryNames);
        newComp.setUpdating();

        // Verify that all required Shared Libraries are installed.

        checkSharedLibraries(newComp);

        // Verify that the bootstrap and life cycle classes can be loaded and 
        // are valid. Determine whether or not the component supports upgrade.
        javax.jbi.component.Bootstrap bootInstance;
        javax.jbi.component.ComponentLifeCycle lifeInstance;
        boolean supportsUpgrade = false;
        try
        {
            bootInstance = newComp.getBootstrapInstance(true);
            lifeInstance = newComp.getLifeCycleInstance(true);
            if ( null != getUpgradeMethod(bootInstance) )
            {
                supportsUpgrade = true;
            }
        }
        catch ( javax.jbi.JBIException ex )
        {
            comp.clearUpdating();
            mLog.warning(mTranslator.getString(
                LocalStringKeys.CF_COMP_UPGRADE_VALIDATE_FAILED,
                componentType, componentName));
            mLog.warning(ex.getMessage());
            throw ex;
        }
        finally
        {
            newComp.clearBootstrapInstance();
            newComp.clearLifeCycleInstance();
        }

        return supportsUpgrade;
    }
    
// ------------------------- Package-Visible methods --------------------------

    /**
     * Get the JMX ObjectName of the InstallerConfigurationMBean for a component.
     * @param componentName the unique name of the component.
     * @return The JMX ObjectName of the InstallerConfigurationMBean.
     * @throws javax.jbi.JBIException If the component is not found.
     */
    ObjectName getInstallerConfigurationMBeanName(String componentName)
        throws javax.jbi.JBIException
    {
        Component comp = mCompReg.getComponent(componentName);
        if ( null == comp )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_COMP_NOT_FOUND,
                componentName));
        }

        return comp.getExtensionMBeanName();
    }

    /**
     * Get the logger for this service.
     * @return The Logger instance.
     */
    Logger getLogger()
    {
        return mLog;
    }

    /**
     * Initialize a Component.
     * @param comp The Component instance.
     * @throws javax.jbi.JBIException If the initialization failed.
     */
    void initializeComponent(Component comp)
        throws javax.jbi.JBIException
    {
        javax.jbi.component.ComponentLifeCycle compInstance =
            comp.getLifeCycleInstance(true);
        initializeComponent(comp, compInstance, false);
    }

    /**
     * Initialize a Component.
     * @param comp The Component instance for this component.
     * @param compInstance The LifeCycle instance for this component.
     * @throws javax.jbi.JBIException If the initialization failed.
     */
    void initializeComponent(Component comp,
        javax.jbi.component.ComponentLifeCycle compInstance, boolean fromClient)
        throws javax.jbi.JBIException
    {
        mLog.fine("Initializing " + comp.getComponentTypeAsString() +
            " " + comp.getName());

        // Create the ComponentContext for this component.
        
        ComponentContext context = new ComponentContext(comp, mContext);
        comp.setContext(context);
        context.getDeliveryChannel(true);
                
        // Call the life cycle class init() method. A failure at this
        // point fails the originating command.

        ComponentStatistics stats =
            comp.getStatisticsInstance();
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(
            ClassLoaderFactory.getInstance().getComponentClassLoader(
                comp.getName()));
        try
        {
            if ( stats.isEnabled() )
            {
                stats.incrementInitRequests();
            }
            compInstance.init(context);
        }
        catch ( Throwable ex )
        {
            if ( stats.isEnabled() )
            {
                stats.incrementFailedRequests();
            }
            String msg;
            if ( fromClient )
            {
                msg = mTranslator.getString(
                    LocalStringKeys.CF_COMP_INITIALIZATION_FAILED_EXCEPTION,
                    comp.getComponentTypeAsString(), comp.getName(),
                    ex.getClass().getName());
            }
            else
            {
                msg = mTranslator.getString(
                    LocalStringKeys.CF_COMP_INITIALIZATION_EXCEPTION,
                    comp.getComponentTypeAsString(), comp.getName(),
                    ex.getClass().getName()) + getExMsg(ex);
            }
            mLog.log(Level.WARNING, msg, ex);
            throw new javax.jbi.JBIException(msg, ex);
        }
        finally
        {
            Thread.currentThread().setContextClassLoader(cl);
        }
        comp.setStopped();
        comp.getDeployerInstance().setServiceUnitManager(
            comp.getServiceUnitManager());
    }

    /**
     * Install a Component into the JBI framework.
     * @param installContext The InstallationContext created by the
     * Installer Service.
     * @return The MBean object name of the LifeCycle MBean for this component.
     * @throws javax.jbi.JBIException If the installation fails.
     */
    synchronized ObjectName installComponent(
        InstallationContext installContext)
        throws javax.jbi.JBIException
    {
        // Make sure an InstallationContext is provided.

        if ( null == installContext )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "installContext"));
        }

        String componentName = installContext.getComponentName();
        String componentType = installContext.isBinding() ? mBinding : mEngine;

        String msg = mTranslator.getString(
            LocalStringKeys.CF_COMP_INSTALL_FAILED,
            componentType, componentName) + " ";
               
        mLog.fine("Installing " + componentType + " " + componentName);

        // A Component instance representing this component must be in the
        // Component Registry in order to proceed. If one is not found, it
        // means that the loadBootstrap() method either was not called or did
        // not complete successfully. Also, the state of the component must be
        // "loaded". If it is in any other state the installation fails.

        Component comp = mCompReg.getComponent(componentName);
        if ( null == comp )
        {
            throw new javax.jbi.JBIException(msg + mTranslator.getString(
                LocalStringKeys.CF_COMP_NOT_LOADED));
        }
        else
        {
            if ( !comp.isLoaded() )
            {
                throw new javax.jbi.JBIException(msg + mTranslator.getString(
                    LocalStringKeys.CF_COMP_ALREADY_INSTALLED));
            }
        }

        // Get the bootstrap instance and call its onInstall() method.

        doInstall(comp, msg, installContext);

        // Verify that the life cycle class can be loaded. This must be done
        // after calling the bootstrap onInstall() method so that if the
        // bootstrap modified the component's runtime class path, those changes
        // will be picked up when the component's class loader is created.
        // After verifying, remove the class loader and life cycle class
        // reference.

        javax.jbi.component.ComponentLifeCycle lifeInstance;
        try
        {
            lifeInstance = comp.getLifeCycleInstance(true);
        }
        catch ( javax.jbi.JBIException ex )
        {
            comp.clearLifeCycleInstance();
            mCompReg.removeComponent(componentName);
            mLog.warning(msg + ex.getMessage());
            throw new javax.jbi.JBIException(msg, ex);
        }

        // Create and register the LifeCycleMBean for this component.

        ObjectName lifeMBeanName = null;
        ComponentLifeCycle lifeMBean = new ComponentLifeCycle(comp, this);
        try
        {
            lifeMBeanName = registerMBean(comp, lifeMBean,
                ComponentLifeCycleMBean.class,
                MBeanNames.CONTROL_TYPE_LIFECYCLE);
        }
        catch ( javax.jbi.JBIException ex )
        {
            comp.clearLifeCycleInstance();
            mCompReg.removeComponent(componentName);
            throw new javax.jbi.JBIException(msg + mTranslator.getString(
                LocalStringKeys.CF_LIFECYCLE_MBEAN_REGISTRATION_FAILED),
                ex);
        }
        comp.setLifeCycleMBeanName(lifeMBeanName);
        
        // Installation was successful, permanently register the component in
        // the persistent registry and sent an MBean notification of the event.

        comp.setShutdown();
        mCompReg.commitComponent(componentName);

        String installMsg = mTranslator.getString(
            LocalStringKeys.CF_COMP_INSTALL_SUCCESSFUL,
            componentType, componentName); 
        mLog.info(installMsg);
        mContext.getNotifier().emitComponentNotification(
            EventNotifier.EventType.Installed, 
            (comp.isBinding() ?
                EventNotifier.SourceType.BindingComponent :
                EventNotifier.SourceType.ServiceEngine),
            componentName, installMsg);

        // Create and register the ComponentStatisticsMBean for this component.
        // If this fails, do not fail the install, just log the error.

        try
        {
            createStatisticsMBean(comp);
        }
        catch ( javax.jbi.JBIException ex )
        {
            mLog.warning(ex.getMessage());
        }

        // Create and register the DeployerMBean for this component. If this
        // fails, do not fail the install, just log the error.

        try
        {
            createDeployerMBean(comp);
        }
        catch ( javax.jbi.JBIException ex )
        {
            mLog.warning(ex.getMessage());
        }

        // Create and register the LoggerMBean for this component. If this
        // fails, do not fail the install, just log the error.

        try
        {
            createLoggerMBean(comp);
        }
        catch ( javax.jbi.JBIException ex )
        {
            mLog.warning(ex.getMessage());
        }

        // Create and register the ConfigurationMBean for this component. If
        // this fails, do not fail the install, just log the error.

        try
        {
            createConfigurationMBean(comp);
        }
        catch ( javax.jbi.JBIException ex )
        {
            mLog.warning(ex.getMessage());
        }        
        
        // Clear the lifecycle instance.
        comp.clearLifeCycleInstance();

        return lifeMBeanName;
    }
    
    /**
     * This method is called by the JBI framework at the very beginning of its 
     * startup so that components can create any local XAResources that might
     * be needed for recovery. If recovery is not enabled this phase will not be
     * called. We wait for the prepare phase to complete before
     * continuing. This method creats a new thread to run the
     * <code>ComponentStartup</code> class, and starts the thread. The
     *<code>run()</code> method of <code>ComponentStartup</code> will call
     * <code>startupAll()</code> with the <code>PREPARE</code> option to perform
     * the tasks required for components' XAResource recovery to proceed.
     */
    void prepare()
    {
        startupAll(PREPARE);
    }

    /**
     * This method is called by the JBI framework at the very end of its starup
     * processing to kick off the processing of component startup after any optional
     * XAResource recovery processing has been completed. When the <code>wait
     * </code> argument is set to <code>true</code>, the <code>startupAll()
     * </code> method is called directly with the <code>START</code> option to
     * perform automatic component startup processing. When the <code>wait</code>
     * argument is set to <code>false</code>, this method creates a new thread
     * to run the <code>ComponentStartup</code> class, and starts the thread.
     * The <code>run()</code> method of <code>ComponentStartup</code> will call
     * <code>startupAll()</code> with the <code>START</code> option to perform
     * the automatic component startup processing.
     *
     * @param wait when set to <code>true</code>, perform startup processing on
     * the current thread; when set to <code>false</code>, perform startup
     * processing on a new thread.
     */
    void ready(boolean wait)
    {
        if (wait)
        {
            startupAll(START);
        }
        else
        {
            ComponentStartup cs = new ComponentStartup(this, START);
            Thread t = new Thread(cs, "ComponentStartup");
            t.start();
        }
    }

    /**
     * Perform automatic startup processing. This is where all installed Shared
     * Libraries have their class loaders set up, and all components are brought
     * to the desired state, based on the ComponentStartup property setting.
     */
    void startupAll(int command)
    {
        // Get a list of all installed bindings and engines.

        List compList = mCompReg.getAllComponents();

        // If this is PREPARE or this is START and PREPARED wasn't seen.
        if (command == PREPARE || (mStartupState == 0 && command == START))
        {   
            mStartupState = command;
            
            // Get a list of all installed Shared Libraries, and recreate their
            // class loaders.

            List sLibList = mCompReg.getAllSharedLibraries();
            ListIterator s = sLibList.listIterator();
            SharedLibrary sl = null;
            while ( s.hasNext() )
            {
                sl = (SharedLibrary) s.next();
                try
                {
                    mClassLoaderFactory.createSharedClassLoader(sl);
                }
                catch (javax.jbi.JBIException jbiEx)
                {
                    mLog.severe(mTranslator.getString(
                        LocalStringKeys.CF_SL_CLASSLOADER_CREATION_FAILED,
                        sl.getName(),
                        jbiEx.getMessage()));
                    continue;
                }
            }

            // Load the life cycle class and create a new instance for each
            // component in the list. Any component for which a failure occurs
            // is removed from the list to exclude it from further processing.

            createAllInstances(compList);

            // Create and register the life cycle MBeans for each component in
            // the list. Any component for which a failure occurs is removed
            // from the list to exclude it from further processing.

            createAllLifeCycleMBeans(compList);

            // Create and register the remaining MBeans for each component in
            // the list. Failures here do not prevent the component from being
            // started.

            createAllOtherMBeans(compList);
        }
        
        if (command == PREPARE)
        {
            // Components must be initialized during the prepare phase so that
            // any local XAResources used by components can be registered with
            // the runtime framework.

            prepareComponents(compList);
        }
        else
        {
            // Components that need to be started are started here during the
            // start phase. The prepare phase may or may not have run, but any
            // uninitialized components are initialized automatically.

            startComponents(compList);
        }
    }

    /**
     * Shut down a Component.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If the shutdown fails.
     */
    void shutdownComponent(Component comp)
        throws javax.jbi.JBIException
    {
        shutdownComponent(comp, false, false);
    }

    /**
     * Shut down a Component.
     * @param comp The Component instance representing the component.
     * @param force Set to true to force the shutdown even if the component's
     * shutDown method fails.
     * @param fromClient Set to true when the call originated from a JMX client.
     * @throws javax.jbi.JBIException If the shutdown fails.
     */
    void shutdownComponent(Component comp, boolean force, boolean fromClient)
        throws javax.jbi.JBIException
    {
        String componentName = comp.getName();
        String componentType = comp.getComponentTypeAsString();
        javax.jbi.component.ComponentLifeCycle compInstance =
            comp.getLifeCycleInstance(false);
        ComponentStatistics stats = comp.getStatisticsInstance();

        if ( comp.isStarted() )
        {
            // Stop all Service Units deployed to this component.
            mSUFramework.stopServiceUnits(comp);

            // Call the life cycle class stop() method. A failure at this
            // point fails the shutdown command, unless force was specified.
            String stopMsg = mTranslator.getString(
                LocalStringKeys.CF_COMP_STOP_SUCCESSFUL,
                componentType, componentName);
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            try
            {
                mLog.fine("Stopping " + componentType + " " + componentName);
                if ( stats.isEnabled() )
                {
                    stats.incrementStopRequests();
                }
                Thread.currentThread().setContextClassLoader(
                    ClassLoaderFactory.getInstance().getComponentClassLoader(
                        comp.getName()));
                compInstance.stop();
                mLog.info(stopMsg);
            }
            catch ( Throwable ex )
            {
                if ( stats.isEnabled() )
                {
                    stats.incrementFailedRequests();
                }
                String msg;
                if ( fromClient )
                {
                    msg = mTranslator.getString(
                        LocalStringKeys.CF_LIFECYCLE_STOP_EXCEPTION,
                        componentType, componentName, ex.getClass().getName());
                }
                else
                {
                    msg = mTranslator.getString(
                        LocalStringKeys.CF_LIFECYCLE_STOP_FAILED,
                        componentType, componentName,
                        ex.getClass().getName()) + getExMsg(ex);
                }
                mLog.log(Level.WARNING, msg, ex);
                if ( !force )
                {
                    throw new javax.jbi.JBIException(msg, ex);
                }
            }
            finally
            {
                Thread.currentThread().setContextClassLoader(cl);
            }
            comp.setStopped();
            mContext.getNotifier().emitComponentNotification(
                EventNotifier.EventType.Stopped, 
                (comp.isBinding() ?
                    EventNotifier.SourceType.BindingComponent :
                    EventNotifier.SourceType.ServiceEngine),
                componentName, stopMsg);
        }
        if ( comp.isStopped() )
        {
            // Shut down all Service Units. Errors here are just logged and do 
            // not prevent the shutdown processing from proceeding.
            mSUFramework.shutDownServiceUnits(comp);

            // Call the life cycle class shutdown() method. A failure at this
            // point fails the shutdown command.
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            try
            {
                mLog.fine("Shutting down " + componentType + " " + componentName);
                if ( stats.isEnabled() )
                {
                    stats.incrementShutDownRequests();
                }
                Thread.currentThread().setContextClassLoader(
                    ClassLoaderFactory.getInstance().getComponentClassLoader(
                        comp.getName()));
                compInstance.shutDown();
            }
            catch ( Throwable ex )
            {
                if ( stats.isEnabled() )
                {
                    stats.incrementFailedRequests();
                }
                String msg;
                if ( fromClient )
                {
                    msg = mTranslator.getString(
                        LocalStringKeys.CF_LIFECYCLE_SHUTDOWN_EXCEPTION,
                        componentType, componentName, ex.getClass().getName());
                }
                else
                {
                    msg = mTranslator.getString(
                        LocalStringKeys.CF_LIFECYCLE_SHUTDOWN_FAILED,
                        componentType, componentName,
                        ex.getClass().getName()) + getExMsg(ex);
                }
                mLog.log(Level.WARNING, msg, ex);
                if ( !force )
                {
                    throw new javax.jbi.JBIException(msg, ex);
                }
            }
            finally
            {
                Thread.currentThread().setContextClassLoader(cl);
            }
 
            // Reset the component to shutdown (installed) state, clear its
            // instance references, remove its class loader, close its NMR
            // channel if one is present, and clear its context.

            comp.setShutdown();
            comp.clearLifeCycleInstance();
            ComponentContext ctx = comp.getContext();
            if ( null != ctx )
            {
                javax.jbi.messaging.DeliveryChannel dc =
                    ctx.getDeliveryChannel(false);
                if ( null != dc )
                {
                    dc.close();
                }
                comp.setContext(null);
            }

            // Log a message that the component was shut down and send an
            // MBean notification of the event.

            String shutDownMsg;
            if ( !force )
            {
                shutDownMsg = mTranslator.getString(
                    LocalStringKeys.CF_COMP_SHUTDOWN_SUCCESSFUL,
                    componentType, componentName);
            }
            else
            {
                shutDownMsg = mTranslator.getString(
                    LocalStringKeys.CF_COMP_SHUTDOWN_FORCED,
                    componentType, componentName);
            }
            mLog.info(shutDownMsg);
            mContext.getNotifier().emitComponentNotification(
                EventNotifier.EventType.ShutDown, 
                (comp.isBinding() ?
                    EventNotifier.SourceType.BindingComponent :
                    EventNotifier.SourceType.ServiceEngine),
                componentName, shutDownMsg);
        }
        else
        {
            if ( comp.isLoaded() )
            {
                throw new javax.jbi.JBIException(mTranslator.getString(
                    LocalStringKeys.CF_COMP_CANNOT_SHUTDOWN,
                    componentType, componentName));
            }
            else
            {
                mLog.fine("Shutdown of " + componentType + " " + componentName +
                    " ignored, already shut down");
            }
        }
    }

    /**
     * Start a Component.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If the component cannot be started.
     */
    void startComponent(Component comp)
        throws javax.jbi.JBIException
    {
        startComponent(comp, false);
    }

    /**
     * Start a Component.
     * @param comp The Component instance representing the component.
     * @param startSUs When <code>true</code>, also brings deployed SUs to
     * their desired states. This is set only when called from an admin client
     * so this flag is also used to determine whether exceptions from the
     * component are handled locally.
     * @throws javax.jbi.JBIException If the component cannot be started.
     */
    void startComponent(Component comp, boolean startSUs)
        throws javax.jbi.JBIException
    {
        boolean fromClient = startSUs;
        String componentName = comp.getName();
        String componentType = comp.getComponentTypeAsString();

        if ( comp.isLoaded() )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_COMP_CANNOT_START,
                componentType, componentName));
        }

        ComponentStatistics stats = comp.getStatisticsInstance();

        if ( !comp.isStarted() )
        {
            // Check to be sure all required Shared Libraries are installed.
            // If any are missing, the start fails.
            checkSharedLibraries(comp);

            javax.jbi.component.ComponentLifeCycle compInstance;

            compInstance = comp.getLifeCycleInstance(true);

            mLog.fine("Starting " + componentType + " " + componentName);

            if ( !comp.isInitialized() )
            {
                initializeComponent(comp, compInstance, fromClient);
            }

            // Call the life cycle class start() method. A failure at this
            // point fails the start command.
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            try
            {
                if ( stats.isEnabled() )
                {
                    stats.incrementStartRequests();
                }
                Thread.currentThread().setContextClassLoader(
                    ClassLoaderFactory.getInstance().getComponentClassLoader(
                        comp.getName()));
                compInstance.start();
            }
            catch ( Throwable ex )
            {
                if ( stats.isEnabled() )
                {
                    stats.incrementFailedRequests();
                }
                String msg;
                if ( fromClient )
                {
                    msg = mTranslator.getString(
                        LocalStringKeys.CF_LIFECYCLE_START_EXCEPTION,
                        componentType, componentName, ex.getClass().getName());
                }
                else
                {
                    msg = mTranslator.getString(
                        LocalStringKeys.CF_LIFECYCLE_START_FAILED,
                        componentType, componentName,
                        ex.getClass().getName()) + getExMsg(ex);
                }
                mLog.log(Level.WARNING, msg, ex);
                throw new javax.jbi.JBIException(msg, ex);
            }
            finally
            {
                Thread.currentThread().setContextClassLoader(cl);
            }

            // The component is now started. Save the restart time and set the
            // state. Send an MBean notification about the event.

            stats.setLastRestartTime(new Date());
            comp.setStarted();

            String startMsg = mTranslator.getString(
                LocalStringKeys.CF_COMP_START_SUCCESSFUL,
                componentType, componentName);
            mLog.info(startMsg);

            mContext.getNotifier().emitComponentNotification(
                EventNotifier.EventType.Started, 
                (comp.isBinding() ?
                    EventNotifier.SourceType.BindingComponent :
                    EventNotifier.SourceType.ServiceEngine),
                componentName, startMsg);

            // Check the Observer flag for this component. If it is set to
            // true, set up the component as an NMR observer.

            if ( comp.isObserver() )
            {
                mContext.getNormalizedMessageService().addObserver(
                    (com.sun.jbi.messaging.DeliveryChannel)
                    comp.getContext().getDeliveryChannel());
            }
            
            // If requested, initialize and start all of the SUs deployed to
            // the component.

            if ( startSUs )
            {
                mSUFramework.initializeServiceUnits(comp);
                mSUFramework.startServiceUnits(comp);
            }
        }
        else
        {
            mLog.fine("Start of " + componentType + " " + componentName +
                " ignored, already started");
        }
    }

    /**
     * Stop a Component.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If the component cannot be stopped.
     */
    void stopComponent(Component comp)
        throws javax.jbi.JBIException
    {
        String componentName = comp.getName();
        String componentType = comp.getComponentTypeAsString();
        javax.jbi.component.ComponentLifeCycle compInstance =
            comp.getLifeCycleInstance(false);
        ComponentStatistics stats = comp.getStatisticsInstance();

        if ( comp.isStarted() )
        {
            mLog.fine("Stopping " + componentType + " " + componentName);

            ComponentContext ctx = comp.getContext();
            if ( comp.isObserver() && null != ctx )
            {
                mContext.getNormalizedMessageService().removeObserver(
                    (com.sun.jbi.messaging.DeliveryChannel)
                    ctx.getDeliveryChannel());
            }
            
            mSUFramework.stopServiceUnits(comp);

            // Call the life cycle class stop() method. A failure at this
            // point fails the stop command.
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            try
            {
                if ( stats.isEnabled() )
                {
                    stats.incrementStopRequests();
                }
                Thread.currentThread().setContextClassLoader(
                    ClassLoaderFactory.getInstance().getComponentClassLoader(
                        comp.getName()));
                compInstance.stop();
            }
            catch ( Throwable ex )
            {
                if ( stats.isEnabled() )
                {
                    stats.incrementFailedRequests();
                }
                String msg = mTranslator.getString(
                    LocalStringKeys.CF_LIFECYCLE_STOP_EXCEPTION,
                    componentType, componentName,
                    ex.getClass().getName());
                throw new javax.jbi.JBIException(msg, ex);
            }
            finally
            {
                Thread.currentThread().setContextClassLoader(cl);
            }

            // The component is now stopped. Set the state and send an MBean
            // notification about the event.

            comp.setStopped();
            String stopMsg = mTranslator.getString(
                LocalStringKeys.CF_COMP_STOP_SUCCESSFUL,
                componentType, componentName);
            mLog.info(stopMsg);
            mContext.getNotifier().emitComponentNotification(
                EventNotifier.EventType.Stopped, 
                (comp.isBinding() ?
                    EventNotifier.SourceType.BindingComponent :
                    EventNotifier.SourceType.ServiceEngine),
                componentName, stopMsg);
        }
        else
        {
            if ( comp.isLoaded() )
            {
                throw new javax.jbi.JBIException(mTranslator.getString(
                    LocalStringKeys.CF_COMP_CANNOT_STOP,
                    componentType, componentName));
            }
            else if ( comp.isStopped() )
            {
                mLog.fine("Stop of " + componentType + " " + componentName +
                    " ignored, already stopped");
            }
            else // comp.isShutDown()
            {
                throw new javax.jbi.JBIException(mTranslator.getString(
                    LocalStringKeys.CF_COMP_STOP_NOT_ACTIVE,
                    componentType, componentName));
            }
        }
    }

    /**
     * Uninstall a Component from the JBI framework.
     * @param installContext The <code>InstallationContext</code> created by the
     * Installation Service.
     * @param force Set to <code>true</code> to proceed with the uninstall
     * regardless of any errors encountered.
     * @throws javax.jbi.JBIException If the uninstall fails.
     */
    synchronized void uninstallComponent(InstallationContext installContext,
        boolean force)
        throws javax.jbi.JBIException
    {
        // Check for null arguments.

        if ( null == installContext )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "installContext"));
        }

        String componentName = installContext.getComponentName();
        String componentType = installContext.isBinding() ? mBinding : mEngine;
        String msg = mTranslator.getString(
            LocalStringKeys.CF_COMP_UNINSTALL_FAILED,
            componentType, componentName) + " ";

        mLog.fine("Uninstalling " + componentType + " " + componentName);

        // Get the Component instance for this component.

        Component comp = mCompReg.getComponent(componentName);
        if ( null == comp )
        {
            throw new javax.jbi.JBIException(msg + mTranslator.getString(
                LocalStringKeys.CF_COMP_NO_COMP_INSTALLED, componentType));
        }

        // If the component is still running, deny the request to uninstall.

        if ( comp.isStarted() )
        {
            throw new javax.jbi.JBIException(msg + mTranslator.getString(
                LocalStringKeys.CF_COMP_UNINSTALL_RUNNING, componentType));
        }
        if ( comp.isStopped() )
        {
            throw new javax.jbi.JBIException(msg + mTranslator.getString(
                LocalStringKeys.CF_COMP_UNINSTALL_STOPPED, componentType));
        }

        // If the component has any Service Units still deployed to it, deny
        // the request to uninstall.

        List suList = comp.getServiceUnitList();
        if ( !suList.isEmpty() )
        {
            int i = 0;
            StringBuffer suNames = new StringBuffer();
            Iterator sui = suList.iterator();
            while ( sui.hasNext() )
            {
                if ( suNames.length() > 0 )
                {
                    suNames.append(", ");
                }
                suNames.append(((ServiceUnit) sui.next()).getName());
            }
            msg = mTranslator.getString(
                LocalStringKeys.CF_COMP_UNINSTALL_FAILED_HAS_SUS,
                componentType, componentName, suNames);
            mLog.warning(msg);
            throw new javax.jbi.JBIException(msg);
        }

        // Get the bootstrap instance and call its onUninstall() method.

        com.sun.jbi.framework.FrameworkWarningException warning = null;
        try
        {
            doUninstall(comp, msg);
        }
        catch ( com.sun.jbi.framework.FrameworkWarningException warnEx )
        {
            if ( !force )
            {
                warning = warnEx;
            }
        }
        catch ( javax.jbi.JBIException ex )
        {
            if ( !force )
            {
                throw ex;
            }
        }

        // Unregister the component's runtime MBeans and mark the component
        // back to LOADED state so that unloadBoostrap() will remove it.

        unregisterMBeans(comp);
        comp.setLoaded();

        String uninstallMsg = mTranslator.getString(
            LocalStringKeys.CF_COMP_UNINSTALL_SUCCESSFUL,
            componentType, componentName);
        mLog.info(uninstallMsg);
        mContext.getNotifier().emitComponentNotification(
            EventNotifier.EventType.Uninstalled, 
            (comp.isBinding() ?
                EventNotifier.SourceType.BindingComponent :
                EventNotifier.SourceType.ServiceEngine),
            componentName, uninstallMsg);

        if ( null != warning )
        {
            throw warning;
        }
    }


// ----------------------------- Private Methods -----------------------------

    /**
     * Check for running dependent components that reference a SharedLibrary.
     * If any are found, throw an exception that lists the running dependent
     * components (dependent components that are not running are not listed).
     * If no running dependent components are found, the return value is a
     * string that contains the component type and name of each dependent
     * component, regardless of its status. This can be use to notify the
     * user that these components cannot be started until the Shared Library
     * has been reinstalled.
     * @param sharedLibraryName The unique name of the Shared Library.
     * @return A string containing a comma-separated list of Components that
     * depend upon this Shared Library.
     * @throws javax.jbi.JBIException If there are dependent components.
     */
    private String checkForActiveDependents(String sharedLibraryName)
        throws javax.jbi.JBIException
    {
        mLog.finer("Checking for components dependent upon Shared Library " +
            sharedLibraryName);

        boolean foundActiveDependents = false;
        StringBuffer actList = new StringBuffer();
        StringBuffer depList = new StringBuffer();
        List comps = mCompReg.getDependents(sharedLibraryName);
        ListIterator c = comps.listIterator();
        Component comp = null;
        while ( c.hasNext() )
        {
            comp = (Component) c.next();

            // Add all dependents to the return string
            if ( depList.length() > 0 )
            {
                depList.append(", ");
            }
            depList.append(comp.getComponentTypeAsString());
            depList.append(" ");
            depList.append(comp.getName());

            // If the dependent is not active, skip to the next one
            if ( comp.isLoaded() || comp.isShutDown() )
            {
                continue;
            }
            mLog.warning(mTranslator.getString(
                LocalStringKeys.CF_SL_REFERENCED_BY,
                sharedLibraryName,
                comp.getComponentTypeAsString(),
                comp.getName()));
            // Add active dependent to the message string
            if ( actList.length() > 0 )
            {
                actList.append(", ");
            }
            actList.append(comp.getComponentTypeAsString());
            actList.append(" ");
            actList.append(comp.getName());
            foundActiveDependents = true;
        }

        if ( foundActiveDependents )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_SL_REFERENCED_BY_LIST, actList));
        }

        return depList.toString();
    }

    /**
     * Check to be sure that all Shared Libraries required by this component
     * are already registered. Construct a list of SharedLibrary IDs that
     * are missing and post it in an exception message. The message issued is
     * based on whether the component is being installed or started, which is
     * determined by checking the component's state. If the component is in
     * LOADED state, it is being installed; otherwise, it is being started.
     * @param component The Component instance for this BC or SE.
     * @throws javax.jbi.JBIException If any of the required Shared Libraries
     * are not already registered.
     */
    private void checkSharedLibraries(
        Component component)
        throws javax.jbi.JBIException
    {
        // Check for required Shared Libraries. If any of them are not
        // installed, abort the installation.

        boolean missingSlib = false;

        List slNameList = component.getSharedLibraryNames();
        if ( null != slNameList )
        {
            ListIterator slNameIter = slNameList.listIterator();
            StringBuffer slNameMissing = new StringBuffer();
            while ( slNameIter.hasNext() )
            {
                String slName = (String) slNameIter.next();
                if ( !mCompReg.isSharedLibraryRegistered(slName) )
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_COMP_MISSING_SHARED_LIBRARY,
                        slName));
                    if ( slNameMissing.length() > 0 )
                    {
                        slNameMissing.append(", ");
                    }
                    slNameMissing.append(slName);
                    missingSlib = true;
                }
            }
            if ( missingSlib )
            {
                if ( component.isUpdating() )
                {
                    throw new javax.jbi.JBIException(mTranslator.getString(
                        LocalStringKeys.CF_COMP_UPGRADE_MISSING_SHARED_LIBRARIES,
                        component.getComponentTypeAsString(),
                        component.getName(), slNameMissing));
                }
                else if ( component.isLoaded() )
                {
                    throw new javax.jbi.JBIException(mTranslator.getString(
                        LocalStringKeys.CF_COMP_MISSING_SHARED_LIBRARY_LIST,
                        component.getComponentTypeAsString(),
                        slNameMissing));
                }
                else
                {
                    throw new javax.jbi.JBIException(mTranslator.getString(
                        LocalStringKeys.CF_COMP_MISSING_SHARED_LIBRARIES_START,
                        component.getComponentTypeAsString(),
                        slNameMissing));
                }
            }
        }
    }

    /**
     * Create new instances of all installed components with a desired state
     * other than SHUTDOWN. This causes the class loader for each component
     * to be created, the life cycle class to be loaded, and a new instance
     * of that class to be created. Any component for which an exception is
     * encountered is removed from the list so that it is not processed further.
     *
     * @param compList The list of installed components.
     */
    private void createAllInstances(List compList)
    {
        // Load the life cycle class and create a new instance for each
        // component in the list with a desired state other than SHUTDOWN.
        // If an exception occurs at any point, log it.

        ListIterator c = compList.listIterator();
        Component comp = null;

        while ( c.hasNext() )
        {
            comp = (Component) c.next();
            if ( ComponentState.SHUTDOWN != comp.getDesiredState() )
            {
                mLog.fine("Loading life cycle for " +
                    comp.getComponentTypeAsString() + " " + comp.getName());
                try
                {
                    Object compInstance = comp.getLifeCycleInstance(true);
                }
                catch ( javax.jbi.JBIException jbiEx )
                {
                    c.remove();
                    mLog.warning(jbiEx.getMessage());
                }
            }
        }
    }

    /**
     * Create lifecycle MBeans for all loaded components. Any component for
     * which the lifecycle MBean cannot be created is removed from the list so
     * that it is not processed further. Without a lifecycle MBean, a component
     * cannot be operated upon.
     * @param compList The list of installed components.
     */
    private void createAllLifeCycleMBeans(List compList)
    {
        // Create and register lifecycle MBeans for each component in the
        // list. If an exception occurs at any point, log it.

        ListIterator c = compList.listIterator();
        Component comp = null;

        while ( c.hasNext() )
        {
            comp = (Component) c.next();
            mLog.finer("Creating LifeCycle MBean for " +
                comp.getComponentTypeAsString() + " " + comp.getName());
            try
            {
                createLifeCycleMBean(comp);
            }
            catch (javax.jbi.JBIException jbiEx)
            {
                c.remove();
                mLog.warning(jbiEx.getMessage());
            }
        }
    }

    /**
     * Create other MBeans for all loaded components. Failures here are just
     * logged, as they do not prevent the components from operating.
     * @param compList The list of installed components.
     */
    private void createAllOtherMBeans(List compList)
    {
        // Create and register all other MBeans for each component in the
        // list. If an exception occurs at any point, log it.

        ListIterator c = compList.listIterator();
        Component comp = null;

        while ( c.hasNext() )
        {
            comp = (Component) c.next();
            mLog.finer("Creating Statistics MBean for " +
                comp.getComponentTypeAsString() + " " + comp.getName());
            try
            {
                createStatisticsMBean(comp);
            }
            catch (javax.jbi.JBIException jbiEx)
            {
                mLog.warning(jbiEx.getMessage());
            }
            mLog.finer("Creating Deployer MBean for " +
                comp.getComponentTypeAsString() + " " + comp.getName());
            try
            {
                createDeployerMBean(comp);
            }
            catch (javax.jbi.JBIException jbiEx)
            {
                mLog.warning(jbiEx.getMessage());
            }
            mLog.finer("Creating Logger MBean for " +
                comp.getComponentTypeAsString() + " " + comp.getName());
            try
            {
                createLoggerMBean(comp);
            }
            catch (javax.jbi.JBIException jbiEx)
            {
                mLog.warning(jbiEx.getMessage());
            }
            mLog.finer("Creating Configuration MBean for " +
                comp.getComponentTypeAsString() + " " + comp.getName());
            try
            {
                createConfigurationMBean(comp);
            }
            catch (javax.jbi.JBIException jbiEx)
            {
                mLog.warning(jbiEx.getMessage());
            }
        }
    }

    /**
     * Create and register a DeployerMBean for a component that supports
     * deployment of Service Units.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If a failure occurs.
     */
    private void createDeployerMBean(Component comp)
        throws javax.jbi.JBIException
    {
        ObjectName deployerMBeanName = null;
        Deployer deployer = new Deployer(comp);
        comp.setDeployerInstance(deployer);
        try
        {
            deployerMBeanName = registerMBean(
                comp, deployer, DeployerMBean.class,
                MBeanNames.CONTROL_TYPE_DEPLOYER);
        }
        catch ( javax.jbi.JBIException ex )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_DEPLOYER_MBEAN_REGISTRATION_FAILED,
                comp.getComponentTypeAsString(), comp.getName()),
                ex);
        }
        comp.setDeployerMBeanName(deployerMBeanName);
    }

    /**
     * Create and register a LifeCycleMBean for a component.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If a failure occurs.
     */
    void createLifeCycleMBean(Component comp)
        throws javax.jbi.JBIException
    {
        ObjectName lifeMBeanName = null;
        ComponentLifeCycle lifeMBean = new ComponentLifeCycle(comp, this);
        try
        {
            lifeMBeanName = registerMBean(
                comp, lifeMBean, ComponentLifeCycleMBean.class,
                MBeanNames.CONTROL_TYPE_LIFECYCLE);
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_LIFECYCLE_MBEAN_NOT_REGISTERED,
                comp.getComponentTypeAsString(), comp.getName(),
                jbiEx.getMessage()));
        }
        comp.setLifeCycleMBeanName(lifeMBeanName);
    }

    /**
     * Create and register the ComponentLoggerMBean for the component.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If a failure occurs.
     */
    private void createLoggerMBean(Component comp)
        throws javax.jbi.JBIException
    {
        ComponentLogger loggerInst = new ComponentLogger(comp);
        comp.setLoggerInstance(loggerInst);

        // Register the logger MBean

        ObjectName loggerMBeanName = null;
        ComponentLoggerMBean loggerMBean = (ComponentLoggerMBean) loggerInst;
        try
        {
            loggerMBeanName = registerMBean(
                comp, loggerMBean, ComponentLoggerMBean.class,
                MBeanNames.CONTROL_TYPE_LOGGER);
        }
        catch ( javax.jbi.JBIException ex )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_LOGGER_MBEAN_REGISTRATION_FAILED,
                comp.getComponentTypeAsString(), comp.getName(),
                ex.getMessage()));
        }
        comp.setLoggerMBeanName(loggerMBeanName);
    }

    /**
     * Create and register a StatisticsMBean for a component.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If a failure occurs.
     */
    void createStatisticsMBean(Component comp)
        throws javax.jbi.JBIException
    {
        ObjectName statsMBeanName = null;
        ComponentStatistics statsMBean = new ComponentStatistics(comp.getName());
        comp.setStatisticsInstance(statsMBean);
        try
        {
            statsMBeanName = registerMBean(comp, statsMBean,
                com.sun.jbi.monitoring.ComponentStatisticsMBean.class,
                MBeanNames.CONTROL_TYPE_STATISTICS);
        }
        catch ( javax.jbi.JBIException ex )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_STATISTICS_MBEAN_REGISTRATION_FAILED,
                comp.getComponentTypeAsString(),
                comp.getName(), ex.getMessage()));
        }
        comp.setStatisticsMBeanName(statsMBeanName);
    }

    /**
     * Create and register a Component Configuration MBean for a component.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If a failure occurs.
     */
    void createConfigurationMBean(Component comp)
        throws javax.jbi.JBIException
    {
        ObjectName cfgMBeanName = null;
        ComponentConfiguration cfgMBean = new ComponentConfiguration(mContext, comp.getName());
        try
        {
            cfgMBeanName = registerMBean(comp, cfgMBean, null,
                MBeanNames.CONTROL_TYPE_CONFIGURATION);
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_CONFIGURATION_MBEAN_REGISTRATION_FAILED,
                comp.getComponentTypeAsString(), comp.getName(),
                jbiEx.getMessage()));
        }
        comp.setConfigurationMBeanName(cfgMBeanName);
    }
    
    /**
     * Call a component's bootstrap onInstall() method to perform the install
     * processing for the component.
     * @param comp The instance of Component for this component.
     * @param msg The error message string for failure messages.
     * @param installContext The installation context.
     * @throws javax.jbi.JBIException If any failure occurs.
     */
    private void doInstall(Component comp, String msg,
                           InstallationContext installContext)
        throws javax.jbi.JBIException
    {
        // Get the bootstrap class instance that was loaded and saved by
        // the loadBootstrap() method.

        Bootstrap bootInstance = comp.getBootstrapInstance(false);
                          
        if ( null == bootInstance )
        {
            mCompReg.removeComponent(comp.getName());
            throw new javax.jbi.JBIException(msg + mTranslator.getString(
                LocalStringKeys.CF_BOOTSTRAP_NOT_LOADED));
        }

        // Call the bootstrap onInstall() method. Note that this can actually
        // modify the component's runtime class path. This is why onInstall()
        // must be called before the creation of the component's class loader.
        // Once onInstall() completes, set the Life Cycle class path from the
        // values in the InstallationContext. Then call the bootstrap cleanUp()
        // method to clean up. Note that cleanUp() must be called even if
        // onInstall() fails. Also, care must be taken to preserve the original
        // exception from onInstall() if cleanUp() fails after onInstall() 
        // fails. The cleanUp() exception is logged but not thrown in that case.

        boolean failure = false;
        try
        {
            bootInstance.onInstall();
            comp.setComponentClassPathElements(
                installContext.getAbsoluteClassPathElements());
        }
        catch ( Throwable ex )
        {
            failure = true;
            mCompReg.removeComponent(comp.getName());
            String error = msg + mTranslator.getString(
                LocalStringKeys.CF_BOOTSTRAP_ONINSTALL_FAILED,
                ex.getClass().getName()) + getExMsg(ex);
            mLog.warning(error);
            mLog.warning(mTranslator.stackTraceToString(ex));
            throw new javax.jbi.JBIException(error, ex);
        }
        finally
        {
            try
            {
                bootInstance.cleanUp();
            }
            catch ( Throwable ex )
            {
                String error = mTranslator.getString(
                    LocalStringKeys.CF_BOOTSTRAP_INSTALL_CLEANUP_FAILED,
                    comp.getComponentTypeAsString(), comp.getName(),
                    ex.getClass().getName()) + getExMsg(ex);
                mLog.warning(error);
                mLog.warning(mTranslator.stackTraceToString(ex));
                if ( !failure )
                {
                    throw new com.sun.jbi.framework.FrameworkWarningException(error);
                }
            }
            finally
            {
                comp.clearBootstrapInstance();
            }
        }
    }

    /**
     * Call a component's bootstrap onUninstall() method to perform the
     * uninstall processing for the component.
     * @param comp The instance of Component for this component.
     * @param msg The error message string for failure messages.
     * @throws javax.jbi.JBIException If any failure occurs.
     */
    private void doUninstall(Component comp, String msg)
        throws javax.jbi.JBIException
    {
        // Get the bootstrap class instance that was loaded and saved by 
        // the loadBootstrap() method.

        Bootstrap bootInstance = comp.getBootstrapInstance(false);
        if ( null == bootInstance )
        {
            throw new javax.jbi.JBIException(msg + mTranslator.getString(
                LocalStringKeys.CF_BOOTSTRAP_NOT_LOADED));
        }

        // Call the bootstrap class onUninstall() method. Then call the
        // bootstrap cleanUp() method to clean up. Note that cleanUp() must be
        // called even if onUninstall() fails. Also, care must be taken to
        // preserve the original exception from onUninstall() if cleanUp()
        // fails after onUninstall() fails. In that case the cleanUp() exception
        // is logged, but not thrown. If cleanUp() fails but onUninstall() was
        // successful, the exception is logged and thrown as a warning exception
        // so that the uninstall still succeeds but with a warning message.

        boolean failure = false;
        try
        {
            bootInstance.onUninstall();
        }
        catch ( Throwable ex )
        {
            failure = true;
            String error = msg + mTranslator.getString(
                LocalStringKeys.CF_BOOTSTRAP_ONUNINSTALL_FAILED,
                ex.getClass().getName()) + getExMsg(ex);
            mLog.warning(error);
            mLog.warning(mTranslator.stackTraceToString(ex));
            throw new javax.jbi.JBIException(error, ex);
        }
        finally
        {
            try
            {
                bootInstance.cleanUp();
            }
            catch ( Throwable ex )
            {
                String error = mTranslator.getString(
                    LocalStringKeys.CF_BOOTSTRAP_UNINSTALL_CLEANUP_FAILED,
                    comp.getComponentTypeAsString(), comp.getName(),
                    ex.getClass().getName()) + getExMsg(ex);
                mLog.warning(error);
                mLog.warning(mTranslator.stackTraceToString(ex));
                if ( !failure )
                {
                    throw new com.sun.jbi.framework.FrameworkWarningException(error);
                }
            }
            finally
            {
                comp.clearBootstrapInstance();
            }
        }
    }

    /**
     * Get the message from an exception, if available, and return either
     * the message "The exception message is: <msg>" or an empty string.
     *
     * @param ex The exception from which the message is requested.
     * @return the exception message text or an empty string.
     */
    private String getExMsg(Throwable ex)
    {
        String msg = "";
        if ( null != ex.getMessage() )
        {
            msg = " " + mTranslator.getString(LocalStringKeys.EXCEPTION_MSG,
                 ex.getMessage());
        }
        return msg;
    }

    /**
     * Get the <code>upgrade()</code> method of a component's bootstrap
     * implementation, if it is present. This is done using reflection.
     * If this method is implemented, then the component supports upgrade;
     * otherwise the component does not support upgrade.
     *
     * @param bootInstance The Bootstrap instance of the component.
     * @return the upgrade method or null if upgrade is not supported by the
     * component.
     */
    private java.lang.reflect.Method getUpgradeMethod(Bootstrap bootInstance)
    {
        Class bootClass = bootInstance.getClass();
        java.lang.reflect.Method upgrade;
        try
        {
            Class args[] = {String.class, List.class};
            upgrade = bootClass.getMethod("upgrade", args);
        }
        catch ( NoSuchMethodException nsmEx )
        {
            upgrade = null;
        }
        return upgrade;
    }

    /**
     * Load and initialize the bootstrap instance for a component, and register
     * an InstallerMBean for that instance.
     * @param comp The instance of Component representing this component.
     * @param msg An error message string for either installation or
     * uninstallation.
     * @param installContext The installation context.
     * @param force Set to <code>true</code> if this is for a forced uninstall.
     * In this case, any failure to load the component's bootstrap class is
     * ignored and the installer MBean is still created.
     * @return The JMX object name of the InstallerMBean for the component.
     * @throws javax.jbi.JBIException If the bootstrap cannot be initialized.
     */
    private ObjectName initBootstrap(Component comp, String msg,
        InstallationContext installContext, boolean force)
        throws javax.jbi.JBIException
    {
        // Load the bootstrap class.

        Bootstrap bootInstance = null;
        try
        {
            bootInstance = comp.getBootstrapInstance(true);
        }
        catch ( javax.jbi.JBIException ex )
        {
            if ( !force )
            {
                throw new javax.jbi.JBIException(msg, ex);
            }
            else
            {
                mLog.fine(msg + ex.getMessage());
            }
        }

        // Protect against an orphan bootstrap extension MBean.

        MBeanServer mbs = mContext.getMBeanServer();
        ObjectName extMBeanName = comp.getExtensionMBeanName();
        if ( null != mbs && null != extMBeanName )
        {
            try
            {
                if ( mbs.isRegistered(extMBeanName) )
                {
                    mbs.unregisterMBean(extMBeanName);
                    comp.setExtensionMBeanName(null);
                }
            }
            catch ( javax.management.InstanceNotFoundException infEx )
            {
                ; // This can be ignored.
            }
            catch ( javax.management.MBeanRegistrationException mbrEx )
            {
                ; // This can be ignored.
            }
        }

        // Call the bootstrap init() method.

        if ( null != bootInstance )
        {
            try
            {
                ComponentContext ctx = new ComponentContext(comp, mContext);
                installContext.setContext(
                    (javax.jbi.component.ComponentContext) ctx);
                bootInstance.init(
                    (javax.jbi.component.InstallationContext) installContext);
                comp.setBootstrapCleanUpNeeded(true);
            }
            catch ( Throwable ex )
            {
                String error = msg + mTranslator.getString(
                    LocalStringKeys.CF_BOOTSTRAP_INIT_FAILED,
                    ex.getClass().getName()) + getExMsg(ex);
                mLog.warning(error);
                mLog.warning(mTranslator.stackTraceToString(ex));
                try
                {
                    bootInstance.cleanUp();
                }
                catch ( Throwable cex )
                {
                    String err = mTranslator.getString(
                        LocalStringKeys.CF_BOOTSTRAP_CLEANUP_FAILED,
                        cex.getClass().getName()) + getExMsg(cex);
                    mLog.warning(msg + err);
                    mLog.warning(mTranslator.stackTraceToString(cex));
                    error = error + " " + err;
                }
                throw new javax.jbi.JBIException(error, ex);
            }
            try
            {
                ObjectName embn = bootInstance.getExtensionMBeanName();
                comp.setExtensionMBeanName(embn);
            }
            catch ( Throwable ex )
            {
                String error = msg + mTranslator.getString(
                    LocalStringKeys.CF_BOOTSTRAP_GET_EXTENSION_FAILED,
                    ex.getClass().getName()) + getExMsg(ex);
                mLog.warning(error);
                mLog.warning(mTranslator.stackTraceToString(ex));
                try
                {
                    bootInstance.cleanUp();
                }
                catch ( Throwable cex )
                {
                    String err = mTranslator.getString(
                        LocalStringKeys.CF_BOOTSTRAP_CLEANUP_FAILED,
                        cex.getClass().getName(), cex.getMessage());
                    mLog.warning(msg + err);
                    mLog.warning(mTranslator.stackTraceToString(cex));
                    error = error + " " + err;
                }
                throw new javax.jbi.JBIException(error, ex);
            }
        }

        // Create and register the InstallerMBean for this component.

        ObjectName instMBeanName;
        ComponentInstaller instMBean =
            new ComponentInstaller(installContext, this, mCompReg);
        try
        {
            instMBeanName = registerMBean(comp, instMBean,
                com.sun.jbi.management.InstallerMBean.class,
                MBeanNames.CONTROL_TYPE_INSTALLER);
            comp.setInstallerMBeanName(instMBeanName);
        }
        catch ( javax.jbi.JBIException ex )
        {
            String error = msg + mTranslator.getString(
                LocalStringKeys.CF_COMP_INSTALLER_MBEAN_REGISTRATION_FAILED);
            mLog.warning(error);
            throw new javax.jbi.JBIException(error, ex);
        }
        return instMBeanName;
    }

    /**
     * Initialize a Component instance using data provided by the Installation
     * Service.
     * @param comp The Component instance to be initialized.
     * @param installContext The context created by the Installation Service.
     * @param bootClassName The name of the bootstrap class for the component.
     * @param bootClassPathElements A List of elements comprising the
     * class path for loading the bootstrap class. Each element is a String
     * containing the full path to either a jar file or a directory containing
     * class files.
     * @param sharedLibraryNames A List of String objects containing the names
     * of the Shared Libraries required by the component.
     */
    private void initComponent(
        Component comp,
        InstallationContext installContext,
        String bootClassName,
        List<String> bootClassPathElements,
        List<String> sharedLibraryNames)
    {
        comp.setName(installContext.getComponentName());
        comp.setDescription(installContext.getDescription());
        comp.setInstallRoot(installContext.getInstallRoot());
        String wsroot = installContext.getWorkspaceRoot();
        if ( null != wsroot )
        {
            comp.setWorkspaceRoot(wsroot);
        }
        else
        {
            comp.setWorkspaceRoot(installContext.getInstallRoot());
        }
        if ( installContext.isBinding() )
        {
            comp.setComponentTypeBinding();
        }
        else
        {
            comp.setComponentTypeEngine();
        }
        comp.setBootstrapClassLoaderSelfFirst(
            installContext.isBootstrapClassLoaderSelfFirst());
        comp.setBootstrapClassName(bootClassName);
        comp.setBootstrapClassPathElements(bootClassPathElements);
        comp.setComponentClassName(installContext.getComponentClassName());
        comp.setComponentClassPathElements(
            installContext.getAbsoluteClassPathElements());
        comp.setComponentClassLoaderSelfFirst(
            installContext.isComponentClassLoaderSelfFirst());
        comp.setSharedLibraryNames(sharedLibraryNames);
        comp.setLoaded();
        comp.setDesiredState(ComponentState.SHUTDOWN);
    }

    /**
     * Register an MBean for a component with the main MBean Server if it is
     * available.
     * @param component The Component object for this component.
     * @param instance The component implementation instance.
     * @param interfaceClass The interface implemented by the instance.
     * @param mbeanType The type of MBean to be registered. The value of this
     * is set using constants from the MBeanNames class.
     * @return The JMX MBean object name or null if there is no MBean Server
     * available.
     * @throws javax.jbi.JBIException If the MBean registration fails.
     */
    private ObjectName registerMBean(
        Component component,
        Object instance,
        Class interfaceClass,
        String mbeanType)
        throws javax.jbi.JBIException
    {
        if ( null == mContext.getMBeanServer() )
        {
            return null;
        }
            mLog.finer("Registering " + mbeanType + " MBean for " +
                component.getComponentTypeAsString() + " " + component.getName());

        // Create a StandardMBean for this component instance if required.

        boolean isDynamicMBean = ( interfaceClass == null ? true : false );

        StandardMBean mbean = null;
        if ( !isDynamicMBean )
        {   
            try
            {
                mbean = new StandardMBean(instance, interfaceClass);
            }
            catch ( javax.management.NotCompliantMBeanException ncEx )
            {
                String exName = ncEx.getClass().getName();
                throw new javax.jbi.JBIException(mTranslator.getString(
                    LocalStringKeys.CF_MBEAN_CREATION_FAILED,
                    exName), ncEx);
            }
        }

        // Create the MBean ObjectName using the component name and the MBean
        // control type.

        ObjectName mbeanName = null;
        if ( component.isBinding() )
        {
            mbeanName = mContext.getMBeanNames().getBindingMBeanName(
                component.getName(), mbeanType);
        }
        else
        {
            mbeanName = mContext.getMBeanNames().getEngineMBeanName(
                component.getName(), mbeanType);
        }

        // Register the MBean. If it is already registered, unregister it
        // first, as it is an old registration with a stale instance reference.

        try
        {
            if ( mContext.getMBeanServer().isRegistered(mbeanName) )
            {
                try
                {
                    mContext.getMBeanServer().unregisterMBean(mbeanName);
                }
                catch ( javax.management.InstanceNotFoundException infEx )
                {
                    ; // Just ignore this error
                }
            }
            if ( isDynamicMBean )
            {
                mContext.getMBeanServer().registerMBean(instance, mbeanName);
            }
            else
            {
                mContext.getMBeanServer().registerMBean(mbean, mbeanName);
            }
        }
        catch ( javax.management.InstanceAlreadyExistsException iaeEx )
        {
            String exName = iaeEx.getClass().getName();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_MBEAN_REGISTRATION_FAILED,
                exName), iaeEx);
        }
        catch ( javax.management.MBeanRegistrationException mbrEx )
        {
            String exName = mbrEx.getClass().getName();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_MBEAN_REGISTRATION_FAILED,
                exName), mbrEx);
        }
        catch ( javax.management.NotCompliantMBeanException ncEx )
        {
            String exName = ncEx.getClass().getName();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.CF_MBEAN_REGISTRATION_FAILED,
                exName), ncEx);
        }

        // Return the ObjectName for the new MBean.
        return mbeanName;
    }

    /**
     * Persist a change in the desired state of a component.
     * @param comp the Component instance representing the component.
     */
    private void persistState(Component comp)
    {
        try
        {
            ((com.sun.jbi.management.registry.Registry)
                mContext.getRegistry()).getUpdater().setComponentState(
                    comp.getDesiredState(), comp.getName());
        }
        catch ( com.sun.jbi.management.registry.RegistryException rEx )
        {
            mLog.log(Level.WARNING, rEx.getMessage(), rEx);
        }
    }
    
    /**
     * Prepare all components and set their desired states based on their
     * last desired states. Preparing components means calling their life cycle
     * <code>init()</code> methods. This allows the components to create
     * local <code>XAResource</code>s and register them with the JBI
     * framework. This registration allows the <code>XAResource</code>s to
     * be recovered by the Transaction Manager. At the end of the prepare
     * phase, all components are in the <code>STOPPED</code> state.
     *
     * When the START phase is executed, all components are brought to their
     * desired states.
     *
     * @param compList The list of installed components.
     */
    private void prepareComponents(List compList)
    {
        ListIterator c = compList.listIterator();
        Component comp = null;
        ComponentOperation oper;
        OperationCounter oc = new OperationCounter();
        ArrayList opers = new ArrayList();
        long timeOut = mContext.getComponentTimeout();

        while ( c.hasNext() )
        {
            comp = (Component) c.next();
            ComponentState      previousDesired = comp.getDesiredState();
            
            //
            //  Components previously in STARTED state need to be initialized
            //  in case XA recovery needs to happen.
            //
            if (previousDesired.equals(ComponentState.STARTED))
            {
                oper = new ComponentOperation(
                    oc, comp, ComponentOperation.INITIALIZE, true);
                opers.add(oper);
                new Thread(oper, comp.getName()).start();
            }
        }

        if ( 0 < opers.size() )
        {
            synchronized (oc)
            {
                try
                {
                    if ( 0 < oc.getValue() )
                    {
                        oc.wait(mContext.getComponentTimeout());
                    }
                }
                catch ( java.lang.InterruptedException iEx )
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_STARTUP_INTERRUPTED));
                }
            }
            Iterator opersIter = opers.iterator();
            while ( opersIter.hasNext() )
            {
                oper = (ComponentOperation) opersIter.next();
                comp = oper.getComponent();
                if ( !oper.completed() )
                {
                    oper.getThread().interrupt();
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_COMP_INIT_TIMEOUT,
                        comp.getComponentTypeAsString(),
                        comp.getName(), new Long(timeOut)));
                }
            }
        }
        
    }

    /**
     * Start components based on their desired states. The desired states were
     * either set by <code>prepareComponents()</code> if the XA recovery phase
     * was needed, or restored from their values prior to the last JBI shutdown.
     * if <code>prepareComponents()</code> ran, all components are in
     * <code>STOPPED</code> state upon entry to this method; otherwise they are
     * in <code>SHUTDOWN</code> state.
     *
     * In order to get components into their desired states, different actions
     * are necessary in some cases depending on whether or not
     * <code>prepareComponents()</code> was called.<br>
     *
     * Actions when <code>prepareComponents()</code> was called:
     *
     * <ul>
     * <li>If the desired state is <code>SHUTDOWN</code>, the component's life
     * cycle <code>shutDown()</code> method is called.
     * </li>
     * <li>If the desired state is <code>STOPPED</code>, no action is required
     * </li>
     * <li>If the desired state is <code>STARTED</code>, the component's life
     * cycle <code>start()</code> method is called.
     * </li>
     * </ul><br>
     *
     * Actions when <code>prepareComponents()</code> was NOT called:
     *
     * <ul>
     * <li>If the desired state is <code>SHUTDOWN</code>, no action is required.
     * </li>
     * <li>If the desired state is <code>STOPPED</code>, the component's life
     * cycle <code>init()</code> method is called.
     * </li>
     * <li>If the desired state is (code>STARTED</code>, the component's life
     * cycle <code>init()</code> and <code>start()</code) methods are called.
     * </li>
     * </ul>
     *
     * @param compList The list of installed components.
     */
    private void startComponents(List compList)
    {
        mLog.finer("startComponents entered, startup state is " +
            ((mStartupState == PREPARE) ? "PREPARE" : "START"));
        ListIterator c = compList.listIterator();
        Component comp = null;
        ComponentOperation oper;
        OperationCounter oc = new OperationCounter();
        ArrayList opers = new ArrayList();
        long timeOut = mContext.getComponentTimeout();
        if (mStartupState == PREPARE)
        {
            // If we performed the PREPARE phase, some components related to XA 
            // recovery that have a desired state of <code>STARTED</code> are in
            // <code>STOPPED</code> state. If the desired state is
            // <code>SHUTDOWN</code>, shut down the component; if the desired
            // state is <code>STARTED</code>, start the component; if the
            // desired state is <code>STOPPED</code>, initialize the component.

            while ( c.hasNext() )
            {
                comp = (Component) c.next();
                mLog.finer("processing component " + comp.getName() +
                    " with desired state " + comp.getDesiredState());
                if ( ComponentState.STARTED == comp.getDesiredState() )
                {
                    oper = new ComponentOperation(
                        oc, comp, ComponentOperation.STARTUP, true);
                    opers.add(oper);
                    new Thread(oper, comp.getName()).start();
                }
                else if ( ComponentState.STOPPED == comp.getDesiredState() )
                {
                    oper = new ComponentOperation(
                        oc, comp, ComponentOperation.INITIALIZE, true);
                    opers.add(oper);
                    new Thread(oper, comp.getName()).start();
                }
                else if ( ComponentState.SHUTDOWN == comp.getDesiredState() )
                {
                    oper = new ComponentOperation(
                        oc, comp, ComponentOperation.SHUTDOWN, true);
                    opers.add(oper);
                    new Thread(oper, comp.getName()).start();
                }
            }
        }
        else
        {
            // If we skipped the PREPARE phase, all components are in
            // <code>SHUTDOWN</code> state. First set the desired state based
            // on the last known desired state. Then, if the desired state is
            // <code>STOPPED</code>, initialize the component; if the desired
            // state is <code>STARTED</code>, initialize and start the component.

            while ( c.hasNext() )
            {
                comp = (Component) c.next();

                if (comp.getDesiredState().equals(ComponentState.STOPPED) )
                {
                    oper = new ComponentOperation(
                        oc, comp, ComponentOperation.INITIALIZE, true);
                    opers.add(oper);
                    new Thread(oper, comp.getName()).start();
                }
                else if (comp.getDesiredState().equals(ComponentState.STARTED) )
                {
                    oper = new ComponentOperation(
                        oc, comp, ComponentOperation.STARTUP, true);
                    opers.add(oper);
                    new Thread(oper, comp.getName()).start();
                }
            }
        }

        // If any operations were performed, wait for all of them to complete
        // and process the results.

        if ( 0 < opers.size() )
        {
            mLog.finer("waiting for " + opers.size() + " operations to complete");
            synchronized (oc)
            {
                try
                {
                    if ( 0 < oc.getValue() )
                    {
                        oc.wait(timeOut);
                    }
                }
                catch ( java.lang.InterruptedException iEx )
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_STARTUP_INTERRUPTED));
                }
            }
            Iterator opersIter = opers.iterator();
            while ( opersIter.hasNext() )
            {
                oper = (ComponentOperation) opersIter.next();
                comp = oper.getComponent();
                if ( !oper.completed() )
                {
                    oper.getThread().interrupt();
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_COMP_START_TIMEOUT,
                        comp.getComponentTypeAsString(),
                        comp.getName(), new Long(timeOut)));
                }
            }
        }

        // Now bring all Service Assemblies to their desired states. Any errors
        // that occur are handled and logged by the Deployment Service. There
        // is no need for any error checking here, because during startup of
        // the runtime, there's really not anything that can be done in the
        // event of an error in the processing other than log the error.

        com.sun.jbi.management.system.ManagementService ms =
            mContext.getManagementService();
        if ( null != ms )
        {
            com.sun.jbi.management.system.ManagementContext mc = 
                ms.getManagementContext();
            if ( null != mc )
            {
                mc.getDeploymentServiceHandle().startAllServiceAssemblies();
            }
        }
    }

    /**
     * Unregister all the MBeans for a component.
     * @param comp The Component instance representing the component.
     * @throws javax.jbi.JBIException If a failure occurs.
     */
    void unregisterMBeans(Component comp)
        throws javax.jbi.JBIException
    {
        ObjectName name;
        MBeanServer mbs = mContext.getMBeanServer();
        if ( null != mbs )
        {
            // Unregister Configuration MBean
            try
            {
                mbs.unregisterMBean(comp.getConfigurationMBeanName());
            }
            catch ( Throwable ex )
            {
                String error = mTranslator.getString(
                    LocalStringKeys.CF_CONFIGURATION_MBEAN_UNREGISTRATION_FAILED,
                    comp.getComponentTypeAsString(), comp.getName(),
                    ex.getClass().getName());
                mLog.warning(error);
                mLog.warning(mTranslator.stackTraceToString(ex));
            }
            comp.setConfigurationMBeanName(null);
            
            // Unregister LifeCycle MBean
            try
            {
                mbs.unregisterMBean(comp.getLifeCycleMBeanName());
            }
            catch ( Throwable ex )
            {
                String error = mTranslator.getString(
                    LocalStringKeys.CF_LIFECYCLE_MBEAN_UNREGISTRATION_FAILED,
                    comp.getComponentTypeAsString(), comp.getName(),
                    ex.getClass().getName());
                mLog.warning(error);
                mLog.warning(mTranslator.stackTraceToString(ex));
            }
            comp.setLifeCycleMBeanName(null);

            // Unregister Deployer MBean if it exists
            name = comp.getDeployerMBeanName();
            if ( null != name )
            {
                try
                {
                    mbs.unregisterMBean(name);
                }
                catch ( Throwable ex )
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_DEPLOYER_MBEAN_UNREGISTRATION_FAILED,
                        comp.getComponentTypeAsString(), comp.getName(),
                        ex.getClass().getName()));
                    mLog.warning(mTranslator.stackTraceToString(ex));
                }
                comp.setDeployerInstance(null);
                comp.setDeployerMBeanName(null);
            }

            // Unregister logger MBean
            name = comp.getLoggerMBeanName();
            if ( null != name )
            {
                try
                {
                    mbs.unregisterMBean(name);
                }
                catch ( Throwable ex )
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_LOGGER_MBEAN_UNREGISTRATION_FAILED,
                        comp.getComponentTypeAsString(), comp.getName(), name,
                        ex.getClass().getName()));
                    mLog.warning(mTranslator.stackTraceToString(ex));
                }
                comp.setLoggerMBeanName(null);
            }

            // Unregister statistics MBean
            name = comp.getStatisticsMBeanName();
            if ( null != name )
            {
                try
                {
                    mbs.unregisterMBean(name);
                }
                catch ( Throwable ex )
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.CF_STATISTICS_MBEAN_UNREGISTRATION_FAILED,
                        comp.getComponentTypeAsString(), comp.getName(),
                        ex.getClass().getName()));
                    mLog.warning(mTranslator.stackTraceToString(ex));
                }
                comp.clearStatisticsInstance();
                comp.setStatisticsMBeanName(null);
            }
        }
    }
}
