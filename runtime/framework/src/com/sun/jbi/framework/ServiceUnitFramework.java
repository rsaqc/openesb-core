/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitFramework.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.management.system.DeploymentService;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.registry.Registry;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.component.ServiceUnitManager;

/**
 * This is the implementation of the Service Unit Framework, which provides
 * an interface to the Service Unit Manager of all components. This class is
 * used solely by the Component Framework during startup and shutdown of
 * components. 
 *
 * @author Sun Microsystems, Inc.
 */
public class ServiceUnitFramework
{
    /**
     * Local handle to EnvironmentContext
     */
    private EnvironmentContext mContext;

    /**
     * Local handle to ComponentRegistry
     */
    private ComponentRegistry mCompReg;

    /**
     * Local handle to Logger
     */
    private Logger mLog;

    /**
     * Local handle to StringTranslator
     */
    private StringTranslator mTranslator;

    /**
     * Timeout limit for heuristic decision on failure limits
     */
    private static final int TIMEOUT_LIMIT = 3;

    /**
     * Default timeout interval for Service Unit operations. The
     * value is in milliseconds and is set to 2 minutes.
     */
    private static final long TIMEOUT_DEFAULT = 1200000;


    /**
     * Constructor.
     * @param env the EnvironmentContext for JBI.
     */
    ServiceUnitFramework(EnvironmentContext env)
    {
        mContext = env;
        mCompReg = env.getComponentRegistry();
        mLog = env.getLogger();
        mTranslator = (StringTranslator) env.getStringTranslatorFor(this);
    }

    /**
     * Initialize one Service Unit for a component. After initialization, if the
     * the desired state of this Service Unit is <CODE>SHUTDOWN</CODE>, shut
     * down the Service Unit to reach the desired state.
     * @param comp the Component instance for this component.
     * @param suMgr the ServiceUnitManager instance for this component.
     * @param su the ServiceUnit instance for this Service Unit.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    private void initializeServiceUnit(Component comp, ServiceUnitManager suMgr,
        ServiceUnit su)
        throws javax.jbi.JBIException
    {
        mLog.finer("Initializing Service Unit " + su.getName() +
            " for " + comp.getComponentTypeAsString() + " " + comp.getName());
        if ( su.isStopped() )
        {
            return;
        }
        ComponentStatistics stats = comp.getStatisticsInstance();
        try
        {
            if ( stats.isEnabled() )
            {
                stats.incrementInitSURequests();
            }
            performServiceUnitOperation(ServiceUnitOperation.INIT,
                "init", su, suMgr, comp);
            mLog.finer("Setting Service Unit " + su.getName() +
                " to STOPPED state");
            su.setStopped();
            if ( ServiceUnitState.SHUTDOWN == su.getDesiredState() )
            {
                mLog.finest("Returning Service Unit " + su.getName() +
                    " to shutdown state");
                if ( stats.isEnabled() )
                {
                    stats.incrementShutDownSURequests();
                }
                performServiceUnitOperation(ServiceUnitOperation.SHUTDOWN,
                    "init", su, suMgr, comp);
                mLog.finer("Setting Service Unit " + su.getName() +
                    " to SHUTDOWN state");
                su.setShutdown();
            }
        }
        catch ( javax.jbi.JBIException ex )
        {
            String m = ex.getMessage();
            if ( ex.getCause() instanceof javax.jbi.JBIException )
            {
                mLog.warning(mTranslator.getString(
                    LocalStringKeys.SUM_EXCEPTION,
                    comp.getComponentTypeAsString(), comp.getName(), m));
            }
            else
            {
                mLog.log(Level.WARNING, mTranslator.getString(
                    LocalStringKeys.SUM_EXCEPTION,
                    comp.getComponentTypeAsString(), comp.getName(), m), ex);
            }
            throw ex;
        }
        finally
        {
            persistState(su, "init");
        }
    }

    /**
     * Initialize all Service Units for a component. This is required in order
     * to ensure that a component knows about all of its Service Units after it
     * has been shut down and restarted.
     *
     * Each initialization call is done under a time limit. There is the danger
     * that a hung component could have many Service Units deployed to it, and
     * this could result in a very long time period waiting for each of those
     * initialization calls to time out. Therefore, a heuristic check is used
     * here; if 3 consecutive calls time out, no more calls are made. Note
     * that this happens only if the 3 timeouts are on consecutive calls. Each
     * time a call completes (successfully or not) before the timeout interval
     * expires, the timeout count is reset.
     *
     * @param comp the Component instance for this component.
     */
    void initializeServiceUnits(Component comp)
    {
        // Don't allow if the component is not started
        if ( !comp.isStarted() )
        {
            return;
        }
        mLog.finest("Initializing Service Units for " +
            comp.getComponentTypeAsString() + " " + comp.getName());
        ServiceUnitManager suMgr = comp.getServiceUnitManager();
        if ( null == suMgr )
        {
            return;
        }
        List sus = comp.getServiceUnitList();
        Iterator suIter = sus.iterator();
        int timeouts = 0;
        while ( suIter.hasNext() )
        {
            try
            {
                initializeServiceUnit(comp, suMgr, (ServiceUnit) suIter.next());
            }
            catch ( Throwable ex )
            {
                if ( ex instanceof com.sun.jbi.framework.TimeOutException )
                {
                    if ( ++timeouts >= TIMEOUT_LIMIT )
                    {
                        mLog.warning(mTranslator.getString(
                            LocalStringKeys.SUM_INIT_TIMEOUT_LIMIT,
                            new Long(TIMEOUT_LIMIT),
                            comp.getComponentTypeAsString(),
                            comp.getName()));
                        break;
                    }
                }
                else
                {
                    timeouts = 0;
                }
            }
        }
        mLog.finest("Initialized Service Units for " +
            comp.getComponentTypeAsString() + " " + comp.getName());
    }

    /**
     * Shut down one Service Unit for a component. Any failure is just logged
     * so as not to terminate other processing.
     * @param comp the Component instance for this component.
     * @param suMgr the ServiceUnitManager instance for this component.
     * @param su the ServiceUnit instance for this Service Unit.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    private void shutDownServiceUnit(Component comp, ServiceUnitManager suMgr,
        ServiceUnit su)
        throws javax.jbi.JBIException
    {
        mLog.finest("Shutting down Service Unit " + su.getName() +
            " for " + comp.getComponentTypeAsString() + " " + comp.getName());
        if ( su.isShutdown() )
        {
            return;
        }
        ComponentStatistics stats = comp.getStatisticsInstance();
        try
        {
            if ( stats.isEnabled() )
            {
                stats.incrementShutDownSURequests();
            }
            performServiceUnitOperation(ServiceUnitOperation.SHUTDOWN,
                "shutDown", su, suMgr, comp);
            mLog.finer("Setting Service Unit " + su.getName() +
                " to SHUTDOWN state");
            su.setShutdown();
            mContext.getNotifier().emitServiceUnitNotification(
                EventNotifier.EventType.ShutDown,
                su.getName(), comp.getName(), su.getServiceAssemblyName(),
                mTranslator.getString(LocalStringKeys.SUF_SHUT_DOWN,
                    su.getName(), comp.getName(), su.getServiceAssemblyName()));
        }
        catch ( javax.jbi.JBIException ex )
        {
            String m = ex.getMessage();
            mLog.warning(mTranslator.getString(
                LocalStringKeys.SUM_EXCEPTION,
                comp.getComponentTypeAsString(), comp.getName(), m));
            if ( !(ex.getCause() instanceof javax.jbi.JBIException) )
            {
                mLog.warning(mTranslator.getString(
                    LocalStringKeys.SUM_STACK_TRACE,
                    stackTraceToString(ex)));
            }
            throw ex;
        }
        finally
        {
            persistState(su, "shutDown");
        }
    }

    /**
     * Shut down all active Service Units for a component.
     * @param comp the Component instance for this component.
     */
    void shutDownServiceUnits(Component comp)
    {
        mLog.finest("Shutting down Service Units for " +
            comp.getComponentTypeAsString() + " " + comp.getName());
        ServiceUnitManager suMgr = comp.getServiceUnitManager();
        if ( null == suMgr )
        {
            return;
        }
        List sus = comp.getServiceUnitList();
        Iterator suIter = sus.iterator();
        int timeouts = 0;
        while ( suIter.hasNext() )
        {
            ServiceUnit su = (ServiceUnit) suIter.next();
            if ( su.isStopped() )
            {
                try
                {
                    shutDownServiceUnit(comp, suMgr, su);
                }
                catch ( Throwable ex )
                {
                    if ( ex instanceof com.sun.jbi.framework.TimeOutException )
                    {
                        if ( ++timeouts >= TIMEOUT_LIMIT )
                        {
                            mLog.warning(mTranslator.getString(
                                LocalStringKeys.SUM_SHUTDOWN_TIMEOUT_LIMIT,
                                new Long(TIMEOUT_LIMIT),
                                comp.getComponentTypeAsString(),
                                comp.getName()));
                            break;
                        }
                    }
                    else
                    {
                        timeouts = 0;
                    }
                }
            }
            else
            {
                mLog.finest("Skipping Service Unit " + su.getName() +
                    ": was not started");
            }
        }
        mLog.finest("Shut down Service Units for " +
            comp.getComponentTypeAsString() + " " + comp.getName());
    }

    /*
     * Start one Service Unit for a component. Any failure is just logged
     * so as not to terminate other processing.
     * @param comp the Component instance for this component.
     * @param suMgr the ServiceUnitManager instance for this component.
     * @param su the ServiceUnit instance for this Service Unit.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    private void startServiceUnit(Component comp, ServiceUnitManager suMgr,
        ServiceUnit su)
        throws javax.jbi.JBIException
    {
        mLog.finest("Starting Service Unit " + su.getName() +
            " for " + comp.getComponentTypeAsString() + " " + comp.getName());
        ComponentStatistics stats = comp.getStatisticsInstance();
        try
        {
            if ( stats.isEnabled() )
            {
                stats.incrementStartSURequests();
            }
            performServiceUnitOperation(ServiceUnitOperation.START,
                "start", su, suMgr, comp);
            mLog.finer("Setting Service Unit " + su.getName() +
                " to STARTED state");
            su.setStarted();
            mContext.getNotifier().emitServiceUnitNotification(
                EventNotifier.EventType.Started,
                su.getName(), comp.getName(), su.getServiceAssemblyName(),
                mTranslator.getString(LocalStringKeys.SUF_STARTED,
                    su.getName(), comp.getName(), su.getServiceAssemblyName()));
        }
        catch ( javax.jbi.JBIException ex )
        {
            try
            {
                if ( stats.isEnabled() )
                {
                    stats.incrementShutDownSURequests();
                }
                performServiceUnitOperation(ServiceUnitOperation.SHUTDOWN,
                    "start", su, suMgr, comp);
                mLog.finer("Setting Service Unit " + su.getName() +
                    " to SHUTDOWN state");
                su.setShutdown();
            }
            catch ( javax.jbi.JBIException e )
            {
                String em = e.getMessage();
                mLog.warning(mTranslator.getString(
                    LocalStringKeys.SUM_EXCEPTION,
                    comp.getComponentTypeAsString(), comp.getName(), em));
                if ( !(e.getCause() instanceof javax.jbi.JBIException) )
                {
                    mLog.warning(mTranslator.getString(
                        LocalStringKeys.SUM_STACK_TRACE,
                        stackTraceToString(e)));
                }
            }
            String m = ex.getMessage();
            mLog.warning(mTranslator.getString(
                LocalStringKeys.SUM_EXCEPTION,
                comp.getComponentTypeAsString(), comp.getName(), m));
            if ( !(ex.getCause() instanceof javax.jbi.JBIException) )
            {
                mLog.warning(mTranslator.getString(
                    LocalStringKeys.SUM_STACK_TRACE,
                    stackTraceToString(ex)));
            }
            throw ex;
        }
        finally
        {
            persistState(su, "start");
        }
    }

    /**
     * Start Service Units for a component, based on their desired states at
     * the last shutdown of the component.
     * @param comp the Component instance for this component.
     */
    void startServiceUnits(Component comp)
    {
        // Don't allow if the component is not started
        if ( !comp.isStarted() )
        {
            return;
        }
        mLog.finest("Starting Service Units for " +
            comp.getComponentTypeAsString() + " " + comp.getName());
        ServiceUnitManager suMgr = comp.getServiceUnitManager();
        if ( null == suMgr )
        {
            return;
        }
        List sus = comp.getServiceUnitList();
        Iterator suIter = sus.iterator();
        int timeouts = 0;
        while ( suIter.hasNext() )
        {
            ServiceUnit su = (ServiceUnit) suIter.next();
            if ( ServiceUnitState.STARTED == su.getDesiredState() && su.isStopped() )
            {
                try
                {
                    startServiceUnit(comp, suMgr, su);
                }
                catch ( Throwable ex )
                {
                    if ( ex instanceof com.sun.jbi.framework.TimeOutException )
                    {
                        if ( ++timeouts >= TIMEOUT_LIMIT )
                        {
                            mLog.warning(mTranslator.getString(
                                LocalStringKeys.SUM_START_TIMEOUT_LIMIT,
                                new Long(TIMEOUT_LIMIT),
                                comp.getComponentTypeAsString(),
                                comp.getName()));
                            break;
                        }
                    }
                    else
                    {
                        timeouts = 0;
                    }
                }
            }
            else
            {
                mLog.finest("Skipping Service Unit " + su.getName() +
                    ": was not started at last shutdown");
            }
        }
        mLog.finest("Started Service Units for " +
            comp.getComponentTypeAsString() + " " + comp.getName());
    }

    /**
     * Stop one Service Unit for a component. Any failure is just logged
     * so as not to terminate other processing.
     * @param comp the Component instance for this component.
     * @param suMgr the ServiceUnitManager instance for this component.
     * @param su the ServiceUnit instance for this Service Unit.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    private void stopServiceUnit(Component comp, ServiceUnitManager suMgr,
        ServiceUnit su)
        throws javax.jbi.JBIException
    {
        mLog.finest("Stopping Service Unit " + su.getName() +
            " for " + comp.getComponentTypeAsString() + " " + comp.getName());
        ComponentStatistics stats = comp.getStatisticsInstance();
        try
        {
            if ( stats.isEnabled() )
            {
                stats.incrementStopSURequests();
            }
            performServiceUnitOperation(ServiceUnitOperation.STOP,
                "stop", su, suMgr, comp);
            mLog.finer("Setting Service Unit " + su.getName() +
                " to STOPPED state");
            su.setStopped();
            mContext.getNotifier().emitServiceUnitNotification(
                EventNotifier.EventType.Stopped,
                su.getName(), comp.getName(), su.getServiceAssemblyName(),
                mTranslator.getString(LocalStringKeys.SUF_STOPPED,
                    su.getName(), comp.getName(), su.getServiceAssemblyName()));
        }
        catch ( javax.jbi.JBIException ex )
        {
            String m = ex.getMessage();
            mLog.warning(mTranslator.getString(
                LocalStringKeys.SUM_EXCEPTION,
                comp.getComponentTypeAsString(), comp.getName(), m));
            if ( !(ex.getCause() instanceof javax.jbi.JBIException) )
            {
                mLog.warning(mTranslator.getString(
                    LocalStringKeys.SUM_STACK_TRACE,
                    stackTraceToString(ex)));
            }
            throw ex;
        }
        finally
        {
            persistState(su, "stop");
        }
    }

    /**
     * Stop all active Service Units for a component, after checkpointing
     * their states.
     * @param comp the Component instance for this component.
     */
    void stopServiceUnits(Component comp)
    {
        // Don't allow if the component is not started
        if ( !comp.isStarted() )
        {
            return;
        }
        mLog.finest("Stopping Service Units for " +
            comp.getComponentTypeAsString() + " " + comp.getName());
        ServiceUnitManager suMgr = comp.getServiceUnitManager();
        if ( null == suMgr )
        {
            return;
        }
        List sus = comp.getServiceUnitList();
        Iterator suIter = sus.iterator();
        int timeouts = 0;
        while ( suIter.hasNext() )
        {
            ServiceUnit su = (ServiceUnit) suIter.next();
            if ( su.isStarted() )
            {
                try
                {
                    stopServiceUnit(comp, suMgr, su);
                }
                catch ( Throwable ex )
                {
                    if ( ex instanceof com.sun.jbi.framework.TimeOutException )
                    {
                        if ( ++timeouts >= TIMEOUT_LIMIT )
                        {
                            mLog.warning(mTranslator.getString(
                                LocalStringKeys.SUM_STOP_TIMEOUT_LIMIT,
                                new Long(TIMEOUT_LIMIT),
                                comp.getComponentTypeAsString(),
                                comp.getName()));
                            break;
                        }
                    }
                    else
                    {
                        timeouts = 0;
                    }
                }
            }
            else
            {
                mLog.finest("Skipping Service Unit " + su.getName() +
                    ": was not started");
            }
        }
        mLog.finest("Stopped Service Units for " +
            comp.getComponentTypeAsString() + " " + comp.getName());
    }

    /**
     * Update the state of Service Assemblies. For each Service Assembly whose
     * name is in the provided list, set the state, and also start any service
     * connections that need to be started.
     *
     * @param saNames The list of Service Assembly names to update.
     */
    void updateServiceAssemblies(Set<String> saNames)
    {
        if ( saNames.isEmpty() )
        {
            mLog.finer("No Service Assemblies to be updated");
            return;
        }
        mLog.finer("Updating Service Assemblies...");
        String result;
        
        for ( String saName : saNames )
        {
            mLog.finer("Updating Service Assembly " + saName + "...");
            result = ManagementContext.getDeploymentServiceHandle().updateSAState(saName);
            mLog.finer("Updated Service Assembly state to " + result);
        }
        mLog.finer("Updated Service Assemblies");
    }

    /**
     * Perform the specified operation on a Service Unit and wait for it to
     * complete or time out.
     * @param operation the operation to be performed.
     * @param task the task being executed.
     * @param su the Service Unit to be operated upon.
     * @param suMgr the SU manager of the owning component.
     * @param comp the owning Component.
     * @throws javax.jbi.JBIException if any runtime error
     * or timeout occurs.
     */
    private void performServiceUnitOperation(int operation, String task,
        ServiceUnit su, ServiceUnitManager suMgr, Component comp)
        throws javax.jbi.JBIException
    {
        String suName = su.getName();
        String msg;
        Object args[] = {suName, su.getFilePath()};
        OperationCounter oc = new OperationCounter();
        ServiceUnitOperation suOper =
            new ServiceUnitOperation(oc, comp.getName(), suMgr, operation, args);
        long timeout = mContext.getServiceUnitTimeout();
        ComponentStatistics stats = comp.getStatisticsInstance();

        new Thread(suOper, suName).start();
        synchronized ( oc )
        {
            try
            {
                if ( 0 < oc.getValue() )
                {
                    oc.wait(timeout);
                }
            }
            catch ( java.lang.InterruptedException iEx )
            {
                suOper.getThread().interrupt();
                msg = mTranslator.getString(
                    LocalStringKeys.DMB_SU_OPERATION_INTERRUPTED,
                    task, suName);
                throw new javax.jbi.JBIException(msg, iEx);
            }
        }
        if ( suOper.completed() )
        {
            Throwable ex = suOper.getException();
            if ( null != ex )
            {
                if ( stats.isEnabled() )
                {
                    stats.incrementFailedSURequests();
                }
                if ( ex instanceof javax.jbi.management.DeploymentException )
                {
                    throw (javax.jbi.management.DeploymentException) ex;
                }
                else
                {
                    String exMsg = ex.getMessage();
                    if ( null == exMsg )
                    {
                        msg = mTranslator.getString(
                            LocalStringKeys.DMB_SU_OPERATION_EXCEPTION_NO_MSG,
                            ex.getClass().getName(), task, suName);
                    }
                    else
                    {
                        msg = mTranslator.getString(
                            LocalStringKeys.DMB_SU_OPERATION_EXCEPTION,
                            ex.getClass().getName(), task, suName, exMsg);
                    }
                    throw new javax.jbi.JBIException(msg, ex);
                }
            }
        }
        else
        {
            if ( stats.isEnabled() )
            {
                stats.incrementTimedOutSURequests();
            }
            suOper.getThread().interrupt();
            msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_OPERATION_TIMEOUT,
                task, suName, new Long(timeout));
            throw new com.sun.jbi.framework.TimeOutException(msg);
        }
    }

    /**
     * Force the state of an SU to be persisted after a change.
     * @param su the SU being processed.
     * @param task the task being executed.
     * @deprecated
     */
    private void persistState(ServiceUnit su, String task)
    {
        try
        {
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mContext.getRegistry()).getUpdater();
            updater.setServiceUnitState(su.getDesiredState(),
                su.getTargetComponent(), su.getName());
        }
        catch(com.sun.jbi.management.registry.RegistryException rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_STATE_PERSIST_FAILURE,
                su.getName(), task, rex.getMessage());
            mLog.warning(msg);
        }
    }

    /**
     * Return an exception's stack trace as a String. If the trace cannot
     * be produced, returns an error message in the string.
     * @param exception the exception to process.
     * @return the stack trace for the exception.
     */
    private String stackTraceToString(Throwable exception)
    {
        try
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            exception.printStackTrace(pw);
            return sw.toString();
        }
        catch ( Exception ex )
        {
            return mTranslator.getString(LocalStringKeys.STACK_TRACE_PRINT_FAILED);
        }
    }
}
