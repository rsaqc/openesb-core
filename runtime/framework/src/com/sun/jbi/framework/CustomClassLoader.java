/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CustomClassLoader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.JBIProvider;

import java.net.URL; 
import java.io.File; 
import java.io.InputStream; 
import java.io.IOException; 
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of a custom class loader.
 * There is one instance of this classloader per component lifecycle
 * /bootstrap/shared library.
 * This classloader subclasses java.net.URLClassLoader and implements
 * functionality to force 'local' loading of classes at the component
 * level before delegating it to the parent classloader .
 * This is to allow the component/SL to load specific versions of libraries
 * such as Xerces rather than be forced to use the JVM/Application Server
 * version of the same .
 *
 * @author Sun Microsystems, Inc.
 */
public final class CustomClassLoader extends java.net.URLClassLoader
{

    /**
     *  String Translator
     */
    private StringTranslator mTranslator;
    
    /**
     *  If this flag is <code>true<code>, the class loader will first 
     *  ask its parent to try and load the class/resource . If not, 
     *  the class loader will try and load the class/resource itself
     *  before delegating to the parent class loader .
     */
    private boolean mSelfFirst = false;

    /**
     *  handle to system classloader flag 
     */
    private ClassLoader mSystemLoader;

    /**
     *  handle to parent of this class loader
     */
    private ClassLoader mParentLoader;

    /**
     * list of filtered classes
     */
    private String[] mFilteredPkgs = {"java.",       // core Java pkgs
                                      "javax.",      // Java extensions
                                      "sun."  ,      // Sun specific JDK 
    };
    /**
     *  handle to system logger
     */
    private Logger mLogger;
    
    /**
     *  Used to load resources safely (without a dangling JarFile ref).
     */
    private JarCache mJarCache;

    /**
     * Constructor.
     * @param  urls the urls to construct this class loader with
     * @param  parentCL the parent of the component class loader
     * @param  selfFirst flag indicating whether the component
     * should try loading libraries itself first or delegate to 
     * its parent .
     */
    public CustomClassLoader( URL[] urls , ClassLoader parentCL ,
                              boolean selfFirst )
    {
        super ( urls , parentCL );
        mSelfFirst = selfFirst;
        mSystemLoader = getSystemClassLoader();
        mParentLoader = (parentCL != null) ? parentCL : mSystemLoader;

        EnvironmentContext ec = EnvironmentContext.getInstance();
        mTranslator = (StringTranslator) ec.getStringTranslatorFor(this);
        mLogger     = (Logger) ec.getLogger();
        
        initJarCache(urls);
    }


    /**
     * Overloaded loadClass method  that works similarly to the 
     * <code>loadClass(String, boolean)</code> method with <code>
     * false</code> asthe second argument.
     *
     * @param  name The name of class to load.
     * @return The loaded class.
     * @throws ClassNotFoundException If the class cannot be loaded  by this
     * classloader.
     */ 
    public Class loadClass(String name)
        throws ClassNotFoundException
    {
        return loadClass (name, false);
    }

    /**
     * Overloaded loadClass method  that implements the 'selfFirst=true/false'
     * functionality . We need to overload loadClass instead of findClass
     * since we want to interfere with the normal Java2 class loading 
     * sequence . By the time the findClass() method is called, the parent
     * class loader has already been asked to try and load the class - this
     * is exactly what we are trying to circumvent .
     *
     * @see java.lang.ClassLoader#loadClass(String, boolean) 
     * @see java.lang.ClassLoader#findClass(String) 
     *
     *
     * The sequence of looking for a class is as follows -
     * <ul>
     * <li>call findLoadedClass(String) to check if the class has
     * already been loaded. 
     * <li>if 'selfFirst=false' hand off the loading to the parent i.e.
     * the Delegating ClassLoader for this classloader. If parent is null
     * the class loader built into the VM is used instead.
     * <li>call findClass() to try loading the class locally, if it 
     * succeeds , hand back the class 
     * <li> finally hand control to parent and let the normal delegation 
     * class loading continue.
     * <li> if class was not found in above sequence, throw a 
     * ClassNotFoundException 
     * <li> resolveClass is called at each stage before returning the 
     * loaded class 
     * </ul>
     *
     * @param  name The name of class to load.
     * @param  resolve whether or not to reolve the loaded class.
     * @return The loaded class.
     * @throws ClassNotFoundException If the class cannot be loaded  by this
     * classloader.
     */ 

    protected synchronized Class loadClass( String name, boolean resolve)
        throws ClassNotFoundException
    {
        Class componentClass = null;

        mLogger.finest("Custom ClassLoader - loadClass: " + name);

        // 1. call findLoadedClass, return if found
        //
        componentClass = findLoadedClass( name );
             
        if ( componentClass != null)
        {
            if (resolve) 
            {
                resolveClass( componentClass );
            }
            mLogger.finest("Class " + name + " will be loaded from cache");
            return componentClass;
        }

        // if the class is 'filtered' i.e. java/javax etc
        // 1. first hand it off to the parent .
        // 2. if the parent fails to load it, have the component/SL
        // attempt to load it . 
        boolean isFiltered = false;

        if (isFilteredClassOrResource( name ) )
        {
            isFiltered = true;
            try
            {
                componentClass =  mParentLoader.loadClass (name);
                return componentClass;
            }
            catch (ClassNotFoundException cnfe)
            {
                ; // ignore and allow self-first loading
            }            
        }

        // 2. if 'selfFirst = false' , send it to the parent classloader
        // if the class is a filtered class, we have already tried the parent
        // so there is no need to do that again.
        try
        {
            if (!mSelfFirst && !isFiltered)
            {
                componentClass = mParentLoader.loadClass( name );
                if (resolve) 
                {
                    resolveClass( componentClass );
                }
                mLogger.finest("Class " + name + " will be loaded using " +
                    mParentLoader.toString());
                return componentClass;
            }
        }
        catch (ClassNotFoundException cnfe)
        {
            ; // ignore 
        }

        // if 'selfFirst = 'true' , try loading it locally first
        //
        try
        {
            componentClass = findClass( name );
            if (componentClass != null)
            {
                if (resolve)
                {
                    resolveClass( componentClass );
                }

                mLogger.finest("Class " + name + " will be loaded using " +
                    this.toString());
                return componentClass;
            }
        }
        catch (ClassNotFoundException cnfe)
        {
            ; // ignore
        }

        // finally delegate to the parent if not 
        // found locally . we do this only if the 
        // selfFirst flag is OFF  and the class is not filtered
        // because otherwise, the parent classloader would have 
        // already been searched and we dont want to do this again .
        //
        if (mSelfFirst && !isFiltered) 
        {
            try
            {
                componentClass = mParentLoader.loadClass( name );
                if (resolve)
                {
                    resolveClass(componentClass);    
                }

                mLogger.finest("Class " + name + " will be loaded using " +
                    mParentLoader.toString());
                return componentClass;
            }
            catch (ClassNotFoundException cnfe)
            {
                ; // ignore
            }
        }

        //  all efforts are in vain - class was not found 
        throw new ClassNotFoundException(name);
    }

    /**
     * Overloaded getResource method  that implements the 'selfFirst'
     * functionality . We need to overload getResource instead of findResource
     * since we want to interfere with the normal Java2 class loading 
     * sequence . By the time the findResource() method is called, the parent
     * class loader has already been asked to try and load the class - this
     * is exactly what we are trying to circumvent .
     *
     * @see java.lang.ClassLoader#getResource(String) 
     *
     * The sequence of looking for a resource is as follows -
     * <ul>
     * <li>if 'selfFirst=false' hand off the loading to the parent i.e.
     * the Delegating ClassLoader for this classloader. If parent is null
     * the class loader built into the VM is used instead.
     * <li>call findResource() to try loading the resource locally, if it 
     * succeeds , hand back the resource , 
     * <li> finally hand control to parent and let the 
     * normal delegation class loading continue.
     * <li> if the resource was not found in above sequence, return NULL
     * </ul>
     *
     * @param  name The name of resource to load.
     * @return The loaded resource.
     */ 
    public URL getResource( String name ) 
    {
        URL retURL = null;

        // if the resource is 'filtered' i.e. java/javax etc
        // 1. first hand it off to the parent .
        // 2. if the parent fails to load it, have the component/SL
        // attempt to load it . 
        boolean isFiltered = false;

        if (isFilteredClassOrResource( name ) )
        {
            isFiltered = true;
            retURL =  mParentLoader.getResource(name);
            if (retURL != null)
            {
                return retURL;
            }
        }

        // 1. if 'selfFirst = false' , send it to the parent classloader
        // if the resource is filtered, then dont bother since we have
        // already tried the parent classloader.
        if (!mSelfFirst && !isFiltered)
        {
            retURL = mParentLoader.getResource( name );
            if (retURL != null)
            {
                return retURL;
            }
        }

        // If 'selfFirst = 'true' , try loading it locally first
        // Check the jar cache first
        retURL = mJarCache.findResource( name );
        if (retURL == null)
        {
            // jar cache didn't pick it up -- try the URL Classloader just
            // in case it's included as a file and not a jar.
            retURL = findResource(name);
        }
        
        if (retURL != null)
        {
            return retURL;
        }

        // finally delegate to the parent if not 
        // found locally . we do this only if the 
        // selfFirst flag is ON because otherwise ,
        // the parent classloader would have already been
        // searched and we dont want to do this again .
        //
        if (mSelfFirst && !isFiltered) 
        {
            retURL = mParentLoader.getResource( name );
            if (retURL != null)
            {
                return retURL;
            }
        }

        //  all efforts are in vain - resource was not found 
        return null;
    }


    /**
     * Overloaded getResourceAsStream method  for the 'selfFirst'
     * functionality . We need to overload getResourceAsStream 
     * since we want to interfere with the normal Java2 class loading 
     * sequence . 
     *
     * @see java.lang.ClassLoader#getResourceAsStream(String) 
     *
     *
     * The sequence of looking for a resource is as follows -
     * <ul>
     * <li>if 'selfFirst=false' hand off the loading to the parent i.e.
     * the Delegating ClassLoader for this classloader. If parent is null
     * the class loader built into the VM is used instead.
     * <li>call findResourceAsStream() to try loading the resource locally, 
     * if it succeeds , hand back the resource , 
     * <li> finally hand control to parent and let the 
     * normal delegation class loading continue.
     * <li> if the resource was not found in above sequence, return NULL
     * </ul>
     *
     * @param  name The name of resource to load.
     * @return The loaded resource.
     */ 
    public InputStream getResourceAsStream( String name ) 
    {
        InputStream retStream = null;

        // if the resource is 'filtered' i.e. java/javax etc
        // 1. first hand it off to the parent .
        // 2. if the parent fails to load it, have the component/SL
        // attempt to load it . 
        boolean isFiltered = false;

        if (isFilteredClassOrResource( name ) )
        {
            isFiltered = true;
            retStream =  mParentLoader.getResourceAsStream(name);
            if (retStream != null)
            {
                return retStream;
            }
        }

        // 1. if 'selfFirst = false' , send it to the parent classloader
        // if the resource is filtered, then dont bother since we have
        // already tried the parent classloader.
        // 
        if (!mSelfFirst && !isFiltered)
        {
            retStream = mParentLoader.getResourceAsStream( name );
            if (retStream != null)
            {
                return retStream;
            }
        }

        // If 'selfFirst = 'true' , try loading it locally first.
        // Check the jar cache for the resource
        URL resURL = mJarCache.findResource( name );
        if (resURL == null)
        {
            // jar cache didn't pick it up -- try the URL Classloader just
            // in case it's included as a file and not a jar.
            resURL = findResource(name);
        }
        
        if (resURL != null)
        {
            try
            {
                return resURL.openStream();
            }
            catch (IOException ioe)
            {
                return null;
            }
        }

        // finally delegate to the parent if not 
        // found locally . we do this only if the
        // selfFirst flag is ON because otherwise ,
        // the parent classloader would have already been
        // searched and we dont want to do this again .
        // dont bother about filtered classes since the 
        // parent classloader has already tried loading it
        if (mSelfFirst && !isFiltered) 
        {
            retStream = mParentLoader.getResourceAsStream( name );
            if (retStream != null)
            {
                return retStream;
            }
        }

        //  all efforts are in vain - resource was not found 
        return null;
    } 
   
    /**
     * This method checks if a class name is among the filtered classes.
     * Typical examples of this are the "java.X , javax.X , sun.X" classes
     * of resource.
     * 
     * @param  name The fully qualified name of class/resource to filter.
     * @return true if the class/resource is in the filtered list , false
     * otherwise.
     */ 
    private boolean isFilteredClassOrResource (String name)
    {
        boolean isFilteredPkg = false;
        String pkgPrefix = name.substring(0, name.indexOf(".") + 1);

        for (int i = 0; i < mFilteredPkgs.length; i++)
        {
            String nextPkg = (String) mFilteredPkgs[i];
            if (nextPkg.equalsIgnoreCase(pkgPrefix))
            {
                isFilteredPkg = true;             
                break;
            }
        }
        return isFilteredPkg;
    }
    
    /** Releases all resources currently held in this classloader's cache. */
    public void releaseResources()
    {
        if (EnvironmentContext.getInstance().getProvider() == JBIProvider.SUNAS)
        {
            // Extra clean-up available on Glassfish
            com.sun.appserv.ClassLoaderUtil.releaseLoader(
                    (java.net.URLClassLoader) this);
        }
        
        mJarCache.close();
    }
    
    /** Initialize the JAR cache by adding entries for each jar in this
     *  classloader's path.
     */
    private void initJarCache(URL[] urls)
    {
        mJarCache = new JarCache();
        
        // iterate through each URL in the classpath
        for (URL url : urls)
        {
            try
            {
                // make sure the entry exists and that it's a jar
                File file = new File(url.getFile());
                if (file.exists() && file.getName().endsWith(".jar"))
                {
                    mJarCache.addJar(file.getPath());
                }
            }
            catch (java.io.IOException ioEx)
            {
                mLogger.log(Level.FINE, mTranslator.getString(
                    LocalStringKeys.CU_CL_CREATE_IO_ERROR, url.getFile()), ioEx);
            }
        }
    }
}
