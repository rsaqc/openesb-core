/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * Tests for ComponentStatistics.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentStatistics
    extends junit.framework.TestCase
{
    /**
     * Instance of ComponentStatistics.
     */
    private ComponentStatistics mStats;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentStatistics(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the ComponentStatistics instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

        mStats = new ComponentStatistics("TestComponent");
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Tests get/setLastRestartTime.
     * @throws Exception if an unexpected error occurs.
     */
    public void testLastRestartTime()
    {
        java.util.Date d = new java.util.Date();
        mStats.setLastRestartTime(d);
        assertEquals("Failure on set/getLastRestartTime: ",
            d, mStats.getLastRestartTime());
    }

    /**
     * Tests get/incrementInitRequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitRequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getInitRequests: ",
            new Integer(n),
            new Integer(mStats.getInitRequests()));
        mStats.incrementInitRequests();
        ++n;
        assertEquals("Failure on incrementInitRequests: ",
            new Integer(n),
            new Integer(mStats.getInitRequests()));
    }

    /**
     * Tests get/incrementStartRequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartRequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getStartRequests: ",
            new Integer(n),
            new Integer(mStats.getStartRequests()));
        mStats.incrementStartRequests();
        ++n;
        assertEquals("Failure on incrementStartRequests: ",
            new Integer(n),
            new Integer(mStats.getStartRequests()));
    }

    /**
     * Tests get/incrementStopRequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopRequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getStopRequests: ",
            new Integer(n),
            new Integer(mStats.getStopRequests()));
        mStats.incrementStopRequests();
        ++n;
        assertEquals("Failure on incrementStopRequests: ",
            new Integer(n),
            new Integer(mStats.getStopRequests()));
    }

    /**
     * Tests get/incrementShutDownRequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownRequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getShutDownRequests: ",
            new Integer(n),
            new Integer(mStats.getShutDownRequests()));
        mStats.incrementShutDownRequests();
        ++n;
        assertEquals("Failure on incrementShutDownRequests: ",
            new Integer(n),
            new Integer(mStats.getShutDownRequests()));
    }

    /**
     * Tests get/incrementFailedRequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testFailedRequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getFailedRequests: ",
            new Integer(n),
            new Integer(mStats.getFailedRequests()));
        mStats.incrementFailedRequests();
        ++n;
        assertEquals("Failure on incrementFailedRequests: ",
            new Integer(n),
            new Integer(mStats.getFailedRequests()));
    }

    /**
     * Tests get/incrementTimedOutRequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testTimedOutRequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getTimedOutRequests: ",
            new Integer(n),
            new Integer(mStats.getTimedOutRequests()));
        mStats.incrementTimedOutRequests();
        ++n;
        assertEquals("Failure on incrementTimedOutRequests: ",
            new Integer(n),
            new Integer(mStats.getTimedOutRequests()));
    }

    /**
     * Tests get/increment/decrementDeployedSUs.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeployedSUs()
        throws Exception
    {
        short n = 0;
        assertEquals("Failure on getDeployedSUs: ",
            new Short(n),
            new Short(mStats.getDeployedSUs()));
        mStats.incrementDeployedSUs();
        ++n;
        assertEquals("Failure on incrementDeployedSUs: ",
            new Short(n),
            new Short(mStats.getDeployedSUs()));
        mStats.decrementDeployedSUs();
        --n;
        assertEquals("Failure on decrementDeployedSUs: ",
            new Short(n),
            new Short(mStats.getDeployedSUs()));
    }

    /**
     * Tests get/incrementDeploySURequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeploySURequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getDeploySURequests: ",
            new Integer(n),
            new Integer(mStats.getDeploySURequests()));
        mStats.incrementDeploySURequests();
        ++n;
        assertEquals("Failure on incrementDeploySURequests: ",
            new Integer(n),
            new Integer(mStats.getDeploySURequests()));
    }

    /**
     * Tests get/incrementUndeploySURequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeploySURequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getUndeploySURequests: ",
            new Integer(n),
            new Integer(mStats.getUndeploySURequests()));
        mStats.incrementUndeploySURequests();
        ++n;
        assertEquals("Failure on incrementUndeploySURequests: ",
            new Integer(n),
            new Integer(mStats.getUndeploySURequests()));
    }

    /**
     * Tests get/incrementInitSURequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitSURequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getInitSURequests: ",
            new Integer(n),
            new Integer(mStats.getInitSURequests()));
        mStats.incrementInitSURequests();
        ++n;
        assertEquals("Failure on incrementInitSURequests: ",
            new Integer(n),
            new Integer(mStats.getInitSURequests()));
    }

    /**
     * Tests get/incrementStartSURequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartSURequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getStartSURequests: ",
            new Integer(n),
            new Integer(mStats.getStartSURequests()));
        mStats.incrementStartSURequests();
        ++n;
        assertEquals("Failure on incrementStartSURequests: ",
            new Integer(n),
            new Integer(mStats.getStartSURequests()));
    }

    /**
     * Tests get/incrementStopSURequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopSURequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getStopSURequests: ",
            new Integer(n),
            new Integer(mStats.getStopSURequests()));
        mStats.incrementStopSURequests();
        ++n;
        assertEquals("Failure on incrementStopSURequests: ",
            new Integer(n),
            new Integer(mStats.getStopSURequests()));
    }

    /**
     * Tests get/incrementShutDownSURequests.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownSURequests()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getShutDownSURequests: ",
            new Integer(n),
            new Integer(mStats.getShutDownSURequests()));
        mStats.incrementShutDownSURequests();
        ++n;
        assertEquals("Failure on incrementShutDownSURequests: ",
            new Integer(n),
            new Integer(mStats.getShutDownSURequests()));
    }

    /**
     * Test resetAllStatistics.
     * @throws Exception if an unexpected error occurs.
     */
    public void testResetAllStatistics()
    {
        // First, populate one field in the framework statistics and one in
        // the message statistics. This is all that is necessary to verify
        // that both resetFrameworkStatistics() and resetMessagingStatistics()
        // were called.

        mStats.incrementInitRequests();

        // Now reset all statistics.

        mStats.resetAllStatistics();

        // Verify that both sets were reset.

        Integer expected = new Integer(0);
        assertEquals("framework statistics not reset",
            expected, new Integer(mStats.getInitRequests()));
    }

    /**
     * Test resetFrameworkStatistics.
     * @throws Exception if an unexpected error occurs.
     */
    public void testResetFrameworkStatistics()
    {
        // First, populate all the fields with values. These methods are all
        // tested in other junit tests so they are assumed to work here.

        mStats.incrementInitRequests();
        mStats.incrementStartRequests();
        mStats.incrementStopRequests();
        mStats.incrementShutDownRequests();
        mStats.incrementFailedRequests();
        mStats.incrementTimedOutRequests();
        mStats.incrementDeployedSUs();
        mStats.incrementDeploySURequests();
        mStats.incrementInitSURequests();
        mStats.incrementStartSURequests();
        mStats.incrementStopSURequests();
        mStats.incrementShutDownSURequests();
        mStats.incrementUndeploySURequests();
        mStats.incrementFailedSURequests();
        mStats.incrementTimedOutSURequests();
        mStats.incrementRegisteredServicesOrEndpoints();

        // Now reset all the fields, then check to see if they all got reset.

        mStats.resetFrameworkStatistics();
        Integer expected = new Integer(0);
        assertEquals("InitRequests not reset",
            expected, new Integer(mStats.getInitRequests()));
        assertEquals("StartRequests not reset",
            expected, new Integer(mStats.getStartRequests()));
        assertEquals("StopRequests not reset",
            expected, new Integer(mStats.getStopRequests()));
        assertEquals("ShutDownRequests not reset",
            expected, new Integer(mStats.getShutDownRequests()));
        assertEquals("FailedRequests not reset",
            expected, new Integer(mStats.getFailedRequests()));
        assertEquals("TimedOutRequests not reset",
            expected, new Integer(mStats.getTimedOutRequests()));
        assertEquals("DeployedSUs not reset",
            expected, new Integer(mStats.getDeployedSUs()));
        assertEquals("DeploySURequests not reset",
            expected, new Integer(mStats.getDeploySURequests()));
        assertEquals("InitSURequests not reset",
            expected, new Integer(mStats.getInitSURequests()));
        assertEquals("StartSURequests not reset",
            expected, new Integer(mStats.getStartSURequests()));
        assertEquals("StopSURequests not reset",
            expected, new Integer(mStats.getStopSURequests()));
        assertEquals("ShutDownSURequests not reset",
            expected, new Integer(mStats.getShutDownSURequests()));
        assertEquals("UndeploySURequests not reset",
            expected, new Integer(mStats.getUndeploySURequests()));
        assertEquals("FailedSURequests not reset",
            expected, new Integer(mStats.getFailedSURequests()));
        assertEquals("TimedOutSURequests not reset",
            expected, new Integer(mStats.getTimedOutSURequests()));
        assertEquals("RegisteredServiceOrEndpoints not reset",
            expected, new Integer(mStats.getRegisteredServicesOrEndpoints()));
    }

}
