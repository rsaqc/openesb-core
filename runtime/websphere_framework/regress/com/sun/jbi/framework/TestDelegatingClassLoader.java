/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDelegatingClassLoader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.framework.ScaffoldPlatformContext;
import java.io.File;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sun.jbi.JBIProvider;

/**
 * Tests for the various methods on the DelegatingClassLoader class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestDelegatingClassLoader
    extends junit.framework.TestCase
{
    /**
     * Value of the $SRCROOT environment variable
     */
    private String mSrcroot;

    /**
     * ClassLoaderFactory
     */
    private ClassLoaderFactory mClassLoaderFactory;

    /**
     * EnvironmentContext
     */
    private EnvironmentContext mEnvironmentContext;
     
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestDelegatingClassLoader(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the ClassLoaderFactory instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mSrcroot = System.getProperty("junit.srcroot"); 
        mEnvironmentContext = new EnvironmentContext(new ScaffoldPlatformContext(), 
                new JBIFramework(), new Properties());
        mClassLoaderFactory = ClassLoaderFactory.getInstance();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }


// =====================  Component classloader test methods ==================

    /**
     * testDCLAddingSharedNameLoaders.
     * tests the adding of Shared Classloaders to the Delegating Classloader.
     */
    public void testDCLAddingSharedNameLoaders()
    {
        String testName = "testDCLAddingSharedNameLoaders";
        DelegatingClassLoader dcl = new DelegatingClassLoader(null);
        try
        {
            //create a valid sharednamespace
	    List sharedLibList = new ArrayList() ;
	    List sharedLibElements = new ArrayList() ;
	    String path = getTestJarsPath() + File.separator +  "a.jar" ;
	    sharedLibElements.add(path) ;
	    SharedLibrary sharedLib = new SharedLibrary("sl2" ,
                                                        "Test namespace",
                                                        "/sl2",
                                                        sharedLibElements );	
	    log ("created sharednamespace") ;

	    // create a shared classloader
            ClassLoader sharedCL = mClassLoaderFactory.createSharedClassLoader
		                                    (sharedLib) ;
	    // add it to the delegating classloader
            dcl.addSharedClassLoader(sharedCL);
	    log (testName + ":passed");
	}
	catch (Exception e)
	{
            fail (testName + ":error adding shared classloader to dcl") ;
	}
    }

    /**
     * testDCLAddingSharedNameLoadersNullCL
     * tests the adding of  null Shared Classloaders to the Delegating Classloader
     * @throws Exception if an unexpected error occurs
     */
    public void testDCLAddingSharedNameLoadersNullCL()
    {
	String testName = "testDCLAddingSharedNameLoadersNullCL";
        DelegatingClassLoader dcl = new DelegatingClassLoader(null);
	try
	{
	    // create a shared classloader
            ClassLoader sharedCL = mClassLoaderFactory.createSharedClassLoader
		                                    (null) ;
	    // add it to the delegating classloader
            dcl.addSharedClassLoader(sharedCL);
            fail (testName + ":error adding shared classloader to dcl") ;
	}
	catch (Exception e)
	{
	    log (testName + ":passed");
	    assertTrue("Wrong Exception caught:" + e.toString() ,
		       (-1 < e.getMessage().indexOf("Null argument"))) ;
	}
    }

    /**
     * testDCLLoadClass
     * tests the loading of classes corresponding to different classloaders
     * added to a DelegatingClassLoader
     * @throws Exception if an unexpected error occurs
     */
    public void testDCLLoadClass()
    {
	String testName = "testDCLLoadClass";
        DelegatingClassLoader dcl = new DelegatingClassLoader(null);
	try
	{
	    //create a valid sharednamespace
	    List sharedLibList1 = new ArrayList() ;
	    List sharedLibElements1 = new ArrayList() ;
	    String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
	    sharedLibElements1.add(path1) ;
	    SharedLibrary sharedLib1 = new SharedLibrary("sl11" ,
                                                         "Jar file A",
                                                         "/sl11",
                                                         sharedLibElements1 );
	    log ("created sharednamespace1") ;

	    // create a shared classloader(1)
            ClassLoader sharedCL1 = mClassLoaderFactory.createSharedClassLoader
		                                    (sharedLib1) ;
	    //create a valid sharednamespace
	    List sharedLibList2 = new ArrayList() ;
	    List sharedLibElements2 = new ArrayList() ;
	    String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
	    sharedLibElements2.add(path2) ;
	    SharedLibrary sharedLib2 = new SharedLibrary("sl12" ,
                                                         "Jar file B",
                                                         "/sl12",
                                                         sharedLibElements2 );
	    log ("created sharednamespace2") ;

	    // create a shared classloader(2)
            ClassLoader sharedCL2 = mClassLoaderFactory.createSharedClassLoader
		                                    (sharedLib2) ;

	    // add the above classloaders to the DCL
	    dcl.addSharedClassLoader( sharedCL1);
	    dcl.addSharedClassLoader( sharedCL2);

	    // load classes from a.jar and b.jar using the DCL
            Class testBJarClass = dcl.loadClass("engine1.Bootstrap1") ;
	    Object testBJarObj  = testBJarClass.newInstance() ;
	    log ("loaded engine") ;
            Class testAJarClass = dcl.loadClass("binding1.Bootstrap1") ;
	    Object testAJarObj  = testAJarClass.newInstance() ;
	    log ("loaded binding") ;
	    log (testName + " passed OK") ;
	}
	catch (Exception e)
	{
	    CLUtils.dumpStackTrace(e);
	    log (testName + ":failed");
	    fail(testName + " failed with exception:" + e.getMessage());
	}
    }


   /**
    * testCCLLoadJBIClassNoSharedLib
    * tests the loading of a JBI class by the DelegatingClassLoader
    * when there is no Shared Lib (and hence no shared class loader)
    * this tests the implementation of loadClass designed for exactly
    * this purpose.
    *  @throws Exception if an unexpected error occurs
    * 
    */
    public void testCCLLoadJBIClassNoSharedLib()
                   

    {
        String testName = "testCCLLoadJBIClassNoSharedLib" ;
        String className = "com.sun.jbi.ServiceLifecycle" ;

        try
	{
           DelegatingClassLoader dcl = new DelegatingClassLoader(null);   
	   Class jbiClass = dcl.loadClass(className) ;
	   log ( className + " loaded by :" + jbiClass.getClassLoader());
	   log (testName + " Passed") ;
	}
	catch (Exception e)
	{
	    fail(testName + " failed with exception:" + e.toString());
	}

    } 

    /**
     * testDCLLoadClassWithSharedLibSelfFirstTrue
     * tests the loading of classes corresponding to different classloaders
     * added to a DelegatingClassLoader. 
     * The "selfFirst" Flag is TRUE at the SharedLibrary Level 
     * @throws Exception if an unexpected error occurs
     */
    public void testDCLLoadClassWithSharedLibSelfFirstTrue()
    {
	String testName = "testDCLLoadClassWithSharedLibSelfFirstTrue";
	boolean selfFirst = true ; // selfFirst = true
	String expectedClassLoader = "CustomClassLoader" ;
	String className = "com.sun.workflow.classes.Utils" ;
	String library = "utilsv2.jar" ;
	testDCLLoadClassWithSharedLib( testName , selfFirst , expectedClassLoader, className, library);
    }

    /**
     * testDCLLoadClassWithSharedLibSelfFirstFalse
     * tests the loading of classes corresponding to different classloaders
     * added to a DelegatingClassLoader. 
     * The "selfFirst" Flag is FALSE at the SharedLibrary Level 
     * @throws Exception if an unexpected error occurs
     */
    public void testDCLLoadClassWithSharedLibSelfFirstFalse()
    {
	String testName = "testDCLLoadClassWithSharedLibSelfFirstFalse";
	boolean selfFirst = false ; // selfFirst = false
	String expectedClassLoader = "sun.misc" ;
	String className = "com.sun.workflow.classes.Utils" ;
	String library = "utilsv2.jar" ;
	testDCLLoadClassWithSharedLib( testName , selfFirst , expectedClassLoader, className , library);
    }

    /**
     * testDCLLoadClassWithPrivateSelfFirstFalseSharedLibSelfFirstTrue
     * tests if a class is loaded correctly by the shared loader when the
     * same class is present in the component private path , shared lib ,
     * and the parent of the shared lib (system class loader) .
     *
     * The "selfFirst" Flag is TRUE at the SharedLibrary Level 
     * The "parentFirst" Flag is TRUE at the Component Level 
     *` 
     * @throws Exception if an unexpected error occurs
     */
    public void testDCLLoadClassWithPrivateSelfFirstFalseSharedLibSelfFirstTrue()
    {
	String testName = "testDCLLoadClassWithPrivateAndSharedLibSelfFirstTrue";
	try
	{
	    start( testName ) ;
	    boolean componentSelfFirst   = false ;  // parentFirst = true
	    boolean sLselfFirst          = true  ;  // selfFirst = true
	  
            // create shared lib (selfFirst = true) 
            // and associated delegating class loader
	    List libElements = new ArrayList() ; // component class path

	    String libName  = "utils.jar" ;
	    libElements.add(libName) ;

	    String root = "/sl13" ;

	    // create Shared Library
	    SharedLibrary sl = createSharedLibrary("sl13", libElements , root , sLselfFirst);
        
	    // create a shared class loader for the above shared lib
            ClassLoader scL = mClassLoaderFactory.createSharedClassLoader(
			    sl) ;
	    
	    // add to shared lib list
	    List slList = new ArrayList() ;
    	    slList.add(sl.getName()) ;

	    // create component (parentFirst) with shared lib as parent 
	    Component comp = createComponent("comp0001", libElements);
            comp.setSharedLibraryNames(slList);
	    comp.setComponentClassLoaderSelfFirst(componentSelfFirst);


	    // create a class loader for the above component
            ClassLoader ccL = mClassLoaderFactory.createComponentClassLoader(
			       comp) ;
        	
    	    String className = "com.sun.workflow.classes.Utils" ;
	    Class loadedClass = ccL.loadClass(className) ;
	    String expectedClassLoader = "CustomClassLoader" ;

	    String actualClassLoader = loadedClass.getClassLoader().toString();
	    log (testName + " actual ClassLoader = " + actualClassLoader) ;
	    if (actualClassLoader.indexOf(expectedClassLoader) == -1)
	    {
               fail ("Class :" + className + " was loaded by wrong class loader:" + loadedClass.getClassLoader().toString()) ;      
	    }

	    log (testName + " Passed") ;
	}
	catch (Exception e)
	{
	    CLUtils.dumpStackTrace(e) ;
            fail (testName + " Failed due to " + e.toString()) ;
	}
    }

    /**
     * testDCLLoadClassWithSharedLib
     * tests the loading of classes java.xxx class
     * @throws Exception if an unexpected error occurs
     */
    private void testDCLLoadClassWithSharedLib(String testName , boolean selfFirst , String expectedClassLoader, String className , String library)
    {

        DelegatingClassLoader dcl = new DelegatingClassLoader(null);
	try
	{
	    //create a valid sharednamespace
	    List sharedLibList1 = new ArrayList() ;
	    List sharedLibElements1 = new ArrayList() ;
	    String path1 = getTestJarsPath() + File.separator + library ;
	    sharedLibElements1.add(path1) ;
	    String slName = testName + "SL" ;
	    SharedLibrary sharedLib1 = new SharedLibrary(testName ,
                                                         "UtilsV2",
                                                         "/"+ slName,
							 selfFirst ,
                                                         sharedLibElements1 );
	    log ("created sharednamespace1") ;

	    // create a shared classloader(1)
            ClassLoader sharedCL1 = mClassLoaderFactory.createSharedClassLoader
		                                    (sharedLib1) ;

	    // add the above classloaders to the DCL
	    dcl.addSharedClassLoader( sharedCL1);

	    // load classes from utilsv2.jar using DCL
            Class testUtilsClass = dcl.loadClass(className) ;
	    Object testUtilsObj  = testUtilsClass.newInstance() ;
	    log ("loaded:" + className) ;

	    String loader = testUtilsClass.getClassLoader().toString();

	    if (loader.indexOf(expectedClassLoader) != -1)
	    {
	        log (testName + " passed OK") ;
	    }
	    else 
	    {
                fail (testName + " failed - wrong class loader") ;
	    }
	}
	catch (Exception e)
	{
	    CLUtils.dumpStackTrace(e);
	    log (testName + ":failed");
	    fail(testName + " failed with exception:" + e.toString());
	}
    }

    // creates a shared library and a 
    // class loader associated with it 
    //
    private SharedLibrary createSharedLibrary(String name,
		    List sharedJars , String root , boolean selfFirst) 
	    throws Exception
    {
        DelegatingClassLoader dcl = new DelegatingClassLoader(null);
	try
	{
	    // create a valid sharednamespace
	    List sharedLibList = new ArrayList() ;
	    List sharedLibElements = new ArrayList() ;

	    for (Iterator i = sharedJars.iterator() ;
			    i.hasNext() ;)
	    {
                String lib = (String) i.next() ;
	        String path = getTestJarsPath() + File.separator +  lib ;
	        sharedLibElements.add(path) ;
	    }

	    SharedLibrary sharedLib = new SharedLibrary(name,
                                                        "Test namespace",
                                                        root,
							selfFirst,
                                                        sharedLibElements );	
	    log ("created sharednamespace") ;

	    // create a shared classloader
            /*ClassLoader sharedCL = mClassLoaderFactory.createSharedClassLoader
		                                    (sharedLib) ;
	    // add it to the delegating classloader
            dcl.addSharedClassLoader(sharedCL); */
	    log ("created sharednamespace");
	    return sharedLib ;
	}
	catch (Exception e)
	{
             log (e.toString());
             throw e ;
	}
    }

    // create  and return a component
    //
    Component createComponent(String name , List jars)
    {
        List cpList = new ArrayList() ;

	for (Iterator i = jars.iterator() ;i.hasNext() ;)
	{
            String libName = (String)i.next();
            String path = getTestJarsPath() + File.separator + libName ;
            cpList.add (path) ;
	}

	Component comp = new Component() ;
        comp.setName(name) ;
        comp.setComponentClassPathElements(cpList);
        log ("created component") ;
	return comp ;
    }
	

    /**
     * private logging method
     */ 
    private static void log ( String msg )
    {
	CLUtils.log ("[TestDelegatingClassLoader]-" + msg);
    }

    /**  
     * private logging method
     */ 
    private String getTestJarsPath ()
    {
        return ( CLUtils.getTestJarsPath (mSrcroot) ) ;
    }

    /*  
     * private logging method to indicate start of test
     */ 
    private void start (String testName)
    {
        log (testName + "-" + "Started") ;
    }

    /*  
     * private logging method to indicate normal start of test
     */ 
    private void endOK (String testName)
    {
        log (testName + "-" + "Ended OK") ;
    }

    /*  
     * private logging method to indicate end of test due to an exception
     */ 
    private void endException (String testName , Exception e)
    {
        log (testName + "-" + "Ended With Following Exception") ;
        log (e.toString()) ;
    }
}
