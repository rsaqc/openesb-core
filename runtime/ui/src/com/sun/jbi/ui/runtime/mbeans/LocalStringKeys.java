/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.mbeans;

/**
 * This interface contains the property keys used for looking up message
 * text in the LocalStrings resource bundle.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{
    /** instance is down */
    public String STATS_SERVER_DOWN = "ui.mbean.system.stats.server.is.down";
    
    /** target not supported */
    public String STATS_TARGET_NOT_SUPPORTED = "ui.mbean.system.stats.target.type.not.supported";
    
    /** invalid target */
    public String STATS_INVALID_TARGET = "ui.mbean.invalid.target.error";
    
    /** comp type not known */
    public String STATS_COMP_TYPE_NOT_KNOWN =  "ui.mbean.system.stats.comp.type.not.known";
    
    /** comp stats not available */
    public String STATS_INVALID_COMP_TYPE = "ui.mbean.system.stats.not.available.for.component";
    
    /** stats mbean not present */
    public String STATS_MBEAN_NOT_PRESENT = "ui.mbean.system.stats.mbean.not.present";
    
    /** component not started */
    public String STATS_COMP_NOT_STARTED = "ui.mbean.system.stats.comp.not.started";

    /**  error in composing stats */
    public String ERROR_IN_STATS_COMPOSING = "ui.mbean.system.stats.error.in.compose";

    /**  error in endpoint stats */
    public String ERROR_ENDPOINT_NOT_EXISTS = "ui.mbean.system.stats.endpoint.not.exists";
    
    /** component is not installed */
    public String STATS_COMP_NOT_INSTALLED = "ui.mbean.system.stats.comp.not.installed";

    /**  error in SA stats */
    public String ERROR_SA_NOT_EXISTS = "ui.mbean.system.stats.sa.not.exists";

    /** component is not installed in all targets */
    public String STATS_COMP_NOT_EXISTS = "ui.mbean.system.stats.comp.not.exists";
}
