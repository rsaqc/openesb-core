/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJavaEEVerifierMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package test;
import javax.management.MBeanException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularType;
import com.sun.jbi.ui.common.JBIRemoteException;

/**
 * This class is used to provide a dummy MBean implementation 
 * for testing JavaEEVerifier integration.
 * @author Sun Microsystems, Inc.
 */
   
public class TestJavaEEVerifierMBeanImpl implements TestJavaEEVerifierMBean
{    
    

    /***
     *  Verify  a service unit targeted to the JavaEE SE. This operation verifies the javaee/j2ee
     *  application and returns a CompositeData which has the verification status i.e. information
     *  related to the resources referenced by the application but are missing.
     *
     *  @param serviceAssemblyPath - this is the path to the SA.
     *  @param suZipName - this is the SU zip name
     *  @param target - identifies the target for the service unit verification. This could
     *           be the DAS server instance "server", a standalone or clustered instance name
     *           or cluster.
     *  @return a TabularData with the verification result. The CompositeType of TabularData
     *       The column names in composite data are - 
     *      "Ear Filename", "Referrence By","Referrence Class", 
     *      "JNDI Name", "JNDI Class Type", "Message", "Status"
     *
     */
    public TabularData verifyServiceUnit(
            String serviceAssemblyPath, 
            String suZipName, 
            String target) 
    throws MBeanException
    {
        
        String[] verifierTableItemaNames = new String[]
        {
            "_filename",
            "_referencerclass",
            "_referencer",
            "_jndiname",
            "_jndinameClass",
            "_message",
            "_status",
            "Ear Filename1",
            "Referrence Class1",
            "Referrence By1",
            "JNDI Name1",
            "JNDI Class Type1",
            "Message1",
            "Status1"
        };
        
        String[] verifierTableItemDescriptions = new String[]
        {
            "_filename",
            "_referencerclass",
            "_referencer",
            "_jndiname",
            "_jndinameClass",
            "_message",
            "_status",
            "Ear Filename1",
            "Referrence Class1",
            "Referrence By1",
            "JNDI Name1",
            "JNDI Class Type1",
            "Message1",
            "Status1"
            
        };
        
        OpenType[] verifierTableItemTypes = new OpenType[]
        {
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.INTEGER,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING            
                    
        };
        try
        {
        
            CompositeType cType = 
                    new CompositeType("JavaEEVerifierTableDataType",
                                      "Java EE Verification Report Table Type",
                                      verifierTableItemaNames,
                                      verifierTableItemDescriptions,
                                      verifierTableItemTypes);

            TabularType reportTableType = 
                    new TabularType(
                        "JavaEEVerifierTableType",
                        "Java EE Verification Report Table Type",
                        cType,
                        new String[]{"Referrence Class1","Referrence By1","JNDI Name1"});

            Object[] row1 = 
            {
                "ordernotifiy.ear", 
                "orders-ejb", 
                "OrderCheck.class", 
                "jms/retailorders", 
                "javax.jms.Queue", 
                "JNDI object not found", 
                new Integer(2),
                "ordernotifiy.ear", 
                "orders-ejb", 
                "OrderCheck.class", 
                "jms/retailorders", 
                "javax.jms.Queue", 
                "Reference to JNDI object not found", 
                "NOT VERIFIED"
            };

            Object[] row2 =
            {
                "classic.ear",
                "V35Collab", 
                "Codegen.class", 
                "env1/Logicalhost1/OraExt1", 
                "Oracle eWay", 
                "An external configuration for Oracle eWay *MAY* be specified",
                new Integer(1),
                "classic.ear",
                "V35Collab", 
                "Codegen.class", 
                "env1/Logicalhost1/OraExt1", 
                "Oracle eWay", 
                "An external configuration for Oracle eWay *MAY* be specified",
                "NOT VERIFIED"
            };

            CompositeData cData1 = new CompositeDataSupport(
                    cType, verifierTableItemaNames, row1);

            CompositeData cData2 = new CompositeDataSupport(
                    cType, verifierTableItemaNames, row2);

            TabularData reportTable = new TabularDataSupport(reportTableType);
            
            //negative test case
            if (suZipName.equals("negative-test-su.jar"))
            {
                return reportTable;
            }
            reportTable.put(cData1);
            reportTable.put(cData2);
            return reportTable;
        }
        catch (javax.management.openmbean.OpenDataException ex)
        {
            System.out.println("JavaEEVerifierMBean could not be added" + ex.getMessage());            
            throw new MBeanException(ex);
        }                
        
    }
}


