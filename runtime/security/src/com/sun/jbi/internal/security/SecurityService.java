/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityService.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.binding.security.SecurityHandler;
import com.sun.jbi.component.ComponentContext;
import java.util.HashMap;
import javax.jbi.JBIException;
import javax.management.MBeanServer;
import javax.management.StandardMBean;
import javax.management.ObjectName;

/**
 * Implementation of the SecurityService interface.
 *
 * @author Sun Microsystems, Inc.
 */
public final class SecurityService
    extends com.sun.jbi.management.system.ModelSystemService
    implements com.sun.jbi.binding.security.SecurityService,
        com.sun.jbi.ServiceLifecycle
    
{
  
    /**
     * The registry of ( ComponnetID: SecurityHandlers )
     */
    private HashMap mSecurityHandlers;
    
    /**
     * My Single Instance
     */
    private static SecurityService sInstance;
    
    /**
     * The Environment Context.
     */
    private com.sun.jbi.EnvironmentContext mEnvCtx;
    
    /** 
     * Ctor.
     */
    public SecurityService()
    {
        mSecurityHandlers = new HashMap();
    }
    
    /**
     * Get a instance of the Service. 
     * TBD::
     *
     * @return a SecurityService instance
     */
    public static SecurityService getInstance()
    {
        if ( sInstance == null )
        {
            sInstance = new SecurityService();
        }
        return sInstance;
    }
    
    /**
     * Create a SecurityHandler based on the SecurityConfiguration.
     *
     * @param compCtx is the ComponentContext of the Binding Component associated
     * with the Securityhandler
     * @param authLayer is the type of layer that requires to use the security services.
     * @return a instance of the newly created SecurityHandler
     * @throws IllegalStateException if the SecurityHandler is in a invalid
     * state on creation.
     */
    public SecurityHandler createSecurityHandler(ComponentContext compCtx, String authLayer)
        throws IllegalStateException
    {
        SecurityHandler secHandler = new SecurityHndlr(
            compCtx);
        if ( compCtx != null )
        {
            mSecurityHandlers.put(compCtx.getComponentName(), secHandler);
        }
        return secHandler;
    }
    
    /**
     * Create a SecurityHandler.
     *
     * @param compCtx the ComponentContext of the Binding Component associated
     * with the Securityhandler.
     * @return a instance of the newly created SecurityHandler
     * @throws IllegalStateException if the SecurityHandler is in a invalid
     * state on creation.
     */
    public SecurityHandler createSecurityHandler(ComponentContext compCtx)
        throws IllegalStateException
    {
        
        return this.createSecurityHandler (compCtx, "SOAP");
    }
    
    /**
     * Get the SecurityHandler for a Component.
     *
     * @param componentId is the Component Id
     * @return a instance of the components SecurityHandler, if it exists null otherwise.
     */
    public SecurityHandler getSecurityHandler(String componentId)
    {
       return  (SecurityHandler) mSecurityHandlers.get(componentId);
    }
    
    /**
     * Remove the SecurityHandler for the component, this method should be called when 
     * the component is shutdown or uninstalled.
     *
     * @param componentId is the Component Id
     */
    public void removeSecurityHandler(String componentId)
    {
        mSecurityHandlers.remove(componentId);
    }
    
    /**
     * Initialize the Service.
     *
     * @param aContext is the Environment Context.
     */ 
    public void initService (com.sun.jbi.EnvironmentContext aContext) 
        throws javax.jbi.JBIException
    {
        mEnvCtx = aContext;
        
        //-- no-op
    }
    
    /**
     * Start the Service.
     */
    public void startService () throws javax.jbi.JBIException
    {
        //-- no-op
    }
    
    /**
     * Stop the security service.
     */
    public void stopService () throws javax.jbi.JBIException
    {
        //-- no-op
    }
}
