/*
 * BEGIN_HEADER - DO NOT EDIT

 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.

 * You can obtain a copy of the license at
 * http://opensource.org/licenses/cddl1.php.
 * See the License for the specific language governing
 * permissions and limitations under the License.

 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * http://opensource.org/licenses/cddl1.php.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package org.openesb.runtime.tracking.util;

import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.NormalizedMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Node;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */
public class NormalizedMessageUtil {

    private static DocumentBuilder db = null;

    static {
        try {
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(NormalizedMessageUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean hasInMessage(MessageExchange mex) {
        if (mex instanceof InOnly) {
            return ((InOnly) mex).getInMessage() != null;
        } else if (mex instanceof InOut) {
            return ((InOut) mex).getInMessage() != null;
        }
        return false;
    }

    public static boolean hasOutMessage(MessageExchange mex) {
        if (mex instanceof InOut) {
            return ((InOut) mex).getOutMessage() != null;
        }
        return false;
    }

    public static boolean hasFaultMessage(MessageExchange mex) {
        return mex.getFault() != null;
    }

    private static Node getInMessage(MessageExchange mex) throws TransformerConfigurationException {
        Source s = null;

        if (mex instanceof InOnly) {
            s = ((InOnly) mex).getInMessage().getContent();
        } else if (mex instanceof InOut) {
            s = ((InOut) mex).getInMessage().getContent();
        } else {
            return null;
        }
        Transformer t = null;
        try {
            t = TransformerPool.get().retrieve();
            DOMResult dr = new DOMResult();
            t.transform(s, dr);
            return dr.getNode();
        } catch (TransformerConfigurationException ex) {
            return null;
        } catch (TransformerException ex) {
            return null;
        } finally {
            if (t != null) {
                TransformerPool.get().relinquish(t);
            }
        }
    }

    public static Source getInMessageCopy(MessageExchange mex) throws TransformerConfigurationException {
        Node n = getInMessage(mex);
        if (n == null) {
            return null;
        }
        return new DOMSource(n);

    }

    private static Node getOutMessage(MessageExchange mex) throws TransformerConfigurationException {
        Source s = null;

        if (mex instanceof InOut) {
            NormalizedMessage nm = ((InOut)mex).getOutMessage();
            if (nm != null) {
                s = nm.getContent();
            }

        }
        if (s == null) {
            return null;
        }
        Transformer t = null;
        try {
            t = TransformerPool.get().retrieve();
            DOMResult dr = new DOMResult();
            t.transform(s, dr);
            return dr.getNode();
        } catch (TransformerConfigurationException ex) {
            return null;
        } catch (TransformerException ex) {
            return null;
        } finally {
            if (t != null) {
                TransformerPool.get().relinquish(t);
            }
        }
    }

    public static Source getOutMessageCopy(MessageExchange mex) throws TransformerConfigurationException {
        Node n = getOutMessage(mex);
        if (n == null) {
            return null;
        }
        return new DOMSource(n);
    }

    private static Node getFaultMessage(MessageExchange mex) throws TransformerConfigurationException {
        Source s = null;
        if (mex.getFault() != null) {
            Fault f = mex.getFault();
            if (f != null) {
                s = f.getContent();
            }
        }

        if (s == null) {
            return null;
        }

        Transformer t = null;
        try {
            t = TransformerPool.get().retrieve();
            DOMResult dr = new DOMResult();
            t.transform(s, dr);
            return dr.getNode();
        } catch (TransformerConfigurationException ex) {
            return null;
        } catch (TransformerException ex) {
            return null;
        } finally {
            if (t != null) {
                TransformerPool.get().relinquish(t);
            }
        }
    }

    public static Source getFaultMessageCopy(MessageExchange mex) throws TransformerConfigurationException {
        Node n = getFaultMessage(mex);
        if (n == null) {
            return null;
        }
        return new DOMSource(n);
    }

    public static String toString(Source messageContent) {
        Transformer t = null;
        try {
            t = TransformerPool.get().retrieve();
            StringWriter sw = new StringWriter();

            t.transform(messageContent, new StreamResult(sw));
            return sw.toString();

        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(NormalizedMessageUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (TransformerException ex) {
            Logger.getLogger(NormalizedMessageUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            if (t != null) {
                try {
                    TransformerPool.get().relinquish(t);
                } catch (TransformerConfigurationException ex) {
                    Logger.getLogger(NormalizedMessageUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
