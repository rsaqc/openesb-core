/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Map;
import java.util.Set;
import javax.management.remote.*;
import javax.management.*;
import javax.management.openmbean.*;


/**
 */
public class TestComponentConfiguration
{
     
    private MBeanServerConnection mbns;
    private static final String RESULT_PREFIX = "##### Result of ";
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String TARGET   = "target";
    private static final String COMPONENT_NAME = "component.name";
    private static final String LOGGER_NAME = "logger.name";
    
    /** Application Variable Composite Type */
    private static CompositeType sAppVarComposite;
    
    /** Application Variable Tabular Type */
    private static TabularType   sAppVarTable;
    
    /** Application Variable Composite Item Names */
    private static String[] sAppVarItemNames;
    
    /** The index of the Application Variables **/
    private static String[] sappVarRowIndex;
    
    
   /** The application configuration TabularData */
    private TabularDataSupport mAppConfigs;
    
    
    /** Application Configuration Composite Type */
    private static CompositeType sAppConfigComposite;
    
    /** Application Configuration Tabular Type */
    private static TabularType   sAppConfigTable;
    
    /** Application Configuration Composite Item Names */
    private static String[] sAppConfigItemNames;
    
    /** The index of the Application Configurations **/
    private static String[] sAppConfigRowIndex;
    
    
    public TestComponentConfiguration()
    {
        try
        {
            initOpenTypes();
            initMBeanServerConnection();
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    
    private void initOpenTypes()
        throws javax.management.openmbean.OpenDataException
    {
            // Define the Application Variables CompositeType
            sAppVarItemNames = new String[] {"name", 
                                "value", 
                                "type" };

            String[] appVarItemDesc = { "Application variable name", 
                                        "Application variable value", 
                                        "Application variable type" };

            OpenType[] appVarRowAttrTypes = { 
                                        SimpleType.STRING, 
                                        SimpleType.STRING, 
                                        SimpleType.STRING };

            sappVarRowIndex = new String[]{ "name" };


            sAppVarComposite = new CompositeType("ApplicationVariableComposite",
                                                  "Application variable name and value pair",
                                                   sAppVarItemNames,
                                                   appVarItemDesc,
                                                   appVarRowAttrTypes);

            sAppVarTable = new TabularType("ApplicationVariableList",
                                           "List of application variables",
                                                    sAppVarComposite,
                                                    sappVarRowIndex);
            
                        // Define the Application Configuration OpenType
            sAppConfigItemNames = new String[] {"configurationName", 
                                "connectionURL", 
                                "securityPrincipal",
                                "securityCredential",
                                "jndienv"};

            String[] appConfigItemDesc = { "Application configuration name", 
                                        "Connection URL", 
                                        "Security Principal",
                                        "Security Credential",
                                        "JNDI Properties"};

                                        
            ArrayType jndiEnvArrayType = new ArrayType(1, SimpleType.STRING);                            
            OpenType[] appConfigRowAttrTypes = { 
                                        SimpleType.STRING, 
                                        SimpleType.STRING, 
                                        SimpleType.STRING,
                                        SimpleType.STRING, 
                                        jndiEnvArrayType};

            sAppConfigRowIndex = new String[]{ "configurationName" };


            sAppConfigComposite = new CompositeType("ApplicationConfigurationComposite",
                                                  "Application Configuration Composite",
                                                   sAppConfigItemNames,
                                                   appConfigItemDesc,
                                                   appConfigRowAttrTypes);

            sAppConfigTable = new TabularType("ApplicationConfigurationList",
                                           "List of application configurations",
                                                    sAppConfigComposite,
                                                    sAppConfigRowIndex);
    }
    
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    public ObjectName getComponentConfigurationName()
        throws Exception
    {
        String info = "com.sun.jbi:Target="        + System.getProperty(TARGET, "domain") + 
                                  ",ComponentName=" + System.getProperty(COMPONENT_NAME)   +
                                  ",ServiceType=Configuration";
        return new ObjectName(info);
    }
    
    public void queryMBeans()
        throws Exception
    {
        java.util.Set names = mbns.queryMBeans(new ObjectName("com.sun.jbi:Target=server,*"), null);
        
        java.util.Iterator itr = names.iterator();
        
        while ( itr.hasNext() )
        {
            System.out.println( ((ObjectInstance) itr.next()).getObjectName());
        }
        
    }
    
    public void setAttribute(String attrName, Object value)
        throws Exception
    {
        mbns.setAttribute(getComponentConfigurationName(),
            new Attribute(attrName, value));
        
        System.out.println(RESULT_PREFIX + " setAttribute = " + "SUCCESS" );
    }
    
    
    public void setAttributes(AttributeList attribs)
        throws Exception
    {
        mbns.setAttributes(getComponentConfigurationName(),
            attribs);
        
        System.out.println(RESULT_PREFIX + " setAttributes = " + "SUCCESS" );
    }
    
    public void getAttribute(String attrName)
        throws Exception
    {
        Object attribValue = (Object) mbns.getAttribute(getComponentConfigurationName(),
            attrName);
        
        System.out.println(RESULT_PREFIX + " getAttribute = " + attribValue.toString() );
    }
    
    
    public void getAttributes(String[] attrNames)
        throws Exception
    {
        AttributeList attribs = (AttributeList) mbns.getAttributes(getComponentConfigurationName(),
            attrNames);
        
        System.out.print(RESULT_PREFIX + " getAttributes = " );
        
        for ( int i=0; i < attribs.size(); i++) 
        {
            Attribute attrib = (Attribute) attribs.get(i);
            System.out.print(attrib.getName() + "=" + attrib.getValue() + ", ");
        }
        System.out.println("");
    }
    
    public void setConfigurationAttribute(String attrName, Object value)
        throws Exception
    {
        String result = (String) mbns.invoke( getComponentConfigurationName(), "setConfigurationAttribute",
            new Object[] { new Attribute(attrName, value) }, new String[] { "javax.management.Attribute" });
        
        // replace "en_US" with "en"
        result = result.replace("en_US", "en");
        System.out.println(RESULT_PREFIX + " setConfigurationAttribute = " + result );
    }
    
    
    public void setConfigurationAttributes(AttributeList attribs)
        throws Exception
    {
        String result = (String) mbns.invoke( getComponentConfigurationName(), "setConfigurationAttributes",
            new Object[] { attribs }, new String[] { "javax.management.AttributeList" });
        
        // replace "en_US" with "en"
        result = result.replace("en_US", "en");
        System.out.println(RESULT_PREFIX + " setConfigurationAttributes = " + result );
    }
    
    /**
     * List the Attributes in the MBean
     */
    public void getMBeanAttributeInfo(ObjectName mbeanName)
        throws Exception
    {
        MBeanInfo mbeanInfo = mbns.getMBeanInfo(mbeanName);
        
        if ( mbeanInfo != null )
        {
            System.out.println("Attribute Information for MBean " + mbeanName);
            
            MBeanAttributeInfo[] attribs = mbeanInfo.getAttributes();
            
            for (MBeanAttributeInfo attrib : attribs )
            {
                System.out.println("\nName : " + attrib.getName() 
                    + " Type  : " + attrib.getType());
            }
        }
        
    }
    
    /**
     *
     */
    public void addApplicationVariable(String name, String value, String type)  
        throws Exception
    {
        CompositeData appVar = new CompositeDataSupport(
            sAppVarComposite, sAppVarItemNames, new String[]{name, value, type});
            
        String result = (String) mbns.invoke( getComponentConfigurationName(), "addApplicationVariable",
            new Object[] { name, appVar }, new String[] { "java.lang.String", 
                "javax.management.openmbean.CompositeData" });
        
        // replace "en_US" with "en"
        result = result.replace("en_US", "en");
        System.out.println(RESULT_PREFIX + " addApplicationVariable = " + result );                           
    }
    
    /**
     *
     */
    public void setApplicationVariable(String name, String value, String type)  
        throws Exception
    {
        CompositeData appVar = new CompositeDataSupport(
            sAppVarComposite, sAppVarItemNames, new String[]{name, value, type});
            
        String result = (String) mbns.invoke( getComponentConfigurationName(), "setApplicationVariable",
            new Object[] { name, appVar }, new String[] { "java.lang.String", 
                "javax.management.openmbean.CompositeData" });
        
        // replace "en_US" with "en"
        result = result.replace("en_US", "en");
        System.out.println(RESULT_PREFIX + " setApplicationVariable = " + result );                
    }
    
    /**
     *
     */
    public void deleteApplicationVariable(String name)  
        throws Exception
    {      
        String result = (String) mbns.invoke( getComponentConfigurationName(), "deleteApplicationVariable",
            new Object[] { name }, new String[] { "java.lang.String" });
        
        // replace "en_US" with "en"
        result = result.replace("en_US", "en");
        System.out.println(RESULT_PREFIX + " deleteApplicationVariable = " + result );                
    }    
    
    /**
     *
     */
    public void getApplicationVariables()  
        throws Exception
    {      
        TabularData appVars = (TabularData) mbns.invoke( getComponentConfigurationName(), "getApplicationVariables",
            new Object[] { }, new String[] {});
        
        System.out.println(RESULT_PREFIX + " getApplicationVariables = " );                
        
        Set names = appVars.keySet();
        
        for ( Object name : names )
        {
            Object[] key = ( (java.util.List) name).toArray();
            
            CompositeData cd = appVars.get(key);
            
            if ( cd != null )
            {
                String appVarName =  (String) cd.get("name");
                String appVarValue = (String) cd.get("value");
                String appVarType =  (String) cd.get("type");
                
                if ( appVarValue == null || appVarType == null )
                {
                    throw new Exception("Illegal CompositeData Values");
                }
                
                System.out.println("      Name : " + appVarName + " Value : " + appVarValue +
                    "  Type : " + appVarType) ;
            }
        }
    }

    /**
     *
     */
    public void getApplicationConfigurations()  
        throws Exception
    {      
        TabularData appCfgs = (TabularData) mbns.invoke( getComponentConfigurationName(), "getApplicationConfigurations",
            new Object[] { }, new String[] {});
        
        System.out.println(RESULT_PREFIX + " getApplicationConfigurations = " );                
        
        Set names = appCfgs.keySet();
        
        for ( Object name : names )
        {
            Object[] key = ( (java.util.List) name).toArray();
            
            CompositeData cd = appCfgs.get(key);
            
            if ( cd != null )
            {
                System.out.println("Application Configuration : " + cd.get("configurationName"));
                System.out.println("\tconnectionURL      = " + (String) cd.get("connectionURL")); 
                System.out.println("\tsecurityPrincipal  = " + (String) cd.get("securityPrincipal"));
                System.out.println("\tsecurityCredential = " + (String) cd.get("securityCredential"));
                System.out.println("\tjndienv = ");
                String[] jndienv = (String[]) cd.get("jndienv");
                System.out.print("\t\t[ ");
                for ( String env : jndienv )
                {
                    System.out.print( env + " ");
                }
                System.out.println(" ]");
            }
        }
    }
    
    /**
     *
     */
    public void addApplicationConfiguration()  
        throws Exception
    {
        CompositeData appConfig = new CompositeDataSupport(
            sAppConfigComposite, sAppConfigItemNames, 
            new Object[]{"newAppConfig","http://www.google.com","admin", "adminadmin", 
                new String[]{"var1", "var2", "var3"}});
            
        String result = (String) mbns.invoke( getComponentConfigurationName(), "addApplicationConfiguration",
            new Object[] { "newAppConfig", appConfig }, new String[] { "java.lang.String", 
                "javax.management.openmbean.CompositeData" });
        
        // replace "en_US" with "en"
        result = result.replace("en_US", "en");
        System.out.println(RESULT_PREFIX + " addApplicationConfiguration = " + result );                           
    }    
    
    /**
     *
     */
    public void setApplicationConfiguration()  
        throws Exception
    {
        CompositeData appConfig = new CompositeDataSupport(
            sAppConfigComposite, sAppConfigItemNames, 
            new Object[]{"newAppConfig","http://www.youtube.com","su", "suPass", 
                new String[]{"var1=value1", "var2=value2", "var3=value3"}});
            
        String result = (String) mbns.invoke( getComponentConfigurationName(), "setApplicationConfiguration",
            new Object[] { "newAppConfig", appConfig }, new String[] { "java.lang.String", 
                "javax.management.openmbean.CompositeData" });
        
        // replace "en_US" with "en"
        result = result.replace("en_US", "en");
        System.out.println(RESULT_PREFIX + " setApplicationConfiguration = " + result );                           
    }
    
    
    /**
     *
     */
    public void deleteApplicationConfiguration()  
        throws Exception
    {
            
        String result = (String) mbns.invoke( getComponentConfigurationName(), "deleteApplicationConfiguration",
            new Object[] { "newAppConfig"}, new String[] { "java.lang.String"});
        
        // replace "en_US" with "en"
        result = result.replace("en_US", "en");
        System.out.println(RESULT_PREFIX + " deleteApplicationConfiguration = " + result );                           
    }
    
    /**
     *
     */
    public void queryApplicationConfigurationType()  
        throws Exception
    {
            
        CompositeType ct = (CompositeType) mbns.invoke( getComponentConfigurationName(), "queryApplicationConfigurationType",
            new Object[] {}, new String[] {});
            
        System.out.println(RESULT_PREFIX + " queryApplicationConfigurationType = " + ct.toString() );                           
    }
    
    public static void main (String[] params)
        throws Exception 
    {
        TestComponentConfiguration test = new TestComponentConfiguration();
        
        //test.queryMBeans();
        //test.getMBeanAttributeInfo(test.getComponentConfigurationName());

        // Simple get/set

        test.setAttribute("HostName", "Mystique01");
        test.getAttribute("HostName");
        
        test.setAttribute("ProxyPassword", "secret");
        test.getAttribute("ProxyPassword");
        
        // get/setAttributes
        AttributeList attrList = new AttributeList();
        attrList.add( new Attribute("HostName", "Magnetto"));
        attrList.add( new Attribute("Port", 6789));
        test.setAttributes(attrList);
        test.getAttributes(new String[]{"HostName", "Port"});
        
        // setConfigurationAttribute
        test.setConfigurationAttribute("Port", 10789);
        test.getAttribute("Port");
        
        // setConfigurationAttributes
        attrList = new AttributeList();
        attrList.add( new Attribute("HostName", "Wolverine"));
        attrList.add( new Attribute("Port", 1234));
        test.setConfigurationAttributes(attrList);
        test.getAttributes(new String[]{"HostName", "Port", "ProxyPassword"});

        // Application Variable operations
        test.getApplicationVariables();
        test.addApplicationVariable("Location", "http://www.sun.com", "STRING");
        test.addApplicationVariable("SecurityCredential", "http://www.sun.com", "PASSWORD");
        test.getApplicationVariables();
        test.setApplicationVariable("Location", "http://www.google.com", "STRING");
        test.getApplicationVariables();
        test.deleteApplicationVariable("Location");
        test.getApplicationVariables();
        
        // Application configuration operations
        test.queryApplicationConfigurationType();
        test.getApplicationConfigurations();
        test.addApplicationConfiguration();
        test.getApplicationConfigurations();
        test.setApplicationConfiguration();
        test.getApplicationConfigurations();
        test.deleteApplicationConfiguration();
        test.getApplicationConfigurations();
    }
}
