/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Map;
import java.util.Set;
import javax.management.remote.*;
import javax.management.*;
import javax.management.openmbean.*;


/**
 */
public class TestApplicationConfigurationSupport
{
     
    private MBeanServerConnection mbns;
    private static final String RESULT_PREFIX = "##### Result of ";
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String TARGET   = "target";
    private static final String COMPONENT_NAME = "component.name";
  
    public TestApplicationConfigurationSupport()
    {
        try
        {
            initMBeanServerConnection();
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    public ObjectName getComponentConfigurationName()
        throws Exception
    {
        String info = "com.sun.jbi:Target="        + System.getProperty(TARGET, "domain") + 
                                  ",ComponentName=" + System.getProperty(COMPONENT_NAME)   +
                                  ",ServiceType=Configuration";
        return new ObjectName(info);
    }
    
    

    
    /**
     *
     */
    public void isAppVarsSupported()  
        throws Exception
    {   
        Boolean result = (Boolean) mbns.invoke( getComponentConfigurationName(), "isAppVarsSupported",
            new Object[] {}, new String[] {});
        
        System.out.println(RESULT_PREFIX + " isAppVarsSupported = " + result );                           
    }
    
    /**
     *
     */
    public void isAppConfigSupported()  
        throws Exception
    {   
        Boolean result = (Boolean) mbns.invoke( getComponentConfigurationName(), "isAppConfigSupported",
            new Object[] {}, new String[] {});
        
        System.out.println(RESULT_PREFIX + " isAppConfigSupported = " + result );                           
    } 
    
    /**
     * Get actual cause
     */
    public Throwable getActualCause(Throwable ex)
    {
        Throwable cause = ex;
            
        do 
        {
            if ( cause == null )
            {
                break;
            }
            cause = cause.getCause();
        }
        while ( cause instanceof javax.management.JMException );
        return cause;
    }
    
    public void printCause(String opName, Throwable ex)
    {
        Throwable cause = getActualCause(ex);
        if ( cause != null )
        {
            String result = cause.getMessage();
            result = result.replace("en_US", "en");
            System.out.println(RESULT_PREFIX + " isAppVarsSupported = " +
                "Exception : " + result );
        }
    }
    
    public static void main (String[] params)
        throws Exception 
    {
        TestApplicationConfigurationSupport test = new TestApplicationConfigurationSupport();
        try
        {
            test.isAppVarsSupported();
        }
        catch ( Throwable ex )
        {
            test.printCause("isAppVarsSupported", ex);
        }
        try
        {
            test.isAppConfigSupported();
        }
        catch ( Exception ex )
        {
            test.printCause("isAppConfigSupported", ex);
        }
    }
}
