#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01311.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin01311 : Test partial service assembly deployment for target=cluster ( Fix for CR 6514223 )"

#############################################################################################
#     Tests deploying a service assembly. One component fails deploy, the                   #
#     deployment result should show the error for the failed deployment                     #  
#     with instance info.                                                                   #             
#                                                                                           #
# A test component used throws an exception on deploy of a service unit                     #
#                                                                                           #
# The test :                                                                                #
# (a) Package, install and start the test components                                        #
# (b) Deploy the service assembly, deploy should be a partial success, SUCCESS with         #
#     WARNING response expected                                                             #
# (c) Cleanup : Undeploy service assembly, uninstall component                              #
#                                                                                           #
#############################################################################################

. ./regress_defs.ksh

COMPONENT_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/component-binding-1.jar
COMPONENT_NAME_1=manage-binding-1

COMPONENT_ARCHIVE_2=$JV_SVC_TEST_CLASSES/dist/component-binding-2.jar
COMPONENT_NAME_2=manage-binding-2

SA_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/deploy-sa.jar
SA_NAME=esbadmin00089-sa

#
# Setup :
#
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml pkg.service.assembly.jars
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01311.xml pkg.test.components


# Setup : Install and start component on cluster1
$JBI_ANT  -Djbi.target=cluster1 -Djbi.install.file=$COMPONENT_ARCHIVE_1  install-component
$JBI_ANT  -Djbi.target=cluster1 -Djbi.component.name=$COMPONENT_NAME_1 start-component

# Setup : Install and start component on cluster1 for deploying the service assembly
$JBI_ANT  -Djbi.target=cluster1 -Djbi.install.file=$COMPONENT_ARCHIVE_2   install-component
$JBI_ANT  -Djbi.target=cluster1 -Djbi.component.name=$COMPONENT_NAME_2 start-component


$JBI_ANT  -Djbi.target=cluster1  -Djbi.deploy.file=$SA_ARCHIVE   deploy-service-assembly
$JBI_ANT  -Djbi.target=cluster1 list-service-assemblies

# Cleanup : undeploy the service assembly
$JBI_ANT  -Djbi.target=cluster1 -Djbi.service.assembly.name=$SA_NAME  -Djbi.force.task=true  -Djbi.keep.archive=false undeploy-service-assembly

# Cleanup : stop/shutdown/uninstall both the components
$JBI_ANT  -Djbi.target=cluster1 -Djbi.component.name=$COMPONENT_NAME_1  stop-component
$JBI_ANT  -Djbi.target=cluster1 -Djbi.component.name=$COMPONENT_NAME_1  shut-down-component
$JBI_ANT  -Djbi.target=cluster1 -Djbi.component.name=$COMPONENT_NAME_1  -Djbi.keep.archive=false uninstall-component

$JBI_ANT  -Djbi.target=cluster1 -Djbi.component.name=$COMPONENT_NAME_2  stop-component
$JBI_ANT  -Djbi.target=cluster1 -Djbi.component.name=$COMPONENT_NAME_2  shut-down-component
$JBI_ANT  -Djbi.target=cluster1 -Djbi.component.name=$COMPONENT_NAME_2  -Djbi.keep.archive=false uninstall-component
