/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestAdminService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.eventmanagement.impl;

import java.io.File;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.management.remote.*;
import javax.management.*;

import com.sun.esb.eventmanagement.api.AlertPersistenceDBType;
import com.sun.esb.eventmanagement.impl.AlertLevelType;
import com.sun.esb.eventmanagement.impl.AlertRemovalPolicyType;
import com.sun.esb.eventmanagement.impl.EventManagementConstants;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.ee.JavaEEApplicationInterceptor;
import com.sun.jbi.management.system.ScaffoldedEnvironmentContext;

/**
 */
public class TestEventManagementControllerMBean extends junit.framework.TestCase implements EventManagementConstants
{



    private static final Boolean ENABLE_PERSISTENCE_TEST_VALUE =true;
    private static final Boolean ENABLE_POLICY_EXECUTION_TEST_VALUE = true;
    private static final Long PERSISTED_ALERT_MAX_AGE_TEST_VALUE = 12345L;
    private static final Long PERSISTED_ALERT_MAX_COUNT_TEST_VALUE = 54321L;
    private static final String PERSISTED_ALERT_LEVEL_TEST_VALUE = AlertLevelType.CRITICAL.getAlertLevel();
    private static final Long PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_TEST_VALUE = 567890L;
    private static final String DATABASE_JNDI_NAME__TEST_VALUE = "testJNDI";
    private static final String DATABASE_TYPE_TEST_VALUE = AlertPersistenceDBType.ORACLE.getDatabasetype();
    private static final String DATABASE_TABLE_TEST_VALUE = "TestTable";



    private EventManagementControllerMBean mEventManagementControllerMBean ;
    private ScaffoldedEnvironmentContext      mEnvCtx;
    private MBeanServer mMBeanServer;



    public TestEventManagementControllerMBean ()
    throws Exception
    {

    	mEnvCtx      = new ScaffoldedEnvironmentContext();
        String srcroot = System.getProperty("junit.srcroot");
        String propertiesDir = srcroot + "/runtime/esb-manage/bld/";
        mEnvCtx.setJbiInstanceRoot(propertiesDir);
    	mMBeanServer = mEnvCtx.getMBeanServer();
     }

    public void setUp()
    throws Exception
    {
    	super.setUp();
        mEventManagementControllerMBean = new EventManagementControllerMBean(mEnvCtx);

        mMBeanServer.registerMBean(
    			mEventManagementControllerMBean, this.getControllerName());
    }

    public void tearDown()
    throws Exception
{
    mEnvCtx.getMBeanServer().unregisterMBean(getControllerName());
}


    public ObjectName getControllerName()
        throws Exception
    {
        // this name is subject to change if it need to conform to the JBI runtime MBean convention.
        String controller = "EventManagement:name=EventManagementControllerMBean";
        return new ObjectName(controller);
    }

    public void testEnablePolicyExecution()
        throws Exception
    {
        Attribute attib = new Attribute(ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME,ENABLE_POLICY_EXECUTION_TEST_VALUE);
        mMBeanServer.setAttribute(getControllerName(), attib);
        Boolean enabled= (Boolean) mMBeanServer.getAttribute(getControllerName(), ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME);
        assertTrue(enabled.equals(true));
    }

    public void testPersistedEventMaxAge()
    throws Exception
    {
        Long testAge = this.PERSISTED_ALERT_MAX_AGE_TEST_VALUE;
        Attribute attib = new Attribute(PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME,testAge);
        mMBeanServer.setAttribute(getControllerName(), attib);
        Long returnAge= (Long) mMBeanServer.getAttribute(getControllerName(), PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME);
        assertTrue(returnAge.equals(PERSISTED_ALERT_MAX_AGE_TEST_VALUE));
    }

    public void testPersistedEventMaxCount()
    throws Exception
    {
        Long testCount = this.PERSISTED_ALERT_MAX_COUNT_TEST_VALUE;
        Attribute attib = new Attribute(PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME,testCount);
        mMBeanServer.setAttribute(getControllerName(), attib);
        Long returnCount= (Long) mMBeanServer.getAttribute(getControllerName(), PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME);
        assertTrue(returnCount.equals(PERSISTED_ALERT_MAX_COUNT_TEST_VALUE));
    }


    public void testPersistedEventAlertLevel()
    throws Exception
    {
        String testLevel = this.PERSISTED_ALERT_LEVEL_TEST_VALUE;
        Attribute attib = new Attribute(PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME,testLevel);
        mMBeanServer.setAttribute(getControllerName(), attib);
        String returnlevel= (String) mMBeanServer.getAttribute(getControllerName(), PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME);
        assertTrue(returnlevel.equals(AlertLevelType.CRITICAL.getAlertLevel()));
    }

    public void testPolicyExecutionInterval()
    throws Exception
    {
        Long testinterval = this.PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_TEST_VALUE;
        Attribute attib = new Attribute(PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME,testinterval);
        mMBeanServer.setAttribute(getControllerName(), attib);
        Long returninterval= (Long) mMBeanServer.getAttribute(getControllerName(), PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME);
        assertTrue(returninterval.equals(PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_TEST_VALUE));
    }



    public void testJNDIName()
    throws Exception
    {
        String testjndi = this.DATABASE_JNDI_NAME__TEST_VALUE;
        Attribute attib = new Attribute(DATABASE_JNDI_NAME__ATTRIBUTE_NAME,testjndi);
        mMBeanServer.setAttribute(getControllerName(), attib);
        String returnjndi= (String) mMBeanServer.getAttribute(getControllerName(), DATABASE_JNDI_NAME__ATTRIBUTE_NAME);
        assertTrue(returnjndi.equals(DATABASE_JNDI_NAME__TEST_VALUE));
    }

    public void testDatabaseType()
    throws Exception
    {
        String testDBType = this.DATABASE_TYPE_TEST_VALUE;
        Attribute attib = new Attribute(DATABASE_TYPE_ATTRIBUTE_NAME,testDBType);
        mMBeanServer.setAttribute(getControllerName(), attib);
        String returnDBType= (String) mMBeanServer.getAttribute(getControllerName(), DATABASE_TYPE_ATTRIBUTE_NAME);
        assertTrue(returnDBType.equals(AlertPersistenceDBType.ORACLE.getDatabasetype()));
    }


    public void testDatabaseTableName()
    throws Exception
    {

        String testTableName = DATABASE_TABLE_TEST_VALUE;
        Attribute attib = new Attribute(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME,testTableName);
        mMBeanServer.setAttribute(getControllerName(), attib);
        String returnTableName= (String) mMBeanServer.getAttribute(getControllerName(), DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME);
        assertTrue(returnTableName.equals(DATABASE_TABLE_TEST_VALUE));
    }


    public void testAlertRemovalPolicy()
    throws Exception
    {

        ArrayList policySettingList = new ArrayList<String>();
        policySettingList.add(AlertRemovalPolicyType.ALERTS_LEVEL.getPolicyType());
        policySettingList.add(AlertRemovalPolicyType.ALERTS_AGE.getPolicyType());
        policySettingList.add(AlertRemovalPolicyType.ALERTS_COUNT.getPolicyType());

        Object[] params = {policySettingList};
        String[] signature ={"java.util.List"};
        mMBeanServer.invoke(getControllerName(), UPDATE_PERSISTED_ALERT_REMOVAL_PROLICY_OPERATION_NAME,params,signature);
        String[] returnpolicy =(String[]) mMBeanServer.invoke(getControllerName(), GET_LAST_PERSISTED_ALERT_REMOVAL_PROLICY_OPERATION_NAME,null,null);
        assertTrue(parsePolicy(policySettingList).equals(parsePolicy(returnpolicy)));
    }





    private String parsePolicy(List<String> policyItems) {
    	StringBuffer policyString = new StringBuffer();
    	for (String item : policyItems) {
    		policyString.append(item+ " ");
		}
    	return policyString.toString();
    }

    private String parsePolicy(String[] policyItems) {
    	StringBuffer policyString = new StringBuffer();
    	for (String item : policyItems) {
    		policyString.append(item+ " ");
		}
    	return policyString.toString();
    }



}
