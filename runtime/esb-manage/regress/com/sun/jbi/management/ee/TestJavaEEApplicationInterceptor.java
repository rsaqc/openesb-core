/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJavaEEApplicationInterceptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.ee;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.system.ScaffoldedEnvironmentContext;

import javax.management.MBeanServer;
import javax.management.ObjectName;

public class TestJavaEEApplicationInterceptor extends junit.framework.TestCase
{
    /** Name of the MBean that the Java EE SE registers to intercept SA operations. */
    private static final String JAVA_EE_DEPLOYER_NAME   = "JavaEEDeployer";
    /** Java EE SE component name. */
    private static final String JAVA_EE_SE_NAME         = "sun-javaee-engine";
    
    /** There are all dummy test values */
    private static final String SA_NAME     = "my-sa";
    private static final String SU_NAME     = "my-su";
    private static final String OP_NAME     = "my-op";
    private static final String TARGET      = "server";
    private static final String SU_PATH     = "/tmp/su.jar";
    
    private EnvironmentContext      mEnvCtx;
    private ObjectName              mJavaEEDeployerName;
    /* This is what we are testing. */
    private ApplicationInterceptor  mInterceptor;
    /** This is a stand-in for the JavaEEDeployer implementation that the Java 
     *  EE SE registers. */
    private JavaEEDeployer          mJavaEEDeployer;
    
    public TestJavaEEApplicationInterceptor (String aTestName)
        throws Exception
    {
        super(aTestName);
        
        mEnvCtx             = new ScaffoldedEnvironmentContext();
        mInterceptor        = new JavaEEApplicationInterceptor(mEnvCtx);
        mJavaEEDeployerName = mEnvCtx.getMBeanNames().getCustomEngineMBeanName(
                JAVA_EE_DEPLOYER_NAME, JAVA_EE_SE_NAME);
    }
    
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mJavaEEDeployer = new JavaEEDeployer();
        mEnvCtx.getMBeanServer().registerMBean(
                mJavaEEDeployer, mJavaEEDeployerName);
    }

    public void tearDown()
        throws Exception
    {
        mEnvCtx.getMBeanServer().unregisterMBean(mJavaEEDeployerName);
    }
     
    /** Verify that a call to deploy is received by the JavaEEDeployer MBean.
     */
    public void testDeploy()
        throws Exception
    {
        mInterceptor.performAction("deploy", SA_NAME, SU_NAME, SU_PATH, TARGET);        
        assertTrue("deploy".equals(mJavaEEDeployer.getLastMethodCalled()));
    }
    
    /** Verify that a call to undeploy is received by the JavaEEDeployer MBean.
     */
    public void testUndeploy()
        throws Exception
    {        
        mInterceptor.performAction("undeploy", SA_NAME, SU_NAME, SU_PATH, TARGET);        
        assertTrue("undeploy".equals(mJavaEEDeployer.getLastMethodCalled()));
    }
    
    /** Verify that a call to start is received by the JavaEEDeployer MBean.
     */
    public void testStart()
        throws Exception
    {
        mInterceptor.performAction("start", SA_NAME, SU_NAME, SU_PATH, TARGET);        
        assertTrue("start".equals(mJavaEEDeployer.getLastMethodCalled()));        
    }
    
    /** Verify that a call to stop is received by the JavaEEDeployer MBean.
     */
    public void testStop()
        throws Exception
    {
        mInterceptor.performAction("stop", SA_NAME, SU_NAME, SU_PATH, TARGET);        
        assertTrue("stop".equals(mJavaEEDeployer.getLastMethodCalled()));        
    }
    
    /** Verify that a call to shutDown is received by the JavaEEDeployer MBean.
     */
    public void testShutDown()
        throws Exception
    {
        mInterceptor.performAction("shutDown", SA_NAME, SU_NAME, SU_PATH, TARGET);        
        assertTrue("shutDown".equals(mJavaEEDeployer.getLastMethodCalled()));        
    }
}

/** This is a stand-in for the JavaEEDeployer implementation that the Java 
  *  EE SE registers. 
 */
class JavaEEDeployer implements JavaEEDeployerMBean
{
    private String lastMethodCalled;
    
    String getLastMethodCalled()
    {
        return lastMethodCalled;
    }
    
    public String deploy(String serviceAssemblyName, 
        String serviceUnitName, 
        String serviceUnitRootPath,
        String target)
        throws Exception
    {
        lastMethodCalled = "deploy";
        return "<SUCCESS/>";
    }

    public void start(String serviceAssemblyName, 
        String serviceUnitName, 
        String target)
        throws Exception            
    {
        lastMethodCalled = "start";        
    }

    public void stop(String serviceAssemblyName, 
        String serviceUnitName, 
        String target)
        throws Exception
    {
        lastMethodCalled = "stop";        
    } 

    public void shutDown(String serviceAssemblyName, 
        String serviceUnitName, 
        String target)
        throws Exception
    {
        lastMethodCalled = "shutDown";        
    }

    public String undeploy(String serviceAssemblyName, 
        String serviceUnitName, 
        String serviceUnitRootPath,
        String target)
        throws Exception
    {
        lastMethodCalled = "undeploy";
        return "<SUCCESS/>";
    }
}
