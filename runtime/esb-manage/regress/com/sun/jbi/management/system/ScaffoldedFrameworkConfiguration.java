/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ScaffoldedFrameworkConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.util.Properties;

/**
 * FrameworkConfigurationMBean defines configuration parameters exposed by the
 * JBI Framework.
 *
 * @author Sun Microsystems, Inc.
 */
public class ScaffoldedFrameworkConfiguration
{
    private Properties mConfig;
    
    public ScaffoldedFrameworkConfiguration()
    {
        mConfig = new Properties();
    }
    
    public void load(Properties config)
    {
        mConfig = config;
    }
    
    /**
     * Get the value of IsEsbMember.
     * @return the current value of IsEsbMember.
     */
    public boolean getIsEsbMember()
    {       
        return mConfig.getProperty("IsEsbMember").trim().equals("true");
    }

    public void setIsEsbMember(boolean aIsEsbMember)
    {
        mConfig.setProperty("IsEsbMember", Boolean.toString(aIsEsbMember));
    }
    
    public boolean getIsCentralAdminServer()
    {
        return mConfig.getProperty("IsCentralAdminServer").trim().equals("true");
    }
    
    /**
     * Set the value of IsCentralAdminServer.
     *
     * @param aIsCentralAdminServer - the value
     */
    public void setIsCentralAdminServer(boolean aIsCentralAdminServer)
    {
        mConfig.setProperty("IsCentralAdminServer", 
                Boolean.toString(aIsCentralAdminServer));
        
    }
    
    /**
     * Get the value of EsbInstanceName.
     * @return the current value of EsbInstanceName.
     */
    public String getEsbInstanceName()
    {        
        return mConfig.getProperty("EsbInstanceName").trim();
    }

    /**
     * Set the value of EsbInstanceName.
     * @param aEsbInstanceName - the value.
     */
    public void setEsbInstanceName(String aEsbInstanceName)
    {        
        mConfig.setProperty("EsbIntanceName", aEsbInstanceName);
    }

    /**
     * Get the value of JbiName.
     * @return the current value of JbiName.
     */
    public String getJbiName()
    {
        return mConfig.getProperty("JbiName").trim();
    }

    /**
     * Set the value of JbiName.
     * @param aJbiName - the value.
     */
    public void setJbiName(String aJbiName)
    {
        mConfig.setProperty("JbiName", aJbiName);
    }

    /**
     * Get the value of CAS JMX Agent URL.
     * @return the current value of CAS JMX Agent URL.
     */
    public String getCASUrl()
    {
        return mConfig.getProperty("CASUrl").trim();
    }

     /**
     * Set the value of CAS JMX Agent URL.
     * @param aEsbMasterAgentUrl - the value.
     */
    public void setCASUrl(String url)
    {
        mConfig.setProperty("CASUrl", url);
    }
    
    /**
     * Get the value of CAS admin user.
     * @return the current value of CAS admin user.
     */
    public String getCASAdminUser()
    {
        return mConfig.getProperty("CASAdminUser").trim();
    }

     /**
     * Set the value of CAS admin user.
     * @param user - the value.
     */
    public void setCASAdminUser(String user)
    {
        mConfig.setProperty("CASAdminUser", user);
    }    
    
    /**
     * Get the value of CAS admin password.
     * @return the current value of CAS admin password.
     */
    public String getCASAdminPassword()
    {
        return mConfig.getProperty("CASAdminPassword").trim();
    }

     /**
     * Set the value of CAS admin password.
     * @param user - the value.
     */
    public void setCASAdminPassword(String password)
    {
        mConfig.setProperty("CASAdminPassword", password);
    }
    
    /**
     * Get the value of ComponentTimeout.
     * @return the current value of ComponentTimeout.
     */
    public long getComponentTimeout()
    {
        return 0;
    }

    /**
     * Set the value of ComponentTimeout.
     * @param componentTimeout - the value.
     */
    public void setComponentTimeout(long componentTimeout)
    {
    }

    /**
     * Get the value of DeploymentTimeout.
     * @return the current value of DeploymentTimeout.
     */
    public long getDeploymentTimeout()
    {
        return 0;
    }

    /**
     * Set the value of DeploymentTimeout.
     * @param deploymentTimeout - the value.
     */
    public void setDeploymentTimeout(long deploymentTimeout)
    {
    }

    /**
     * Get the value of InstallationTimeout.
     * @return the current value of InstallationTimeout.
     */
    public long getInstallationTimeout()
    {
        return 0;
    }

    /**
     * Set the value of InstallationTimeout.
     * @param installationTimeout - the value.
     */
    public void setInstallationTimeout(long installationTimeout)
    {
    }

    /**
     * Get the value of ServiceUnitTimeout.
     * @return the current value of ServiceUnitTimeout.
     */
    public long getServiceUnitTimeout()
    {
        return 0;
    }

    /**
     * Set the value of ServiceUnitTimeout.
     * @param serviceUnitTimeout - the value.
     */
    public void setServiceUnitTimeout(long serviceUnitTimeout)
    {
    }

    /**
     * Apply the current configuration.
     * @return 0 if successful.
     */
    public int apply()
    {
        return 0;
    }

    /**
     * Save the configuration to persistent storage.
     * @return 0 if successful.
     */
    public int save()
    {
        return 0;
    }

    /**
     * Restore the configuration from persistent storage.
     * @return 0 if successful.
     */
    public int restore()
    {
        return 0;
    }
}
