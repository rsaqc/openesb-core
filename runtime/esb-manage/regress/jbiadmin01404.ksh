#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01402.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT


echo jbiadmin01404 - Negative tests for runtime configuration [ Test fix for Issue 50 ]

. ./regress_defs.ksh

asadmin set-jbi-runtime-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS componentTimeout=-1

asadmin set-jbi-runtime-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS deploymentTimeout=-1

asadmin set-jbi-runtime-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS jbiHome=\tmp

asadmin set-jbi-runtime-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS startOnDeploy=xxx



