#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01602.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin01604 : Negative tests for application variable operations for target=server"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.test.component

TARGET=server
COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-with-custom-mbean.jar
COMPONENT_NAME=manage-binding-1

NEW_APP_VAR_FILE=$JV_SVC_REGRESS/deploytest/new-app-var.properties
UPDATED_APP_VAR_FILE=$JV_SVC_REGRESS/deploytest/updated-app-var.properties
INCORRECT_APP_VAR_FILE=$JV_SVC_REGRESS/deploytest/non-existant-app-var.properties
                                                  

JBI_ANT_CMD="$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET"

# Test : Test the application configuration operations for a component which is not installed - should get back an error.
$JBI_ANT_CMD                                                     list-application-variables
$JBI_ANT_CMD -Djbi.app.variables.file=$NEW_APP_VAR_FILE          create-application-variable
$JBI_ANT_CMD -Djbi.app.variables.file=$UPDATED_APP_VAR_FILE      update-application-variable
$JBI_ANT_CMD -Djbi.app.variable.name=securityCredentials         delete-application-variable

# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="server" install-component

# Test : Test the application configuration for a component which is not started - should get back an error.
$JBI_ANT_CMD                                                      list-application-variables
$JBI_ANT_CMD -Djbi.app.variables.file=$NEW_APP_VAR_FILE           create-application-variable
$JBI_ANT_CMD -Djbi.app.variables.file=$UPDATED_APP_VAR_FILE       update-application-variable
$JBI_ANT_CMD -Djbi.app.variable.name=securityCredentials          delete-application-variable

# Start the component
$JBI_ANT_CMD start-component

# Test adding a duplicate application configuration
$JBI_ANT_CMD -Djbi.app.variables.file=$NEW_APP_VAR_FILE              create-application-variable
$JBI_ANT_CMD                                                         list-application-variables
$JBI_ANT_CMD -Djbi.app.variables.file=$NEW_APP_VAR_FILE              create-application-variable
$JBI_ANT_CMD                                                         list-application-variables
$JBI_ANT_CMD -Djbi.app.variable.name=securityCredentials             delete-application-variable
$JBI_ANT_CMD -Djbi.app.variable.name=securityPrincipal               delete-application-variable
$JBI_ANT_CMD -Djbi.app.variable.name=port                            delete-application-variable

# Test setting a non-existing application configuration
$JBI_ANT_CMD                                                         list-application-variables
$JBI_ANT_CMD -Djbi.app.variables.file=$INCORRECT_APP_VAR_FILE        update-application-variable

# Test deleting a non-existing application configuration
$JBI_ANT_CMD -Djbi.app.variable.name=xyz                             delete-application-variable

# Test adding a application variable with type=password
asadmin create-jbi-application-variable -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --component $COMPONENT_NAME myPassword=[password]secret
$JBI_ANT_CMD                                                         list-application-variables
asadmin delete-jbi-application-variable -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --component $COMPONENT_NAME myPassword

# Test adding a application variable with the type not specified / empty ( test fix for issue 148 ) 
$JBI_ANT_CMD -Djbi.app.variable.name=FirstName -Djbi.app.variable.value=John              create-application-variable
$JBI_ANT_CMD                                                         list-application-variables
$JBI_ANT_CMD -Djbi.app.variable.name=FirstName -Djbi.app.variable.value=1 -Djbi.app.variable.type=NUMBER             update-application-variable
$JBI_ANT_CMD                                                         list-application-variables
$JBI_ANT_CMD -Djbi.app.variable.name=LastName -Djbi.app.variable.value=Doe -Djbi.app.variable.type=String             create-application-variable
$JBI_ANT_CMD                                                         list-application-variables
#asadmin create-jbi-application-variable -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --component $COMPONENT_NAME FirstName=[]Jane
#$JBI_ANT_CMD                                                         list-application-variables

# Test upating a non String application variable where type is not specified
$JBI_ANT_CMD -Djbi.app.variable.name=Numeric -Djbi.app.variable.value=1   -Djbi.app.variable.type=NUMBER              create-application-variable
$JBI_ANT_CMD                                                                                                          list-application-variables
$JBI_ANT_CMD -Djbi.app.variable.name=Numeric -Djbi.app.variable.value=5                                               update-application-variable
$JBI_ANT_CMD                                                                                                          list-application-variables

# Test creating/updating a BOOLEAN application variable with a non-boolean value
$JBI_ANT_CMD -Djbi.app.variable.name=Employed -Djbi.app.variable.value=xxx  -Djbi.app.variable.type=BOOLEAN           create-application-variable

$JBI_ANT_CMD -Djbi.app.variable.name=Employed -Djbi.app.variable.value=true  -Djbi.app.variable.type=BOOLEAN           create-application-variable
$JBI_ANT_CMD -Djbi.app.variable.name=Employed -Djbi.app.variable.value=yyy                                             update-application-variable

# Test creating/updating a NUMBER application variable with a non-numeric value
#  create
$JBI_ANT_CMD -Djbi.app.variable.name=Count -Djbi.app.variable.value=One  -Djbi.app.variable.type=NUMBER                create-application-variable
#  update
$JBI_ANT_CMD -Djbi.app.variable.name=Count -Djbi.app.variable.value=1  -Djbi.app.variable.type=NUMBER                  create-application-variable
$JBI_ANT_CMD -Djbi.app.variable.name=Count -Djbi.app.variable.value=five                                               update-application-variable

# Test creating/updating a NUMBER application variable with a valid-numeric value
$JBI_ANT_CMD -Djbi.app.variable.name=Count -Djbi.app.variable.value=-99                                               update-application-variable
$JBI_ANT_CMD -Djbi.app.variable.name=Count -Djbi.app.variable.value=99.9                                               update-application-variable

# Cleanup
$JBI_ANT_CMD  shut-down-component
$JBI_ANT_CMD  uninstall-component


