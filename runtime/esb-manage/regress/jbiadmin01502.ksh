#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01502.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# This tests is for component update 
# A component that locks a config file is installed in server and a standalone instance
# When we try to update the component, update fails because component root could not be 
# removed
# This test verifies that the install root was restored from backup by starting the component
# agian 
# a stand-alone instance instance1
####
echo "jbiadmin01502 : Test Component Update."


. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01502.xml  1>&2

echo install the bad component test-component in instance1
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 $JV_SVC_TEST_CLASSES/dist/file-locking-component.jar

echo start the component in instance1
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 file-locking-component

echo shutdown the component in instance1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 file-locking-component

echo update the component
#file locks do not happen in Unix. Hence redirect the output of the update command to err file. In Solaris update-component would be successful.
$JBI_ANT -Djbi.component.name="file-locking-component" -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/file-locking-component.jar update-component 1>&2

echo verify that install root has been replaced in instance1
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 file-locking-component

echo shutdown the component in instance1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 file-locking-component

echo uninstall the component from instance1
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 file-locking-component

