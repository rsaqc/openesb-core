package deploytest;

import java.util.Collection;

import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;


public class TestEventManagementSupport extends NotificationBroadcasterSupport implements
		TestEventManagementSupportMBean {

	private static final String EVENTMANAGEMENT_CHANNEL_MBEAN_NAME = "EventManagement:name=EventManagementChannelSupportMBean";    
    private long mNotificationSeqNum;
    private Notification NotificationInstance = null;
    
	public void forwardEvent(CompositeData data) {
        try {
            NotificationInstance = new Notification("JCAPSEvent",new ObjectName(EVENTMANAGEMENT_CHANNEL_MBEAN_NAME),
                    mNotificationSeqNum,System.currentTimeMillis());
        } catch (MalformedObjectNameException e) {
            //  should not fail - its this object object name
        } catch (NullPointerException e) {
        //  should not fail - the name defined in the same package
        }
        NotificationInstance.setUserData(data);
        sendNotification(NotificationInstance);
		
	}

}
