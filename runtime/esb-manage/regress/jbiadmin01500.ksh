#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01500.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# This tests is for component update 
# A bad component that throws on start is installed in server and a standalone instance
# The component is updated with a corrected version and then it is verified that
# start component succeeds
# This test verifies component update in server instance and 
# a stand-alone instance instance1
####
echo "jbiadmin01500 : Test Component Update."


. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01500.xml 1>&2

echo start instance instance1
asadmin start-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS instance1
startInstanceDelay

echo install the bad component test-component
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/bad-test-component.jar
installComponentDelay
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 $JV_SVC_TEST_CLASSES/dist/bad-test-component.jar
installComponentDelay

echo start the component in server
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT test-component
startComponentDelay

echo start the component in instance1
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 test-component
startComponentDelay

echo shut-down the component in server
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT test-component
stopComponentDelay

echo shut-down the component in instance1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 test-component
stopComponentDelay

#copy the workspace settings file for future checking
cp $JBI_DOMAIN_ROOT/jbi/components/test-component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01500-CAS-settings.txt
cp $JBI_DOMAIN_ROOT/../../nodeagents/agent1/instance1/jbi/components/test-component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01500-instance1-settings.txt

echo update the component
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01500.xml update-component 
installComponentDelay

echo verify the workspace dir has been preserved in server 
diff $JBI_DOMAIN_ROOT/jbi/components/test-component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01500-CAS-settings.txt 2>&1

echo verify the workspace dir has been preserved in instance1
diff $JBI_DOMAIN_ROOT/../../nodeagents/agent1/instance1/jbi/components/test-component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01500-instance1-settings.txt 2>&1

echo verify that install root has been replaced in server
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT test-component
startComponentDelay

echo verify that install root has been replaced in instance1
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 test-component
startComponentDelay

echo shutdown the component in server
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT test-component
stopComponentDelay

echo shutdown the component in instance1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 test-component
stopComponentDelay

echo uninstall the component from server
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT test-component
uninstallComponentDelay

echo uninstall the component from instance1
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 test-component
uninstallComponentDelay

rm $SVC_TEST_CLASSES/jbiadmin01500-CAS-settings.txt
rm $SVC_TEST_CLASSES/jbiadmin01500-instance1-settings.txt
