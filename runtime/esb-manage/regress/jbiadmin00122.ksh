#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00121.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin00121 - Component configuration tests

echo jbiadmin00122 : Test fix for Issue 126
. ./regress_defs.ksh

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00122.xml pkg.test.component

COMPONENT_PATH=$JV_SVC_TEST_CLASSES/dist/missing-config-binding.jar
COMPONENT_NAME=config-binding

$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_PATH -Djbi.install.params.file=$JV_SVC_TEST_CLASSES/deploytest/config/params.properties -Djbi.target=server install-component

