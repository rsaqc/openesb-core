/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingComponent.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package componentconfiguration.cconfigmbeaninstartstop;

import java.io.InputStream;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;
import java.util.Date;
import java.util.Properties;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;

import javax.management.StandardMBean;
import javax.management.ObjectName;

/**
 * Dummy binding component used to test deployment.
 *
 * @author Sun Microsystems, Inc.
 */
public class BindingComponent implements 
    Component, ComponentLifeCycle, ServiceUnitManager, Bootstrap
{
    /** Optional config file used to change behavior of component. */
    private static final String CONFIG_FILE_NAME = "config.properties";
    
    /** 
     * Config property indicating that the component should throw an exception
     *  on start. 
     */
    private static final String THROW_ON_COMP_START = "throw.on.comp.start";
    
    /** Config property indicating that the component should throw an exception
     *  on SU init. 
     */
    private static final String THROW_ON_SU_INIT = "throw.on.su.init";
    
    
    /** Config property indicating that the component should throw an exception
     *  on shutdown. 
     */
    private static final String THROW_ON_COMP_SHUTDOWN = "throw.on.comp.shutdown";
    
    /** Config property indicating that the component should throw an exception
     *  on service unit shutdown. 
     */
    private static final String THROW_ON_SU_SHUTDOWN = "throw.on.su.shutdown";
    
    
    /** Config property indicating that the component should throw an exception
     *  on service unit start. 
     */
    private static final String THROW_ON_SU_START = "throw.on.su.start";
    
    /** Config property indicating that the component should throw an exception
     *  on service unit undeploy. 
     */
    private static final String THROW_ON_SU_UNDEPLOY = "throw.on.su.undeploy";
    
    /** Config property indicating that the component should throw an exception
     *  on service unit deploy. 
     */
    private static final String THROW_ON_SU_DEPLOY = "throw.on.su.deploy";

    /** Config property indicating that the component should throw a plan-text exception
     */
    private static final String THROW_TEXT_EXCEPTION = "throw.text.exception";

    /** Config property indicating that the component should throw an exception
     *  on service unit undeploy. 
     */
    private static final String REGISTER_CUSTOM_MBEAN = "register.custom.mbean";    
    
    /**
     * Local copy of the component name.
     */
    protected String mComponentName;

    /**
     * Type of component.
     */
    protected String mComponentType = "Binding";

    /**
     * Local handle to the ComponentContext.
     */
    protected ComponentContext mContext;

    /**
     * Logger instance.
     */
    protected Logger mLog = Logger.getLogger("com.sun.jbi.management");
    
    /**
     * Component configuration
     */
    private Properties mConfig = new Properties();
    
    //
    // Component methods
    //

    /**
     * Get the ComponentLifeCycle implementation instance for this Binding
     * Component.
     * @return the life cycle implementation instance.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        mLog.info(mComponentType + " getLifeCycle called");
        return this;
    }
 
    /**
     * Get the ServiceUnitManager implementation instance for this Binding
     * Component.
     * @return the Service Unit manager implementation instance.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        mLog.info(mComponentType + " " + mComponentName +
            " getServiceUnitManager called");
        return this;
    }

    /**
     * Resolve descriptor details for the specified reference, which is for a
     * service provided by this component.
     * @param ref the endpoint reference to be resolved.
     * @return the description for the specified reference.
     */
    public org.w3c.dom.Document getServiceDescription(
        javax.jbi.servicedesc.ServiceEndpoint ref)
    {
        mLog.info(mComponentType + " " + mComponentName +
            " getServiceDescription called");
        return null;
    }
    
    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually 
     * perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        mLog.info(mComponentType + " " + mComponentName +
            " isExchangeWithConsumerOkay called");
        return true;
    }
    
    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually 
     * interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        mLog.info(mComponentType + " " + mComponentName +
            " isExchangeWithProviderOkay called");
        return true;
    }

    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public javax.jbi.servicedesc.ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment epr)
    {
        mLog.info(mComponentType + " " + mComponentName +
            " resolveEndpointReference called");
        return null;
    }

    //
    // ComponentLifeCycle methods
    //

    /**
     * Initialize the Binding Component.
     * @param context the JBI component context created by the JBI framework.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void init(ComponentContext context)
        throws javax.jbi.JBIException
    {
        mComponentName = context.getComponentName();
        mContext = context;
        File configFile;
        InputStream inputStream;
        try
        {
            configFile = new File(context.getInstallRoot(), CONFIG_FILE_NAME);
            if (configFile.exists())
            {
                inputStream = new java.io.FileInputStream(configFile);
                mConfig.load(inputStream);
                inputStream.close();
            }
            
            //write the timestamp in a properties file in workspace dir
            try
            {
                File settingsFile = new File(context.getWorkspaceRoot(), "settings.txt");
                FileWriter writer = new FileWriter(settingsFile);
                SimpleDateFormat format =  new SimpleDateFormat("yyyyMMddHHmm");
                writer.write(format.format(new Date()));
                writer.close();
            } 
            catch (Exception ex)
            {
            }
           
        }
        catch (Exception ex)
        {
            throw new javax.jbi.JBIException(ex.toString());
        }
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. This
     * implementation always returns null.
     * @return javax.management.ObjectName is always null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        mLog.info(mComponentType + " " +
            " getExtensionMBeanName called");
        return null;
    }

    /**
     * Start the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.info(mComponentType + " " + mComponentName + " start called");
        
        if (mConfig.containsKey(THROW_ON_COMP_START))
        {
            throw new javax.jbi.JBIException(
                    createErrorResult("start", "Component start failed"));
        }        
        if ( mConfig.containsKey(REGISTER_CUSTOM_MBEAN) )
        {
            registerCustomMBeans();
            registerLoggerMBean();
        }
    }

    /**
     * Stop the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLog.info(mComponentType + " " + mComponentName + " stop called");

        if ( mConfig.containsKey(REGISTER_CUSTOM_MBEAN) )
        {
            unregisterCustomMBeans();
        }
    }

    /**
     * Shut down the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLog.info(mComponentType + " " + mComponentName + " shutDown called");
        
        if (mConfig.containsKey(THROW_ON_COMP_SHUTDOWN))
        {
            throw new javax.jbi.JBIException(
                    createErrorResult("shutDown", "Component shutDown failed"));
        }
    }

    //
    // ServiceUnitManager methods
    //
 
    /**
     * Deploy a Service Unit.
     * @param serviceUnitName the name of the Service Unit being deployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return a deployment status message.
     * @throws javax.jbi.management.DeploymentException if the deployment
     * operation is unsuccessful.
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        if (mConfig.containsKey(THROW_ON_SU_DEPLOY))
        {
            if (mConfig.containsKey(THROW_TEXT_EXCEPTION))
            {
                throw new javax.jbi.management.DeploymentException("Deployment of service unit " + serviceUnitName + " FAILED.");
            }
            else
            {
                throw new javax.jbi.management.DeploymentException(
                        createErrorResult("init", "SU deploy failed"));
            }
        }
                
        mLog.info(mComponentType + " " + mComponentName +
            " deployed Service Unit " + serviceUnitName + " with serviceUnitRootPath "
                + serviceUnitRootPath);
        
        if ( serviceUnitName.equals("esbadmin00089-su-3") ||
             serviceUnitName.equals("esbadmin00089-su-6"))
        {
            return createDeployResult("deploy", false);
        }
        else
        {
            return createDeployResult("deploy", true);
        }
    }

    /**
     * Initialize the deployment.
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state.
     */
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info(mComponentType + " " + mComponentName +
            " initialized Service Unit " + serviceUnitName + " with serviceUnitRootPath "
                + serviceUnitRootPath);
        
        if (mConfig.containsKey(THROW_ON_SU_INIT))
        {
            throw new javax.jbi.management.DeploymentException(
                    createErrorResult("init", "SU initialization failed"));
        }
        
        java.io.File suRoot = new java.io.File(serviceUnitRootPath);
        
        if ( !suRoot.exists() )
        {
             throw new javax.jbi.management.DeploymentException("Invalid service unit root " 
                + serviceUnitRootPath + " for service unit " + serviceUnitName);
        }
    }

    /**
     * Shut down the deployment.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void shutDown(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        if (mConfig.containsKey(THROW_ON_SU_SHUTDOWN))
        {
            throw new javax.jbi.management.DeploymentException(
                    createErrorResult("shutDown", "Service unit " + serviceUnitName 
                        + " shutDown failed"));
        }
        
        mLog.info(mComponentType + " " + mComponentName +
            " shut down Service Unit " + serviceUnitName);
    }

    /**
     * Start the deployment.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        
        if (mConfig.containsKey(THROW_ON_SU_START))
        {
            throw new javax.jbi.management.DeploymentException(
                    createErrorResult("start", "Service unit " + serviceUnitName
                        + " start failed"));
        }
        
        mLog.info(mComponentType + " " + mComponentName +
            " started Service Unit " + serviceUnitName);
    }

    /**
     * Stop the deployment.
     * @param serviceUnitName the name of the Service Unit being stopped.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void stop(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info(mComponentType + " " + mComponentName +
            " stopped Service Unit " + serviceUnitName);
    }

    /**
     * Undeploy a Service Unit from the component.
     * @param serviceUnitName the name of the Service Unit being undeployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return an undeployment status message.
     * @throws javax.jbi.management.DeploymentException if the undeployment
     * operation is unsuccessful.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info(mComponentType + " " + mComponentName +
            " undeployed Service Unit " + serviceUnitName);
                
        if (mConfig.containsKey(THROW_ON_SU_UNDEPLOY))
        {
            throw new javax.jbi.management.DeploymentException(
                    createErrorResult("undeploy", "Service unit " + serviceUnitName 
                        + " undeploy failed"));
        }
        else
        if ( serviceUnitName.equals("esbadmin00089-su-3") ||
             serviceUnitName.equals("esbadmin00089-su-6"))
        {
            return createDeployResult("undeploy", false);
        }
        else
        {
            return createDeployResult("undeploy", true);
        }
    }

    public void onUninstall() 
        throws javax.jbi.JBIException
    {
    }

    public void onInstall() throws javax.jbi.JBIException
    {
        
    }

    public void init(javax.jbi.component.InstallationContext installationContext)
        throws javax.jbi.JBIException
    {
        
    }

    public void cleanUp() throws javax.jbi.JBIException
    {
        
    }
    
    /** Creates a (un)deployment result string.
     *  @param task 'deploy' or 'undeploy'
     */
    private String createDeployResult(String task, boolean isSuccess)
    {
        return "<component-task-result xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"
            + "<component-name>" + mComponentName + "</component-name>"
            + "<component-task-result-details>"
            + "<task-result-details>"
            + "<task-id>" + task + "</task-id>"
            + "<task-result>" + (isSuccess ? "SUCCESS" : "FAILED") + "</task-result>"
            + "</task-result-details>"
            + "</component-task-result-details>"
            + "</component-task-result>";
    }
    
    /** Creates a component failure response with a configurable message.
     *  @param task 'deploy' or 'undeploy'
     */
    protected String createErrorResult(String task, String msg)
    {
        return "<component-task-result xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"
            + "<component-name>" + mComponentName + "</component-name>"
            + "<component-task-result-details>"
            + "<task-result-details>"
            + "<task-id>" + task + "</task-id>"
            + "<task-result>" + "FAILED" + "</task-result>"
            + "<message-type>ERROR</message-type>"
            + "<task-status-msg>"
            + "<msg-loc-info>"
            + "<loc-token>JBIWHOOPS</loc-token>"
            + "<loc-message>" + msg + "</loc-message>"
            + "</msg-loc-info>"
            + "</task-status-msg>"
            + "</task-result-details>"
            + "</component-task-result-details>"
            + "</component-task-result>";
    }
    
    /**
     * Register custom MBeans
     */
    protected void registerCustomMBeans()
        throws javax.jbi.JBIException
    {
        try
        {
            CustomConfigMBean mbean = new CustomConfig(mComponentName + " custom config MBean");

            StandardMBean compMBean = new StandardMBean(mbean, 
                    CustomConfigMBean.class);

            ObjectName compMBeanName = 
                mContext.getMBeanNames().createCustomComponentMBeanName("Configuration");

            mContext.getMBeanServer().registerMBean(compMBean, compMBeanName);
        }
        catch ( Exception exp )
        {
            throw new javax.jbi.JBIException(exp.getMessage());
        }
    }
    
    /**
     * Unregister custom MBeans
     */
    protected void unregisterCustomMBeans()
        throws javax.jbi.JBIException
    {
        try
        {
            ObjectName compMBeanName = 
                mContext.getMBeanNames().createCustomComponentMBeanName("Configuration");

            mContext.getMBeanServer().unregisterMBean(compMBeanName);
        }
        catch ( Exception exp )
        {
            throw new javax.jbi.JBIException(exp.getMessage());
        }
    }
    
    /**
     * ComponentContext.getLogger() registers a Logger MBean for the component. 
     * When the component is uninstalled the Logger MBean is unregistered.
     */
    protected void registerLoggerMBean()
       throws javax.jbi.JBIException
    {
        mContext.getLogger("com.sun.jbi.test.binding.logger", null);
    }
}
