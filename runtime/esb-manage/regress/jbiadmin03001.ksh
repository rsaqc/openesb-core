#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin03001.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
#test to verify that event listeners for create registers the ComponentFacadeMBeans
#start domain  
#create an instance before any JBI specific activity is done
#execute a lifecycle operation on a system component
#this verifies that the ComponentFacadeMBeans are registered for this instance
####

echo "jbiadmin03001 : Test fix for CR 6532519 - not able to start system components in standalone instance"

my_test_domain=ESBTest
. ./regress_defs.ksh
. $AS8BASE/ESBTest_port.sh

start_domain

echo Creating nodeagent esbagent1
cmd="asadmin create-node-agent -t --port $ASADMIN_PORT $ASADMIN_PW_OPTS esbagent1"
1>&2 eval $cmd
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
    shutdown_domain
    exit 1
fi

echo Creating instance esbinst1
cmd="asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --nodeagent esbagent1 esbinst1"
1>&2 eval $cmd
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
    shutdown_domain
    exit 1
fi

echo Starting sun-http-binding on target esbinst1
asadmin start-jbi-component --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target=esbinst1 sun-http-binding 2>&1

echo Deleting instance esbinst1
cmd="asadmin delete-instance --port $ESBTest_admin_port $ASADMIN_PW_OPTS esbinst1"
1>&2 eval $cmd
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
    exit 1
fi

echo Deleting node agent config for esbagent1
cmd="asadmin delete-node-agent-config --port $ESBTest_admin_port $ASADMIN_PW_OPTS esbagent1"
1>&2 eval $cmd
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
    exit 1
fi

echo Deleting node agent esbagent1
cmd="asadmin delete-node-agent esbagent1"
1>&2 eval $cmd
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
    exit 1
fi

shutdown_domain
exit 0
