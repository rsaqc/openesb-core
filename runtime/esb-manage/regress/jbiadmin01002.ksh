#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01002.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01002 - Test repeated lifecycle operations

echo testname is jbiadmin01002
. ./regress_defs.ksh

SA_DEPLOY_ARCHIVE=$JV_SVC_TEST_CLASSES/testdata/service-assembly-te.zip
SA_NAME=9ae51feb-0fc3-4ff6-wsdl-deployment13

# Setup
$JBI_ANT -Djbi.install.file=$MANAGE_TEST_SL install-shared-library
$JBI_ANT -Djbi.install.file=$JBI_KIT_TE install-component

# Start a component twice
$JBI_ANT -Djbi.component.name=SunTransformationEngine start-component
$JBI_ANT -Djbi.component.name=SunTransformationEngine start-component

# Deploy the SA and try to start it twice
$JBI_ANT -Djbi.deploy.file=$SA_DEPLOY_ARCHIVE deploy-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME start-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME start-service-assembly

# Stop and shutdown the SA twice
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME stop-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME stop-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME shut-down-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME shut-down-service-assembly

# Try and stop the shutdown SA (this should fail)
$JBI_ANT_NEG -Djbi.service.assembly.name=$SA_NAME stop-service-assembly

# Start the SA and then invoke shutdown as a shortcut (this should work)
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME start-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME shut-down-service-assembly

# Get rid of the service assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME undeploy-service-assembly

# Stop and shutdown the component twice
$JBI_ANT -Djbi.component.name=SunTransformationEngine stop-component
$JBI_ANT -Djbi.component.name=SunTransformationEngine stop-component
$JBI_ANT -Djbi.component.name=SunTransformationEngine shut-down-component
$JBI_ANT -Djbi.component.name=SunTransformationEngine shut-down-component

# Try and stop the shutdown component (this should fail)
$JBI_ANT_NEG -Djbi.component.name=SunTransformationEngine stop-component

# Clean up
$JBI_ANT -Djbi.component.name=SunTransformationEngine uninstall-component
$JBI_ANT -Djbi.shared.library.name=TestSharedLibrary uninstall-shared-library
