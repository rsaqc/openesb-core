/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventRepositoryConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.eventmanagement.impl;

public interface EventRepositoryConstants {
    
     static final String ALERTS_TASK_ID = "ALERT_MAMANGEMENT_TASK";

     static final  String ONE_OF_GLASSFISH_DOMAIN_NAMES = "com.sun.appserv"; 
     static final long EVENTID_SEED = 10000000; 
     static String EVENT_STORE_DB_NAME =  "EventStoreDb";

     static final String EVENT_POLICY_FILTER_TYPE = "alertsDB.removalPolicy";

     static final String EVENT_STORE_GET_EVENT_WITH_INSTANCE_FILTER = 
        "SELECT eventid,eventdata FROM NOTIFICATION_EVENT  where eventid = '?0' and instancename = '?1' and  portnumber = '?2' ";
     static final String EVENT_STORE_GET_EVENT = 
        "SELECT eventid,eventdata FROM NOTIFICATION_EVENT  where eventid = '?0'";
     static final String EVENT_STORE_DELETE_EVENT_BY_ID_WITH_INSTANCE_FILTER = 
        "DELETE FROM NOTIFICATION_EVENT  where eventid = '?0' and instancename = '?1' and  portnumber = '?2' ";
     static final String EVENT_STORE_DELETE_EVENT_BY_ID = 
        "DELETE FROM NOTIFICATION_EVENT  where eventid = '?0' ";
     static final String EVENT_STORE_DELETE_EVENT_BY_DATE_WITH_INSTANCE_FILTER = 
        "DELETE FROM NOTIFICATION_EVENT  where eventdate < ?0 and instancename = '?1' and  portnumber = '?2' ";
     static final String EVENT_STORE_DELETE_EVENT_BY_DATE = 
        "DELETE FROM NOTIFICATION_EVENT  where eventdate < ?0 ";
     static final String EVENT_STORE_DELETE_EVENT_BY_LEVEL_WITH_INSTANCE_FILTER = 
        "DELETE FROM NOTIFICATION_EVENT  where eventlevel >= ?0 and instancename = '?1' and  portnumber = '?2' ";
     static final String EVENT_STORE_DELETE_EVENT_BY_LEVEL = 
        "DELETE FROM NOTIFICATION_EVENT  where eventlevel >= ?0  ";

     static final String EVENT_STORE_SAVE_EVENT =
        "INSERT INTO NOTIFICATION_EVENT  (eventid,eventdata,eventlevel,eventdate,instancename,portnumber) VALUES (?,?,?,?,?,?)";

     static final String EVENT_STORE_TABLE_EXIST_TEST = 
        "select table_name,table_type from system_tables where table_name=" +
            "'NOTIFICATION_EVENT '";
 
     static final String EVENT_STORE_GET_EVENT_IDS_WITH_INSTANCE_FILTER = 
        "SELECT eventid,eventid FROM NOTIFICATION_EVENT  where eventid > '?' and instancename = '?' and  portnumber = '?' order by eventid asc";
     static final String EVENT_STORE_GET_EVENT_IDS = 
        "SELECT eventid,eventid FROM NOTIFICATION_EVENT  where eventid > '?' order by eventid asc";
    
     static final String EVENT_STORE_GET_EVENT_IDS_WITHOUT_FILTER = 
        "SELECT eventid,eventid FROM NOTIFICATION_EVENT  where instancename = '?' and  portnumber = '?' order by eventid asc";
    
     static final String EVENT_STORE_EVENT_COUNT_WITH_INSTANCE_FILTER = 
        "SELECT count(eventid),count(eventid) FROM NOTIFICATION_EVENT  where instancename = '?' and  portnumber = '?'";
     static final String EVENT_STORE_EVENT_COUNT = 
        "SELECT count(eventid) FROM NOTIFICATION_EVENT  ";

     static final String EVENT_STORE_GET_EVENT_DATES_WITH_INSTANCE_FILTER = 
        "SELECT eventdate FROM NOTIFICATION_EVENT  where instancename = '?' and  portnumber = '?' order by eventdate desc";
     static final String EVENT_STORE_GET_EVENT_DATES = 
        "SELECT eventdate FROM NOTIFICATION_EVENT   order by eventdate desc";
   
     static final String INSERT_STATEMENT_INSERT_PART ="insert into NOTIFICATION_EVENT (id, timeStamp, physicalHostName, environmentName," +
     		" logicalHostName, serverType, serverName, componentType, componentProjectPathName, componentName," +
     		" eventType, severity, operationalState, messageCode, messageDetail, observationalState, deploymentName) ";

     static final String INSERT_STATEMENT_VALUES_PART_STD =" values ( null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
     static final String INSERT_STATEMENT_VALUES_PART_ORACLE =" values ( autoincrement_id.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
     

}
