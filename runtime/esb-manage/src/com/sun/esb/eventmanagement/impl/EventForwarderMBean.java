/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventForwarderMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanRegistration;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.RuntimeOperationsException;
import javax.management.openmbean.CompositeData;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;

import com.sun.esb.eventmanagement.api.AlertPersistenceDBType;
import com.sun.esb.eventmanagement.api.Event;
import com.sun.esb.eventmanagement.api.InstanceIdentifier;
import com.sun.esb.eventmanagement.api.NotificationEvent;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.platform.PlatformContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * this class is a mbean that process the JCAP & JBI generate events. it will 
 * forward the messages it received from the event listener to the the enterpise
 * monitor (EM) that monitoring the given integration server. It assume that 
 * there is only 1 EM it need to forward the event to. if the event forwrding 
 * encounters errors or the remote jmx call return false. the message will be 
 * persisted and forward at later time when the remote EM reconnect. 
 * Window - Preferences - Java - Code Style - Code Templates
 */
public final class EventForwarderMBean extends NotificationBroadcasterSupport
    implements DynamicMBean, MBeanRegistration, Serializable,
    EventManagementConstants {


    ExecutorService mExecutorService;
    /**
     * MBean attributes
     */
    protected ArrayList<MBeanAttributeInfo> mAttributesInfo = 
            new ArrayList<MBeanAttributeInfo>();

    /**
     * MBean constructors
     */
    protected ArrayList<MBeanConstructorInfo> mConstructorsInfo = 
            new ArrayList<MBeanConstructorInfo>();

    /**
     * MBean operations
     */
    protected ArrayList<MBeanOperationInfo> mOperationsInfo = 
            new ArrayList<MBeanOperationInfo>();

    /**
     * MBean notifications
     */
    protected ArrayList<MBeanNotificationInfo> mNotificationsInfo = 
             new ArrayList<MBeanNotificationInfo>();
    /**
     * MBean info
     */
    protected MBeanInfo mMBeanInfo;

    /**
     * the MBeanServer
     */
    
    private JMXConnector mConnector;

    private static final Logger mLogger;
    private static boolean isDebugEnabled;
    static {
       mLogger = Logger.getLogger(EventForwarderMBean.class.getName());
        isDebugEnabled = mLogger.isLoggable(Level.FINEST);
    
    }

    private Properties mPersistedProps;
    private MBeanServerConnection mMBeanServer;
    transient private DBEventStore mEventStore;
    transient private Thread eventForwarderThread;

    private String mHostingHostName;
    private String mHostingHttpPort;

    private boolean mShutdownInProgress;
    private String mLastCachedId;
    private long mNotificationSeqNum;


    private String mJBIinstanceRoot;
    private EventForwardingHelper mEventForwardingHelper;
    private AlertPersistenceConfiguration mAlertPersistenceConfiguration;
    private PlatformContext mPlatformContext;
//    private ConcurrentLinkedQueue<NotificationEvent> mEventsQueue;
    private  LinkedBlockingQueue<CompositeData> mEventsQueue;
    private boolean  mExecutorActive;
    private Object mSynchronizationObject = new Object();
    private Boolean mPersisteanceeEnabled = new Boolean(false);
    private AlertPersistenceDBType mDBType;
    private String  mJNDIName;
    private String mDomainUniqueAlertTableName;
    private EventDataConverter mEventDataConverter;
    private String mPhysicalHostAndPort;
    /**
     * 
     */
    public EventForwarderMBean(EnvironmentContext context) throws Exception{
        buildDynamicMBeanInfo();
        
        
        mPlatformContext =  context.getPlatformContext();
        mExecutorService = Executors.newFixedThreadPool(2);
//      mEventsQueue = new ConcurrentLinkedQueue<NotificationEvent>(); 
        mEventsQueue = new LinkedBlockingQueue<CompositeData>(); 
        mJBIinstanceRoot = context.getJbiInstanceRoot()+ EVENT_CONFIG_DIRECTORY;
        mMBeanServer = mPlatformContext.getMBeanServer();
        mEventForwardingHelper =  new EventForwardingHelper(mJBIinstanceRoot); 
        mAlertPersistenceConfiguration = AlertPersistenceConfiguration.getInstance(mMBeanServer);
        mAlertPersistenceConfiguration.setAdminServer(mPlatformContext.isAdminServer());
        mPersistedProps = mEventForwardingHelper.GetProperties();
        initializeSetting();
        mEventStore = DBEventStore.getInstance(mAlertPersistenceConfiguration,
                                        mMBeanServer,mPlatformContext);
        if(mAlertPersistenceConfiguration.getPersistenceEnabled().booleanValue()) {
            try {
                mEventStore.initializeDataSource();
            } catch (ManagementRemoteException e) {
                mLogger.log(Level.WARNING, AlertUtil.getPackageBundle().
                        getMessage("caps.management.server.alert.persistence.setup.error.log.msg"), e);
            }
        }

        mAlertPersistenceConfiguration.setDBEventStore(mEventStore);
    }

   /**
     * Build the protected MBeanInfo field,
     * which represents the management interface exposed by the MBean;
     * i.e., the set of attributes, constructors, operations and notifications
     * which are available for management.
     *
     * A reference to the MBeanInfo object is returned by the getMBeanInfo()
     * method of the DynamicMBean interface. Note that, once constructed,
     * an MBeanInfo object is immutable.
     */
    protected void buildDynamicMBeanInfo() {

       mConstructorsInfo.add(
          new MBeanConstructorInfo(
             "EventForwarderMBean Constructor",
             getClass().getConstructors()[0]));
 
        addOperationsInfo();
        addAttributesInfo();
        
         mMBeanInfo =  new MBeanInfo(
             getClass().getName(),
             "Event Forwarder DynamicMBean",
             (MBeanAttributeInfo[]) mAttributesInfo.toArray(
                new MBeanAttributeInfo[mAttributesInfo.size()]),
             (MBeanConstructorInfo[]) mConstructorsInfo.toArray(
                new MBeanConstructorInfo[mConstructorsInfo.size()]),
             (MBeanOperationInfo[]) mOperationsInfo.toArray(
                new MBeanOperationInfo[mOperationsInfo.size()]),
             (MBeanNotificationInfo[]) mNotificationsInfo.toArray(
                new MBeanNotificationInfo[mNotificationsInfo.size()]));

    }


    public Object getAttribute(String aName)
       throws AttributeNotFoundException, MBeanException, ReflectionException {

        
        if(aName.equals(ENABLE_PERSISTENCE_ATTRIBUTE_NAME) == true || 
           aName.equals(PERSISTENCE_ENABLED_ATTRIBUTE_NAME)) {
            synchronized (mAlertPersistenceConfiguration) {
                return mAlertPersistenceConfiguration.getPersistenceEnabled();
            }
        } else if(aName.equals(DATABASE_JNDI_NAME__ATTRIBUTE_NAME) == true) {
                return mAlertPersistenceConfiguration.getDataSourceJndiName();
        } else if(aName.equals(DATABASE_TYPE_ATTRIBUTE_NAME) == true) {
                return mAlertPersistenceConfiguration.getAlertPersistenceDBType().getDatabasetype();
        } else if(aName.equals(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME) == true) {
                return mAlertPersistenceConfiguration.getAlertTableName();
        } else if(aName.equals(PHYSICAL_HOST_PORT_ATTRIBUTE_NAME) == true) {
                return mPhysicalHostAndPort == null ? this.getHostAndPort() : mPhysicalHostAndPort;
        } else {
            throw new AttributeNotFoundException();
        }
       
    }

    /**
     */
    public void setAttribute(Attribute aAttribute)
       throws
          AttributeNotFoundException,
          InvalidAttributeValueException,
          MBeanException,
          ReflectionException {
        
        
        if(aAttribute.getName().equals(ENABLE_PERSISTENCE_ATTRIBUTE_NAME)
                == true) {
            synchronized (mAlertPersistenceConfiguration) {
                try {
                    mAlertPersistenceConfiguration.setPersistenceEnabled((Boolean) aAttribute.getValue());
                } catch (ManagementRemoteException e) {
                }
            }
        } else if(aAttribute.getName().equals(DATABASE_JNDI_NAME__ATTRIBUTE_NAME) == true) {
            String aNewJndi = (String) aAttribute.getValue();
            synchronized (mAlertPersistenceConfiguration) {
                mAlertPersistenceConfiguration.setDataSourceJndiName(aNewJndi);
            }
            if(mAlertPersistenceConfiguration.getPersistenceEnabled()) {
                this.mEventStore.reInitializeDataSource(aNewJndi);
            }
        } else if(aAttribute.getName().equals(DATABASE_TYPE_ATTRIBUTE_NAME) == true) {
            synchronized (mAlertPersistenceConfiguration) {
                mAlertPersistenceConfiguration.setAlertPersistenceDBType(AlertPersistenceDBType.valueOf((String) aAttribute.getValue()));
            }
        }else if(aAttribute.getName().equals(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME) == true) {
            synchronized (mAlertPersistenceConfiguration) {
                mAlertPersistenceConfiguration.setAlertTableName((String) aAttribute.getValue());
            }
        }
        
        // if the MBean reside on the instance make sure it
        // save the persistence info. On the DAS the controller
        // MBean will save the information
        if(!mPlatformContext.isAdminServer()) {
            Properties alertRemovalProps = mAlertPersistenceConfiguration.getPersistenceProperties();
            mPersistedProps.putAll(alertRemovalProps);
            mEventForwardingHelper.saveProperties(mPersistedProps);
        }     
    }

    /**
     */
    public MBeanInfo getMBeanInfo() {
       return mMBeanInfo;
    }
    /**
     * Retrieves the value of specified attributes of the Dynamic MBean
     * @param aNames aNames of the attributes
     * @return AttributeList list of attribute aNames and values
     */
    public AttributeList getAttributes(String[] aNames) {
       AttributeList attributes = new AttributeList(aNames.length);
       for (int i = 0; i < aNames.length; i++) {
            try {
                attributes.add(new Attribute(aNames[i], 
                        getAttribute(aNames[i])));
            } catch (Exception e) {
                if(mLogger.isLoggable(Level.FINE)) {
                    mLogger.fine(">>>>EventForwarderMBean :: Unable to add attribute " 
                        + aNames[i] + " "+ e);
                }
                continue;
            }
        }

        return attributes;
    }
    
    


    /**
     * Sets the value of specified aAttributes of the Dynamic MBean.
     * @param aAttributes list of attribute names and values.
     * @return AttributeList list of attribute names and values
     */
    public AttributeList setAttributes(AttributeList aAttributes) {
       for (Iterator it = aAttributes.iterator(); it.hasNext();) {
          Attribute attribute = (Attribute) it.next();
          try {
             setAttribute(attribute);
          } catch (Exception e) {
             continue;
          }
       }
       return aAttributes;
    }
 
    
    /**
     * Invokes an operation on the Dynamic MBean.
     * @param operationName The name of the action to be invoked
     * @param params An array containing the parameters to be set when the
     * action is invoked
     * @param signature An array containing the aSignature of the action.
     * The class objects will be loaded through the same class loader as
     * the one used for loading the MBean on which the action is invoked.
     * @return The object returned by the action, which represents the result
     * of invoking the action on the MBean specified
     * @throws MBeanException MBeanException
     * @throws ReflectionException ReflectionException
     */
    public Object invoke(
       String operationName,
       Object params[],
       String signature[]) throws MBeanException, ReflectionException {
       Object result = null; 
       // Check operationName is not null to avoid NullPointerException later on
       if (operationName.equalsIgnoreCase(EVENT_FORWARD_OPERATION)) {
           NotificationEvent lEvent = null;
           if(params[0] instanceof CompositeData) {
               // support for open jbi components
             EventDataConverter converter =  new EventDataConverter();
             lEvent = converter.getEventFromCompositeData((CompositeData)params[0] );
           } else {
               // support for classic caps
               lEvent = (NotificationEvent)params[0];
           }    
           forwardEvent(lEvent);
       }  else if (operationName.equalsIgnoreCase(EVENT_CREATEFORWARD_OPERATION)) {
           String physicalHostName = (String)params[0];
           String serverType = (String)params[1];
           String componentType = (String)params[2];
           String type = (String)params[3];
           int severity = ((Integer)params[4]).intValue();
           String messageCode = (String)params[5];
           String messageDetails  = (String)params[6];           
           if(params.length == 7) {
               createAndForwordEvent(physicalHostName, serverType, componentType, 
                       type, severity, messageCode, messageDetails); 
           }else if(params.length == 8){ // new api 
               int state = ((Integer)params[7]).intValue();
               createAndForwordEvent(physicalHostName, serverType, componentType, 
                               type, severity, messageCode, messageDetails,state); 
               
           }else if(params.length == 10){ 
               int state = ((Integer)params[7]).intValue();
               String  serverName = (String)params[8];
               String  componentName = (String)params[9];
               createAndForwordEvent(physicalHostName, serverType, componentType, 
                               type, severity, messageCode, messageDetails,state,
                               serverName, componentName); 
               
           }else {
               throw new RuntimeOperationsException(
                       new IllegalArgumentException("invalid number of arguments only 7,8 or 10 are allowed"));
               
           }
       } else if ( operationName.equals("testSendEvent") ) {
            String message = (String)params[0];
            Integer severity = (Integer)params[1];
       	    testSendEvent(message,severity);
       } else if ( operationName.equals("testSendEvents") ) {
           String message = (String)params[0];
           Boolean randomize = (Boolean)params[1];
           Integer count = (Integer)params[2];
     	    testSendEvents(message,randomize,count);
      } else if ( operationName.equals(GET_QUEUED_EVENTS) ) {
          result = getQueuedEvents();
     }
       
       
       return result;
    }
    public void postDeregister() {
    }

    public void postRegister(Boolean registrationDone) {

    }

    public void preDeregister() {
       // shutdown the event management message server for publishing events
        mShutdownInProgress = true; 
        mEventStore.unRegisterTimerService();
    }

    
   
    public ObjectName preRegister(MBeanServer server, ObjectName name) {
       return name;
    }
    
    public void forwardEvent(NotificationEvent aEvent) throws MBeanException {
        
        if(mShutdownInProgress) {
            mExecutorService.shutdown();
            return;
        }

        if(mEventDataConverter == null) {
            mEventDataConverter = new EventDataConverter();
        }
        
        fixEventPhysicalHostAttribute(aEvent);
        fixEventServerNameAttribute(aEvent);

        synchronized(mAlertPersistenceConfiguration) {
            // 11/27 - persistence on prevent event from being forwarded to the listener/s.
            // it only persisted in the database. The forwarding channel is not a reliable
            // channel.
            if(mAlertPersistenceConfiguration.getPersistenceEnabled().booleanValue() == true) {
                try {
                    mEventStore.save(aEvent);
                } catch (ManagementRemoteException e) {
                   throw new MBeanException(e);
                }
                return;
            }
        }

        try {
            CompositeData compositeData = 
                mEventDataConverter.getEventCompositeData(aEvent,"");
            mEventsQueue.add(compositeData);
        } catch (Exception e) {
            throw new MBeanException(e);
        }
         
        synchronized (mSynchronizationObject) {
            if(!mExecutorActive) {
                mExecutorService.execute(new EventQueueHandler());
                mExecutorActive = true;
            }
        }
    }
    
    private Object getQueuedEvents() {
        Collection<CompositeData> eventsData = new ArrayList<CompositeData>();
        mEventsQueue.drainTo(eventsData);
        return eventsData;
    }
    
    private void  createAndForwordEvent(String physicalHostName,
            String serverType,
            String componentType,
            String type,
            int severity,
            String messageCode,
            String messageDetails ) {

        NotificationEvent lEvent = new NotificationEventImpl();
        lEvent.setPhysicalHostName(physicalHostName);
        lEvent.setServerType(serverType);
        lEvent.setComponentType(componentType);
        lEvent.setType(type);
        lEvent.setSeverity(severity);
        lEvent.setMessageCode(messageCode);
        lEvent.setMessageDetails(messageDetails);
        lEvent.setTimeStamp(new Date().getTime());
        
        try {
            forwardEvent(lEvent);
        } catch (MBeanException e) {
            mLogger.log(Level.FINEST, "forwarding event failed", e);       
        }
    }

    private void  createAndForwordEvent(String physicalHostName,
                    String serverType,
                    String componentType,
                    String type,
                    int severity,
                    String messageCode,
                    String messageDetails,
                    int opState) {

                NotificationEvent lEvent = new NotificationEventImpl();
                lEvent.setPhysicalHostName(physicalHostName);
                lEvent.setServerType(serverType);
                lEvent.setComponentType(componentType);
                lEvent.setType(type);
                lEvent.setSeverity(severity);
                lEvent.setMessageCode(messageCode);
                lEvent.setMessageDetails(messageDetails);
                lEvent.setTimeStamp(new Date().getTime());
                lEvent.setOperationalState(opState);
                
                try {
                    forwardEvent(lEvent);
                } catch (MBeanException e) {
                    mLogger.log(Level.FINEST, "forwarding event failed", e);       
                }
            }
    private void  createAndForwordEvent(String physicalHostName,
                    String serverType,
                    String componentType,
                    String type,
                    int severity,
                    String messageCode,
                    String messageDetails,
                    int opState,
                    String serverName,
                    String componentName) {

                NotificationEvent lEvent = new NotificationEventImpl();
                lEvent.setPhysicalHostName(physicalHostName);
                lEvent.setServerType(serverType);
                lEvent.setComponentType(componentType);
                lEvent.setType(type);
                lEvent.setSeverity(severity);
                lEvent.setMessageCode(messageCode);
                lEvent.setMessageDetails(messageDetails);
                lEvent.setTimeStamp(new Date().getTime());
                lEvent.setOperationalState(opState);
                lEvent.setComponentName(componentName);
                lEvent.setServerName(serverName);
               
                try {
                    forwardEvent(lEvent);
                } catch (MBeanException e) {
                    mLogger.log(Level.FINEST, "forwarding event failed", e);       
                }
            }

    
  
    private void fixEventPhysicalHostAttribute(Event aEvent) {
        if(aEvent == null) {
            return;
        }
        String lPhysicalHost = getHostAndPort();
        aEvent.setPhysicalHostName(lPhysicalHost);
    }
    
    private String getPhysicalHostName() {
        String hostName = null;
       try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
        }
        if(hostName == null) {
            try {
                hostName = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e1) {
                mLogger.log(Level.FINEST, "unknow host failure", e1);       
            }
        }
        if(hostName == null) {
            hostName = "127.0.0.1";
        }
        return hostName;
    }
    
    private void fixEventServerNameAttribute(Event aEvent) {
        if(aEvent == null) {
            return;
        }
        String serverName = mPlatformContext.getInstanceName();
        
        aEvent.setServerName(serverName);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    private void testSendEvent(String message,Integer severity) {
       message =  message == null ? "" : message;
        
       // for physical host name we send the instance name
       createAndForwordEvent(mPlatformContext.getInstanceName(),
            NotificationEvent.SERVER_TYPE_INTEGRATION,"EventManagement",
            NotificationEvent.EVENT_TYPE_ALERT,severity,
            "CUSTOM-00001",message);
    }
    
    private void testSendEvents(String message,Boolean randomSeverity,Integer alertCount) {
        message =  message == null ? "" : message;
        int severity = NotificationEvent.SEVERITY_TYPE_INFO;
        int count = alertCount == null || alertCount.intValue() < 1 ? 1 : alertCount.intValue();
        Random r = new Random(100000);
        boolean randomizeSeverity = randomSeverity == null ? false : randomSeverity.booleanValue();
        for (int i=0; i<count; i++) {
            if(randomizeSeverity == true) {
                severity = r.nextInt() % 6;
                if(severity < 0) {
                    severity = -severity; // make sure the severity is positive value
                }
            }
            testSendEvent(i+": "+message,severity);
        }
     }
    
    private Long getPersistedEventsCount() {
        int count = mEventStore.getPersistedEventCount();
        
        return new Long(count);
    }
    
    
    private void addOperationsInfo() {
            mOperationsInfo.add(
                    new MBeanOperationInfo(
                        "testSendEvent",
                        "Send a test event via the EventForwarder",
                        new MBeanParameterInfo[] {
                                new MBeanParameterInfo("message","java.lang.String","the message assoc. with this test event"),
                                new MBeanParameterInfo("severity","java.lang.Integer","the message assoc. with this test even (0-5)")},
                        "void",MBeanOperationInfo.ACTION));    
            mOperationsInfo.add(
                    new MBeanOperationInfo(
                        "testSendEvents",
                        "Send a test event via the EventForwarder",
                        new MBeanParameterInfo[] {
                                    new MBeanParameterInfo("message","java.lang.String","the message assoc. with this test event"),
                                    new MBeanParameterInfo("randomSeverity","java.lang.Boolean","flag to indicate random severity should be applied to the test message"),
                                    new MBeanParameterInfo("alertCount","java.lang.Integer","Number of alerts to be sent") },
                        "void",MBeanOperationInfo.ACTION));    
            
            mOperationsInfo.add(
                    new MBeanOperationInfo(
                            GET_QUEUED_EVENTS,
                        "get all events currently queued in the EventForwarder",
                        null,"java.util.Collection",MBeanOperationInfo.ACTION));    
            
    }
    
    private void addAttributesInfo() {
        mAttributesInfo.add(new MBeanAttributeInfo(
                ENABLE_PERSISTENCE_ATTRIBUTE_NAME, "java.lang.Boolean",
                "flag to indicate if the db persistence should be enabled or disabled. " ,
                true, true, false));

       
       mAttributesInfo.add(new MBeanAttributeInfo(
               DATABASE_JNDI_NAME__ATTRIBUTE_NAME ,"java.lang.String",
               "The JNDI name for the data source used when alert are persisted",
               true, true, false));
       
       mAttributesInfo.add(new MBeanAttributeInfo(
               DATABASE_TYPE_ATTRIBUTE_NAME ,"java.lang.String",
               "The database type used for persistence - need because saving event" +
               " require diffrent statements base on database type",
               true, true, false));
     
       mAttributesInfo.add(new MBeanAttributeInfo(
               EVENT_MANAGEMENT_UNIQUE_TABLE_NAME_ATTRIBUTE_NAME ,"java.lang.Integer",
               "Each domain in the enterprise has unique name from the alert it generated and persisted" +
               "Enterprise monitor",
               true, true, false));
       
       mAttributesInfo.add(new MBeanAttributeInfo(
               PHYSICAL_HOST_PORT_ATTRIBUTE_NAME ,"java.lang.String",
               "the Physical host name combined with the admin port for the DAS or http port for the instances",
               true, false, false));
     }
    
    
 

    private void initializeSetting() {
        
        if(mPersistedProps == null || mPersistedProps.size()==0) {
            // by default persistence is disabled as well as policy enforcement
            try {
                mAlertPersistenceConfiguration.setPersistenceEnabled(new Boolean(false));
            } catch (ManagementRemoteException e) {
                // do nothing the database setup logic does not throw exception when
                // persistence enabled set to false
            }       
            mAlertPersistenceConfiguration.setPersistenceAlertPolicyExecInterval(DEFAULT_POLICY_EXEC_INTERVAL);
            mAlertPersistenceConfiguration.setPersistedAlertRemovalPolicyExecEnabled(new Boolean(false));
            mAlertPersistenceConfiguration.setPersistenceAlertLevelPolicyValue(AlertLevelType.NONE);       
            mAlertPersistenceConfiguration.setPersistenceAlertAgePolicyValue(new Long(-1));
            mAlertPersistenceConfiguration.setPersistenceAlertCountPolicyValue(new Long(-1));
            mAlertPersistenceConfiguration.setAlertPersistenceDBType(AlertPersistenceDBType.DERBY);
            mAlertPersistenceConfiguration.setDataSourceJndiName(mAlertPersistenceConfiguration.DEFAULT_PERSISTENCE_JNDI_NAME);
            mAlertPersistenceConfiguration.setAlertTableName(createTableName());

            Properties alertRemovalProps = mAlertPersistenceConfiguration.getPersistenceProperties();
            mPersistedProps.putAll(alertRemovalProps);
            mEventForwardingHelper.saveProperties(mPersistedProps);
        }else {
            String propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_ENABLED_PROPERTY);
            try {
                mAlertPersistenceConfiguration.setPersistenceEnabled(propValue == null ? new Boolean(false) : new Boolean(propValue));
                mPersisteanceeEnabled = mAlertPersistenceConfiguration.getPersistenceEnabled();
            } catch (ManagementRemoteException e) {
                // do nothing - reading from configuration persistence can mean
                // 1. persistence not enabled and therefore database is not setup 
                // 2. persistence is  enabled and database setup was done before.
            }
            if(mPlatformContext.isAdminServer()) {
                propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_EXEC_ENABLED_PROPERTY);
                mAlertPersistenceConfiguration.setPersistedAlertRemovalPolicyExecEnabled(
                        propValue == null ? new Boolean(false) : new Boolean(propValue));
                propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_EXEC_INTERVAL_PROPERTY);
                mAlertPersistenceConfiguration.setPersistenceAlertPolicyExecInterval(
                        propValue == null ? DEFAULT_POLICY_EXEC_INTERVAL : new Long(propValue));
                propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_AGE_PROPERTY);
                mAlertPersistenceConfiguration.setPersistenceAlertAgePolicyValue(
                        propValue == null ? new Long(-1) : new Long(propValue));
                propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_COUNT_PROPERTY);
                mAlertPersistenceConfiguration.setPersistenceAlertCountPolicyValue(
                        propValue == null ? new Long(-1)  : new Long(propValue));
                propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_LEVEL_PROPERTY);
                mAlertPersistenceConfiguration.setPersistenceAlertLevelPolicyValue(
                        propValue == null ? AlertLevelType.NONE  : AlertLevelType.valueOf(propValue)); 
            }
            propValue = mPersistedProps.getProperty(EventManagementConstants.DATABASE_JNDI_NAME_PROPERTY);
            mAlertPersistenceConfiguration.setDataSourceJndiName(propValue == null ? "" :propValue);
            mJNDIName = mAlertPersistenceConfiguration.getDataSourceJndiName();
            propValue = mPersistedProps.getProperty(EventManagementConstants.DATABASE_TYPE_PROPERTY);
            mAlertPersistenceConfiguration.setAlertPersistenceDBType(propValue == null ? 
                    AlertPersistenceDBType.DERBY  : AlertPersistenceDBType.valueOf(propValue)); 
            mDBType = mAlertPersistenceConfiguration.getAlertPersistenceDBType();
            propValue = mPersistedProps.getProperty(EventManagementConstants.DATABASE_ALERT_TABLE_NAME_PROPERTY_NAME);
            mAlertPersistenceConfiguration.setAlertTableName(propValue);
            mDomainUniqueAlertTableName = mAlertPersistenceConfiguration.getAlertTableName();
        }
    }
    
    private class EventQueueHandler implements Runnable {

        public void run() {
            if (mEventsQueue.peek() != null) {
                Notification NotificationInstance = null;
                CompositeData compositeData = null;
                if(mShutdownInProgress) {
                    mExecutorService.shutdown();
                    return;
                }
                try {
                    NotificationInstance = new Notification(JCAPS_EVENT_TYPE,new ObjectName(MBeanNames.EVENT_MANAGEMENT_MBEAN_NAME),
                            mNotificationSeqNum,System.currentTimeMillis());
                } catch (MalformedObjectNameException e) {
                    //  should not fail - its this object object name
                } catch (NullPointerException e) {
                //  should not fail - the name defined in the same package
                }
                String notificationMsg = mPlatformContext.getInstanceName();
                NotificationInstance.setUserData(notificationMsg);
                sendNotification(NotificationInstance);
            }
             // queue is empty now exit.
            synchronized (mSynchronizationObject) {
                mExecutorActive = false; 
            }
       }

     }
    private String updateIdentifierInformation() {
        
        InstanceIdentifier instnaceID = InstanceIdentifierFactory.getInstanceIdentifier(mPlatformContext);
        if(instnaceID == null) {
            return  "";
            
        } else {
            return  instnaceID.getInstanceUniquePort();
        }
    }
    
    
    private String createTableName() {
        StringBuffer ltableName = new StringBuffer();
        String name =  EVENT_MAMAGENEMT_TABLE_PREFIX  + getPhysicalHostName() + updateIdentifierInformation() ;
        // remove any special char. in order to valid table name
        char[] chr = name.toCharArray();
        
        for(int i=0; i<chr.length; i++)
        {
              if(chr[i] >= '0' && chr[i] <= '9')
              {
                  ltableName.append(chr[i]);
                   continue;  
              }
              if((chr[i] >= 'A' && chr[i] <= 'Z') || (chr[i] >= 'a' && chr[i] <= 'z'))
              {
                  ltableName.append(chr[i]);
                  continue;  
              }
        }
 

        return ltableName.toString();
    }
    


    
    private String getHostAndPort() {
        InstanceIdentifier ii = 
            InstanceIdentifierFactory.getInstanceIdentifier(mPlatformContext);
         StringBuffer buffer = new StringBuffer(getPhysicalHostName());
         buffer.append(":");
         if(mPlatformContext.isAdminServer()) {
             buffer.append(ii.getDomainAdminPort());
         } else {
             buffer.append(ii.getInstanceHttpPort());
         }
         mPhysicalHostAndPort = buffer.toString();
         return mPhysicalHostAndPort;
        
    }
    
}
