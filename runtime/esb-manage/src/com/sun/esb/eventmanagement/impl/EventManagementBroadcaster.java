package com.sun.esb.eventmanagement.impl;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;

import com.sun.esb.eventmanagement.api.EventProperties;
import com.sun.jbi.EnvironmentContext;

public class EventManagementBroadcaster extends NotificationBroadcasterSupport {
    
    ConcurrentHashMap<String,NotificationListenerInfo> mListenerMap;
    private final static Executor mExecutor = new Executor() {
        // DirectExecutor using caller thread
        public void execute(Runnable r) {
            r.run();
        }
    }; 
    
    public EventManagementBroadcaster() throws Exception{
        mListenerMap = new ConcurrentHashMap<String,NotificationListenerInfo>();
    }
    
    
    
    protected void addEventNotificationListener(NotificationListener aListener, NotificationFilter aFilter, Object handback,
            String UUID) {
        if(aListener == null) {
            return;
        }
        NotificationListenerInfo nli = new NotificationListenerInfo(aListener,aFilter,handback);
        mListenerMap.put(UUID, nli);
    } 
    
    protected void removeEventNotificationListener(String UUID) {
        mListenerMap.remove(UUID);
    }

    protected Boolean isValidID(String UUID) {
        boolean exist = mListenerMap.containsKey(UUID);
        return new Boolean(exist);
    }
    
    protected void removeAllListener() {
        mListenerMap.clear();
    }
    
    protected boolean hasListeners() {
        return mListenerMap.size() > 0 ? true : false;
    }
    
    public void sendEventNotification(Notification notification) {

        if (notification == null) {
            return;
        }
        
        boolean enabled;

        for (String listenerID : mListenerMap.keySet()) {

            NotificationListenerInfo  nli = mListenerMap.get(listenerID);
            NotificationFilter nf = nli.getNotificationFilter();
            enabled = nf == null || nf.isNotificationEnabled(notification);

            if (enabled) {
                mExecutor.execute(new SendEvent(notification, nli,listenerID));
            }
        }
    }
   
    
    protected void handleNotification(NotificationListener aListener,
            Notification notif,
            Object handback) {
        
        if(aListener == null) {
            return;
        }
        super.handleNotification(aListener, notif, handback);
    }
    
    
    private class NotificationListenerInfo {
        private NotificationListener mNotificationListener;
        private NotificationFilter mNotificationFilter; 
        private Object mHandback;
        
        private NotificationListenerInfo(NotificationListener aListener, NotificationFilter aFilter, Object handback) {
            mNotificationListener = aListener;
            mNotificationFilter = aFilter; 
            mHandback = handback;
        }

        private NotificationListener getNotificationListener() {
            return mNotificationListener;
        }

        private NotificationFilter getNotificationFilter() {
            return mNotificationFilter;
        }

        private Object getHandback() {
            return mHandback;
        }
        
    }
    
    
    private class SendEvent implements Runnable {
       private Notification mNotification;
       private NotificationListenerInfo mNotificationListenerInfo;
       private String mUUID;
       public SendEvent(Notification aNotification, NotificationListenerInfo aNotificationListenerInfo,
               String aUUID) {
            mNotification = aNotification;
            mNotificationListenerInfo = aNotificationListenerInfo;
            mUUID = aUUID;
        }

        public void run() {
            try {
                handleNotification(mNotificationListenerInfo.getNotificationListener(),
                        mNotification, mNotificationListenerInfo.getHandback());
            } catch (Exception e) {
                removeEventNotificationListener(mUUID);
            }
        }

    }

}
