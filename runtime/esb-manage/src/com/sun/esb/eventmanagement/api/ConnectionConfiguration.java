/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConnectionConfiguration.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.eventmanagement.api;


import java.util.Properties;

import javax.naming.InitialContext;

import com.sun.esb.management.common.ManagementRemoteException;

/**
 * Interface for connection configuration requirements that the BPEL engine requires. 
 * The context in which the BPEL engine resides should register the implementation class 
 * on the Engine as part of its initilaization. The engine will rely on the implementation
 * to create the necessary connections and resources.
 * The intention is to keep the implementation details of such configurations independent of 
 * the core engine, wherby isolating the core engine from any dependency on the context in 
 * which it resides/run's in.
 * 
 * @author Sun Microsystems.
 *
 */
public interface ConnectionConfiguration extends ConnectionProperties {
        /**
         * Called by the core engine to create the connection pools and jdbc resources that are 
         * needed for its persistence functionality. 
         * @throws Exception RuntimeException if there is any issue while creating the connection 
         * configuration.
         */
        public void createConnectionsPools() throws ManagementRemoteException;
        
        /**
         * Access to the new naming context once the ConnectionConfiguration has updated the 
         * InitialContext, like adding new JNDI names, etc.
         * @return an instance of the InitialContext.
         */
//        public InitialContext getNamingContext();

}
