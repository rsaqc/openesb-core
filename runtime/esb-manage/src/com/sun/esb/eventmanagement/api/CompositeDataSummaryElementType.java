package com.sun.esb.eventmanagement.api;

public enum CompositeDataSummaryElementType {
    EVENTDATE("EVENTDATE"),
    EVENTID("EVENTID"),
    EVENTENVIRONMENT("EVENTENVIRONMENT"),
    EVENTLOGICALHOST("EVENTLOGICALHOST"),
    EVENTSERVER("EVENTSERVER"),         
    EVENTCOMPONENTPROJECTPATH("EVENTCOMPONENTPROJECTPATH"),
    EVENTDEPLOYMENT("EVENTDEPLOYMENT"),     
    EVENTCOMPONENTNAME("EVENTCOMPONENTNAME"),
    EVENTSEVERITY("EVENTSEVERITY"),       
    EVENTTYPE("EVENTTYPE"),           
    EVENTSTATUS("EVENTSTATUS"),         
    EVENTSTATE("EVENTSTATE"),          
    EVENTPHYSICALHOST("EVENTPHYSICALHOST"),   
    EVENTMESSAGECODE("EVENTMESSAGECODE"),    
    EVENTMESSAGEDETAILS("EVENTMESSAGEDETAILS"), 
    EVENTSERVERTYPE("EVENTSERVERTYPE"),      
    EVENTCOMPONENTTYPE("EVENTCOMPONENTTYPE"),
    DBEVENTID("DBEVENTID");  
   
    private String ElementName;
    
    private CompositeDataSummaryElementType(String filterElementName) {
        this.ElementName = filterElementName;
    }

    public String getDescription() {
        switch (this) {
            case EVENTDATE:
                 return "The timestamp when the event was genereted";
            case EVENTID:
                return  "Internal unique ID";
            case EVENTENVIRONMENT:
                return  "The environment the event generated in";
            case EVENTLOGICALHOST:
                return  "The logical host  the event generated in";
            case EVENTSERVER:
                return  "The server the event generated in";
            case EVENTCOMPONENTPROJECTPATH:
                return  "The component path"; 
            case EVENTDEPLOYMENT:
                return  "The deployemnt";
            case EVENTCOMPONENTNAME:
                return  "The component name";
            case EVENTSEVERITY:
                return  "The severity of the event";
            case EVENTTYPE:
                return  "The type of the event"; 
            case EVENTSTATUS:
                return  "The status of the event";
            case EVENTSTATE:
                return  "The component generating this event state ";
            case EVENTPHYSICALHOST:
                return  "The name of the server the event generated in";
            case EVENTMESSAGECODE:
                return  "The message code assoc. with the event";
            case EVENTMESSAGEDETAILS:
                return  "The details of the event message "; 
            case EVENTSERVERTYPE:
                return "The server type"; 
            case EVENTCOMPONENTTYPE:
                return "The component type"; 
            case DBEVENTID:    
                return "The unique id associated with this event when peristed in the database"; 
        }
        return "";
    }

    
    
    public String getCompositeDataElement() {
        return ElementName;
    }

}
