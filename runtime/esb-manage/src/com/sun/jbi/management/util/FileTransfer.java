/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileTransfer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  FileTransfer.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 2, 2006, 4:49 PM
 */

package com.sun.jbi.management.util;

import com.sun.jbi.management.MBeanNames;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

/**
 * FileTransfer is a utility class for uploading files to a remote JBI instance.
 * This class uses the ArchiveUpload MBean registered by an instance to upload files
 * to the remote instance.
 * 
 * @author Sun Microsystems, Inc
 */
public class FileTransfer
{
    private        String                mInstanceName;
    private        MBeanNames            mMBeanNames;
    private        MBeanServerConnection mMBnSvr;
    private        ObjectName            mUploadMBeanName;
    
    public FileTransfer(String instanceName, MBeanNames mbeanNames,
        MBeanServerConnection mbns)
    {
        mInstanceName = instanceName;
		mMBeanNames = mbeanNames;
        mMBnSvr = mbns;
    }
    
    /**
     * Upload the file using the Upload MBean
     */
    public String uploadFile(String srcPath)
        throws Exception
    {
        int count;
        byte[] buf                   = new byte[8042];
        java.io.File srcFile         = new java.io.File(srcPath);
        java.io.FileInputStream fis  = new java.io.FileInputStream(srcFile);
        
        Object upId = initiateUpload(srcFile.getName());
        
        while ((count = fis.read(buf)) != -1)
        {
            if (buf.length != count)
            {
                byte[] tmp = new byte[count];
                System.arraycopy(buf, 0, tmp, 0, count);
                buf = tmp;
            }
            uploadBytes(upId, buf);
        }
        terminateUpload(upId, srcFile.lastModified()); 

        if ( fis != null )
        {
            try
            {
                fis.close();
            }
            catch ( java.io.IOException ioex )
            {
                ;
            }
        }
        return getArchiveURL(upId);
    }
    
    /**
     * Get the object name of the Upload MBean
     */
    private ObjectName getUploadMBeanName()
        throws Exception
    {
        if (mUploadMBeanName == null)
        {
            mUploadMBeanName = mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.FileTransferService, 
                MBeanNames.ServiceType.Upload, mInstanceName);         
        }
        return mUploadMBeanName;
    }
    
    /**
     * Initiate the upload of the specified file.
     */
    private Object initiateUpload(String fileName)
        throws Exception
    {
        Object[] params = new Object[]{fileName};
        String[] sign = new String[]{"java.lang.String"};
        
        return mMBnSvr.invoke(getUploadMBeanName(), "initiateUpload", params, sign);
    }
    
    /**
     * Upload Bytes.
     */
    private void uploadBytes(Object upId, byte[] buf)
        throws Exception
    {
        Object[] params = new Object[]{upId, buf};
        String[] sign = new String[]{"java.lang.Object", "[B"};
        
        mMBnSvr.invoke(getUploadMBeanName(), "uploadBytes", params, sign);
        
    }

    /**
     * Terminate the Upload.
     */
    private void terminateUpload(Object upId, long timestamp)
        throws Exception
    {
        Object[] params = new Object[]{upId, Long.valueOf(timestamp)};
        String[] sign = new String[]{"java.lang.Object", "java.lang.Long"};
        
        mMBnSvr.invoke(getUploadMBeanName(), "terminateUpload", params, sign);
    }

    /**
     * Get the URL to the file on the remote instance.
     */
    private String getArchiveURL(Object upId)
        throws Exception
    {
        Object[] params = new Object[]{upId};
        String[] sign = new String[]{"java.lang.Object"};
        
        return (String) 
            mMBnSvr.invoke(getUploadMBeanName(), "getArchiveURL", params, sign);
    }
}
