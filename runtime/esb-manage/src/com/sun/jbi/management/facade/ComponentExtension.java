/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentExtension.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.system.ManagementException;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.management.ObjectName;

/**
 * This ComponentExtensionMBean is a facade MBean which provides component specific 
 * information such as the ObjectNames of the custom configuration MBeans registered
 * by the component.
 *
 * This ComponentExtension MBean is registered for each installation of the component
 * on a target i.e it is target specific. 
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentExtension
    extends Facade
    implements com.sun.jbi.management.ComponentExtensionMBean
{
    
    private String                     mComponentName;
    private ComponentType              mComponentType;
        
    public ComponentExtension(EnvironmentContext ctx, String target, 
        String compName, ComponentType compType)
        throws ManagementException
    {
        super(ctx, target);
        
        mComponentName                  = compName;
        mComponentType                  = compType;
    }

    /**
     * Get the ObjectName of the Component Configuration facade MBean for a target.
     *
     * @param target - target name 
     * @return the Component Configuration MBean name.
     */
    public ObjectName getComponentConfigurationFacadeMBeanName(String target)
        throws javax.jbi.JBIException
    {
        ObjectName facadeCompCfgMBean = null;
        if ( mComponentType == ComponentType.BINDING )
       {
            facadeCompCfgMBean = mMBeanNames.getBindingMBeanName(
                mComponentName, 
                MBeanNames.ComponentServiceType.Configuration, 
                target);
       }
       else
       {
            facadeCompCfgMBean = mMBeanNames.getEngineMBeanName(
                mComponentName, 
                MBeanNames.ComponentServiceType.Configuration, 
                target);
       }
       return facadeCompCfgMBean;
    }

    /**
     * Get the ObjectNames of all the custom MBeans registered by the component on the
     * target. If the target is :
     * <ul>
     *  <li>domain     - the return map has all the instances the component is installed on,
     *                   the corresponding value is the ObjectName(s) of the cutom MBeans
     *                   registered on the instance. </li>
     *  <li>server     - the return map has a single instance "server",
     *                   the corresponding value is the ObjectName(s) of the cutom MBeans 
     *                   registered on the instance. </li>
     *  <li>"instance" - the return map has a single instance "instance",
     *                   the corresponding value is the ObjectName(s) of the cutom MBeans 
     *                   registered on the instance. </li>
     *  <li>"cluster"  - the return map has all the member instances in the cluster,
     *                   the corresponding value is the ObjectName(s) of the cutom MBeans 
     *                   registered on the instance.  </li>
     *  </ul>
     *
     * @param  customName - custom control name
     * @return a Map of the MBeanNames, each map entry is keyed by the instance
     *         name and the value is an array of ObjectNames of custom MBeans registered
     *         by the component on the instance. Only those MBeans whose ObjectNames 
     *         have been generated based on MBeanNames.createCustomComponentMBeanName()
     *         will be present in the array. If the component does not have any registered
     *         MBeans the ObjectName array is empty.
     * @exception javax.jbi.JBIException if some other failure occurs.
     * @see javax.jbi.management.MBeanNames#createCustomComponentMBeanName()
     */
    public Map<String, ObjectName[]> getCustomMBeanNames(String customName) 
        throws javax.jbi.JBIException
    {
        if ( !mTarget.equals(DOMAIN))
        {
            return getCustomMBeanNamesOnTarget(customName);
        }
        else
        {
            return getCustomMBeanNamesOnAllTargets(customName);
        }
    }
    
    
    /**
     * Get the ObjectNames of all the Logger MBeans registered by the component on the
     * target. If the target is :
     * <ul>
     *  <li>domain     - the return map has all the instances the component is installed on,
     *                   the corresponding value is the ObjectName(s) of the Logger MBeans
     *                   registered on the instance. </li>
     *  <li>server     - the return map has a single instance "server",
     *                   the corresponding value is the ObjectName(s) of the Logger MBeans 
     *                   registered on the instance. </li>
     *  <li>"instance" - the return map has a single instance "instance",
     *                   the corresponding value is the ObjectName(s) of the Logger MBeans 
     *                   registered on the instance. </li>
     *  <li>"cluster"  - the return map has all the member instances in the cluster,
     *                   the corresponding value is the ObjectName(s) of the Logger MBeans 
     *                   registered on the instance.  </li>
     *  </ul>
     *
     * @return a Map of the MBeanNames, each map entry is keyed by the instance
     *         name and the value is an array of ObjectNames of Logger MBeans registered
     *         by the component on the instance. Only those MBeans whose ObjectNames 
     *         have been generated based on MBeanNames.createCustomComponentMBeanName()
     *         will be present in the array. If the component does not have any registered
     *         MBeans the ObjectName array is empty.
     * @exception javax.jbi.JBIException if some other failure occurs.
     */
    public Map<String, ObjectName[]> getLoggerMBeanNames() throws javax.jbi.JBIException
    {
        if ( !mTarget.equals(DOMAIN))
        {
            return getLoggerMBeanNamesOnTarget();
        }
        else
        {
            return getLoggerMBeanNamesOnAllTargets();
        }
    }
    
    /*----------------------------------------------------------------------------------*\
     *                              Private Helpers                                     *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Get custom MBeans registered by the component on the target.
     */
    private Map<String, ObjectName[]> getCustomMBeanNamesOnTarget(String customName)
        throws javax.jbi.JBIException
    {
        HashMap<String, ObjectName[]> objNames = new HashMap();
        mLog.finer(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_GETTING_CUSTOM_MBEANS_FOR_COMPONENT , 
            mComponentName, mTarget));

        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            objNames.put(mTarget, getCustomMBeanNamesOnInstance(mTarget, customName));
        }
        else
        {
            Set<String> instances = mPlatform.getServersInCluster(mTarget);
            for ( String instance : instances )
            {
                objNames.put(instance, getCustomMBeanNamesOnInstance(instance,customName));
            }
        }
        return objNames;
    }
    
    /**
     * This operation gets the custom MBeans registered on all instances by the installed
     * component.
     *
     * @param  customName - custom control name 
     * @return a map of instance name and corresponding custom Mbean names for this 
     *         component in the domain ( i.e. all targets ).
     */
    private Map<String, ObjectName[]> getCustomMBeanNamesOnAllTargets(String customName)
        throws javax.jbi.JBIException
    {
        List<String> targets = null;
        
        try
        {
            targets = getAllTargets();
        }
        catch(com.sun.jbi.management.system.ManagementException mex)
        {
            throw new javax.jbi.JBIException(mex.getMessage());            
        }  
        
        Map<String, ObjectName[]> objNames = new HashMap();
        for( String target : targets )
       {
           try
           {
               ObjectName facadeCompExtMBean = null;
               if ( mComponentType == ComponentType.BINDING )
               {
                    facadeCompExtMBean = mMBeanNames.getBindingMBeanName(
                        mComponentName, 
                        MBeanNames.ComponentServiceType.Extension, 
                        target);
               }
               else
               {
                    facadeCompExtMBean = mMBeanNames.getEngineMBeanName(
                        mComponentName, 
                        MBeanNames.ComponentServiceType.Extension, 
                        target);
               }
               
               Map<String, ObjectName[]> targetObjNames = (Map<String, ObjectName[]>) 
                mMBeanSvr.invoke(facadeCompExtMBean, "getCustomMBeanNames",
                    new Object[]{customName}, new String[]{"java.lang.String"});
               objNames.putAll(targetObjNames);
           }
           catch ( Exception ex )
           {
               continue;
           }
       }
       
       
       mLog.finer("The custom MBeans for component  " + mComponentName + " on targets " 
                + targets.toString() + " are " + objNames.toString());
       return objNames;
    }
    
    /**
     * @param instanceName - target instance name
     * @param customName   - target custom control name
     * @return an array of ObjectNames of the Custom MBeans registered by this component
     * on an instance
     * @throws javax.jbi.JBIException on errors
     */
    private ObjectName[] getCustomMBeanNamesOnInstance(String instanceName, String customName)
    {
        // Custom MBeanName filter is : 
        // com.sun.jbi:JbiName=instanceName,CustomControlName=customName,ComponentName=mComponentName,ControlType=Custom,*
        ObjectName customMBeanFilter = 
            mMBeanNames.getCustomComponentMBeanNameFilter(instanceName, customName, mComponentName);
        java.util.Set<ObjectName> nameSet = mMBeanSvr.queryNames(customMBeanFilter, null);
        
        return nameSet.toArray(new ObjectName[nameSet.size()]);
    }
    
    /**
     * Get custom MBeans registered by the component on the target.
     */
    private Map<String, ObjectName[]> getLoggerMBeanNamesOnTarget()
        throws javax.jbi.JBIException
    {
        HashMap<String, ObjectName[]> objNames = new HashMap();

        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            objNames.put(mTarget, getLoggerMBeanNamesOnInstance(mTarget));
        }
        else
        {
            Set<String> instances = mPlatform.getServersInCluster(mTarget);
            for ( String instance : instances )
            {
                objNames.put(instance, getLoggerMBeanNamesOnInstance(instance));
            }
        }
        return objNames;
    }
    
    /**
     * This operation gets the Logger MBeans registered on all instances by the installed
     * component.
     *
     * @return a map of instance name and corresponding Logger Mbean names for this 
     *         component in the domain ( i.e. all targets ).
     */
    private Map<String, ObjectName[]> getLoggerMBeanNamesOnAllTargets()
        throws javax.jbi.JBIException
    {
        List<String> targets = null;
        
        try
        {
            targets = getAllTargets();
        }
        catch(com.sun.jbi.management.system.ManagementException mex)
        {
            throw new javax.jbi.JBIException(mex.getMessage());            
        }  
        
        Map<String, ObjectName[]> objNames = new HashMap();
        for( String target : targets )
       {
           try
           {
               ObjectName facadeCompExtMBean = null;
               if ( mComponentType == ComponentType.BINDING )
               {
                    facadeCompExtMBean = mMBeanNames.getBindingMBeanName(
                        mComponentName, 
                        MBeanNames.ComponentServiceType.Extension, 
                        target);
               }
               else
               {
                    facadeCompExtMBean = mMBeanNames.getEngineMBeanName(
                        mComponentName, 
                        MBeanNames.ComponentServiceType.Extension, 
                        target);
               }
               
               Map<String, ObjectName[]> targetObjNames = (Map<String, ObjectName[]>) 
               mMBeanSvr.invoke(facadeCompExtMBean, "getLoggerMBeanNames",
                     new Object[]{}, new String[]{});
               objNames.putAll(targetObjNames);
           }
           catch ( Exception ex )
           {
               continue;
           }
       }
       
       
       mLog.finer("The Logger MBeans for component  " + mComponentName + " on targets " 
                + targets.toString() + " are " + objNames.toString());
       return objNames;
    }
    
    /**
     * @param instanceName - target instance name
     * @return an array of ObjectNames of the Logger MBeans registered by this component
     * on an instance
     */
    private ObjectName[] getLoggerMBeanNamesOnInstance(String instanceName)
    {
        // Logger MBeanName filter is : 
        // com.sun.jbi:JbiName=instanceName,ComponentName=mComponentName,ControlType=Logger,*
        ObjectName loggerMBeanFilter = 
            mMBeanNames.getComponentLoggerMBeanNameFilter(instanceName, mComponentName);
        java.util.Set<ObjectName> nameSet = mMBeanSvr.queryNames(loggerMBeanFilter, null);
        
        return nameSet.toArray(new ObjectName[nameSet.size()]);
    }
}
