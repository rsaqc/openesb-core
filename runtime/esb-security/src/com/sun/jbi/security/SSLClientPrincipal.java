/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SSLClientPrincipal.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SSLClientPrincipal.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 11, 2005, 3:12 PM
 */

package com.sun.jbi.security;

import javax.security.auth.x500.X500Principal;

/**
 * This is a Principal class created soley to add a SSLClientPrincipal Type.
 * The name of this Principal is the Subject DN from the SSL Clients Public Key 
 * Certificate, i.e. the certificate used for SSL Client Authentication.
 *
 * This is a wrapper around X500 Principal.
 *
 * @author Sun Microsystems, Inc.
 */
public class SSLClientPrincipal 
    extends X500PrincipalWrapper
    implements java.security.Principal,
        java.io.Serializable
{
    
    /**
     *
     * Creates a new instance of SSLClientPrincipal.
     *
     * @param name an X.500 distinguished name in RFC 1779 or RFC 2253 format.
     * @see javax.security.auth.x500.X500Principal
     */
    public SSLClientPrincipal (String name)
    {
        super(name);
    }
    
    /**
     * Creates a new instance of SSLClientPrincipal.
     *
     * @param name a byte array containing the distinguished name in ASN.1 DER
     * encoded form.
     * @see javax.security.auth.x500.X500Principal
     */
    public SSLClientPrincipal (byte[] name)
    {
        super(name);
    }
    
    /**
     * Creates a new instance of SSLClientPrincipal.
     *
     * @param fis an InputStream containing the distinguished name in ASN.1 DER
     * encoded form.
     *
     * @see javax.security.auth.x500.X500Principal
     */
    public SSLClientPrincipal (java.io.FileInputStream fis)
    {
        super(fis);
    }
    
    /**
     * Creates a new instance of SSLClientPrincipal.
     *
     * @param x500Principal is the X500Principal this class
     * encapsulates.
     *
     * @see javax.security.auth.x500.X500Principal
     */
    public SSLClientPrincipal (X500Principal x500Principal)
    {
        super(x500Principal);
    }    
    
    /**
     * Compares the specified Object with this X500Principal for equality.
     * Specifically, this method returns true if the Object o is an X500Principal
     * and if the respective canonical string representations (obtained via the
     * getName(X500Principal.CANONICAL) method) of this object and o are equal.
     *
     * @param o obj to compare with
     * @return true if the specified Object is equal to this X500Principal,
     * false otherwise
     */
    public boolean equals (Object o)
    {
        if ( o == null )
        {
            return false;
        }
        
        if ( o instanceof SSLClientPrincipal)
        {
            return getName (X500Principal.CANONICAL).equals (
                ( (SSLClientPrincipal) o).getName (X500Principal.CANONICAL));
        }
        
        return false;
    }
    
    /**
     * Return a hash code for this X500Principal.
     *
     * The hash code is calculated via: getName(X500Principal.CANONICAL).hashCode()
     *
     * @return a hash code for this X500Principal.
     */
    public int hashCode ()
    {
        return super.hashCode();
    }
    
}
