/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityInstallConfigWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityInstallConfigWriter.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 16, 2004, 2:53 PM
 */

package com.sun.jbi.internal.security.config;

import java.io.File;
import java.io.OutputStream;
import java.io.Writer;

import javax.jbi.JBIException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;


/**
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityInstallConfigWriter
{
    
    /** Creates a new instance of SecurityInstallConfigWriter. */
    public SecurityInstallConfigWriter ()
    {
    }
    
    /**
     * Write the contents of the Security Configuration out to a file.
     *
     * @param config is the source security configuration data.
     * @param file is the file to write the output to.
     * @throws JBIException on errors.
     */
    public static void write(SecurityConfiguration config, File file)
        throws JBIException
    {
        try
        {
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
            xformer.transform( new DOMSource(config.generateDocument()), 
                new StreamResult(file) );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }
    }
    
    
    /**
     * Write the contents of the Security Configuration out to a outputstream.
     *
     * @param config is the source security configuration data.
     * @param ostr is the OutputStream to write the contents out to.
     * @throws JBIException on errors.
     * 
     */
    public static void write(SecurityConfiguration config, OutputStream ostr)
        throws JBIException
    {
        try
        {
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
            xformer.transform( new DOMSource(config.generateDocument()), 
                new StreamResult(ostr) );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }
    }
    
    /**
     * Write the contents of the Security Configuration out to a String.
     *
     * @param config is the source security configuration data.
     * @param writer is the output Writer to write the contents out to.
     * @throws JBIException on errors.
     */
    public static void write(SecurityConfiguration config, Writer writer)
        throws JBIException
    {
        try
        {
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
            xformer.transform( new DOMSource(config.generateDocument()), 
                new StreamResult(writer) );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }
    
    }
}
