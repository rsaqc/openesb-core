/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)KeyStoreManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  KeyStoreManager.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 24, 2004, 12:35 AM
 */

package com.sun.jbi.internal.security;

import java.security.cert.CertStore;
import java.security.KeyStore;
import java.util.Properties;
import javax.security.auth.callback.CallbackHandler;

/**
 * A basic KeyStoreManager interface.
 * @author Sun Microsystems, Inc.
 */
public interface KeyStoreManager
    extends CallbackHandler
{
    
    /**
     * Manager type: Java Standard.
     */
    String JAVA_STD = "JavaStandard";
    
    /**
     * Manager type: App Serv Env.
     */
    String SJSAS = "SJSAS";
    
    /**
     * Get the KeyStoreManager Name.
     * @return the name of the KeyStoreManager
     */
    String getName();
    
    /**
     * Set the KeyStoreManager Name.
     * @param name is the name of the KeyStoreManager
     */
    void setName(String name);
    
    /**
     * Get the initialized KeyStore instance.
     * @return a KeyStore instance. This KeyStore has the Private Keys and corresponding
     * Key Certificates.
     */
    KeyStore getKeyStore();
    
    /**
     * Get the initialized TrustStore instance.
     * @return a TrustStore instance. This KeyStore has the Trusted CA Certificates.
     */
    KeyStore getTrustStore();
    
    /**
     * Get the Certificate Store.
     *
     * @return a CertificateStore instance, this store has the client certificates which
     * will be used in verifying a certification path.
     */
    CertStore getCertStore();
    
    /**
     * Get the the password required for accessing the KeyStore.
     * @return the password required for accessing the KeyStore
     */
    String getKeyStorePassword();
    
    /**
     * Get the Type String. 
     * In Shasta 1.0 we support "JavaStandard" which encompasses : JKS, PKCS12, JCEKS
     * and "SJSAS" which implies the Application Server environment.
     *
     * @return manager type
     */
    String getType();
    
    /**
     * Set the type string.
     *
     * @param type is the type.
     */
    void setType(String type);
    
    /**
     * Initialize this KeyStore Service.
     * @param initProperties are the Properties specified for the Service
     * @throws IllegalStateException if the state of the instance is invalid 
     * after initialization.
     */
    void initialize(Properties initProperties)
        throws IllegalStateException;
    
    /**
     * Reload / Synchronize the Key / Trust Stores.
     * @throws IllegalStateException If the state of the instance is invalid 
     * after synchronization.
     */
    void reload()
        throws IllegalStateException;

    
}
