/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SSLClientKeyManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SSLClientKeyManager.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 20, 2004, 10:36 PM
 */

package com.sun.jbi.internal.security.https;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.internal.security.KeyStoreManager;
import com.sun.enterprise.security.jauth.callback.PrivateKeyCallback;

import java.security.Principal;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.security.auth.callback.Callback;
import javax.net.ssl.X509KeyManager;
import java.util.logging.Logger;
import java.util.Vector;

/**
 * This is an implementation of the X509KeyManger, an instance of which is
 * initialized for a particular alias and can be passed to 
 * the SSLContext.init() method. This is done when the SSL configuration
 * specifies that a particular client alias is to be used for Client Authentication.
 *
 * @author Sun Microsystems, Inc.
 */
public class SSLClientKeyManager 
    implements X509KeyManager
{
    
    /** The alias. */
    private String mAlias;  
    
    /** The Public Key Type. */
    private String mKeyType;
    
    /** The Name of the issuer. */
    private Principal mIssuer;
    
    /** The Logger. */
    private Logger mLogger;
    
    /** The String Translator. */
    private StringTranslator mTranslator;
    
    /** The Private Key. */
    private PrivateKey mPrivateKey;
    
    /** The Public Key Cert Chain. */
    private Certificate[] mCertChain;
    
    /** The Public Key Certificate. */
    private X509Certificate mCert;
  
    /** 
     * Creates a new instance of SSLClientKeyManager. 
     *
     * @param ksMgr is KeyStoreManager, which is the source of
     * the private Key and Certificate for the alias.
     * @param translator is the StringTranslator
     * @param alias is the alias
     * @throws KeyStoreException if there are problems in getting the 
     * Client Key/Certificate
     */
    public SSLClientKeyManager (KeyStoreManager ksMgr, String alias,
        StringTranslator translator)
        throws KeyStoreException
    {
        mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
        mTranslator = translator;
        mAlias = alias;
        
        getPrivateKeyCertPair( alias, ksMgr);    
        mKeyType = mCert.getPublicKey().getAlgorithm();
        mIssuer = mCert.getIssuerX500Principal(); 
        

    }
    
    /**
     *
     * @param str the key algorithm type name(s), ordered with the most-preferred 
     * key type first.
     * @param principal issuers the list of acceptable CA issuer subject names or null 
     * if it does not matter which issuers are used.
     * @param socket the socket to be used for this connection. This parameter can be 
     * null, in which case this method will return the most generic alias to use. 
     * @return the alias name for the desired key, or null if there are no matches.
     */

    public String chooseClientAlias (String[] str, java.security.Principal[] principal, 
        java.net.Socket socket)
    {
        for ( int s = 0; s < str.length; s++ )
        {
            if ( mKeyType.equals(str[s]) )
            {
                if ( principal == null )
                {
                    return mAlias;
                }
                for ( int p = 0; p < principal.length; p++ )
                {
                    if ( mIssuer.equals(principal[p]) )
                    {
                        return mAlias;
                    }
                }
            }
        }
        return null;
    }    
    
    /**
     * This method should never be called, as we can only control the client side 
     * of the SSL connection since the server connections are managed by the HTTP
     * Listener.
     *
     * @param keyType the key algorithm type name(s), ordered with the most-preferred 
     * key type first.
     * @param principal issuers the list of acceptable CA issuer subject names or null 
     * if it does not matter which issuers are used.
     * @param socket the socket to be used for this connection. This parameter can be 
     * null, in which case this method will return the most generic alias to use. 
     * @return the alias name for the desired key, or null if there are no matches.
     */
    public String chooseServerAlias (String keyType, java.security.Principal[] principal, 
        java.net.Socket socket)
    {
        String[] keyTypes = {keyType};
        return chooseClientAlias(keyTypes, principal, socket);
    }    
    
    /**
     *
     * @param alias is the Alias name
     * @return the certificate chain associated with the given alias. 
     */
    public X509Certificate[] getCertificateChain (String alias)
    {
        Vector x509Certs = new Vector();
        if ( mCertChain != null )
        {
            for ( int c = 0; c < mCertChain.length; c++ )
            {
                x509Certs.add(mCertChain[c]);
            }

            return ( (X509Certificate[]) x509Certs.toArray(new X509Certificate[1]));      
        }
        return null;
    }
    
    /**
     * Get the matching aliases for authenticating the client side of a secure socket 
     * given the public key type and the list of certificate issuer authorities
     * recognized by the peer (if any). 
     *
     * @param keyType is the key algorithm type name
     * @param principal is the list of acceptable CA issuer subject names, or null if 
     * it does not matter which issuers are used. 
     * @return an array of the matching alias names, or null if there were no matches.
     */
    public String[] getClientAliases (String keyType, java.security.Principal[] principal)
    {
        String[] keyTypes = {keyType};
        String alias = chooseClientAlias(keyTypes, principal, null);
        if ( alias == null )
        {
            return new String[0];
        }
        else
        {
            String[] aliases = new String[1];
            aliases[0] = alias;
            return aliases;  
        }
    }
    
    /**
     *
     * @param alias is the alias name.
     * @return the key associated with the given alias.
     */
    public PrivateKey getPrivateKey (String alias)
    {
        try
        {
            return mPrivateKey;
        }
        catch (Exception ksex)
        {
            mLogger.severe(mTranslator.getString(
                HttpConstants.BC_ERR_GET_PRIVATE_KEY_FAILED, mAlias, ksex.toString()));
            return null;
        }
        
    }
    
    /**
     * Get the matching aliases for authenticating the server side of a secure socket 
     * given the public key type and the list of certificate issuer authorities
     * recognized by the peer (if any). 
     *
     * @param keyType is the key algorithm type name
     * @param principal is the list of acceptable CA issuer subject names, or null if 
     * it does not matter which issuers are used. 
     * @return an array of the matching alias names, or null if there were no matches.
     */
    public String[] getServerAliases (String keyType, java.security.Principal[] principal)
    {
        return getClientAliases(keyType, principal);
    } 
    
    /**
     * Get the Private Key and Cert. Chain for the alias
     *
     * @param alias is the alias for whom the Private Key / Certificate pair 
     * is to be retrieved.
     * @param ksMgr the KeyStoreManager which has the Private Key / Public Key Cert info.
     * @throws KeyStoreException if the Private Key / Certificate pair information
     * cannot be retrieved.
     */
    private void getPrivateKeyCertPair(String alias, KeyStoreManager ksMgr)
        throws KeyStoreException 
    {
        PrivateKeyCallback pkCB = 
            new PrivateKeyCallback(new PrivateKeyCallback.AliasRequest(alias));
        try
        {
            ksMgr.handle( new Callback[]{pkCB});
        }
        catch (Exception ex)
        {
            throw new KeyStoreException( ex.toString() );
        }
        
        mPrivateKey = pkCB.getKey();
        mCertChain = pkCB.getChain();
        if ( mCertChain != null )
        {
            mCert = (X509Certificate) mCertChain[0];
        }
        
        if ( mPrivateKey == null || mCertChain == null )
        {
            String errMsg = mTranslator.getString(
                HttpConstants.BC_ERR_MISSING_PRIVATE_KEY, ksMgr.getName(),
                        ksMgr.getType(), mAlias);
            mLogger.severe(errMsg);
            throw new KeyStoreException(errMsg);
        }
            
    }
    
}
