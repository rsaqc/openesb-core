/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AppSrvAuthenticator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Authenticator.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 25, 2005, 10:29 PM
 */

package com.sun.jbi.internal.security.auth;

import com.sun.jbi.internal.security.UserDomain;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * This class enforces user authentication.
 *
 * @author Sun Microsystems, Inc.
 */
public class AppSrvAuthenticator implements Authenticator
{
    /** The User Domain. */
    private UserDomain mDomain;
    
    /** The App Srv Callback Handler. */
    private CallbackHandler mAppSrvCBHandler;
    
    /**
     * Initialize the authenticator with the UserDomain.
     *
     * @param domain is the UserDomain which has the Authentication Context.
     * @throws IllegalStateException if initialization fails.
     */
    public void initialize(UserDomain domain)
        throws IllegalStateException 
    {
        mDomain = domain;
        
        try
        {
            mAppSrvCBHandler = 
                com.sun.jbi.internal.security.AppSrvEnvironment.getCallbackHandler();
        }
        catch (Exception ex)
        {
            throw new IllegalStateException(ex.toString());
        }
    }
    
    /**
     * The implementation on the CallbackHandlerInterface. This class only supports 
     * PasswordValidationCallback
     * 
     *
     * @param callbacks - array of Callbacks to be handled.
     * @throws java.io.IOException - if an input or output error occurs.
     * @throws UnsupportedCallbackException - if the implementation of this method
     * does not support one or more of the Callbacks specified in the callbacks
     * parameter.
     */
    public void handle (Callback[] callbacks) 
        throws java.io.IOException, UnsupportedCallbackException
    {
        mAppSrvCBHandler.handle(callbacks);
    }
}
