/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DefaultHostnameVerifier.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  DefaultHostnameVerifier.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 20, 2004, 5:33 PM
 */

package com.sun.jbi.internal.security.https;

import com.sun.jbi.StringTranslator;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;
import javax.net.ssl.SSLSession;

/**
 * The default hostname verifier is throwing a java.io.IOException:
 * HTTPS hostname wrong:  should be [hostname] ( this was probably due 
 * to a failure to map the IP to the hostname.
 *              
 * @author Sun Microsystems, Inc.
 */
public class DefaultHostnameVerifier
    implements javax.net.ssl.HostnameVerifier
{
    /** The Logger */
    private Logger mLogger = null;
    
    /** The String Translator. */
    private StringTranslator mTranslator;
    
    /** 
     * Creates a new instance of DefaultHostnameVerifier. 
     *
     * @param translator is the StringTranslator
     */
    public DefaultHostnameVerifier(StringTranslator translator)
    {
        mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
        mTranslator = translator;
    }
    

    /**
     *
     * @param urlHostName is the Hostname from the Service URL
     * @param session is the SSL Session
     * @return true if verified.
     */            
    public boolean verify(String urlHostName, SSLSession session) 
    {     
        InetAddress[] urlHostAddresses = null;
        InetAddress[] peerHostAddresses = null;
        try
        {
            urlHostAddresses = InetAddress.getAllByName(urlHostName);
            peerHostAddresses = InetAddress.getAllByName(session.getPeerHost());
        }
        catch (UnknownHostException uhex)
        {
            // -- Log the exception
            mLogger.warning(uhex.toString()); 
            return false;
        }
        
        // -- Maybe there is a better way of doing this comparison,
        // -- but the list of ip addresses on a multihomed system should be small.
        
        for ( int i = 0; i < urlHostAddresses.length; i++)
        {
            // -- Compare each urlHostIP to each peerHostIP
            for ( int j = 0; j < peerHostAddresses.length; j++)
            {
                if ( urlHostAddresses[i].equals(peerHostAddresses[j]))
                {
                    return true;
                }

            }
        }
        mLogger.severe(mTranslator.getString( 
            HttpConstants.BC_ERR_HOSTNAME_VERIFICATION_FAILED,  
            urlHostName, session.getPeerHost()));  
        return false;    
    }
            
    
}
