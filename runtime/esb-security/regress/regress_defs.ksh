#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#Definitions required by the Security regression tests:

my_test_domain=domain1

#use global regress setup:
. $SRCROOT/antbld/regress/common_defs.ksh

#Port for HTTP Listener 1
JBITest_HTTP_LISTENER_1=$DOMAIN_HTTP_PORT

#Port for HTTP Listener 2
JBITest_HTTP_LISTENER_2=$DOMAIN_HTTP_PORT2

#Port for HTTP Listener 3
JBITest_HTTP_LISTENER_3=$DOMAIN_HTTP_PORT3

#JBI build Library directory
JBILIB=$SRCROOT/antbld/lib

# The localhost name
#HOSTNAME=`hostname`
HOSTNAME=${HOST_NAME}

# Security Regress Dirs
export SEC_BLD_DIR SEC_BLD_REGRESS_DIR SEC_REGRESS_DIR TEST_SVLT_WAR
SEC_BLD_DIR=$JV_SVC_BLD
SEC_BLD_REGRESS_DIR=$JV_SVC_TEST_CLASSES
SEC_REGRESS_DIR=$JV_SVC_REGRESS
TEST_SVLT_WAR="TestSecurityServlet.war"

# Server.policy backup and append files
SERVER_POLICY_BAK=server.policy.bak
SERVER_POLICY_APPEND=server.policy.append

# Login Conf file
LOGIN_CONF_BAK=login.conf.bak
LOGIN_CONF_APPEND=login.conf.append

# User data file required by File Logi Module
USER_FILE=JbiUsers.xml

#in order to use the certutil reliably, we have to set
#the PATH and LD_LIBRARY_PATH to the appserver lib, where the shlib's
#live.  RT 3/31/05
export LD_LIBRARY_PATH PATH
LD_LIBRARY_PATH=$AS8BASE/lib
PATH="$AS8BASE/lib${PS}$PATH"
