/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestThreadLocalContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  TestThreadLocalContext.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 9, 2005, 5:03 PM
 */

package com.sun.jbi.internal.security;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class TestThreadLocalContext
    extends junit.framework.TestCase
{
    
    public TestThreadLocalContext (String name)
    {
        super(name);
    }
    
    /**
     * Test the setting of a Subject in the TLS,
     * getting the same Subject and modifying it.
     */
    public static void testSetGetSubject()
    {
        int threadCount = 4;
        java.util.Vector threads = new java.util.Vector();
        // -- Create the Threads
        for ( int i = 0; i < threadCount; i++ )
        {
            threads.add(new SampleThread()); 
        }
        
        
        for ( int j = 0; j < threadCount; j++ )
        {
            ( (Thread) threads.get(j)).start();
        }
        
    }
    
}
