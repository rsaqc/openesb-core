/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMessagingStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

/**
 * Tests for MessagingStatistics.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestMessagingStatistics
    extends junit.framework.TestCase
{
    /**
     * Instance of MessagingStatistics.
     */
    private MessagingStatistics mStats;

    /**
     * Constant for tests.
     */
    private static final long ONE_HUNDRED = 100;

    /**
     * Constant for tests.
     */
    private static final long ONE_THOUSAND = 1000;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestMessagingStatistics(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the MessagingStatistics instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

        mStats = new MessagingStatistics();
        mStats.setLastRestartTime(new java.util.Date());
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Tests get/setLastRestartTime.
     * @throws Exception if an unexpected error occurs.
     */
    public void testLastRestartTime()
    {
        java.util.Date d = new java.util.Date();
        mStats.setLastRestartTime(d);
        assertEquals("Failure on set/getLastRestartTime: ",
            d, mStats.getLastRestartTime());
    }

    /**
     * Tests get/increment/decrementActiveExchanges.
     * @throws Exception if an unexpected error occurs.
     */
    public void testActiveExchanges()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getActiveExchanges: ",
            new Integer(n),
            mStats.getActiveExchanges());
        mStats.incrementActiveExchanges();
        ++n;
        assertEquals("Failure on incrementActiveExchanges: ",
            new Integer(n),
            mStats.getActiveExchanges());
        mStats.decrementActiveExchanges();
        --n;
        assertEquals("Failure on decrementActiveExchanges: ",
            new Integer(n),
            mStats.getActiveExchanges());
    }

    /**
     * Tests getActiveExchangeRate.
     * @throws Exception if an unexpected error occurs.
     */
    public void testActiveExchangeRate()
        throws Exception
    {
        // TODO: Set a fixed value here?
        java.util.Date d = new java.util.Date();
        mStats.setLastRestartTime(d);
        double n = 0;
        assertEquals("Failure on getActiveExchangeRate: ",
            new Double(n),
            mStats.getActiveExchangeRate());
    }

    /**
     * Tests get/incrementInOnlyExchanges.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInOnlyExchanges()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getInOnlyExchanges: ",
            new Long(n),
            mStats.getInOnlyExchanges());
        mStats.incrementInOnlyExchanges();
        ++n;
        assertEquals("Failure on incrementInOnlyExchanges: ",
            new Long(n),
            mStats.getInOnlyExchanges());
    }

    /**
     * Tests get/incrementRobustInOnlyExchanges.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRobustInOnlyExchanges()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getRobustInOnlyExchanges: ",
            new Long(n),
            mStats.getRobustInOnlyExchanges());
        mStats.incrementRobustInOnlyExchanges();
        ++n;
        assertEquals("Failure on incrementRobustInOnlyExchanges: ",
            new Long(n),
            mStats.getRobustInOnlyExchanges());
    }

    /**
     * Tests get/incrementInOptionalOutExchanges.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInOptionalOutExchanges()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getInOptionalOutExchanges: ",
            new Long(n),
            mStats.getInOptionalOutExchanges());
        mStats.incrementInOptionalOutExchanges();
        ++n;
        assertEquals("Failure on incrementInOptionalOutExchanges: ",
            new Long(n),
            mStats.getInOptionalOutExchanges());
    }

    /**
     * Tests get/incrementInOutExchanges.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInOutExchanges()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getInOutExchanges: ",
            new Long(n),
            mStats.getInOnlyExchanges());
        mStats.incrementInOutExchanges();
        ++n;
        assertEquals("Failure on incrementInOutExchanges: ",
            new Long(n),
            mStats.getInOutExchanges());
    }

    /**
     * Tests getTotalExchanges.
     * @throws Exception if an unexpected error occurs.
     */
    public void testTotalExchanges()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getTotalExchanges: ",
            new Long(n),
            mStats.getTotalExchanges());
        mStats.incrementInOnlyExchanges();
        ++n;
        assertEquals("Failure on getTotalExchanges: ",
            new Long(n),
            mStats.getTotalExchanges());
        mStats.incrementInOutExchanges();
        ++n;
        assertEquals("Failure on getTotalExchanges: ",
            new Long(n),
            mStats.getTotalExchanges());
        mStats.incrementInOptionalOutExchanges();
        ++n;
        assertEquals("Failure on getTotalExchanges: ",
            new Long(n),
            mStats.getTotalExchanges());
        mStats.incrementRobustInOnlyExchanges();
        ++n;
        assertEquals("Failure on getTotalExchanges: ",
            new Long(n),
            mStats.getTotalExchanges());
    }

    /**
     * Tests get/incrementFailedExchanges.
     * @throws Exception if an unexpected error occurs.
     */
    public void testFailedExchanges()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getFailedExchanges: ",
            new Long(n),
            mStats.getFailedExchanges());
        mStats.incrementFailedExchanges();
        ++n;
        assertEquals("Failure on incrementFailedExchanges: ",
            new Long(n),
            mStats.getFailedExchanges());
    }

    /**
     * Tests get/incrementTransactedExchanges.
     * @throws Exception if an unexpected error occurs.
     */
    public void testTransactedExchanges()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getTransactedExchanges: ",
            new Long(n),
            mStats.getTransactedExchanges());
        mStats.incrementTransactedExchanges();
        ++n;
        assertEquals("Failure on incrementTransactedExchanges: ",
            new Long(n),
            mStats.getTransactedExchanges());
    }

    /**
     * Tests addTotalExchangeTimeToClose/getTotalExchangeTimeToClose/
     * getExchangeMeanTimeToClose.
     * @throws Exception if an unexpected error occurs.
     */
    public void testExchangeTimeToClose()
        throws Exception
    {
        long t = 0;
        assertEquals("Failure on getTotalExchangeTimeToClose: ",
            new Long(t),
            new Long(mStats.getTotalExchangeTimeToClose()));
        t = ONE_THOUSAND;
        mStats.addTotalExchangeTimeToClose(t);
        assertEquals("Failure on addTotalExchangeTimeToClose: ",
            new Long(t),
            new Long(mStats.getTotalExchangeTimeToClose()));
        assertEquals("Failure on getExchangeMeanTimeToClose: ",
            new Long(0),
            mStats.getExchangeMeanTimeToClose());
        int e = 0;
        mStats.incrementInOnlyExchanges();
        ++e;
        assertEquals("Failure on getExchangeMeanTimeToClose: ",
            new Long(t / e),
            mStats.getExchangeMeanTimeToClose());
        mStats.incrementInOptionalOutExchanges();
        ++e;
        assertEquals("Failure on getExchangeMeanTimeToClose: ",
            new Long(t / e),
            mStats.getExchangeMeanTimeToClose());
        mStats.incrementInOutExchanges();
        ++e;
        assertEquals("Failure on getExchangeMeanTimeToClose: ",
            new Long(t / e),
            mStats.getExchangeMeanTimeToClose());
        mStats.incrementRobustInOnlyExchanges();
        ++e;
        assertEquals("Failure on getExchangeMeanTimeToClose: ",
            new Long(t / e),
            mStats.getExchangeMeanTimeToClose());
    }

    /**
     * Tests getExchangeSuccessRate.
     * @throws Exception if an unexpected error occurs.
     */
    public void testExchangeSuccessRate()
        throws Exception
    {
        float percent = ONE_HUNDRED;
        assertEquals("Failure on getExchangeSuccessRate: ",
            new Float(percent),
            mStats.getExchangeSuccessRate());
        mStats.incrementInOnlyExchanges();
        assertEquals("Failure on getExchangeSuccessRate: ",
            new Float(percent),
            mStats.getExchangeSuccessRate());
        percent = 0;
        mStats.incrementFailedExchanges();
        assertEquals("Failure on getExchangeSuccessRate: ",
            new Float(percent),
            mStats.getExchangeSuccessRate());
    }

    /**
     * Tests toCompositeData.
     * @throws Exception if an unexpected error occurs.
     */
    public void testToCompositeData()
        throws Exception
    {
        assertTrue("Failure on toCompositeData: ",
            mStats.toCompositeData() instanceof
            javax.management.openmbean.CompositeData);
    }

    /**
     * Test resetStatistics.
     * @throws Exception if an unexpected error occurs.
     */
    public void testResetStatistics()
    {
        // First, populate all the fields with values. These methods are all
        // tested in other junit tests so they are assumed to work here.

        mStats.incrementActiveExchanges();
        mStats.incrementInOnlyExchanges();
        mStats.incrementInOptionalOutExchanges();
        mStats.incrementInOutExchanges();
        mStats.incrementRobustInOnlyExchanges();
        mStats.incrementFailedExchanges();
        mStats.incrementTransactedExchanges();
        mStats.addTotalExchangeTimeToClose(ONE_THOUSAND);

        // Now reset all the fields, then check to see if they all got reset.

        mStats.resetStatistics();
        assertEquals("ActiveExchanges not reset",
            new Integer(0), mStats.getActiveExchanges());
        assertEquals("InOnlyExchanges not reset",
            new Long(0), mStats.getInOnlyExchanges());
        assertEquals("InOptionalOutExchanges not reset",
            new Long(0), mStats.getInOptionalOutExchanges());
        assertEquals("InOutExchanges not reset",
            new Long(0), mStats.getInOutExchanges());
        assertEquals("RobustInOnlyExchanges not reset",
            new Long(0), mStats.getRobustInOnlyExchanges());
        assertEquals("FailedExchanges not reset",
            new Long(0), mStats.getFailedExchanges());
        assertEquals("TransactedExchanges not reset",
            new Long(0), mStats.getTransactedExchanges());
        assertEquals("TotalExchangeTimeToClose not reset",
            new Long(0), new Long(mStats.getTotalExchangeTimeToClose()));
    }
}
