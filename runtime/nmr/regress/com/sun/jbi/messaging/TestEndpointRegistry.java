/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEndpointRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.util.ArrayList;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;

/**
 * Tests for the EndpointRegistry class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestEndpointRegistry extends junit.framework.TestCase
{
    private static final String OWNER_A     = "ownerA";
    private static final String OWNER_B     = "ownerB";
    private static final QName  SERVICE_A   = new QName("serviceA");
    private static final QName  SERVICE_B   = new QName("serviceB");
    private static final String ENDPOINT_A  = "endpointA";
    private static final String ENDPOINT_B  = "endpointB";
    private static final QName  INTERFACE_A = new QName("interfaceA");
    private static final QName  INTERFACE_B = new QName("interfaceB");
    
    private EndpointRegistry mRegistry;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestEndpointRegistry(String aTestName)
    {
        super(aTestName);
        mRegistry = EndpointRegistry.getInstance();
	mRegistry.setMessageService(new MessageService());
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        // Make sure that toString() works after any type of change.
        System.out.println(mRegistry.toString());
        
        super.tearDown();
        mRegistry.clear();
    }

// =============================  test methods ================================

    /**
     *  Verify registration and check for pollution.
     */
    public void testRegisterInternalEndpoint()
        throws Exception
    {
        RegisteredEndpoint re1;
        RegisteredEndpoint re2;

        re1 = mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, OWNER_A);
        
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 1);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 0);

        // If the same owner attempts to register the same endpoint, the
        // same RegisteredEndpoint instance should be returned.

        re2 = mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, OWNER_A);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 1);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 0);
        assertSame(re1, re2);

        // If a different owner attempts to register the same endpoint, an
        // exception should be thrown.

        try
        {
            re2 = mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, OWNER_B);
            fail("Expected exception not received.");
        }
        catch ( javax.jbi.messaging.MessagingException mEx )
        {
            assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 1);
            assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 0);
        }
    }
    
    /**
     *  Verify registration and check for pollution.
     */
    public void testRegisterExternalEndpoint()
        throws Exception
    {
        ServiceEndpoint external;
        RegisteredEndpoint re1;
        RegisteredEndpoint re2;
        
        external = new Endpoint(SERVICE_A, ENDPOINT_A);
        re1 = mRegistry.registerExternalEndpoint(external, OWNER_A);
        
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 1);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 0);
	assertTrue(((ExternalEndpoint)re1).getDelegate() == external);

        // If the same owner attempts to register the same endpoint, an
        // exception should be thrown.

        try
        {
            re2 = mRegistry.registerExternalEndpoint(external, OWNER_A);
            fail("Expected exception not received.");
        }
        catch ( javax.jbi.messaging.MessagingException mEx )
        {
            assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 0);
            assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 1);
        }

        // If a different owner attempts to register the same endpoint, an
        // exception should be thrown.

        try
        {
            re2 = mRegistry.registerExternalEndpoint(external, OWNER_B);
            fail("Expected exception not received.");
        }
        catch ( javax.jbi.messaging.MessagingException mEx )
        {
            assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 0);
            assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 1);
        }
    }
    
    /**
     *  Verify get succeeds on internal endpoint and check for pollution.
     */
    public void testGetInternalEndpoint()
        throws Exception
    {
        mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, OWNER_A);
        
        assertTrue(mRegistry.getInternalEndpoint(SERVICE_A, ENDPOINT_A) != null);
        assertTrue(mRegistry.getExternalEndpoint(SERVICE_A, ENDPOINT_A) == null);
    }  
    
    /**
     *  Verify get succeeds on external endpoint and check for pollution.
     */
    public void testGetExternalEndpoint()
        throws Exception
    {
        ServiceEndpoint external;
        
        external = new Endpoint(SERVICE_A, ENDPOINT_A);
        mRegistry.registerExternalEndpoint(external, OWNER_A);
        
        assertTrue(mRegistry.getExternalEndpoint(SERVICE_A, ENDPOINT_A) != null);
        assertTrue(mRegistry.getInternalEndpoint(SERVICE_A, ENDPOINT_A) == null);
    }
    
    /**
     *  Verify get succeeds on internal endpoints and check for pollution.
     */
    public void testGetInternalEndpointsForService()
        throws Exception
    {
        ServiceEndpoint[] internal;
        ServiceEndpoint external;
        
        external = new Endpoint(SERVICE_A, ENDPOINT_B);
        
        mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, OWNER_A);
        mRegistry.registerInternalEndpoint(SERVICE_B, ENDPOINT_B, OWNER_A);
        mRegistry.registerExternalEndpoint(external, OWNER_A);
        
        internal = mRegistry.getInternalEndpointsForService(SERVICE_A, true);
        
        assertTrue(internal.length == 1);
        
        internal = mRegistry.getInternalEndpointsForService(null, false);
        
        assertTrue(internal.length == 2);
    }  
    
    /**
     *  Verify get succeeds on external endpoints and check for pollution.
     */
    public void testGetExternalEndpointsForService()
        throws Exception
    {
        ServiceEndpoint[] external;
        
        mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, OWNER_A);
        mRegistry.registerExternalEndpoint(new Endpoint(SERVICE_A, ENDPOINT_B), OWNER_A);
        
        external = mRegistry.getExternalEndpointsForService(SERVICE_A);
        
        assertTrue(external.length == 1);
        
    }  
    
    /**
     *  Verify removal and check for pollution.
     */
    public void testRemoveEndpoint()
        throws Exception
    {
        ServiceEndpoint external;
        ServiceEndpoint internal;
        
        external = new Endpoint(SERVICE_A, ENDPOINT_B);
        internal = mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, OWNER_A);
        mRegistry.registerExternalEndpoint(external, OWNER_B);
        
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 1);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 1);
        
        mRegistry.removeEndpoint(internal);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 1);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 0);
        
        mRegistry.removeEndpoint(external);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 0);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 0);
    }
    
    /**######################################################################
     * #################### ConnectionManager Tests ########################
     * ####################################################################*/
    
    /**
     *  Verify addition and check for pollution.
     */
    public void testAddInterfaceConnectionGood()
        throws Exception
    {
        RegisteredEndpoint[] endpoints;
        ServiceEndpoint      a;
        ServiceEndpoint      b;
        
        a = mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, "boo");
        b = mRegistry.registerInternalEndpoint(SERVICE_B, ENDPOINT_B, "hoo");
        mRegistry.addInterfaceConnection(INTERFACE_A, SERVICE_A, ENDPOINT_A);
        
        endpoints = mRegistry.getInternalEndpointsForInterface(INTERFACE_A);
        assertTrue(endpoints.length == 1);
        assertTrue(endpoints[0].getServiceName().equals(SERVICE_A));
        assertTrue(endpoints[0].getEndpointName().equals(ENDPOINT_A));

        endpoints = mRegistry.getInternalEndpointsForInterface(null);
        assertTrue(endpoints.length == 2);
        
        mRegistry.registerExternalEndpoint(a, "boo");
        mRegistry.registerExternalEndpoint(b, "hoo");
        mRegistry.addInterfaceConnection(INTERFACE_B, SERVICE_A, ENDPOINT_A);
        
        endpoints = mRegistry.getExternalEndpointsForInterface(INTERFACE_B);
        assertTrue(endpoints.length == 1);
        assertTrue(endpoints[0].getServiceName().equals(SERVICE_A));
        assertTrue(endpoints[0].getEndpointName().equals(ENDPOINT_A));

        endpoints = mRegistry.getExternalEndpointsForInterface(null);
        assertTrue(endpoints.length == 2);

    }
    
    /**
     *  Make sure duplicates are rejected
     */
    public void testAddInterfaceConnectionBad()
        throws Exception
    {
        mRegistry.addInterfaceConnection(INTERFACE_A, SERVICE_A, ENDPOINT_A);
        
        try
        {
            mRegistry.addInterfaceConnection(INTERFACE_A, SERVICE_A, ENDPOINT_A);
        }
        catch (javax.jbi.messaging.MessagingException ex) {}
    }
     /**
     *  Make sure identical duplicates are permitted.
     */
    public void testAddInterfaceConnectionDupeGood()
        throws Exception
    {
        mRegistry.addInterfaceConnection(INTERFACE_A, SERVICE_A, ENDPOINT_A);        
        mRegistry.addInterfaceConnection(INTERFACE_A, SERVICE_A, ENDPOINT_A);
    }
    
    /**
     *  Make sure non-identical duplicates are rejected.
     */
    public void testAddInterfaceConnectionDupeBad()
        throws Exception
    {
        mRegistry.addInterfaceConnection(INTERFACE_A, SERVICE_A, ENDPOINT_A);
        try
        {
            mRegistry.addInterfaceConnection(INTERFACE_A, SERVICE_A, "foo");
            fail("Able to add non-identical duplicate service connection");
        }
        catch (javax.jbi.messaging.MessagingException ex) {}
    }
    
    /**
     *  Make sure identical duplicates are permitted.
     */
    public void testAddEndpointConnectionDupeGood()
        throws Exception
    {
        mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
        mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
    }
    
    /**
     *  Make sure non-identical duplicates are rejected.
     */
    public void testAddEndpointConnectionDupeBad()
        throws Exception
    {
        mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
        
        try
        {
            mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, "foo", Link.STANDARD);
            
            fail("Able to add non-identical duplicate service connection");
        }
        catch (javax.jbi.messaging.MessagingException ex) {}
    }
   
    /**
     *  Verify addition and check for pollution.
     */
    public void testAddEndpointConnectionGood()
        throws Exception
    {
        mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
        
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL) == 0);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.EXTERNAL) == 0);
        assertTrue(mRegistry.countEndpoints(RegisteredEndpoint.LINKED) == 1);
        
        // make sure that a mapped endpoint is returned
        RegisteredEndpoint re;
        
        re = mRegistry.getInternalEndpoint(SERVICE_A, ENDPOINT_A);
        assertTrue(re instanceof LinkedEndpoint);
    }
    
    /**
     *  Test conversion of service connections to internal endpoints.
     */
    public void testGetInternalEndpointsForServiceConversion()
        throws Exception
    {
        mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
        mRegistry.registerInternalEndpoint(SERVICE_B, ENDPOINT_B, OWNER_B);
        
        RegisteredEndpoint[] re;
        
        // try without conversion        
        re = mRegistry.getInternalEndpointsForService(SERVICE_A, false);
        assertTrue(re.length == 1);
        assertTrue(re[0] instanceof LinkedEndpoint);
        assertEquals(re[0].getServiceName(), SERVICE_A);
        
        // try with conversion        
        re = mRegistry.getInternalEndpointsForService(SERVICE_A, true);
        assertTrue(re.length == 1);
        assertTrue(re[0] instanceof InternalEndpoint);
        assertEquals(re[0].getServiceName(), SERVICE_B);
    }
    
    /**
     *  Test for mixed results, with linked and internal endpoints available
     *  for a given service.
     */
    public void testGetInternalEndpointsForServiceMixed()
        throws Exception
    {
        mRegistry.addEndpointConnection(
            SERVICE_A, "linked1", SERVICE_B, ENDPOINT_B, Link.STANDARD);
        mRegistry.addEndpointConnection(
            SERVICE_A, "linked2", SERVICE_B, ENDPOINT_B, Link.STANDARD);
        
        mRegistry.registerInternalEndpoint(SERVICE_A, "internal1", OWNER_B);
        mRegistry.registerInternalEndpoint(SERVICE_A, "internal2", OWNER_B);
        
        RegisteredEndpoint[] re;
        ArrayList endpointNames = new ArrayList();
        
        // peform a query for the service SERVICE_A
        re = mRegistry.getInternalEndpointsForService(SERVICE_A, false);
        assertTrue(re.length == 4);
        
        // build a list of endpoint names and check 'em
        for (int i = 0; i < re.length; i++)
        {
            endpointNames.add(re[i].getEndpointName());
        }
        
        assertTrue(endpointNames.contains("linked1"));
        assertTrue(endpointNames.contains("linked2"));
        assertTrue(endpointNames.contains("internal1"));
        assertTrue(endpointNames.contains("internal2"));
    }
    
    /**
     *  Test for mixed results, with linked and external endpoints available
     *  for a given service.
     */
    public void testGetExternalEndpointsForServiceMixed()
        throws Exception
    {
        mRegistry.registerExternalEndpoint(new Endpoint(SERVICE_A, "internal1"), OWNER_B);
        mRegistry.registerExternalEndpoint(new Endpoint(SERVICE_A, "internal2"), OWNER_B);
        
        RegisteredEndpoint[] re;
        ArrayList endpointNames = new ArrayList();
        
        // peform a query for the service SERVICE_A
        re = mRegistry.getExternalEndpointsForService(SERVICE_A);
        assertEquals(2, re.length);
        
        // build a list of endpoint names and check 'em
        for (int i = 0; i < re.length; i++)
        {
            endpointNames.add(re[i].getEndpointName());
        }
        
        assertTrue(endpointNames.contains("internal1"));
        assertTrue(endpointNames.contains("internal2"));
    }
    
    /**
     *  Make sure duplicates are rejected
     */
    public void testAddEndpointConnectionBad()
        throws Exception
    {        
        mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
        
        try
        {            
            mRegistry.addEndpointConnection(
                SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
        }
        catch (javax.jbi.messaging.MessagingException ex) {}
    }
    
    /**
     *  Verify removal and check for pollution.
     */
    public void testRemoveInterfaceConnection()
        throws Exception
    {        
        mRegistry.addInterfaceConnection(
            INTERFACE_A, SERVICE_A, ENDPOINT_A);
        
        // verify that the endpoint was removed
        assertTrue(mRegistry.removeInterfaceConnection(
            INTERFACE_A, SERVICE_A, ENDPOINT_A));
        
        // verify that subsequent calls fail
        assertFalse(mRegistry.removeInterfaceConnection(
            INTERFACE_A, SERVICE_A, ENDPOINT_A));
    }
        
    /**
     *  Verify removal and check for pollution.
     */
    public void testRemoveEndpointConnection()
        throws Exception
    {             
        mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
        
        // verify that the endpoint was removed
        assertTrue(mRegistry.removeEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B));
        
        // verify that subsequent calls fail
        assertFalse(mRegistry.removeEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B));
    }
    
    /**
     * Verify a LinkedEndpoint and InternalEndpoint of the same name don't
     * damage each other.
     */
    public void testLinkedInternalDuplicate()
	throws Exception
    {
        mRegistry.addEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B, Link.STANDARD);
        ServiceEndpoint	ie = mRegistry.registerInternalEndpoint(SERVICE_A, ENDPOINT_A, OWNER_B);
	assertEquals(2, mRegistry.getInternalEndpointsForService(SERVICE_A, true).length);
	mRegistry.removeEndpoint(ie);
	assertEquals(1, mRegistry.getInternalEndpointsForService(SERVICE_A, true).length);
	assertTrue(mRegistry.getInternalEndpointsForService(SERVICE_A, true)[0] instanceof LinkedEndpoint);
	mRegistry.removeEndpointConnection(
            SERVICE_A, ENDPOINT_A, SERVICE_B, ENDPOINT_B);
	assertEquals(0, mRegistry.getInternalEndpointsForService(SERVICE_A, true).length);
    }

    class Endpoint implements ServiceEndpoint
    {
        private QName mService;
        private String mEndpoint;
        
        Endpoint(QName service, String endpoint)
        {
            mService    = service;
            mEndpoint   = endpoint;
        }
        
        public org.w3c.dom.DocumentFragment getAsReference(QName operationName)
        {
            return null;
        }
        
        public String getEndpointName()
        {
            return mEndpoint;
        }
        
        public QName[] getInterfaces()
        {
            return null;
        }
        
        public javax.xml.namespace.QName getServiceName()
        {
            return mService;
        }
        
    }
}
