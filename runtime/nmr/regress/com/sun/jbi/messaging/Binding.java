/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Binding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/** Generic binding skeleton for use with NMS junit/regress tests.
 * @author Sun Microsystems, Inc.
 */
public abstract class Binding implements Runnable
{
    private   Exception             mError;
    private   String                mFailTxt;
    protected boolean               mIsProvider;
    protected DeliveryChannelImpl   mChannel;
    protected ServiceEndpoint       mEndpoint;
    protected Sequencer             mSequencer;
    
    protected Binding(DeliveryChannelImpl channel)
    {
        mChannel = channel;
    }
    
    /** Used for provider cases. */
    public void init(QName service, String endpoint)
        throws Exception
    {
        mEndpoint = mChannel.activateEndpoint(service, endpoint);
        mIsProvider = true;
    }    
    
    /** Used in consumer cases. */
    public void init(QName service)
        throws Exception
    {
        ServiceEndpoint[] refs;
        
        refs = mChannel.getEndpointsForService(service);
        if (refs.length == 0)
        {
            throw new Exception("No endpoints found for service: " + service.getLocalPart());
        }
        
        mIsProvider = false;
        mEndpoint   = refs[0];
    }
 
    public void setSequencer(Sequencer sequencer)
    {
        mSequencer = sequencer;
    }
    
    public Sequencer getSequencer()
    {
        return (mSequencer);
    }
    
    public void stop()
        throws Exception
    {
        if (mIsProvider)
        {
            mChannel.deactivateEndpoint(mEndpoint);
        }
        
        mChannel.close();
    }
    
    public void run()
    {
        try
        {
            start();
        }
        catch (Exception ex)
        {
            mError = ex;
        }
    }
    
    public abstract void start()
        throws Exception;
    
    public void checkError()
        throws Exception
    {
        if (mError != null)
        {
            throw mError;
        }
        else if (mFailTxt != null)
        {
            throw new Exception(mFailTxt);
        }
    }
    
    public void setFailure(String txt)
    {
        mFailTxt = txt;
    }
}
