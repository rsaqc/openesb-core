/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestExchangeFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.net.URI;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.RobustInOnly;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;

/**
 * Tests for the TestExchangeFactory class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestExchangeFactory extends junit.framework.TestCase
{
    /** NMS impl */
    private MessageService mMsgSvc;
    
     /** NMR Environment Context */
    private NMRContext mContext;
    
    /** Exchange Factory */
    private ExchangeFactory mFactory;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestExchangeFactory(String aTestName)
    {
        super(aTestName);
        
        mMsgSvc = new MessageService();
        mFactory = new ExchangeFactory(mMsgSvc);
        mContext = new NMRContext(mMsgSvc);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mMsgSvc.initService(mContext);
        mMsgSvc.startService();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
        mMsgSvc.stopService();
        mContext.reset();
        
    }

// =============================  test methods ================================

    /**
     *  Test to make sure the 4 standard URIs are good.
     */
    public void testCreateExchangeUriGood()
        throws Exception
    {
        assertTrue(mFactory.createExchange(
            ExchangePattern.IN_ONLY.getURI()) instanceof InOnly);
        
        assertTrue(mFactory.createExchange(
            ExchangePattern.IN_OUT.getURI()) instanceof InOut);
        
        assertTrue(mFactory.createExchange(
            ExchangePattern.ROBUST_IN_ONLY.getURI()) instanceof RobustInOnly);
        
        assertTrue(mFactory.createExchange(
            ExchangePattern.IN_OPTIONAL_OUT.getURI()) instanceof InOptionalOut);
    }
    
    /** 
     *  Factory which has been initialized with an interface should always
     *  set that value on created exchanges.
     */
    public void testCreateExchangeWithInterface()
        throws Exception
    {
        QName           interfaceName = new QName("boo");        
        ExchangeFactory iFactory;
        
        iFactory = ExchangeFactory.newInterfaceFactory(mMsgSvc,
            interfaceName);
        
        assertEquals(iFactory.createExchange(
            ExchangePattern.IN_ONLY.getURI()).getInterfaceName(), interfaceName);
    }
    
    /** 
     *  Test WSDL 2.0 parsing.
     */
    public void testCreateExchangeFromWsdl20()
        throws Exception
    {
        String              id = "testcomp";
        Component           com;
        ComponentContext    ctx;
        MessageExchange     me;
        
        // create a component, add it to env, and get it's context object
        com = new NMRComponent();
        mContext.addComponentInstance(id, com);
        ctx = mContext.getComponentContext(id);
        
        // activate an endpoint and create an exchange for one of its operations
        ctx.activateEndpoint(
            WsdlDocument.STOCK_SERVICE_Q, WsdlDocument.STOCK_ENDPOINT);
        me = mFactory.createExchange(
            WsdlDocument.STOCK_SERVICE_Q, WsdlDocument.STOCK_OPERATION_Q);
        
        assertTrue(me instanceof InOut);
    }
    
    
    /** 
     *  Test WSDL 1.1 parsing.
     */
    public void testCreateExchangeFromWsdl11()
        throws Exception
    {
        String              id = "testcomp";
        Component           com;
        ComponentContext    ctx;
        MessageExchange     me;
        
        // create a component, add it to env, and get it's context object
        com = new NMRComponent(NMRComponent.WSDL_11);
        mContext.addComponentInstance(id, com);
        ctx = mContext.getComponentContext(id);
                
        // activate an endpoint and create an exchange for one of its operations
        ctx.activateEndpoint(
            WsdlDocument.STOCK_SERVICE_Q, WsdlDocument.STOCK_ENDPOINT);
        me = mFactory.createExchange(
            WsdlDocument.STOCK_SERVICE_Q, WsdlDocument.STOCK_OPERATION_Q);
        
        
        assertTrue(me instanceof InOut);
    }
    
    /** 
     *  Factory which has been initialized with an endpoint should always
     *  set that value on created exchanges.
     */
    public void testCreateExchangeWithEndpoint()
        throws Exception
    {
        RegisteredEndpoint  endpoint;
        ExchangeFactory     eFactory;
        
        endpoint = new InternalEndpoint(new QName("foo"), "bar", "la");
        eFactory = ExchangeFactory.newEndpointFactory(mMsgSvc,
            endpoint);
        
        assertEquals(eFactory.createExchange(
            ExchangePattern.IN_OUT.getURI()).getEndpoint(), endpoint);
    }
    
    /**
     *  Test to make sure bogus URIs throw an exception
     */
    public void testCreateExchangeUriBad()
        throws Exception
    {
        try
        {
            // this line should throw an exception
            mFactory.createExchange(new URI("inside-out-mep"));            
            // shouldn't get here
            fail("able to create exchange with bogus uri");
        }
        catch (Exception ex)
        {
            // expected
        }
    }
    
    /**
     * Test MEP factory method.
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateInOnlyExchange()
           throws Exception
    {        
        InOnly inOnly = mFactory.createInOnlyExchange();
    }
    
    /**
     * Test MEP factory method.
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateInOptionalOutExchange()
           throws Exception
    {        
        InOptionalOut inOptionalOut = mFactory.createInOptionalOutExchange();
    }
        
    /**
     * Test MEP factory method.
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateInOutExchange()
           throws Exception
    {
        InOut inOut = mFactory.createInOutExchange();
    }
    
    /**
     * Test MEP factory method.
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateRobustInOnlyExchange()
           throws Exception
    {
        RobustInOnly robInOnly = mFactory.createRobustInOnlyExchange();
    }
    
}
