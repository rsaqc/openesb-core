/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Framework.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

/** Simple utility class used to mock up a framework for running engine and 
 *  binding tests in independent threads.
 * @author Sun Microsystems, Inc.
 */
public class Framework
{
    private static final int MAX_WAIT = 30000;
    
    public static void runTest(Runnable run1, Runnable run2)
        throws Exception
    {
        Thread t1;
        Thread t2;
        
        t1 = new Thread(run1);
        t2 = new Thread(run2);
        
        t1.start();
        t2.start();
        
        t2.join(MAX_WAIT);
        t1.join(MAX_WAIT);
    }    
}
