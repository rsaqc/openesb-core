/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointReference.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.messaging.util.XMLUtil;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;

import javax.jbi.servicedesc.ServiceEndpoint;

/** XML representation of an internal service endpoint.
 * @author Sun Microsystems, Inc.
 */
public class EndpointReference
{
    public static final String NS_URI_JBI = 
        "http://java.sun.com/xml/ns/jbi";
    public static final String NS_PFIX_JBI = 
        "jbi";
    public static final String NS_PFIX_SERVICE = 
        "s";
    public static final String ELE_END_POINT_REFERENCE = 
        "end-point-reference";
    public static final String ATTR_SERVICE_NAME = 
        "service-name";
    public static final String ATTR_ENDPOINT_NAME = 
        "end-point-name";
    
    public static DocumentFragment asReference(ServiceEndpoint endpoint)
        throws javax.jbi.messaging.MessagingException
    {
        Document            doc;
        DocumentFragment    frag;
        Element             ele;
        
        doc = XMLUtil.getInstance().newDocument();
        
        ele = doc.createElementNS(NS_URI_JBI, qname(NS_PFIX_JBI, ELE_END_POINT_REFERENCE));
        ele.setAttribute("xmlns:" + NS_PFIX_JBI, NS_URI_JBI);
        ele.setAttribute("xmlns:" + NS_PFIX_SERVICE, endpoint.getServiceName().getNamespaceURI());
        ele.setAttributeNS(NS_URI_JBI, 
                           qname(NS_PFIX_JBI, ATTR_SERVICE_NAME), 
                           qname(NS_PFIX_SERVICE, endpoint.getServiceName().getLocalPart()));
        ele.setAttributeNS(NS_URI_JBI, 
                           qname(NS_PFIX_JBI, ATTR_ENDPOINT_NAME), 
                           endpoint.getEndpointName());
        
        doc.appendChild(ele);
        
        frag = doc.createDocumentFragment();
        frag.appendChild(doc.getDocumentElement());
        
        return frag;
    }
    
    private static String qname(String prefix, String value)
    {
        return prefix + ":" + value;
    }
}
