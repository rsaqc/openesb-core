/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDLHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging.util;

import com.sun.jbi.messaging.LocalStringKeys;
import com.sun.jbi.messaging.ExchangeFactory;
import com.sun.jbi.messaging.ExchangePattern;

import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Endpoint;
import com.sun.jbi.wsdl2.Interface;
import com.sun.jbi.wsdl2.InterfaceOperation;
import com.sun.jbi.wsdl2.Service;
import com.sun.jbi.wsdl2.WsdlReader;
import com.sun.jbi.wsdl2.WsdlException;
import com.sun.jbi.wsdl2.impl.WsdlFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;
import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/** Silly little utility object to help out in parsing WSDL for some NMR-specific
 *  requirements.  NOTE: this is built as a layer on top of our existing WSDL
 *  API.
 * @author Sun Microsystems, Inc.
 */
public final class WSDLHelper
{
    private static final String WSDL_11 = "http://schemas.xmlsoap.org/wsdl/";
    private static final String WSDL_20 = "http://www.w3.org/2005/05/wsdl";
    
    private static final String TARGET_NS       = "targetNamespace";
    private static final String BINDING         = "binding";
    private static final String BINDING_TYPE    = "type";
    private static final String PORTTYPE        = "portType";
    private static final String SERVICE         = "service";
    private static final String PORT            = "port";
    private static final String NAME            = "name";
    private static final String OPERATION       = "operation";
    private static final String IN_MSG          = "input";
    private static final String OUT_MSG         = "output";
            
    /** Entry point to WSDL API. */
    private static WsdlFactory mFactory;
    /** Reads WSDL definitions. */
    private static WsdlReader mReader;
    
    private static Logger mLog = Logger.getLogger("com.sun.jbi.messaging.util");
        
    /** Static initializer for factory and reader objects. */
    static
    {
        mFactory = new WsdlFactory();
        mReader  = mFactory.newWsdlReader();
    }
    
    /** Uses the owner id of the reference to query the appopriate channel. */
    public static HashMap getOperationsForService(Document serviceDesc, QName serviceName) 
        throws javax.jbi.messaging.MessagingException
    {
        HashMap operations;
        String  version;
        
        version = getWSDLVersion(serviceDesc);
        
        if (version.equals(WSDL_11))
        {
            operations = getOperationsForService11(serviceDesc, serviceName);
        }
        else
        {
            operations = getOperationsForService20(serviceDesc, serviceName);
        }
        mLog.fine(operations.size() + " operation(s) found for service " + serviceName);
        return operations;
    }    
    
    public static QName[] getInterfacesForService(Document serviceDesc, QName serviceName)
        throws javax.jbi.messaging.MessagingException
    {
        QName[] interfaces;
        String  version;
        
        version = getWSDLVersion(serviceDesc);
        
        if (version.equals(WSDL_11))
        {
            interfaces = getInterfacesForService11(serviceDesc, serviceName);
        }
        else
        {
            interfaces = getInterfacesForService20(serviceDesc, serviceName);
        }
        mLog.fine(interfaces.length + " interface(s) found for service " + serviceName);
        return interfaces;
    }
    
    
    private static HashMap getOperationsForService11(Document serviceDesc, QName serviceName) 
        throws javax.jbi.messaging.MessagingException
    {
        HashMap     operations = new HashMap();
        ArrayList   portTypes;
        String      namespace;
        Element     definitions;
        
        definitions = serviceDesc.getDocumentElement();         
        namespace = getTargetNamespace(serviceDesc);
        
        if (namespace != null && serviceName.getNamespaceURI().equals(namespace))
        {
            portTypes = getPortTypes(definitions, serviceName.getLocalPart());
            
            for (int i = 0; i < portTypes.size(); i++)
            {
                Element  portType = (Element)portTypes.get(i);
                NodeList opList   = portType.getElementsByTagNameNS(WSDL_11, OPERATION);
                
                for (int j = 0; j < opList.getLength(); j++)
                {
                    Element     op   = (Element)opList.item(j);
                    QName       name = new QName(namespace, op.getAttribute(NAME));
                    NodeList    ins  = op.getElementsByTagNameNS(WSDL_11, IN_MSG);                    
                    NodeList    outs = op.getElementsByTagNameNS(WSDL_11, OUT_MSG);
                    
                    // All supported meps must have an in message
                    if (ins.getLength() > 0)
                    {
                        if (outs.getLength() > 0)
                        {
                            operations.put(name.toString(), 
                                    ExchangePattern.IN_OUT.toString());
                        }
                        else
                        {
                            operations.put(name.toString(), 
                                    ExchangePattern.IN_ONLY.toString());
                        }
                    }                    
                }
            }
        }
        return operations;   
    }
    
    private static HashMap getOperationsForService20(Document serviceDesc, QName serviceName) 
        throws javax.jbi.messaging.MessagingException
    {
        HashMap     operations = new HashMap();
        Description defs;
        Service     service;
        
        defs = readWSDL(serviceDesc, serviceName);

        for (int i = 0; i < defs.getServicesLength(); i++)
        {
            service = defs.getService(i);
            if (service.getName().equals(serviceName.getLocalPart()))
            {
                Interface             interfaze;
                InterfaceOperation[]  iops;

                interfaze = service.getInterface();
                iops = interfaze.getExtendedOperations();
                
                for (int j = 0; j < iops.length; j++)
                {
                    operations.put(iops[j].getQualifiedName().toString(),
                                   iops[j].getPattern());
                }
                break;
            }
        }

        return operations;
    }
    
    private static QName[] getInterfacesForService11(Document serviceDesc, QName serviceName)
        throws javax.jbi.messaging.MessagingException
    {        
        QName[]     interfaces;
        ArrayList   portTypes;
        String      namespace;
        Element     definitions;
        
        definitions = serviceDesc.getDocumentElement();         
        namespace = getTargetNamespace(serviceDesc);
        
        if (namespace != null && serviceName.getNamespaceURI().equals(namespace))
        {
            portTypes  = getPortTypes(definitions, serviceName.getLocalPart());     
            interfaces = new QName[portTypes.size()];
            
            for (int i = 0; i < portTypes.size(); i++)
            {
                Element portType = (Element)portTypes.get(i);
                String  name     = portType.getAttribute(NAME);
                
                interfaces[i] = new QName(namespace, name);
            }
        }
        else
        {
            interfaces = new QName[0];
        }
        
        return interfaces;   
    }
    
    private static QName[] getInterfacesForService20(Document serviceDesc, QName serviceName)
        throws javax.jbi.messaging.MessagingException
    {
        ArrayList   interfaces = new ArrayList();
        Description defs;
        Service     service;
        
        defs = readWSDL(serviceDesc, serviceName);
        
        for (int i = 0; i < defs.getServicesLength(); i++)
        {
            service = defs.getService(i);
            if (service.getName().equals(serviceName.getLocalPart()))
            {
                Interface   impl     = service.getInterface();
                Interface[] extended = impl.getExtendedInterfaces();
                                
                interfaces.add(impl.getQName());
                for (int j = 0; j < extended.length; j++)
                {
                    interfaces.add(extended[j].getQName());
                }
                
                break;
            }
        }
        
        return (QName[])interfaces.toArray(new QName[0]);
    }
    
    private static String getWSDLVersion(Document description)
    {
        return description.getDocumentElement().getNamespaceURI();
    }
    
    private static String getTargetNamespace(Document description)
    {
        String  namespace = null;
        Element definitions;
        
        definitions = description.getDocumentElement();
        namespace   = definitions.getAttribute(TARGET_NS);
        
        return namespace;
    }
    
    private static ArrayList getPortTypes(
        Element definitions, String serviceName)
    {
        NodeList    services;
        NodeList    bindings;
        NodeList    portTypes;
        ArrayList   result          = new ArrayList();
        ArrayList   bindingNames    = new ArrayList();
        ArrayList   portTypeNames   = new ArrayList();
        
        // get a list of service bindings
        services = definitions.getElementsByTagNameNS(WSDL_11, SERVICE);
        for (int i = 0; i < services.getLength(); i++)
        {
            Element service = (Element)services.item(i);
            if (service.getAttribute(NAME).equals(serviceName))
            {                
                mLog.finer("Found service " + serviceName + " in WSDL 1.1 definition");
                NodeList ports = service.getElementsByTagNameNS(WSDL_11, PORT);
                for (int j = 0; j < ports.getLength(); j++)
                {
                    Element port = (Element)ports.item(j);
                    bindingNames.add(getLocalName(port.getAttribute(BINDING)));
                }
                mLog.finer(bindingNames.size() + " bindings found for service " + serviceName + " in WSDL 1.1 definition");
                break;
            }
        }
        
        // troll through the bindings we found to find port types
        bindings = definitions.getElementsByTagNameNS(WSDL_11, BINDING);
        for (int i = 0; i < bindings.getLength(); i++)
        {
            Element binding = (Element)bindings.item(i);
            String  name    = binding.getAttribute(NAME);
            
            // check to see if this is one of our service bindings
            if (bindingNames.contains(name))
            {
                portTypeNames.add(getLocalName(binding.getAttribute(BINDING_TYPE)));
            }
        }
        mLog.finer(portTypeNames.size() + " portTypes found for service " + serviceName + " in WSDL 1.1 definition");
        
        // troll through port type definitions to find matches
        portTypes = definitions.getElementsByTagNameNS(WSDL_11, PORTTYPE);        
        for (int i = 0; i < portTypes.getLength(); i++)
        {
            Element portType = (Element)portTypes.item(i);
            String  name    = portType.getAttribute(NAME);
            
            // check to see if this is one of our service bindings
            if (portTypeNames.contains(name))
            {
                result.add(portType);
            }
        }
        
        return result;
    }
    
    private static String getLocalName(String qname)
    {
        if (qname.indexOf(':') != -1)
        {
            qname = qname.split(":")[1];
        }
        
        return qname;
    }
    
    private static Description readWSDL(Document description, QName serviceName)
        throws javax.jbi.messaging.MessagingException
    {        
        try
        {
            return mReader.readDescription(null, description);
        }
        catch (java.io.IOException ioEx)
        {
            throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.WSDL_IMPORT_ERROR, 
                    new Object[] {serviceName.toString(), ioEx.toString()} ));                        
        }
        catch (WsdlException wEx)
        {
            throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.WSDL_IMPORT_ERROR, 
                    new Object[] {serviceName.toString(), wEx.toString()} ));
        }
    }
}
