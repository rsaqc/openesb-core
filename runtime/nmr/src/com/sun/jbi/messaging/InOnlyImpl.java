/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InOnlyImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.net.URI;

import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.NormalizedMessage;

/** Implementation of In Only Message Exchange Pattern.
 * @author Sun Microsystems, Inc.
 */
public class InOnlyImpl 
        extends MessageExchangeProxy 
        implements InOnly
{
    /** Simple state machine for the SOURCE side of the  pattern. */
    private static final int[][]  SOURCE  =
    {
        { SET_IN | ADDRESS | SET_TRANSACTION |
          SET_PROPERTY | DO_SEND | DO_SENDSYNCH | MARK_ACTIVE | SUSPEND_TX,
          1, -1, -1, -1
        },
        { DO_ACCEPT | STATUS | RESUME_TX,
          -1, 2, -1, -1
        },
        { MARK_DONE | COMPLETE,
          -1, -1, -1, -1
        },
    };

    /** Simple state machine for the TARGET side of the pattern */
    private static final int[][]  TARGET  = 
    {
        { DO_ACCEPT | REQUEST | RESUME_TX,
          -1, 1, -1, -1
        },
        { SET_ERROR | SET_DONE | SET_PROPERTY,
          -1, -1, 2, -1 
        },
        { SET_ERROR | SET_DONE | DO_SEND | SUSPEND_TX | SET_PROPERTY, 
          3, -1, 2, -1
        },
        { MARK_DONE, 
          -1, -1, -1, -1
        }
    };
        
    /**
     * Default constructor.
     */
    InOnlyImpl()
    {
        this(SOURCE);
    }
        
    /** 
     * Create a new InOutImpl in the forward or reverse direction.
     */
    InOnlyImpl(int[][] state)
    {
        super(state);
    }

    /**
     * Return instance of outselves in the target role.
     */
    MessageExchangeProxy newTwin()
    {
        return (new InOnlyImpl(TARGET));
    }

    /** Get the pattern.
     * @return the message exchange pattern.
     */
    public URI getPattern()
    {
        return (ExchangePattern.IN_ONLY.getURI());
    }

    /** Retrieve the message with reference id "in" from this exchange.
     * @return the out message, or null if it is not present in the exchange
     */    
    public NormalizedMessage getInMessage()
    {
        return getMessage(IN_MSG);
    }

    /** Retrieve the message with reference id "in" from this exchange.
     * @return the out message, or null if it is not present in the exchange
     */    
    public void  setInMessage(NormalizedMessage message)
                throws javax.jbi.messaging.MessagingException
    {
        setMessage(message, IN_MSG);
    }
}
    
