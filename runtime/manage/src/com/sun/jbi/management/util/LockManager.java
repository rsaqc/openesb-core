/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LockManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  LockManager.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 2, 2006, 4:49 PM
 */

package com.sun.jbi.management.util;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.system.ManagementContext;

import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Utility class to manage the acquition and release of a ReentrantReadWrite Lock.
 *
 * @author Sun Microsystems, Inc
 */
public class LockManager
{
    
    /**
     * The ReentrantReadWriteLock 
     */
    private ReentrantReadWriteLock mLock;
    
    /**
     * The Logger
     */
    private Logger mLogger;
    
    /**
     * Lock Name.
     */
    private String mLockName;
    
    /**
     * The Waiting Time to acquire a Lock, 60 seconds
     */
    private static long DEFAULT_LOCK_WAIT_INTERVAL = 60;
    
    /**
     * String Translator
     */
    private StringTranslator mTranslator;
    
    /**
     * Actual Lock Interval
     */
    private long mLockWaitInterval;
    
    /**
     *
     */
    static final String STRING_TRANSLATOR_NAME = "com.sun.jbi.management";
    
    /** 
     * Creates a new instance of LockManager 
     * 
     * @param lock - the ReentrantReadWriteLock to be managed
     * @param lockName - user friendly name for the lock
     * @param ctx - management context
     * @param lockInterval - secons to wait for acquiring a registry read/write lock.
     */
    public LockManager (ReentrantReadWriteLock lock, String lockName
        , ManagementContext ctx, String lockInterval)
    {
        mLock = lock;
        mLockName = lockName;
        mLogger = ctx.getLogger();
        
        mTranslator = ctx.getEnvironmentContext().
        getStringTranslator(STRING_TRANSLATOR_NAME);
        
        if ( lockInterval == null )
        {
            mLockWaitInterval = DEFAULT_LOCK_WAIT_INTERVAL; 
        }
        else
        {
            try
            {
                mLockWaitInterval = Long.parseLong(lockInterval);
            }
            catch (NumberFormatException nex)
            {
                mLockWaitInterval = DEFAULT_LOCK_WAIT_INTERVAL;
            }
        }
        mLogger.fine("Lock wait interval for lock " + lockName + " is " + mLockWaitInterval +
            " seconds.");
    }
    
     /**
     * Acquire Read Lock
     *
     * @throws a RegistryException if a Read Lock cannot be obtained after waiting 
     * for the lock interval
     */
    public void acquireReadLock () throws RegistryException
    {
        if ( mLogger.getLevel() == Level.FINEST )
        {
            mLogger.finest (mTranslator.getString (LocalStringKeys.JBI_ADMIN_ATTEMPT_READ_LOCK,
                Thread.currentThread().getName(), getCallerInformation(), mLockName));
        }
        
        
        boolean acquired = false;
        
        try
        {
            acquired = mLock.readLock ().tryLock (mLockWaitInterval,
                TimeUnit.SECONDS);
        }
        catch (InterruptedException iex)
        {
            acquired = false;
        }
        
        if ( !acquired )
        {
            
            if ( mLogger.getLevel() == Level.FINEST )
            {
                mLogger.finest (mTranslator.getString (
                    LocalStringKeys.JBI_ADMIN_THREAD_FAILED_ACQUIRE_READ_LOCK,
                    Thread.currentThread().getName(), getCallerInformation(), mLockName, 
                    new Long(mLockWaitInterval).toString ()));
            }
            
            
            throw new RegistryException(mTranslator.getString(LocalStringKeys.JBI_ADMIN_FAILED_ACQUIRE_READ_LOCK, 
                Thread.currentThread().getName(), getCallerInformation(), mLockName));   
             

        }
        else
        {
            if ( mLogger.getLevel() == Level.FINEST )
            {
                mLogger.finest (mTranslator.getString (LocalStringKeys.JBI_ADMIN_ACQUIRED_READ_LOCK,
                    Thread.currentThread().getName(), getCallerInformation(), mLockName));
            }
        }
        
    }
    
    /**
     * Acquire write lock to the in-memory registry lock.
     *
     * @throws a RegistryException if a Write Lock cannot be obtained after waiting 
     * for the lock wait interval
     */
    public void acquireWriteLock () throws RegistryException
    {
        if ( mLogger.getLevel() == Level.FINEST )
        {
            mLogger.finest (mTranslator.getString (LocalStringKeys.JBI_ADMIN_ATTEMPT_WRITE_LOCK,
                Thread.currentThread().getName(), getCallerInformation(),  mLockName));
            
        }
        
        boolean acquired = false;
        
        try
        {
            acquired = mLock.writeLock ().tryLock (mLockWaitInterval,
                TimeUnit.SECONDS);
        }
        catch (InterruptedException iex)
        {
            acquired = false;
        }
        
        if ( !acquired )
        {
         
            if ( mLogger.getLevel() == Level.FINEST )
            {
                mLogger.finest (mTranslator.getString (
                    LocalStringKeys.JBI_ADMIN_THREAD_FAILED_ACQUIRE_WRITE_LOCK,
                    Thread.currentThread().getName(), getCallerInformation(), mLockName,
                    new Long(mLockWaitInterval).toString ()));
            }
            

            throw new RegistryException(
                mTranslator.getString(LocalStringKeys.JBI_ADMIN_FAILED_ACQUIRE_WRITE_LOCK,
                    Thread.currentThread().getName(), getCallerInformation(), mLockName));
        }
        else
        {
            if ( mLogger.getLevel() == Level.FINEST )
            {
                mLogger.finest (mTranslator.getString (LocalStringKeys.JBI_ADMIN_ACQUIRED_WRITE_LOCK,
                    Thread.currentThread().getName(), getCallerInformation(), mLockName));
            }
        }
    }
    
    /**
     * Release Read Lock
     */
    public void releaseReadLock ()
    {
        
        mLock.readLock ().unlock ();
        if ( mLogger.getLevel() == Level.FINEST )
        {
            mLogger.finest (mTranslator.getString (LocalStringKeys.JBI_ADMIN_RELEASED_READ_LOCK,
                Thread.currentThread ().getName(), getCallerInformation(), mLockName));
        }
        
    }
    
    /**
     * Release write lock
     */
    public void releaseWriteLock ()
    {
        if ( mLock.isWriteLockedByCurrentThread () )
        {
            mLock.writeLock ().unlock ();
            
            if ( mLogger.getLevel() == Level.FINEST )
            {
                mLogger.finest (mTranslator.getString (LocalStringKeys.JBI_ADMIN_RELEASED_WRITE_LOCK,
                    Thread.currentThread().getName(), getCallerInformation(), mLockName));    
            }
        }
        
    }
    
    /**
     * Downgrade Write Lock
     *
     * @throws RegistryException if the Read Lock cannot be acquired.
     */
    public void downgradeWriteLock () throws RegistryException
    {
        
        if ( mLogger.getLevel() == Level.FINEST )
        {
            mLogger.finest (mTranslator.getString (LocalStringKeys.JBI_ADMIN_ATTEMPT_DOWNGRADE_LOCK,
                Thread.currentThread().getName(), getCallerInformation(), mLockName));
        }
        acquireReadLock ();
        releaseWriteLock ();
        
        if ( mLogger.getLevel() == Level.FINEST )
        {
            mLogger.finest (mTranslator.getString (LocalStringKeys.JBI_ADMIN_DOWNGRADED_LOCK,
                Thread.currentThread().getName(), getCallerInformation(), mLockName));
        }
    }
    
    /**
     * @return a String with information on the caller Class, Method and LineNumber
     */
    private String getCallerInformation()
    {
        StackTraceElement[] callStack = Thread.currentThread().getStackTrace();
        StringBuffer strBuf = new StringBuffer();
        if ( callStack.length >= 5 )
        {
            StackTraceElement top = callStack[4];
            strBuf.append("[ Class: ");
            strBuf.append(top.getClassName());
            strBuf.append(", Method: ");
            strBuf.append(top.getMethodName());
            strBuf.append(", Line: ");
            strBuf.append(top.getLineNumber());
            strBuf.append("]");
        }
        return strBuf.toString();
    }
    
}
