/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentInstallationContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import com.sun.jbi.util.StringTranslator;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Contains information necessary for a component (BC or SE) to perform
 * special installation/uninstallation processing. This context is provided
 * to the <CODE>init</CODE> method of the javax.jbi.component.Bootstrap
 * implementation for the component.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentInstallationContext
    implements com.sun.jbi.component.InstallationContext
{
    /**
     * Flag to determine whether the bootstrap class loader should use an
     * inverted (self-first) hierarchy or the normal (parent-first) hierarchy.
     * Set to true for self-first, false for parent-first.
     */
    private boolean mBootstrapClassLoaderSelfFirst;

    /**
     * Name of the class that implements the javax.jbi.component.Component
     * interface.
     */
    private String mClassName;

    /**
     * List of class path elements as relative paths based off of the component
     * install root (mInstallRoot).
     */
    private java.util.List mClassPathElements;

    /**
     * Flag to determine whether the component class loader should use an
     * inverted (self-first) hierarchy or the normal (parent-first) hierarchy.
     * Set to true for self-first, false for parent-first.
     */
    private boolean mComponentClassLoaderSelfFirst;

    /**
     * Unique component name.
     */
    private String mComponentName;

    /**
     * Component type (BINDING or ENGINE).
     */
    private int mComponentType;

    /**
     * Handle to the ComponentContext.
     */
    private javax.jbi.component.ComponentContext mContext;

    /**
     * Description of this component.
     */
    private String mDescription;

    /**
     * Installation root directory for this component.
     */
    private String mInstallRoot;

    /**
     * Flag set to true for installation or false for uninstallation.
     */
    private boolean mIsInstall;

    /**
     * StringTranslator for constructing messages.
     */
    private StringTranslator mTranslator;

    /**
     * Workspace root directory for this component.
     */
    private String mWorkspaceRoot;

    /**
     * DOM document fragment representing the installation descriptor (jbi.xml)
     * extension data for the component, if any.
     */
    private org.w3c.dom.DocumentFragment mInstallationDescriptorExtension;

    /**
     * Public constructor.
     * @param componentName the unique component name.
     * @param componentType the type of this component - either BINDING or
     * ENGINE.
     * @param className the name of the class the implements the
     * javax.jbi.component.Component interface for this component.
     * @param classPathElements the list of elements that comprise the
     * runtime class path for this component. NOTE: The elements in this list
     * are relative paths based off the component's install root.
     * @param extensionData the DOM document fragment representing the contents
     * of the installation descriptor extension from the jbi.xml file.
     */
    public ComponentInstallationContext(
        String componentName,
        int componentType,
        String className,
        List classPathElements,
        org.w3c.dom.DocumentFragment extensionData)
    {
        mComponentName = componentName;
        mComponentType = componentType;
        mClassName = className;
        mClassPathElements = classPathElements;
        mInstallationDescriptorExtension = extensionData;

        // Initialize other attributes

        mBootstrapClassLoaderSelfFirst = false;
        mComponentClassLoaderSelfFirst = false;
        mIsInstall = false;
        mTranslator = new StringTranslator(
            this.getClass().getPackage().getName(), null);
    }

// ----------- Methods from javax.jbi.component.InstallationContext -----------

    /**
     * Get a list of elements that comprise the class path for this component.
     * Each element represents either a directory (containing class files) or a
     * jar file. The elements are relative paths based off of the component's
     * install root, and are returned with the File.separator character as their
     * file separators.
     * @return a list of String objects containing class path elements.
     */
    public List getClassPathElements()
    {
        if ( File.separator.equals("/") )
        {
            return mClassPathElements;
        }
        else
        {
            List retList = new ArrayList();
            Iterator i = mClassPathElements.iterator();
            while ( i.hasNext() )
            {
                String e = ((String) i.next()).replace('/', File.separatorChar);
                retList.add(e);
            }
            return retList;
        }
    }

    /**
     * Get the name of the class that implements javax.jbi.component.Component
     * for this component.
     * @return the class name.
     */
    public String getComponentClassName()
    {
        return mClassName;
    }

    /**
     * Get the unique name assigned to this component.
     * @return the unique component name.
     */
    public String getComponentName()
    {
        return mComponentName;
    }

    /**
     * Get the component context.
     * @return the component context.
     */
    public javax.jbi.component.ComponentContext getContext()
    {
        return mContext;
    }

    /**
     * Return a DOM document fragment representing the installation descriptor
     * (jbi.xml) extension data for this component, if any is present.
     * @return the DOM document fragment for the extension data.
     */
    public org.w3c.dom.DocumentFragment getInstallationDescriptorExtension()
    {
        return mInstallationDescriptorExtension;
    }

    /**
     * Get the installation root directory for this component.
     * @return the name of the installation root directory, with the levels
     * separated by the local File.separator character.
     */
    public String getInstallRoot()
    {
        if ( File.separator.equals("/") )
        {
            return mInstallRoot;
        }
        else
        {
            return mInstallRoot.replace('/', File.separatorChar);
        }
    }

    /**
     * Return an indication as to whether this context is for an install or
     * an uninstall.
     * @return true for an install, false for an uninstall.
     */
    public boolean isInstall()
    {
        return mIsInstall;
    }

    /**
     * Set the list of elements that comprise the class path for this component.
     * Each element represents either a directory (containing class files) or a
     * jar file. This method is provided to allow the component's installer to
     * modify the list of class path elements. Each element must be formatted
     * using the File.separator character, and must be a relative path based
     * on the component's install root.
     * @param classPathElements a list of String objects containing class path
     * elements.
     */
    public void setClassPathElements(List classPathElements)
    {
        if ( null == classPathElements )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.IC_NULL_CLASS_PATH_ELEMENTS));
        }
        if ( classPathElements.isEmpty() )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.IC_EMPTY_CLASS_PATH_ELEMENTS));
        }

        // Validate each element in the list. No absolute paths are allowed,
        // and the paths must use the File.separator character in compliance
        // with the JBI spec.

        Iterator i = classPathElements.iterator();
        int index = 0;
        while ( i.hasNext() )
        {
            String element = (String) i.next();
            File f = new File(element);
            if ( f.isAbsolute() )
            {
                throw new IllegalArgumentException(mTranslator.getString(
                    LocalStringKeys.IC_ABSOLUTE_PATH_NOT_ALLOWED,
                    new Integer(index), element));
            }
            String sep = File.separator.equals("\\") ? "/" : "\\";
            int offset = element.indexOf(sep);
            if ( offset > -1 )
            {
                throw new IllegalArgumentException(mTranslator.getString(
                    LocalStringKeys.IC_INCORRECT_FILE_SEPARATOR,
                    sep, new Integer(offset), new Integer(index), element));
            }

            // This looks stupid, but inside Appserver all file paths use the
            // Unix-style file separator.

            element.replace('\\', '/');
            index++;
        }

        mClassPathElements = classPathElements;
    }

// ---------- Methods from com.sun.jbi.component.InstallationContext ----------

    /**
     * Get a list of elements that comprise the class path for this component.
     * Each element represents either a directory (containing class files) or a
     * jar file. The elements are absolute paths and are returned with a forward
     * slash as their file separators.
     * @return a list of String objects containing class path elements.
     */
    public List getAbsoluteClassPathElements()
    {
        List retList = new ArrayList();
        Iterator i = mClassPathElements.iterator();
        while ( i.hasNext() )
        {
            String e = mInstallRoot + "/" + (String) i.next();
            retList.add(e);
        }
        return retList;
    }

    /**
     * Return the description of this component.
     * @return the description of this component.
     */
    public String getDescription()
    {
        return mDescription;
    }

    /**
     * Return the workspace root directory for the component.
     * @return the workspace root directory path.
     */
    public String getWorkspaceRoot()
    {
        return mWorkspaceRoot;
    }

    /**
     * Return an indication as to whether the component being installed is a
     * Binding Component.
     * @return true if the component is a BC, false if not.
     */
    public boolean isBinding()
    {
        return (BINDING == mComponentType);
    }

    /**
     * Return an indication as to whether the bootstrap class loader should
     * use the normal hierarchy (parent-first) or an inverted hierarchy
     * (self-first).
     * @return true if the bootstrap class loader should use the self-first
     * hierarchy, false if it should use parent-first.
     */
    public boolean isBootstrapClassLoaderSelfFirst()
    {
        return mBootstrapClassLoaderSelfFirst;
    }

    /**
     * Return an indication as to whether the component class loader should
     * use the normal hierarchy (parent-first) or an inverted hierarchy
     * (self-first).
     * @return true if the component class loader should use the self-first
     * hierarchy, false if it should use parent-first.
     */
    public boolean isComponentClassLoaderSelfFirst()
    {
        return mComponentClassLoaderSelfFirst;
    }

    /**
     * Return an indication as to whether the component being installed is a
     * Service Engine.
     * @return true if the component is a SE, false if not.
     */
    public boolean isEngine()
    {
        return (ENGINE == mComponentType);
    }

    /**
     * Set the context for this component. This method is used by the
     * Component Framework to set the context during installation processing.
     * @param context the component context.
     */
    public void setContext(javax.jbi.component.ComponentContext context)
    {
        mContext = context;
    }

// ------------------- Methods local to this implementation -------------------

    /**
     * Set a flag to force the bootstrap class loader to use an inverted
     * hierarchy (self-first).
     */
    public void setBootstrapClassLoaderSelfFirst()
    {
        mBootstrapClassLoaderSelfFirst = true;
    }

    /**
     * Set a flag to force the component class loader to use an inverted
     * hierarchy (self-first).
     */
    public void setComponentClassLoaderSelfFirst()
    {
        mComponentClassLoaderSelfFirst = true;
    }

    /**
     * Set the name of the class that implements javax.jbi.component.Component
     * for this component.
     * @param name the component class name of this component.
     */
    public void setComponentClassName(String name)
    {
        mClassName = name;
    }

    /**
     * Set the name of this component.
     * @param name the name of this component.
     */
    public void setComponentName(String name)
    {
        mComponentName = name;
    }

    /**
     * Set the description of this component.
     * @param description the description of this component.
     */
    public void setDescription(String description)
    {
        mDescription = description;
    }

    /**
     * Set the installation descriptor extension.
     * @param extension the extension portion of the component's installation
     * descriptor.
     */
    public void setInstallationDescriptorExtension(
        org.w3c.dom.DocumentFragment extension)
    {
        mInstallationDescriptorExtension = extension;
    }

    /**
     * Set the installation root directory name for this component.
     * @param installRoot the installation root directory name.
     */
    public void setInstallRoot(String installRoot)
    {
        mInstallRoot = installRoot;
    }

    /**
     * Set the flag to indicate whether this is an install or an uninstall.
     * @param isInstall is true for an install, false for an uninstall.
     */
    public void setIsInstall(boolean isInstall)
    {
        mIsInstall = isInstall;
    }

    /**
     * Set the workspace root directory name for this component.
     * @param workspaceRoot the workspace root directory name.
     */
    public void setWorkspaceRoot(String workspaceRoot)
    {
        mWorkspaceRoot = workspaceRoot;
    }
}
