/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DirectoryUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;

import java.io.File;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 *  Provides utility methods for common file system tasks.
 *
 * @author Sun Microsystems, Inc.
 */
public class DirectoryUtil
{  
    /**
     * file that gets created when a directory is not successfully removed
     */
    static final String DELETE_ME_FILE = ".DELETE_ME";
    
	private static Logger mLogger = Logger.getLogger(DirectoryUtil.class.getName());
    /**
     * Deletes directory including the sub directories.
     *
     * @param dirName Name of the directory to delete
     * @throws Exception If failed to delete directory
     */
    public static void deleteDir(String dirName) throws Exception
    {
        boolean success = true;
        
        File dir = new File(dirName);
        
        if (dir.isDirectory())
        {
            for (String child : dir.list())
            {
                String absChildName = dirName + File.separator + child;
                File chldFile = new File (absChildName);
                if (chldFile.isDirectory ())
                {
                    deleteDir(absChildName);
                }
                else
                {
                    success = chldFile.delete();
                }
                
                if (!success)
                {
                    throw new Exception("COULD_NOT_DELETE_FILE");
                }
            }
        }
        
        // The directory is now empty so delete it
        if (!dir.delete ())
        {
            throw new Exception ("COULD_NOT_DELETE_FILE");
        }
    }
    
    /** Complements deleteDir, but provides a more comprehensive contract
     *  for removal of a directory.  If the deletion fails at any stage
     *  (parent or child), the top-level directory is marked for deletion
     *  at a subsequent time with a .DELETE_ME file.
     */
    public static boolean removeDir(String dirName)
    {
        boolean success = false;
        
        try
        {
            deleteDir(dirName);
            success = true;
        }
        catch (Exception ex)
        {
            markDir(dirName);
        }
        
        return success;
    }
    
    /** Mark a directory for deletion by adding a .DELETE_ME file */
    public static void markDir(String dirName)
    {
        File dir;
        File deleteMe;
        
        try
        {
            dir = new File(dirName);
            
            if (dir.isDirectory())
            {
                deleteMe = new File(dir, DELETE_ME_FILE);
                deleteMe.createNewFile();
            }
        }
        catch (java.io.IOException ioEx)
        {
        	mLogger.log(Level.WARNING, ioEx.getClass().getName(), ioEx);
        }
    }
    
    /** Finds all marked child directories of parent and deletes them.
     */
    public static void removeMarkedDirs(String root)
    {
        ArrayList markedDirs = new ArrayList();        
        
        findMarkedDirs(new File(root), markedDirs);
        
        for (int i = 0; i < markedDirs.size(); i++)
        {
            File dir = (File)markedDirs.get(i);
            try
            {
                deleteDir(dir.getAbsolutePath());
            }
            catch (Exception ex)
            {
            	mLogger.log(Level.WARNING, ex.getClass().getName(), ex);
            }
        }
    }
    
    /**
     * Util method to get the list of folders.
     * @param aDir folder name for which contents have to be listed.
     * @return array of folder names.
     */
    public static String[] getFolderNameListing(File aDir)
    {
        ArrayList<String> folderList = new ArrayList<String>();
        
        if (aDir == null || !aDir.isDirectory())
        {
            return null;
        }
        
        for (File file : aDir.listFiles())
        {
            if (file.isDirectory())
            {
                folderList.add(file.getName());
            }
        }
        
        return folderList.toArray(new String[] {});
    }   
    
    /** Determies if a given directory has been marked for deletion.
     */
    public static boolean isMarked(File dir)
    {
        File[]  files;
        boolean isMarked = false;
        
        if (dir == null || !dir.isDirectory())
        {
            return false;
        }
        
        files = dir.listFiles();
        for (int i = 0; i < files.length; i++)
        {
            if (files[i].isFile() && files[i].getName().equals(DELETE_ME_FILE))
            {
                isMarked = true;
                break;
            }
        }
        
        return isMarked;
    }
    
    /** Returns a list of files within the specified directory that start with
     *  the specified prefix.
     */
    public static File[] listFiles(File root, Pattern prefix)
    {
        return root.listFiles(new FilePrefixFilter(prefix));
    }
    
    /** Adds all marked directories found underneath root to the list.
     */
    private static void findMarkedDirs(File root, List markedDirs)
    {
        File[] children;
        
        // If the root directory is marked for deletion, no need to traverse
        // it's children (they will be deleted too)
        if (isMarked(root))
        {
            markedDirs.add(root);
            return;
        }
        
        // Recurse through children
        children = root.listFiles();
        for (int i = 0; i < children.length; i++)
        {
            if (children[i].isDirectory())
            {
                findMarkedDirs(children[i], markedDirs);
            }
        }
    }
}

/** Matches all files that start with the specified prefix.
 */
class FilePrefixFilter implements java.io.FileFilter
{
    private Pattern mPrefix;
    
    FilePrefixFilter(Pattern prefix)
    {
        mPrefix = prefix;
    }
    
    public boolean accept(File file)
    {
        Matcher matcher = mPrefix.matcher(file.getName());
        return matcher.matches();
    }
}
