/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationServiceMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import javax.management.ObjectName;

/**
 * ConfigurationServiceMBean defines the interface that must be implemented by
 * the ConfigurationService in a JBI Framework.  The ConfigurationService provides a
 * mangement interface for setting configuration parameters for one or more
 * system services or installable components.  It also provides access to
 * JBI Framework configuration parameters.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ConfigurationServiceMBean
{

    /**
     * Lookup a system Configuration MBean by system service type. For each service type
     * there are two instance MBeans the target specific instance configuration MBean
     * and the global configuration instance MBean.
     *
     * @param aSvcName is the name of the system service
     * @return the JMX object name array of the service Configuration MBean names.
     */
    ObjectName[] getSystemConfigMBean(String aSvcName);
    
    /**
     * Looks up all the instance ConfigurationMBeans registered for the JBI system services.
     *
     * This returns all the instance configuration MBeans : instance configuration 
     * and global instance configuration MBeans
     * </br>
     *   ObjectName pattern is : 
     * </br>
     *       com.sun.jbi:ComponentType=System, ServiceName=ConfigurationService
     *
     *
     * @return array of object names for all system service ConfigurationMBeans.
     * @return zero-length array if no services registered.
     */
    ObjectName[] getSystemConfigMBeans();
}
