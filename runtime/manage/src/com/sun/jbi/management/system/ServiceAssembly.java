/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssembly.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ServiceAssembly.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on April 22, 2005, 3:01 PM
 */

package com.sun.jbi.management.system;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.internal.support.JarURLHelper;

import java.io.InputStream;
import java.net.URL;

import java.util.logging.Logger;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;

import javax.jbi.management.DeploymentException;


/**
 * This is a wrapper class  which encapulates the Service Assembly.
 *
 * @author Sun Microsystems Inc.
 */
public class ServiceAssembly
{
    /**
     * URL to the SA, this could be based on the file, http or jar protocol.
     */
    private URL mSaURL;
    
    /**
     * The Archive.
     */
    private ZipFile mZipFile = null;
    
    /**
     * Manangement Message Builder.
     */
    private BuildManagementMessageImpl mMImpl = null;
    
    /**
     * The StringTranslator.
     */
    private StringTranslator mTranslator = null;
    
    /**
     * The Logger.
     */
    private Logger mLogger = null;

    /** 
     * Creates a new instance of ServiceAssembly. This constructor creates a Service 
     * Assembly object for a Service Assembly being deployed.
     *
     * @param saURL is the URL for the ServiceAssembly.
     * @param translator is the StringTranslator.
     * @param logger is the DeploymentService Logger.
     * @throws java.io.IOException on IO related errors.
     * @throws DeploymentException if the ZipFile cannot be read.
     */
    public ServiceAssembly(URL saURL, StringTranslator translator, Logger logger)
        throws java.io.IOException, 
            DeploymentException
    {
        mLogger = logger;
        mTranslator = translator;
        mSaURL = JarURLHelper.convertToJarURL(saURL, mTranslator);
        mMImpl = new BuildManagementMessageImpl();
        mZipFile = JarURLHelper.getZipFileFromURL(mSaURL, mTranslator);
        
    }
    
    /**
     * @return the ZipFile.
     */
    public ZipFile getZipFile()
    {
        return mZipFile;
    }
    
    /**
     * Get the Zip URL.
     *
     * @return the URL to the Service Assembly Zip file
     */
    public URL getZipURL()
    {
        return mSaURL;
    }
    
    /**
     * @return a stream to the jbi.xml
     * @throws DeploymentException if the jbi.xml deployment descriptor is not found 
     * in the Service Assembly. 
     * @throws java.io.IOException on IO errors
     */
    public InputStream getJbiXml()
        throws DeploymentException, java.io.IOException
    {                        
        InputStream is = null;
        boolean hasJbiXML = false;
        java.util.Enumeration e = mZipFile.entries ();
        while (e.hasMoreElements ())
        {
            ZipEntry ze = (ZipEntry) e.nextElement();
            if (ze.getName().equalsIgnoreCase ("META-INF/jbi.xml"))
            {
                is = mZipFile.getInputStream (ze);
                hasJbiXML = true;
            }
        }

        if (!hasJbiXML)
        {
            ManagementMessageHolder 
                mmHolder = new ManagementMessageHolder("EXCEPTION_MSG");
            mmHolder.setTaskName ("readjbiXmlFromSAZip");
            mmHolder.setExceptionObject (
                new Exception("Cannot find deployment descriptor"));
            mmHolder.setLocToken (1, "JBI_XML_NOT_FOUND_IN_SA_JAR");
            String jbiTaskStr = mMImpl.buildCompleteExceptionMessage (mmHolder);
            throw new DeploymentException (jbiTaskStr);
        }
        return is;
    }
    
    /**
     * The Service Assembly is no longer needed, do any cleanup here.
     */
    public void done()
    {
        try
        {
            if ( mZipFile != null )
            {
                mZipFile.close();
            }
        }
        catch (java.io.IOException ioex)
        {
            // -- cannot do much here.
            mLogger.finest("Could not close the Service Assembly zip file " +
                getZipURL().toString());
        }
    }
}
