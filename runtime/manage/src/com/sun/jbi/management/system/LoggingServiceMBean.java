/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LoggingServiceMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import javax.management.ObjectName;

/**
 * LoggingServiceMBean defines the interface that must be implemented by
 * the LoggingService in a JBI Framework.  The LoggingService provides a
 * mangement interface for setting logging parameters for one or more
 * system services or installable components.  It also provides access to
 * JBI Framework logging parameters.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LoggingServiceMBean
{
    /**
     * Lookup a system LoggerMBean by system service name.
     * @param aSvcName is the name of the system service
     * @return the JMX object name of the service LoggerMBean or null
     */
    ObjectName getSystemLoggerMBean(String aSvcName);

    /**
     * Looks up LoggerMBeans for all JBI Framework
     * System Services currently installed.
     * @return array of object names for all system service LoggerMBeans.
     * @return zero-length array if no services registered.
     */
    ObjectName[] getSystemLoggerMBeans();

    /**
     * lookup a JBI Installable Component by its unique ID.
     * @param aComponentId is the unique ID of the BC or SE.
     * @return the JMX object name of the LifeCycle MBean for the component
     */
    ObjectName getComponentLoggerMBeanById(String aComponentId);
}
