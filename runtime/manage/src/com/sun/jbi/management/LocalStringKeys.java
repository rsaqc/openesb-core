/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

/**
 * This interface contains the property keys used for looking up message
 * text in the LocalStrings resource bundle.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{
    //
    // Message keys for common words or phrases
    //

    /**
     * Name of the autodeploy directory.
     */
    String AA_APPLY_DEPLOY_NAME =
          "AA_APPLY_DEPLOY_NAME";

    /**
     * Name of the autoinstall directory.
     */
    String AA_APPLY_INSTALL_NAME =
          "AA_APPLY_INSTALL_NAME";

    /**
     * Name of the top level ("drop"/spool) directory.
     */
    String AA_APPLY_TOP_NAME =
          "AA_APPLY_TOP_NAME";

    /**
     * Binding.
     */
    String BINDING =
          "BINDING";
    /**
     * Engine.
     */
    String ENGINE =
          "ENGINE";
    /**
     * Unknown.
     */
    String UNKNOWN =
          "UNKNOWN";

    //
    // Message keys for common messages
    //

    /**
     * Info message - method entered.
     */
    String METHOD_ENTERED =
          "METHOD_ENTERED";
    /**
     * Info message - method exited.
     */
    String METHOD_EXITED =
          "METHOD_EXITED";

    //
    // Message keys for JBIFramework
    //

    /**
     * Error message - designate an error.
     */
    String ERROR =
          "ERROR";

    /**
     * Info message - designate an information message.
     */
    String INFO =
          "INFO";

    /**
     * Warning message - designate a warning.
     */
    String WARNING =
          "WARNING";

    // error messages

    //
    // Message keys for Autodeploy/Autoinstall task
    //

    /**
     *  Autoinstall of service assemblies not supported.
     */
    String AA_DEPLOY_NOT_SUPPORTED =
            "AA_DEPLOY_NOT_SUPPORTED";
    /**
     *  Autodeploy of shared libraries or components not supported.
     */
    String AA_INSTALL_NOT_SUPPORTED =
            "AA_INSTALL_NOT_SUPPORTED";

    //
    // Message keys for Admin Service
    //

    /**
     * Lookup failure in getComponentById.
     */
    String AS_GETCOMPONENTBYID_LOOKUP_FAILED =
          "AS_GETCOMPONENTBYID_LOOKUP_FAILED";

    /**
     * Too many MBeans returned in getComponentById.
     */
    String AS_GETCOMPONENTBYID_TOO_MANY_MBEANS =
          "AS_GETCOMPONENTBYID_TOO_MANY_MBEANS";

    /**
     * Lookup failure in getComponentByName.
     */
    String AS_GETCOMPONENTBYNAME_LOOKUP_FAILED =
          "AS_GETCOMPONENTBYNAME_LOOKUP_FAILED";

    /**
     * Lookup failure in getInstalledComponents.
     */
    String AS_GETINSTALLEDCOMPONENTS_FAILURE =
          "AS_GETINSTALLEDCOMPONENTS_FAILURE";

    /**
     * Exception occured in getRemoteFileUploadURL.
     */
    String AS_GETREMOVEFILEUPLOADURL_EXCEP =
          "AS_GETREMOVEFILEUPLOADURL_EXCEP";

    /**
     * Lookup failure in getSystemService.
     */
    String AS_GETSYSTEMSERVICE_LOOKUP_FAILED =
          "AS_GETSYSTEMSERVICE_LOOKUP_FAILED";

    /**
     * No system services found.
     */
    String AS_GETSYSTEMSERVICE_NO_SERVICES =
          "AS_GETSYSTEMSERVICE_NO_SERVICES";

    /**
     * Too many MBeans in getSystemService.
     */
    String AS_GETSYSTEMSERVICE_TOO_MANY_MBEANS =
          "AS_GETSYSTEMSERVICE_TOO_MANY_MBEANS";

    /**
     * Could not find heartbeat MBean.
     */
    String AS_HEARTBEAT_NULL =
          "AS_HEARTBEAT_NULL";

    /**
     * Starting up the heartbeat MBean.
     */
    String AS_HEARTBEAT_START =
          "AS_HEARTBEAT_START";

    /**
     * Stopping the heartbeat MBean.
     */
    String AS_HEARTBEAT_STOP =
          "AS_HEARTBEAT_STOP";

    /**
     * Exception occured in queryHelper.
     */
    String AS_QUERYHELPER_EXCEP =
          "AS_QUERYHELPER_EXCEP";

    /**
     * Status message during AdminService.start().
     */
    String AS_START_SERVICE =
          "AS_START_SERVICE";

    //
    // Message keys for Configuration Service
    //

    /**
     * No system service configuration MBeans found.
     */
    String CS_GETSYSTEMCONFIGMBEANS_NO_SERVICES =
          "CS_GETSYSTEMCONFIGMBEANS_NO_SERVICES";
    
    /**
     * Invalid attribute value 
     */
    String CS_INVALID_ATTRIBUTE_VALUE =
        "CS_INVALID_ATTRIBUTE_VALUE";
    
    String CS_SET_ATTRIBUTE_FAILED
        = "CS_SET_ATTRIBUTE_FAILED";
    
    
    String CS_GET_ATTRIBUTE_FAILED
        = "CS_GET_ATTRIBUTE_FAILED";
    
    String CS_DESCR_REQ_FIELD_MISSING =
        "CS_DESCR_REQ_FIELD_MISSING";
    
    /**
     * Invalid attribute descriptor
     */ 
    String CS_ATTRIBUTE_DESCR_INVALID =
        "CS_ATTRIBUTE_DESCR_INVALID";
    
    /**
     * Null attribute descriptor
     */ 
    String CS_ATTRIBUTE_DESCR_NULL =
        "CS_ATTRIBUTE_DESCR_NULL";
    
    //
    // Message keys for Deployment Service
    //

    /**
     * SA did not deploy.
     */
    String DS_DEPLOYMENT_FAILURE =
          "DS_DEPLOYMENT_FAILURE";

    /**
     * SA did deploy.
     */
    String DS_DEPLOYMENT_SUCCESS =
          "DS_DEPLOYMENT_SUCCESS";

    /**
     * Deploy error, but normal exception was not created.
     */
    String DS_DEPLOY_NO_EXCEPTION_MSG =
          "DS_DEPLOY_NO_EXCEPTION_MSG";

    /**
     * SA did not undeploy.
     */
    String DS_UNDEPLOYMENT_FAILURE =
          "DS_UNDEPLOYMENT_FAILURE";

    /**
     * SA did undeploy.
     */
    String DS_UNDEPLOYMENT_SUCCESS =
          "DS_UNDEPLOYMENT_SUCCESS";

    /**
     * getSADescriptor error, Cannot find descriptor.
     */
    String DS_SA_DESCRIPTOR_NOT_FOUND =
          "DS_SA_DESCRIPTOR_NOT_FOUND";
    
    /**
     * Cannot deploy service unit
     */
    String DS_CANNOT_DEPLOY_SERVICE_UNIT_COMP_NOT_INSTALLED =
          "DS_CANNOT_DEPLOY_SERVICE_UNIT_COMP_NOT_INSTALLED";
    
    /**
     * Cannot deploy service unit
     */
    String DS_CANNOT_DEPLOY_SERVICE_UNIT_COMP_NOT_STARTED =
          "DS_CANNOT_DEPLOY_SERVICE_UNIT_COMP_NOT_STARTED";
    
    /**
     * Cannot deploy service assembly
     */
    String DS_CANNOT_DEPLOY_SERVICE_ASSEMBLY =
          "DS_CANNOT_DEPLOY_SERVICE_ASSEMBLY";
    
    /**
     * Cannot undeploy service assembly
     */
    String DS_CANNOT_UNDEPLOY_SERVICE_ASSEMBLY =
          "DS_CANNOT_UNDEPLOY_SERVICE_ASSEMBLY";
    
    /**
     * Cannot get service unit list.
     */
    String DS_CANNOT_GET_SERVICE_UNIT_LIST =
          "DS_CANNOT_GET_SERVICE_UNIT_LIST";
    
    /**
     * Cannot change SU state when component is in incorrect state.
     */
    String DS_CANNOT_CHANGE_SU_STATE_COMP_IN_INCORRECT_STATE =
        "DS_CANNOT_CHANGE_SU_STATE_COMP_IN_INCORRECT_STATE";
    
    /**
     * Unable to find lifecycle mbean object name.
     */
    String DS_UNABLE_TO_FIND_LIFECYCLE_MBEAN_OBJECT_NAME =
          "DS_UNABLE_TO_FIND_LIFECYCLE_MBEAN_OBJECT_NAME";
    
    /**
     * Unable to find deployed service assembly names.
     */
    String DS_UNABLE_TO_FIND_DEPLOYED_SERVICE_ASSEMBLY_NAMES =
          "DS_UNABLE_TO_FIND_DEPLOYED_SERVICE_ASSEMBLY_NAMES";
    
    /**
     * Unable to find service assembly names for component.
     */
    String DS_UNABLE_TO_GET_SERVICE_ASSEMBLY_NAMES_FOR_COMPONENT =
          "DS_UNABLE_TO_GET_SERVICE_ASSEMBLY_NAMES_FOR_COMPONENT";
    
    /**
     * Unable to find component names for service assembly.
     */
    String DS_UNABLE_TO_FIND_COMPONENT_NAMES_FOR_SA =
          "DS_UNABLE_TO_FIND_COMPONENT_NAMES_FOR_SA";
    
    /**
     * Cannot undeploy service unit
     */
    String DS_CANNOT_UNDEPLOY_SERVICE_UNIT_COMP_NOT_INSTALLED =
          "DS_CANNOT_UNDEPLOY_SERVICE_UNIT_COMP_NOT_INSTALLED";

    /**
     * Service unit registration failed
     */
    String DS_REGISTER_SERVICE_UNIT_FAILED =
          "DS_REGISTER_SERVICE_UNIT_FAILED";
    
    /**
     * Service unit unregistration failed
     */
    String DS_UNREGISTER_SERVICE_UNIT_FAILED =
          "DS_UNREGISTER_SERVICE_UNIT_FAILED";
    
    /**
     * Service Assembly state change failed
     */
    String DS_CANNOT_CHANGE_SERVICE_ASSEMBLY_STATE =
          "DS_CANNOT_CHANGE_SERVICE_ASSEMBLY_STATE";
    
    /**
     * Service Unit state change failed
     */
    String DS_CANNOT_CHANGE_SU_STATE_COMP_NOT_INSTALLED =
          "DS_CANNOT_CHANGE_SU_STATE_COMP_NOT_INSTALLED";
    
    /**
     * Unable to get SA Information
     */
    String DS_CANNOT_GET_SA_INFO_INVALID_SA_NAME =
          "DS_CANNOT_GET_SA_INFO_INVALID_SA_NAME";
    
    /**
     * Unable to get connections info.
     */
    String DS_UNABLE_TO_GET_CONNECTIONS_INFO =
          "DS_UNABLE_TO_GET_CONNECTIONS_INFO";
    
    /**
     * Unable to activate connections
     */
    String DS_UNABLE_TO_ACTIVATE_CONNECTION =
          "DS_UNABLE_TO_ACTIVATE_CONNECTION";
    
    /**
     * Unable to deactivate connections
     */
    String DS_UNABLE_TO_DEACTIVATE_CONNECTION =
          "DS_UNABLE_TO_DEACTIVATE_CONNECTION";
    
   /**
     * Unable to get link type
     */
    String DS_UNABLE_TO_GET_LINK_TYPE_FROM_SU_DESCRIPTOR =
          "DS_UNABLE_TO_GET_LINK_TYPE_FROM_SU_DESCRIPTOR";

    /**
     * Unable to start service assembly. Invalid SA state.
     */
    String DS_CANNOT_STOP_SA_SHUTDOWN_SA_STATE =
          "DS_CANNOT_STOP_SA_SHUTDOWN_SA_STATE";
    
    /**
     * Unable to start service assembly. Invalid SA state.
     
    /**
     * Unable to deploy service assembly. SA zip URL is null.
     */
    String DS_CANNOT_DEPLOY_SA_ZIP_URL_NULL =
          "DS_CANNOT_DEPLOY_SA_ZIP_URL_NULL";
    
    /**
     * Warning message - directory not marked for deletion.
     */
    String DS_DIRECTORY_NOT_MARKED =
          "DS_DIRECTORY_NOT_MARKED";

    /**
     * Warning message - directory not deleted.
     */
    String DS_DIRECTORY_NOT_REMOVED =
          "DS_DIRECTORY_NOT_REMOVED";
    
    /**
     * debugging output -- this should be cleaned up asap.
     */
    String DS_DEBUG_OUTPUT =
          "DS_DEBUG_OUTPUT";
    
    /**
     * Error message - No JBIROOT found -- Cannot create Trash.
     */
    String DS_NO_TRASH =
          "DS_NO_TRASH";
    
    /**
     * Info message. SA deployment was successful.
     */
    String DS_DEPLOY_SCSS_INFO_MSG =
          "DS_DEPLOY_SCSS_INFO_MSG";
    
    
    /**
     * Warning message. SA deployment succeeded with warnings.
     */
    String DS_DEPLOY_SCSS_WITH_WRNING_MSG =
        "DS_DEPLOY_SCSS_WITH_WRNING_MSG";
    
    /**
     * Warning message. SA start succeeded with warnings.
     */
    String DS_START_SCSS_WITH_WRNING_MSG =
        "DS_START_SCSS_WITH_WRNING_MSG";
    
    /**
     * Service assembly with the same name and different content exists
     */
    String DS_SERVICE_ASSEMBLY_WITH_SAME_NAME_EXISTS =
          "DS_SERVICE_ASSEMBLY_WITH_SAME_NAME_EXISTS";
    
    /**
     * Service unit has been deployed already
     */
    String DS_SERVICE_UNIT_ALREADY_DEPLOYED =
          "DS_SERVICE_UNIT_ALREADY_DEPLOYED";
    
    /**
     * Service unit has been started already
     */
    String DS_SERVICE_UNIT_ALREADY_STARTED =
          "DS_SERVICE_UNIT_ALREADY_STARTED";    
    
    /**
     * Unable to deploy service unit. Duplicate service unit contents.
     */
    String DS_CANNOT_DEPLOY_SERVICE_UNIT_DUPLICATE_SU_CONTENTS =
          "DS_CANNOT_DEPLOY_SERVICE_UNIT_DUPLICATE_SU_CONTENTS";
    
    /**
     * Unable to deploy service unit. Duplicate service unit contents.
     */
    String DS_CANNOT_DEPLOY_SERVICE_UNIT_DUPLICATE_SU_NAME =
          "DS_CANNOT_DEPLOY_SERVICE_UNIT_DUPLICATE_SU_NAME";
    
    /**
     * Unable to check service unit contents. 
     */
    String DS_CANNOT_CHECK_SU_CONTENTS =
          "DS_CANNOT_CHECK_SU_CONTENTS";
    
    /**
     * Unable to check service unit binary contents. 
     */
    String DS_FAILED_COMPARING_BINARY_SU_CONTENTS =
          "DS_FAILED_COMPARING_BINARY_SU_CONTENTS";
    
    /**
     * SU LC Op. Success
     */
    String DS_SU_LIFECYCLE_OP_SUCCESS = 
        "DS_SU_LIFECYCLE_OP_SUCCESS";
    
    static final String DS_SU_OP_FAILED =
        "DS_SU_OP_FAILED";
    
    /**
     * URL Protocol is not supported.
     */
    String UNSUPPORTED_URL_PROTOCOL = 
        "UNSUPPORTED_URL_PROTOCOL";
    
    static final String DS_CANNOT_GET_SA_STATE = 
        "DS_CANNOT_GET_SA_STATE";
    
    /** Deployer Instance is null */
    static final String DS_CANNOT_UNDEPLOY_SU_DEPLOYER_INSTANCE_NULL =
        "DS_CANNOT_UNDEPLOY_SU_DEPLOYER_INSTANCE_NULL";
    
    /** Deployer Instance is null */
    static final String DS_CANNOT_DEPLOY_SU_DEPLOYER_INSTANCE_NULL =
        "DS_CANNOT_DEPLOY_SU_DEPLOYER_INSTANCE_NULL";
    
    /**
     * Service Assembly startup error count.
     */
    static final String DS_SA_STARTUP_ERRORS =
        "DS_SA_STARTUP_ERRORS";

    /**
     * Service Assembly startup success count.
     */
    static final String DS_SA_STARTUP_SUCCESS =
        "DS_SA_STARTUP_SUCCESS";
    
    //
    // Message keys for Installation Service
    //

    /**
     * Warning message - component already installed.
     */
    String IS_COMPONENT_ALREADY_INSTALLED =
          "IS_COMPONENT_ALREADY_INSTALLED";

    /**
     * Warning message - component is not an engine or binding.
     */
    String IS_COMPONENT_IS_WRONG_TYPE =
          "IS_COMPONENT_IS_WRONG_TYPE";

    /**
     * Warning message - component not installed.
     */
    String IS_COMPONENT_NOT_INSTALLED =
          "IS_COMPONENT_NOT_INSTALLED";

    /**
     * debugging output -- this should be cleaned up asap.
     */
    String IS_DEBUG_OUTPUT =
          "IS_DEBUG_OUTPUT";

    /**
     * Error message - Framework exception during install.
     */
    String IS_FRAMEWORK_INSTALL =
          "IS_FRAMEWORK_INSTALL";

    /**
     * Error message - Framework exception during uninstall.
     */
    String IS_FRAMEWORK_UNINSTALL =
          "IS_FRAMEWORK_UNINSTALL";

    /**
     * Warning message - caught an IO Exception.
     */
    String IS_IO_EXCEP =
          "IS_IO_EXCEP";

    /**
     * Warning message - Jar file is null.
     */
    String IS_JAR_FILE_NULL =
          "IS_JAR_FILE_NULL";

    /**
     * Warning message - No jbi.xml file present in jar.
     */
    String IS_NO_JBI_XML_IN_JAR =
          "IS_NO_JBI_XML_IN_JAR";

    /**
     * Error message - No JBIROOT found -- Cannot create Trash.
     */
    String IS_NO_TRASH =
          "IS_NO_TRASH";

    /**
     * Warning message - URL Exception caught.
     */
    String IS_URL_EXCEP =
          "IS_URL_EXCEP";

    /**
     * Validation message - error in XML parser during schema validation.
     */
    String IS_VALIDATION_EXCEP =
          "IS_VALIDATION_EXCEP";
    
    /**
     * Failed to locate InstallerMBean for component. 
     */
    String IS_COMPONENT_NO_INSTALL_MBEAN =
            "IS_COMPONENT_NO_INSTALL_MBEAN";
    
    /**
     * Cannot delete component root of installed component.
     */
    String IS_COMPONENT_NOT_UNINSTALLED =
            "IS_COMPONENT_NOT_UNINSTALLED";
    
    /**
     * failed to delete component install root
     */    
    String IS_COMPONENT_UPGRADE_INSTALL_ROOT_NOT_DELETED =
        "IS_COMPONENT_UPGRADE_INSTALL_ROOT_NOT_DELETED";

    /**
     * failed to backup component install root
     */    
    String IS_COMPONENT_UPGRADE_INSTALL_ROOT_BACKUP_FAILED =
        "IS_COMPONENT_UPGRADE_INSTALL_ROOT_BACKUP_FAILED";

    /**
     * failed to create new component install root
     */      
    String IS_COMPONENT_UPGRADE_INSTALL_ROOT_NOT_CREATED =
        "IS_COMPONENT_UPGRADE_INSTALL_ROOT_NOT_CREATED";    

    /**
     * failed to recreate component workspace
     */         
    String IS_COMPONENT_UPGRADE_WORKSPACE_NOT_RESTORED =
        "IS_COMPONENT_UPGRADE_WORKSPACE_NOT_RESTORED";

    /**
     * failed to restore component install root
     */         
    String IS_COMPONENT_UPGRADE_INSTALL_ROOT_RESTORE_FAILED =
        "IS_COMPONENT_UPGRADE_INSTALL_ROOT_RESTORE_FAILED";
    
    /**
     * component upgrade successful
     */         
    String IS_COMPONENT_UPGRADE_SUCCESSFUL =
        "IS_COMPONENT_UPGRADE_SUCCESSFUL";
    
    /**
     * component upgrade failed
     */         
    String IS_COMPONENT_UPGRADE_FAILED =
        "IS_COMPONENT_UPGRADE_FAILED";    
    
    /**
     * component upgrade restoring logger settings failed
     */         
    String IS_COMPONENT_UPGRADE_LOGGER_NOT_RESTORED =
        "IS_COMPONENT_UPGRADE_LOGGER_NOT_RESTORED";        
    //
    // Message keys for Logging Service
    //

    /**
     * Lookup failure in getComponentLoggerMBeanById.
     */
    String LS_GETCOMPONENTLOGGERMBEANBYID_LOOKUP_FAILED =
          "LS_GETCOMPONENTLOGGERMBEANBYID_LOOKUP_FAILED";

    /**
     * Too many MBeans returned in getComponentLoggerMBeanById.
     */
    String LS_GETCOMPONENTLOGGERMBEANBYID_TOO_MANY_MBEANS =
          "LS_GETCOMPONENTLOGGERMBEANBYID_TOO_MANY_MBEANS";

    /**
     * Lookup failure in getSystemLoggerMBean.
     */
    String LS_GETSYSTEMLOGGERMBEAN_LOOKUP_FAILED =
          "LS_GETSYSTEMLOGGERMBEAN_LOOKUP_FAILED";

    /**
     * No system service logger MBeans found.
     */
    String LS_GETSYSTEMLOGGERMBEANS_NO_SERVICES =
          "LS_GETSYSTEMLOGGERMBEANS_NO_SERVICES";

    /**
     * Too many MBeans returned in getSystemLoggerMBean.
     */
    String LS_GETSYSTEMLOGGERMBEAN_TOO_MANY_MBEANS =
          "LS_GETSYSTEMLOGGERMBEAN_TOO_MANY_MBEANS";

    /**
     * Null class path elements passed to setClassPathElements.
     */
    String IC_NULL_CLASS_PATH_ELEMENTS =
          "IC_NULL_CLASS_PATH_ELEMENTS";

    /**
     * Empty class path elements passed to setClassPathElements.
     */
    String IC_EMPTY_CLASS_PATH_ELEMENTS =
          "IC_EMPTY_CLASS_PATH_ELEMENTS";

    /**
     * Class path elements passed to setClassPathElements contains an
     * absolute path.
     */
    String IC_ABSOLUTE_PATH_NOT_ALLOWED =
          "IC_ABSOLUTE_PATH_NOT_ALLOWED";

    /**
     * Class path elements passed to setClassPathElements contains invalid
     * file separators.
     */
    String IC_INCORRECT_FILE_SEPARATOR =
          "IC_INCORRECT_FILE_SEPARATOR";
    
    String DS_AUTOSTART_CONNECTION_PROCESSING_FAILED =
         "DS_AUTOSTART_CONNECTION_PROCESSING_FAILED";
    
    /*---------------------------------------------------------------------------------*\
     *                      Repository Keys                                            *
    \*---------------------------------------------------------------------------------*/
    
    String JBI_ADMIN_ARCHIVE_EXISTS =
        "JBI_ADMIN_ARCHIVE_EXISTS";
    
    String JBI_ADMIN_ARCHIVE_TYPE_MISMATCH =
        "JBI_ADMIN_ARCHIVE_TYPE_MISMATCH";
    
    String JBI_ADMIN_ARCHIVE_NOT_EXIST =
        "JBI_ADMIN_ARCHIVE_NOT_EXIST";
    
    String JBI_ADMIN_UPLOAD_ID_NOT_FOUND =
        "JBI_ADMIN_UPLOAD_ID_NOT_FOUND";
    
    String JBI_ADMIN_DOWNLOAD_ARCHIVE_DOES_NOT_EXIST = 
        "JBI_ADMIN_DOWNLOAD_ARCHIVE_DOES_NOT_EXIST";
    
    String JBI_ADMIN_DOWNLOAD_ID_NOT_FOUND =
        "JBI_ADMIN_DOWNLOAD_ID_NOT_FOUND";
    
    String JBI_ADMIN_ARCHIVE_REMOVAL_FAILED =
        "JBI_ADMIN_ARCHIVE_REMOVAL_FAILED";
    
    String JBI_ADMIN_FILE_DELETE_FAILED =
        "JBI_ADMIN_FILE_DELETE_FAILED";
    
    String JBI_ADMIN_REMOVING_COMPONENT_FROM_REPOSITORY = 
        "JBI_ADMIN_REMOVING_COMPONENT_FROM_REPOSITORY";
    
    String JBI_ADMIN_REMOVING_SHARED_LIBRARY_FROM_REPOSITORY = 
        "JBI_ADMIN_REMOVING_SHARED_LIBRARY_FROM_REPOSITORY";
    
    String JBI_ADMIN_REMOVING_SERVICE_ASSEMBLY_FROM_REPOSITORY = 
        "JBI_ADMIN_REMOVING_SERVICE_ASSEMBLY_FROM_REPOSITORY";
    
    String JBI_ADMIN_COMPONENT_MISSING_IN_REPOSITORY =
        "JBI_ADMIN_COMPONENT_MISSING_IN_REPOSITORY";
    
    String JBI_ADMIN_SHARED_LIBRARY_MISSING_IN_REPOSITORY =
        "JBI_ADMIN_SHARED_LIBRARY_MISSING_IN_REPOSITORY";
    
    String JBI_ADMIN_SERVICE_ASSEMBLY_MISSING_IN_REPOSITORY =
        "JBI_ADMIN_SERVICE_ASSEMBLY_MISSING_IN_REPOSITORY";

    
    /*---------------------------------------------------------------------------------*\
     *                      Registry Keys                                              *
    \*---------------------------------------------------------------------------------*/
    String JBI_ADMIN_UNKNOWN_TARGET =
        "JBI_ADMIN_UNKNOWN_TARGET";
    
    String JBI_ADMIN_DUPLICATE_TARGET =
        "JBI_ADMIN_DUPLICATE_TARGET";
    
    String JBI_ADMIN_CANNOT_ADD_DUPLICATE_SU =
        "JBI_ADMIN_CANNOT_ADD_DUPLICATE_SU";
    
    String JBI_ADMIN_INVALID_REGISTRY_OBJECT =
        "JBI_ADMIN_INVALID_REGISTRY_OBJECT";
    
    String JBI_ADMIN_REGISTRY_FILE_DELETE_FAILED =
        "JBI_ADMIN_REGISTRY_FILE_DELETE_FAILED";
    
    String JBI_ADMIN_REGISTRY_FILE_RENAME_FAILED =
        "JBI_ADMIN_REGISTRY_FILE_RENAME_FAILED";
    
    String JBI_ADMIN_FAILED_SERIALIZE_REGISTRY =
        "JBI_ADMIN_FAILED_SERIALIZE_REGISTRY";
    
    String JBI_ADMIN_REGISTRY_CORRUPT =
        "JBI_ADMIN_REGISTRY_CORRUPT";
    
    String JBI_ADMIN_REGISTRY_CREATION_FAILED =
        "JBI_ADMIN_REGISTRY_CREATION_FAILED";
    
    String JBI_ADMIN_REGISTRY_CREATE =
        "JBI_ADMIN_REGISTRY_CREATE";
    
    String JBI_ADMIN_REMOVING_COMPONENT_FROM_REGISTRY = 
        "JBI_ADMIN_REMOVING_COMPONENT_FROM_REGISTRY";
    
    String JBI_ADMIN_REMOVING_SHARED_LIBRARY_FROM_REGISTRY = 
        "JBI_ADMIN_REMOVING_SHARED_LIBRARY_FROM_REGISTRY";
    
    String JBI_ADMIN_REMOVING_SERVICE_ASSEMBLY_FROM_REGISTRY = 
        "JBI_ADMIN_REMOVING_SERVICE_ASSEMBLY_FROM_REGISTRY";
    
    String JBI_ADMIN_COMPONENT_MISSING_IN_REGISTRY =
        "JBI_ADMIN_COMPONENT_MISSING_IN_REGISTRY";
    
    String JBI_ADMIN_SHARED_LIBRARY_MISSING_IN_REGISTRY =
        "JBI_ADMIN_SHARED_LIBRARY_MISSING_IN_REGISTRY";
    
    String JBI_ADMIN_SERVICE_ASSEMBLY_MISSING_IN_REGISTRY =
        "JBI_ADMIN_SERVICE_ASSEMBLY_MISSING_IN_REGISTRY";
    
    String JBI_ADMIN_ATTEMPT_READ_LOCK =
        "JBI_ADMIN_ATTEMPT_READ_LOCK";
    
    String JBI_ADMIN_ACQUIRED_READ_LOCK =
        "JBI_ADMIN_ACQUIRED_READ_LOCK";
    
    String JBI_ADMIN_RELEASED_READ_LOCK =
        "JBI_ADMIN_RELEASED_READ_LOCK";
    
    String JBI_ADMIN_ATTEMPT_WRITE_LOCK =
        "JBI_ADMIN_ATTEMPT_WRITE_LOCK";
    
    String JBI_ADMIN_ACQUIRED_WRITE_LOCK =
        "JBI_ADMIN_ACQUIRED_WRITE_LOCK";
    
    String JBI_ADMIN_RELEASED_WRITE_LOCK =
        "JBI_ADMIN_RELEASED_WRITE_LOCK";
    
    String JBI_ADMIN_ATTEMPT_DOWNGRADE_LOCK =
        "JBI_ADMIN_ATTEMPT_DOWNGRADE_LOCK";
    
    String JBI_ADMIN_DOWNGRADED_LOCK =
        "JBI_ADMIN_DOWNGRADED_LOCK";
    
    String JBI_ADMIN_FAILED_ACQUIRE_READ_LOCK =
        "JBI_ADMIN_FAILED_ACQUIRE_READ_LOCK";
    
    String JBI_ADMIN_FAILED_ACQUIRE_WRITE_LOCK =
        "JBI_ADMIN_FAILED_ACQUIRE_WRITE_LOCK";
    
    String JBI_ADMIN_THREAD_FAILED_ACQUIRE_READ_LOCK =
        "JBI_ADMIN_THREAD_FAILED_ACQUIRE_READ_LOCK";
    
    String JBI_ADMIN_THREAD_FAILED_ACQUIRE_WRITE_LOCK =
        "JBI_ADMIN_THREAD_FAILED_ACQUIRE_WRITE_LOCK";
    
    String JBI_ADMIN_INVALID_REGISTRY_FORMAT =
        "JBI_ADMIN_INVALID_REGISTRY_FORMAT";
    
    String JBI_ADMIN_REGISTRY_USING_BKUP =
        "JBI_ADMIN_REGISTRY_USING_BKUP";
    
    String JBI_ADMIN_FAILED_JAXB_INIT =
        "JBI_ADMIN_FAILED_JAXB_INIT";
    
    String JBI_ADMIN_REGISTRY_CREATE_FAILED =
        "JBI_ADMIN_REGISTRY_CREATE_FAILED";
    
    /*---------------------------------------------------------------------------------*\
     *                      Validation Message Keys                                    *
    \*---------------------------------------------------------------------------------*/
    String JBI_ADMIN_INVALID_COMPONENT_ARCHIVE_TYPE =
        "JBI_ADMIN_INVALID_COMPONENT_ARCHIVE_TYPE";
    
    String JBI_ADMIN_EMPTY_BOOTSTRAP_CLASSPATH =
          "JBI_ADMIN_EMPTY_BOOTSTRAP_CLASSPATH";

    String JBI_ADMIN_EMPTY_LIFECYCLE_CLASSPATH =
          "JBI_ADMIN_EMPTY_LIFECYCLE_CLASSPATH";
    
    String JBI_ADMIN_MISSING_COMPONENT_BOOTSTRAP =
        "JBI_ADMIN_MISSING_COMPONENT_BOOTSTRAP";
    
    String JBI_ADMIN_MISSING_COMPONENT_LIFECYCLE =
          "JBI_ADMIN_MISSING_COMPONENT_LIFECYCLE";
    
    String JBI_ADMIN_EMPTY_SHARED_LIBRARY_CLASSPATH =
          "JBI_ADMIN_EMPTY_SHARED_LIBRARY_CLASSPATH";
    
    String JBI_ADMIN_INVALID_SHARED_LIBRARY_ARCHIVE_TYPE =
          "JBI_ADMIN_INVALID_SHARED_LIBRARY_ARCHIVE_TYPE";

    String JBI_ADMIN_INVALID_SERVICE_ASSEMBLY_ARCHIVE_TYPE =
          "JBI_ADMIN_INVALID_SERVICE_ASSEMBLY_ARCHIVE_TYPE";
    
    String JBI_ADMIN_ARCHIVE_EMPTY =
        "JBI_ADMIN_ARCHIVE_EMPTY";
    
    String JBI_ADMIN_NO_DESCRIPTOR_IN_ARCHIVE =
        "JBI_ADMIN_NO_DESCRIPTOR_IN_ARCHIVE";
    
    String JBI_ADMIN_ARCHIVE_NONEXISTENT =
        "JBI_ADMIN_ARCHIVE_NONEXISTENT";
    
    String JBI_ADMIN_ARCHIVE_DESCRIPTOR_NOT_SCHEMA_VALID =
        "JBI_ADMIN_ARCHIVE_DESCRIPTOR_NOT_SCHEMA_VALID";
    
    /* MBean Helper fails to create SystemServiceLoggerMBean */
    
    String MBEAN_HELPER_FAILED_OBJECT_CREATION =
            "MBEAN_HELPER_FAILED_OBJECT_CREATION";
    
    /* MBean Server in MBeanSet */
    
    String MBEANSET_MBEANSERVER_NULL_OP =
            "MBEANSET_MBEANSERVER_NULL_OP";
    String MBEANSET_OBJECTNAME_NULL =
            "MBEANSET_OBJECTNAME_NULL";
    String MBEANSET_MBEANSERVER_NULL_REG = 
        "MBEANSET_MBEANSERVER_NULL_REG";
    String MBEANSET_MBEANSERVER_NULL_UNREG = 
        "MBEANSET_MBEANSERVER_NULL_UNREG";
    
    /* ManagementService Messages*/
    
    String MNGMTSVC_CANNOT_CREATE_LOGGERMBEAN = 
            "MNGMTSVC_CANNOT_CREATE_LOGGERMBEAN";
    String MNGMTSVC_CANNOT_START_HTML_ADAPTOR = 
            "MNGMTSVC_CANNOT_START_HTML_ADAPTOR";
    
    //following strings are used in MessageHelper to format Management Message
    String TASK_ID = "TASK_ID";
    
    String EXCEPTION_INFO = "EXCEPTION_INFO";
    
    String COMPONENT_NAME = "COMPONENT_NAME";    
    
    /**
     * Installation configuration parameters.
     */
    
    // Component Timeout
    String COMPONENT_TIMEOUT_DISPLAY_NAME = 
        "COMPONENT_TIMEOUT_DISPLAY_NAME";
    
    String COMPONENT_TIMEOUT_DESCR = 
        "COMPONENT_TIMEOUT_DESCR";
    
    String COMPONENT_TIMEOUT_TOOLTIP = 
        "COMPONENT_TIMEOUT_TOOLTIP";
    
    // Installation Timeout
    String INSTALLATION_TIMEOUT_DISPLAY_NAME = 
        "INSTALLATION_TIMEOUT_DISPLAY_NAME";
    
    String INSTALLATION_TIMEOUT_DESCR = 
        "INSTALLATION_TIMEOUT_DESCR";
    
    String INSTALLATION_TIMEOUT_TOOLTIP = 
        "INSTALLATION_TIMEOUT_TOOLTIP"; 
    
    // Enable auto install
    String ENABLE_AUTO_INSTALL_DISPLAY_NAME =
        "ENABLE_AUTO_INSTALL_DISPLAY_NAME";
    
    String ENABLE_AUTO_INSTALL_DESCR =
        "ENABLE_AUTO_INSTALL_DESCR";
    
    String ENABLE_AUTO_INSTALL_TOOLTIP =
        "ENABLE_AUTO_INSTALL_TOOLTIP";
    
    // Enable auto uninstall
    String ENABLE_AUTO_UNINSTALL_DISPLAY_NAME =
        "ENABLE_AUTO_UNINSTALL_DISPLAY_NAME";
    
    String ENABLE_AUTO_UNINSTALL_DESCR =
        "ENABLE_AUTO_UNINSTALL_DESCR";
    
    String ENABLE_AUTO_UNINSTALL_TOOLTIP =
        "ENABLE_AUTO_UNINSTALL_TOOLTIP";
    
    // Enable auto reinstall
    String ENABLE_AUTO_REINSTALL_DISPLAY_NAME =
        "ENABLE_AUTO_REINSTALL_DISPLAY_NAME";
    
    String ENABLE_AUTO_REINSTALL_DESCR =
        "ENABLE_AUTO_REINSTALL_DESCR";
    
    String ENABLE_AUTO_REINSTALL_TOOLTIP =
        "ENABLE_AUTO_REINSTALL_TOOLTIP";
    
    String AUTO_INSTALL_DIR_DISPLAY_NAME =
        "AUTO_INSTALL_DIR_DISPLAY_NAME";
    
    String AUTO_INSTALL_DIR_DESCR =
        "AUTO_INSTALL_DIR_DESCR";
    
    String AUTO_INSTALL_DIR_TOOLTIP =
        "AUTO_INSTALL_DIR_TOOLTIP";
    
    /**
     * Deployment configuration parameters.
     */
    
    // Deployment Timeout
    String DEPLOYMENT_TIMEOUT_DISPLAY_NAME = 
        "DEPLOYMENT_TIMEOUT_DISPLAY_NAME";
    
    String DEPLOYMENT_TIMEOUT_DESCR = 
        "DEPLOYMENT_TIMEOUT_DESCR";
    
    String DEPLOYMENT_TIMEOUT_TOOLTIP = 
        "DEPLOYMENT_TIMEOUT_TOOLTIP";
    
    String SERVICE_UNIT_TIMEOUT_DISPLAY_NAME = 
        "SERVICE_UNIT_TIMEOUT_DISPLAY_NAME";
    
    String SERVICE_UNIT_TIMEOUT_DESCR = 
        "SERVICE_UNIT_TIMEOUT_DESCR";
    
    String SERVICE_UNIT_TIMEOUT_TOOLTIP = 
        "SERVICE_UNIT_TIMEOUT_TOOLTIP"; 
    
    // Enable auto deploy
    String ENABLE_AUTO_DEPLOY_DISPLAY_NAME =
        "ENABLE_AUTO_DEPLOY_DISPLAY_NAME";
    
    String ENABLE_AUTO_DEPLOY_DESCR =
        "ENABLE_AUTO_DEPLOY_DESCR";
    
    String ENABLE_AUTO_DEPLOY_TOOLTIP =
        "ENABLE_AUTO_DEPLOY_TOOLTIP";
    
    // Enable auto undeploy
    String ENABLE_AUTO_UNDEPLOY_DISPLAY_NAME =
        "ENABLE_AUTO_UNDEPLOY_DISPLAY_NAME";
    
    String ENABLE_AUTO_UNDEPLOY_DESCR =
        "ENABLE_AUTO_UNDEPLOY_DESCR";
    
    String ENABLE_AUTO_UNDEPLOY_TOOLTIP =
        "ENABLE_AUTO_UNDEPLOY_TOOLTIP";

    // Enable auto redeploy
    String ENABLE_AUTO_REDEPLOY_DISPLAY_NAME =
        "ENABLE_AUTO_REDEPLOY_DISPLAY_NAME";
    
    String ENABLE_AUTO_REDEPLOY_DESCR =
        "ENABLE_AUTO_REDEPLOY_DESCR";
    
    String ENABLE_AUTO_REDEPLOY_TOOLTIP =
        "ENABLE_AUTO_REDEPLOY_TOOLTIP";
    
    // auto deploy directory name
    String AUTO_DEPLOY_DIR_DISPLAY_NAME =
        "AUTO_DEPLOY_DIR_DISPLAY_NAME";
    
    String AUTO_DEPLOY_DIR_DESCR =
        "AUTO_DEPLOY_DIR_DESCR";
    
    String AUTO_DEPLOY_DIR_TOOLTIP =
        "AUTO_DEPLOY_DIR_TOOLTIP";
    
    // start on deploy
    String START_ON_DEPLOY_DISPLAY_NAME =
        "START_ON_DEPLOY_DISPLAY_NAME";
    
    String START_ON_DEPLOY_DESCR =
        "START_ON_DEPLOY_DESCR";
    
    String START_ON_DEPLOY_TOOLTIP =
        "START_ON_DEPLOY_TOOLTIP";
    
    // start on verify
    String START_ON_VERIFY_DISPLAY_NAME =
        "START_ON_VERIFY_DISPLAY_NAME";
    
    String START_ON_VERIFY_DESCR =
        "START_ON_VERIFY_DESCR";
    
    String START_ON_VERIFY_TOOLTIP =
        "START_ON_VERIFY_TOOLTIP";
        
    /** System configuration parameters */
    String JBI_HOME_DISPLAY_NAME  =
        "JBI_HOME_DISPLAY_NAME";
    
    String JBI_HOME_DESCR =
        "JBI_HOME_DESCR";
    
    String JBI_HOME_TOOLTIP =
        "JBI_HOME_DESCR";
    
    String DEFAULT_LOG_LEVEL_DISPLAY_NAME  =
        "DEFAULT_LOG_LEVEL_DISPLAY_NAME";
    
    String DEFAULT_LOG_LEVEL_DESCR =
        "DEFAULT_LOG_LEVEL_DESCR";
    
    String DEFAULT_LOG_LEVEL_TOOLTIP =
        "DEFAULT_LOG_LEVEL_TOOLTIP";
        
    String HEART_BEAT_INTERVAL_DISPLAY_NAME =
        "HEART_BEAT_INTERVAL_DISPLAY_NAME";
    
    String HEART_BEAT_INTERVAL_DESCR =
        "HEART_BEAT_INTERVAL_DESCR";
    
    String HEART_BEAT_INTERVAL_TOOLTIP =
        "HEART_BEAT_INTERVAL_TOOLTIP";
    
    String MSG_SVC_STATS_ENABLED_DISPLAY_NAME =
        "MSG_SVC_STATS_ENABLED_DISPLAY_NAME";
    
    String MSG_SVC_STATS_ENABLED_DESCR =
        "MSG_SVC_STATS_ENABLED_DESCR";
    
    String MSG_SVC_STATS_ENABLED_TOOLTIP =
        "MSG_SVC_STATS_ENABLED_TOOLTIP";
    
    /**
     * Logger Configuration parameters
     */
    String JBI_LOGGER_DISPLAY_NAME =
        "JBI_LOGGER_DISPLAY_NAME";
    
    String JBI_LOGGER_DESCR =
        "JBI_LOGGER_DESCR";
    
    String JBI_LOGGER_TOOLTIP =
        "JBI_LOGGER_TOOLTIP";
    
    String FRAMEWORK_LOGGER_DISPLAY_NAME =
        "FRAMEWORK_LOGGER_DISPLAY_NAME";
    
    String FRAMEWORK_LOGGER_DESCR =
        "FRAMEWORK_LOGGER_DESCR";
    
    String FRAMEWORK_LOGGER_TOOLTIP =
        "FRAMEWORK_LOGGER_TOOLTIP";

    String MESSAGING_LOGGER_DISPLAY_NAME =
        "MESSAGING_LOGGER_DISPLAY_NAME";
    
    String MESSAGING_LOGGER_DESCR =
        "MESSAGING_LOGGER_DESCR";
    
    String MESSAGING_LOGGER_TOOLTIP =
        "MESSAGING_LOGGER_TOOLTIP";

    String INSTALLATION_LOGGER_DISPLAY_NAME  =
        "INSTALLATION_LOGGER_DISPLAY_NAME";
    
    String INSTALLATION_LOGGER_DESCR =
        "INSTALLATION_LOGGER_DESCR";
    
    String INSTALLATION_LOGGER_TOOLTIP =
        "INSTALLATION_LOGGER_TOOLTIP";

    String MANAGEMENT_LOGGER_DISPLAY_NAME =
        "MANAGEMENT_LOGGER_DISPLAY_NAME";
    
    String MANAGEMENT_LOGGER_DESCR =
        "MANAGEMENT_LOGGER_DESCR";
    
    String MANAGEMENT_LOGGER_TOOLTIP =
        "MANAGEMENT_LOGGER_TOOLTIP";

    String ADMIN_LOGGER_DISPLAY_NAME=
        "ADMIN_LOGGER_DISPLAY_NAME";
    
    String ADMIN_LOGGER_DESCR=
        "ADMIN_LOGGER_DESCR";
    
    String ADMIN_LOGGER_TOOLTIP=
        "ADMIN_LOGGER_TOOLTIP";
    
    String DEPLOYMENT_LOGGER_DISPLAY_NAME =
        "DEPLOYMENT_LOGGER_DISPLAY_NAME";
    
    String DEPLOYMENT_LOGGER_DESCR =
        "DEPLOYMENT_LOGGER_DESCR";
    
    String DEPLOYMENT_LOGGER_TOOLTIP =
        "DEPLOYMENT_LOGGER_TOOLTIP";
    
    String CONFIGURATION_LOGGER_DISPLAY_NAME =
        "CONFIGURATION_LOGGER_DISPLAY_NAME";
    
    String CONFIGURATION_LOGGER_DESCR =
        "CONFIGURATION_LOGGER_DESCR";
    
    String CONFIGURATION_LOGGER_TOOLTIP =
        "CONFIGURATION_LOGGER_TOOLTIP";
    
    String LOGGING_LOGGER_DISPLAY_NAME =
        "LOGGING_LOGGER_DISPLAY_NAME";
    
    String LOGGING_LOGGER_DESCR =
        "LOGGING_LOGGER_DESCR";
    
    String LOGGING_LOGGER_TOOLTIP =
        "LOGGING_LOGGER_TOOLTIP";
    
    String ATTRIBUTE_CHANGE_NOTIF_MSG = 
            "ATTRIBUTE_CHANGE_NOTIF_MSG";
    
    String JBI_ADMIN_FAILED_ADD_LOGGER_LISTENER =
            "JBI_ADMIN_FAILED_ADD_LOGGER_LISTENER";
    
    String JBI_ADMIN_LOGGER_MBN_CREATE_FAILED =
        "JBI_ADMIN_LOGGER_MBN_CREATE_FAILED";
    
    /**
     * Configuration messages
     */
    String JBI_ADMIN_GLOBAL_CFG_PERSIST_FAILED =
        "JBI_ADMIN_GLOBAL_CFG_PERSIST_FAILED";
    
    String JBI_ADMIN_SET_GLOBAL_CFG_ATTRIB_FAILED =
        "JBI_ADMIN_SET_GLOBAL_CFG_ATTRIB_FAILED";
    
    String JBI_ADMIN_SET_CFG_ATTRIB_FAILED =
        "JBI_ADMIN_SET_CFG_ATTRIB_FAILED";

    String JBI_ADMIN_APP_CONFIG_PROPS_MISSING_NAME =
        "JBI_ADMIN_APP_CONFIG_PROPS_MISSING_NAME";
    
    String JBI_ADMIN_FAILED_RM_LOGGER_LISTENER =
            "JBI_ADMIN_FAILED_RM_LOGGER_LISTENER";
    
    String JBI_ADMIN_FAILED_MIN_VALUE_CONVERSION =
            "JBI_ADMIN_FAILED_MIN_VALUE_CONVERSION";
    
    String JBI_ADMIN_CONFIG_PARAM_LESS_THAN_MIN_VALUE =
            "JBI_ADMIN_CONFIG_PARAM_LESS_THAN_MIN_VALUE";

    String JBI_ADMIN_FAILED_MAX_VALUE_CONVERSION =
            "JBI_ADMIN_FAILED_MAX_VALUE_CONVERSION";
    
    String JBI_ADMIN_CONFIG_PARAM_MORE_THAN_MAX_VALUE =
            "JBI_ADMIN_CONFIG_PARAM_MORE_THAN_MAX_VALUE";
    
    String JBI_ADMIN_CONFIG_PARAM_OUT_OF_RANGE =
            "JBI_ADMIN_CONFIG_PARAM_OUT_OF_RANGE";
    
    String JBI_ADMIN_CONFIG_READ_ONLY_ATTRIBUTE =
            "JBI_ADMIN_CONFIG_READ_ONLY_ATTRIBUTE";
    
    //DeploymentService statistics messages
    /**
     * no stats available for this SA
     */
    String DS_NO_STATS_FOR_SA = "DS_NO_STATS_FOR_SA";

    /**
     * stats could nor be collected for this SA
     */
    String DS_ERROR_IN_COLLECTING_STATISTICS_FOR_SA = 
            "DS_ERROR_IN_COLLECTING_STATISTICS_FOR_SA";
    
    /**
     * stats could not be composed for SA
     */
    String DS_ERROR_IN_COMPOSING_STATISTICS_FOR_SA = 
            "DS_ERROR_IN_COMPOSING_STATISTICS_FOR_SA";    
}
