/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SharedLibraryDescriptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ArchiveValidator.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 2, 2006, 4:49 PM
 */

package com.sun.jbi.management.descriptor;

import com.sun.jbi.component.InstallationContext;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.Jbi.SharedLibrary;
import com.sun.jbi.management.util.StringHelper;

import java.util.List;


/**
 *
 * This class encapsulates the JAXB model for the Shared Library Descriptor
 *
 * @author Sun Microsystems, Inc
 */
public class SharedLibraryDescriptor
{

    private com.sun.jbi.management.descriptor.Jbi.SharedLibrary mSlType;
    
    /**
     * Constructs a SharedLibraryDescriptor.
     *
     * @throws an IllegalArgumentException if the JbiType passed in not that
     * for a component
     */
    public SharedLibraryDescriptor(Jbi jbi)
        throws IllegalArgumentException
    {
        mSlType = jbi.getSharedLibrary();
        
        if ( mSlType == null )
        {
            throw new IllegalArgumentException();
        }
    }
    
    /**
     * Get the name of the shared library
     */
    public String getName()
    {
        if ( mSlType.getIdentification() != null )
        {
            return StringHelper.trim( mSlType.getIdentification().getName() );
        }
        else
        {
            return "";
        }
    }
    
    /**
     * @return the Shared Library Description
     */
    public String getDescription()
    {
        if ( mSlType.getIdentification() != null )
        {
            return StringHelper.trim( mSlType.getIdentification().getDescription() );
        }
        else
        {
            return "";
        }
    }
    
    /**
     * @return true if the Class Loader Delegation is self first.
     * @see com.sun.jbi.component.InstallationContext
     */
    public boolean isSharedLibraryClassLoaderSelfFirst()
    {
        return ( InstallationContext.SELF_FIRST.equals(
            StringHelper.trim(mSlType.getClassLoaderDelegation())) );
    }
    
    /**
     * @return the Shared Library Class Path elements. White spaces in individual elements
     * are trimmed.
     */
    public List<String> getSharedLibraryClassPathElements()
    {
        if ( mSlType.getSharedLibraryClassPath() != null )
        {
            return StringHelper.trim(mSlType.getSharedLibraryClassPath().getPathElement());
        }
        return new java.util.ArrayList<String>();
    }
    
}
