/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MBeanSet.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.support;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.util.EnvironmentAccess;
import com.sun.jbi.management.LocalStringKeys;

import javax.management.MBeanServer;
import javax.management.StandardMBean;
import javax.management.InstanceNotFoundException;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Iterator;
import javax.management.ObjectName;

/**
 * Implementation of MBeanSet interface.
 *
 * @author Sun Microsystems, Inc.
 */
public class MBeanSet implements com.sun.jbi.management.MBeanSet
{

    /** Handle to the main MBean server */
    private final MBeanServer mMBeanServer;

    /** Handle to the logger where this class will log errors.  */
    private final Logger mLogger;

    /** Handle to StringTranslator for message translation */
    private StringTranslator mTranslator = null;

    /** Storage for our MBeanSet:*/
    private final HashMap mStorage;

    /** the null mbean object name: */
    private static ObjectName nullObjectName = null;

    /** the null entry: */
    private static Entry nullEntry = null;

    /**
     * Constructs a <CODE>MBeanSet</CODE>.
     * @param aMBeanServer is the MBean server provided by the framework.
     * @param aLogger is where this class should log messages.
     */
    public MBeanSet(MBeanServer aMBeanServer, Logger aLogger)
    {
        mMBeanServer = aMBeanServer;
        mLogger = aLogger;
        mStorage = new HashMap();

        EnvironmentContext ctx = null;
        
        try
        {
            ctx = EnvironmentAccess.getContext();
        }
        catch (NoClassDefFoundError e)
        {
            // just leave ctx null; this class won't log errors.
        }

        if (null != ctx)
        {
            mTranslator = ctx.getStringTranslator("com.sun.jbi.management");
        }


        if ((null == mMBeanServer) && (null != mTranslator))
        {
            String warningMsg = mTranslator.getString(
                                    LocalStringKeys.MBEANSET_MBEANSERVER_NULL_OP);

            mLogger.warning(warningMsg);
        }

        if (null == nullObjectName)
        {
            try
            {
                nullObjectName = new ObjectName("NULL_JMX_DOMAIN:NULL_ATTRIBUTE_KEY=NULL_ATTRIBUTE_VALUE");
            }
            catch (Exception e)
            {
                if (null != mTranslator)
                {
                    String severeMsg = mTranslator.getString(
                                    LocalStringKeys.MBEANSET_OBJECTNAME_NULL,
                                    e.toString());
                    mLogger.severe(severeMsg);
                }
            }
        }

        if (null == nullEntry)
        {
            nullEntry = new Entry(nullObjectName, null, null, false);
        }
    }

    /** private inner class to implement MBeanSet entries: */
    private class Entry
    {
        public final ObjectName mMBeanName;  //repeated for convenience
        public final Class mMBeanDefinition;
        public final Object mMBeanImplementation;
        public final boolean  mIsEmitter;  //true if mbean is to emit notifications

        /** constructor: */
        Entry(ObjectName theMBeanName, Class theMBeanDefinition,
            Object theMBeanImplementation, boolean isEmitter)
        {
            if (null == theMBeanName)
            {
                theMBeanName = nullObjectName;
            }

            mMBeanName = theMBeanName;
            mMBeanDefinition = theMBeanDefinition;
            mMBeanImplementation = theMBeanImplementation;
            mIsEmitter = isEmitter;  //true if mbean is to emit notifications
        }

        /**
         * toString - create a string representation of a MBeanSet Entry. 
         * @return String representing the Entry.
         */
        public String toString()
        {
            return (
                mMBeanName.toString() + "\t" +
                ( (mMBeanDefinition == null)? "NULL_DEFINITION" : mMBeanDefinition.toString() ) + "\t" +
                ( (mMBeanImplementation == null)? "NULL_IMPLEMENTATION" : mMBeanImplementation.toString() )
            );
        }
    }

    /**
     * Add an Standard MBean to the Set that can potentially emit JMX notifications.
     *
     * @param theMBeanName - the object name of the mbean to add.
     * @param theMBeanDefinition - the interface that defines the MBean.
     * @param theMBeanImplementation - an instance of the MBean implementation class.
     * @param isEmitter - true if this MBean wants to emit jmx notifications.
     *
     * @return true if mbean is added to the set, otherwise false.
     */
    public boolean add(ObjectName theMBeanName, Class theMBeanDefinition,
        Object theMBeanImplementation, boolean isEmitter)
    {
        if (null == theMBeanName)
        {
            //map null object names to a dummy entry:
            mStorage.put(nullObjectName, nullEntry);
        }
        else
        {
            Entry ee = new Entry(theMBeanName, theMBeanDefinition, theMBeanImplementation, isEmitter);
            mStorage.put(theMBeanName, ee);
        }
        return true;
    }

    /**
     * Add an Standard MBean to the Set that will not emit notifications.
     *
     * @param theMBeanName - the object name of the mbean to add.
     * @param theMBeanDefinition - the interface that defines the MBean.
     * @param theMBeanImplementation - an instance of the MBean implementation class.
     *
     * @return true if mbean is added to the set, otherwise false.
     */
    public boolean add(ObjectName theMBeanName, Class theMBeanDefinition,
        Object theMBeanImplementation)
    {
        return add(theMBeanName, theMBeanDefinition, theMBeanImplementation, false);
    }

    /**
     * Replace a Standard MBean in the Set that can potentially emit JMX notifications.
     *
     * @param theMBeanName - the object name of the mbean to replace.
     * @param theMBeanDefinition - the interface that defines the MBean.
     * @param theMBeanImplementation - an instance of the MBean implementation class.
     * @param isEmitter - true if this MBean wants to emit jmx notifications.
     *
     * @return true if mbean is replaced in the set, otherwise false.
     */
    public boolean replace(ObjectName theMBeanName, Class theMBeanDefinition,
        Object theMBeanImplementation, boolean isEmitter)
    {
        return add(theMBeanName, theMBeanDefinition, theMBeanImplementation, isEmitter);
    }

    /**
     * Replace a Standard MBean in the Set.
     *
     * @param theMBeanName - the object name of the mbean to replace.
     * @param theMBeanDefinition - the interface that defines the MBean.
     * @param theMBeanImplementation - an instance of the MBean implementation class.
     *
     * @return true if mbean is replaced in the set, otherwise false.
     */
    public boolean replace(ObjectName theMBeanName, Class theMBeanDefinition,
        Object theMBeanImplementation)
    {
        return add(theMBeanName, theMBeanDefinition, theMBeanImplementation, false);
    }

    /**
     * Delete an MBean from an existing MBean Set.
     *
     * @param theMBeanName - the object name of the mbean to delete.
     *
     * @return true if the MBean is deleted from the set, otherwise false.
     */
    public boolean delete(ObjectName theMBeanName)
    {
        if (null == theMBeanName)
        {
            //map null object names to a dummy entry:
            theMBeanName = nullObjectName;
        }

        //true if we delete it:
        return ( mStorage.remove(theMBeanName) != null );
    }

    /**
     * True if an MBean exists in the named MBean Set.
     *
     * @param theMBeanName - the object name of the mbean to check.
     *
     * @return true if the MBean exists in the set, otherwise false.
     */
    public boolean exists(ObjectName theMBeanName)
    {
        if (null == theMBeanName)
        {
            //map null object names to a dummy entry:
            theMBeanName = nullObjectName;
        }

        return ( mStorage.containsKey(theMBeanName) );
    }

    /**
     * True if an MBean is part of this set and is registered.
     *
     * @param theMBeanName - the object name of the mbean to check.
     *
     * @return true if the MBean exists in the set, otherwise false.
     */
    public boolean isRegistered(ObjectName theMBeanName)
    {
        if (null == mMBeanServer || null == theMBeanName)
        {
            return false;
        }

        if (theMBeanName.toString() == nullObjectName.toString())
        {
            return false;
        }

        return mMBeanServer.isRegistered(theMBeanName);
    }

    /**
     * Register all of the MBean's in an MBean Set.
     *
     * Okay if some are already registered.
     * Ignores null entries.
     *
     * To re-register, you must unregister the named object before
     * registering the set.
     *
     * @return true iff all mbeans are now registered.
     */
    public boolean register()
    {
        if ((null == mMBeanServer) && (null != mTranslator))
        {
            String warningMsg = mTranslator.getString(
                                    LocalStringKeys.MBEANSET_MBEANSERVER_NULL_REG);

            mLogger.warning(warningMsg);
            return false;
        }

        int nerrors = 0;

        //foreach key...
        Iterator ii = mStorage.keySet().iterator();
        while ( ii.hasNext() )
        {
            ObjectName mbn = (ObjectName) ii.next();
            Entry ee  = (Entry) mStorage.get(mbn);

            //register as standard mbean if not already registered:
            if (!isRegistered(mbn) && ee != nullEntry)
            {
                try
                {
                    StandardMBean mbean = null;
                    if (ee.mIsEmitter)
                    {
                        mbean = new NotifyStandardMBean(ee.mMBeanImplementation, ee.mMBeanDefinition);
                    }
                    else
                    {
                        if ( ee.mMBeanImplementation instanceof StandardMBean )
                        {
                            mbean = (StandardMBean) ee.mMBeanImplementation;
                        }
                        else
                        {
                            mbean = new StandardMBean(ee.mMBeanImplementation, ee.mMBeanDefinition) {};
                        }
                    }
                    mMBeanServer.registerMBean(mbean, mbn);
                }
                catch (Exception e)
                {
                    /* throws:
                     *    InstanceAlreadyExistsException,
                     *    MBeanRegistrationException,
                     *    NotCompliantMBeanException
                     */
                    e.printStackTrace();
                    ++nerrors;
                }
            }
        }

        return (0 == nerrors);
    }

    /**
     * Unregister all of the MBean's in an MBean Set.
     *
     * It is not an error to unregister an MBean that is not registered.
     *
     * @return true iff all mbeans are unregistered.
     */
    public boolean unregister()
    {
        if ((null == mMBeanServer) && (null != mTranslator))
        {
            String warningMsg = mTranslator.getString(
                                    LocalStringKeys.MBEANSET_MBEANSERVER_NULL_UNREG);

            mLogger.warning(warningMsg);
            return false;
        }

        int nerrors = 0;

        //foreach key...
        Iterator ii = mStorage.keySet().iterator();
        while ( ii.hasNext() )
        {
            ObjectName mbn = (ObjectName) ii.next();
            Entry ee  = (Entry) mStorage.get(mbn);

            //unregister as standard mbean if not already registered:
            if (isRegistered(mbn) && ee != nullEntry)
            {
                try
                {
                    mMBeanServer.unregisterMBean(mbn);
                }
                catch (InstanceNotFoundException infe)
                {
                    //ignore
                }
                catch (Exception e)
                {
                    /* throws:
                     *    InstanceAlreadyExistsException,
                     *    MBeanRegistrationException,
                     *    NotCompliantMBeanException
                     */
                    e.printStackTrace();
                    ++nerrors;
                }
            }
        }

        return (0 == nerrors);
    }

    /**
     * dump contents of an MBeanSet to the log.
     * @param aTitle - the title of the dump.
     */
    public void dump(String aTitle)
    {
        String txt = aTitle;

        //foreach key...
        Iterator ii = mStorage.keySet().iterator();
        while ( ii.hasNext() )
        {
            ObjectName mbn = (ObjectName) ii.next();
            Entry ee  = (Entry) mStorage.get(mbn);

            txt +=  "\n" + ee.toString();
        }

        mLogger.info( txt );
    }

}
