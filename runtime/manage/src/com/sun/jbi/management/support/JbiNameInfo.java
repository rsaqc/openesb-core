/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiNameInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.support;

/**
 * Implementation of JbiNameInfo interface.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiNameInfo implements com.sun.jbi.management.JbiNameInfo
{
    private final String mName;
    private final String mAltName;
    private final boolean mIsEngine;
    private final boolean mIsBinding;
    private final boolean mIsSystemService;

    /**
     * Constructs a <CODE>JbiNameInfo</CODE>.
     * This version of the constructor creates a system service name object.
     */
    public JbiNameInfo(String aServiceName)
    {
        mName = aServiceName;
        mAltName = null;
        mIsBinding = false;
        mIsEngine = false;
        mIsSystemService = true;
    }

    /**
     * Constructs a <CODE>JbiNameInfo</CODE>.
     * This version of the constructor creates an installed component name object.
     */
    public JbiNameInfo(String aComponentName, boolean isBinding)
    {
        mName = aComponentName;
        mAltName = aComponentName;
        mIsBinding = isBinding;
        mIsEngine = !isBinding;
        mIsSystemService = false;
    }

    /**
     * Constructs a <CODE>JbiNameInfo</CODE>.
     * This version of the constructor creates an installed component name object.
     */
    public JbiNameInfo(String aComponentName, String aComponentAlias, boolean isBinding)
    {
        mName = aComponentName;
        mAltName = aComponentAlias; //this is to be deprecated when we eliminate aliases
        mIsBinding = isBinding;
        mIsEngine = !isBinding;
        mIsSystemService = false;
    }

    /**
     * Return the name of the JBI Framework Service or Installed component.
     * @return name of service or component
     */
    public String name()
    {
        return mName;
    }

    /**
     * Return the alternative name of a JBI Framework installed component.
     * @return alternative name of the installed component
     */
    public String altName()
    {
        return mAltName;
    }

    /**
     * True iff this object identifies an SE.
     * By definition, isEngine() => !isBinding() && !isSystemService()
     *
     * @return true iff object identifies an SE.
     */
    public boolean isEngine()
    {
        return mIsEngine;
    }

    /**
     * True iff this object identifies an SE.
     * By definition, isBinding() => !isEngine() && !isSystemService()
     *
     * @return true iff object identifies an SE.
     */
    public boolean isBinding()
    {
        return mIsBinding;
    }

    /**
     * True iff this object identifies a JBI Framework System Service.
     * By definition, isSystemService() => !isEngine() && !isBinding()
     *
     * @return true iff object identifies an JBI Framework System Service.
     */
    public boolean isSystemService()
    {
        return mIsSystemService;
    }

}
