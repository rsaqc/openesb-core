/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DefaultLoggerMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.support;

import com.sun.jbi.management.common.LoggerMBean;
import com.sun.jbi.management.support.MBeanHelper;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * LoggerMBean defines standard controls for setting the properties of a
 * single JBI Framework service or JBI Installable Component logger.
 *
 * @author Sun Microsystems, Inc.
 */
public class DefaultLoggerMBeanImpl 
        extends javax.management.StandardMBean
        implements LoggerMBean
{
    /** Copy of Logger handle managed by this LoggerMBean implementation. */
    protected Logger mLogger;

    /** Display name to be used for this logger in the UI. */
    protected String mDisplayName;

    /** Constructs a <CODE>DefaultLoggerMBeanImpl</CODE>.  */
    public DefaultLoggerMBeanImpl(Logger aLogger) 
        throws Exception
    {
        
        //allocate a logger:
       this(aLogger, aLogger.getName());
    }

    /** Constructs a <CODE>DefaultLoggerMBeanImpl</CODE>.  */
    public DefaultLoggerMBeanImpl(Logger aLogger, String aDisplayName) 
        throws Exception
    {
        super(com.sun.jbi.management.common.LoggerMBean.class);
        //allocate a logger:
        mLogger = aLogger;
        mDisplayName = aDisplayName;
    }

    /**
     * Get the localized display name of this logger.
     * @return String representing the localized display name.
     */
    public String getDisplayName()
    {
        return mDisplayName;
    }

    /**
     * Get the log level of this logger. If the level is not set, search the
     * parent logger chain until a logger is found with a level set.
     * @return String representing log level or null if no level is set.
     */
    public String getLogLevel()
    {
        Logger log = mLogger;
        Level level = log.getLevel();

        while ( null == level )
        {
            log = log.getParent();
            if ( null != log )
            {
                level = log.getLevel();
            }
            else
            {
                break;
            }
        }
        if ( null != level )
        {
            return level.getLocalizedName();
        }
        else
        {
            return null;
        }
    }

    /**
     * Get the name of this logger.
     * @return String representing the logger name.
     */
    public String getLoggerName()
    {
        return mLogger.getName();
    }

    /**
     * Set the log level of this logger to ALL.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setAll()
    {
        mLogger.setLevel(Level.ALL);
        return 0;
    }

    /**
     * Set the log level of this logger to CONFIG.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setConfig()
    {
        mLogger.setLevel(Level.CONFIG);
        return 0;
    }

    /**
     * Set the log level of this logger to null to inherit from its parent.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setDefault()
    {
        mLogger.setLevel(null);
        return 0;
    }

    /**
     * Set the log level of this logger to FINE.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setFine()
    {
        mLogger.setLevel(Level.FINE);
        return 0;
    }

    /**
     * Set the log level of this logger to FINER.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setFiner()
    {
        mLogger.setLevel(Level.FINER);
        return 0;
    }

    /**
     * Set the log level of this logger to FINEST.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setFinest()
    {
        mLogger.setLevel(Level.FINEST);
        return 0;
    }

    /**
     * Set the log level of this logger to INFO.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setInfo()
    {
        mLogger.setLevel(Level.INFO);
        return 0;
    }

    /**
     * Set the log level of this logger to OFF.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setOff()
    {
        mLogger.setLevel(Level.OFF);
        return 0;
    }

    /**
     * Set the log level of this logger to SEVERE.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setSevere()
    {
        mLogger.setLevel(Level.SEVERE);
        return 0;
    }

    /**
     * Set the log level of this logger to WARNING.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    public int setWarning()
    {
        mLogger.setLevel(Level.WARNING);
        return 0;
    }

}
