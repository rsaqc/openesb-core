/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ModelEngineComponent.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.engine;

import com.sun.jbi.management.JbiNameInfo;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.support.MBeanSet;
import com.sun.jbi.management.common.LoggerMBean;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Properties;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;
import javax.management.NotificationFilter;
import javax.management.MBeanNotificationInfo;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.jbi.component.Component;
import com.sun.jbi.management.common.ControllerMBean;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.component.ComponentContext;
import javax.jbi.JBIException;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import javax.jbi.management.DeploymentException;

/**
 * The ModelEngineComponent implements the management controls for a standard
 * JBI Framework engine component, and can be extended to provide a full engine
 * implementation.
 *
 * @author Sun Microsystems, Inc.
 */
public abstract class ModelEngineComponent 
    implements Component, ControllerMBean, ComponentLifeCycle, ServiceUnitManager, 
        NotificationEmitter
{
    protected com.sun.jbi.component.ComponentContext mEnv;

    //allocate mbean sets for INITIAL and START/STOP mbeans:
    protected MBeanSet mInitialMBeans, mStartMBeans;

    /** Logger to be used by ModelEngineComponent methods: */
    protected Logger mLogger;

    /** handle to our MBean Server: */
    protected MBeanServer mMBeanServer;

    /** handle to our MBean Namer: */
    protected com.sun.jbi.management.MBeanNames mMBeanNames;

    /** handle to jbi system or installed component identification: */
    protected JbiNameInfo mJbiNameInfo;

    /** User-friendly display name to be used by ModelEngineComponent methods: */
    protected String mDisplayName;

    /** JMX object names for standard mbeans: */
    protected ObjectName mLifeCycleMBeanName;
    protected ObjectName mLoggerMBeanName;

    protected final String mModelName = "ModelEngineComponent";

    /** Handle to StringTranslator for message translation: */  
    protected StringTranslator mTranslator;

    /** allocate a logger, and then initialize the ModelEngineComponent. */
    protected void initModelEngineComponent(javax.jbi.component.ComponentContext anEnv, JbiNameInfo aJbiName)
    {
        //allocate a logger, since none was provided::
        mLogger = Logger.getLogger(anEnv.getComponentName());

        //call the secondary form of init:
        initModelEngineComponent(anEnv, mLogger, aJbiName);
    }

    /** Initialize the ModelEngineComponent.  This does not register any mbeans. */
    protected void initModelEngineComponent(javax.jbi.component.ComponentContext anEnv, Logger aLogger, JbiNameInfo aJbiName)
    {
        //get access to environment implementation, if different:
        mEnv = (com.sun.jbi.component.ComponentContext) anEnv;

        //save handle to jbi system or installed component identification:
        mJbiNameInfo = aJbiName;

        //save logger handle
        mLogger = aLogger;

        mMBeanServer = mEnv.getMBeanServer();
        try {
            //why doesn't this work for model components????
            //(throws a class-cast exception).
            //mMBeanNames = (com.sun.jbi.management.MBeanNames) mEnv.getMBeanNames();

            //work-around for above:
            com.sun.jbi.management.support.MBeanNamesImpl mbnamesImpl  = 
                new com.sun.jbi.management.support.MBeanNamesImpl(
                    mEnv.getMBeanNames().getJmxDomainName(), 
                    System.getProperty("com.sun.jbi.instanceName"));
            mMBeanNames = (com.sun.jbi.management.MBeanNames) mbnamesImpl;

            //create MBEAN sets for INITIAL and START states:
            mInitialMBeans = new MBeanSet(mMBeanServer, mLogger);
            mStartMBeans = new MBeanSet(mMBeanServer, mLogger);

            //create Object names for standard mbeans, and populate mbean sets:
            initModelEngineMBeanNames();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
                //this is for translating messages in management support packages:
            mTranslator = (StringTranslator) mEnv.getStringTranslator("com.sun.jbi.management");
        } catch (Exception e) {
            e.printStackTrace();
        }
        mLogger.fine("initModelEngineComponent: Initialization complete for " + mJbiNameInfo.name());
    }

    /** perform one-time startup tasks for this model */
    protected void bootstrap()
    {
        mLogger.fine("ModelEngineComponent: START bootstrap for '" + mJbiNameInfo.name() + "'");

        //register INITIAL mbean set:
        mInitialMBeans.register();

        mLogger.fine("ModelEngineComponent: FINISH bootstrap for '" + mJbiNameInfo.name() + "'");
    }

    ////////////////
    //implementation of JBI Framework Component:
    ////////////////

    /**
     * Get the life cycle control interface for this component. This interface
     * allows the JBI implementation to control the running state of this 
     * component.
     * <p>
     * This method must be called before any other methods of this interface
     * are called. In addition, the JBI implementation must call the init()
     * method of the component life cycle returned by this method before 
     * calling any other methods on this interface, or the component life cycle
     * interface.
     * 
     * @return the life cycle control interface for this component; must be
     *         non-null.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return this;
    }

    /**
     * Get the Service Unit manager for this component. If this component does
     * not support deployments, it must return <code>null</code>.
     *
     * @return the <code>ServiceUnitManager</code> for this component, or
     *         <code>null</code> if there is none.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        return this;
    }

    /**
     * Retrieves a DOM representation containing metadata which describes the 
     * service provided by this component, through the given endpoint. The 
     * result can use WSDL 1.1 or WSDL 2.0.
     *
     * @param endpoint the service endpoint.
     * @return the description for the specified service endpoint.
     */
    public Document getServiceDescription(ServiceEndpoint endpoint)
    {
        return (Document) null;
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually 
     * perform the operation desired. 
     *
     * @param endpoint the endpoint to be used by the consumer; must be
     *        non-null.
     * @param exchange the proposed message exchange to be performed; must be
     *        non-null.
     * @return <code>true</code> if this provider component can perform the
     *         given exchange with the described consumer.
     */
    public boolean isExchangeWithConsumerOkay(
        ServiceEndpoint endpoint,
        MessageExchange exchange)
    {
        return true;
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually 
     * interact with the provider properly. The provider is described by the
     * given endpoint and the service description supplied by that endpoint.
     *
     * @param endpoint the endpoint to be used by the provider; must be
     *        non-null.
     * @param exchange the proposed message exchange to be performed; must be
     *        non-null.
     * @return <code>true</code> if this consumer component can interact with
     *         the described provider to perform the given exchange.
     */
    public boolean isExchangeWithProviderOkay(
        ServiceEndpoint endpoint,
        MessageExchange exchange)
    {
        return true;
    }

    /**
     * Resolve the given endpoint reference. This is called by JBI when it is 
     * attempting to resolve the given EPR on behalf of a component.
     * <p>
     * If this component returns a non-null result, it must conform to the
     * following:
     * <ul>
     *   <li>This component implements the {@link ServiceEndpoint} returned.
     *   </li>
     *   <li>The result must not be registered or activated with the JBI
     *       implementation.</li>
     * </ul>
     * 
     * Dynamically resolved endpoints are distinct from static ones; they must
     * not be activated (see {@link ComponentContext#activateEndpoint(QName, 
     * String)}), nor registered (see {@link ComponentContext}) by components. 
     * They can only be used to address message exchanges; the JBI 
     * implementation must deliver such exchanges to the component that resolved
     * the endpoint reference (see {@link 
     * ComponentContext#resolveEndpointReference(DocumentFragment)}).
     * 
     * @param epr the endpoint reference, in some XML dialect understood by
     *        the appropriate component (usually a binding); must be non-null.
     * @return the service endpoint for the EPR; <code>null</code> if the
     *         EPR cannot be resolved by this component.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }

    ////////////////
    //implementation of JBI Framework ComponentLifeCycle:
    ////////////////

    /**
     * Get the JMX object name for the extension MBean for this component; if
     * there is none, return <code>null</code>.
     *
     * @return the JMX object name of the additional MBean or <code>null</code>
     * if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        return mControllerMBeanName;
    }

    /**
     * Initialize the component. This performs initialization required by the 
     * component but does not make it ready to process messages. This method is 
     * called once for each life cycle of the component.
     * <p>
     * If the component needs to register an additional MBean to extend its
     * life cycle, or provide other component management tasks, it should
     * be registered during this call.
     * 
     * @param context the component's context, providing access to component
     *                data provided by the JBI environment; must be non-null.
     * @exception JBIException if the component is unable to initialize.
     */
    public void init(ComponentContext context)
        throws JBIException
    {
    }

    /**
     * Shut down the component. This performs clean-up, releasing all run-time
     * resources used by the component. Once this method has been called, 
     * {@link #init(ComponentContext)} must be called before the component can 
     * be started again with a call to {@link #start()}.
     *
     * @exception JBIException if the component is unable to shut down.
     */
    public void shutDown()
        throws JBIException
    {
        try { 
            //unregister INITIAL mbean set:
            mInitialMBeans.unregister();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Start the component. This makes the component ready to process messages. 
     * This method is called after {@link #init(ComponentContext)}, both when
     * the component is being started for the first time and when the component
     * is being restarted after a previous call to {@link #shutDown()}.
     * If {@link #stop()} was called previously but {@link #shutDown()} was not,
     * <code>start()</code> can be called again without another call to
     * {@link #init(ComponentContext)}.
     * 
     * @exception JBIException if the component is unable to start.
     */
    public void start()
        throws JBIException
    {
        //register START/STOP mbean set:
        mStartMBeans.register();
    }

    /**
     * Stop the component. This makes the component stop accepting messages for 
     * processing. After a call to this method, {@link #start()} may be called
     * again without first calling {@link #init(ComponentContext)}.
     * 
     * @exception JBIException if the component is unable to stop.
     */
    public void stop()
        throws JBIException
    {
        //unregister START/STOP mbean set:
        mStartMBeans.unregister();
    }

    ////////////////
    //implementation of ServiceUnitManager:
    ////////////////

    /**
     * Deploy a Service Unit to the component. This is called by the JBI
     * implementation in order to deploy the given artifact to the implementing
     * component.
     * <p>
     * Upon successful deployment, a non-empty result string must be returned,
     * that starts with the JBI-defined component-task-result element.
     * For example:
     * <pre>
     * &lt;component-task-result&gt;
     *   &lt;component-name&gt;BC1&lt;/component-name&gt;
     *   &lt;component-task-result-details
     *     xmlns="http://java.sun.com/xml/ns/jbi/management-message"&gt;
     *       &lt;task-result-details&gt;
     *           &lt;task-id>deploy&lt;/task-id&gt;
     *           &lt;task-result>SUCCESS&lt;/task-result&gt;
     *       &lt;/task-result-details&gt;
     *   &lt;/component-task-result-details&gt;
     * &lt;/component-task-result&gt;
     * </pre>
     * A failed deployment of the service unit must be reported using the
     * <code>component-task-result</code> element as well; the
     * <code>task-result</code> must be set to FAILED.
     *
     * @param serviceUnitName name of the service unit being deployed; must be
     *        non-null and non-empty and unique among service units already
     *        deployed to the component.
     * @param serviceUnitRootPath path of the service unit artifact root, in
     *        platform specific format; must be non-null and non-empty.
     * @return a deployment status message, which is an XML string that conforms
     *         to the schema given in the <i>MBean Status and Result Strings</i>
     *         section of the <i><b>Management</b></i> chapter of the JBI
     *         specification; must be non-null and non-empty.
     * @exception DeploymentException if the deployment operation is
     *            unsuccessful.
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws DeploymentException
    {
        return messageDeployOkay();
    }

    /**
     * Initialize the given deployed service unit. This is the first phase of
     * a two-phase start, where the component must prepare to receive service
     * requests related to the deployment (if any).
     * <p>
     * The serviceUnitRootPath parameter is provided to facilitate restart of
     * the component. This allows simple components to rely entirely on JBI's
     * ability to persist deployment information, avoiding the need for the
     * component to provide its own persistence mechanism.
     *
     * @param serviceUnitName name of the service unit being initialized; must
     *        be non-null, non-empty, and match the name of a previously
     *        deployed (but not yet undeployed) service unit.
     * @param serviceUnitRootPath path of the service unit artifact root, in
     *        platform specific format; must be non-null and non-empty.
     * @exception DeploymentException if the service unit is not deployed, or
     *            if it is in an incorrect state.
     */
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws DeploymentException
    {
    }

    /**
     * Start the deployed service unit. This is the second phase of a two-phase
     * start, where the component can now initiate service requests related to
     * the deployment.
     *
     * @param serviceUnitName the name of the service unit being started; must
     *        be non-null, non-empty, and match the name of a previously
     *        deployed (but not yet undeployed) service unit.
     * @exception DeploymentException if the service unit is not deployed, or
     *           if it is in an incorrect state.
     */
    public void start(String serviceUnitName)
        throws DeploymentException
    {
    }

    /**
     * Stop the deployed service unit. This causes the component to cease
     * generating service requests related to the given service unit. This
     * returns the service unit to a state equivalent to after
     * {@link #init(String, String)} was called.
     *
     * @param serviceUnitName name of the service unit being stopped; must
     *        be non-null, non-empty, and match the name of a previously
     *        deployed (but not yet undeployed) service unit.
     * @exception DeploymentException if the service unit is not deployed, or
     *            if it is in an incorrect state.
     */
    public void stop(String serviceUnitName)
        throws DeploymentException
    {
    }

    /**
     * Shut down the deployment. This causes the deployment to return to the
     * to the state it was in after {@link #deploy(String, String)}, and before
     * {@link #init(String, String)}.
     *
     * @param serviceUnitName name of the service unit being shut down; must
     *        be non-null, non-empty, and match the name of a previously
     *        deployed (but not yet undeployed) service unit.
     * @exception DeploymentException if the service unit is not deployed, or
     *            if it is in an incorrect state.
     */
    public void shutDown(String serviceUnitName)
        throws DeploymentException
    {
    }

    /**
     * Undeploy a service unit from the component. The service unit must be
     * shut down to undeploy it.
     *
     * @param serviceUnitName name of the service unit being undeployed; must
     *        be non-null, non-empty, and match the name of a previously
     *        deployed (but not yet undeployed) service unit.
     * @param serviceUnitRootPath path of the service unit artifact root, in
     *        platform specific format; must be non-null and non-empty.
     * @return deployment status message, which is an XML string that conforms
     *         to the <code>component-task-result</code> type from
     *         the schema given in the <i>MBean Status and Result Strings</i>
     *         section of the <i><b>Management</b></i> chapter of the JBI
     *         specification; must be non-null and non-empty.
     * @exception DeploymentException if undeployment operation is unsuccessful,
     *            or if the service unit is in an incorrect state.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
        throws DeploymentException
    {
        return messageUnDeployOkay();
    }

    ////////////////
    //implementation of JBI Framework ControllerMBean:
    ////////////////

    /**
     * Return current version and other info about this component.
     * @return info String
     */
    public String getComponentInfo()
    {
        //TODO:  add version info.
        return mModelName + ":  " + mJbiNameInfo.name();
    }

    /**
     * Return help text about this component.
     * @return help text about this component.
     */
    public String getHelp()
    {
        return "no soup for you";
    }

    /**
     * Return the JMX ObjectName for this component's DeployerMBean or null.
     * @return the JMX ObjectName for this component's DeployerMBean or null.
     */
    public ObjectName getDeployerMBean()
    {
        return mDeployerMBeanName;
    }

    /**
     * Return the JMX ObjectName for this component's LifeCycleMBean
     * or null.
     * @return the JMX ObjectName for this component's LifeCycleMBean
     * or null.
     */
    public ObjectName getLifeCycleMBean()
    {
        return mLifeCycleMBeanName;
    }

    /**
     * Return the ObjectName for LoggerMBean named <CODE>aLoggerName</CODE>,
     * or null, if the named logger does not exist in this component.
     * @return ObjectName of LoggerMBean.
     */
    public ObjectName getLoggerMBeanByName(String aLoggerName)
    {
        return mLoggerMBeanName;
    }

    /**
     * Return the ObjectName of the default LoggerMBean for this component.
     * @return ObjectName of default LoggerMBean or null.
     */
    public ObjectName getDefaultLoggerMBean()
    {
        return mLoggerMBeanName;
    }

    /**
     * Return the ObjectNames for all of the LoggerMBean's for this component.
     * @return array of ObjectName, possibly of zero length.
     */
    public ObjectName[] getLoggerMBeans()
    {
        ObjectName[] names = new ObjectName[1];
        names[0] = mLoggerMBeanName;

        return names;
    }

    /**
     * Return the componentName
     * @return the componentName
     */
    public String getcomponentName()
    {
        return mJbiNameInfo.name();
    }

    /** JMX object names for component-only mbeans: */
    protected ObjectName mDeployerMBeanName;
    protected ObjectName mControllerMBeanName;

    /**
     * create Object names for required ModelEngineComponent mbeans,
     * and populate the INITIAL & START state mbean sets.
     */
    private void initModelEngineMBeanNames()
    {
        /////////////
        //controller:
        /////////////
        mControllerMBeanName = mMBeanNames.getEngineMBeanName(
                mJbiNameInfo.name(),
                mMBeanNames.CONTROL_TYPE_CONTROLLER
            );
        mStartMBeans.add(
                mControllerMBeanName,
                com.sun.jbi.management.common.ControllerMBean.class,
                this
            );

        ///////////
        //deployer:
        ///////////
        mDeployerMBeanName = mMBeanNames.getEngineMBeanName(
                mJbiNameInfo.name(),
                mMBeanNames.CONTROL_TYPE_DEPLOYER
            );
        //note - we do not add our deployer mbean to the initial set,
        //as the implementation of $deployerMBean is managed by the framework.

        ////////////
        //lifecycle:
        ////////////
        mLifeCycleMBeanName = mMBeanNames.getEngineMBeanName(
                mJbiNameInfo.name(),
                mMBeanNames.CONTROL_TYPE_LIFECYCLE
            );
        //note - we do not add our lifecycle mbean to the initial set,
        //as the implementation of $lifecycleMBean is managed by the framework.

        /////////
        //logger:
        /////////
        mLoggerMBeanName = mMBeanNames.getEngineMBeanName(
                mJbiNameInfo.name(),
                mMBeanNames.CONTROL_TYPE_LOGGER
            );
        //note - we do not add our logger mbean to the initial set,
        //as the implementation of $loggerMBean is managed by the framework.
    }

    //allocate our broadcast support class:
    private NotificationBroadcasterSupport mEmitter = new NotificationBroadcasterSupport();

//Implementation for NotificationBroadcaster, extended by NotificationEmitter
    /**
     * Adds a listener to this MBean.
     *
     * @param listener The listener object which will handle the
     * notifications emitted by the broadcaster.
     * @param filter The filter object. If filter is null, no
     * filtering will be performed before handling notifications.
     * @param handback An opaque object to be sent back to the
     * listener when a notification is emitted. This object cannot be
     * used by the Notification broadcaster object. It should be
     * resent unchanged with the notification to the listener.
     *
     * @exception IllegalArgumentException Listener parameter is null.
     *
     * @see #removeNotificationListener
     */
    public void addNotificationListener(NotificationListener listener,
            NotificationFilter filter, Object handback)
        throws java.lang.IllegalArgumentException
    {
        mEmitter.addNotificationListener(listener, filter, handback);
    }

    /**
     * Removes a listener from this MBean.  If the listener
     * has been registered with different handback objects or
     * notification filters, all entries corresponding to the listener
     * will be removed.
     *
     * @param listener A listener that was previously added to this
     * MBean.
     *
     * @exception ListenerNotFoundException The listener is not
     * registered with the MBean.
     *
     * @see #addNotificationListener
     * @see NotificationEmitter#removeNotificationListener
     */
    public void removeNotificationListener(NotificationListener listener)
        throws ListenerNotFoundException
    {
        mEmitter.removeNotificationListener(listener);
    }

    /**
     * <p>Returns an array indicating, for each notification this
     * MBean may send, the name of the Java class of the notification
     * and the notification type.</p>
     *
     * <p>It is not illegal for the MBean to send notifications not
     * described in this array.  However, some clients of the MBean
     * server may depend on the array being complete for their correct
     * functioning.</p>
     *
     * @return the array of possible notifications.
     */
    public MBeanNotificationInfo[] getNotificationInfo()
    {
        return (new MBeanNotificationInfo[0]);
    }

//Implementaion for NotificationEmitter, which extends NotificationBroadcaster

    /**
     * <p>Removes a listener from this MBean.  The MBean must have a
     * listener that exactly matches the given <code>listener</code>,
     * <code>filter</code>, and <code>handback</code> parameters.  If
     * there is more than one such listener, only one is removed.</p>
     *
     * <p>The <code>filter</code> and <code>handback</code> parameters
     * may be null if and only if they are null in a listener to be
     * removed.</p>
     *
     * @param listener A listener that was previously added to this
     * MBean.
     * @param filter The filter that was specified when the listener
     * was added.
     * @param handback The handback that was specified when the listener was
     * added.
     *
     * @exception ListenerNotFoundException The listener is not
     * registered with the MBean, or it is not registered with the
     * given filter and handback.
     */
    public void removeNotificationListener(NotificationListener listener,
           NotificationFilter filter,
           Object handback)
        throws ListenerNotFoundException
    {
        mEmitter.removeNotificationListener(listener, filter, handback);
    }

    /**
     * Getter for local NotificationBroadcaster.
     *
     * @return NotificationBroadcaster
     */
    public NotificationBroadcasterSupport getNotificationBroadcaster()
    {
        return mEmitter;
    }

    private String mm_prolog = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><jbi-task version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/managementMessage\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://java.sun.com/xml/ns/managementMessage ./managementMessage.xsd\"><jbi-task-result><frmwk-task-result><frmwk-task-result-details><task-result-details>";
    private String mm_deploy_task = "<task-id>deploy</task-id>";
    private String mm_undeploy_task = "<task-id>undeploy</task-id>";
    private String mm_task_success = "<task-result>SUCCESS</task-result>";
    private String mm_task_details_1 = "</task-result-details><locale>en_US</locale></frmwk-task-result-details></frmwk-task-result><component-task-result><component-name>";
    private String mm_task_details_2 = "</component-name><component-task-result-details><task-result-details>";
    private String mm_epilog = "</task-result-details></component-task-result-details></component-task-result></jbi-task-result></jbi-task>";

    public String messageDeployOkay()
    {
        return  mm_prolog
                + mm_deploy_task
                + mm_task_success
                + mm_task_details_1
                + mJbiNameInfo.name()
                + mm_task_details_2
                + mm_deploy_task
                + mm_task_success
                + mm_epilog;
    }

    public String messageUnDeployOkay()
    {
        return  mm_prolog
                + mm_undeploy_task
                + mm_task_success
                + mm_task_details_1
                + mJbiNameInfo.name()
                + mm_task_details_2
                + mm_undeploy_task
                + mm_task_success
                + mm_epilog;
    }
}
