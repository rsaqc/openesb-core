/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
import com.sun.jbi.management.system.Util;

import java.io.File;

public class TestComponentValidator 
    extends junit.framework.TestCase
{

    private String mConfigDir = null;
    private String GOOD_COMP               = "component.zip";
    private String BAD_COMP                = "bad-component.zip";
    private String COMP_MISSING_CLASS      = "component-missing-classname.zip";
    private String COMP_MISSING_BOOTCLASS  = "component-missing-bootclassname.zip";
    private String COMP_EMPTY_BOOT_PATH    = "component-empty-bootclasspath.zip";
    private String COMP_EMPTY_CLASS_PATH   = "component-empty-componentclasspath.zip";
    private Validator mValidator;

    public TestComponentValidator (String aTestName)
        throws Exception
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/target/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/target/regress/testdata/";
        }
      
        ValidatorFactory.setDescriptorValidation(true);
        
        ScaffoldEnvironmentContext envCtx = new ScaffoldEnvironmentContext();
        envCtx.setJbiInstallRoot(System.getProperty("junit.as8base") + "/jbi");
        com.sun.jbi.util.EnvironmentAccess.setContext(envCtx);
        //this is a bad dependency on ManagementContext. 
        //This should be fixed when validation logic is removed from Archive
        Util.createManagementContext(); 
        
        mValidator = ValidatorFactory.createValidator(envCtx, ValidatorType.COMPONENT);

    }

    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    public void tearDown()
        throws Exception
    {
    }

    
    /**
     * Test with a good component
     */
    public void testValidateComponentGood()
           throws Exception
    {
        File testComp = new File(mConfigDir, GOOD_COMP); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            fail("Unexpected exception " + ex.getMessage());
        }
    }
    
    /**
     * Test with a bad component missing a jbi.xml
     */
    public void testValidateComponentBad()
           throws Exception
    {
        File testComp = new File(mConfigDir, BAD_COMP); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            assertTrue(ex.getMessage().contains("JBI_ADMIN_NO_DESCRIPTOR_IN_ARCHIVE"));
        }
    }
    
    /**
     * Test the case when the component life cycle class is missing
     */
    public void testValidateComponentMissingComponentClass()
           throws Exception
    {
        File testComp = new File(mConfigDir, COMP_MISSING_CLASS); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            assertTrue(ex.getMessage().contains("validateDescriptor"));
            assertTrue(ex.getMessage().contains("FAILED"));
            assertTrue(ex.getMessage().contains("JBI_ADMIN_MISSING_COMPONENT_LIFECYCLE"));
        }
    }
    
    /**
     * Test the case when the bootstrap class is missing
     */
    public void testValidateComponentMissingBootstrapClass()
           throws Exception
    {
        File testComp = new File(mConfigDir, COMP_MISSING_BOOTCLASS); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            assertTrue(ex.getMessage().contains("validateDescriptor"));
            assertTrue(ex.getMessage().contains("FAILED"));
            assertTrue(ex.getMessage().contains("JBI_ADMIN_MISSING_COMPONENT_BOOTSTRAP"));
        }
    }
    
    
    /**
     * Test the case when the bootstrap class is missing
     */
    public void testValidateComponentMissingBootstrapClassPath()
           throws Exception
    {
        File testComp = new File(mConfigDir, COMP_EMPTY_BOOT_PATH); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            System.out.println(ex.getMessage());
            assertTrue(ex.getMessage().contains("validateDescriptor"));
            assertTrue(ex.getMessage().contains("FAILED"));
            assertTrue(ex.getMessage().contains("JBI_ADMIN_EMPTY_BOOTSTRAP_CLASSPATH"));
        }
    }
    
    /**
     * Test the case when the bootstrap class is missing
     */
    public void testValidateComponentMissingComponentClassPath()
           throws Exception
    {
        File testComp = new File(mConfigDir, COMP_EMPTY_CLASS_PATH); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            System.out.println(ex.getMessage());
            assertTrue(ex.getMessage().contains("validateDescriptor"));
            assertTrue(ex.getMessage().contains("FAILED"));
            assertTrue(ex.getMessage().contains("JBI_ADMIN_EMPTY_LIFECYCLE_CLASSPATH"));
        }
    }
}
