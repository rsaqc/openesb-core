/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestXmlReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import org.xml.sax.EntityResolver;

import com.sun.jbi.management.internal.support.XmlReader;

/**
 * Tests Xml Parser/validation.
 * @author Sun Microsystems, Inc.
 */

public class TestXmlReader
    extends junit.framework.TestCase
{

    /**
     * one sample jbi.xml filename
     */
    private String mConfig1 = null;

    /**
     * another sample jbi.xml filename
     */
    private String mConfig2 = null;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestXmlReader(String aTestName)
    {
        super(aTestName);
        String testPath = System.getProperty("junit.srcroot") + "/runtime/manage/";
        mConfig1 = testPath + "/regress/support/binding1.xml";
        mConfig2 = testPath + "/regress/support/flawed1.xml";
        
        // Instantiate an Environment Context instance and  set the context in EnvironmentAccess
        ScaffoldEnvironmentContext envCtx = new ScaffoldEnvironmentContext();
        
        envCtx.setJbiInstanceRoot(testPath + "/target/");
        envCtx.setAppServerInstanceRoot(testPath + "/target/");
        envCtx.setJbiInstallRoot(System.getProperty("junit.as8base") + "/jbi");
        com.sun.jbi.util.EnvironmentAccess.setContext(envCtx);
    }
    
    /**
     * Test entity resolver.
     * @throws Exception if an unexpected error occurs
     */
    public void testEntityLoader()
        throws Exception
    {
        String testname = "testEntityLoader";
        try 
        {
            // -- Test the resolveEntity method
            EntityResolver er = new XmlReader(); 
            System.out.println("er.resolver.resolveEntity: " +
                er.resolveEntity(null, "./regress/jbi.xsd"));
            System.out.println("er.resolver.resolveEntity: " +
                er.resolveEntity(null, "./jbi.xsd"));
        }
        catch (Exception anEx)
        {
            anEx.printStackTrace();
            fail(testname + ": failed due to -" + anEx.getMessage());
            throw anEx;
        }
    }
    
    /**
     * Test the load without any validation.
     * @throws Exception if an unexpected error occurs
     */
    public void testLoadAndParse()
        throws Exception
    {
        String testname = "testLoadAndParse";
        try 
        {
            // -- Test the loadAndParse method
            XmlReader xmlReader = new XmlReader(); 
            String doc = xmlReader.loadAndParse(mConfig1, true);
            assertNotNull("The Document was not loaded correctly.", doc); 
        }
        catch (Exception anEx)
        {
            anEx.printStackTrace();
            fail(testname + ": failed due to -" + anEx.getMessage());
            throw anEx;
        }
    }
    
    /**
     * Test the load with validation.
     * @throws Exception if an unexpected error occurs
     */
    public void testLoadWithValidate()
        throws Exception
    {
        String testname = "testLoadWithValidate";
        try 
        {
            // -- Test the validate method
            XmlReader xmlReader = new XmlReader(); 
            boolean valid = xmlReader.validate(mConfig1);
            assertTrue("The Document did not get validated.", valid);
            String doc = xmlReader.loadAndParse(mConfig1, true);
            assertNotNull("The Document was not loaded correctly.", doc); 
        }
        catch (Exception anEx)
        {
            anEx.printStackTrace();
            fail(testname + ": failed due to -" + anEx.getMessage());
            throw anEx;
        }
    }
    
    /**
     * Test the load (for a flawed jbi.xml) without any validation.
     * @throws Exception if an unexpected error occurs
     */
    public void testBadLoadAndParse()
        throws Exception
    {
        String testname = "testBadLoadAndParse";
        try 
        {
            // -- Test the loadAndParse method
            XmlReader xmlReader = new XmlReader(); 
            String doc = xmlReader.loadAndParse(mConfig2, true);
            assertNull("Document was not flagged as invalid.", doc); 
        }
        catch (Exception anEx)
        {
            anEx.printStackTrace();
            fail(testname + ": failed due to -" + anEx.getMessage());
            throw anEx;
        }
    }
    
    /**
     * Test the load (for a flawed jbi.xml) with validation.
     * @throws Exception if an unexpected error occurs
     */
    public void testBadValidate()
        throws Exception
    {
        String testname = "testBadValidate";
        try 
        {
            // -- Test the validate method
            XmlReader xmlReader = new XmlReader(); 
            boolean valid = xmlReader.validate(mConfig2);
            fail("The Document got incorrectly validated.");
        }
        catch (Exception anEx)
        {
            // expect this exception -- ignore it.
            assertTrue("caught expected exception", true);
        }
    }
}
