/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestXmlReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.io.File;

import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryBuilder;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.repository.Archive;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.system.DeploymentService;
import com.sun.jbi.management.system.Util;
import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
/**
 * Tests some methods in DeploymentService
 * @author Sun Microsystems, Inc.
 */

public class TestDeploymentService
    extends junit.framework.TestCase
{

    private DeploymentService   mDeploymentSvc;
    private Repository          mRepository;
    private ManagementContext   mCtx;
    private String              mJbiRoot;
    private File mRegFile;
    private File mRegBkupFile;
    
    private static final String SERVICE_ASSEMBLY_ARCHIVE =  
            "service-assembly.zip";
    
    private static final String SERVICE_ASSEMBLY_ARCHIVE_PATH =
            "/test-classes/testdata/" + SERVICE_ASSEMBLY_ARCHIVE;    
    
    private static final String JBI_ROOT_PATH =
            "/target";
    
    private Registry mRegistry;

            
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestDeploymentService(String aTestName) throws Exception
    {
        super(aTestName);
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        String mConfigDir = srcroot + manage + "/target/test-classes/testdata/";
        String mRegFilePath        = mConfigDir + File.separator + "jbi-registry.xml";
        String mRegBkupFilePath    = mConfigDir + File.separator + "jbi-registry-backup.xml";        
        mRegFile = new File(mRegFilePath);
        mRegBkupFile = new File(mRegBkupFilePath);        
        mJbiRoot = srcroot + manage + JBI_ROOT_PATH;
        ScaffoldEnvironmentContext envCtx = new ScaffoldEnvironmentContext();
   
        mRegistry = Util.createRegistry();
        mRepository = new Repository(mCtx);               
        
       
        envCtx.setJbiInstanceRoot(mJbiRoot);
        envCtx.setAppServerInstanceRoot(mJbiRoot);
        envCtx.setProperty("com.sun.jbi.home", System.getProperty("junit.as8base") + "/jbi");
        envCtx.setJbiInstallRoot(System.getProperty("junit.as8base") + "/jbi");    
        envCtx.setRegistry(mRegistry);
        com.sun.jbi.util.EnvironmentAccess.setContext(envCtx);
        
                 
        
        mCtx = new ManagementContext(envCtx);
        mDeploymentSvc = new DeploymentService(mCtx);

            

    }
    
    /**
     * Test entity resolver.
     * @throws Exception if an unexpected error occurs
     */
    public void testGetServiceAssemblyDescriptor()
        throws Exception
    {

        try {
            
        Updater updater = mRegistry.getUpdater();
        updater.addServiceAssembly("CompositeApplication",  "service-assembly.zip", java.util.Calendar.getInstance());

        String path  = mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH;
        Archive archive = mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, path);       
        
        String descriptor = 
                mDeploymentSvc.getServiceAssemblyDescriptor("CompositeApplication");

        assertTrue(descriptor.startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?"));
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /** 
     * Test the code that creates a component-task-result from a plain-text exception message
     */
    public void testGetComponentResultXmlFragment()
        throws Exception
    {
        Class clazz = mDeploymentSvc.getClass();

        java.lang.reflect.Method
                mtd = clazz.getDeclaredMethod("getComponentResultXmlFragment",
                    new Class[]{java.lang.String.class, java.lang.String.class, java.lang.Exception.class} );
        mtd.setAccessible(true);
        Object elem = mtd.invoke(mDeploymentSvc, new Object[]{"myTask", "myComponent", new Exception("My exception message.")});
        assertNotNull(elem);
        org.w3c.dom.Element element = (org.w3c.dom.Element) elem;
        System.out.println(element.getTagName());
        assertTrue("component-task-result".equals(element.getTagName()));

    }    
    
    /**
     * Test fix for Issue 52 : https://open-esb.dev.java.net/issues/show_bug.cgi?id=52
     * An exception with a message string less than 9 chars should not result in a NPE.
     */
    public void testBuildComponentExMsg()
        throws Exception
    {
        Class clazz = mDeploymentSvc.getClass();

        java.lang.reflect.Method
                mtd = clazz.getDeclaredMethod("buildComponentExMsg",
                    new Class[]{java.lang.String.class, java.lang.String.class, java.lang.Exception.class} );
        mtd.setAccessible(true);
        Object elem = mtd.invoke(mDeploymentSvc, new Object[]{"myTask", "myComponent", new Exception("null")});
        assertNotNull(elem);
        org.w3c.dom.Element element = (org.w3c.dom.Element) elem;
        System.out.println(element.getTagName());
        assertTrue("component-task-result".equals(element.getTagName()));
    }
    
    /**
     * Test fix for Issue 52 : https://open-esb.dev.java.net/issues/show_bug.cgi?id=52
     * A message string less than 9 chars should not result in a exception.
     */
    public void testBuildFrmwkInfoMsg()
        throws Exception
    {
        Class clazz = mDeploymentSvc.getClass();

        java.lang.reflect.Method
                mtd = clazz.getDeclaredMethod("buildFrmwkInfoMsg",
                    new Class[]{java.lang.String.class} );
        mtd.setAccessible(true);
        Object elem = mtd.invoke(mDeploymentSvc, new Object[]{"abc"});
        assertNotNull(elem);
        org.w3c.dom.Element element = (org.w3c.dom.Element) elem;
        System.out.println(element.getTagName());
        org.w3c.dom.NodeList list = element.getChildNodes();
        assertTrue(list.getLength() > 0);
        assertEquals("msg-loc-info", list.item(0).getNodeName());    
    }    

    public void setUp()
        throws Exception
    {
        super.setUp();
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        if ( mRegBkupFile.exists())
        {
            mRegBkupFile.delete();
        }
    }

    public void tearDown()
        throws Exception
    {
        // -- restore registry.xml
       RegistryBuilder.destroyRegistry(); 
    }
    


}


