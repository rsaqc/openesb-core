/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDirectoryUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;

import java.io.File;
import java.io.FileInputStream;

public class TestDirectoryUtil extends junit.framework.TestCase
{
    private final static String TEST_ROOT = System.getProperty("junit.srcroot") + 
            "/runtime/manage/target/test-classes/dir-util";
    
    private final static String CHILD_DIR_PATH = "parent/child";
    private final static String TEST_FILE_NAME = "foo.txt";
    
    private File mTestRoot;    
    
    public TestDirectoryUtil(String aTestName)
        throws Exception
    {
        super(aTestName);
        mTestRoot = new File(TEST_ROOT);   
    }
    
    public void setUp()
        throws Exception
    {
        super.setUp();     
        mTestRoot.mkdir();
    }

    public void tearDown()
        throws Exception
    {
        try
        {
            DirectoryUtil.deleteDir(TEST_ROOT);
        }
        catch (Exception ex)
        {
            System.out.println("tearDown failed to remove " + TEST_ROOT);
        }
    }
    
    /** Happy path for deleteDir(). 
     */
    public void testDeleteDir()
        throws Exception
    {
        File dir = createTestDir("testDeleteDir");
        
        // sanity check
        assertTrue(dir.exists());
        
        // delete the directory
        DirectoryUtil.deleteDir(dir.getAbsolutePath());
        
        // it should be gone now
        assertFalse(dir.exists());
    }    
    
    /** Failure path for deleteDir() - file handle still open.  I'm pretty sure
     *  this will only fail on windows, so this test is kind of tricky.  If
     *  the deletion failed, an exception should be thrown.  If the deletion
     *  succeeded, then an exception should not be thrown.
     */
    public void testDeleteDirBad()
        throws Exception
    {
        File dir = createTestDir("testDeleteDirBad");
        FileInputStream fis = openFileStream(dir.getAbsolutePath());
        boolean deletionFailed = false;
        
        // sanity check
        assertTrue(dir.exists());
        
        // delete the directory (this should fail on windows)
        try
        {
            DirectoryUtil.deleteDir(dir.getAbsolutePath());
        }
        catch (Exception ex)
        {
            deletionFailed = true;
            // print the exception so we can see what's going on in the junit output
            System.out.println("testDeleteDirBad: " + ex.toString());
        }
        
        if (deletionFailed)
        {
            // Deletion failed, so the directory should still exist
            assertTrue(dir.exists());
        }
        else
        {
            // No exception thrown, so the directory should be toast
            assertFalse(dir.exists());
        }
        
        try
        {
            fis.close();
        }
        catch (Exception ex) { /* unimportant */ }
    }
    
    /** Happy path for removeDir(). 
     */
    public void testRemoveDir()
        throws Exception
    {
        File dir = createTestDir("testRemoveDir");
        
        // sanity check
        assertTrue(dir.exists());
        
        // delete the directory
        DirectoryUtil.removeDir(dir.getAbsolutePath());
        
        // it should be gone now
        assertFalse(dir.exists());
    }    
    
    /** Failure path for removeDir() - file handle still open.  I'm pretty sure
     *  this will only fail on windows, so this test is kind of tricky.  If
     *  the deletion failed, the directory should be marked for deletion.  If 
     *  the deletion succeeded, then an exception should not be thrown.
     */
    public void testRemoveDirBad()
        throws Exception
    {
        File dir = createTestDir("testRemoveDirBad");
        FileInputStream fis = openFileStream(dir.getAbsolutePath());
        boolean removed;
        
        // sanity check
        assertTrue(dir.exists());
        
        // remove the directory (this should fail on windows)
        removed = DirectoryUtil.removeDir(dir.getAbsolutePath());
        
        if (removed)
        {
            // It should be removed
            assertFalse(dir.exists());
        }
        else
        {
            // Removal failed, so the directory should still exist
            assertTrue(dir.exists());
            // And it should be marked now
            assertTrue(DirectoryUtil.isMarked(dir));
        }
                
        try
        {
            fis.close();
        }
        catch (Exception ex) { /* unimportant */ }
    }
    
    /** Happy-path tests for markDir() and isMarked(). */
    public void testMarkDir()
        throws Exception
    {        
        File dir = createTestDir("testMarkDir");
        
        // sanity check
        assertTrue(dir.exists());
        
        // mark the directory and test it
        DirectoryUtil.markDir(dir.getAbsolutePath());        
        assertTrue(DirectoryUtil.isMarked(dir));
    }
    
    /** Negative tests for markDir() and isMarked(). */
    public void testMarkDirBad()
        throws Exception
    {        
        File dir = createTestDir("testMarkDirBad");
        
        // sanity check
        assertTrue(dir.exists());
        
        // don't mark the directory and test it
        assertFalse(DirectoryUtil.isMarked(dir));
    }
    
    /** Happy-path test for removeMarkedDirs().  Test to make sure it deletes
     *  what it's supposed to and leaves the other stuff alone.
     */
    public void testRemoveMarkedDirs()
        throws Exception
    {
        File topDir      = new File(mTestRoot, "testRemoveMarkedDirs");
        File markedDir   = createTestDir("testRemoveMarkedDirs/marked");
        File unmarkedDir = createTestDir("testRemoveMarkedDirs/unmarked");
        
        // sanity check
        assertTrue(topDir.exists());
        assertTrue(markedDir.exists());
        assertTrue(unmarkedDir.exists());
        
        // mark one directory and run cleanup
        DirectoryUtil.markDir(markedDir.getAbsolutePath());
        DirectoryUtil.removeMarkedDirs(topDir.getAbsolutePath());
        
        // make sure the right stuff was removed
        assertTrue(topDir.exists());
        assertTrue(unmarkedDir.exists());
        assertFalse(markedDir.exists());
    }
    
    /** Various tests for testGetFolderNameListing() */
    public void testGetFolderNameListing()
        throws Exception
    {   
        String[] list;
        
        // test an empty dir
        File emptyDir = new File(mTestRoot, "testGetFolderNameListing");
        emptyDir.mkdir();        
        list = DirectoryUtil.getFolderNameListing(emptyDir);
        assertTrue(list != null && list.length == 0);
        
        // create the test dir which has one immediate child directory
        File fullDir = createTestDir("testGetFolderNameListing");
        list = DirectoryUtil.getFolderNameListing(fullDir);
        assertTrue(list != null && list.length == 1);
    }
    
    /** ################  HELPER METHODS ###################### */
    
    /** Creates a set of test directories with some dummy files.
     *  @param dirName the directory root name under TEST_ROOT
     */
    private File createTestDir(String dirName)
        throws Exception
    {
        File root = new File(mTestRoot, dirName);
        File childDir = new File(root, CHILD_DIR_PATH);
        childDir.mkdirs();
        
        // create test files
        new File(root, TEST_FILE_NAME).createNewFile();
        new File(childDir, TEST_FILE_NAME).createNewFile();
        
        return root;
    }
    
    /** Opens a stream to a newly-created file in a test directory.  This should
     *  prevent deletion on windows-based platforms.
     */
    private FileInputStream openFileStream(String rootPath)
        throws Exception
    {
        return new FileInputStream(rootPath + "/" + CHILD_DIR_PATH + "/" + TEST_FILE_NAME);
    }
    
}
