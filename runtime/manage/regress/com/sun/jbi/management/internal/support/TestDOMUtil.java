/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDOMUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;

import java.io.File;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TestDOMUtil extends junit.framework.TestCase
{    
    private static final String DOC_PATH =
            "/testdata/DOMUtil-ns-good.xml";    
    private static final String ELEMENT_NAME = "nselement";    
    private static final String ATTRIBUTE_NAME = "qname";
    private static final String ATTRIBUTE_VAL = "myattribute";
    private static final String NS_URI = "http://mynamespace.com/n1";
    private static final String NS_PREFIX = "n1";
    private static final String ROOT_NODE_NAME = "good-namespace";
    private static final QName  ATTR_VAL_QNAME = new QName(NS_URI, ATTRIBUTE_VAL);
    
    private DocumentBuilder mBuilder;
    private File            mDocFile;
    private Document        mDoc;
    private DOMUtil         mUtil;
    
    public TestDOMUtil (String aTestName)
        throws Exception
    {
        super(aTestName);
        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        
        mBuilder    = dbf.newDocumentBuilder();        
        mUtil       = new DOMUtil();

        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage/target/test-classes";   // open-esb build

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/manage/target/regress";           // mainline/whitney build
        }

        mDocFile    = new File(srcroot + manage + DOC_PATH);
    }
    
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mDoc = mBuilder.parse(mDocFile);
    }

    public void tearDown()
        throws Exception
    {

    }
    
    public void testGetNamespace()
        throws Exception
    {
        Element element;
        String  ns;
        
        element = (Element)mDoc.getElementsByTagName(ELEMENT_NAME).item(0);
        
        ns = mUtil.getNamespace(element, element.getAttribute(ATTRIBUTE_NAME));
        
        assertEquals(ns, NS_URI);
    }
    
    public void testGetQualifiedAttributeValue()
        throws Exception
    {        
        Element  element;
        QName    qname;
        
        element = (Element)mDoc.getElementsByTagName(ELEMENT_NAME).item(0);
        qname   = mUtil.getQualifiedAttributeValue(element, ATTRIBUTE_NAME);
        
        assertEquals(qname, ATTR_VAL_QNAME);
    }
    
    /**
     * This method is used to test the areDocumentsEqual method
     * @throws Exception if the test could not be executed
     */
    public void testIsElementEqual()
        throws Exception
    {        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage/target/test-classes";          
        File desc1 = new File(srcroot + manage, "testdata/ComponentDescriptor1.xml");
        File desc2 = new File(srcroot + manage, "testdata/ComponentDescriptor2.xml");
        File desc3 = new File(srcroot + manage, "testdata/ComponentDescriptor3.xml");
        File desc4 = new File(srcroot + manage, "testdata/ComponentDescriptor4.xml");        
        
        //descriptor1 and descriptor2 differ in many things but not in component name
        assertTrue(mUtil.areElementsEqual(desc1, desc2, "/jbi:jbi/jbi:component/jbi:identification/jbi:name"));
        
        //descriptor1 and descriptor3 have different component names
        assertFalse(mUtil.areElementsEqual(desc1, desc3, "/jbi:jbi/jbi:component/jbi:identification/jbi:name"));
        
        //descriptor1 and descriptor4 are the same but with different namespace prefix        
        assertTrue(mUtil.areElementsEqual(desc1, desc4, "/jbi:jbi/jbi:component/jbi:identification/jbi:name"));        
        
    }   
    
    /**
     * Test ElementToString operation
     */
    public void testElementToString()
        throws Exception
    {
        Element element = (Element)mDoc.getElementsByTagName(ROOT_NODE_NAME).item(0);
        String elementStr = mUtil.elementToString(element);
        
        assertTrue(elementStr.indexOf(ELEMENT_NAME) != -1);
    }
    
}
