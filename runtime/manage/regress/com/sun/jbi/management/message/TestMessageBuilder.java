/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMessageBuilder.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.message;

import com.sun.jbi.management.ComponentMessageHolder;
import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
import com.sun.jbi.management.system.ManagementException;

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

public class TestMessageBuilder 
    extends junit.framework.TestCase
{

    private static final String RESPONSE_1 =  
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
            "<jbi-task xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\" " +
                      "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" " +
                      "xsi:schemaLocation=\"http://java.sun.com/xml/ns/jbi/management-message ./managementMessage.xsd\"> " +
              "<jbi-task-result>" +
            "<frmwk-task-result>" +
                "<frmwk-task-result-details>" +
                    "<task-result-details>"+
                        "<task-id>deploy</task-id>"+
                        "<task-result>SUCCESS</task-result>" +
                        "<message-type>INFO</message-type>"+
                        "<task-status-msg>" +
                            "<msg-loc-info><loc-token>JBIMA0495</loc-token><loc-message>Deployment of service assembly esbadmin00089-sa succeeded.</loc-message></msg-loc-info>"+
                        "</task-status-msg>" +
                      "</task-result-details>" +
                      "<locale>en_US</locale>" +
                  "</frmwk-task-result-details>" +
              "</frmwk-task-result>" +
              "<component-task-result>" +
                "<component-name>manage-binding-1</component-name>" +
                    "<component-task-result-details>" +
                        "<task-result-details><task-id>deploy</task-id><task-result>SUCCESS</task-result></task-result-details>" +
                    "</component-task-result-details>" +
              "</component-task-result>" +
              "<component-task-result>" +
                "<component-name>manage-binding-2</component-name>" +
                    "<component-task-result-details>" +
                        "<task-result-details><task-id>deploy</task-id><task-result>SUCCESS</task-result></task-result-details>" +
                    "</component-task-result-details>" +
              "</component-task-result>" +
          "</jbi-task-result></jbi-task>";
    
    private static final String SAME_COMPONENT_RESPONSE_1 =  
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
            "<jbi-task xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\" " +
                      "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" " +
                      "xsi:schemaLocation=\"http://java.sun.com/xml/ns/jbi/management-message ./managementMessage.xsd\"> " +
              "<jbi-task-result>" +
            "<frmwk-task-result>" +
                "<frmwk-task-result-details>" +
                    "<task-result-details>"+
                        "<task-id>deploy</task-id>"+
                        "<task-result>SUCCESS</task-result>" +
                        "<message-type>INFO</message-type>"+
                        "<task-status-msg>" +
                            "<msg-loc-info><loc-token>JBIMA0495</loc-token><loc-message>Deployment of service assembly esbadmin00089-sa succeeded.</loc-message></msg-loc-info>"+
                        "</task-status-msg>" +
                      "</task-result-details>" +
                      "<locale>en_US</locale>" +
                  "</frmwk-task-result-details>" +
              "</frmwk-task-result>" +
              "<component-task-result>" +
                "<component-name>manage-binding-1</component-name>" +
                    "<component-task-result-details>" +
                        "<task-result-details>"+
                            "<task-id>deploy</task-id>"+
                            "<task-result>SUCCESS</task-result>" +
                            "<message-type>INFO</message-type>"+
                            "<task-status-msg>" +
                                "<msg-loc-info><loc-token>JBIMA0495</loc-token><loc-message>Deployment of service unit su11 succeeded.</loc-message></msg-loc-info>"+
                            "</task-status-msg>" +
                      "</task-result-details>" +
                    "</component-task-result-details>" +
              "</component-task-result>" +
              "<component-task-result>" +
                "<component-name>manage-binding-1</component-name>" +
                    "<component-task-result-details>" +
                        "<task-result-details>"+
                            "<task-id>deploy</task-id>"+
                            "<task-result>SUCCESS</task-result>" +
                            "<message-type>INFO</message-type>"+
                            "<task-status-msg>" +
                                "<msg-loc-info><loc-token>JBIMA0495</loc-token><loc-message>Deployment of service unit su12 succeeded.</loc-message></msg-loc-info>"+
                            "</task-status-msg>" +
                      "</task-result-details>" +
                    "</component-task-result-details>" +
              "</component-task-result>" +
          "</jbi-task-result></jbi-task>";

    private static final String SAME_COMPONENT_RESPONSE_2 =  
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
            "<jbi-task xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\" " +
                      "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" " +
                      "xsi:schemaLocation=\"http://java.sun.com/xml/ns/jbi/management-message ./managementMessage.xsd\"> " +
              "<jbi-task-result>" +
            "<frmwk-task-result>" +
                "<frmwk-task-result-details>" +
                    "<task-result-details>"+
                        "<task-id>deploy</task-id>"+
                        "<task-result>SUCCESS</task-result>" +
                        "<message-type>INFO</message-type>"+
                        "<task-status-msg>" +
                            "<msg-loc-info><loc-token>JBIMA0495</loc-token><loc-message>Deployment of service assembly esbadmin00089-sa succeeded.</loc-message></msg-loc-info>"+
                        "</task-status-msg>" +
                      "</task-result-details>" +
                      "<locale>en_US</locale>" +
                  "</frmwk-task-result-details>" +
              "</frmwk-task-result>" +
              "<component-task-result>" +
                "<component-name>manage-binding-1</component-name>" +
                    "<component-task-result-details>" +
                        "<task-result-details>"+
                            "<task-id>deploy</task-id>"+
                            "<task-result>SUCCESS</task-result>" +
                            "<message-type>INFO</message-type>"+
                            "<task-status-msg>" +
                                "<msg-loc-info><loc-token>JBIMA0495</loc-token><loc-message>Deployment of service unit su1 succeeded.</loc-message></msg-loc-info>"+
                            "</task-status-msg>" +
                      "</task-result-details>" +
                    "</component-task-result-details>" +
              "</component-task-result>" +
              "<component-task-result>" +
                "<component-name>manage-binding-1</component-name>" +
                    "<component-task-result-details>" +
                        "<task-result-details>"+
                            "<task-id>deploy</task-id>"+
                            "<task-result>SUCCESS</task-result>" +
                            "<message-type>INFO</message-type>"+
                            "<task-status-msg>" +
                                "<msg-loc-info><loc-token>JBIMA0495</loc-token><loc-message>Deployment of service unit su2 succeeded.</loc-message></msg-loc-info>"+
                            "</task-status-msg>" +
                      "</task-result-details>" +
                    "</component-task-result-details>" +
              "</component-task-result>" +
          "</jbi-task-result></jbi-task>";
    
    private static final String EXCEPTION_1 =
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><jbi-task version=\"1.0\" " +
        "xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">" +
        "<jbi-task-result>" +
            "<frmwk-task-result>" +
                "<frmwk-task-result-details>" +
                    "<task-result-details>" +
                        "<task-id>deploy</task-id>" +
                        "<task-result>FAILED</task-result>" +
                        "<message-type>ERROR</message-type>" +
                        "<task-status-msg>" +
                            "<msg-loc-info>" +
                                "<loc-token>JBIMA0000</loc-token>" +
                                "<loc-message>java.io.FileNotFoundException: /export2/nikita/jbi/main/open-esb/install/as8/nodeagents/agent1/cluster1-instance2/jbi/service-assemblies/esbadmin00089-sa/deploy-sa.jar (Permission denied)</loc-message>" +
                            "</msg-loc-info>" +
                        "</task-status-msg>" +
                        "<exception-info>" +
                            "<nesting-level>1</nesting-level>" +
                            "<msg-loc-info>" +
                            	"<loc-token>JBIMA0000</loc-token>" +
                                "<loc-message>java.io.FileNotFoundException: /export2/nikita/jbi/main/open-esb/install/as8/nodeagents/agent1/cluster1-instance2/jbi/service-assemblies/esbadmin00089-sa/deploy-sa.jar (Permission denied)</loc-message>" +
                            "</msg-loc-info>" +
                            "<stack-trace>com.sun.jbi.management.repository.Repository.addToRepository(Repository.java:419)" +
                                "com.sun.jbi.management.repository.Repository.addArchive(Repository.java:167)" +
                                "com.sun.jbi.management.system.DeploymentService.deploy(DeploymentService.java:219)" +
                                "sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)" +
                                "sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)" +
                                "sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)" +
                                "java.lang.reflect.Method.invoke(Method.java:585)" +
                                "com.sun.jmx.mbeanserver.StandardMetaDataImpl.invoke(StandardMetaDataImpl.java:414)" +
                                "javax.management.StandardMBean.invoke(StandardMBean.java:323)" +
                                "com.sun.jmx.mbeanserver.DynamicMetaDataImpl.invoke(DynamicMetaDataImpl.java:213)" +
                                "com.sun.jmx.mbeanserver.MetaDataImpl.invoke(MetaDataImpl.java:220)" +
                                "com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.invoke(DefaultMBeanServerInterceptor.java:815)" +
                                "com.sun.jmx.mbeanserver.JmxMBeanServer.invoke(JmxMBeanServer.java:784)" +
                                "sun.reflect.GeneratedMethodAccessor11.invoke(Unknown Source)" +
                                "sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)" +
                                "java.lang.reflect.Method.invoke(Method.java:585)" +
                                "com.sun.enterprise.admin.util.proxy.ProxyClass.invoke(ProxyClass.java:77)" +
                                "$Proxy1.invoke(Unknown Source)" +
                                "com.sun.enterprise.admin.server.core.jmx.SunoneInterceptor.invoke(SunoneInterceptor.java:297)" +
                                "javax.management.remote.rmi.RMIConnectionImpl.doOperation(RMIConnectionImpl.java:1408)" +
                                "javax.management.remote.rmi.RMIConnectionImpl.access$100(RMIConnectionImpl.java:81)" +
                                "javax.management.remote.rmi.RMIConnectionImpl$PrivilegedOperation.run(RMIConnectionImpl.java:1245)" +
                                "javax.management.remote.rmi.RMIConnectionImpl.doPrivilegedOperation(RMIConnectionImpl.java:1341)" +
                                "javax.management.remote.rmi.RMIConnectionImpl.invoke(RMIConnectionImpl.java:782)" +
                                "sun.reflect.GeneratedMethodAccessor104.invoke(Unknown Source)" +
                                "sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)" +
                                "java.lang.reflect.Method.invoke(Method.java:585)" +
                                "sun.rmi.server.UnicastServerRef.dispatch(UnicastServerRef.java:294)" +
                                "sun.rmi.transport.Transport$1.run(Transport.java:153)" +
                                "java.security.AccessController.doPrivileged(Native Method)" +
                                "sun.rmi.transport.Transport.serviceCall(Transport.java:149)" +
                                "sun.rmi.transport.tcp.TCPTransport.handleMessages(TCPTransport.java:466)" +
                                "sun.rmi.transport.tcp.TCPTransport$ConnectionHandler.run(TCPTransport.java:707)" +
                                "java.lang.Thread.run(Thread.java:595)" +
                                "</stack-trace></exception-info><exception-info><nesting-level>2</nesting-level><msg-loc-info><loc-token>JBIMA0000</loc-token><loc-message>/export2/nikita/jbi/main/open-esb/install/as8/nodeagents/agent1/cluster1-instance2/jbi/service-assemblies/esbadmin00089-sa/deploy-sa.jar (Permission denied)</loc-message></msg-loc-info><stack-trace>java.io.FileOutputStream.open(Native Method)" +
                                "java.io.FileOutputStream.&lt;init&gt;(FileOutputStream.java:179)" +
                                "java.io.FileOutputStream.&lt;init&gt;(FileOutputStream.java:131)" +
                                "com.sun.jbi.management.repository.Repository.addToRepository(Repository.java:402)" +
                                "com.sun.jbi.management.repository.Repository.addArchive(Repository.java:167)" +
                                "com.sun.jbi.management.system.DeploymentService.deploy(DeploymentService.java:219)" +
                                "sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)" +
                                "sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)" +
                                "sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)" +
                                "java.lang.reflect.Method.invoke(Method.java:585)" +
                                "com.sun.jmx.mbeanserver.StandardMetaDataImpl.invoke(StandardMetaDataImpl.java:414)" +
                                "javax.management.StandardMBean.invoke(StandardMBean.java:323)" +
                                "com.sun.jmx.mbeanserver.DynamicMetaDataImpl.invoke(DynamicMetaDataImpl.java:213)" +
                                "com.sun.jmx.mbeanserver.MetaDataImpl.invoke(MetaDataImpl.java:220)" +
                                "com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.invoke(DefaultMBeanServerInterceptor.java:815)" +
                                "com.sun.jmx.mbeanserver.JmxMBeanServer.invoke(JmxMBeanServer.java:784)" +
                                "sun.reflect.GeneratedMethodAccessor11.invoke(Unknown Source)" +
                                "sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)" +
                                "java.lang.reflect.Method.invoke(Method.java:585)" +
                                "com.sun.enterprise.admin.util.proxy.ProxyClass.invoke(ProxyClass.java:77)" +
                                "$Proxy1.invoke(Unknown Source)" +
                                "com.sun.enterprise.admin.server.core.jmx.SunoneInterceptor.invoke(SunoneInterceptor.java:297)" +
                                "javax.management.remote.rmi.RMIConnectionImpl.doOperation(RMIConnectionImpl.java:1408)" +
                                "javax.management.remote.rmi.RMIConnectionImpl.access$100(RMIConnectionImpl.java:81)" +
                                "javax.management.remote.rmi.RMIConnectionImpl$PrivilegedOperation.run(RMIConnectionImpl.java:1245)" +
                                "javax.management.remote.rmi.RMIConnectionImpl.doPrivilegedOperation(RMIConnectionImpl.java:1341)" +
                                "javax.management.remote.rmi.RMIConnectionImpl.invoke(RMIConnectionImpl.java:782)" +
                                "sun.reflect.GeneratedMethodAccessor104.invoke(Unknown Source)" +
                                "sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)" +
                                "java.lang.reflect.Method.invoke(Method.java:585)" +
                                "sun.rmi.server.UnicastServerRef.dispatch(UnicastServerRef.java:294)" +
                                "sun.rmi.transport.Transport$1.run(Transport.java:153)" +
                                "java.security.AccessController.doPrivileged(Native Method)" +
                                "sun.rmi.transport.Transport.serviceCall(Transport.java:149)" +
                                "sun.rmi.transport.tcp.TCPTransport.handleMessages(TCPTransport.java:466)" +
                                "sun.rmi.transport.tcp.TCPTransport$ConnectionHandler.run(TCPTransport.java:707)" +
                                "java.lang.Thread.run(Thread.java:595)" +
                                "</stack-trace>" +
                            "</exception-info>" +
                        "</task-result-details>" +
                    "<locale>en_US</locale>" +
                "</frmwk-task-result-details></frmwk-task-result>" +
            "</jbi-task-result></jbi-task>";
    
    private ScaffoldEnvironmentContext mEnvCtx;
    private JAXBContext         mJaxbContext;
     
    public TestMessageBuilder (String aTestName)
    {
        super(aTestName);
        mEnvCtx = new ScaffoldEnvironmentContext();
        
    }

    public void setUp()
        throws Exception
    {
        super.setUp();        
    }

    public void tearDown()
        throws Exception
    {
    }

    /**
     * @throws Exception if an unexpected error occurs
     */
    public void testBuildExceptionMessage()
           throws Exception
    {
        MessageBuilder msgBldr = new MessageBuilder(mEnvCtx.getStringTranslator("com.sun.jbi.management"));
        
        try
        {
            throw new IllegalArgumentException("The argumement state is of invalid type");
            
        }
        catch ( Exception ex )
        {
            String msg = msgBldr.buildExceptionMessage("testBuildExceptionMessage", ex);
            System.out.println(msg);
        }
    }
    
    /**
     * Test building a Framework message that has Component task results too.
     */
    public void testBuildFrameworkMessageWithComponentTaskMessages()
        throws Exception
    {
        int numComponents = 5;
        java.util.List<ComponentMessageHolder> 
            compMsgs = new java.util.ArrayList<ComponentMessageHolder>();
        
        for (int i=1; i<=numComponents; i++ )
        {
            ComponentMessageHolder 
            compMsg = new ComponentMessageHolder(MessageBuilder.MessageType.INFO.toString());
            compMsg.setComponentName("TestComponent" + i);
            compMsg.setTaskName("TestTask" + i);
            compMsg.setTaskResult(MessageBuilder.TaskResult.SUCCESS.toString());
            
            compMsgs.add(compMsg);
        }
        
        // Test
        MessageBuilder 
            msgBldr = new MessageBuilder(mEnvCtx.getStringTranslator("com.sun.jbi.management"));
        String message = msgBldr.buildFrameworkMessage(
            "someTask", MessageBuilder.TaskResult.SUCCESS, compMsgs);

        assertNotNull(message);
        System.out.println(message);
        assertTrue(message.indexOf("TestComponent3") != -1);
    }
    
    /**
     * Test building a composite message with a good response and a exception message
     * with a task result of FAILED. Expected composite message should be a SUCCESS 
     * with WARNING
     */
    public void testBuildCompositeMessage()
        throws Exception
    {
        java.util.Map<String, String> responseMap  = new java.util.HashMap();
        java.util.Map<String, Throwable> exceptionMap = new java.util.HashMap();
        
        responseMap.put("cluster1-instance1", RESPONSE_1);
        exceptionMap.put("cluster1-instance2", new Exception(EXCEPTION_1));
        
        // Test
        MessageBuilder 
            msgBldr = new MessageBuilder(mEnvCtx.getStringTranslator("com.sun.jbi.management"));
        
        com.sun.jbi.management.message.MessageBuilder.Message msg = 
            msgBldr.buildCompositeMessage("deploy", responseMap, exceptionMap, false) ;
        assertTrue(msg.getMessage().contains("WARNING"));
    }
    
    /**
     * Test building a composite message with the instance messages with two component
     * task results from the same component.
     */
    public void testBuildCompositeMessageSameComponent()
        throws Exception
    {
        java.util.Map<String, String> responseMap  = new java.util.HashMap();
        java.util.Map<String, Throwable> exceptionMap = new java.util.HashMap();
        
        responseMap.put("cluster1-instance1", SAME_COMPONENT_RESPONSE_1);
        responseMap.put("cluster1-instance2", SAME_COMPONENT_RESPONSE_2);
        
        // Test
        MessageBuilder 
            msgBldr = new MessageBuilder(mEnvCtx.getStringTranslator("com.sun.jbi.management"));
        
        com.sun.jbi.management.message.MessageBuilder.Message msg = 
            msgBldr.buildCompositeMessage("deploy", responseMap, exceptionMap, false) ;
        
       
       
        // Verify there are two component results for manage-binding-1
        JbiTaskResult jbiTask = getJbiTaskFromString(msg.getMessage());
        
        List<ComponentTaskResult> compResults = jbiTask.getJbiTaskResult().getComponentTaskResult();
        
        assertEquals(2, compResults.size());
        
        for (ComponentTaskResult compResult : compResults ) 
        {
            assertEquals("manage-binding-1", compResult.getComponentName());
            List<TaskStatusMsg> taskMsgs =     compResult.getComponentTaskResultDetails().
                getTaskResultDetails().getTaskStatusMsg();
            assertEquals(4, taskMsgs.size());
        }
    }
    
    /** Test for well-formed XML strings. */
    public void testIsXmlString()
        throws Exception
    {
        String xml1 = "<test></test>";
        String xml2 = "<test></test2>";
        String xml3 = "hello";
        
        assertTrue(MessageBuilder.isXmlString(RESPONSE_1));
        assertTrue(MessageBuilder.isXmlString(xml1));
        assertFalse(MessageBuilder.isXmlString(xml2));
        assertFalse(MessageBuilder.isXmlString(xml3));
    }
    
    
    /**
     * Test getting the token from a string less than 9 chars.
     * and is not a JBI localized message.
     */
    public void testGetMessageToken1()
        throws Exception
    {
        String msg = "abc";
        String token = MessageBuilder.getMessageToken(msg);
        
        assertEquals("JBIMA0000", token);
    }
    
    /**
     * Test getting the token from a string less than 9 chars, that starts with JBIUU 
     */
    public void testGetMessageToken2()
        throws Exception
    {
        
        String msg = "JBIU";
        String token = MessageBuilder.getMessageToken(msg);
        
        assertEquals("JBIMA0000", token);
    }
    
    /**
     * Test getting the message string when string less than 9 chars and is not a JBI
     * localized message.
     */
    public void testGetMessageString1()
        throws Exception
    {
        String msg = "JBIU";
        String str = MessageBuilder.getMessageString(msg);
        
        assertEquals("JBIU", str);
    }
    
    /**
     * Test getting the message string and token when string is 12 chars 
     */
    public void testGetMessageTokenAndString1()
        throws Exception
    {
        String msg = "JBIMA1234: A";
        String token = MessageBuilder.getMessageToken(msg);
        String str = MessageBuilder.getMessageString(msg);
        assertEquals("JBIMA1234", token);
        assertEquals("A", str);
    }
    
    
    /**
     * Test getting the message string and token when string is 9 chars.
     */
    public void testGetMessageTokenAndString2()
        throws Exception
    {
        String msg = "JBIMA1234: ";
        String token = MessageBuilder.getMessageToken(msg);
        String str = MessageBuilder.getMessageString(msg);
        assertEquals("JBIMA1234", token);
        assertEquals("", str);
    }
    
    /**
     *
     */
        private synchronized JAXBContext getJaxbContext()
        throws Exception
    {
        if ( mJaxbContext == null )
        {
            ClassLoader cl = 
                    Class.forName(
                        "com.sun.jbi.management.message.JbiTaskResult").
                        getClassLoader();
            mJaxbContext = JAXBContext.newInstance( "com.sun.jbi.management.message", cl);
        }
        
        return mJaxbContext;
    }
    
    private Object unmarshal(StreamSource input)
        throws Exception
    {
        Unmarshaller reader = getJaxbContext().createUnmarshaller();
        return reader.unmarshal(input);
    }
    
    
    /**
     * Get the JbiTask Message JAXB object from the message string version
     */
    private JbiTask getJbiTaskFromString(String jbiTaskMsg)
        throws ManagementException
    {
        try
        {
            StringBuffer strBuf = new StringBuffer(jbiTaskMsg);
            return  (JbiTask) unmarshal( new StreamSource( 
                new java.io.StringReader( strBuf.toString())));
        }
        catch (Exception jex )
        {
            throw new ManagementException(jex);
        }
    }
    
}
