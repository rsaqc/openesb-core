/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMessageHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.message;

import com.sun.jbi.management.system.Util;
import com.sun.jbi.management.ComponentMessageHolder;
import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
import java.util.logging.Level;

public class TestMessageHelper 
    extends junit.framework.TestCase
{

    private static String TAB = "\t"; 
    private static String NEWLINE = System.getProperty("line.separator");
    public TestMessageHelper (String aTestName)
    {
        super(aTestName);
        try 
        {
            Util.createManagementContext();        
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void setUp()
        throws Exception
    {
        super.setUp();        
    }

    public void tearDown()
        throws Exception
    {
    }

    /**
     * @throws Exception if an unexpected error occurs
     */
    public void testFrameworkMessageExtraction()
           throws Exception
    {

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><jbi-task version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">" +
                "<jbi-task-result><frmwk-task-result><frmwk-task-result-details><task-result-details><task-id>archiveEqualityCheck</task-id><task-result>FAILED</task-result>" +
                "<message-type>ERROR</message-type><task-status-msg><msg-loc-info><loc-token>JBIMA1327</loc-token>"+
                "<loc-message>Installation archive for component manage-binding-1 is not identical to the archive"+
                " which already exists in the domain. </loc-message><loc-param>manage-binding-1</loc-param></msg-loc-info>" +
                "</task-status-msg></task-result-details><locale>en</locale></frmwk-task-result-details></frmwk-task-result></jbi-task-result></jbi-task>";

        try
        {
            String message = MessageHelper.getMsgString(new Exception(xml));
            System.out.println(message);
            assertEquals("The strings are not equal",
                    NEWLINE + "Scaffolded String isTASK_ID archiveEqualityCheck failed" + NEWLINE +
                     TAB +"ERROR: " + NEWLINE +
                     TAB + "Installation archive for component manage-binding-1 is not identical to the archive which already exists in the domain. " + NEWLINE,
                    message);
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
    

    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><jbi-task xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\" "+
            "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" xsi:schemaLocation=\"http://java.sun.com/xml/ns/jbi/management-message " +
             "./managementMessage.xsd\"><jbi-task-result><frmwk-task-result><frmwk-task-result-details><task-result-details><task-id>rollbackSUInits</task-id>" +
            "<task-result>FAILED</task-result><message-type>ERROR</message-type><task-status-msg><msg-loc-info><loc-token>TEST_TOKEN</loc-token>"+
            "<loc-message>This is a test message"+
            " used for testing. </loc-message><loc-param>manage-binding-1</loc-param></msg-loc-info>" +
            "</task-status-msg><task-status-msg><msg-loc-info><loc-token>TEST_TOKEN</loc-token>"+
            "<loc-message>This is one more test message"+
            " used for testing. </loc-message><loc-param>manage-binding-1</loc-param></msg-loc-info>" +
            "</task-status-msg><exception-info><nesting-level>1</nesting-level><msg-loc-info><loc-token>WRONG_DOC</loc-token><loc-message>" +
            "ENT_ERR: A node is used in a different document than the one that created it.</loc-message></msg-loc-info><stack-trace>com.sun.org.apache.xerces." +
            "internal.dom.ParentNode.internalInsertBefore(ParentNode.java:389) " +
            "com.sun.org.apache.xerces.internal.dom.ParentNode.insertBefore(ParentNode.java:321)" +
            "sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)" +
            "..." +
            "com.sun.enterprise.web.connector.grizzly.ssl.SSLWorkerThread.run(SSLWorkerThread.java:93)" +
            "</stack-trace></exception-info></task-result-details><locale>en_US</locale></frmwk-task-result-details><is-cause-framework>YES</is-cause-framework></frmwk-task-result></jbi-task-result></jbi-task>";
    
   /**
     * @throws Exception if an unexpected error occurs
     */
    public void testFrameworkExceptionExtraction()
           throws Exception
    {

        try
        {
            String message = MessageHelper.getMsgString(new Exception(xml));
            System.out.println(message);
            assertEquals("The strings are not equal",
                    NEWLINE + "Scaffolded String isTASK_ID rollbackSUInits failed" + NEWLINE 
                    + TAB + "ERROR: " + NEWLINE
                    + TAB + "This is a test message used for testing. " + NEWLINE
                    + TAB + "This is one more test message used for testing. "+ NEWLINE 
                    + TAB + TAB + "Scaffolded String isEXCEPTION_INFO:" + NEWLINE
                    + TAB + TAB + TAB + "ENT_ERR: A node is used in a different document than the one that created it." + NEWLINE,
                    message);
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }    
    
   /**
     * @throws Exception if an unexpected error occurs
     */
    public void testFrameworkExceptionExtractionAtInfo()
           throws Exception
    {    
        try
        {        
            String messageWithTrace = MessageHelper.getMsgString(new Exception(xml), Level.INFO);
            System.out.println(messageWithTrace);
            assertEquals("The strings are not equal",
                    NEWLINE + "Scaffolded String isTASK_ID rollbackSUInits failed" + NEWLINE 
                    + TAB + "ERROR: " + NEWLINE
                    + TAB + "This is a test message used for testing. " + NEWLINE
                    + TAB + "This is one more test message used for testing. "+ NEWLINE 
                    + TAB + TAB + "Scaffolded String isEXCEPTION_INFO:" + NEWLINE
                    + TAB + TAB + TAB + "ENT_ERR: A node is used in a different document than the one that created it." + NEWLINE,
                    messageWithTrace);      
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }      
    }
    
    
   /**
     * @throws Exception if an unexpected error occurs
     */
    public void testFrameworkExceptionExtractionAtFiner()
           throws Exception
    {    
        try
        {        
            String messageWithTrace = MessageHelper.getMsgString(new Exception(xml), Level.FINER);
            System.out.println(messageWithTrace);
            assertEquals("The strings are not equal",
                    NEWLINE + "Scaffolded String isTASK_ID rollbackSUInits failed" + NEWLINE 
                    + TAB + "ERROR: " + NEWLINE
                    + TAB + "This is a test message used for testing. " + NEWLINE
                    + TAB + "This is one more test message used for testing. "+ NEWLINE 
                    + TAB + TAB + "Scaffolded String isEXCEPTION_INFO:" + NEWLINE
                    + TAB + TAB + TAB + "ENT_ERR: A node is used in a different document than the one that created it." + NEWLINE 
                    + TAB + TAB + TAB + "com.sun.org.apache.xerces.internal.dom.ParentNode.internalInsertBefore(ParentNode.java:389) com.sun.org.apache.xerces.internal.dom.ParentNode.insertBefore(ParentNode.java:321)sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)...com.sun.enterprise.web.connector.grizzly.ssl.SSLWorkerThread.run(SSLWorkerThread.java:93)" + NEWLINE,                    
                    messageWithTrace);      
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }      
    }    
    
    public void testComponentMessageExtraction()
           throws Exception
    {

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><jbi-task xmlns=\"http://java.sun.com/xml/ns/" +
                "jbi/management-message\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" xsi:schemaLocation=\"http://" +
                "java.sun.com/xml/ns/jbi/management-message ./managementMessage.xsd\"><jbi-task-result><frmwk-task-result>" +
                "<frmwk-task-result-details><task-result-details><task-id>start</task-id><task-result>SUCCESS</task-result><message-type>WARNING</message-type>" +
                "<task-status-msg><msg-loc-info><loc-token>TEST_TOKEN</loc-token>"+
                "<loc-message>This is a test message"+
                " used for testing. </loc-message><loc-param>manage-binding-1</loc-param></msg-loc-info>" +
                "</task-status-msg> "+
                "</task-result-details><locale>en_US</locale></frmwk-task-result-details></frmwk-task-result><component-task-result>" +
                "<component-name>manage-binding-1</component-name><component-task-result-details><task-result-details><task-id>start</task-id>" +
                "<task-result>FAILED</task-result><message-type>ERROR</message-type><task-status-msg><msg-loc-info><loc-token>JBIWHOOPS</loc-token>" +
                "<loc-message>Service unit esbadmin00089-su-1 start failed</loc-message></msg-loc-info></task-status-msg></task-result-details>" +
                "</component-task-result-details></component-task-result><component-task-result><component-name>manage-binding-2</component-name>" +
                "<component-task-result-details><task-result-details><task-id>start</task-id><task-result>SUCCESS</task-result><message-type>" +
                "INFO</message-type><task-status-msg><msg-loc-info><loc-token>JBIMA0500</loc-token><loc-message>" +
                "Lifecycle operation start succeeded for Service Unit esbadmin00089-su-2.</loc-message><loc-param>start</loc-param>" +
                "<loc-param>esbadmin00089-su-2</loc-param></msg-loc-info></task-status-msg></task-result-details>" +
                "</component-task-result-details></component-task-result></jbi-task-result></jbi-task>";
        try
        {
            String message = MessageHelper.getMsgString(new Exception(xml));
            System.out.println(message);
            assertEquals("The strings are not equal",
                   NEWLINE + "Scaffolded String isTASK_ID start success" + NEWLINE 
                    + TAB + "WARNING: " + NEWLINE              
                    + TAB + "This is a test message used for testing. " + NEWLINE
                    + TAB + TAB + TAB + "Scaffolded String isCOMPONENT_NAME: manage-binding-1" + NEWLINE
                    + TAB + TAB + TAB + TAB +"Scaffolded String isTASK_ID start failed" + NEWLINE				                  
                    + TAB + TAB + TAB + TAB + TAB + "ERROR: " + NEWLINE
                    + TAB + TAB + TAB + TAB + TAB + "Service unit esbadmin00089-su-1 start failed" + NEWLINE                 
                    + TAB + TAB + TAB + "Scaffolded String isCOMPONENT_NAME: manage-binding-2" + NEWLINE
                    + TAB + TAB + TAB + TAB +"Scaffolded String isTASK_ID start success" + NEWLINE				                  
                    + TAB + TAB + TAB + TAB + TAB + "INFO: " + NEWLINE
                    + TAB + TAB + TAB + TAB + TAB + "Lifecycle operation start succeeded for Service Unit esbadmin00089-su-2." + NEWLINE,             
                    message);
            
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }    
    }
        
  
 }
