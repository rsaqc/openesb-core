/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestRepository.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

import com.sun.jbi.management.internal.support.DirectoryUtil;
import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
import com.sun.jbi.management.system.ManagementContext;

import java.io.File;
import java.util.Calendar;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

public class TestRepository extends junit.framework.TestCase
{
    private static final String JBI_ROOT_PATH =
            "/target";
    
    private static final String COMPONENT_ARCHIVE_PATH =
            "/testdata/component.zip";
    
    private static final String BINDING_COMPONENT_ARCHIVE_PATH =
            "/testdata/jmsbinding.jar";
    
    private static final String COMPONENT_ARCHIVE_NAME =
            "SunSequencingEngine";
    
    private static final String BINDING_COMPONENT_ARCHIVE_NAME =
            "SunJMSBinding";
    
    private static final String SERVICE_ASSEMBLY_ARCHIVE =  
            "service-assembly.zip";
    
    private static final String SA_WITH_BAD_SU_ARCHIVE_PATH =  
            "/testdata/sa-bad-su.zip";
    
    private static final String SERVICE_ASSEMBLY_ARCHIVE_PATH =
            "/testdata/" + SERVICE_ASSEMBLY_ARCHIVE;
    
    private static final String SERVICE_ASSEMBLY_NAME = 
            "CompositeApplication";
    
    private static final String BOGUS_SERVICE_ASSEMBLY_NAME = 
            "CompositeApp";

    private static final String SERVICE_UNIT_1_NAME = 
            "ESB_ADMIN_SERVICE_UNIT_1";
    
    private static final String SERVICE_UNIT_2_NAME = 
            "ESB_ADMIN_SERVICE_UNIT_2";
    
    private static final String SERVICE_UNIT_1_PATH =
            "su1.jar";
    
    private static final String SERVICE_UNIT_2_PATH =
            "su2.jar";
    
    private Repository          mRepository;
    private ManagementContext   mCtx;
    private String              mJbiRoot;
            
    public TestRepository (String aTestName)
        throws Exception
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        String blddir = "/target/test-classes";          // open-esb build

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            blddir = "/target/regress";         // mainline/whitney build
        }

        mJbiRoot = srcroot + manage + JBI_ROOT_PATH;
        ScaffoldEnvironmentContext envCtx = new ScaffoldEnvironmentContext();
        envCtx.setJbiInstanceRoot(mJbiRoot);
        envCtx.setAppServerInstanceRoot(mJbiRoot);
        envCtx.setProperty("com.sun.jbi.home", System.getProperty("junit.as8base") + "/jbi");
        envCtx.setJbiInstallRoot(System.getProperty("junit.as8base") + "/jbi");        
        com.sun.jbi.util.EnvironmentAccess.setContext(envCtx);
        mJbiRoot = srcroot + manage + blddir;
        mCtx = new ManagementContext(envCtx);
        
        mRepository = new Repository(mCtx);
    }
    
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mRepository.purge();
    }

    public void tearDown()
        throws Exception
    {
        mRepository.purge();
    }

    public void testAddComponentArchive()
        throws Exception
    {
        String  path = mJbiRoot + COMPONENT_ARCHIVE_PATH;
        Archive archive;
        
        archive = mRepository.addArchive(ArchiveType.COMPONENT, path);
        
        // verify that the archive path now points to a repository location
        assertTrue(!archive.getPath().equals(path));
        
        // verify upload timestamp is set correctly
        Calendar up1 = archive.getUploadTimestamp();
        Thread.sleep(200);
        Calendar up2 = archive.getUploadTimestamp();
        assertTrue(up2.getTimeInMillis() == up1.getTimeInMillis());
    }
    
    public void testAddComponentArchiveWrongType()
    {
        try
        {
            mRepository.addArchive(ArchiveType.SHARED_LIBRARY, 
                    mJbiRoot + COMPONENT_ARCHIVE_PATH);
        }
        catch (RepositoryException repEx)
        {
            // expected
            return;
        }
        
        fail("Added component to repository as a shared library.");
    }   
    
    public void testAddComponentArchiveDuplicate()
        throws Exception
    {
        // add it once
        mRepository.addArchive(ArchiveType.COMPONENT, mJbiRoot + COMPONENT_ARCHIVE_PATH);
        
        try
        {
            // try a second time
            mRepository.addArchive(ArchiveType.COMPONENT, mJbiRoot + COMPONENT_ARCHIVE_PATH);
        }
        catch (RepositoryException repEx)
        {
            // expected
            return;
        }
        
        fail("Added duplicate component to repository.");
    }
    
    public void testRemoveComponentArchive()
        throws Exception
    {
        // add the archive
        mRepository.addArchive(ArchiveType.COMPONENT, mJbiRoot + COMPONENT_ARCHIVE_PATH);
        
        // remove the archive
        mRepository.removeArchive(ArchiveType.COMPONENT, COMPONENT_ARCHIVE_NAME);
        
        // attempt to find the archive
        Archive archive = mRepository.getArchive(
                ArchiveType.COMPONENT, COMPONENT_ARCHIVE_NAME);
        assertTrue(archive == null);
    }
    
    public void testRemoveComponentArchiveBad()
        throws Exception
    {
        mRepository.removeArchive(ArchiveType.COMPONENT, "foo");
    }
    
    public void testFindComponentArchive()
        throws Exception
    {
        // add the archive
        mRepository.addArchive(ArchiveType.COMPONENT, mJbiRoot + COMPONENT_ARCHIVE_PATH);
        
        // try and find it
        String path = mRepository.findArchive(ArchiveType.COMPONENT, COMPONENT_ARCHIVE_NAME);
        
        assertTrue(path != null);
    }
        
    public void testAddServiceAssemblyArchive()
        throws Exception
    {
        String  path;
        Archive archive;
        File    sa_file;
        File    su1_dir;
        File    su1_file;
        File    su2_dir;
        File    su2_file;
        
        path    = mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH;
        archive = mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, path);
        sa_file = new File(archive.getPath());
        
        // verify that the archive path now points to a repository location
        assertTrue(!archive.getPath().equals(path));
           
        su1_dir = new File(sa_file.getParent(), SERVICE_UNIT_1_NAME);
        su2_dir = new File(sa_file.getParent(), SERVICE_UNIT_2_NAME);
        
        // verify that the service unit directories were created
        assertTrue(su1_dir.exists());
        assertTrue(su2_dir.exists());
        
        su1_file = new File(su1_dir, SERVICE_UNIT_1_PATH);
        su2_file = new File(su2_dir, SERVICE_UNIT_2_PATH);
        
        // verify that the service units were extracted into directories
        assertTrue(su1_file.exists());
        assertTrue(su2_file.exists());
    }
    
    public void testAddServiceAssemblyArchiveWrongType()
    {
        try
        {
            mRepository.addArchive(ArchiveType.COMPONENT, 
                    mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);
        }
        catch (RepositoryException repEx)
        {
            // expected
            return;
        }
        
        fail("Added service assembly to repository as a component.");
    }   
    
    public void testAddServiceAssemblyArchiveDuplicate()
        throws Exception
    {
        // add it once
        mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, 
                               mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);
        
        try
        {
            // try a second time
            mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, 
                                   mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);
        }
        catch (RepositoryException repEx)
        {
            // expected
            return;
        }
        
        fail("Added duplicate service assembly to repository.");
    }
    
    public void testRemoveServiceAssemblyArchive()
        throws Exception
    {
        // add the archive
        mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY,
                               mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);
        
        // remove the archive
        mRepository.removeArchive(ArchiveType.SERVICE_ASSEMBLY, SERVICE_ASSEMBLY_NAME);
        
        // attempt to find the archive
        Archive archive = mRepository.getArchive(
                ArchiveType.SERVICE_ASSEMBLY, SERVICE_ASSEMBLY_NAME);
        assertTrue(archive == null);
    }
    
    /**
     * Test removing a empty service assembly folder
     */
    public void testRemoveEmptyServiceAssemblyDirectory()
        throws Exception
    {
        // Create the empty sa dir
        String dummySA = "emptySA";
        
        File emptySADir = new File(mCtx.getEnvironmentContext().getJbiInstanceRoot() 
            + "/service-assemblies/" + dummySA);
        
        emptySADir.mkdirs();
        
        assertTrue(emptySADir.exists());
        
        // remove the archive
        mRepository.removeArchive(ArchiveType.SERVICE_ASSEMBLY, dummySA);
        
        // attempt to find the archive
        Archive archive = mRepository.getArchive(
                ArchiveType.SERVICE_ASSEMBLY, SERVICE_ASSEMBLY_NAME);
        assertTrue(archive == null);
        System.out.println(emptySADir.getAbsolutePath());
        assertFalse(emptySADir.exists());
    }
    
    public void testRemoveServiceAssemblyArchiveBad()
        throws Exception
    {
        // remove an archive that doesn't exist
        mRepository.removeArchive(ArchiveType.SERVICE_ASSEMBLY, "foo");
    }
    
    public void testFindServiceAssemblyArchive()
        throws Exception
    {
        // add the archive
        mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, 
                               mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);
        
        // try and find it
        String path = mRepository.findArchive(
                ArchiveType.SERVICE_ASSEMBLY, SERVICE_ASSEMBLY_NAME);
        
        assertTrue(path.endsWith(SERVICE_ASSEMBLY_ARCHIVE));
        assertTrue(path != null);
    }    
    
    public void testFindServiceUnitArchive()
        throws Exception
    {
        String su1_path;
        String su2_path;
        
        // add the archive
        mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, 
                               mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);
        
        // try and find service units
        su1_path = mRepository.findArchive(
                ArchiveType.SERVICE_UNIT, 
                SERVICE_ASSEMBLY_NAME + "/" + SERVICE_UNIT_1_NAME);
        
        su2_path = mRepository.findArchive(
                ArchiveType.SERVICE_UNIT,  
                SERVICE_ASSEMBLY_NAME + "/" + SERVICE_UNIT_2_NAME);
        
        assertTrue(su1_path != null);
        assertTrue(su2_path != null);
    }
   
    public void testGetServiceUnitArchive()
        throws Exception
    {
        Archive su;
        
        // add the archive
        mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, 
                               mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);        
        
        // go and get it
        su = mRepository.getArchive(
                ArchiveType.SERVICE_UNIT,                  
                SERVICE_ASSEMBLY_NAME + "/" + SERVICE_UNIT_1_NAME);

        assertTrue(su.getType().equals(ArchiveType.SERVICE_UNIT));
        assertTrue(su.getSize().intValue() > 0);
        assertTrue(su.getJbiName().equals(SERVICE_UNIT_1_NAME));
    }
       
    public void testServiceUnitMissingJbiXml()
        throws Exception
    {
        Archive su;
        
        // try and add an SA containing an SU that's missing jbi.xml
        try
        {
             mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, 
                               mJbiRoot + SA_WITH_BAD_SU_ARCHIVE_PATH);
            
            fail("Archive constructed from invalid source -- missing jbi.xml");
        }
        catch (RepositoryException rEx) {}        
    }
    
    /** Tests the ability to add an archive to the repository when a 
     *  previous entry in the repository already exists but is marked for
     *  deletion.
     */
    public void testAddComponentArchiveMarked()
        throws Exception
    {
        String  path = mJbiRoot + COMPONENT_ARCHIVE_PATH;
        Archive archive1;
        Archive archive2;
        
        // add the archive and mark it to simulate a failed removal
        archive1 = mRepository.addArchive(ArchiveType.COMPONENT, path);
        assertTrue(mRepository.archiveExists(ArchiveType.COMPONENT, archive1.getJbiName()));
        
        // mark deleted
        File archive1Path = new File(archive1.getPath());
        mRepository.removeArchive(ArchiveType.COMPONENT, archive1.getJbiName());
        if ( !(archive1Path.exists()))
        {
            archive1Path.getParentFile().mkdirs();
        }
        DirectoryUtil.markDir(new File(archive1.getPath()).getParent());
        
        // check to make sure it counts as deleted
        assertFalse(mRepository.archiveExists(ArchiveType.COMPONENT, archive1.getJbiName()));
        
        // try and add the same thing back in (this should succeed)        
        archive2 = mRepository.addArchive(ArchiveType.COMPONENT, path);
        
        // verify that archive1 and archive 2 do not have the same path
        assertFalse(archive1.getPath().equals(archive2.getPath()));
        
        // remove the latest one
        mRepository.removeArchive(ArchiveType.COMPONENT, archive2.getJbiName());
        assertFalse(mRepository.archiveExists(ArchiveType.COMPONENT, archive2.getJbiName()));
    }
    
    
    /** Tests whether the service unit archive path is constructed correctly 
     *  when a service assembly is added when a previous service assembly
     *  with the same name is marked for deltetion.
     */
    public void testGettingServiceUnitArchiveWhenMarked()
        throws Exception
    {
        String  path = mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH;
        Archive archive1;
        Archive archive2;
        Archive suArchive;
        
        // add the archive and mark it to simulate a failed removal
        archive1 = mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, path);
        assertTrue(mRepository.archiveExists(ArchiveType.SERVICE_ASSEMBLY, archive1.getJbiName()));
        
        File archive1Path = new File(archive1.getPath());
        mRepository.removeArchive(ArchiveType.SERVICE_ASSEMBLY, archive1.getJbiName());
        if ( !(archive1Path.exists()))
        {
            archive1Path.getParentFile().mkdirs();
        }
        DirectoryUtil.markDir(new File(archive1.getPath()).getParent());
        
        // Add the same service assembly again
        archive2 = mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, path);
        
        // Get the path to the service unit
        suArchive = mRepository.getArchive(ArchiveType.SERVICE_UNIT, 
            SERVICE_ASSEMBLY_NAME + File.separator + SERVICE_UNIT_1_NAME);
        File suDir = new File(suArchive.getPath());
        assertTrue(suDir.exists());
        
        // remove the latest one
        mRepository.removeArchive(ArchiveType.SERVICE_ASSEMBLY, archive2.getJbiName());
        assertFalse(mRepository.archiveExists(ArchiveType.SERVICE_ASSEMBLY, archive2.getJbiName()));
        
        // remove the marked service assembly archive
        DirectoryUtil.removeMarkedDirs(new File(archive1.getPath()).getParent());
    }
    
    /** Tests the ability to remove an archive when an open file reference is 
     *  hanging around.  Since this only is a problem on Windows, we need to 
     *  make sure that this test passes even if the initial removal succeeds.
     */
    public void testRemoveServiceAssemblyArchiveMarked()
        throws Exception
    {
        String  path = mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH;
        Archive archive;
        
        // add the archive
        archive = mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, path);
        assertTrue(mRepository.archiveExists(ArchiveType.SERVICE_ASSEMBLY, archive.getJbiName()));
        
        // create a JarFile ref for one of the SUs
        Archive suArchive = mRepository.getArchive(
                ArchiveType.SERVICE_UNIT, archive.getJbiName() + "/" + SERVICE_UNIT_1_NAME);
        JarFile jarFile = new JarFile(new File(suArchive.getPath()));
        
        // Remove it and make sure it's gone. Deletion of the folder should fail
        // on windows, but the directory should be marked and the remove operation
        // should succeed.
        mRepository.removeArchive(ArchiveType.SERVICE_ASSEMBLY, archive.getJbiName());
        assertFalse(mRepository.archiveExists(ArchiveType.SERVICE_ASSEMBLY, archive.getJbiName()));
                
        // try and add the same thing back in (this should succeed)       
        archive = mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY, path);
        
        // remove the latest one
        mRepository.removeArchive(ArchiveType.SERVICE_ASSEMBLY, archive.getJbiName());
        assertFalse(mRepository.archiveExists(ArchiveType.COMPONENT, archive.getJbiName()));
        
        // clean up
        try
        {
            jarFile.close();
        } 
        catch (Exception ex) { /* irrelevant */ }
        
        // simulate a repository startup to see if the DELETED folder is cleaned up.
        new Repository(mCtx);
    }
    
    /**
     * Test the regular expression used to match the component/shared library/service unit
     * folders.
     */
    public void testDirSuffixPattern()
        throws Exception
    {
        String patternSuffix = Repository.DIR_SUFFIX_PATTERN;
        
        assertFalse(Pattern.matches(  "SunFileBinding" + patternSuffix, "SunFileBinding" ));
        assertFalse(Pattern.matches( "SunFileBinding" + patternSuffix , "SunFileBinding."));
        assertFalse(Pattern.matches( "SunFileBinding" + patternSuffix, "SunFileBinding_.99" ));
        
        assertTrue(Pattern.matches( "SunFileBinding" + patternSuffix , "SunFileBinding.1" ));
        assertTrue(Pattern.matches(  "SunFileBinding" + patternSuffix, "SunFileBinding.0" ));
        assertTrue(Pattern.matches( "SunFileBinding" + patternSuffix, "SunFileBinding.190" ));
        assertTrue(Pattern.matches( "SunFileBinding" + patternSuffix, "SunFileBinding.9999999"));
    }
    
    /**
     * Test getting a Service Assembly archive with a name which partially matches that
     * of the added service assembly. The getArchive() should fail.
     */
    public void testGetServiceAssemblyArchive()
        throws Exception
    {
        // add the archive
        mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY,
                               mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);
        
        
        // attempt to find the archive - should not find a match
        Archive archive = mRepository.getArchive(
                ArchiveType.SERVICE_ASSEMBLY, BOGUS_SERVICE_ASSEMBLY_NAME);
        assertTrue(archive == null);
        
        // remove the archive
        mRepository.removeArchive(ArchiveType.SERVICE_ASSEMBLY, SERVICE_ASSEMBLY_NAME);
    }
    
    /**
     * Test getting the names of service assemblies in the repository
     */
    public void testGetServiceAssemblyNames()
        throws Exception
    {
        // add the archive
        mRepository.addArchive(ArchiveType.SERVICE_ASSEMBLY,
                               mJbiRoot + SERVICE_ASSEMBLY_ARCHIVE_PATH);
        
        // test getting the service assembly names
        java.util.List<String> saNames = mRepository.getArchiveEntityNames(ArchiveType.SERVICE_ASSEMBLY);
        assertEquals(1, saNames.size());
        assertEquals(SERVICE_ASSEMBLY_NAME, saNames.get(0));
    }
    
    /**
     * Test getting the names of components in the repository
     */
    public void testGetComponentNames()
        throws Exception
    {
        // add the archives
        mRepository.addArchive(ArchiveType.COMPONENT,
                               mJbiRoot + COMPONENT_ARCHIVE_PATH);
        
        mRepository.addArchive(ArchiveType.COMPONENT,
                               mJbiRoot + BINDING_COMPONENT_ARCHIVE_PATH);
        
        java.util.List<String> components = mRepository.getArchiveEntityNames(ArchiveType.COMPONENT);
        assertEquals(2, components.size());
        assertTrue(components.contains(COMPONENT_ARCHIVE_NAME));
        assertTrue(components.contains(BINDING_COMPONENT_ARCHIVE_NAME));
    }
    
    /**
     * Test getting the names of components in the repository, with a component
     * marked for deletion, this component should be omitted from the list.
     */
    public void testGetComponentNamesWithMarkedDir()
        throws Exception
    {
        // add the archives
        mRepository.addArchive(ArchiveType.COMPONENT,
                               mJbiRoot + COMPONENT_ARCHIVE_PATH);
        
        mRepository.addArchive(ArchiveType.COMPONENT,
                               mJbiRoot + BINDING_COMPONENT_ARCHIVE_PATH);
        
        String archiveDir = mRepository.findArchiveDirectory(ArchiveType.COMPONENT,
                               BINDING_COMPONENT_ARCHIVE_NAME);
        
        com.sun.jbi.management.internal.support.DirectoryUtil.markDir(archiveDir);
        
        java.util.List<String> components = mRepository.getArchiveEntityNames(ArchiveType.COMPONENT);
        assertEquals(1, components.size());
        assertTrue(components.contains(COMPONENT_ARCHIVE_NAME));
        assertFalse(components.contains(BINDING_COMPONENT_ARCHIVE_NAME));
    }
    
    
    /**
     * Test getting the names of components in the repository, with a component
     * folder having a numeric suffix appended, this component should be added to the list.
     */
    public void testGetComponentNamesWithSuffixedDirs()
        throws Exception
    {
        // add the archive
        mRepository.addArchive(ArchiveType.COMPONENT,
                               mJbiRoot + BINDING_COMPONENT_ARCHIVE_PATH);
        
        // mark it for deletion
        String archiveDir = mRepository.findArchiveDirectory(ArchiveType.COMPONENT,
                               BINDING_COMPONENT_ARCHIVE_NAME);
        
        File archivePath = new File(archiveDir);
        mRepository.removeArchive(ArchiveType.COMPONENT, BINDING_COMPONENT_ARCHIVE_NAME);
        if ( !(archivePath.exists()))
        {
            archivePath.mkdirs();
        }
        com.sun.jbi.management.internal.support.DirectoryUtil.markDir(archiveDir);
        
        // add the archive again
        mRepository.addArchive(ArchiveType.COMPONENT,
                               mJbiRoot + BINDING_COMPONENT_ARCHIVE_PATH);
        
        java.util.List<String> components = mRepository.getArchiveEntityNames(ArchiveType.COMPONENT);
        assertEquals(1, components.size());
        assertTrue(components.contains(BINDING_COMPONENT_ARCHIVE_NAME));
    }
}
