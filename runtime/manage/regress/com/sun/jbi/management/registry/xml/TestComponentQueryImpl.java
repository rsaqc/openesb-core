/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentQueryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryBuilder;
import com.sun.jbi.management.registry.RegistryException;

import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.repository.ArchiveType;
 
import com.sun.jbi.management.system.Util;

import java.io.File;
import java.util.List;

public class TestComponentQueryImpl 
    extends junit.framework.TestCase
{
    /**
     * The sample Configuration Directory.
     */
    private String mConfigDir = null;
    private File mRegFile;
    String mRegFilePath;
    String mRegGoodFilePath;
    String mEngineZipPath;
    String mBindingZipPath;
    String mSharedLibraryZipPath;
    String mServiceAssemblyZipPath;
    
    static final String ENGINE_NAME = "SunSequencingEngine";
    static final String BINDING_NAME = "SunJMSBinding";
    static final String SHARED_LIBRARY_NAME = "SunWSDLSharedLibrary";
    static final String SERVICE_ASSEMBLY_NAME = "CompositeApplication";

     
    public TestComponentQueryImpl (String aTestName)
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/target/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/target/regress/testdata/";
        }
        
        mRegFilePath              = mConfigDir + File.separator + "jbi-registry.xml";
        mRegGoodFilePath          = mConfigDir + File.separator + "jbi-registry-good.xml";
        mEngineZipPath            = mConfigDir + "component.zip";
        mServiceAssemblyZipPath   = mConfigDir + "service-assembly.zip";
        mSharedLibraryZipPath     = mConfigDir + "wsdlsl.jar";
        mBindingZipPath           = mConfigDir + "jmsbinding.jar";
        
        mRegFile = new File(mRegFilePath);
        
    }

    public void setUp()
        throws Exception
    {
        super.setUp();
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        Util.fileCopy(mRegGoodFilePath, mRegFilePath);
    }

    public void tearDown()
        throws Exception
    {
        // -- restore registry.xml
       RegistryBuilder.destroyRegistry(); 
       Util.fileCopy(mRegGoodFilePath, mRegFilePath );
    }
    
    public void  testGetEngineComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        List<String> comps = query.getComponentIds(ComponentType.ENGINE);
        System.out.println(comps.toString());
        assertTrue(comps.contains(ENGINE_NAME));
        repository.purge();
    }
    
    public void  testGetBindingComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        List<String> comps = query.getComponentIds(ComponentType.BINDING);
        System.out.println(comps.toString());
        assertTrue(comps.contains(BINDING_NAME));
        repository.purge();
    }
    
    /**
     * Test getting the binding components for the domain target.
     */
    public void  testGetBindingComponentIds2()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("domain");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        List<String> comps = query.getComponentIds(ComponentType.BINDING);
        System.out.println(" Domain Binding Components " + comps.toString());
        assertTrue(comps.contains(BINDING_NAME));
        repository.purge();
    }
    
    
    public void  testGetBindingsAndEngineComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
           
        List<String> comps = query.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        System.out.println(comps.toString());
        assertTrue(comps.contains(BINDING_NAME));
        assertTrue(comps.contains(ENGINE_NAME));
        repository.purge();
    }
    
    public void  testGetSharedLibraryComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
           
        List<String> sls = query.getComponentIds(ComponentType.SHARED_LIBRARY);
        System.out.println(sls.toString());
        assertTrue(sls.contains(SHARED_LIBRARY_NAME));
        assertEquals(1, sls.size());
        repository.purge();
    }
    
    
    public void  testGetAllComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
           
        List<String> list = query.getComponentIds(ComponentType.ALL);
        System.out.println(list.toString());
        assertTrue(list.contains(SHARED_LIBRARY_NAME));
        assertTrue(list.contains(ENGINE_NAME));
        assertTrue(list.contains(BINDING_NAME));
        repository.purge();
    }
    
    public void  testGetAllStartedComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
           
        List<String> list = query.getComponentIds(ComponentType.ALL, ComponentState.STARTED);
        System.out.println(list.toString());
        assertTrue(list.contains(ENGINE_NAME));
        assertTrue(list.contains(BINDING_NAME));
        repository.purge();
    }
    
    public void testGetComponentInfo()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        ComponentInfo comp = query.getComponentInfo(BINDING_NAME);
        
	if ( comp == null )
	   System.out.println("This is null");
	else
	   System.out.println("This is not null");

        assertNotNull(comp);
        System.out.println(comp.getName());
        System.out.println(comp.getDescription());
        System.out.println(comp.getInstallRoot());
        System.out.println(comp.getClassPathElements().toString());
        comp.getComponentType().toString().equals("BINDING");
        assertTrue(comp.getSharedLibraryNames().toString().equals("[SunWSDLSharedLibrary]"));
        repository.purge();
    }
    
    /**
     * Test getting from the domain
     */
    public void testGetComponentInfo2()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("domain");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        ComponentInfo comp = query.getComponentInfo(BINDING_NAME);
        
        System.out.println("Domain Component");
        System.out.println(comp.getName());
        System.out.println(comp.getDescription());
        System.out.println(comp.getInstallRoot());
        System.out.println(comp.getClassPathElements().toString());
        comp.getComponentType().toString().equals("BINDING");
        assertTrue(comp.getSharedLibraryNames().toString().equals("[SunWSDLSharedLibrary]"));
        assertTrue("Started".equals(comp.getStatus().toString()));
        repository.purge();
    }
    
    public void testGetDependentComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
        
        List<String> depComps = query.getDependentComponentIds(SHARED_LIBRARY_NAME);
        assertTrue(BINDING_NAME.equals(depComps.get(0)));
        repository.purge();
    }
    
    public void testGetDependentComponentIds2()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("domain");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
        
        List<String> depComps = query.getDependentComponentIds(SHARED_LIBRARY_NAME);
        assertTrue(BINDING_NAME.equals(depComps.get(0)));
        repository.purge();
    }
    
    public void testGetSharedLibraryInstallationDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRepository();
        
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
        ComponentInfo comp = query.getSharedLibraryInfo(SHARED_LIBRARY_NAME);
        
        assertNotNull(comp);
        assertNotNull(comp.getInstallationDescriptor());
        assertTrue(comp.getInstallationDescriptor().indexOf(SHARED_LIBRARY_NAME) > 0);
        repository.purge();
    }
    
    public void testGetComponentInstallationDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        ComponentInfo comp = query.getComponentInfo(BINDING_NAME);
        
        assertNotNull(comp.getInstallationDescriptor());
        assertTrue(comp.getInstallationDescriptor().indexOf(BINDING_NAME) > 0);
        repository.purge();
    }
    
    /**
     * This tests the fix for CR 6548857. For non-domain targets if the server-ref or
     * cluster-ref is missing in a fresh registry, the component query operations to
     * get components/shared libraries for that target should return an empty list
     * instead of throwing an "Unknown target : XXX " exception. 
     *
     * This tests the private getTarget() operation in ComponentQueryImpl. If the target
     * is missing a null value is expected and not an exception.
     */
    public void  testGetTarget()
        throws Exception
    {
        // Start with a blank registry
        RegistryBuilder.destroyRegistry();
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        Registry reg = Util.createRegistry(true);
        try
        {
            ComponentQueryImpl query = (ComponentQueryImpl) reg.getComponentQuery("someTarget");
            Class clazz = query.getClass();
            
            java.lang.reflect.Method 
                getTargetMtd = clazz.getDeclaredMethod("getTarget", 
                    new Class[]{Class.forName("java.lang.String")} );
            getTargetMtd.setAccessible(true);
            Object target = getTargetMtd.invoke(query, new Object[]{"someTarget"});
            assertNull(target);
        }
        catch ( Exception ex )
        {
            fail();
        }
    }
    
    /**
     * Test getting component configuration properties.
     */
    public void testGetComponentConfiguration()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        java.util.Properties props = compInfo.getConfiguration();
        assertNotNull(props);
        assertEquals(props.get("HostName"), "tango");
        assertEquals(props.get("Port"), "5656");
        repository.purge();
    }
    
    /**
     * Test getting component configuration properties.
     */
    public void testGetComponentConfigurationForClusterTarget()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("clusterA");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        java.util.Properties props = compInfo.getConfiguration();
        assertNotNull(props);
        assertFalse(props.isEmpty());
        assertEquals(props.get("HostName"), "Eight");
        assertEquals(props.get("Port"), "8888");
        repository.purge();
    }

    /**
     * Test getting non-existant component configuration properties.
     */
    public void testGetEmptyComponentConfiguration()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(BINDING_NAME);
        assertNotNull(compInfo);
        java.util.Properties props = compInfo.getConfiguration();
        assertNotNull(props);
        assertTrue(props.isEmpty());
        repository.purge();
    }
    
    /**
     * Test getting component application variables
     */
    public void testGetComponentApplicationVariables()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        
        com.sun.jbi.management.ComponentInfo.Variable[] vars = compInfo.getVariables();
        
        assertNotNull(vars);
        assertEquals(2, vars.length);
        
        repository.purge();
    }
    
    
    /**
     * Test getting component application variables
     */
    public void testGetEmptyComponentApplicationVariables()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(BINDING_NAME);
        assertNotNull(compInfo);
        
        com.sun.jbi.management.ComponentInfo.Variable[] vars = compInfo.getVariables();
        
        assertNotNull(vars);
        assertEquals(0, vars.length);
        
        repository.purge();
    }
    
    /**
     * Test getting component application configuration names.
     */
    public void testGetComponentApplicationConfigurationNames()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        
        String[] configNames = compInfo.getApplicationConfigurationNames();
        
        assertNotNull(configNames);
        assertEquals(2, configNames.length);
        
        repository.purge();
    }
        
    /**
     * Test getting component application configuration.
     */
    public void testGetComponentApplicationConfiguration()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        
        // -- Test whether the property values are read correctly
        java.util.Properties config1 = compInfo.getApplicationConfiguration("SEQ_CONFIG1");
        assertTrue(config1.getProperty("configurationName").equals("SEQ_CONFIG1"));
        assertTrue(config1.getProperty("initialContextFactory").equals("com.sonicsw.jndi.mfcontext.MFContextFactory"));
        assertTrue(config1.getProperty("connectionURL").equals("jndi://cfg1"));
        
        
        java.util.Properties config2 = compInfo.getApplicationConfiguration("SEQ_CONFIG2");
        assertTrue(config2.getProperty("configurationName").equals("SEQ_CONFIG2"));
        assertTrue(config2.getProperty("initialContextFactory").equals("com.sonicsw.jndi.mfcontext.MFContextFactory"));
        assertTrue(config2.getProperty("connectionURL").equals("jndi://cfg2")); 
        
        repository.purge();
    }
}
