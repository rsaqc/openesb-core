/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestGenericQueryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.registry.GenericQuery;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryBuilder;
import com.sun.jbi.management.registry.RegistryException;

import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.repository.ArchiveType;
 
import com.sun.jbi.management.system.Util;

import java.io.File;
import java.math.BigInteger;
import java.util.List;

public class TestGenericQueryImpl 
    extends junit.framework.TestCase
{
    /**
     * The sample Configuration Directory.
     */
    private String mConfigDir = null;
    private File mRegFile;
    String mRegFilePath;
    String mRegGoodFilePath;
    String mComponentZipPath;
    String mSharedLibraryZipPath;
    String mServiceAssemblyZipPath;
    
    static final String COMPONENT_NAME = "SunSequencingEngine";
    static final String SHARED_LIBRARY_NAME = "SunWSDLSharedLibrary";
    static final String SERVICE_ASSEMBLY_NAME = "CompositeApplication";
    static final String SERVICE_UNIT_NAME = "ESB_ADMIN_SERVICE_UNIT_1";

     
    public TestGenericQueryImpl (String aTestName)
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/target/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/target/regress/testdata/";
        }
        
        mRegFilePath        = mConfigDir + File.separator + "jbi-registry.xml";
        mRegGoodFilePath    = mConfigDir + File.separator + "jbi-registry-good.xml";
        mComponentZipPath   = mConfigDir + "component.zip";
        mServiceAssemblyZipPath   = mConfigDir + "service-assembly.zip";
        mSharedLibraryZipPath     = mConfigDir + "wsdlsl.jar";
      
        
        mRegFile = new File(mRegFilePath);
        
    }

    public void setUp()
        throws Exception
    {
        super.setUp();
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        Util.fileCopy(mRegGoodFilePath, mRegFilePath);
    }

    public void tearDown()
        throws Exception
    {
        // -- restore registry.xml
       RegistryBuilder.destroyRegistry(); 
       Util.fileCopy(mRegGoodFilePath, mRegFilePath );
    }

    /**
     * @throws Exception if an unexpected error occurs
     */
    public void testGetClustersDeployingServiceAssembly()
           throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        List<String> clusters = query.getClustersDeployingServiceAssembly(SERVICE_ASSEMBLY_NAME);
        
        assertTrue(clusters.contains("clusterA"));

    }
    
    /**
     * @throws Exception if an unexpected error occurs
     */
    public void testGetClustersInstallingComponent()
           throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        List<String> clusters = query.getClustersInstallingComponent(COMPONENT_NAME);
        assertTrue(clusters.contains("clusterA"));
    }
    
    /**
     * @throws Exception if an unexpected error occurs
     */
    public void testGetClustersInstallingSharedLibrary()
           throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        List<String> clusters = query.getClustersInstallingSharedLibrary(SHARED_LIBRARY_NAME);
        assertTrue(clusters.contains("clusterA"));
        
    }
    
    public void  testGetComponentInstallationDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mComponentZipPath);
        
         String jbiXml = query.getComponentInstallationDescriptor(COMPONENT_NAME);
        assertTrue(jbiXml.startsWith("<?xml"));
        repository.purge();
    }
    
    public void  testGetSharedLibraryInstallationDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
        
        String jbiXml = query.getSharedLibraryInstallationDescriptor(SHARED_LIBRARY_NAME);
        assertTrue(jbiXml.startsWith("<?xml"));
        repository.purge();
    }
    
    public void  testGetServiceAssemblyDeploymentDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.SERVICE_ASSEMBLY, mServiceAssemblyZipPath);
        
        String jbiXml = query.getServiceAssemblyDeploymentDescriptor(SERVICE_ASSEMBLY_NAME);
        assertTrue(jbiXml.startsWith("<?xml"));
        repository.purge();
    }
    
    public void  testGetServiceUnitDeploymentDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.SERVICE_ASSEMBLY, mServiceAssemblyZipPath);
        
        String jbiXml = query.getServiceUnitDeploymentDescriptor(SERVICE_ASSEMBLY_NAME, SERVICE_UNIT_NAME);
        assertTrue(jbiXml.startsWith("<?xml"));
        repository.purge();
    }
    
    public void testIsServiceAssemblyDeployed()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertFalse(query.isServiceAssemblyDeployed("SA"));
        assertTrue(query.isServiceAssemblyDeployed(SERVICE_ASSEMBLY_NAME));
    }
    
  
    /**
     * This method is used to test the system-install attribute
     */    
    public void testIsSystemComponent() throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertFalse(query.isSystemComponent("SunSequencingEngine"));
        assertFalse(query.isSystemComponent("SunJMSBinding"));
    }
    
    /**
     * This method is used to test the system-install attribute for a 
     * shared library
     */    
    public void testIsSystemSharedLibrary() throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertFalse(query.isSystemSharedLibrary("SunWSDLSharedLibrary"));

    }
    
    /**
     * Test getting the component file name
     */
    public void testGetComponentFileName()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertEquals(query.getComponentFileName(COMPONENT_NAME), "sequenceengine.zip");
        assertNull(query.getComponentFileName("xyz"));
    }

    
    /**
     * Test getting the shared library file name
     */
    public void testGetSharedLibraryFileName()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertEquals(query.getSharedLibraryFileName(SHARED_LIBRARY_NAME), 
            "wsdlsl.jar");
        
        assertNull(query.getSharedLibraryFileName("xyz"));
    }
    
    /**
     * Test getting the service assembly file name
     */
    public void testGetServiceAssemblyFileName()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertEquals(query.getServiceAssemblyFileName(SERVICE_ASSEMBLY_NAME), 
            "service-assembly.zip");
        
        assertNull(query.getServiceAssemblyFileName("xyz"));
    }
    
    /**
     * Test isComponentRegistered(), the component should be removed from the
     * registry, since there is no repos entry and a false is returned
     */
    public void testIsComponentRegistered()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertTrue(query.isComponentRegistered(COMPONENT_NAME));
    }
    
    
    /**
     * Test isSharedLibraryRegistered(), the library should be removed from the
     * registry, since there is no repos entry and a false is returned
     */
    public void testIsSharedLibraryRegistered()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertTrue(query.isSharedLibraryRegistered(SHARED_LIBRARY_NAME));
    }
    
    /**
     * Test isServiceAssemblyRegistered(), the sa should be removed from the
     * registry, since there is no repos entry and a false is returned
     */
    public void testIsServiceAssemblyRegistered()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mComponentZipPath);
        assertTrue(query.isServiceAssemblyRegistered(SERVICE_ASSEMBLY_NAME));
        
        com.sun.jbi.ComponentQuery compQuery = reg.getComponentQuery("server");
        
        com.sun.jbi.ComponentInfo compInfo = compQuery.getComponentInfo(COMPONENT_NAME);
        assertNotNull(compInfo);
        
        List<com.sun.jbi.ServiceUnitInfo> suList = compInfo.getServiceUnitList();
        
        assertFalse(suList.isEmpty());
        repository.purge();
    }
    
    /**
     * Test getting a configuration attribute from the domain-configuration
     */
    public void testGetAttribute()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        String attrValue = query.getAttribute("domain", ConfigurationCategory.Installation, 
            "componentTimeout");
        
        assertTrue("5000".equals(attrValue));
    }
    
    /**
     * Test getting a configuration attribute for a target which is not overriden.
     */
    public void testGetAttributeForTarget()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        String attrValue = query.getAttribute("clusterB", ConfigurationCategory.Installation, 
            "componentTimeout");
        
        assertTrue("5000".equals(attrValue));
    }
    
    /**
     * Test getting a configuration attribute for a target which is overriden.
     */
    public void testGetAttributeForOverridenTarget()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        String attrValue = query.getAttribute("clusterA", ConfigurationCategory.Installation, 
            "componentTimeout");
        
        assertTrue("4000".equals(attrValue));
    }
    
    /**
     * Test getting a missing configuration attribute, should get back a null.
     */
    public void testGetNonExistantAttribute()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        String attrValue = query.getAttribute("clusterA", ConfigurationCategory.Installation, 
            "someTimeout");
        
        assertNull(attrValue);
    }
    
    /**
     * Test if domain config exists
     */
    public void testIsGlobalConfigurationDefined()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        assertTrue(query.isGlobalConfigurationDefined());
    }
    
    /**
     * This method is used to test the method getComponentUpgradeNumber.
     * upgrade-number is an optional attribute. For a component that does not have
     * it would return 0.
     */
    public void testGetComponentUpgradeNumber()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        assertEquals(query.getComponentUpgradeNumber("SunSequencingEngine"), BigInteger.ZERO);        
        assertEquals(query.getComponentUpgradeNumber("SunJMSBinding"), BigInteger.ONE);        
    }        
}
