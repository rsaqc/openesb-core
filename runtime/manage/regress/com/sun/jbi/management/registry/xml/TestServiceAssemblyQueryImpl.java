/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceAssemblyQueryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryBuilder;
import com.sun.jbi.management.registry.RegistryException;

import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.repository.ArchiveType;
 
import com.sun.jbi.management.system.Util;

import java.io.File;
import java.util.List;

public class TestServiceAssemblyQueryImpl 
    extends junit.framework.TestCase
{
    /**
     * The sample Configuration Directory.
     */
    private String mConfigDir = null;
    private File mRegFile;
    String mRegFilePath;
    String mRegGoodFilePath;
    String mEngineZipPath;
    String mBindingZipPath;
    String mSharedLibraryZipPath;
    String mServiceAssemblyZipPath;
    
    static final String ENGINE_NAME = "SunSequencingEngine";
    static final String BINDING_NAME = "SunJMSBinding";
    static final String SHARED_LIBRARY_NAME = "sun-wsdl-library";
    static final String SERVICE_ASSEMBLY_NAME = "CompositeApplication";

     
    public TestServiceAssemblyQueryImpl (String aTestName)
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/target/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/target/regress/testdata/";
        }
        
        mRegFilePath              = mConfigDir + File.separator + "jbi-registry.xml";
        mRegGoodFilePath          = mConfigDir + File.separator + "jbi-registry-good.xml";
        mEngineZipPath            = mConfigDir + "component.zip";
        mServiceAssemblyZipPath   = mConfigDir + "service-assembly.zip";
        mSharedLibraryZipPath     = mConfigDir + "wsdlsl.jar";
        mBindingZipPath           = mConfigDir + "jmsbinding.jar";
        
        mRegFile = new File(mRegFilePath);
        
    }

    public void setUp()
        throws Exception
    {
        super.setUp();
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        Util.fileCopy(mRegGoodFilePath, mRegFilePath);
    }

    public void tearDown()
        throws Exception
    {
        // -- restore registry.xml
       RegistryBuilder.destroyRegistry(); 
       Util.fileCopy(mRegGoodFilePath, mRegFilePath );
    }
    
    public void  testGetServiceAssemblies()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ServiceAssemblyQuery query = reg.getServiceAssemblyQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.SERVICE_ASSEMBLY, mServiceAssemblyZipPath);
        
        List<String> sas = query.getServiceAssemblies();
        assertTrue(sas.contains(SERVICE_ASSEMBLY_NAME));
        
        repository.purge();
    }
    
    public void  testGetServiceAssemblyInfo()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ServiceAssemblyQuery query = reg.getServiceAssemblyQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.SERVICE_ASSEMBLY, mServiceAssemblyZipPath);

        
        com.sun.jbi.ServiceAssemblyInfo sa = query.getServiceAssemblyInfo(SERVICE_ASSEMBLY_NAME);
        assertEquals(sa.getName(), SERVICE_ASSEMBLY_NAME);
        assertTrue(sa.getServiceUnitList().size() == 2);
        assertTrue(sa.getStatus() == com.sun.jbi. ServiceAssemblyState.STOPPED);
        
        repository.purge();
    }
    
    
}
