/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CalculatorServiceController.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.abc.jbi.calculator.engine;

import com.sun.jbi.management.support.JbiNameInfo;

/**
 * The CalculatorServiceController class uses the functionality of the model
 * Engine Component to do all its work.
 */
public class CalculatorServiceController extends com.sun.jbi.management.engine.ModelEngineComponent
{

    /** our immutable name: */
    private JbiNameInfo mJbiNameInfo;

    /**
     * Initializes CalculatorServiceController.
     * @param anEnv is the JBI Framework Context provided for this component.
     */
    public void init(javax.jbi.component.ComponentContext anEnv)
        throws javax.jbi.JBIException
    {
        mJbiNameInfo = new JbiNameInfo( anEnv.getComponentName(), false );

        //we are a model component - intialize the model:
        super.initModelEngineComponent(anEnv, mJbiNameInfo);

        super.bootstrap();
    }

    /**
     * This method is called by the framework when a deployment request comes
     * for the component.
     *
     * @param suId service unit id.
     * @param suPath service unit path.
     *
     * @return status message.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    public String deploy(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        String retMsg = "<?xml version=\"1.0\" encoding=\"UTF-8\" " +
            "standalone=\"yes\"?><jbi-task version=\"1.0\" " +
            "xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\" " +
            "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
            "xsi:schemaLocation=\"http://java.sun.com/xml/ns/jbi/management-message " +
            "./managementMessage.xsd\"><jbi-task-result><frmwk-task-result>" +
            "<frmwk-task-result-details><task-result-details><task-id>deploy" +
            "</task-id><task-result>SUCCESS</task-result><message-type>INFO" +
            "</message-type><task-status-msg><msg-loc-info><loc-token>JBIMA0495" +
            "</loc-token><loc-message>Deployment of empty service assembly succeeded." +
            "</loc-message></msg-loc-info></task-status-msg></task-result-details>" +
            "<locale>en_US</locale></frmwk-task-result-details></frmwk-task-result>" +
            "<component-task-result><component-name>CalculatorService" +
            "</component-name><component-task-result-details><task-result-details>" +
            "<task-id>deploy</task-id><task-result>SUCCESS</task-result>" +
            "</task-result-details></component-task-result-details>" +
            
            "</component-task-result></jbi-task-result></jbi-task>";
        return retMsg;
    }

}
