#
# BEGIN_HEADER - DO NOT EDIT
# 
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)fortepj.ksh - ver 1.1 - 01/04/2006
#
# Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
# 
# END_HEADER - DO NOT EDIT
#

#common defs for Forte VSPMS users:
#
# 04-Dec-96 (russt)
#   created
# 23-Oct-00 (russt)
#   revised for CVS
#

#g=rwx, o=r-x
umask 002

export BLDDATE

alias fortepjhelp=FORTEPJ_HELP_ONLY

function showhelpusage { echo HELP COMMAND is fortepjhelp; }

function getlogroot {
    if [ "$REMOTE_LOGBASE" != "" ]; then
        echo $REMOTE_LOGBASE/regress
    else
        echo $SRCROOT/regress
    fi
}

function getblddate {
    _tmp=`getlogroot`
    BLDDATE="`if [ -r $_tmp/.date ]; then cat $_tmp/.date; fi`"
}

alias golog='getblddate; pushd `getlogroot`/log/$BLDDATE'

function showblddate {
    getblddate
    if [ x$BLDDATE != x ]; then
        echo LAST BUILD $BLDDATE
    fi
}

function showsrcpath { echo "SRCROOT:  $SRCROOT"; echo "CVSROOT:  $CVSROOT"; }

function showlinkroot {
    if [ x$FORTE_LINKROOT != x ]; then
        echo FORTE_LINKROOT is $FORTE_LINKROOT;
    fi 
}

function showtoolroot {
    if [ x$TOOLROOT != x ]; then
        echo TOOLROOT is $TOOLROOT
    fi
}

function pathinfo { showsrcpath; showtoolroot; showblddate; showhelpusage; }

if [ x$SBPJ != x ]; then
    alias srcdir='subpj `getsrcdir \!*`'
    alias regdir='set tmp = `wheretst \!^`; subpj `dirname $tmp`'
    alias gosrc='subpj `echo $SBPJ | sed s./tst/./src/. | sed s./regress/./src/.`'
    alias gotst='subpj `echo $SBPJ | sed s./src/./tst/. | sed s./regress/./tst/.`'
    alias goreg='subpj `echo $SBPJ | sed s./src/./regress/. | sed s./tst/./regress/.`'

    alias goreglog='getblddate; subpj regress/$FORTE_PORT/$BLDDATE/\!*'

    ### Note that REV is a VSPMS variable, and is already exported.  RT 11/9/00
    if [ "$CVS_BRANCH_NAME" != "" ]; then
        REV="$CVS_BRANCH_NAME"
    elif [ "$PATHNAME" != "" ]; then
        REV="$PATHNAME"
    else
        REV="NULL"
    fi
fi

#CVS ALIASES:
alias checkOut="cvs edit"
alias checkIn="echo checkIn is not available"
alias uncheckOut="cvs unedit"
alias newFile="cvs add"
function newDir { mkdir -p $1; newfile $1; }
alias updatePath="cvs -q update -d -P"
alias integratePath="cvs commit"

alias includeService="cvs checkout"
alias deleteService="cvs release -d"
alias deleteFile="cvs remove -f"

#shortcuts.  make sure you update these when you update above.
#do not redirect to above aliases to make it easier for the
#user to get the definition.
alias pco="cvs edit"
alias pci="echo checkIn is not available"
alias uco="cvs unedit"
alias newfile="cvs add"
function newdir { mkdir -p $1; newfile $1; }
alias update="cvs -q update -d -P"
alias integrate="cvs commit"

alias cvsdiff="cvs diff"
alias cvslog="cvs log"

alias pull='$TOOLROOT/boot/updateDist'

######
#maven common aliases:
######
alias smaven='maven -b -DSRCROOT=$JV_SRCROOT -Dmaven.home.local=$JV_SRCROOT/maven'
alias smavencb='smaven -Dgoal=clean,jar:install multiproject:goal'
alias smavencbt='smaven -Dmaven.test.skip=false -Dgoal=clean,jar:install,test multiproject:goal'
alias smaventest='smaven -Dmaven.test.skip=false test'
alias smaventestall='smaven -Dmaven.test.skip=false -Dgoal=test multiproject:goal'

#######
#maven2 common aliases:
#######
alias smvn='mvn -Dmaven.test.skip=true -Dmaven.repo.local=$JV_SRCROOT/m2/repository'
alias smvntest='mvn -Dmaven.test.skip=false -Dmaven.repo.local=$JV_SRCROOT/m2/repository'
alias helppom='smvn help:effective-pom'
alias helpsettings='smvn help:effective-settings'
alias helpprofiles='smvn help:active-profiles'

#####
#test result common aliases (jregress/junit):
#####
alias jres="egrep 'Passed|FAILED|TIMED OUT'"
#include junit results as well
alias tres="egrep 'Tests run:|Passed|FAILED|TIMED OUT'"

opt=`optpath ` > /dev/null 2>&1
if [ $? -eq 0 -a "$opt" != "" ]; then
    export PATH="$opt"
else
    echo 'WARNING:  \$PATH optimizization failed (optpath command).'
fi

#####
#NOTE - the aliases below are used by the help command:
#####

function onecol { perl -n -a -F"'"'/\s+|:|;/'"'" -e "'"'{for (@F) {print "$_\n";}}'"'"; }
function addplus { perl -n -e "'"'{printf "+%d %s", $ii++, $_;}'"'"; }
alias pdirs='dirs|onecol|addplus'
alias showpath='echo $PATH|onecol'

function FORTEPJ_HELP_ONLY
{
    ##################
    #PERL USAGE NOTES:
    #   -n opens each arg and adds  while (<>) loop.
    #   -a auto-splits each line into @F as specified by -F pattern [ or defaults to /\s+/
    #   -e adds in the specified script - executed once per input line.
    ##################

    more << EOF

    =============================================================================
    ====================== STANDARD FORTE PROJECT SETTINGS ======================
    =============================================================================

    Standard command aliases:

    ### CVS aliases for old FORTE path tools:
    checkOut,pco        - cvs edit [files]
    uncheckOut,uco      - cvs unedit [files]
    checkIn,pci     - not available
    integratePath,integrate - cvs commit [directories]
    updatePath,update   - cvs update -d -P [directories]
    newDir,newdir       - mkdir; cvs add [directory]
    newFile,newfile     - cvs add [files]
    includeService      - cvs release -d [modules]
    deleteService       - cvs release -d [directories]

    ### CVS shortcuts:
    cvsdiff - cvs diff
    cvslog  - cvs log

    ### update your tools:
    pull      - runs \$TOOLROOT/boot/updateDist (updates tools)

    Informational:

    pathinfo - show usePath & other important environment settings.

    =============================================================================
EOF
}
