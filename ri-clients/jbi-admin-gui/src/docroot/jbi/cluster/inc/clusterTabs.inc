<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/cluster/inc/clusterTabs.inc -->

<sun:tabSet id="clusterTabs" immediate="$boolean{true}" selected="#{sessionScope.clusterTabs}">
     
    <sun:tab id="general" immediate="$boolean{true}" text="$resource{i18n2.cluster.GeneralTab}" >
        <!command
            setSessionAttribute(key="clusterTabs" value="general");
            redirect(page="../../cluster/clusterGeneral.jsf?clusterName=$pageSession{instanceName}");
        />       
    </sun:tab>

    <sun:tab id="clusterApp" immediate="$boolean{true}" text="$resource{i18n2.cluster.Applications}">
	<!command
            setSessionAttribute(key="clusterTabs" value="clusterApp");
            redirect(page="../../cluster/clusterApplications.jsf?name==$pageSession{instanceName}");
        />
    </sun:tab>

    <sun:tab id="clusterInst" immediate="$boolean{true}" text="$resource{i18n2.cluster.InstancesTab}">
	<!command
            setSessionAttribute(key="clusterTabs" value="clusterInst");
            redirect(page="../../cluster/clusterInstances.jsf?clusterName=$pageSession{instanceName}");
        />
    </sun:tab>

     <sun:tab id="clusterResources" immediate="$boolean{true}" text="$resource{i18n2.cluster.ResourcesTab}">
	<!command
            setSessionAttribute(key="clusterTabs" value="clusterResources");
            redirect(page="../../cluster/clusterResources.jsf?name=$pageSession{instanceName}");
        />
    </sun:tab>

    <sun:tab id="clusterJbi" immediate="$boolean{true}" text="$resource{i18n2.cluster.JbiTab}">
	<!command
setSessionAttribute(key="clusterTabs" value="clusterIntegrationsDeployments");
redirect(page="../jbi/cluster/clusterDeployments.jsf?clusterName==$pageSession{instanceName}");
        />

#include "jbi/cluster/inc/clusterSubtabs.inc"

    </sun:tab>

    <sun:tab id="clusterPhysicalDest" immediate="$boolean{true}" text="$resource{i18n2.cluster.PhysicalDestTab}">
	<!command
            setSessionAttribute(key="clusterTabs" value="clusterPhysicalDest");
            redirect(page="../../cluster/clusterPhysicalDest.jsf?name=#{clusterName}");
        />
    </sun:tab>

    <sun:tab id="clusterProps" immediate="$boolean{true}" text="$resource{i18n2.cluster.PropertiesTab}">
	<!command
            setSessionAttribute(key="clusterTabs" value="clusterConfigProps");
            redirect(page="../../cluster/clusterConfigProperties.jsf?clusterName=#{clusterName}");
        />
        <sun:tab id="clusterConfigProps" immediate="$boolean{true}" text="$resource{i18n2.cluster.tab.ClusterConfigProperties}">
            <!command
                    setSessionAttribute(key="clusterTabs" value="clusterConfigProps");
                    redirect(page="../../cluster/clusterConfigProperties.jsf?clusterName=#{clusterName}");
            />
        </sun:tab>
            <sun:tab id="clusterInstanceProps" immediate="$boolean{true}" text="$resource{i18n2.cluster.tab.ClusterProperties}">
            <!command
                    setSessionAttribute(key="clusterTabs" value="clusterInstanceProps");
                    redirect(page="../../cluster/clusterProperties.jsf?clusterName=#{clusterName}");
                />
        </sun:tab>
    </sun:tab>

</sun:tabSet>
