<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/cluster/showCompAppConfigs.jsf -->

<sun:page>

    <!beforeCreate 
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks")

setSessionAttribute(key="showTabs", value="application")
setSessionAttribute(key="showCompAppTabs", value="configurations")

setSessionAttribute(key="showCompAppConfigTitleSuffixText", value='#{"binding-component"==sessionScope.sharedRequestType ? "$resource{i18n.jbi.show.comp.app.config.page.title.binding.suffix.text}"  : "$resource{i18n.jbi.show.comp.app.config.page.title.engine.suffix.text}" }')
setSessionAttribute(key="showNameLinkUrl", value="showCompAppConfigs.jsf")

getRequestValue(key="name", value=>$attribute{name});
getRequestValue(key="type", value=>$attribute{type});

//setJBIComponentId(JBIComponentName="$requestParameter{name}",JBIComponentType="$requestParameter{type}");
setJBIComponentId(JBIComponentName="$attribute{name}",JBIComponentType="$attribute{type}");
 
if (!$session{sharedShowName}) {
    //setSessionAttribute(key="sharedShowName" value="$requestParameter{name}")
    setSessionAttribute(key="sharedShowName" value="$attribute{name}")
}
if (!$session{sharedShowType}) {
    //setSessionAttribute(key="sharedShowType" value="$requestParameter{type}")
    setSessionAttribute(key="sharedShowType" value="$attribute{type}")
}
if (!$session{jbiSelectedInstanceValue}){
    setSessionAttribute(key="jbiSelectedInstanceValue", value="server" );
}

jbiSetCompAppConfigNamesData(
   compName="#{sessionScope.sharedShowName}"
   compType="#{sessionScope.sharedShowType}"
   instance="#{sessionScope.jbiSelectedInstanceValue}"
   alertDetails=>$session{jbiAlertDetails}
   alertSummary=>$session{jbiAlertSummary}
   instanceList=>$page{instanceList}
   isAlertNeeded=>$session{isJbiAlertNeeded}
   selectedInstance=>$session{jbiSelectedInstanceValue}
   showStartButton=>$session{showStartButton}
   installedOnZeroTargets=>$session{installedOnZeroTargets}
);
      
   
    />

    <sun:html>
     <sun:head id="showCompAppConfigHead" />

     <sun:body>

         <sun:form id="showCompAppConfigBreadcrumbsForm" > 
                <sun:hidden id="helpKey" value="$resource{help.jbi.cluster.showCompAppConfigs}" />

#include treeBreadcrumbs.inc

             </sun:form>

            <sun:form id="tabsForm">
#include "jbi/cluster/inc/showTabs.inc"
            </sun:form>

#include "jbi/cluster/inc/alertBox.inc"

      <sun:form id="showCompAppConfigTableForm" >

                "<br />
                <sun:image id="indentDropDown"
                    align  = "top"
                    height = "$int{10}"
                    url    = "/resource/images/dot.gif"
                    width  = "$int{8}"
                />
                <sun:dropDown id="FromInstance"
                    enabled    = "$session{isJbiInstanceDropDownEnabled}"
                    immediate  = "#{true}"
                    items      = "$pageSession{instanceList}"
                    label      = "$resource{i18n.jbi.show.comp.app.configs.from.instance.dropdown.label}"
                    selected   = "#{sessionScope.jbiSelectedInstanceValue}"
                    submitForm = "#{true}"
                    rendered   = "#{sessionScope.installedOnZeroTargets == 'false'}"
                    >
                    <!command
                        setAttribute(key="click" value="$this{component}");
                        setAttribute(key="fromValue" value="#{click.selected}")
                        setSessionAttribute(key="jbiSelectedInstanceValue", value="$attribute{fromValue}" );
                        setAttribute(key="dropDownChange" value="true")
                    jbiLogFine(where="showCompAppConfigs.jsf-fromDropDown", diagnostic="jbiSelectedInstanceValue=#{jbiSelectedInstanceValue}")
                        navigate(page="jbi/cluster/showCompAppConfigs.jsf");
                    />
                </sun:dropDown>
                <sun:title id="showCompAppConfigPageTitle"
                    title="#{sessionScope.sharedShowName} - #{sessionScope.showCompAppConfigTitleSuffixText}"
                    helpText="$resource{i18n.jbi.show.comp.app.config.page.help.inline.text}"
                    />

#include "jbi/cluster/inc/startComponent.inc"

"<br />               
            <sun:table id="showCompAppConfigTable"
                    deselectMultipleButton="$boolean{true}"
                    deselectMultipleButtonOnClick="setTimeout('changeOneTableButton()', 0)"
                    paginateButton="#{true}"
                    paginationControls="#{true}"
                    rendered="#{ShowBean.compAppConfigSupported}"
                    selectMultipleButton="$boolean{true}"
                    selectMultipleButtonOnClick="setTimeout('changeOneTableButton()', 0)" 
                    title="$resource{i18n.jbi.show.comp.app.config.table.title}"
            >  
                       
                    <!afterCreate
                        getClientId(component="$this{component}" clientId=>$page{tableId});
                    />
           
            <sun:tableRowGroup id="showCompAppConfigTableRowGroup"
                         aboveColumnHeader="#{true}"
                 selected="#{sourceVarRow.value.selected}"
             sourceData="#{ShowBean.compAppConfigNamesTableData}"
                 sourceVar="sourceVarRow"
                         >
                         
                         <sun:tableColumn id="sharedSelectedTableColumn" 
                              selectId="select"
                            <!--  sort="#{sourceVarRow.value.selectedState}"  -->                          
                              >

                              <sun:checkbox id="select" 
                    selected="#{sourceVarRow.value.selected}"
                selectedValue="$boolean{true}"
                                onClick="setTimeout('initAllRows(); changeOneTableButton()', 0)" 
                              />

                         </sun:tableColumn>

                         <sun:tableColumn id="sharedNamesTableColumn" 
                              headerText="$resource{i18n.jbi.shared.table.column.name.text}"
                              rowHeader="#{true}"
                              sort="#{sourceVarRow.value.name}"                             
                              >
                              
                              <sun:hyperlink id="showCompAppConfigTableNameHyperlink"
                                    rendered="#{true}"
                                    style="font-weight:normal"
                                    toolTip="$resource{i18n.jbi.shared.table.name.link.tooltip}" 
                                    value="#{sourceVarRow.value.name}"
                        url="editAppConfig.jsf?compName=$session{sharedShowName}&appConfigName=#{sourceVarRow.value.name}"
                                    />

                         </sun:tableColumn>

                         <sun:tableColumn id="sharedDescriptionTableColumn" 
                              headerText="$resource{i18n.jbi.shared.table.column.description.text}"
                              rowHeader="#{true}"
                              rendered="#{false}"
                              sort="#{sourceVarRow.value.description}"                             
                              >
                          <!-- the Application Configuration APIs do not support a description -->
                              <sun:staticText id="sharedDescriptionText" 
                                  value="#{sourceVarRow.value.description}"
                                  />
                         </sun:tableColumn>

                    </sun:tableRowGroup>

                    <!facet actionsTop>

                         <sun:panelGroup id="topActionsGroup1">

                               <!afterCreate
                                   getClientId(component="$this{component}" clientId=>$page{topActionGroup});
                               />

                               <sun:button id="newAppConfigButton"
                                   primary="#{true}"
                                   styleClass="Btn1"
                                   text="$resource{i18n.jbi.button.AddCompAppConfig}" 
                                   url='#{sessionScope["newButtonUrl"]}' 
                                   >
                   <!command
navigate(page="jbi/cluster/newAppConfig.jsf");
                       />
                       </sun:button>

                               <sun:image id="actionTopSeparator1"
                                   align="top"
                                   height="$int{18}"
                                   url="/resource/images/jbi/actions_separator.gif"
                                   width="$int{18}"
                                   />

                               <sun:button id="button1"
                   disabled="#{true}"
                                   primary="#{false}"
                                   text="$resource{i18n.jbi.button.DeleteCompAppConfig}" 
                                   onClick='
                                       if (! getConfirm(this, "$resource{i18n.jbi.show.comp.app.configs.delete.confirmation}")) 
                                           return false;
                                       return submitAndDisable(this, "$resource{i18n2.button.Processing}");'   
                                   >
                <!command
getUIComponent(clientId="showCompAppConfigTableForm:showCompAppConfigTable:showCompAppConfigTableRowGroup", component=>$attribute{tableRowGroup});  
jbiDeleteSelectedCompAppConfigRows(compName="$session{sharedShowName}", instanceName="#{jbiSelectedInstanceValue}", tableRowGroup="$attribute{tableRowGroup}", isAlertNeeded=>$session{isJbiAlertNeeded}, alertSummary=>$session{jbiAlertSummary}, alertDetails=>$session{jbiAlertDetails});
redirect(page="showCompAppConfigs.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}");
                />              
                   </sun:button>


                         </sun:panelGroup>

                    </facet>

                </sun:table>

             </sun:form>

     </sun:body> 
     
#include "changeButtonsJS.inc"           

     </sun:html>  
 </sun:page>
