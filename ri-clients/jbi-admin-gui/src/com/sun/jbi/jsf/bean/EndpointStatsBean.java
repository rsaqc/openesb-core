/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 * EndpointStatsBean.java
 */

package com.sun.jbi.jsf.bean;

/**
 * holds component endpoint stats for
 * a single consuming or providing endpoint
 */
public class EndpointStatsBean
{
    /**
     * no-arg ctor
     */
    public EndpointStatsBean()
    {
    }

    //
    // getters
    //

    /**
     * gets endpoint stats disablement
     * @return boolean true if endpoint stats is to be disabled
     */
    public boolean getDisabled()
    {
        return mDisabled;
    }

    /**
     * gets endpoint stats name
     * @return String name
     */
    public String getEndpointName()
    {
        return mEndpointName;
    }

    /**
     * gets endpoint stats received dones
     * @return String receivedDones
     */
    public String getReceivedDones()
    {
        return mReceivedDones;
    }
    /**
     * gets endpoint stats received errors
     * @return String receivedErrors
     */
    public String getReceivedErrors()
    {
        return mReceivedErrors;
    }
    /**
     * gets endpoint stats received faults
     * @return String receivedFaults
     */
    public String getReceivedFaults()
    {
        return mReceivedFaults;
    }
    /**
     * gets endpoint stats received replies
     * @return String receivedReplies
     */
    public String getReceivedReplies()
    {
        return mReceivedReplies;
    }
    /**
     * gets endpoint stats received requests
     * @return String receivedRequests
     */
    public String getReceivedRequests()
    {
        return mReceivedRequests;
    }

    /**
     * gets endpoint stats rendering
     * @return boolean true if endpoint stats is to be rendering
     */
    public boolean getRendered()
    {
        return mRendered;
    }

    /**
     * gets endpoint stats selection
     * @return boolean true if endpoint stats is to selected
     */
    public boolean getSelected()
    {
        return mSelected;
    }

    /**
     * gets endpoint stats sent dones
     * @return String sentDones
     */
    public String getSentDones()
    {
        return mSentDones;
    }
    /**
     * gets endpoint stats sent errors
     * @return String sentErrors
     */
    public String getSentErrors()
    {
        return mSentErrors;
    }
    /**
     * gets endpoint stats sent faults
     * @return String sentFaults
     */
    public String getSentFaults()
    {
        return mSentFaults;
    }
    /**
     * gets endpoint stats sent replies
     * @return String sentReplies
     */
    public String getSentReplies()
    {
        return mSentReplies;
    }
    /**
     * gets endpoint stats sent requests
     * @return String sentRequests
     */
    public String getSentRequests()
    {
        return mSentRequests;
    }

    //
    // setters
    //

    /**
     * sets endpoint stats disablement
     * @param isDisabled boolean true if endpoint stats is to be disabled
     */
    public void setDisabled(boolean isDisabled)
    {
        mDisabled = isDisabled;
    }

    /**
     * sets endpoint stats endpoint name
     * @param aEndpointName String with endpoint stats endpoint name
     */
    public void setEndpointName(String aEndpointName)
    {
        // make potentially long strings "wrappable"
        // in table column
        mEndpointName = 
            aEndpointName.replaceAll(",",",\n"); 
    }

    /**
     * sets endpoint stats received dones
     * @param aReceivedDones String with endpoint stats received dones
     */
    public void setReceivedDones(String aReceivedDones)
    {
        mReceivedDones = aReceivedDones;
    }

    /**
     * sets endpoint stats received errors
     * @param aReceivedErrors String with endpoint stats received errors
     */
    public void setReceivedErrors(String aReceivedErrors)
    {
        mReceivedErrors = aReceivedErrors;
    }

    /**
     * sets endpoint stats received faults
     * @param aReceivedFaults String with endpoint stats received faults
     */
    public void setReceivedFaults(String aReceivedFaults)
    {
        mReceivedFaults = aReceivedFaults;
    }

    /**
     * sets endpoint stats received replies
     * @param aReceivedReplies String with endpoint stats received replies
     */
    public void setReceivedReplies(String aReceivedReplies)
    {
        mReceivedReplies = aReceivedReplies;
    }

    /**
     * sets endpoint stats received requests
     * @param aReceivedRequests String with endpoint stats received requests
     */
    public void setReceivedRequests(String aReceivedRequests)
    {
        mReceivedRequests = aReceivedRequests;
    }

    /**
     * sets endpoint stats rendering
     * @param isRendered boolean true if endpoint stats is to be rendered
     */
    public void setRendered(boolean isRendered)
    {
        mRendered = isRendered;
    }

    /**
     * sets endpoint stats selection state
     * @param isSelected boolean true if endpoint stats is to be selected
     */
    public void setSelected(boolean isSelected)
    {
        mSelected = isSelected;
    }

    /**
     * sets endpoint stats sent dones
     * @param aSentDones String with endpoint stats sent dones
     */
    public void setSentDones(String aSentDones)
    {
        mSentDones = aSentDones;
    }

    /**
     * sets endpoint stats sent errors
     * @param aSentErrors String with endpoint stats sent errors
     */
    public void setSentErrors(String aSentErrors)
    {
        mSentErrors = aSentErrors;
    }

    /**
     * sets endpoint stats sent faults
     * @param aSentFaults String with endpoint stats sent faults
     */
    public void setSentFaults(String aSentFaults)
    {
        mSentFaults = aSentFaults;
    }

    /**
     * sets endpoint stats sent replies
     * @param aSentReplies String with endpoint stats sent replies
     */
    public void setSentReplies(String aSentReplies)
    {
        mSentReplies = aSentReplies;
    }

    /**
     * sets endpoint stats sent requests
     * @param aSentRequests String with endpoint stats sent requests
     */
    public void setSentRequests(String aSentRequests)
    {
        mSentRequests = aSentRequests;
    }

    // 
    // fields
    //

    /**
     * endpoint stats disabled attribute
     */
    private boolean mDisabled;

    /**
     * endpoint stats endpoint name attribute
     */
    private String mEndpointName;

    /**
     * endpoint stats received dones attribute
     */
    private String mReceivedDones;

    /**
     * endpoint stats received errors attribute
     */
    private String mReceivedErrors;

    /**
     * endpoint stats received faults attribute
     */
    private String mReceivedFaults;

    /**
     * endpoint stats received replies attribute
     */
    private String mReceivedReplies;

    /**
     * endpoint stats received requests attribute
     */
    private String mReceivedRequests;

    /**
     * endpoint stats rendered attribute
     */
    private boolean mRendered;

    /**
     * endpoint stats selected attribute
     */
    private boolean mSelected;

    /**
     * endpoint stats sent dones attribute
     */
    private String mSentDones;

    /**
     * endpoint stats sent errors attribute
     */
    private String mSentErrors;

    /**
     * endpoint stats sent faults attribute
     */
    private String mSentFaults;

    /**
     * endpoint stats sent replies attribute
     */
    private String mSentReplies;

    /**
     * endpoint stats sent requests attribute
     */
    private String mSentRequests;


}
