/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */

/*
 *  JBIConstants.java
 */

package com.sun.jbi.jsf.util;

/**
 * Contstants related to the admin-gui implementation
 *
 * @author   Sun Microsystems Inc.
 */

public final class SharedConstants
{

    /**
     * Used as a <code>Properties</code> key when passing an alert detail message
     */
    public static final String ALERT_DETAIL_KEY = "alert-details"; // not I18n

    /**
     * Used as a <code>Properties</code> key when passing an alert summary message
     */
    public static final String ALERT_SUMMARY_KEY = "alert-summary"; // not I18n

    /**
     * Used when creating an Application Configuration
     */
    public static final String APP_CONFIG_ACTION_CREATE = 
        "APP_CONFIG_ACTION_CREATE"; // not I18n

    /**
     * Used when updating an Application Configuration
     */
    public static final String APP_CONFIG_ACTION_UPDATE = 
        "APP_CONFIG_ACTION_UPDATE"; // not I18n

    /**
     * Used when editing/creating application variables of boolean type
     */
    public static final String APP_VAR_TYPE_BOOLEAN = "[BOOLEAN]"; // not I18n

    /**
     * Used when editing/creating application variables of numeric type
     */
    public static final String APP_VAR_TYPE_NUMBER = "[NUMBER]"; // not I18n

    /**
     * Used when editing/creating application variables of password type
     */
    public static final String APP_VAR_TYPE_PASSWORD = "[PASSWORD]"; // not I18n

    /**
     * Used when editing/creating application variables of string type
     */
    public static final String APP_VAR_TYPE_STRING = "[STRING]"; // not I18n

    /**
     * <code>#{sharedTableType}</code> session variable for JBI Components
     */
    public static final String COMPONENT_TABLE_TYPE = "bindingsEngines"; // not I18n

    /**
     * <code>#{sharedTableType}</code> session variable for JBI Service Assemblies
     */
    public static final String DEPLOYMENT_TABLE_TYPE = "deployment"; // not I18n


    /**
     * Binding, Engine, SA state dropDown filtering on the 
     * cluster-profile list components/deployments pages
     */
    public static final String DROP_DOWN_STATE_SHOW_ALL = "Show All"; // not I18n

    /**
     * Represents a filter by state dropDown Disabled choice
     */
    public static final String DROP_DOWN_STATE_DISABLED = "Disabled"; // not I18n

    /**
     * Represents a state dropDown Enabled choice
     */
    public static final String DROP_DOWN_STATE_ENABLED = "Enabled"; // not I18n

    /**
     * Represents a filter by state dropDown No Targets choice
     */
    public static final String DROP_DOWN_STATE_NO_TARGETS = "No Targets"; // not I18n

    /**
     * Binding Engine type filtering on the list components page
     */
    public static final String DROP_DOWN_TYPE_SHOW_ALL = "Show All"; // not I18n

    /**
     * Represents a JBI binding-component
     */
    public static final String DROP_DOWN_TYPE_BINDING = "Binding"; // not I18n

    /**
     * Represents a JBI service-engine
     */
    public static final String DROP_DOWN_TYPE_ENGINE = "Engine"; // not I18n

    /**
     * Consuming endpoints type <code>ENDPOINTS_TYPE_CONSUMING</code>
     */
    public static final String ENDPOINTS_TYPE_CONSUMING =
        "ENDPOINTS_TYPE_CONSUMING"; // not I18n

    /**
     * Providing endpoints type <code>ENDPOINTS_TYPE_PROVIDING</code>
     */
    public static final String ENDPOINTS_TYPE_PROVIDING =
        "ENDPOINTS_TYPE_PROVIDING"; // not I18n

    /**
     * for storing/retrieving a failure result from <code>Properties</code> objects
     */
    public static final String FAILURE_RESULT = "failure-result"; // not I18n

    /**
     * Used when uninstalling components or undeploying service assemblies
     */
    public static final boolean FORCE_DELETE = true;

    /**
     * for adding a break between lines (e.g. in Alert box multiline details messages)
     * <code>&lt;br /&gt</code>
     */
    public static final String HTML_BREAK = "<br />"; // not I18n

    /**
     * for storing/retrieving an internal error from <code>Properties</code> objects
     */
    public static final String INTERNAL_ERROR = "internal-error"; // not I18n

    /**
     * for storing/retrieving names from <code>Properties</code> objects
     */
    public static final String KEY_NAME = "name"; // not I18n

    /** 
     * for storing/retrieving types from <code>Properties</code> objects
     */
    public static final String KEY_TYPE = "type"; // not I18n

    /** 
     * for storing/retrieving values from <code>Properties</code> objects
     */
    public static final String KEY_VALUE = "value"; // not I18n

    /**
     * <code>#{sharedTableType}</code> session variable for JBI Service Assemblies
     */
    public static final String LIBRARY_TABLE_TYPE = "libraries"; // not I18n

    /**
     * used when calling JBICommonClient api to bypass filtering by component
     */
    public static final String NO_COMPONENT_CHECK = null; // not I18n

    /**
     * used when calling JBICommonClient api to bypass filtering by service assembly
     */
    public static final String NO_DEPLOYMENT_CHECK = null; // not I18n

    /**
     * Used when uninstalling components or undeploying service assemblies
     */
    public static final boolean NO_FORCE_DELETE = false;

    /**
     * used when calling JBICommonClient api to bypass filtering by library
     */
    public static final String NO_LIBRARY_CHECK = null; // not I18n

    /**
     * used when calling JBICommonClient api to bypass state checking
     */
    public static final String NO_STATE_CHECK = null; // not I18n

    /**
     * Used for an application server instance state
     */
    public static final String STATE_RUNNING = "running"; // not I18n

    /**
     * Used as a default state of a JBI component or service assembly 
     * when the actual state cannot be determined (for checking/logging only)
     */
    public static final String STATE_UNKNOWN = "*unknown*"; // not I18n

    /**
     * Used when displaying the I18n state of a JBI component or service assembly 
     * when the state cannot be determined
     */
    public static final String STATE_UNKNOWN_KEY = 
        "jbi.operations.comp.state.unknown"; // not I18n

    /**
     * for storing/retrieving a success result from <code>Properties</code> objects
     */
    public static final String SUCCESS_RESULT = "success-result"; // not I18n

    /**
     * for storing/retrieving a warning result from <code>Properties</code> objects
     */
    public static final String WARNING_RESULT = "warning-result"; // not I18n

    /**
     * for storing/retrieving a warning summary from <code>Properties</code> objects
     */
    public static final String WARNING_SUMMARY = "warning-summary"; // not I18n

    /**
     * Used to determine if the runtime error message is a warning message
     */
    public static final String MESSAGE_WARNING_TAG = "WARNING:";


    /**
     * prevents instantiation and subclassing
     */
    private SharedConstants()
    {
    }
}
