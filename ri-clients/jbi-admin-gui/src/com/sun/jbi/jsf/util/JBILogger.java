/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  JBILogger.java
 *
 */
package com.sun.jbi.jsf.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides logging facilities for JBI admin-gui web console classes
 *
 * @author   Sun Microsystems Inc.
 */
public final class JBILogger
{

    /**
     * returns an instance of the logger for the glassfish admin-gui
     * JBI subconsole package
     * i.e. <code>com.sun.jbi.jsf</code>
     *
     * @return the logger
     */
    public static synchronized Logger getInstance()
    {
        if (sLogger == null)
            {
                sLogger = Logger.getLogger(PACKAGE_NAME);
            }
        return sLogger;
    }

    /**
     * Determines if a fine (or a higher) level of logging is 
     * currently being used
     * @return true if messages at the <code>FINE</code> level
     * are currently being logged
     */
    public static boolean isLoggableFine()
    {
        boolean result = false;
        if (null != sLogger)
            {
                result =
                    sLogger.isLoggable(Level.FINE);
            }
        return result;
    }

    /**
     * Determines if a finer (or a higher) level of logging is 
     * currently being used
     * @return true if messages at the <code>FINER</code> level
     * are currently being logged
     */
    public static boolean isLoggableFiner()
    {
        boolean result = false;
        if (null != sLogger)
            {
                result =
                    sLogger.isLoggable(Level.FINER);
            }
        return result;
    }

    /**
     * Logs, at a <code>FINER</code> level, the entry
     * into a method that has no arguments
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged 
     */
    public static void entering(String aClassName,
                                String aMethodName)
    {
        if (isLoggableFiner())
            {
                sLogger.entering(aClassName,
                                 aMethodName);
            }
    }

    /**
     * Logs, at a <code>FINER</code> level, the entry
     * into a method that has one or more arguments
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged 
     * @param anArgsArray the arguments to be logged
     */
    public static void entering(String aClassName,
                                String aMethodName,
                                Object[] anArgsArray)
    {
        if (isLoggableFiner())
            {
                sLogger.entering(aClassName,
                                 aMethodName,
                                 anArgsArray);
            }
    }

    /**
     * Logs, at a <code>FINER</code> level, the exit
     * from a method that returns no result
     * (or returns a result that is not to be logged)
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged 
     */
    public static void exiting(String aClassName,
                               String aMethodName)
    {
        if (sLogger.isLoggable(Level.FINER))
            {
                sLogger.exiting(aClassName,
                                aMethodName);
            }
    }

    /**
     * Logs, at a <code>FINER</code> level, the exit
     * from a method that returns a result
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged 
     * @param aResult the result to be logged
     */
    public static void exiting(String aClassName,
                               String aMethodName,
                               Object aResult)
    {
        if (isLoggableFiner())
            {
                sLogger.exiting(aClassName,
                                aMethodName,
                                aResult);
            }
    }

    /**
     * Logs, at a <code>FINE</code> level, a diagnostic
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged
     * @param aNonI18nMessage the non-I18n diagnostic to be logged
     */
    public static void logFine(String aClassName,
                               String aMethodName,
                               String aNonI18nMessage)
    {
        if (isLoggableFine())
            {
                sLogger.logp(Level.FINE,
                             aClassName,
                             aMethodName,
                             buildMessage(aClassName,
                                          aMethodName,
                                          aNonI18nMessage));
            }
    }

    /**
     * Logs, at a <code>FINER</code> level, a diagnostic
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged
     * @param aNonI18nMessage the non-I18n diagnostic to be logged
     */
    public static void logFiner(String aClassName,
                                String aMethodName,
                                String aNonI18nMessage)
    {
        if (isLoggableFiner())
            {
                sLogger.logp(Level.FINER,
                             aClassName,
                             aMethodName,
                             buildMessage(aClassName,
                                          aMethodName,
                                          aNonI18nMessage));
            }
    }

    /**
     * Logs, at a <code>FINE</code> level, an I18n message
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged
     * @param aMessageKey the key of an I18n message,
     * without inserts, to be logged
     */
    public static void logpFine(String aClassName,
                                String aMethodName,
                                String aMessageKey)
    {
        if (isLoggableFine())
            {
                sLogger.logrb(Level.FINE,
                              aClassName,
                              aMethodName,
                              JBI_JSF_BUNDLE_NAME,
                              aMessageKey);
            }
    }

    /**
     * Logs, at a <code>FINE</code> level, an I18n message
     * with inserts
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged
     * @param aMessageKey the key of an I18n message,
     * with inserts, to be logged
     * @param anInsertsArray an array of message inserts
     */
    public static void logpFine(String aClassName,
                                String aMethodName,
                                String aMessageKey,
                                Object[] anInsertsArray)
    {
        if (isLoggableFine())
            {
                sLogger.logrb(Level.FINE,
                              aClassName,
                              aMethodName,
                              JBI_JSF_BUNDLE_NAME,
                              aMessageKey,
                              anInsertsArray);
            }
    }

    /**
     * Logs, at a <code>FINER</code> level, an I18n message
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged
     * @param aMessageKey the key of an I18n message,
     * without inserts, to be logged
     */
    public static void logpFiner(String aClassName,
                                 String aMethodName,
                                 String aMessageKey)
    {
        if (isLoggableFiner())
            {
                sLogger.logrb(Level.FINER,
                              aClassName,
                              aMethodName,
                              JBI_JSF_BUNDLE_NAME,
                              aMessageKey);
            }
    }

    /**
     * Logs, at a <code>FINER</code> level, an I18n message
     * with inserts
     * @param aClassName the class to be logged
     * @param aMethodName the method to be logged
     * @param aMessageKey the key of an I18n message,
     * with inserts, to be logged
     * @param anInsertsArray an array of message inserts
     */
    public static void logpFiner(String aClassName,
                                 String aMethodName,
                                 String aMessageKey,
                                 Object[] anInsertsArray)
    {
        if (isLoggableFiner())
            {
                sLogger.logrb(Level.FINER,
                              aClassName,
                              aMethodName,
                              JBI_JSF_BUNDLE_NAME,
                              aMessageKey,
                              anInsertsArray);
            }
    }

    /**
     * Builds a log string of the form: 
     * <code>"ClassName.MethodName() message"</code>
     * @param aClassName the class name part of the string
     * @param aMethodName the method name part of the string
     * @param aMessage the remainder of the string
     * @return the combined string 
     */
    private static final String buildMessage(String aClassName,
                                             String aMethodName,
                                             String aMessage)
    {
        StringBuffer logMessage =
            new StringBuffer(aClassName);
        logMessage.append(".");
        logMessage.append(aMethodName);
        logMessage.append("() ");
        logMessage.append(aMessage);
        return logMessage.toString();
    }

    private static Logger sLogger;
    private static final String PACKAGE_NAME =
        "com.sun.jbi.jsf";

    private static final String JBI_JSF_BUNDLE_NAME =
        "com.sun.jbi.jsf.resources.Bundle.properties";

    /**
     * prevents instantiation
     */
    private JBILogger()
    {
    }
}
