/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.jbi.jsf.util;

import com.sun.enterprise.tools.admingui.util.GuiUtil;
import com.sun.jbi.jsf.bean.LoggingBean;
import com.sun.jbi.jsf.bean.AlertBean;
import com.sun.jbi.jsf.factory.PropertySheetAdaptorBase;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jsftemplating.layout.descriptors.LayoutComponent;
import com.sun.webui.jsf.component.Property;
import com.sun.webui.jsf.component.StaticText;
import com.sun.webui.jsf.component.DropDown;
import com.sun.webui.jsf.component.HiddenField;
import com.sun.webui.jsf.model.Option;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.io.InputStream;
import org.w3c.dom.Document;  


/**
 *  <p> The <code>JBIRuntimeLogLevelsPropertySheetAdaptor</code> implementation must have a
 *  <code>public static JBIRuntimeLogLevelsPropertySheetAdaptor getInstance(FacesContext,
 *  LayoutComponent, UIComponent)</code> method in order to get access to
 *  an instance of the <code>JBIRuntimeLogLevelsPropertySheetAdaptor</code> instance.</p>
 *
 *  <p> This class is used by <code>DynamicPropertySheetNodeFactory</code>.</p>
 *
 *
 */
public class JBIRuntimeLogLevelsPropertySheetAdaptor extends PropertySheetAdaptorBase 
{

    private static Logger sLog;
    private JBIAdminCommands mJac = BeanUtilities.getClient();
     
    /**
     *  <p> This constructor is not used.</p>
     */
    private JBIRuntimeLogLevelsPropertySheetAdaptor() 
    {
    }


    /**
     *  <p> This constructor saves the <code>LayoutComponent</code> descriptor
     *      and the <code>UIComponent</code> associated with this
     *      <code>PropertySheetAdaptor</code>.  This constructor is used by the
     *      getInstance() method.</p>
     * @param desc layout description
     * @param parent parent component for this component
     */
    protected JBIRuntimeLogLevelsPropertySheetAdaptor(LayoutComponent desc, UIComponent parent) 
    {
        super(desc, parent);
    }


    /**
     *  <p> This method provides access to an <code>JBIRuntimeLogLevelsPropertySheetAdaptor</code>
     *      instance.  Each time it is invoked, it returns a new instance.</p>
     * @param ctx faces context
     * @param desc layout description
     * @param parent parent component for this component
     * @return a new instance 
     */
    public static JBIRuntimeLogLevelsPropertySheetAdaptor getInstance(FacesContext ctx, 
                                                                      LayoutComponent desc, 
                                                                      UIComponent parent) 
    {
        return new JBIRuntimeLogLevelsPropertySheetAdaptor(desc, parent);
    }


    /**
     *  <p> Method that is called to initialize the PropertySheet component.</p>
     */
    public void init() 
    {
        // Initialise the logger
        sLog = JBILogger.getInstance();

        // The parent UIComponent
        UIComponent parent = getParentUIComponent();

        // Retrieve the required option values
        mPropertySheetId           = getRequiredOptionValue("propertySheetId", parent);
        mPropertySheetSectionIdTag = getRequiredOptionValue("propertySheetSectionIdTag", parent);
        mPropertyIdTag             = getRequiredOptionValue("propertyIdTag", parent);
        mStaticTextIdTag           = getRequiredOptionValue("staticTextIdTag", parent);
        mDropDownIdTag             = getRequiredOptionValue("dropDownIdTag", parent);
        mHiddenFieldIdTag          = getRequiredOptionValue("hiddenFieldIdTag", parent);
    }


    /**
     *  <p> Method that is called to retrieve the property sheet object.
     * 
     *  @param  parent - The parent component
     *  @return PropertySheet component
     */
    public UIComponent getPropertySheet(UIComponent parent) 
    {
        constructPropertySheet(parent);
        return parent;
    }

    
    /**
     * Helper class that will create the property sheet components.
     * @param parent parent component for this component
     * @return updated property sheet in parent
     */
    private UIComponent constructPropertySheet(UIComponent parent) 
    {
        String instanceName   = getRequiredOptionValue("instanceName", parent);
        String targetName     = (String)getOptionValue("targetName", parent);
        String additionalFile = (String)getOptionValue("additionalLoggerFile", parent);

        // If a config file is specified, the get the input stream to it.
        InputStream configFileInputStream = null;
        String configFileName = (String)getOptionValue("configFile", parent);
        if (configFileName != null) 
        {
            try {
                configFileInputStream = this.getClass().getResourceAsStream(configFileName);
            } 
            catch (Exception ex) {
                sLog.fine("JBILogLevelsPropertySheetAdaptor(): Error retrieving input stream for Logger Config File: " + configFileInputStream);
            }
        }
        Document document = SystemLoggerUtilities.getConfigDocument(configFileInputStream);

        // Initialize the Save button to be enabled
        LoggingBean loggingBean = BeanUtilities.getLoggingBean();
        loggingBean.setSaveButtonDisabled(false);

        // Retrieve any default value.  This would be used instead in the dropdown
        // instead of the level value read from the MBean.
        String defaultLevel  = (String)getOptionValue("dropDownDefaultLevel", parent);

        if (targetName == null)
        {
            targetName = instanceName;
        }

        TreeMap logLevels = SystemLoggerUtilities.getJBIRuntimeLoggerLevels(targetName,instanceName);
        if (logLevels == null)
        {
            logLevels = new TreeMap();
        }

        // Retrieve the no default list from the logger config file
        ArrayList noDefaultList = SystemLoggerUtilities.getNoDefaultList (document);

        UIComponent propertySheetSection = getPropertySheetSectionComponent(parent);

        if (logLevels.size() == 0)
        {
            UIComponent propertyComponent = getPropertyComponent(propertySheetSection);
            String msg = "";
            ((Property)propertyComponent).setLabel(msg);
            loggingBean.setSaveButtonDisabled(true);
            String alertSummary = I18nUtilities.getResourceString("jbi.internal.error.summary");
            String alertDetails = I18nUtilities.getResourceString("jbi.runtime.loggers.none.found");
            AlertBean alertBean = BeanUtilities.getAlertBean();
            alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
            alertBean.setAlertSummary(alertSummary);
            alertBean.setAlertDetail(alertDetails);
        } 
        else
        {
            Set logLevelSet = null;
            Level level = null;
            String label = "";
            logLevelSet = logLevels.keySet();
            Iterator iter = logLevelSet.iterator();
            while (iter.hasNext())
            {
                String name = (String)iter.next();
                label = SystemLoggerUtilities.getRuntimeDisplayName (name, 
                                                                     targetName,
                                                                     instanceName);
                level = (Level)logLevels.get(name);

                String displayName = "(" + name + ")";

                UIComponent propertyComponent    = getPropertyComponent(propertySheetSection);
                UIComponent dropDownComponent    = getDropDownComponent(propertyComponent);
                UIComponent staticTextComponent  = getStaticTextComponent(propertyComponent);
                UIComponent hiddenFieldComponent = getHiddenFieldComponent(propertyComponent);

                ((Property)propertyComponent).setLabelAlign("left");
                ((Property)propertyComponent).setNoWrap(true);
                ((Property)propertyComponent).setOverlapLabel(false);
                ((Property)propertyComponent).setLabel(label);

                Option optionList[] =  SystemLoggerUtilities.getLogLevelDropDownValues (name, 
                                                                                        noDefaultList);
                ((DropDown)dropDownComponent).setItems(optionList);

                String levelName = level.getName();
                if (defaultLevel != null)
                {
                    levelName = SystemLoggerUtilities.getDefaultLevel(optionList, defaultLevel);
                }

                ((DropDown)dropDownComponent).setSelected(levelName);
                ((StaticText)staticTextComponent).setText(displayName);
                ((HiddenField)hiddenFieldComponent).setText(name);
            } 
        }
        
        return parent;
    }


    /**
     *  <p> This method returns the "options" that should be supplied to the
     *      factory that creates the <code>PropertySheet</code>.</p>
     *
     *  <p> Some useful options for the standard <code>PropertySheet</code>
     *      component include:<p>
     *
     * <ul><li>propertySheetId</li>
     * <li>propertySheetSectionIdTag</li>
     * <li>propertyIdTag</li>
     * <li>staticTextIdTag</li>
     * <li>dropDownIdTag</li>
     * <li>dropDownDefaultLevel</li>
     * <li>hiddenFieldIdTag</li>
     * <li>componentName</li>
     * <li>targetName</li>
     * <li>instanceName</li>
     * <li>propertySheetAdaptorClass</li></ul>
     *
     *  <p> See PropertySheet component documentation for more details.</p>
     * @return a map of property IDs to layout descriptors
     */
    public Map<String, Object> getFactoryOptions() 
    {
        Map<String, Object> result = null;
        LayoutComponent desc = getLayoutComponent();
        result = new HashMap<String, Object>();
        setProperty(result, "propertySheetId", desc.getOption("propertySheetId"));
        setProperty(result, "propertySheetSectionIdTag", desc.getOption("propertySheetSectionIdTag"));
        setProperty(result, "propertyIdTag", desc.getOption("propertyIdTag"));
        setProperty(result, "staticTextIdTag", desc.getOption("staticTextIdTag"));
        setProperty(result, "dropDownIdTag", desc.getOption("dropDownIdTag"));
        setProperty(result, "dropDownDefaultLevel", desc.getOption("dropDownDefaultLevel"));
        setProperty(result, "hiddenFieldIdTag", desc.getOption("hiddenFieldIdTag"));
        setProperty(result, "componentName", desc.getOption("componentName"));
        setProperty(result, "targetName", desc.getOption("targetName"));
        setProperty(result, "instanceName", desc.getOption("instanceName"));
        setProperty(result, "propertySheetAdaptorClass", desc.getOption("propertySheetAdaptorClass"));
        return result;
    }


    /**
     *  <p> Helper method for setting Properties while avoiding NPE's.</p>
     * @param props a map to be updated
     * @param key a property name
     * @param value a possibly null property value
     */
    private void setProperty(Map props, String key, Object value) 
    {
        if (value != null)
            {
                props.put(key, value);
            }
    }


    /**
     *  <p> This method returns any facets that should be applied to the
     *      <code>PropertySheetNode (comp)</code>.  Useful facets for the sun
     *      <code>PropertySheetNode</code> component are: "content" and "image".</p>
     *
     *  <p> Facets that already exist on <code>comp</code>, or facets that
     *      are directly added to <code>comp</code> do not need to be returned
     *      from this method.</p>
     *
     *  <p> This implementation directly adds a "content" facet and returns
     *      <code>null</code> from this method.</p>
     *
     *  @param  comp        The PropertySheet node <code>UIComponent</code>.
     *  @param  nodeObject  The (model) object representing the PropertySheet node.
     *  @return null (this method must be declared, but it is not intended to be called)
     */
    public Map<String, UIComponent> getFacets(UIComponent comp, Object nodeObject) 
    {
        return null;
    }


    /**
     *  <p> Advanced framework feature which provides better handling for
     *      things such as expanding PropertySheetNodes, beforeEncode, and other
     *      events.</p>
     *
     *  <p> This method should return a <code>Map</code> of <code>List</code>
     *      of <code>Handler</code> objects.  Each <code>List</code> in the
     *      <code>Map</code> should be registered under a key that cooresponds
     *      to to the "event" in which the <code>Handler</code>s should be
     *      invoked.</p>
     * @param comp unused
     * @param nodeObject unused
     * @return null (this method must be declared, but is not intended to be called)
     */
    public Map getHandlersByType(UIComponent comp, Object nodeObject) 
    {
        return null;
    }
}
