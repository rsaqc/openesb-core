/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  ESBConstants.java
 */
package com.sun.jbi.jsf.util;

/**
 * ESB Contstants
 * Declares namespace URIs, elements, and attributes for
 * Open ESB extensions to JBI Components' jbi.xml
 *
 * @author   Sun Microsystems Inc.
 */
public final class ESBConstants
{
    /**
     * <code>configurationName</code> application configuration property name
     */
    public static final String APP_CONFIG_NAME_PROP = "configurationName";
    /**
     * <code>defaultValue</code> attribute
     */
    public static final String ATTR_DEFAULT_VALUE = "defaultValue";
    /**
     * <code>displayDescription</code> attribute
     */
    public static final String ATTR_DISPLAY_DESCRIPTION = "displayDescription";
    /**
     * <code>displayName</code> attribute
     */
    public static final String ATTR_DISPLAY_NAME = "displayName";
    /**
     * <code>facet</code> attribute
     */
    public static final String ATTR_FACET = "facet";
    /**
     * <code>isApplicationRestartRequired</code> attribute
     */
    public static final String ATTR_IS_APP_RESTART_REQUIRED = "isApplicationRestartRequired";
    /**
     * <code>isComponentRestartRequired</code> attribute
     */
    public static final String ATTR_IS_COMP_RESTART_REQUIRED = "isComponentRestartRequired";
    /**
     * <code>encrypted</code> attribute
     */
    public static final String ATTR_IS_VALUE_ENCRYPTED = "encrypted";
    /**
     * <code>required</code> attribute
     */
    public static final String ATTR_IS_VALUE_REQUIRED = "required";
    /**
     * <code>isServerRestartRequired</code> attribute
     */
    public static final String ATTR_IS_SERVER_RESTART_REQUIRED = "isServerRestartRequired";
    /**
     * <code>maxOccurs</code> attribute
     */
    public static final String ATTR_MAX_OCCURS = "maxOccurs";
    /**
     * <code>maxOccurs</code> default (<code>1</code>) value 
     */
    public static final String ATTR_MAX_OCCURS_VALUE_DEFAULT = "1";
    /**
     * <code>maxOccurs</code> <code>unbounded</code> value (indicates array)
     */
    public static final String ATTR_MAX_OCCURS_VALUE_UNBOUNDED = "unbounded";
    /**
     * <code>name</code> attribute
     */
    public static final String ATTR_NAME = "name";
    /**
     * <code>onChangeMessage</code> attribute
     */
    public static final String ATTR_ON_CHANGE_MESSAGE = "onChangeMessage";
    /**
     * <code>showDisplay</code> attribute
     */
    public static final String ATTR_SHOW_DISPLAY = "showDisplay";
    /**
     * <code>showDisplay</code> attribute value <code>all</code>
     */
    public static final String ATTR_SHOW_DISPLAY_VALUE_ALL = "all";
    /**
     * <code>showDisplay</code> attribute value <code>install</code>
     */
    public static final String ATTR_SHOW_DISPLAY_VALUE_INSTALL = "install";
    /**
     * <code>showDisplay</code> attribute value <code>runtime</code>
     */
    public static final String ATTR_SHOW_DISPLAY_VALUE_RUNTIME = "runtime";
    /**
     * <code>type</code> attribute
     */
    public static final String ATTR_TYPE = "type";
    /**
     * <code>value</code> attribute
     */
    public static final String ATTR_VALUE = "value";
    /**
     * <code>config-type-comp-app</code> Configuration type for component application
     * configuration
     */
    public static final String CONFIG_TYPE_COMP_APP = "config-type-comp-app";
    /**
     * <code>config-type-comp-install</code> Configuration type for component installation 
     * configuration
     */
    public static final String CONFIG_TYPE_COMP_INSTALL = "config-type-comp-install";
    /**
     * <code>config-type-comp-runtime</code> Configuration type for application configuration
     */
    public static final String CONFIG_TYPE_COMP_RUNTIME = "config-type-comp-runtime";
    /**
     * <code>enumeration</code> Constraint facet attribute value
     */
    public static final String CONSTRAINT_FACET_ATTR_ENUMERATION = "enumeration";
    /**
     * <code>maxInclusive</code> Constraint facet attribute value
     */
    public static final String CONSTRAINT_FACET_ATTR_MAX_INCLUSIVE = "maxInclusive";
    /**
     * <code>minInclusive</code> Constraint facet attribute value
     */
    public static final String CONSTRAINT_FACET_ATTR_MIN_INCLUSIVE = "minInclusive";
    /**
     * <code>Configuration</code> element
     */
    public static final String ELT_CFG = "Configuration";
    /**
     * <code>ApplicationConfiguration</code> element
     */
    public static final String ELT_APP_CFG = "ApplicationConfiguration";
    /**
     * <code>ApplicationVariable</code> element
     */
    public static final String ELT_APP_VAR = "ApplicationVariable";
    /**
     * <code>Constraint</code> element
     */
    public static final String ELT_CONSTRAINT = "Constraint";
    /**
     * <code>Property</code> element
     */
    public static final String ELT_PROP = "Property";
    /**
     * <code>PropertyGroup</code> element
     */
    public static final String ELT_PROP_GROUP = "PropertyGroup";
    /**
     * Configuration extension namespace URI <code>http://www.sun.com/jbi/Configuration/V1.0</code>
     */
    public static final String NS_CFG = "http://www.sun.com/jbi/Configuration/V1.0";
    /**
     * Namespace prefix separator
     */
    public static final String NS_PREFIX_SEP = ":";
    /**
     * <code>xsd:boolean</code> type
     */
    public static final String XSD_TYPE_BOOLEAN = "xsd:boolean";
    /**
     * <code>xsd:int</code> type
     */
    public static final String XSD_TYPE_INT = "xsd:int";
    /**
     * <code>xsd:positiveInteger</code> type
     */
    public static final String XSD_TYPE_POSITIVE_INTEGER = "xsd:positiveInteger";
    /**
     * <code>xsd:string</code> type
     */
    public static final String XSD_TYPE_STRING = "xsd:string";

    /**
     * prevents instantiation
     */
    private ESBConstants()
    {
    }
}
