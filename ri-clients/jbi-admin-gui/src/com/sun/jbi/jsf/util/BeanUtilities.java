/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  BeanUtilities.java
 *
 */
package com.sun.jbi.jsf.util;

import com.sun.jbi.jsf.bean.AlertBean;
import com.sun.jbi.jsf.bean.ArchiveBean;
import com.sun.jbi.jsf.bean.CompConfigBean;
import com.sun.jbi.jsf.bean.DeletionBean;
import com.sun.jbi.jsf.bean.InstallationBean;
import com.sun.jbi.jsf.bean.JBIConfigBean;
import com.sun.jbi.jsf.bean.ListBean;
import com.sun.jbi.jsf.bean.LoggingBean;
import com.sun.jbi.jsf.bean.OperationBean;
import com.sun.jbi.jsf.bean.RuntimeConfigurationBean;
import com.sun.jbi.jsf.bean.RuntimeStatsBean;
import com.sun.jbi.jsf.bean.ServiceUnitBean;
import com.sun.jbi.jsf.bean.ShowBean;
import com.sun.jbi.jsf.bean.TargetBean;
import com.sun.jbi.jsf.bean.UploadCopyRadioBean;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIRemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;

/**
 * This class is used to provide utilities function for JBI related tasks
 *
 * @author   Sun Microsystems Inc.
 */

public final class BeanUtilities
{

    /**
     * Gets the AlertBean attribute of the BeanUtilities class
     *
     * @return   The AlertBean value
     */
    public static AlertBean getAlertBean()
    {
        initBeans();
        AlertBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{AlertBean}", AlertBean.class);
        result = (AlertBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the DeletionBean attribute of the BeanUtilities class
     *
     * @return   The DeletionBean value
     */
    public static DeletionBean getDeletionBean()
    {
        DeletionBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{DeletionBean}", DeletionBean.class);
        result = (DeletionBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the InstallationBean attribute of the BeanUtilities class
     *
     * @return   The InstallationBean value
     */
    public static InstallationBean getInstallationBean()
    {
        InstallationBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{InstallationBean}", InstallationBean.class);
        result = (InstallationBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the JBIConfigBean attribute of the BeanUtilities class
     *
     * @return   The JBIConfigBean value
     */
    public static JBIConfigBean getJBIConfigBean()
    {
        JBIConfigBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{JBIConfigBean}", JBIConfigBean.class);
        result = (JBIConfigBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the OperationBean attribute of the BeanUtilities class
     *
     * @return   The OperationBean value
     */
    public static OperationBean getOperationBean()
    {
        OperationBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{OperationBean}", OperationBean.class);
        result = (OperationBean) ve.getValue(elCtx);
        return result;
    }

    /*
     *  @return static copy of UploadCopyRadioBean Bean's instance
     */
    /**
     * Gets the UploadCopyRadioBean attribute of the BeanUtilities class
     *
     * @return   The UploadCopyRadioBean value
     */
    public static UploadCopyRadioBean getUploadCopyRadioBean()
    {
        UploadCopyRadioBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{UploadCopyRadioBean}", UploadCopyRadioBean.class);
        result = (UploadCopyRadioBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the ArchiveBean attribute of the BeanUtilities class
     *
     * @return   The ArchiveBean value
     */
    public static ArchiveBean getArchiveBean()
    {
        ArchiveBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{ArchiveBean}", ArchiveBean.class);
        result = (ArchiveBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the LoggingBean attribute of the BeanUtilities class
     *
     * @return   The LoggingBean value
     */
    public static LoggingBean getLoggingBean()
    {
        LoggingBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve = ef.createValueExpression(elCtx, "#{LoggingBean}", LoggingBean.class);
        result = (LoggingBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the RuntimeConfigurationBean attribute of the BeanUtilities class
     *
     * @return   The RuntimeConfigurationBean value
     */
    public static RuntimeConfigurationBean getRuntimeConfigurationBean()
    {
        RuntimeConfigurationBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{RuntimeConfigurationBean}", RuntimeConfigurationBean.class);
        result = (RuntimeConfigurationBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the RuntimeStatsBean attribute of the BeanUtilities class
     *
     * @return   The RuntimeStatsBean value
     */
    public static RuntimeStatsBean getRuntimeStatsBean()
    {
        RuntimeStatsBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{RuntimeStatsBean}", RuntimeStatsBean.class);
        result = (RuntimeStatsBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the ListBean attribute of the BeanUtilities class
     *
     * @return   The ListBean value
     */
    public static ListBean getListBean()
    {
        initBeans();
        ListBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{ListBean}", ListBean.class);
        result = (ListBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the ServiceUnitBean attribute of the BeanUtilities class
     *
     * @return   The ServiceUnitBean value
     */
    public static ServiceUnitBean getServiceUnitBean()
    {
        ServiceUnitBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{ServiceUnitBean}", ServiceUnitBean.class);
        result = (ServiceUnitBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the ShowBean attribute of the BeanUtilities class
     *
     * @return   The ShowBean value
     */
    public static ShowBean getShowBean()
    {
        ShowBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{ShowBean}", ShowBean.class);
        result = (ShowBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the TargetBean attribute of the BeanUtilities class
     *
     * @return   The TargetBean value
     */
    public static TargetBean getTargetBean()
    {
        TargetBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{TargetBean}", TargetBean.class);
        result = (TargetBean) ve.getValue(elCtx);
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("BeanUtitlies.getTargetBean() result=" + result);
        }
        
        return result;
    }

    /**
     * Gets the Client attribute of the BeanUtilities class
     *
     * @return   The Client value
     */
    public static JBIAdminCommands getClient()
    {
        JBIAdminCommands result = null;
        if (null == sClient)
            {
                sClient = (new ConnectionUtilities()).getClient();
            }
        result = sClient;
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("BeanUtitlies.getClient() result=" + result);
        }
        
        return result;
    }


    /**
     * Gets the ComponentType attribute of the BeanUtilities class
     *
     * @param aComponentName  Description of Parameter
     * @param aTargetName     Description of Parameter
     * @return                The ComponentType value
     */
    public static String getComponentType(String aComponentName, String aTargetName)
    {
        String componentType = "";
        String result = "";
        try
            {
                JBIAdminCommands mJac = BeanUtilities.getClient();
                componentType = JBIConstants.JBI_BINDING_COMPONENT_TYPE;
                result = mJac.showBindingComponent(aComponentName, "", "", "", aTargetName);
                List list = JBIComponentInfo.readFromXmlText(result);
                if (list.size() == 0)
                    {
                        componentType = JBIConstants.JBI_SERVICE_ENGINE_TYPE;
                        result = mJac.showServiceEngine(aComponentName, "", "", "", aTargetName);
                        list = JBIComponentInfo.readFromXmlText(result);
                        if (list.size() == 0)
                            {
                                componentType = "";
                            }
                    }
            }
        catch (Exception e)
            {
            }
        return componentType;
    }


    /**
     * Gets the StringPropertyUsingExpression attribute of the BeanUtilities
     * class
     *
     * @param anExpression  Description of Parameter
     * @return              The StringPropertyUsingExpression value
     */
    public static String getStringPropertyUsingExpression(String anExpression)
    {
        String result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve = ef.createValueExpression(elCtx, anExpression, String.class);
        result = (String) ve.getValue(elCtx);
        return result;
    }

    /**
     * Gets the CompConfigBean attribute of the BeanUtilities class
     *
     * @return   The CompConfigBean value
     */
    public static CompConfigBean getCompConfigBean()
    {
        CompConfigBean result = null;
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve =
            ef.createValueExpression(elCtx, "#{CompConfigBean}",
                                     CompConfigBean.class);
        result = (CompConfigBean) ve.getValue(elCtx);
        return result;
    }

    /**
     * Sets the StringPropertyUsingExpression attribute of the BeanUtilities
     * class
     *
     * @param aStringValue  The new StringPropertyUsingExpression value
     * @param anExpression  The new StringPropertyUsingExpression value
     */
    public static void setStringPropertyUsingExpression(String aStringValue, String anExpression)
    {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        ValueExpression ve = ef.createValueExpression(elCtx, anExpression, String.class);
        ve.setValue(elCtx, aStringValue);
    }

    /**
     * Description of the Method
     */
    public static void initBeans()
    {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Object alreadyRegisteredBean =
            facesContext.getExternalContext().getSessionMap().get(BEAN_NAME);

        // check for bean already in session
        if (null != alreadyRegisteredBean)
            {
                // nothing to do
            }
        // create bean if needed (e.g. after a console logout)
        else
            {
                // need to init (re-init) bean(s) (e.g. for a new session)
                try
                    {
                        Class beanClass = Class.forName(CLASS_NAME);
                        Object bean = beanClass.newInstance();
                        // this inits all JBI session beans
                        facesContext.getExternalContext().getSessionMap().put(BEAN_NAME, bean);
                        
                        if (sLog.isLoggable(Level.FINER)){
                            sLog.finer("BeanUtilities.initBeans() BEAN_NAME=" + BEAN_NAME +
                                  ", beanClass=" + beanClass +
                                  ", bean=" + bean);
                        }
                    }
                catch (Exception ex)
                    {
                	    if (sLog.isLoggable(Level.FINE)){
                        sLog.log(Level.FINE,
                                 "BeanUtilities.initBeans() caught Exception",
                                 ex);
                	    }
                    }
            }
    }


    /**
     * Checks a component state against a filter state (developer profile only)
     * Filter "all" returns true for every component regardless of state
     * Filter "started" returns true for a started component
     * Filter "stopped" returns true for a stopped component
     * Filter "shutdown" returns true for a shut down component
     *
     * @param aComponentState the actual state, one of: started, stopped,
     * shut down (or possibly unknown)
     * @param aFilterState the desired state, one of: all, started, stopped,
     * or shut down
     * @param optionList the possible states
     * @return true if the component is to be included for the specified filter state.
     */
    public static boolean checkFilterOptions(String aComponentState,
                                             String aFilterState,
                                             ArrayList optionList)
    {
        boolean flag = true;
        if (null != aFilterState)
            {
                if (SharedConstants.DROP_DOWN_STATE_SHOW_ALL.equals(aFilterState))
                    {
                        //no-op --include all states
                    }
                else
                    {
                        //filter state is not all

                        flag = false;
                        if (null != optionList)
                            {
                                int index = optionList.indexOf(aFilterState);
                                if (index != -1)
                                    {
                                        flag = 
                                            containsIgnoreCase(aComponentState, 
                                                               (String) optionList
                                                               .get(index));
                                    }
                            }
                    }
            }
        return flag;
    }

    /**
     * For a given component state, determines if it should be included
     * for the specified filter state.
     * filter "all" always returns true
     * "no targets" returns true only for a component with no targets
     * "enabled" returns true only for a component with targets that is started
     * "disabled" returns true only for a component with targets that is not started
     *
     * @param aComponentState the actual state of a component
     * @param aFilterState the desired state for a filtered view
     * @param optionList the possible states
     * @param aTargetsList possibly empty list of targets
     * @return true if this component is to be included in the filtered view
     */
    public static boolean checkFilterOptions(String aComponentState,
                                             String aFilterState,
                                             ArrayList optionList,
                                             List aTargetsList)
    {
        boolean result = true;
        if (null != aFilterState)
            {
                // If the desired filter state is "all"
                // indicates that the component is to be included
                if (SharedConstants.DROP_DOWN_STATE_SHOW_ALL.equals(aFilterState))
                    {
                        // default is to include
                    }
                // If the desired filter state is not "all"
                // checks if the filtering matches the actual targets and state
                else
                    {
                        // If the desired filter state is "no targets"
                        // includes the component only if it has no targets
                        if (SharedConstants
                            .DROP_DOWN_STATE_NO_TARGETS
                            .equals(aFilterState))
                            {
                                // If this component has no targets.
                                // indicates that the component is to be included
                                if ((null == aTargetsList)
                                    ||(0 == aTargetsList.size()))
                                    {
                                        // default is to include
                                    }
                                // If this component has targets,
                                // indicates that the component is to be skipped
                                else
                                    {
                                        result = false;
                                    }
                            }
                        // If filtering by state (i.e. not by "no targets")
                        // and component has one or more targets,
                        // and the component state matches,
                        // includes te component
                            else
                                {
                                    if ((null != aTargetsList)
                                        &&(0 < aTargetsList.size()))
                                        {
                                            // If the component (with targets) 
                                            // state matches
                                            // indicates that the component is to be included
                                            if ((SharedConstants
                                                 .DROP_DOWN_STATE_ENABLED
                                                 .equals(aFilterState)
                                                 && (JBIConstants
                                                     .JBI_STATE_STARTED
                                                     .equals(aComponentState)))
                                                ||(SharedConstants
                                                   .DROP_DOWN_STATE_DISABLED
                                                   .equals(aFilterState)
                                                   && (!JBIConstants
                                                       .JBI_STATE_STARTED
                                                       .equals(aComponentState))))
                                                {
                                                    // default is to include
                                                }
                                            // If the component (with targets) 
                                            // state does not match
                                            // indicates that the component is to be skipped
                                            else
                                                {
                                                    result = false;
                                                }

                                        }
                                    // If filtering by state, 
                                    // but component has no targets,
                                    // indicates that this component is to be skipped
                                    else
                                        {
                                            result = false;
                                        }
                                }
                    }
            }

        return result;
    }



    /**
     * Will extract the JBIManagementMessage from the Remote exception. or
     * create a new management message from the exception
     *
     * @param ex  the exception to process
     * @return    Description of the Returned Value
     */
    public static JBIManagementMessage extractJBIManagementMessage(Exception ex)
    {
        JBIManagementMessage mgmtMsg = null;
        if (ex instanceof JBIRemoteException)
            {
                JBIRemoteException rEx = (JBIRemoteException) ex;
                mgmtMsg = rEx.extractJBIManagementMessage();
                
                if (sLog.isLoggable(Level.FINER)){
                    sLog.finer("BeanUtitlies.extractJBIManagementMessage()" +
                          ", rEx.getClass()=" + rEx.getClass() + ", mgmtMsg=" + mgmtMsg);
                }
            }

        if (null == mgmtMsg)
            {
                String exMessage = ex.getMessage();
                
                if (sLog.isLoggable(Level.FINER)){
                    sLog.finer("BeanUtitlies.extractJBIManagementMessage()" +
                          ", ex.getClass()=" + ex.getClass() + ", exMessage=" + exMessage);
                }
                
                if (null == exMessage)
                    {
                        exMessage = ex.toString();
                        
                        if (sLog.isLoggable(Level.FINER)){
                            sLog.finer("BeanUtitlies.extractJBIManagementMessage()" +
                                  ", ex.getClass()=" + ex.getClass() + ", exMessage=" + exMessage);
                        }

                    }
                mgmtMsg = JBIManagementMessage.createJBIManagementMessage(exMessage);
            }
        
        if (sLog.isLoggable(Level.FINER)){
            sLog.finer("BeanUtitlies.extractJBIManagementMessage()" +
                  ", ex.getClass()=" + ex.getClass() + ", mgmtMsg=" + mgmtMsg);
        }
        
        return mgmtMsg;
    }


    /**
     * @param jbiResultXml  - a common client result that may contain zero or
     *      more warnings
     * @return              Description of the Returned Value
     * @returns             JBIManagementMessage - null if no warnings,
     *      otherwise a message to be displayed
     */
    public static JBIManagementMessage extractJBIManagementMessage(String jbiResultXml)
    {
        JBIManagementMessage mgmtMsg =
            JBIManagementMessage.createJBIManagementMessage(jbiResultXml);

        if (sLog.isLoggable(Level.FINER)){
            sLog.finer("BeanUtilities.extractJBIManagementMessage(String jbiResultXml), mgmtMsg=" + mgmtMsg);
        }
        
        if (mgmtMsg == null)
            {
                // normal case: no-op (return null)
            }
        else
            {
                // log if the mgmt msg is a success --or-- warning or error
                if (!mgmtMsg.isFailedMsg())
                    {
                	   if (sLog.isLoggable(Level.FINER)){
                          sLog.finer("BeanUtilities.extractJBIManagementMessage()" +
                                  ", happy: isFailedMsg()=false");
                	   }
                    }
                else
                    {
                	    if (sLog.isLoggable(Level.FINER)){
                           sLog.finer("BeanUtilities.extractJBIManagementMessage()" +
                                  ", error: isFailedMsg()=true");
                	    }
                    }
            }
        return mgmtMsg;
    }

    /**
     * Will add the alert footer message to the provided alert message string
     *
     * @param aAlertMsg  The feature to be added to the AlertFooterMessage
     *      attribute
     * @return           the alert message with the appended footer message
     */
    public static String addAlertFooterMessage(String aAlertMsg)
    {
        String footerMsg = I18nUtilities.getResourceString("jbi.alert.footer.see.log.message");
        aAlertMsg += "<br>" + footerMsg;
        return aAlertMsg;
    }


    /**
     * Description of the Method
     *
     * @param str1  Description of Parameter
     * @param str2  Description of Parameter
     * @return      Description of the Returned Value
     */
    private static boolean containsIgnoreCase(String str1, String str2)
    {
        String str1Upper = str1.toUpperCase();
        String str2Upper = str2.toUpperCase();
        return (str1Upper.contains(str2Upper));
    }

    /**
     * prevents instantiation
     */
    private BeanUtilities()
    {
    }

    private static final String BEAN_NAME = "JBIConfigBean";
    private static final String CLASS_NAME = "com.sun.jbi.jsf.bean.JBIConfigBean";

    /**
     * Controls printing of diagnostic messages to the log
     */
    private static Logger sLog = JBILogger.getInstance();

    private static JBIAdminCommands sClient = null;

    static
    {
        initBeans();
    }
}

