#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00017.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

JBI_ANT="$JBI_ANT -Djbi.task.fail.on.error=false"
echo $JBI_ANT

#
# This tests the force option on the uninstall-component target
#

test_uninstall_force()
{
# Install the test component
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-engine4.jar  install-component

# Delete the bootstrap jar from the component's install root
rm $JV_JBI_DOMAIN_ROOT/jbi/components/ant_test_engine4/install_root/ant-test-engine4-boot.jar

# Attempt a normal uninstall. This will fail due to the missing jar.
$JBI_ANT -Djbi.component.name=ant_test_engine4 uninstall-component

# Now do a forced uninstall. This will succeed.
$JBI_ANT -Djbi.component.name=ant_test_engine4 -Djbi.force.task=true uninstall-component

# List engines. The list should be empty.
$JBI_ANT list-service-engines
}

run_test()
{
# ant -emacs -debug -f $JV_SVC_REGRESS/scripts/build-test-components.ant
build_test_artifacts
test_uninstall_force
}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test | tr -d '\r' | sed -e '/^$/d'

exit 0
