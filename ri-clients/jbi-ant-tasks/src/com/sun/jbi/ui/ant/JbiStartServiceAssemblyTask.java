/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiStartServiceAssemblyTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import org.apache.tools.ant.BuildException;


/** This class is an ant task for starting service assembly.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiStartServiceAssemblyTask extends JbiTargetTask
{
    /**
     * success msg key
     */
    private static final String PARTIAL_SUCCESS_STATUS_KEY = "jbi.ui.ant.start.sasm.partial.success";    
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.start.sasm.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.start.sasm.failed";
    
    
    /** Holds value of property ServiceAssemblyName. */
    private String mServiceAssemblyName;
    
    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public String getName()
    {
        return this.mServiceAssemblyName;
    }
    
    /**
     * Setter for property name.
     * @param name service assembly name
     */
    public void setName(String name)
    {
        this.mServiceAssemblyName = name;
    }
    /** executes the start service assembly task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        
        String saName = getValidServiceAssemblyName(getName());
        String target = getValidTarget();                
        String result = null;
        try
        {
            result = this.getJBIAdminCommands().startServiceAssembly(saName, target);
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
        
        processTaskResult(result);  
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    
    /**
     * return i18n key for the partial success
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return PARTIAL_SUCCESS_STATUS_KEY;
    }
    
}
