/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiListRuntimeConfigurationTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for displaying the runtime configuration or component 
 * configuration.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiListRuntimeConfigurationTask extends JbiTargetTask
{
    /** success msg key   */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.list.configuration.successful";
    /** failure msg key   */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.list.configuration.failed";
    /** logger success msg key   */

    /** INSTANCE ERROR PROPERTY */
    private static final String INSTANCE_ERROR_PROP = "com.sun.jbi.cluster.instance.error";
    
    /** Holds Param Nested elements */
    private List mParamList;

    /**
     * Logic for listing runtime configuration
     */

    private void executeListRuntimeConfiguration()
	throws BuildException
    {
        this.logDebug("Executing List Runtime Configuration ....");
        try
        {
            String	target		= getValidTarget();
            List        tmpParamList   = this.getParamList();
            List        paramList      = new ArrayList(tmpParamList);

            // Check the list, remove the entry with empty name value
            Iterator iter = tmpParamList.iterator();
            while (iter.hasNext())
            {
                Param param = (Param) iter.next();
                if (param != null)
                {
                    String paramName = param.getName();
                    if ((paramName != null) && (paramName.trim().compareTo("")==0))
                    {
                        paramList.remove(param);
                    }
                }
            }

            Properties returnProps = this.getJBIAdminCommands().getRuntimeConfiguration(target);
            printRuntimeConfiguration(paramList, returnProps);
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
    }
   
    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        this.logDebug("Executing List Configuration Task....");
        
        executeListRuntimeConfiguration();
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getEmptyQueryResultI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.empty" : 
            "jbi.ui.ant.print.jbi.comp.config.info.empty";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header" :
            "jbi.ui.ant.print.jbi.comp.config.info.header";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.header.separator";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultPageSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.separator" ;
    }
    
    /**
     * Printing runtime configuration
     * @param params the parameters to be printed
     */
    protected void printRuntimeConfiguration(List paramList, Properties returnProps)
    {
        this.logDebug("Printing Rimtime Configuration ....");
        boolean isRuntime = true;
        String header =
            getI18NBundle().getMessage( getQueryResultHeaderI18NKey(isRuntime), getValidTarget() );
        String headerSeparator =
            getI18NBundle().getMessage( getQueryResultHeaderSeparatorI18NKey(isRuntime) );
        String pageSeparator =
            getI18NBundle().getMessage( getQueryResultPageSeparatorI18NKey(isRuntime) );
        String emptyResult =
            getI18NBundle().getMessage( getEmptyQueryResultI18NKey(isRuntime) );
        
        StringWriter stringWriter = new StringWriter();
        PrintWriter msgWriter = new PrintWriter(stringWriter);
        
        msgWriter.println(headerSeparator);
        msgWriter.println(header);
        msgWriter.println(headerSeparator);
        
        if ( (returnProps == null) || (returnProps.size() <= 0) )
        {
            msgWriter.println(emptyResult);
            msgWriter.println(pageSeparator);
        }
        else if ( (paramList == null) || (paramList.size() <= 0) )
        {
            // sort the keys and display for deterministic output
            SortedSet keys = new TreeSet(returnProps.keySet());
            for(Object key : keys)
            {
                String name = (String)key;
                String value = returnProps.getProperty(name, "");
                String param = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.jbi.config.param",
                    name, value);
                msgWriter.println(param);
            }
            msgWriter.println(pageSeparator);
        }
        else
        {
            Iterator itr = paramList.iterator();
            while( itr.hasNext() )
            {
                String name = "" + ((Param) itr.next()).getName();
                String value = returnProps.getProperty(name);
                if (value != null)
                {
                    String msg = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.jbi.config.param",
                        name, value);
                    msgWriter.println(msg);
                }
                else
                {
                    String msg = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.jbi.config.param.not.retrieved",
                        name);
                    msgWriter.println(msg);
                }
            }    
            msgWriter.println(pageSeparator);
        }

        msgWriter.close();
        printMessage(stringWriter.getBuffer().toString());
        
    }
    
    /**
     * returns param element list
     * @return param List
     */
    protected List getParamList()
    {
        if ( this.mParamList == null )
        {
            this.mParamList = new ArrayList();
        }
        return this.mParamList;
    }

    /**
     * factory method for creating the nested element &lt;param>
     * @return Logger Object
     */
    public Param createParam()
    {
        Param param = new Param();
        this.getParamList().add(param);
        return param;
    }
}
