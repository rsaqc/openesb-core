/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiUpgradeComponentTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for updating service engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiUpgradeComponentTask extends JbiJmxTask
{
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.upgrade.component.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.upgrade.component.failed";
    /** Holds value of property installFile. */
    private File mInstallFile;
    
    /** Holds value of property componentName. */
    private String mComponentName = null;
    
    /** Getter for property componentName.
     * @return Value of property componentName.
     *
     */
    public String getName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param name component name.
     */
    public void setName(String name)
    {
        this.mComponentName = name;
    }
    
    /** Getter for property component installer zip file.
     * @return Value of property  installer zip file.
     *
     */
    public File getFile()
    {
        return this.mInstallFile;
    }
    
    /** Setter for property  installer zip file.
     * @param file New value of property  installer zip file.
     *
     */
    public void setFile(File file)
    {
        this.mInstallFile = file;
    }
    
    /**
     * validates the file
     */
    protected void validateInstallFileAttribute(File installFile) throws BuildException
    {
        if ( installFile == null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.upgrade.error.comp.archive.file.path.null");
        }
        
        if ( installFile.getPath().trim().length() <= 0 )
        {
            throwTaskBuildException(
                "jbi.ui.ant.upgrade.error.comp.archive.file.path.required");
        }
        
        if ( !installFile.exists() )
        {
            
            throwTaskBuildException(
                "jbi.ui.ant.upgrade.error.comp.archive.file.not.exist",
                installFile.getName());
        }
        
        if ( installFile.isDirectory() )
        {
            throwTaskBuildException(
                "jbi.ui.ant.upgrade.error.comp.archive.file.is.directory");
        }
        
    }
    /**
     * validate compName with valid target
     * @param compName name of the component in the repository
     */
    protected void validateNameAttribute(String compName)  throws BuildException
    {
        if ( compName == null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.upgrade.error.comp.name.required");
        }
        
        if ( compName.trim().length() == 0 )
        {
            throwTaskBuildException(
                "jbi.ui.ant.task.error.nullCompName");
        }
    }    
    
    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        String installFileAbsolutePath = null;
        
        String compName = getName();
        
        File installFile = getFile();
        
        validateNameAttribute(compName);        
        validateInstallFileAttribute(installFile);
        installFileAbsolutePath = installFile.getAbsolutePath();
        
        try
        {
            String result = null;
            result = this.getJBIAdminCommands().updateComponent(compName, installFileAbsolutePath);
            // Wait for Marc's change
	    // result = this.getJBIAdminCommands().upgradeComponent(compName, installFileAbsolutePath);
            printTaskSuccess(result);
            
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
        
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    
    
}
