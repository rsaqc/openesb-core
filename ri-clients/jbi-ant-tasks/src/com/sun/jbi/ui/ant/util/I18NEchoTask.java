/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)I18NEchoTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

/**
 * Writes a i18n message to the Ant logging facilities.
 * @author chikkala
 */
public class I18NEchoTask extends org.apache.tools.ant.taskdefs.Echo
{
    private static final String DEFAULT_BUNDLE = "com.sun.jbi.ui.ant.util.Bundle";
    
    /**
     * Holds mValue of property mBundle.
     */
    private String mBundle;
    
    /**
     * Getter for property mBundle.
     *
     * @return Value of property mBundle.
     */
    public String getBundle()
    {
        return ( this.mBundle == null ) ? DEFAULT_BUNDLE : this.mBundle;
    }
    
    /**
     * Setter for property mBundle.
     *
     *
     * @param bundle New value of property mBundle.
     */
    public void setBundle(String bundle)
    {
        this.mBundle = bundle;
    }
    
    /**
     * Holds mValue of property mKey.
     */
    private String mKey;
    
    /**
     * Getter for property mKey.
     *
     * @return Value of property mKey.
     */
    public String getKey()
    {
        return this.mKey;
    }
    
    /**
     * Setter for property mKey.
     *
     *
     * @param key New value of property mKey.
     */
    public void setKey(String key)
    {
        this.mKey = key;
    }
    
    /** Holds Args Nested elements */
    private List mArgList;
    
    public Argument createArg()
    {
        if ( this.mArgList == null )
        {
            this.mArgList = new ArrayList();
        }
        Argument arg = new Argument();
        this.mArgList.add(arg);
        return arg;
    }
    
    protected String[] getArguments()
    {
        
        if ( this.mArgList == null )
        {
            this.mArgList = new ArrayList();
        }
        
        List args = new ArrayList();
        for ( int i=0; i < this.mArgList.size(); ++i )
        {
            args.add( ((Argument)this.mArgList.get(i)).getValue());
        }
        return (String[]) args.toArray(new String[args.size()]);
    }
    
    /**
     * Does the work.
     *
     * @exception BuildException if something goes wrong with the build
     */
    public void execute() throws BuildException
    {
        String i18nKey = this.getKey();
        String bundleBase = this.getBundle();
        String msg = null;
        if ( i18nKey != null )
        {
            msg = loadMessageFromBundle(bundleBase, i18nKey, getArguments());
        }
        
        if ( msg != null )
        {
            this.setMessage(msg);
        }
        // do actual echo
        super.execute();
    }
    
    private String loadMessageFromBundle(String bundleBase, String key, String[] args) throws BuildException
    {
        String i18nMessage = loadBundle(bundleBase).getString(key);
        MessageFormat mf = new MessageFormat(i18nMessage);
        String formattedI18NMsg = mf.format(args);
        return formattedI18NMsg;
    }
    
    private ResourceBundle loadBundle(String bundleBase) throws BuildException
    {
        ResourceBundle resBundle = null;
        
        try
        {
//            log("I18NEchoTask loading Bundle Base " + bundleBase, Project.MSG_DEBUG);
            resBundle = ResourceBundle.getBundle(bundleBase);
        }
        catch ( MissingResourceException ex )
        {
            log("I18NEchoTask MissingResourceException loading  " 
                + bundleBase + " Message : " + ex.getMessage(), Project.MSG_DEBUG);
            log("Trying to load Locale independent default Bundle ", Project.MSG_DEBUG);
            
            // Try with locale independent default Bundle ( e.g. bundleBase.properties )
            resBundle = ResourceBundle.getBundle(bundleBase, new Locale(""));
        }
        if ( resBundle != null ) {
            log("I18NEchoTask got resource bundle with Locale " + resBundle.getLocale(), Project.MSG_DEBUG);
        } else {
            log("I18NEchoTask got null resource bundle for bundel base " + bundleBase, Project.MSG_DEBUG);
        }
        return resBundle;
    }
    
    
    
    public static class Argument
    {
        /**
         * Holds mValue of property mValue.
         */
        private String mValue = "";
        
        /**
         * Getter for property mValue.
         *
         * @return Value of property mValue.
         */
        public String getValue()
        {
            return this.mValue;
        }
        
        /**
         * Setter for property mValue.
         *
         * @param value New value of property mValue.
         */
        public void setValue(String value)
        {
            if ( value == null )
            {
                value = "";
            }
            this.mValue = value;
        }
        
        public String toString()
        {
            return getValue();
        }
    }
    
}
