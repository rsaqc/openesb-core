/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiListApplicationConfigurationsTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for displaying the runtime configuration or component 
 * configuration.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiListApplicationConfigurationsTask extends JbiTargetTask
{
    /** application configuration success msg key   */
    private static final String APPCONFIG_SUCCESS_STATUS_KEY = "jbi.ui.ant.list.appconfig.successful";
    /** application configuration failure msg key   */
    private static final String APPCONFIG_FAILED_STATUS_KEY = "jbi.ui.ant.list.appconfig.failed";

    /** INSTANCE ERROR PROPERTY */
    private static final String INSTANCE_ERROR_PROP = "com.sun.jbi.cluster.instance.error";
    
    /** Configuration Name Key */
    private static final String APP_CONFIG_NAME_KEY = "configurationName";
    
    /** Holds value of property componentName.  */
    private String mComponentName = null;
   
    /** Holds Param Nested elements */
    private List mAppConfigList;

    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    /**
     * Central logic for listing component configuration, component logger,
     * application variables and application configuration
     * @param componentName component name
     */
    private void executeListApplicationConfigurations(String componentName)
	throws BuildException
    {
        try
        {
	    String	target		= getValidTarget();
            List	tmpAppConfigList	= this.getAppConfigList();
            List        appConfigList = new ArrayList(tmpAppConfigList);

            // Check the list, remove the entry with empty name value
            Iterator iter = tmpAppConfigList.iterator();
            while (iter.hasNext())
            {
                Param appConfig = (Param) iter.next();
                if (appConfig != null)
                {
                    String appConfigName = appConfig.getName();
                    if ((appConfigName != null) && (appConfigName.trim().compareTo("")==0))
                    {
                        appConfigList.remove(appConfig);
                    }
                }
            }

	    // Go throught the appconfig nested elements
            if ((componentName == null) || (componentName.compareTo("") == 0))
            {
                String errMsg = createFailedFormattedJbiAdminResult(
                                            "jbi.ui.ant.task.error.nullCompName",
                                            null);
                throw new BuildException(errMsg);
            }

            this.logDebug("Executing List component application configurations ....");

            if (appConfigList.size() == 0)
            {
                this.logDebug("Executing List all component application configurations ....");
                Map<String,Properties> resultMap =
                     this.getJBIAdminCommands().getApplicationConfigurations(componentName, target);
                if (resultMap == null)
                {
                    String errMsg = createFailedFormattedJbiAdminResult(
                                            "jbi.ui.ant.list.no.appconfig.data.retrieved",
                                            new String [] { componentName } );
                    throw new BuildException(errMsg);
                }
                printApplicationConfigurations(componentName, resultMap, target);
            }
            else
            {
                Iterator it = appConfigList.iterator();
                TreeMap<String,Properties> resultMap = new TreeMap<String,Properties>();
   
                while (it.hasNext())
                {
                    Param param = (Param) it.next();
                    String appConfigName = param.getName();

                    if ((appConfigName != null) && (appConfigName.compareTo("") != 0))
                    {
                        this.logDebug("Executing List component application configuration named "
                            + appConfigName + " for component " + componentName);
    
                        Properties resultProp =
                            this.getJBIAdminCommands().getApplicationConfiguration(componentName,
                                    target,
                                    appConfigName);
                        if (resultProp == null)
                        {
                            String errMsg = createFailedFormattedJbiAdminResult(
                                            "jbi.ui.ant.list.appconfig.not.exist",
                                            new String [] { appConfigName } );
                            throw new BuildException(errMsg);
                        }

                        resultMap.put(appConfigName, resultProp);
                    }
                }

                printApplicationConfigurations(componentName, resultMap, target);
            }
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }                
    }

    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        this.logDebug("Executing List Configuration Task....");
        String compName = getComponentName();
        
        executeListApplicationConfigurations(compName);
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
	return APPCONFIG_FAILED_STATUS_KEY;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
	return APPCONFIG_SUCCESS_STATUS_KEY;
    }
    
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getEmptyQueryResultI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.empty" : 
            "jbi.ui.ant.print.jbi.comp.config.info.empty";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header" :
            "jbi.ui.ant.print.jbi.comp.config.info.header";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.header.separator";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultPageSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.separator" ;
    }
    
    protected void printApplicationConfigurations(String compName,
						Map<String, Properties> appConfigList,
						String target)
    {
        this.logDebug("Printing Application Configuration .... for component=" +
						compName +
						" on target=" +
						target);
        String header =
            getI18NBundle().getMessage(  "jbi.ui.ant.print.jbi.comp.appconfig.info.header",
                                                new String[] { compName, target } );
        String headerSeparator =
            getI18NBundle().getMessage( "jbi.ui.ant.print.jbi.comp.appconfig.info.header.separator" );
        String pageSeparator =
            getI18NBundle().getMessage( "jbi.ui.ant.print.jbi.comp.appconfig.info.separator" );
        String emptyResult =
            getI18NBundle().getMessage( "jbi.ui.ant.list.no.appconfig.data.retrieved",
                                                new String[] { compName }  );

        StringWriter stringWriter = new StringWriter();
        PrintWriter msgWriter = new PrintWriter(stringWriter);

        msgWriter.println(headerSeparator);
        msgWriter.println(header);
        msgWriter.println(headerSeparator);

        if (appConfigList.size() == 0)
        {
            msgWriter.println(emptyResult);
        }
        else
        {
            Iterator it = appConfigList.keySet().iterator();
            while (it.hasNext())
            {
                String appConfigName = (String) it.next();
                Properties appConfigParamList = appConfigList.get(appConfigName);
                String appConfigHeader = getI18NBundle().getMessage(
                                        "jbi.ui.ant.print.jbi.comp.appconfig.info.name",
                                        appConfigName, target);
                msgWriter.println(appConfigHeader);
	        printApplicationConfigurationRecursively(appConfigParamList, msgWriter, "", -1);
            }
	}

        msgWriter.close();
        printMessage(stringWriter.getBuffer().toString());
    }

    protected void printApplicationConfigurationRecursively(Properties appConfigParamList,
					PrintWriter msgWriter, String theTab, int passInArrayIndex)
    {
	String localTab = "\t" + theTab;

        SortedSet keys = new TreeSet(appConfigParamList.keySet());
	String arrayKey = null;
        boolean inArray = false;

	for(Object key : keys)
        {
            String name = (String)key;
	    String valueString =  appConfigParamList.getProperty(name);
	    int dotIndex = -1;

	    if ((name != null) && (name.indexOf('.') > 0))
	    {
		dotIndex = name.indexOf('.');
		String tmpArrayKey = null;
		int theIndex = -1;
		tmpArrayKey =  name.substring(0, dotIndex);
		String arrayIndex = name.substring(dotIndex + 1);

		try
		{
		    theIndex = Integer.parseInt(arrayIndex);
		    if ((tmpArrayKey != null) &&
				(tmpArrayKey.compareTo(arrayKey+"") != 0)) 
		    {
		        msgWriter.println(localTab + tmpArrayKey + ":");
		    }

                    if (valueString.indexOf(",") <= 0)
                    {
                        msgWriter.println(localTab + "\t[" + arrayIndex + "] " +  valueString);
                    }
                    else
                    {
                        printApplicationConfigurationRecursively(stringToProperties(valueString),
										 msgWriter, localTab, theIndex);
		    }
                    arrayKey = tmpArrayKey;
		}
		catch (NumberFormatException exp)
		{
		    if (valueString.indexOf(",") <= 0)
                    {
                        msgWriter.println(localTab + name + "=" + valueString);
                    }
                    else
                    {
                        printApplicationConfigurationRecursively(stringToProperties(valueString),
										msgWriter, localTab, -1);
                    }
		}
            }
            else if  (valueString.indexOf(",") > 0)
            {
                msgWriter.println(localTab + name + ":");
                printApplicationConfigurationRecursively(stringToProperties(valueString),
                                                                                msgWriter, localTab, -1);
            }
            else
            {
                if (passInArrayIndex >= 0)
                {
                    msgWriter.println(localTab +  "[" + passInArrayIndex + "] " + name + "=" + valueString);
                    passInArrayIndex = -1;
                    inArray = true;
                }
                else if (inArray == true)
                {
                    msgWriter.println(localTab +  "    " + name + "=" + valueString);
                }
                else
                {
                    msgWriter.println(localTab + name + "=" + valueString);
                }
            }
        }
    }

    private Properties stringToProperties(String theStr)
    {
        Properties prop = new Properties();

        StringTokenizer commaTk = new StringTokenizer(theStr, ",");
        while (commaTk.hasMoreTokens())
        {
            String entryStr = (String) commaTk.nextToken();
            StringTokenizer equalSignTk = new StringTokenizer(entryStr, "=");
            String key = equalSignTk.hasMoreTokens() ? equalSignTk.nextToken() : null;
            String value = equalSignTk.hasMoreTokens() ? equalSignTk.nextToken() : null;

            if (key != null)
            {
                prop.put(key, value+"");
            }
        }

        return prop;
    }

    /**
     * returns appconfig element list
     * @return appconfig List
     */
    protected List getAppConfigList()
    {
        if ( this.mAppConfigList == null )
        {
            this.mAppConfigList = new ArrayList();
        }
        return this.mAppConfigList;
    }

    /**
     * factory method for creating the nested element &lt;appconfig>
     * @return Logger Object
     */
    public Param createAppConfig()
    {
        Param param = new Param();
        this.getAppConfigList().add(param);
        return param;
    }
}
