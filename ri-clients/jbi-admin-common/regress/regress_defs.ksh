#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for regression tests.

#set this prop to the test domain. default is JBITest
#my_test_domain=domain1

#use global regress setup:
. $SRCROOT/antbld/regress/common_defs.ksh

#override the variable or define other common scripts here
#JBI_DOMAIN_NAME=JBITest
#JBI_ADMIN_HOST=localhost
#JBI_ADMIN_PORT=1962
#JBI_DOMAIN_ROOT=$AS8BASE/domains/$JBI_DOMAIN_NAME

# my module level test variables
#DEV_TEST=false

# MY_COMP_DIST=$SRCROOT/ui/bld/regress/dist
# JBIANT_COMM_OPT="-emacs -q -f $JBI_ADMIN_XML $USR_OPT -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$JBI_ADMIN_PORT"

export UI_REGRESS_DIST_DIR JV_SVC_TEST_CLASSES
JV_SVC_TEST_CLASSES=$JV_SVC_BLD/regress
if [ $OPENESB_BUILD -eq 1 ]; then
    UI_REGRESS_DIST_DIR=$JV_SVC_TEST_CLASSES/testdata
else
    UI_REGRESS_DIST_DIR=$JV_SVC_TEST_CLASSES/dist
fi

JBI_TARGET_NAME=server

JMX4ANT_JAR=`find $MAVEN_REPOSITORY/jmx4ant -name \*.jar -print`
export REGRESS_CLASSPATH
REGRESS_CLASSPATH="$JV_JBI_DOMAIN_ROOT/lib/jbi.jar\
${JBI_PS}$AS_INSTALL/lib/jmxremote_optional.jar\
${JBI_PS}$AS_INSTALL/lib/appserv-rt.jar\
${JBI_PS}$AS_INSTALL/lib/j2ee.jar\
${JBI_PS}$JMX4ANT_JAR\
${JBI_PS}$ANT_HOME/lib/canonxml.jar\
${JBI_PS}$JV_SVC_TEST_CLASSES\
"

# Calls ant with jbi.task.fail.on.error=false so that we can track negative
# test case output in the .out file
export JBI_ANT_NEG
JBI_ANT_NEG="$JBI_ANT -Djbi.task.fail.on.error=false -propertyfile $JV_JBI_DOMAIN_PROPS -lib $REGRESS_CLASSPATH"

clean_jbi_install() 
{
    rm $JBI_DOMAIN_ROOT/logs/server.log
    rm $JBI_DOMAIN_ROOT/logs/.jbi_admin*
    rm -rf $JBI_DOMAIN_ROOT/jbi/engines
    rm -rf $JBI_DOMAIN_ROOT/jbi/bindings
    rm -rf $JBI_DOMAIN_ROOT/jbi/sharedlibraries
    rm -rf $JBI_DOMAIN_ROOT/jbi/repository
    rm -rf $JBI_DOMAIN_ROOT/jbi/system/deployment
    rm -rf $JBI_DOMAIN_ROOT/jbi/Trash
    mkdir $JBI_DOMAIN_ROOT/jbi/repository
    mkdir $JBI_DOMAIN_ROOT/jbi/system/deployment
    return 0;
}

# only used in module level testing
start_appserver()
{
    clean_jbi_install
    asadmin start-domain -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --domaindir "$JV_JBI_DOMAIN_DIR" $JBI_DOMAIN_NAME
    sleep 10
    return 0
}

stop_appserver() {
    $AS8BASE/bin/asadmin stop-domain $JBI_DOMAIN_NAME
    return 0
}

local_test_setup() {
    if [ "$DEV_TEST" = "true" ]; then
        start_appserver
    fi
    return 0
}

local_test_cleanup() {
    if [ "$DEV_TEST" = "true" ]; then
        stop_appserver
    fi
    return 0
}

