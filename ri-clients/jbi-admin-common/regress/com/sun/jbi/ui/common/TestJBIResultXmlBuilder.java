/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJBIResultXmlBuilder.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;
import java.io.IOException;
import junit.framework.TestCase;


/**
 * Tests the temporary ui class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestJBIResultXmlBuilder
    extends TestCase
{
    
    /**
     * test case constructor
     * @param aTestName name of the text
     */
    public TestJBIResultXmlBuilder(String aTestName)
    {
        super(aTestName);
    }
    
    
// =============================  test methods ================================
    
    /**
     * returns test data dir
     * @return test data dir
     */
    private String getTestDataDir()
    {
        String srcroot = System.getProperty("junit.srcroot");
        String testDataDir = srcroot +
            "/shasta/ui/admin-client/regress/testdata/result/";
        return testDataDir;
    }
    
    /**
     * prints the formatted jbi results
     * @param resultFor main message
     * @param jbiResultXml xml text
     * @throws java.lang.Exception on error
     */
    private void printFormattedJbiResultXml(String resultFor, String jbiResultXml)  throws Exception
    {
        
//        System.out.println();
//        System.out.println(jbiResultXml);
//        System.out.println();
        
        JBIManagementMessage mgmtMsg = JBIManagementMessage.createJBIManagementMessage(jbiResultXml);
        
        String result = mgmtMsg.getMessage();
        
        System.out.println();
        System.out.println(resultFor + " [");
        System.out.println(result);
        System.out.println("]");
        System.out.println();
        System.out.println("----------------------------------------------------");
        
    }
    
    /**
     * testTest
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateMessageOnlyJbiResultXml()
    throws Exception
    {
        String jbiResultXml = JBIResultXmlBuilder.getInstance()
        .createJbiResultXml("UIANT_TASK", JBIResultXmlBuilder.SUCCESS_RESULT,
            JBIResultXmlBuilder.INFO_MSG_TYPE, "UIANT000", "This is a message for ant task1", null);
        
        printFormattedJbiResultXml("Esb Result from builder - esb message only", jbiResultXml);
    }
    
    /**
     * testTest
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateExceptionOnlyJbiResultXml()
    throws Exception
    {
        
        Exception ex1 = new Exception("First Exception");
        Exception ex2 = new Exception("Second Exception", ex1);
        Exception ex = new Exception("Final Exception", ex2);
        
        String jbiResultXml =
            JBIResultXmlBuilder.getInstance()
            .createJbiResultXml("UIANT_TASK", "UIANT002", ex);
        
        printFormattedJbiResultXml("Esb Result from builder - esb exception only message", jbiResultXml);
    }
    
    /**
     * testTest
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateMessageAndExceptionJbiResultXml()
    throws Exception
    {
        Exception ex1 = new Exception("First Exception");
        Exception ex2 = new Exception("Second Exception", ex1);
        Exception ex = new Exception("Final Exception", ex2);
        
        String jbiResultXml =
            JBIResultXmlBuilder.getInstance()
            .createJbiResultXml("UIANT_TASK", JBIResultXmlBuilder.FAILED_RESULT,
            JBIResultXmlBuilder.ERROR_MSG_TYPE,
            "UIANT002", "This is a task level message", null, ex);
        
        printFormattedJbiResultXml("Esb Result from builder - esb message and exception only message", jbiResultXml);
    }
    
    /**
     * testTest
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateJbiResultXmlFromI18NBundle()
    throws Exception
    {
        Exception ex1 = new Exception("First Connection Exception");
        Exception ex2 = new Exception("Second Connection Exception", ex1);
        Exception ex = new Exception("Final Connection Exception", ex2);
        
        String url = "http://host:8989/xjy";
//        IOException ioEx = new IOException("This is Connection IO Exception");
        
        IOException ioEx = new IOException("IO Error occurred connecting to jmx server");
        I18NBundle i18nBundle = new I18NBundle("com.sun.jbi.ui.client");
        
        JMXConnectionException jmxEx =  new JMXConnectionException(
            JBIResultXmlBuilder.createJbiResultXml(
            i18nBundle,"jbi.ui.jmx.open.io.error", new Object[] {url}, ioEx), null);
            
//        String result = JBIResultXmlBuilder.createFormattedJbiResult(
//            i18nBundle,"jbi.ui.jmx.open.io.error", new Object[] {url}, ex);
            
//            JMXConnectionException jmxEx = new JMXConnectionException(result, ex);
            // JBIRemoteException rEx = new JBIRemoteException(connEx);
            
//            JBIRemoteException rEx = new JBIRemoteException(jmxEx.getMessage(), jmxEx.getCause());
            JBIRemoteException rEx = new JBIRemoteException(jmxEx);
            System.out.println("JMX COnnection Exception Message : " + jmxEx.getMessage());
            
            System.out.println("JMX Connection results In JBIRemoteException [ ");
            String result = null;
            JBIManagementMessage mgmtMsg = rEx.extractJBIManagementMessage();
            if ( mgmtMsg != null )
            {
                result = mgmtMsg.getMessage();
            }
            else
            {
                result = rEx.getMessage();
            }
            System.out.println(result);
            
            System.out.println("]");
            
            System.out.println("JMX Connection results In JBIRemoteException.toString() [ ");
            System.out.println(rEx.toString());
            System.out.println("]");
    }
    
    
}
