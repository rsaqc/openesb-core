<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://java.sun.com/xml/ns/jbi/esb" xmlns:jbi="http://java.sun.com/xml/ns/jbi/management-message">

   <xsl:output method="xml"/>
   
   <!-- Look through each node for excluded items -->
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>
   
   <!-- ##################### EXCLUDED ITEMS ###################### -->
   <!-- set locale to "en" -->
   <xsl:template match="jbi:locale">
      <xsl:element name="{name()}">
         <xsl:text>en</xsl:text>
      </xsl:element>
   </xsl:template>

</xsl:stylesheet>
