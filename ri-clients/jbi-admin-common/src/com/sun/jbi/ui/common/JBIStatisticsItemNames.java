/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIStatisticsItemNames.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

/**
 * This class provides the JBI Statistics item names.
 * These names are used as keys in the CompositeData used
 * to represent the various statistics: Framework Statistics,
 * NMR Statistics, Component Statistics, Endpoint Statistics, 
 * SA Statistics.
 * 
 * @author Sun Microsystems, Inc.
 */
public final class JBIStatisticsItemNames {

    
//--------------------------BEGIN - Common Statistics Keys------------------------------
    
    /** 
     * key for instance name
     */
    public static String INSTANCE_NAME = "InstanceName";
    
   /**  
    * table index   
    */        
    public static String[] STATS_TABLE_INDEX = new String[]{ "InstanceName" };          
    
//--------------------------END - Common Statistics Keys----------------------------------
    

    
    
//--------------------------BEGIN - Framework Statistics Keys------------------------------
    
    /** 
     * framework startup time 
     */        
    public static String FRAMEWORK_STARTUP_TIME = "StartupTime (ms)";
    
    /** 
     * framework uptime 
     */
    public static String FRAMEWORK_UPTIME = "UpTime (ms)";
    
    /**
     * composite data name for framework statistics
     */
    public static String FRAMEWORK_STATISTICS_NAME = "FrameworkStatistics";
    
    /**
     * description for framework statistics 
     */
    public static String FRAMEWORK_STATISTICS_DESCRIPTION = "Framework statistics for target";
    
    /**
     * framework stats. table item name
     */ 
    public static String FRAMEWORK_STATISTICS_TABLE_ITEM_NAME = "FrameworkStatisticsItem";
    
    /**
     * framework. stats table item description
     */
    public static final String FRAMEWORK_STATISTICS_TABLE_ITEM_DESCRIPTION = "Framework statistics for instance";
    
    
//----------------------------END - Framework Statistics Keys-------------------------------
    

//---------------------------BEGIN - NMR Statistics Keys -------------------------------------
    /**
     * nmr active channels list
     */
    public static String NMR_STATS_ACTIVE_CHANNELS = "ListActiveChannels";
    
    /**
     * nmr active endpoints list
     */
    public static String NMR_STATS_ACTIVE_ENDPOINTS = "ListActiveEndpoints";
    
    /**
     * composite data name for nmr statistics
     */
    public static String NMR_STATISTICS_NAME = "NMRStatistics";
    
    /**
     * description for nmr statistics 
     */
    public static String NMR_STATISTICS_DESCRIPTION = "NMR statistics for a target";
    
    /**
     * nmr stats. table item name
     */ 
    public static String NMR_STATISTICS_TABLE_ITEM_NAME = "NMRStatisticsItem";
    
    /**
     * nmr. stats table item description
     */
    public static final String NMR_STATISTICS_TABLE_ITEM_DESCRIPTION = "NMR statistics for an instance";
     
//--------------------------END - NMR Statistics Keys--------------------------------------------------------------
    
    
//---------------------------BEGIN - Deployment Service Statistics Keys-------------------------------------------     
    
    /**
     * service unit name
     */
    public static String SERVICE_UNIT_NAME = "ServiceUnitName";
    
    /**
     * service unit startup time
     */
    public static String SERVICE_UNIT_STARTUP_TIME = "ServiceUnitStartupTime Avg (ms)";
    
    /**
     * service unit stop time
     */
    public static String SERVICE_UNIT_STOP_TIME = "ServiceUnitStopTime Avg (ms)";
    
    /**
     * service unit shutdown time
     */
    public static String SERVICE_UNIT_SHUTDOWN_TIME = "ServiceUnitShutdownTime Avg (ms)";
    
    /**
     * SA Name
     */
    public static String SERVICE_ASSEMBLY_NAME = "ServiceAssemblyName";
    
    /**
     * SA Uptime
     */
    public static String SERVICE_ASSEMBLY_UPTIME = "UpTime (ms)";
    
    /**
     * last startup time
     */
    public static String SERVICE_ASSEMBLY_LAST_STARTUP_TIME = "LastStartupTime";
    
    /**
     * SA startup time
     */
    public static final String SERVICE_ASSEMBLY_STARTUP_TIME = "StartupTime Avg (ms)";
    
    /**
     * SA stop time
     */
    public static final String SERVICE_ASSEMBLY_STOP_TIME = "StopTime Avg (ms)";
    
    /**
     * SA Shutdown time
     */
    public static final String SERVICE_ASSEMBLY_SHUTDOWN_TIME = "ShutdownTime Avg (ms)";
    
    /**
     * SA list of SU statistics
     */
    public static final String SERVICE_ASSEMBLY_SU_STATISTICS = "ServiceUnitStatistics";    
    
    
    /**
     * composite data name for SA statistics
     */
    public static String SA_STATISTICS_NAME = "ServiceAssemblyStatistics";
    
    /**
     * description for SA statistics 
     */
    public static String SA_STATISTICS_DESCRIPTION = "SA statistics for a target";    
    
    /**
     * name for SA statistics table item
     */
    public static String SA_STATISTICS_ITEM_NAME = "ServiceAssemblyStatisticsItem";
    
    /**
     * description for SA statistics table item
     */
    public static String SA_STATISTICS_ITEM_DESCRIPTION = "SA statistics for an instance";    
     
    
//----------------------------END - Deployment Service Statistics Keys -------------------------------------------                          
    
    
//----------------------------BEGIN - Common keys for component/endpoint statistics ----------------------------------
    
    /**
     * number of received requests 
     */
    public static final String NUMBER_OF_RECEIVED_REQUESTS = "NumReceivedRequests";
            
    /**
     *  number of sent requests 
     */
    public static final String NUMBER_OF_SENT_REQUESTS = "NumSentRequests";
    
    /**
     *  number of received replies 
     */
    public static final String NUMBER_OF_RECEIVED_REPLIES = "NumReceivedReplies";
    
    /**
     *  number of sent replies 
     */
    public static final String NUMBER_OF_SENT_REPLIES = "NumSentReplies";
    
    /**
     *  number of received DONEs 
     */
    public static final String NUMBER_OF_RECEIVED_DONES = "NumReceivedDONEs";
    
    /**
     *  number of sent DONEs
     */
    public static final String NUMBER_OF_SENT_DONES = "NumSentDONEs";
    
    /**
     *  number of received faults
     */
    public static final String NUMBER_OF_RECEIVED_FAULTS = "NumReceivedFaults";
    
    /**
     * number of sent faults
     */
    public static final String NUMBER_OF_SENT_FAULTS = "NumSentFaults";
    
    /**
     *  number of received errors 
     */
    public static final String NUMBER_OF_RECEIVED_ERRORS = "NumReceivedErrors";
    
    
    /**
     *  number of sent requests 
     */
    public static final String NUMBER_OF_SENT_ERRORS = "NumSentErrors";
    
    /**
     *  number of completed requests 
     */
    public static final String NUMBER_OF_COMPLETED_EXCHANGES = "NumCompletedExchanges";
    
    /**
     *  number of active exchanges
     */
    public static final String NUMBER_OF_ACTIVE_EXCHANGES = "NumActiveExchanges";

    /**
     *  number of active exchanges max
     */
    public static final String NUMBER_OF_ACTIVE_EXCHANGES_MAX = "NumActiveExchangesMax";
    
    
    /**
     *  number of queued exchanges
     */
    public static final String NUMBER_OF_QUEUED_EXCHANGES = "NumQueuedExchanges";
    
    /**
     *  number of queued exchanges max
     */
    public static final String NUMBER_OF_QUEUED_EXCHANGES_MAX = "NumQueuedExchangesMax";
    
    /**
     *  number of error exchanges
     */
    public static final String NUMBER_OF_ERROR_EXCHANGES = "NumErrorExchanges";
    
    /**
     * message exchange response time
     */
    public static final String MESSAGE_EXCHANGE_RESPONSE_TIME = "MessageExchangeResponseTime Avg (ns)";
    
    /**
     * message exchange component time
     */
    public static final String MESSAGE_EXCHANGE_COMPONENT_TIME = "MessageExchangeComponentTime Avg (ns)";
    
    /**
     * message exchange delivery channel time
     */
    public static final String MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME = "MessageExchangeDeliveryTime Avg (ns)";
    
    /**
     * message exchange status time
     */
    public static final String MESSAGE_EXCHANGE_STATUS_TIME = "MessageExchangeStatusTime Avg (ns)";
                
   
    /**
     * message exchange nmr time
     */
    public static final String MESSAGE_EXCHANGE_NMR_TIME = "MessageExchangeNMRTime Avg (ns)";    

    
//----------------------------END  - Common keys for component/endpoint statistics----------------------------------
    
    
    
//----------------------------BEGIN - Component statistics keys ------------------------------------------------------
    /**
     * Uptime
     */
    public static final String COMPONENT_UPTIME = "ComponentUpTime (ms)";
    
    /**
     * activated endpoints
     */
    public static final String NUMBER_OF_ACTIVATED_ENDPOINTS = "NumActivatedEndpoints";
    
   /**
    * component stats
    */
    public static final String COMPONENT_EXTENSION_STATS = "ComponentExtensionStats";    
    
    /**
     * component statistics table item name
     */
    public static final String COMPONENT_STATISTICS_TABLE_ITEM_NAME = "ComponentStatisticsItem";
    
    /**
     * component statistics table item description
     */
    public static final String COMPONENT_STATISTICS_TABLE_ITEM_DESCRIPTION = "Component statistics in an instance";
    
        
//----------------------------END - Component statistics keys ------------------------------------------------------    
 
//----------------------------BEGIN - Common endpoint statistics keys ------------------------------------------------------        
    /**
     * component name
     */
    public static final String ENDPOINT_COMPONENT_NAME = "ComponentName";
    
    /**
     * OJC performance stats
     */
    public static final String OJC_PERFORMANCE_STATS = "PerformanceMeasurements";
    
//----------------------------END - Common endpoint statistics keys ------------------------------------------------------        
    
    
       
//----------------------------BEGIN - Provider specific statistics keys ------------------------------------------------------                    
    /**
     * activation time
     */
    public static final String PROVIDER_ENDPOINT_ACTIVATION_TIME = "ActivationTime";
    
    /**
     * Uptime
     */
    public static final String PROVIDER_ENDPOINT_UPTIME = "EndpointUpTime (ms)";    
    
    /**
     * prividing endpoint statistics data name
     */
    public static final String PROVIDER_STATS_NAME = "ProviderEndpointStats";
    
    /**
     * prividing endpoint statistics data description
     */
    public static final String PROVIDER_STATS_DESCRIPTION = "Providing endpoint statistics in an instance";    
    
//----------------------------END - Provider specific statistics keys ------------------------------------------------------                    
    

//----------------------------BEGIN - Consuming endpoint specific statistics keys ------------------------------------------------------                    
    /**
     * Status time
     */
    public static final String CONSUMING_ENDPOINT_STATUS_TIME = "MessageExchangeStatusTime Avg (ns)";
            
    /**
     * stats name
     */
    public static final String CONSUMING_ENDPOINT_STATS_NAME = "ConsumerEndpointStats";
            

    /**
     * stats description 
     */
    public static final String CONSUMING_ENDPOINT_STATS_DESCRIPTION = "Consumer Endpoint Statistics";
    
//----------------------------END - Consuming endpoint specific statistics keys ------------------------------------------------------                    
    
    
//----------------------------BEGIN - Endpoint list statistics specific statistics keys ------------------------------------------------------                    
    
    /**
     * endpoints list - composite data name
     */
    public static final String ENDPOINT_LIST_STATISTICS_NAME = "EndpointsList";
    
    
    /**
     * endpoints list - composite data description
     */
    public static final String ENDPOINT_LIST_STATISTICS_DESCRIPTION = "List of Endpoints";
    
    /**
     * endpoints list table name
     */
    public static final String ENDPOINT_LIST_STATISTICS_TABLE_NAME = "Endpoints Table";
    
    /**
     * endpoints list table description
     */
    public static final String ENDPOINT_LIST_STATISTICS_TABLE_DESCRIPTION = "Endpoints Table";
    
    /**
     * item name for endpoints list 
     */
    public static final String ENDPOINTS_LIST_ITEM_NAME = "Endpoints";
    
    
    
//----------------------------END  - Endpoint list statistics specific keys ------------------------------------------------------                        
    

}
