/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMXConnectionFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client;

import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.JMXConnectionException;
import com.sun.jbi.ui.common.I18NBundle;


import java.util.Properties;


/**
 * Abstract facoty implementation.
 *
 * @author Sun Microsystems, Inc.
 */
public abstract class JMXConnectionFactory
{
    /**
     * property name for class to load.
     */
    public static final String FACTORY_IMPL_CLASS_PROP =
        "com.sun.jbi.tools.jmx.connection.factory";
    
    /**
     * if no property set, try load this impl class.
     */
    public static final String FACTORY_IMPL_CLASS =
        "com.sun.jbi.ui.client.jmxremote.RemoteJMXConnectionFactory";
    /** i18n */
    private static I18NBundle sI18NBundle = null;
    
    /**
     * The full path/name of the property file.
     */
    private static String sFullPropertyFileName = null;
    /** gives the I18N bundle.
     *@return I18NBundle object
     */
    protected static I18NBundle getI18NBundle()
    {
        // lazzy initialize the JBI Client
        if ( sI18NBundle == null )
        {
            sI18NBundle = new I18NBundle("com.sun.jbi.ui.client");
        }
        return sI18NBundle;
    }
    /**
     *
     *
     * @param conprops
     *
     * @return
     */
    public abstract JMXConnection getConnection(Properties conprops)
    throws JMXConnectionException;
    
    /**
     * creates jmx connection.
     *
     * @param provprops
     * @param host host name
     * @param port port number
     * @param username username
     * @param password password
     *
     * @return jmx connection
     */
    public static Properties getConnectionProperties(
        Properties provprops,
        String host,
        String port,
        String username,
        String password)
    {
        Properties props =
            JMXConnectionProperties.getJMXConnectionPropertyMap(provprops,
            null, host, port, username, password);
        
        return props;
    }
    
    /**
     *
     *
     * @param factoryImplName
     *
     * @return
     */
    public static  JMXConnectionFactory newInstance(
        String factoryImplName) throws JMXConnectionException
    {
        if ((factoryImplName == null) || (factoryImplName.trim().equals("")))
        {
            return newInstance();
        }
        
        try
        {
            Class cl = Class.forName(factoryImplName);
            
            return (JMXConnectionFactory) cl.newInstance();
        }
        catch (Exception ex)
        {
            // pass null to the cause as you have already serialized the cause to jbi mgmt xml
            throw new JMXConnectionException(
                JBIResultXmlBuilder.createJbiResultXml(getI18NBundle(),
                "jbi.ui.client.connection.factory.error", new Object[] {factoryImplName}, ex), null);
        }
    }
    /**
     *
     *
     * @param factoryImplName
     *
     * @return
     */
    public static JMXConnectionFactory newInstance(
        Properties props) throws JMXConnectionException
    {
        String facImplname = null;
        
        if (props != null)
        {
            facImplname = (String)props.getProperty(FACTORY_IMPL_CLASS_PROP);
        }
        
        return newInstance(facImplname);
    }
    /**
     *
     *
     * @return
     */
    public static JMXConnectionFactory newInstance() throws JMXConnectionException
    {
        String factoryImplClass = JMXConnectionProperties.
            getInstance().getDefaultConnectionFactory();
        
        
        if (factoryImplClass == null)
        {
            factoryImplClass = FACTORY_IMPL_CLASS;
        }
        
        JMXConnectionFactory impl = null;
        
        try
        {
            impl =
                (JMXConnectionFactory) Class.forName(factoryImplClass)
                .newInstance();
        }
        catch (Exception ex)
        {
            // any exception, just load the default class?
            // pass null to the cause as you have already serialized the cause to jbi mgmt xml
            throw new JMXConnectionException(
                JBIResultXmlBuilder.createJbiResultXml(getI18NBundle(),
                "jbi.ui.client.connection.factory.error", new Object[] {factoryImplClass}, ex), null);
            
        }
        
        return impl;
    }
}
