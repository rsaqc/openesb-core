/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIAdminCommandsClientFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.Util;

/**
 * This class is a factory class to create a JBIAdminCommands object.
 * 
 * @author graj
 */
public class JBIAdminCommandsClientFactory {

        /** i18n */
        private static I18NBundle sI18NBundle = null;

        /** Creates a new instance of JBIAdminCommandsClientFactory */
        private JBIAdminCommandsClientFactory() {
        }

        /**
         * gives the I18N bundle
         * 
         * @return I18NBundle object
         */
        protected static I18NBundle getI18NBundle() {
                // lazzy initialize the JBI Client
                if (sI18NBundle == null) {
                        sI18NBundle = new I18NBundle("com.sun.jbi.ui.client");
                }
                return sI18NBundle;
        }

        /**
         * Creates a management message string and populates the exception
         * 
         * @param bundleKey
         * @param args
         *            of Strings
         * @param sourceException -
         *            the source exception to propagate
         * @return Exception object created with a valid XML Management Message
         */
        static Exception createManagementException(String bundleKey, String[] args,
                        Exception sourceException) {
                Exception exception = null;
                String xmlManagementMessage = JBIResultXmlBuilder.createJbiResultXml(
                                JBIAdminCommandsClientFactory.getI18NBundle(), bundleKey, args,
                                sourceException);
                exception = new Exception(xmlManagementMessage);
                return exception;
        }

        /**
         * Creates a new instance of JBIAdminCommands object
         * 
         * @param hostName
         * @param portNumber
         * @param userName
         * @param password
         * @param connectionType
         * @return JBIAdminCommands object
         * @throws JBIRemoteException
         */
        public static JBIAdminCommands getInstance(String hostName, int portNumber,
                        String userName, String password, ConnectionType connectionType)
                        throws JBIRemoteException {

                JBIAdminCommandsClientFactory factory = new JBIAdminCommandsClientFactory();
                JBIAdminCommands commands = null;
                // Create a remote connection
                MBeanServerConnection connection = null;

                try {
                        connection = factory.getMBeanServerConnection(hostName, portNumber,
                                        userName, password, connectionType);
                } catch (JBIRemoteException rEx) {
                        throw rEx;
                } catch (Exception e) {
                        String[] args = { hostName, (new Integer(portNumber)).toString(),
                                        userName, connectionType.getProtocol() };
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection.host.port.uname.password.protocol",
                                        args, e);
                        throw new JBIRemoteException(exception);
                }
                boolean isLocalHost = false;
                try {
                        isLocalHost = Util.isLocalHost(hostName);
                } catch (UnknownHostException e) {
                        String[] args = { hostName };
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection.unknown.host", args, null);
                        throw new JBIRemoteException(exception);
                }
                if (isLocalHost == true) {
                        commands = new JBIAdminCommandsClientImpl(connection, false);
                } else {
                        commands = new JBIAdminCommandsClientImpl(connection, true);
                }
                return commands;
        }

        /**
         * Creates a new instance of JBIAdminCommands object
         * 
         * @param url
         * @param userName
         * @param password
         * @param isRemoteConnection -
         *            true if remote, false if local
         * @return JBIAdminCommands object
         * @throws JBIRemoteException
         */
        public static JBIAdminCommands getInstance(String url, String userName,
                        String password, boolean isRemoteConnection)
                        throws JBIRemoteException {

                JBIAdminCommandsClientFactory factory = new JBIAdminCommandsClientFactory();
                JBIAdminCommands commands = null;

                MBeanServerConnection connection = null;

                try {
                        connection = factory.getMBeanServerConnection(url, userName,
                                        password);
                } catch (JBIRemoteException rEx) {
                        throw rEx;
                } catch (Exception e) {
                        String[] args = { url, userName };
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection.url.uname.password",
                                        args, e);
                        throw new JBIRemoteException(exception);
                }

                commands = new JBIAdminCommandsClientImpl(connection,
                                isRemoteConnection);
                return commands;
        }

        /**
         * Creates a new instance of JBIAdminCommands object First tries to
         * establish a HTTP connection. If that fails, tries to establish a HTTPS
         * connection, and if that fails tries to establish a JRMP Connection.
         * 
         * @param hostName
         * @param portNumber
         * @param userName
         * @param password
         * @return JBIAdminCommands object
         * @throws JBIRemoteException
         */
        public static JBIAdminCommands getInstance(String hostName, int portNumber,
                        String userName, String password) throws JBIRemoteException {

                JBIAdminCommandsClientFactory factory = new JBIAdminCommandsClientFactory();
                JBIAdminCommands commands = null;

                MBeanServerConnection connection = null;
                boolean result = false;
                ObjectName mbeanName = null;
                Exception exceptionArgument = null;

                // Try to obtain a HTTP connection
                try {
                        connection = factory.getMBeanServerConnection(hostName, portNumber,
                                        userName, password, ConnectionType.HTTP);
                        mbeanName = JBIJMXObjectNames
                                        .getJbiReferenceAdminUiMBeanObjectName();
                        result = connection.isRegistered(mbeanName);
                } catch (MalformedObjectNameException e) {
                        connection = null;
                        exceptionArgument = e;
                } catch (IOException e) {
                        connection = null;
                        exceptionArgument = e;
                } catch (RuntimeException runtimeException) {
                        connection = null;
                        exceptionArgument = runtimeException;
                } catch (Exception e) {
                        connection = null;
                        exceptionArgument = e;
                }

                if (connection == null) {
                        // Try to obtain a HTTPS (secure) connection
                        try {
                                connection = factory.getMBeanServerConnection(hostName,
                                                portNumber, userName, password, ConnectionType.HTTPS);
                                mbeanName = JBIJMXObjectNames
                                                .getJbiReferenceAdminUiMBeanObjectName();
                                result = connection.isRegistered(mbeanName);
                        } catch (MalformedObjectNameException e) {
                                connection = null;
                                exceptionArgument = e;
                        } catch (IOException e) {
                                connection = null;
                                exceptionArgument = e;
                        } catch (RuntimeException runtimeException) {
                                connection = null;
                                exceptionArgument = runtimeException;
                        } catch (Exception e) {
                                connection = null;
                                exceptionArgument = e;
                        }
                }

                if (connection == null) {
                        // Try to obtain a JRMP connection
                        try {
                                connection = factory.getMBeanServerConnection(hostName,
                                                portNumber, userName, password, ConnectionType.JRMP);
                                mbeanName = JBIJMXObjectNames
                                                .getJbiReferenceAdminUiMBeanObjectName();
                                result = connection.isRegistered(mbeanName);
                        } catch (MalformedObjectNameException e) {
                                connection = null;
                                exceptionArgument = e;
                        } catch (IOException e) {
                                connection = null;
                                exceptionArgument = e;
                        } catch (RuntimeException runtimeException) {
                                connection = null;
                                exceptionArgument = runtimeException;
                        } catch (Exception e) {
                                connection = null;
                                exceptionArgument = e;
                        }
                }

                if (connection == null) {
                        // Try to obtain a IIOP connection for websphere
                        // The CORBA calls below send errors to stderr,
                        // leading to very ugly ant output (CR 6586235).
                        // It turns out that this is documented as CR 5068014
                        // and the suggested workaround there is to redirect
                        // System.err, as we do below:
                        java.io.PrintStream oldErr = System.err;
                        try {
                                java.io.PrintStream newErr = new java.io.PrintStream(
                                    new java.io.ByteArrayOutputStream());
                                System.setErr(newErr);
                                connection = factory.getMBeanServerConnection(hostName,
                                                portNumber, userName, password, ConnectionType.IIOP);
                                mbeanName = JBIJMXObjectNames
                                                .getJbiReferenceAdminUiMBeanObjectName();
                                result = connection.isRegistered(mbeanName);
                                newErr.close();
                        } catch (MalformedObjectNameException e) {
                                connection = null;
                                exceptionArgument = e;
                        } catch (IOException e) {
                                connection = null;
                                exceptionArgument = e;
                        } catch (RuntimeException runtimeException) {
                                connection = null;
                                exceptionArgument = runtimeException;
                        } catch (Exception e) {
                                connection = null;
                                exceptionArgument = e;
                        }
                        System.setErr(oldErr);
                }
                
                boolean isLocalHost = false;
                try {
                        isLocalHost = Util.isLocalHost(hostName);
                } catch (UnknownHostException e) {
                        String[] args = { hostName };
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection.unknown.host", args, null);
                        throw new JBIRemoteException(exception);
                }
                if (connection != null) {
                        if (isLocalHost == true) {
                                commands = new JBIAdminCommandsClientImpl(connection, false);
                        } else {
                                commands = new JBIAdminCommandsClientImpl(connection, true);
                        }
                } else {
                        String[] args = { hostName, Integer.valueOf(portNumber).toString(),
                                        userName };
                        Exception exception = createManagementException(
                                        "jbi.ui.client.connection.failure", args, null);
                        throw new JBIRemoteException(exception);
                }
                return commands;
        }

        /**
         * Creates a new instance of JBIAdminCommands object for a remote connection
         * 
         * @param connection
         * @return JBIAdminCommands object
         * @throws JBIRemoteException
         */
        public static JBIAdminCommands getInstance(MBeanServerConnection connection)
                        throws JBIRemoteException {
                JBIAdminCommands commands = null;
                if (connection != null) {
                        commands = new JBIAdminCommandsClientImpl(connection, true);
                } else {
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection", null, null);
                        throw new JBIRemoteException(exception);
                }

                return commands;
        }

        /**
         * Creates a new instance of JBIAdminCommands object
         * 
         * @param connection
         * @param isRemoteConnection -
         *            true if remote, false if local
         * @return JBIAdminCommands object
         * @throws JBIRemoteException
         */
        public static JBIAdminCommands getInstance(
                        MBeanServerConnection connection, boolean isRemoteConnection)
                        throws JBIRemoteException {
                JBIAdminCommands commands = null;
                if (connection != null) {
                        commands = new JBIAdminCommandsClientImpl(connection,
                                        isRemoteConnection);
                } else {
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection", null, null);
                        throw new JBIRemoteException(exception);
                }

                return commands;
        }

        /**
         * This method returns the MBeanServerConnection to used to invoke the MBean
         * methods via HTTP connector.
         * 
         * @param url -
         *            service:jmx:rmi:///jndi/rmi://<hostName>:<portNumber>/management/rmi-jmx-connector
         * @userName - the userName name for authenticating with MBeanServer
         * @password - the password for authenticating with MBeanServer
         * @return MBeanServerConnection
         * @throws JBIRemoteException
         */
        protected MBeanServerConnection getMBeanServerConnection(String urlString,
                        String userName, String password) throws JBIRemoteException {
                try {
                        // Create a JMXMP connector client and
                        // connect it to the JMXMP connector server
                        // final JMXServiceURL url = new JMXServiceURL(urlString);
                        // final JMXServiceURL url = new JMXServiceURL(null, hostName,
                        // portNumber);
                        final JMXServiceURL url = new JMXServiceURL(urlString);
                        String[] credentials = new String[] { userName, password };
                        Map<String, String[]> environment = new HashMap<String, String[]>();
                        environment.put("jmx.remote.credentials", credentials);
                        final JMXConnector connector = JMXConnectorFactory.connect(url,
                                        environment);
                        return connector.getMBeanServerConnection();
                } catch (Exception e) {
                        String[] args = { urlString, userName };
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection.url.uname.password",
                                        args, e);
                        throw new JBIRemoteException(exception);
                }
        }

        /**
         * This method returns the MBeanServerConnection to used to invoke the MBean
         * methods via HTPP connector.
         * 
         * @param hostName -
         *            the hostName part of the URL. If null, defaults to the local
         *            hostName name, as determined by
         *            InetAddress.getLocalHost().getHostName(). If it is a numeric
         *            IPv6 address, it can optionally be enclosed in square brackets
         *            [].
         * @portNumber - the portNumber part of the URL.
         * @userName - the userName name for authenticating with MBeanServer
         * @password - the password for authenticating with MBeanServer
         * @return MBeanServerConnection
         * @throws JBIRemoteException
         */
        protected MBeanServerConnection getMBeanServerConnection(String hostName,
                        int portNumber, String userName, String password,
                        ConnectionType type) throws JBIRemoteException {
                try {
                        if (type == ConnectionType.JRMP) {
                                // Create a JMXMP connector client and
                                // connect it to the JMXMP connector server
                                // final JMXServiceURL url = new JMXServiceURL(null, hostName,
                                // portNumber);
                                // String urlString =
                                // "service:jmx:rmi:///jndi/rmi://"+hostName+":"+portNumber+"/jmxri";
                                String urlString = "service:jmx:rmi:///jndi/rmi://" + hostName
                                                + ":" + portNumber + "/jmxrmi";
                              return this.getMBeanServerConnection(urlString, userName,
                                                password);
                        }
                        else   if (type == ConnectionType.JRMX) {
                            // Create a JMXMP connector client and
                            // connect it to the JMXMP connector server
                            // final JMXServiceURL url = new JMXServiceURL(null, hostName,
                            // portNumber);
                            // String urlString =
                            // "service:jmx:rmi:///jndi/rmi://"+hostName+":"+portNumber+"/jmxri";
                            String urlString = "service:jmx:rmi:///jndi/rmi://" + hostName
                                            + ":" + portNumber + "/jmxconnector";
                          return this.getMBeanServerConnection(urlString, userName,
                                            password);
                    }                        else if ( type == ConnectionType.IIOP)
                        {
                                String urlString = 
                                        "service:jmx:iiop://" + hostName + ":" + 
                                        portNumber + "/jndi/JMXConnector";       
                                return this.getMBeanServerConnection(urlString, userName,
                                                password);                                
                        }
                        else {
                                final JMXServiceURL url = new JMXServiceURL(type.getProtocol(),
                                                hostName, portNumber);
                                final JMXConnector connector = JMXConnectorFactory.connect(url,
                                                this.initEnvironment(userName, password));
                                return connector.getMBeanServerConnection();
                        }
                } catch (Exception e) {
                        String[] args = { hostName, (new Integer(portNumber)).toString(),
                                        userName, type.getProtocol() };
                        // Exception exception = createManagementException(
                        // "jbi.ui.client.factory.connection.host.port.uname.password.protocol",
                        // args, e);
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection.host.port.uname.password.protocol",
                                        args, null);

                        throw new JBIRemoteException(exception);
                }
        }

        /**
         * This method initialize the environment for creating the JMXConnector.
         * 
         * @return Map - HashMap of environemtn
         */
        private Map<String, Object> initEnvironment(String userName, String password) {
                final Map<String, Object> environment = new HashMap<String, Object>();
                final String PKGS = "com.sun.enterprise.admin.jmx.remote.protocol";

                environment.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                                getClass().getClassLoader());
                environment.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, PKGS);
                environment.put("USER", userName);
                environment.put("PASSWORD", password);
                environment.put("com.sun.enterprise.as.http.auth", "BASIC");
                return (environment);
        }

        /**
         * This method creates the JMXServiceURL
         * 
         * @param protocol
         * @param hostName
         * @param portNumber
         * @throws JBIRemoteException
         */
        private JMXServiceURL getJMXServiceURL(String protocol, String hostName,
                        int portNumber) throws JBIRemoteException {
                try {
                        // Create a JMXMP connector client and connect it to the JMXMP
                        // connector server

                        final JMXServiceURL url = new JMXServiceURL(protocol, hostName,
                                        portNumber);
                        System.out.println("url = " + url.toString());
                        return url;
                } catch (MalformedURLException mue) {
                        String[] args = { protocol, hostName,
                                        (new Integer(portNumber)).toString(), };
                        Exception exception = createManagementException(
                                        "jbi.ui.client.factory.connection.protocol.host.port",
                                        args, mue);
                        throw new JBIRemoteException(exception);
                }
        }

        /**
         * @param args
         */
        public static void main(String[] args) {
                JBIAdminCommandsClientFactory factory = null;
                JBIAdminCommands commands = null;

                String hostName = "localhost";
                // int port = 18451;
                // int port = 18449;
                int port = 5651; // CAS
                port = 5649; // JBITest
                String userName = "admin";
                String password = "adminadmin";
                String dummyName = "foo";

                // Test 1
                // try {
                // commands = JBIAdminCommandsClientFactory
                // .getInstance(
                // "service:jmx:rmi:///jndi/rmi://"+hostName+":"+port+"/management/rmi-jmx-connector",
                // userName, password, false);
                // } catch (JBIRemoteException e) {

                // e.printStackTrace();
                // }

                // Test 2
                // try {
                // commands = JBIAdminCommandsClientFactory.getInstance(hostName,
                // port, userName, password, ConnectionType.JRMP);
                // } catch (JBIRemoteException e) {

                // e.printStackTrace();
                // }

                // // Test 3
                // try {
                // commands = JBIAdminCommandsClientFactory.getInstance(hostName,
                // port, userName, password, ConnectionType.HTTP);
                // } catch (JBIRemoteException e) {

                // e.printStackTrace();
                // }

                // // Test 4
                try {
                        commands = JBIAdminCommandsClientFactory.getInstance(hostName,
                                        port, userName, password);
                } catch (JBIRemoteException e) {

                        e.printStackTrace();
                }

                // // Test 5
                // factory = new JBIAdminCommandsClientFactory();
                // MBeanServerConnection connection = null;

                // try {
                // connection = factory.getMBeanServerConnection(hostName,
                // port, userName, password, ConnectionType.HTTP);
                // commands = JBIAdminCommandsClientFactory.getInstance(connection);
                // } catch (JBIRemoteException e) {

                // e.printStackTrace();
                // }

                // boolean isJBIRuntimeEnabled = false;
                // if (commands != null) {
                // try {
                // isJBIRuntimeEnabled = commands.isJBIRuntimeEnabled();
                // } catch (JBIRemoteException e) {

                // e.printStackTrace();
                // }
                // if (isJBIRuntimeEnabled == true) {
                // try {
                // String result = commands.listBindingComponents("domain1");
                // System.out.println("Result is: " + result);
                // } catch (JBIRemoteException e) {

                // e.printStackTrace();
                // }
                // }
                // }
                // System.out.println("The JBI Framework is "
                // + (isJBIRuntimeEnabled ? "Enabled." : "NOT Enabled."));
        }

}
