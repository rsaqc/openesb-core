/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PerformanceDataMapWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.sun.esb.management.common.data.PerformanceData;

/**
 * Writes Performance Measurement data to a String or file as XML
 *
 * @author graj
 */
public class PerformanceDataMapWriter implements
        PerformanceDataXMLConstants, Serializable {
    private static final long serialVersionUID = 1L;

    static final String       FILE_NAME_KEY    = "performancedatamap.xml";

    /** Constructor - Creates a new instance of PerformanceDataMapWriter */
    public PerformanceDataMapWriter() {
    }

    /**
     *
     * @param document
     * @param directoryPath
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws Exception
     */
    public static void writeToFile(Document document, String directoryPath)
            throws TransformerConfigurationException, TransformerException,
            Exception {
        File file = new File(directoryPath);
        if ((file.isDirectory() == false) || (file.exists() == false)) {
            throw new Exception("Directory Path: " + directoryPath
                    + " is invalid.");
        }
        String fileLocation = file.getAbsolutePath() + File.separator
                + FILE_NAME_KEY;
        System.out.println("Writing out to file: " + fileLocation);
        File outputFile = new File(fileLocation);
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputFile);

        // indent the aspectOutput to make it more legible...
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        transformer.transform(source, result);
    }

    /**
     * Change the contents of text file in its entirety, overwriting any
     * existing text. This style of implementation throws all exceptions to the
     * caller.
     *
     * @param aFile
     *            is an existing file which can be written to.
     * @throws IllegalArgumentException
     *             if param does not comply.
     * @throws FileNotFoundException
     *             if the file does not exist.
     * @throws IOException
     *             if problem encountered during write.
     */
    public static void setContents(File aFile, String aContents)
            throws FileNotFoundException, IOException {
        if (aFile == null) {
            throw new IllegalArgumentException("File should not be null.");
        }
        if (!aFile.exists()) {
            aFile.createNewFile();
        }
        if (!aFile.isFile()) {
            throw new IllegalArgumentException("Should not be a directory: "
                    + aFile);
        }
        if (!aFile.canWrite()) {
            throw new IllegalArgumentException("File cannot be written: "
                    + aFile);
        }

        // declared here only to make visible to finally clause; generic
        // reference
        Writer output = null;
        try {
            // use buffering
            // FileWriter always assumes default encoding is OK!
            output = new BufferedWriter(new FileWriter(aFile));
            output.write(aContents);
        } finally {
            // flush and close both "aspectOutput" and its underlying FileWriter
            if (output != null) {
                output.close();
            }
        }
    }

    /**
     *
     * @param performance
     *            data Map<String, PerforamanceData>
     * @return
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static String serialize(Map<String, PerformanceData> dataMap)
            throws ParserConfigurationException, TransformerException {
        Document document = null;
        PerformanceDataMapWriter writer = new PerformanceDataMapWriter();
        if (dataMap != null) {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument(); // Create from whole cloth

            // ////////////////////////////////
            // <PerformanceMeasurementDataList>
            Element root = (Element) document
                    .createElement(PERFORMANCE_MEASUREMENT_DATA_LIST_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/PerformanceMeasurementDataList"
            root.setAttribute(NAMESPACE_KEY, NAMESPACE_VALUE);
            // version = "1.0"
            root.setAttribute(VERSION_KEY, VERSION_VALUE);

            for (String category : dataMap.keySet()) {
                PerformanceData data = dataMap.get(category);
                // ////////////////////////////////
                // <PerformanceMeasurementData>
                Element performanceMeasurementDataElementChild = writer
                        .createPerformanceMeasurementDataElement(document, data);
                // </PerformanceMeasurementData>
                root.appendChild(performanceMeasurementDataElementChild);
                // ////////////////////////////////
            }
            // </PerformanceMeasurementDataList>
            document.appendChild(root);
            // ////////////////////////////////

        }
        return writer.writeToString(document);
    }

    /**
     *
     * @param document
     * @param data
     * @return
     */
    protected Element createPerformanceMeasurementDataElement(
            Document document, PerformanceData data) {
        Element performanceDataElement = null;
        if ((document != null) && (data != null)) {
            // <PerformanceMeasurementData>
            performanceDataElement = document
                    .createElement(PERFORMANCE_MEASUREMENT_DATA_KEY);

            // <category>
            Element categoryElementChild = document.createElement(CATEGORY_KEY);
            if (categoryElementChild != null) {
                categoryElementChild.setTextContent(data.getCategory());
            }
            // </category>
            performanceDataElement.appendChild(categoryElementChild);

            // <endpoint>
            Element endpointElementChild = document.createElement(ENDPOINT_KEY);
            if (endpointElementChild != null) {
                endpointElementChild.setTextContent(data.getEndpoint());
            }
            // </endpoint>
            performanceDataElement.appendChild(endpointElementChild);

            // <sourceClassName>
            Element sourceClassNameElementChild = document
                    .createElement(SOURCE_CLASS_NAME_KEY);
            if (sourceClassNameElementChild != null) {
                sourceClassNameElementChild.setTextContent(data
                        .getSourceClassName());
            }
            // </sourceClassName>
            performanceDataElement.appendChild(sourceClassNameElementChild);

            // <numberOfMeasurementObjects>
            Element numberOfMeasurementObjectsElementChild = document
                    .createElement(NUMBER_OF_MEASUREMENT_OBJECTS_KEY);
            if (numberOfMeasurementObjectsElementChild != null) {
                numberOfMeasurementObjectsElementChild.setTextContent(data
                        .getNumberOfMeasurementObjects()
                        + "");
            }
            // </numberOfMeasurementObjects>
            performanceDataElement
                    .appendChild(numberOfMeasurementObjectsElementChild);

            // <numberOfMeasurements>
            Element numberOfMeasurementsElementChild = document
                    .createElement(NUMBER_OF_MEASUREMENTS_KEY);
            if (categoryElementChild != null) {
                numberOfMeasurementsElementChild.setTextContent(data
                        .getNumberOfMeasurements()
                        + "");
            }
            // </numberOfMeasurements>
            performanceDataElement
                    .appendChild(numberOfMeasurementsElementChild);

            // <average>
            Element averageElementChild = document.createElement(AVERAGE_KEY);
            if (averageElementChild != null) {
                averageElementChild.setTextContent(data.getAverage() + "");
            }
            // </average>
            performanceDataElement.appendChild(averageElementChild);

            // <averageWithoutFirstMeasurement>
            Element averageWithoutFirstMeasurementElementChild = document
                    .createElement(AVERAGE_WITHOUT_FIRST_MEASUREMENT_KEY);
            if (averageWithoutFirstMeasurementElementChild != null) {
                averageWithoutFirstMeasurementElementChild.setTextContent(data
                        .getAverageWithoutFirstMeasurement()
                        + "");
            }
            // </averageWithoutFirstMeasurement>
            performanceDataElement
                    .appendChild(averageWithoutFirstMeasurementElementChild);

            // <firstMeasurementTime>
            Element firstMeasurementTimeElementChild = document
                    .createElement(FIRST_MEASUREMENT_TIME_KEY);
            if (firstMeasurementTimeElementChild != null) {
                firstMeasurementTimeElementChild.setTextContent(data
                        .getFirstMeasurementTime()
                        + "");
            }
            // </firstMeasurementTime>
            performanceDataElement
                    .appendChild(firstMeasurementTimeElementChild);

            // <load>
            Element loadElementChild = document.createElement(LOAD_KEY);
            if (loadElementChild != null) {
                loadElementChild.setTextContent(data.getLoad() + "");
            }
            // </load>
            performanceDataElement.appendChild(loadElementChild);

            // <median>
            Element medianElementChild = document.createElement(MEDIAN_KEY);
            if (medianElementChild != null) {
                medianElementChild.setTextContent(data.getMedian() + "");
            }
            // </median>
            performanceDataElement.appendChild(medianElementChild);

            // <throughput>
            Element throughputElementChild = document
                    .createElement(THROUGHPUT_KEY);
            if (throughputElementChild != null) {
                throughputElementChild
                        .setTextContent(data.getThroughput() + "");
            }
            // </throughput>
            performanceDataElement.appendChild(throughputElementChild);

            // <timeTaken>
            Element timeTakenElementChild = document
                    .createElement(TIME_TAKEN_KEY);
            if (timeTakenElementChild != null) {
                timeTakenElementChild.setTextContent(data.getTimeTaken() + "");
            }
            // </timeTaken>
            performanceDataElement.appendChild(timeTakenElementChild);

            // <totalTime>
            Element totalTimeElementChild = document
                    .createElement(TOTAL_TIME_KEY);
            if (totalTimeElementChild != null) {
                totalTimeElementChild.setTextContent(data.getTotalTime() + "");
            }
            // </totalTime>
            performanceDataElement.appendChild(totalTimeElementChild);
        }
        return performanceDataElement;
    }

    /**
     * @param document
     * @return
     * @throws TransformerException
     */
    protected String writeToString(Document document)
            throws TransformerException {
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);

        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        // indent the aspectOutput to make it more legible...
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);

        return result.getWriter().toString();
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // String uri = "C:/test/performance/instance.xml";
        String uri = "C:/test/schema/performance/PerformanceMeasurementDataList.xml";
        try {
            Map<String, PerformanceData> map = PerformanceDataMapReader
                    .parseFromFile(uri);
            for (String category : map.keySet()) {
                System.out.println(map.get(category).getDisplayString());
            }
            String content = PerformanceDataMapWriter.serialize(map);
            System.out.println(content);
            PerformanceDataMapWriter.setContents(new File(uri), content);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
