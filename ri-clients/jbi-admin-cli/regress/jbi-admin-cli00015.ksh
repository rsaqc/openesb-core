#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test set-jbi-component-configuration"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-configuration --target domain--component=sun-http-binding OutboundThreads=5"
  $AS8BASE/bin/asadmin set-jbi-component-configuration --target domain --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS OutboundThreads=5
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test create-jbi-application-configuration"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-configuration --target domain --component=cli-config-binding --configname=testConfig testConfigData.properties"
  $AS8BASE/bin/asadmin create-jbi-application-configuration --target domain --component=cli-config-binding --configname=testConfig --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS testConfigData.properties
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test update-jbi-application-configuration"
  echo "-------------------------------------------------------------------"
  echo "update-jbi-application-configuration --target domain --component=cli-config-binding --configname=testConfig testConfigData.properties"
  $AS8BASE/bin/asadmin update-jbi-application-configuration --target domain --component=cli-config-binding --configname=testConfig --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS testConfigData.properties
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test create-jbi-application-variable"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-variable --target domain --component=cli-config-binding FirstName=[String]Tom"
  $AS8BASE/bin/asadmin create-jbi-application-variable --target domain --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS FirstName=[String]Tom
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test update-jbi-application-variable"
  echo "-------------------------------------------------------------------"
  echo "update-jbi-application-variable --target domain --component=cli-config-binding FirstName=[String]Steve,LastName=Jones"
  $AS8BASE/bin/asadmin update-jbi-application-variable --target domain --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS FirstName=[String]Steve,LastName=Jones
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test delete-jbi-application-variable"
  echo "-------------------------------------------------------------------"
  echo "delete-jbi-application-configuration --target domain --component=cli-config-binding testConfig"
  $AS8BASE/bin/asadmin delete-jbi-application-configuration --target domain --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS testConfig
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test delete-jbi-application-configuration"
  echo "-------------------------------------------------------------------"
  echo "delete-jbi-application-configuration --target domain --component=cli-config-binding testConfig"
  $AS8BASE/bin/asadmin delete-jbi-application-configuration --target domain --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS testConfig
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test list-jbi-application-configuration"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-configurations --target domain --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-configurations --target domain --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test list-jbi-application-variables"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --target domain --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --target domain --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test show-jbi-application-configuration"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-application-configuration --target domain --component=cli-config-binding testConfig"
  $AS8BASE/bin/asadmin show-jbi-application-configuration --target domain --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS testConfig
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test show-jbi-statistics"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --target domain --framework"
  $AS8BASE/bin/asadmin show-jbi-statistics --target domain --framework --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test verify-jbi-application-environment"
  echo "-------------------------------------------------------------------"
  echo "verify-jbi-application-environment --target domain sa2.jar"
  $AS8BASE/bin/asadmin verify-jbi-application-environment --target domain --templatedir=${JV_SVC_BLD}/regress/testdata/tmp/verify --includedeploy --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/sa2.jar
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Test export-jbi-application-environment"
  echo "-------------------------------------------------------------------"
  echo "export-jbi-application-environment --target domain SA2"
  $AS8BASE/bin/asadmin export-jbi-application-environment --target domain --configdir=${JV_SVC_BLD}/regress/testdata/tmp/export --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SA2
      
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00015"
TEST_DESCRIPTION="Test unsupported domain commands"
. ./regress_defs.ksh
run_test

exit 0


