#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Remove any components or shared libraries that may impede the test.  Note, all
# output is directed to the temp output file.
#----------------------------------------------------------------------------------
test_setup()
{
 $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --echo cli_test_binding1 >> $TEMP_OUTPUT_FILE 2>&1
 $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --echo cli_test_engine1 >> $TEMP_OUTPUT_FILE 2>&1
 $AS8BASE/bin/asadmin uninstall-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --echo cli_test_sns1 >> $TEMP_OUTPUT_FILE 2>&1
}

#----------------------------------------------------------------------------------
# Install the shared library and the components
#----------------------------------------------------------------------------------
test_install()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install the shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library cli-test-sns1.jar"
  $AS8BASE/bin/asadmin install-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-sns1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install binding component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-binding1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-binding1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install service engine cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-engine1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-engine1.jar 
}

#----------------------------------------------------------------------------------
# Uninstall the components and shared library
#----------------------------------------------------------------------------------
test_uninstall()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the component cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-shared-library cli_test_sns1"
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1
}


#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  test_setup
}

#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
 initilize_test 
 test_setup
 test_install
 test_uninstall
 test_cleanup
}

#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00001"
TEST_DESCRIPTION="Test Component Install and Uninstall"
. ./regress_defs.ksh
run_test

exit 0
