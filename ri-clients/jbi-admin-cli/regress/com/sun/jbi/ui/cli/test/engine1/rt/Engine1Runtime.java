/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Engine1Runtime.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.cli.test.engine1.rt;

import com.sun.jbi.management.ManagementMessageBuilder;
import com.sun.jbi.ui.cli.test.engine1.sns1impl.SNS1DocImpl;
import com.sun.jbi.ui.cli.test.sns1.SNS1Doc;
import java.util.logging.Logger;
import javax.jbi.JBIException;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.management.MBeanNames;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;




/**
 * This is an implementation of a Business Process Engine that is purely for
 * unit testing. It does nothing other than log messages when its methods
 * are called.
 *
 * @author Sun Microsystems, Inc.
 */
public class Engine1Runtime implements Component, ComponentLifeCycle
{
    /**
     * Local copy of the component ID
     */
    private String mComponentId;
    
    /**
     * fix it
     */    
    private ComponentContext mContext;
    /**
     * fix it
     */    
    private ManagementMessageBuilder mMgmtMsgBuilder;
    
    /**
     * fix it
     */    
    private Engine1Deployer mDeployer;
    
    /**
     * Logger instance
     */
    private Logger mLog = Logger.getLogger("com.sun.jbi.ui.cli.test.engine1");
    
    /**
     * Get the required life cycle control implementation for this component.
     * @return the life cycle control implementation for this component.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return this;
    }

    /**
     * Get the Service Unit manager for this component. If this component
     * does not support deployments, return <code>null</code>.
     * @return the Service Unit manager for this component, or <code>null</code>
     * if there is none.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        return mDeployer;
    }

    /**
     * Resolve descriptor details for the specified reference, which is for a
     * service provided by this component.
     * @param ref the endpoint reference to be resolved.
     * @return the description for the specified reference.
     */
    public org.w3c.dom.Document getServiceDescription(ServiceEndpoint ref)
    {
        return null;
    }

    
    /**
     * Initialize the Business Process Engine.
     * @param context the JBI environment context created
     * by the JBI framework
     * @throws JBIException if an error occurs
     */
    public void init(ComponentContext context) throws JBIException
    {
        
        if ( context == null  )
        {
            throw new JBIException("Null argument received for " +
            "ComponentContext");
        }
        
        this.mLog = Logger.getLogger("com.sun.jbi.ui.cli.test.engine1");
        
        this.mContext = context;
        this.mComponentId = context.getComponentName();
        
        try
        {
            this.mMgmtMsgBuilder =
                ((com.sun.jbi.component.ComponentContext) mContext).
                getManagementMessageFactory().newBuildManagementMessage();
        }
        catch (Exception e)
        {
            throw new JBIException("Unable to create Mmgt Msg Builder", e);
        }
        
        createDeployer();
        
        mLog.info("Engine " + mComponentId + " initialized");
    }
    
    /**
     * Get the JMX ObjectName for any additional MBean for this BPE. If there
     * is none, return null.
     * @return ObjectName the JMX object name of the additional MBean or null
     * if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        return null;
    }
    
    /**
     * Start the Business Process Engine.
     * @throws JBIException if an error occurs
     */
    public void start() throws JBIException
    {
        
        try
        {
            SNS1Doc sns1Doc = new SNS1DocImpl();
            mLog.info(sns1Doc.getContentAsString());
        } catch (Exception ex )
        {
            mLog.info("SNS Access Error : " + ex.getMessage());
        }
        mLog.info("Engine " + mComponentId + " started");
    }
    
    /**
     * Stop the Business Process Engine.
     * @throws JBIException if an error occurs
     */
    public void stop() throws JBIException
    {
        mLog.info("Engine " + mComponentId + " stopped");
    }
    
    /**
     * Shut down the Business Process Engine.
     * @throws JBIException if an error occurs
     */
    public void shutDown() throws JBIException
    {
        mLog.info("Engine " + mComponentId + " shut down");
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public Logger getLogger()
    {
        return this.mLog;
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public String getId()
    {
        return this.mComponentId;
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public ComponentContext getContext()
    {
        return this.mContext;
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public ManagementMessageBuilder getMgmtMsgBuilder()
    {
        return this.mMgmtMsgBuilder;
    }
    
    /**
     * fix it
     * @throws JBIException fix it
     */    
    public void createDeployer() throws JBIException
    {
        try
        {
            if ( this.mDeployer == null )
            {
                this.mDeployer = new Engine1Deployer(this); 
            }
            
        } catch (Exception e)
        {
            throw new JBIException("cli Test Engine1 Deployer Creation Failed",e);
        }
    }
    
     /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public javax.jbi.servicedesc.ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment epr)
    {
        return null;
    }
}
