/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Engine3Installer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.cli.test.engine3.boot;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.InstallationContext;
import java.util.logging.Logger;


/**
 * Class to install the  engine.
 *
 * @author Sun Microsystems, Inc.
 */
public class Engine3Installer implements Bootstrap
{
    
    /**
     * Internal handle to the logger instance
     */
    private Logger mLogger ;
    
    /**
     * Creates a new instance of Engine3Installer
     */
    public Engine3Installer()
    {
        mLogger = Logger.getLogger("com.sun.jbi.ui.cli.test.engine3.boot");
    }

   /**
    * Cleans up any resources allocated by the bootstrap implementation,
    * including deregistration of the extension MBean, if applicable.
    * This method will be called after the onInstall() or onUninstall() method
    * is called, whether it succeeds or fails.
    * @throws javax.jbi.JBIException when cleanup processing fails to complete
    * successfully.
    */
   public void cleanUp()
       throws javax.jbi.JBIException
    {
    }
    
    /**
     * Called to initialize the BC bootstrap.
     * @param installContext is the context containing information
     * from the install command and from the BC jar file.
     * @throws javax.jbi.JBIException when there is an error requiring that
     * the installation be terminated.
     */
    public void init(InstallationContext installContext)
        throws javax.jbi.JBIException
    {
        return;
    }

    /**
     * Get the JMX ObjectName for the optional installation configuration MBean
     * for this BC. If there is none, the value is null.
     * @return ObjectName the JMX object name of the installation configuration
     * MBean or null if there is no MBean.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Called at the beginning of installation of BPEL Engine. For this
     * Engine, all the required installation tasks have been taken care
     * by the InstallationService.
     * @throws javax.jbi.JBIException when there is an error requiring that
     * the installation be terminated.
     */
    public void onInstall()
        throws javax.jbi.JBIException
    {
        mLogger.info("Installing cli Test Engine3");
    }

    /**
     * Called at the beginning of uninstallation of FileEngine . For this
     * file engine, all the required uninstallation tasks have been taken care
     * of by the InstallationService
     * @throws javax.jbi.JBIException when there is an error requiring that
     * the uninstallation be terminated.
     */
    public void onUninstall()
        throws javax.jbi.JBIException
    {
        mLogger.info("Uninstalling cli Test Engine3");
    }
}
