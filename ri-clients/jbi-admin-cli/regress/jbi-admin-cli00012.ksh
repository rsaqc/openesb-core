#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
  #test_cleanup
              
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Verify the jbi application sa1.jar"
  echo "-------------------------------------------------------------------"
  echo "verify-jbi-application-environment sa1.jar"
  $AS8BASE/bin/asadmin verify-jbi-application-environment --templatedir=${JV_SVC_BLD}/regress/testdata/tmp --includedeploy --target server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/sa1.jar
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Verify the jbi application ping-sa.jar"
  echo "-------------------------------------------------------------------"
  echo "verify-jbi-application-environment sa1.jar"
  $AS8BASE/bin/asadmin verify-jbi-application-environment --templatedir=${JV_SVC_BLD}/regress/testdata/tmp --includedeploy --target server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/ping-sa.jar
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Verify the jbi application javaee-sa.jar"
  echo "-------------------------------------------------------------------"
  echo "verify-jbi-application-environment javaee.jar"
  $AS8BASE/bin/asadmin verify-jbi-application-environment --templatedir=${JV_SVC_BLD}/regress/testdata/tmp --includedeploy --target server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/javaee-sa.jar
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install test components test-component and test-component1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component test-component.jar"
  echo "install-jbi-component test-component1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/test-component.jar 
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/test-component1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Verify the jbi application test-sa.jar"
  echo "-------------------------------------------------------------------"
  echo "verify-jbi-application-environment test-sa.jar"
  $AS8BASE/bin/asadmin verify-jbi-application-environment --templatedir=${JV_SVC_BLD}/regress/testdata/tmp --includedeploy --target server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/test-sa.jar
  
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00012"
TEST_DESCRIPTION="Test Verify Command"
. ./regress_defs.ksh
run_test

exit 0


