/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
* 
* Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
* 
* The contents of this file are subject to the terms of either the GNU
* General Public License Version 2 only ("GPL") or the Common Development
* and Distribution License("CDDL") (collectively, the "License").  You
* may not use this file except in compliance with the License. You can obtain
* a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
* or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
* language governing permissions and limitations under the License.
* 
* When distributing the software, include this License Header Notice in each
* file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
* Sun designates this particular file as subject to the "Classpath" exception
* as provided by Sun in the GPL Version 2 section of the License file that
* accompanied this code.  If applicable, add the following below the License
* Header, with the fields enclosed by brackets [] replaced by your own
* identifying information: "Portions Copyrighted [year]
* [name of copyright owner]"
* 
* Contributor(s):
* 
* If you wish your version of this file to be governed by only the CDDL or
* only the GPL Version 2, indicate your decision by adding "[Contributor]
* elects to include this software in this distribution under the [CDDL or GPL
* Version 2] license."  If you don't indicate a single choice of license, a
* recipient has the option to distribute your version of this file under
* either the CDDL, the GPL Version 2 or to extend the choice of license to
* its licensees as provided above.  However, if you add GPL Version 2 code
* and therefore, elected the GPL Version 2 license, then the option applies
* only if the new code is made subject to such option by the copyright
* holder.
*/

package com.sun.jbi.cli.commands;

import com.sun.enterprise.util.i18n.StringManager; 
import com.sun.enterprise.cli.framework.CLITokenizer;
import com.sun.enterprise.cli.framework.CommandException;
import com.sun.enterprise.cli.framework.CommandValidationException;
import com.sun.enterprise.cli.framework.CLILogger;
import com.sun.jbi.cli.commands.JBICommandConstants;

import com.sun.jbi.ui.common.JBITimeUtil;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.common.ServiceUnitInfo;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIStatisticsItemNames;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIVerifierReportItemNames;

import com.sun.jbi.ui.client.JBIAdminCommandsClientFactory;
import com.sun.appserv.management.util.misc.ExceptionUtil;

import javax.management.openmbean.TabularData;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.ArrayType;
import javax.management.MBeanServerConnection;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Properties;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.StringTokenizer;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;

import java.lang.Long;
import java.lang.Short;
import java.lang.Boolean;
import java.lang.Integer;


/**
*  Will start the JBI component on the specified target.
*  @version  $Revision: 1.26 $
*/
public abstract class JBICommand extends com.sun.enterprise.cli.commands.S1ASCommand
{
    JBIAdminCommands mJBICmds = null;

    static String[] mValidStates = {
        "started",
        "stopped",
        "shutdown"};

    static String[] mVariableTypes = {
        JBICommandConstants.STRING_TYPE, 
        JBICommandConstants.BOOLEAN_TYPE, 
        JBICommandConstants.NUMBER_TYPE, 
        JBICommandConstants.PASSWORD_TYPE };


    static final String CONFIG_COMMAND_FILENAME_ANT     = "configure.xml";
    static final String CONFIG_COMMAND_FILENAME_ASADMIN = "configure.txt";
    static final String CONFIG_PROPERTIES_FILENAME      = "connection.properties";
    
    // Hash that is used when converting the configure.xml (ant syntax) to file
    // to configue.txt (asadmin syntax).
    static Hashtable mCommandConversionTbl = new Hashtable ();

    static
    {
        mCommandConversionTbl.put (
            "create-app-variable", 
            "create-jbi-application-variable --component=component.name properties.file.name");
        mCommandConversionTbl.put (
            "create-app-configuration", 
            "create-jbi-application-configuration --component=component.name --configname=config.name properties.file.name");
        mCommandConversionTbl.put (
            "deploy-application",       
            "deploy-jbi-service-assembly deploy.file");
    }

    // Hash that is used when converting the conversion.properties from
    // ant syntax to asadmin syntax.
    static Hashtable mConnectionConversionTbl = new Hashtable();
    static
    {
        mConnectionConversionTbl.put ("jbi.username", "AS_ADMIN_USER");
        mConnectionConversionTbl.put ("jbi.password", "AS_ADMIN_PASSWORD");
        mConnectionConversionTbl.put ("jbi.host",     "AS_ADMIN_HOST");
        mConnectionConversionTbl.put ("jbi.port",     "AS_ADMIN_PORT");
        mConnectionConversionTbl.put ("jbi.secure",   "AS_ADMIN_SECURE");
        mConnectionConversionTbl.put ("jbi.target",   "AS_ADMIN_TARGET");
    }

    JBIComponentInfo mInfo = null;
    JBIAdminCommands mJbiAdminCommands = null;
    MBeanServerConnection mbsc = null;

    private static final StringManager _strMgr =  StringManager.getManager("com.sun.jbi.cli.resources"); 

    Hashtable mOptionHeaderInfo = new Hashtable();


    /**
     * Overloaded method that will first use the StringManager to look for
     * the property value,  If not found there, the super class getLocalizedString
     * method will used.  This needs to be implemented for multimode to work.
     * @param key The properties key
     * @return The property value (localized string)
     */
    protected String getLocalizedString(String key) {
        String localizedString = _strMgr.getStringWithDefault(key, "unabletofind");
        if (localizedString.equalsIgnoreCase("unabletofind"))
        {
            localizedString = super.getLocalizedString(key);
        }
        return localizedString;
    }

   
    /**
     * Overloaded method that will first use the StringManager to look for
     * the property value,  If not found there, the super class getLocalizedString
     * method will used.  This needs to be implemented for multimode to work.
     * @param key The properties key
     * @param args[] The array of arguments
     * @return The property value (localized string)
     */
    protected String getLocalizedString(String key, Object[] args) {
        String localizedString = _strMgr.getStringWithDefault(key, "unabletofind", args);
        if (localizedString.equalsIgnoreCase("unabletofind"))
        {
            localizedString = super.getLocalizedString(key,args);
        }
        return localizedString;
    }


    /**
     * Will insert the default type [STRING] into the properties string
     * if one is not present.
     * @param properties The properties to analyize and update
     */
    protected Properties insertType (Properties properties)
    {
        Properties returnProperties = new Properties();
        Set keys = properties.keySet();
        TreeSet <String> sortedKeys = new TreeSet <String>(keys);
        for (String key : sortedKeys)
        {
            String value = (String)properties.get(key);
            Boolean typeFound = false;
            for (int i=0; i<mVariableTypes.length; i++)
            {
                if (value.toUpperCase().startsWith(mVariableTypes[i]))
                {
                    typeFound = true;
                    break;
                }
            }
            if (!(typeFound))
            {
                value = JBICommandConstants.STRING_TYPE + (String)properties.get(key);
            }
            returnProperties.put(key,value);
        }
        return returnProperties;
    }


    /**
     * Will display the descriptor result (xml file)
     * @param descriptor string (xml format)
     */
    protected void displayDescriptor (String descriptor)
    {
        displayMessage (descriptor,0);
    }


    /**
     * Will return the command name
     * @return the command name
     */
    public String getName () {
        return name;
    }


    /**
     * Will process the NMR TabularData object to display the NMR
     * information to the screen.
     * @param data The TabularData containing the CompositData to display
     */
    protected void processNMRResults (TabularData data) throws CommandException
    {
        displayOptionHeader("JBINMRStatisticsHeader",0);

        if ((data == null) || (data.size() == 0) || (data.isEmpty())) {
            throw new CommandException(
                getLocalizedString ("JBIErrorRetrievingNMRStats",new Object[] {}));
        }

        else 
        {
            String msg = "";
            Iterator iterator = data.values().iterator();
            while (iterator.hasNext()) {
                CompositeData compData = (CompositeData)iterator.next();
                String[] activeChannels  = (String []) compData.get(JBIStatisticsItemNames.NMR_STATS_ACTIVE_CHANNELS);
                String[] activeEndpoints = (String []) compData.get(JBIStatisticsItemNames.NMR_STATS_ACTIVE_ENDPOINTS);
                //    String[] activeChannels = {"sun-http-binding","sun-bpel-engine","sun-javaee-engine"};
                //    String[] activeEndpoints = {"http://localhost/SynchronousSample,service1,endpoint1","http://localhost/New,service1,endpoint2","http://localhost/Old,service1,endpoint3"};

                // Display the Instance Name
                String instanceName = (String) compData.get(JBIStatisticsItemNames.INSTANCE_NAME);
                msg = getLocalizedString ("JBIStatsNMRInstanceName",new Object[] {instanceName});
                displayMessage (msg,0);
                
                msg = getLocalizedString ("JBIStatsNMRTotalActiveChannels",new Object[] {activeChannels.length});
                displayMessage (msg,0);

                msg = getLocalizedString ("JBIStatsNMRTotalActiveEndpoints",new Object[] {activeEndpoints.length});
                displayMessage (msg,0);


                if (activeChannels != null)
                {
                    displayOptionHeader("JBIStatsNMRActiveChannelsHeader",4);
                    for (int i=0; i<activeChannels.length; i++)
                    {
                        displayMessage (activeChannels[i],4);
                    }
                }

                if (activeEndpoints != null)
                {
                    displayOptionHeader("JBIStatsNMRActiveEndpointsHeader",4);
                    for (int i=0; i<activeEndpoints.length; i++)
                    {
                        displayMessage (activeEndpoints[i],4);
                    }
                }
            }
            displayMessage (" ",0);
        }
    }


    /**
     * Will process the Component TabularData object to display the Component
     * information to the screen.
     * @param data The TabularData containing the CompositData to display
     * @param componentName The Component name
     */
    protected void processComponentResults (TabularData data, String componentName) throws CommandException,
                                                                                           JBIRemoteException
    {
        if ((data == null) || (data.size() == 0) || (data.isEmpty())) {
            throw new CommandException(
                getLocalizedString ("JBIErrorRetrievingComponentStats",new Object[] {}));
        }

        else 
        {
            String msg = "";
            Iterator iterator = data.values().iterator();
            while (iterator.hasNext()) {

                CompositeData compData = (CompositeData)iterator.next();
                CompositeType compType = compData.getCompositeType();
                Set compItemSet        = compType.keySet();

                displayOptionHeader("JBIComponentStatisticsHeader",0);

                msg = getLocalizedString ("JBIStatsCompComponentLabel",new Object[] {componentName});
                displayMessage (msg,0);

                String instanceName = (String) compData.get(JBIStatisticsItemNames.INSTANCE_NAME);
                msg = getLocalizedString ("JBIStatsCompInstanceLabel",new Object[] {instanceName});
                displayMessage (msg,0);

                displayCompositeUpTime (compData,
                                        JBIStatisticsItemNames.COMPONENT_UPTIME,
                                        "JBIStatsCompontUpTime",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.NUMBER_OF_ACTIVATED_ENDPOINTS,
                                      "JBINumberOfActiveEndpoints",0);


                //displayOptionHeader("JBIShowReceivedStatisticsHeader",4);
                saveOptionHeaderInfo ("JBIShowReceivedStatisticsHeader",4);
                displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REQUESTS,
                                      "JBINumberOfReceivedRequests",4);
                displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REPLIES,
                                      "JBINumberOfReceivedReplies",4);
                displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES,
                                      "JBINumberOfReceivedDones",4);
                displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS,
                                      "JBINumberOfReceivedFaults",4);
                displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS,
                                      "JBINumberOfReceivedErrors",4);
                removeOptionHeaderInfo ("JBIShowReceivedStatisticsHeader");


                //displayOptionHeader("JBIShowSentStatisticsHeader",4);
                saveOptionHeaderInfo ("JBIShowSentStatisticsHeader",4);
                displayCompositeItem ("JBIShowSentStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_SENT_REQUESTS,
                                      "JBINumberOfSentRequests",4);
                displayCompositeItem ("JBIShowSentStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_SENT_REPLIES,
                                      "JBINumberOfSentReplies",4);
                displayCompositeItem ("JBIShowSentStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_SENT_DONES,
                                      "JBINumberOfSentDones",4);
                displayCompositeItem ("JBIShowSentStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS,
                                      "JBINumberOfSentFaults",4);
                displayCompositeItem ("JBIShowSentStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS,
                                      "JBINumberOfSentErrors",4);
                removeOptionHeaderInfo ("JBIShowSentStatisticsHeader");

                //displayOptionHeader("JBIShowExchangesStatisticsHeader",4);
                saveOptionHeaderInfo ("JBIShowExchangesStatisticsHeader",4);
                displayCompositeItem ("JBIShowExchangesStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_COMPLETED_EXCHANGES,
                                      "JBINumberOfCompletedExchanges",4);
                displayCompositeItem ("JBIShowExchangesStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES,
                                      "JBINumberOfActiveExchanges",4);
                displayCompositeItem ("JBIShowExchangesStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES_MAX,
                                      "JBINumberOfActiveExchangesMax",4);
                displayCompositeItem ("JBIShowExchangesStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_QUEUED_EXCHANGES,
                                      "JBINumberOfQueuedExchanges",4);
                displayCompositeItem ("JBIShowExchangesStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_QUEUED_EXCHANGES_MAX,
                                      "JBINumberOfQueuedExchangesMax",4);
                displayCompositeItem ("JBIShowExchangesStatisticsHeader",
                                      compData,
                                      JBIStatisticsItemNames.NUMBER_OF_ERROR_EXCHANGES,
                                      "JBINumberOfErrorExchanges",4);
                removeOptionHeaderInfo ("JBIShowExchangesStatisticsHeader");

                //displayOptionHeader("JBIShowMessageExchangeHeader",4);
                saveOptionHeaderInfo ("JBIShowMessageExchangeHeader",4);
                displayCompositeItem ("JBIShowMessageExchangeHeader",
                                      compData,
                                      JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME,
                                      "JBIStatsComponentsMsgExchangeResponseTimeAvg",4);
                displayCompositeItem ("JBIShowMessageExchangeHeader",
                                      compData,
                                      JBIStatisticsItemNames.MESSAGE_EXCHANGE_STATUS_TIME,
                                      "JBIStatsComponentsMsgExchangeStatusTimeAvg",4);
                displayCompositeItem ("JBIShowMessageExchangeHeader",
                                      compData,
                                      JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME,
                                      "JBIStatsComponentsMsgExchangeHttpBindingTimeAvg",4);
                displayCompositeItem ("JBIShowMessageExchangeHeader",
                                      compData,
                                      JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME,
                                      "JBIStatsComponentsMsgExchangeDeliveryChannelTimeAvg",4);
                displayCompositeItem ("JBIShowMessageExchangeHeader",
                                      compData,
                                      JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME,
                                      "JBIStatsComponentsMsgExchangeMessageServiceTimeAvg",4);

                // If no message exchange value was displayed, then display the header
                // and the appropriate text message
                if (mOptionHeaderInfo.containsKey("JBIShowMessageExchangeHeader")) {
                    displayOptionHeader("JBIShowMessageExchangeHeader",4);
                    String value = getRuntimeConfigurationValue(instanceName,
                                                               "msgSvcTimingStatisticsEnabled");
                    boolean flag = Boolean.valueOf(value).booleanValue(); 
                    if (flag) {
                        msg = getLocalizedString ("JBINoMessageExchangeInfoTrueFlag");
                        displayMessage (msg,4);
                    }
                    else {
                        msg = getLocalizedString ("JBINoMessageExchangeInfoFalseFlag1");
                        displayMessage (msg,4);
                        msg = getLocalizedString ("JBINoMessageExchangeInfoFalseFlag2");
                        displayMessage (msg,4);
                    }
                }
                removeOptionHeaderInfo ("JBIShowMessageExchangeHeader");

                if (compItemSet.contains(JBIStatisticsItemNames.COMPONENT_EXTENSION_STATS))
                {
                    displayOptionHeader("JBIShowComponentProvidedStatisticsHeader",4);
                    CompositeData  componentExtensionStats = (CompositeData) compData.get(
                                                                JBIStatisticsItemNames.COMPONENT_EXTENSION_STATS);
                    if (componentExtensionStats != null)
                    {
                        printCompositeData(componentExtensionStats,0,4,4);
                    }
                }
            }
            displayMessage (" ",0);
        }
    }


    /**
     * Will process the Framework TabularData object to display the Framework
     * information to the screen.
     * @param data The TabularData containing the CompositData to display
     */
    protected void processFrameworkResults (TabularData data) throws CommandException
    {
        if ((data == null) || (data.size() == 0) || (data.isEmpty())) {
            throw new CommandException(
                getLocalizedString ("JBIErrorRetrievingFrameworkStats",new Object[] {}));
        }
        else 
        {
            String msg = "";
            Iterator iterator = data.values().iterator();
            while (iterator.hasNext()) {

                CompositeData compData = (CompositeData)iterator.next();
                CompositeType compType = compData.getCompositeType();
                Set compItemSet        = compType.keySet();

                displayOptionHeader("JBIFrameworkStatisticsHeader",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.INSTANCE_NAME,
                                      "JBIStatsFrameworkInstanceLabel",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.FRAMEWORK_STARTUP_TIME,
                                      "JBIStatsFrameworkStartupTime",0);

                displayCompositeUpTime (compData,
                                        JBIStatisticsItemNames.FRAMEWORK_UPTIME,
                                        "JBIStatsFrameworkUpTime",0);
            }
            displayMessage (" ",0);
        }
    }


    /**
     * Will process the Endpoint TabularData object to display the Endpoint
     * information to the screen.
     * @param data The TabularData containing the CompositData to display
     * @param endpointName The Endpoint name
     */
    protected void processEndpointResults (TabularData data, String endpointName) throws CommandException,
                                                                                         JBIRemoteException
    {
        if ((data == null) || (data.size() == 0) || (data.isEmpty())) {
            throw new CommandException(
                getLocalizedString ("JBIErrorRetrievingEndpointStats",new Object[] {}));
        }
        else 
        {
            String msg = "";
            Iterator iterator = data.values().iterator();
            while (iterator.hasNext()) {

                CompositeData compData = (CompositeData)iterator.next();
                CompositeType compType = compData.getCompositeType();
                Set compItemSet        = compType.keySet();

                displayOptionHeader("JBIEndpointStatisticsHeader",0);

                msg = getLocalizedString ("JBIStatsEndpointName",new Object[] {endpointName});
                displayMessage (msg,0);

                String instanceName = (String) compData.get(JBIStatisticsItemNames.INSTANCE_NAME);

                // Display the Endpoint Type (Provider or Consumer)
                String compTypeName = compData.getCompositeType().getTypeName();
                if (compTypeName.compareTo(JBICommandConstants.PROVIDER_ENDPOINT_TYPE_NAME) == 0)
                    msg = getLocalizedString ("JBIStatsEndpointProviderType");
                else if (compTypeName.compareTo(JBICommandConstants.COMSUMER_ENDPOINT_TYPE_NAME) == 0)
                    msg = getLocalizedString ("JBIStatsEndpointConsumerType");
                displayMessage (msg,0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.INSTANCE_NAME,
                                      "JBIStatsEndpointInstanceName",0);

                // Providing Endpoint
                if (compTypeName.compareTo(JBICommandConstants.PROVIDER_ENDPOINT_TYPE_NAME) == 0)
                {
                    //displayOptionHeader("JBIShowTimeStatisticsHeader",4);
                    saveOptionHeaderInfo ("JBIShowTimeStatisticsHeader",4);
                    displayCompositeItem ("JBIShowTimeStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.PROVIDER_ENDPOINT_ACTIVATION_TIME,
                                          "JBIStatsEndpointActivationTime",4);
                    displayCompositeUpTime ("JBIShowTimeStatisticsHeader",
                                            compData,
                                            JBIStatisticsItemNames.PROVIDER_ENDPOINT_UPTIME,
                                            "JBIStatsEndpointUpTime",4);
                    removeOptionHeaderInfo ("JBIShowTimeStatisticsHeader");

                    //displayOptionHeader("JBIShowReceivedStatisticsHeader",4);
                    saveOptionHeaderInfo ("JBIShowReceivedStatisticsHeader",4);
                    displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REQUESTS,
                                          "JBIStatsEndpointNumReceivedRequestsProvider",4);
                    displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES,
                                          "JBIStatsEndpointNumReceivedDonesProvider",4);
                    displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS,
                                          "JBIStatsEndpointNumReceivedFaultsProvider",4);
                    displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS,
                                          "JBIStatsEndpointNumReceivedErrorsProvider",4);
                    removeOptionHeaderInfo ("JBIShowReceivedStatisticsHeader");

                    //displayOptionHeader("JBIShowSentStatisticsHeader",4);
                    saveOptionHeaderInfo ("JBIShowSentStatisticsHeader",4);
                    displayCompositeItem ("JBIShowSentStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_SENT_REPLIES,
                                          "JBIStatsEndpointSentRepliesProvider",4);
                    displayCompositeItem ("JBIShowSentStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_SENT_DONES,
                                          "JBIStatsEndpointNumSentDonesProvider",4);
                    displayCompositeItem ("JBIShowSentStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS,
                                          "JBIStatsEndpointNumSentFaultsProvider",4);
                    displayCompositeItem ("JBIShowSentStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS,
                                          "JBIStatsEndpointNumSentErrorsProvider",4);
                    removeOptionHeaderInfo ("JBIShowSentStatisticsHeader");

                    //displayOptionHeader("JBIShowMessageExchangeHeader",4);
                    saveOptionHeaderInfo ("JBIShowMessageExchangeHeader",4);
                    displayCompositeItem ("JBIShowMessageExchangeHeader",
                                          compData,
                                          JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME,
                                          "JBIStatsEndpointMEResponseTimeAvg",4);
                    displayCompositeItem ("JBIShowMessageExchangeHeader",
                                          compData,
                                          JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME,
                                          "JBIStatsEndpointMEComponentTimeAvg",4);
                    displayCompositeItem ("JBIShowMessageExchangeHeader",
                                          compData,
                                          JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME,
                                          "JBIStatsEndpointMEDeliveryChannelTimeAvg",4);
                    displayCompositeItem ("JBIShowMessageExchangeHeader",
                                          compData,
                                          JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME,
                                          "JBIStatsEndpointMEMessageServiceTimeAvg",4);

                    // If no message exchange value was displayed, then display the header
                    // and the appropriate text message
                    if (mOptionHeaderInfo.containsKey("JBIShowMessageExchangeHeader")) {
                        displayOptionHeader("JBIShowMessageExchangeHeader",4);
                        String value = getRuntimeConfigurationValue(instanceName,
                                                                   "msgSvcTimingStatisticsEnabled");
                        boolean flag = Boolean.valueOf(value).booleanValue(); 
                        if (flag) {
                            msg = getLocalizedString ("JBINoMessageExchangeInfoTrueFlag");
                            displayMessage (msg,4);
                        }
                        else {
                            msg = getLocalizedString ("JBINoMessageExchangeInfoFalseFlag1");
                            displayMessage (msg,4);
                            msg = getLocalizedString ("JBINoMessageExchangeInfoFalseFlag2");
                            displayMessage (msg,4);
                        }
                    }
                    removeOptionHeaderInfo ("JBIShowMessageExchangeHeader");
                }

                // Consuming Endpoint
                else
                {
                    //displayOptionHeader("JBIShowReceivedStatisticsHeader",4);
                    saveOptionHeaderInfo ("JBIShowReceivedStatisticsHeader",4);
                    displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REPLIES,
                                          "JBIStatsEndpointNumReceivedRepliesConsumer",4);
                    displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES,
                                          "JBIStatsEndpointNumReceivedDonesConsumer",4);
                    displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS,
                                          "JBIStatsEndpointNumReceivedFaultsConsumer",4);
                    displayCompositeItem ("JBIShowReceivedStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS,
                                          "JBIStatsEndpointNumReceivedErrorsConsumer",4);
                    removeOptionHeaderInfo ("JBIShowReceivedStatisticsHeader");

                    //displayOptionHeader("JBIShowSentStatisticsHeader",4);
                    saveOptionHeaderInfo ("JBIShowSentStatisticsHeader",4);
                    displayCompositeItem ("JBIShowSentStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_SENT_REQUESTS,
                                          "JBIStatsEndpointSentRequestConsumer",4);
                    displayCompositeItem ("JBIShowSentStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_SENT_DONES,
                                          "JBIStatsEndpointNumSentDonesConsumer",4);
                    displayCompositeItem ("JBIShowSentStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS,
                                          "JBIStatsEndpointNumSentFaultsConsumer",4);
                    displayCompositeItem ("JBIShowSentStatisticsHeader",
                                          compData,
                                          JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS,
                                          "JBIStatsEndpointNumSentErrorsConsumer",4);
                    removeOptionHeaderInfo ("JBIShowSentStatisticsHeader");

                    //displayOptionHeader("JBIShowMessageExchangeHeader",4);
                    saveOptionHeaderInfo ("JBIShowMessageExchangeHeader",4);
                    displayCompositeItem ("JBIShowMessageExchangeHeader",
                                          compData,
                                          JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME,
                                          "JBIStatsEndpointMEComponentTimeAvg",4);
                    displayCompositeItem ("JBIShowMessageExchangeHeader",
                                          compData,
                                          JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME,
                                          "JBIStatsEndpointMEDeliveryChannelTimeAvg",4);
                    displayCompositeItem ("JBIShowMessageExchangeHeader",
                                          compData,
                                          JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME,
                                          "JBIStatsEndpointMEMessageServiceTimeAvg",4);
                    displayCompositeItem ("JBIShowMessageExchangeHeader",
                                          compData,
                                          JBIStatisticsItemNames.CONSUMING_ENDPOINT_STATUS_TIME,
                                          "JBIStatsEndpointMEMessageExStatusTimeAvg",4);
                    // If no message exchange value was displayed, then display the header
                    // and the appropriate text message
                    if (mOptionHeaderInfo.containsKey("JBIShowMessageExchangeHeader")) {
                        displayOptionHeader("JBIShowMessageExchangeHeader",4);
                        String value = getRuntimeConfigurationValue(instanceName,
                                                                   "msgSvcTimingStatisticsEnabled");
                        boolean flag = Boolean.valueOf(value).booleanValue(); 
                        if (flag) {
                            msg = getLocalizedString ("JBINoMessageExchangeInfoTrueFlag");
                            displayMessage (msg,4);
                        }
                        else {
                            msg = getLocalizedString ("JBINoMessageExchangeInfoFalseFlag1");
                            displayMessage (msg,4);
                            msg = getLocalizedString ("JBINoMessageExchangeInfoFalseFlag2");
                            displayMessage (msg,4);
                        }
                    }
                    removeOptionHeaderInfo ("JBIShowMessageExchangeHeader");

                }

                if (compItemSet.contains(JBIStatisticsItemNames.OJC_PERFORMANCE_STATS))
                {
                    TabularData pfTbData = (TabularData)compData.get(JBIStatisticsItemNames.OJC_PERFORMANCE_STATS);
                    int loopCount = 0;
                    for (Iterator pfTbIter=pfTbData.values().iterator(); pfTbIter.hasNext(); )
                    {
                        if (loopCount == 0) {
                            displayOptionHeader("JBIShowPerformanceInstrumentationHeader",4);
                        }
                        else {
                            displayMessage (" ",0);
                        }
                        CompositeData pfCompData = (CompositeData) pfTbIter.next();
                        printCompositeData(pfCompData,0,4,4);
                        loopCount++;
                    }
                }
            }
            displayMessage (" ",0);
        }
    }


    /**
     * Will process the Service Assembly TabularData object to display the Service
     * Assembly information to the screen.
     * @param data The TabularData containing the CompositData to display
     * @param saName The Service Assembly name
     */
    protected void processServiceAssemblyResults (TabularData data, String saName) throws CommandException
    {
        if ((data == null) || (data.size() == 0) || (data.isEmpty())) {
            throw new CommandException(
                getLocalizedString ("JBIErrorRetrievingServiceAssemblyStats",new Object[] {}));
        }
        else 
        {
            String msg = "";
            Iterator iterator = data.values().iterator();
            while (iterator.hasNext()) {

                CompositeData compData = (CompositeData)iterator.next();
                CompositeType compType = compData.getCompositeType();
                Set compItemSet        = compType.keySet();

                displayOptionHeader("JBIServiceAssemblyStatisticsHeader",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.SERVICE_ASSEMBLY_NAME,
                                      "JBIStatsServiceAssemblyName",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.INSTANCE_NAME,
                                      "JBIStatsServiceAssemblyInstanceName",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.SERVICE_ASSEMBLY_LAST_STARTUP_TIME,
                                      "JBIStatsServiceAssemblyLastStartupTime",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.SERVICE_ASSEMBLY_STARTUP_TIME,
                                      "JBIStatsServiceAssemblyStartupTimeAvg",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.SERVICE_ASSEMBLY_STOP_TIME,
                                      "JBIStatsServiceAssemblyStopTimeAvg",0);

                displayCompositeItem (compData,
                                      JBIStatisticsItemNames.SERVICE_ASSEMBLY_SHUTDOWN_TIME,
                                      "JBIStatsServiceAssemblyShutdownTimeAvg",0);

                if (compItemSet.contains(JBIStatisticsItemNames.SERVICE_ASSEMBLY_SU_STATISTICS))
                {
                    CompositeData[] suStats = (CompositeData[]) compData.
                        get(JBIStatisticsItemNames.SERVICE_ASSEMBLY_SU_STATISTICS);

                    for (int i=0; i < suStats.length; ++i)
                    {
                        CompositeData suCompData = suStats[i];
                        CompositeType suCompType = suCompData.getCompositeType();
                        Set suCompItemSet        = suCompType.keySet();

                        displayOptionHeader("JBIServiceUnitStatisticsHeader",4);

                        displayCompositeItem (suCompData,
                                              JBIStatisticsItemNames.SERVICE_UNIT_NAME,
                                              "JBIStatsServiceUnitName",4);

                        displayCompositeItem (suCompData,
                                              JBIStatisticsItemNames.SERVICE_UNIT_STARTUP_TIME,
                                              "JBIStatsServiceUnitStartupTimeAvg",4);

                        displayCompositeItem (suCompData,
                                              JBIStatisticsItemNames.SERVICE_UNIT_STOP_TIME,
                                              "JBIStatsServiceUnitStopTimeAvg",4);

                        displayCompositeItem (suCompData,
                                              JBIStatisticsItemNames.SERVICE_UNIT_SHUTDOWN_TIME,
                                              "JBIStatsServiceUnitShutdownTimeAvg",4);
                    }
                }
            }
            displayMessage (" ",0);
        }
    }


    /**
     * Will process the Verify CompositData results.
     * @param compData The CompositeData
     */
    protected void processVerifyResults (CompositeData compData) throws CommandException
    {
        if (compData == null) {
            throw new CommandException(
                getLocalizedString ("JBIErrorProcessingVerifyResults",new Object[] {}));
        }
        else 
        {
            CompositeType compType = compData.getCompositeType();
            Set compItemSet        = compType.keySet();

            displayOptionHeader("JBIVerifyApplicationHeader",0);

            displayCompositeItem (compData,
                                  "ServiceAssemblyName",
                                  "JBIVerifyServiceAssemblyName",0);

            displayCompositeItem (compData,
                                  "ServiceAssemblyDescription",
                                  "JBIVerifyServiceAssemblyDescription",0);

            displayCompositeItem (compData,
                                  "NumServiceUnits",
                                  "JBIVerifyNumberOfServiceUnits",0);

            displayCompositeItem (compData,
                                  "AllComponentsInstalled",
                                  "JBIVerifyAllComponentsInstalled",0);

            displayCompositeItem (compData,
                                  "TemplateZIPID",
                                  "JBIVerifyTemplateZiped",0);

            // Display the components that are missing or have not been installed yet
            boolean allComponentsInstalled = ((Boolean) compData.get("AllComponentsInstalled")).booleanValue();
            if (!allComponentsInstalled)
            {
                String[] missingComponents = (String[]) compData.get("MissingComponentsList");
                displayOptionHeader("JBIVerifyMissingComponentsHeader",4);
                for (int i=0 ; i < missingComponents.length; i++)
                {
                    String msg = getLocalizedString ("JBIVerifyMissingComponentName", new Object[] {missingComponents[i]});
                    displayMessage (msg,4);
                }
            }

            if (compItemSet.contains("EndpointInfo"))
            {
                CompositeData[] endpointInfo = (CompositeData[]) compData.get("EndpointInfo");

                for (int i=0; i<endpointInfo.length; i++)
                {
                    displayOptionHeader("JBIVerifyEndpointConfigHeader",4);
                    displayCompositeItem (endpointInfo[i],
                                          "EndpointName",
                                          "JBIVerifyEndpointName",4);
                    displayCompositeItem (endpointInfo[i],
                                          "ServiceUnitName",
                                          "JBIVerifyServiceUnitName",4);
                    displayCompositeItem (endpointInfo[i],
                                          "ComponentName",
                                          "JBIVerifyComponentName",4);
                    displayCompositeItemLocalizeValue (endpointInfo[i],
                                          "Status",
                                          "JBIVerifyStatus",4);

                    String[] appVars = (String[])getCompositeItem (endpointInfo[i],"MissingApplicationVariables");
                    if (appVars != null && appVars.length > 0)
                    {
                        displayOptionHeader("JBIVerifyMissingAppVarHeader",8);

                        for (int j=0; j<appVars.length; j++)
                        {
                            displayMessage (appVars[j],8);
                        }
                    }

                    String[] appConfigs = (String[])getCompositeItem (endpointInfo[i],"MissingApplicationConfigurations");
                    if (appConfigs != null && appConfigs.length > 0)
                    {
                        displayOptionHeader("JBIVerifyMissingAppConfigHeader",8);
                        for (int j=0; j<appConfigs.length; j++)
                        {
                            displayMessage (appConfigs[j],8);
                        }
                    }
                }
            }

            if (compItemSet.contains("JavaEEVerificationReport"))
            {
                CompositeData[] javaEEVerifierReports = (CompositeData[]) compData.get("JavaEEVerificationReport");
                
                if ((javaEEVerifierReports != null) && (javaEEVerifierReports.length > 0))
                {
                    displayOptionHeader("JBIVerifyVerificationReportHeader",4);

                    for (int i=0; i<javaEEVerifierReports.length; i++)
                    {
                        String suName = (String)getCompositeItem (javaEEVerifierReports[i],"ServiceUnitName");
                        displayOptionHeader("JBIVerifyVerificationSUHeader",suName,8);
                        TabularData reportTable = (TabularData) javaEEVerifierReports[i].get("JavaEEVerifierReport");
                        Set rows = reportTable.keySet();
                        int blockCount = 0;
                        for (Object row : rows)
                        {
                            if (blockCount > 0) {
                                displayMessage (" ",0);
                            }
                            blockCount++;
                            Object[] key = ( (java.util.List) row).toArray();
                            CompositeData suCompData = reportTable.get(key);
                            
                            //the javaeeverifier report will not contain complex types
                            //parse the report and do not display items starting with "_" 
                            //which are machine readable non-i18ned items
                            if (suCompData !=null)
                            {
                                CompositeType suCompType = suCompData.getCompositeType();
                                Set suCompItemSet = suCompType.keySet();
                                Iterator itr = suCompItemSet.iterator();
                                int maxKeyLength = getMaxKeyLength(suCompItemSet,suCompType);
                                
                                while (itr.hasNext())
                                {          
                                    String keyName = (String) itr.next();
                                    if (!keyName.startsWith(JBIVerifierReportItemNames.NON_I18N_KEY_PREFIX))
                                    {
                                        String msg = addTokenSeperator(keyName,": ",maxKeyLength) + suCompData.get(keyName);
                                        displayMessage(msg,8);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            displayMessage (" ",0);
        }
    }
 
    
    /**
     * Utility method that will determine the max width of the key field (col).
     * @param aSet The set containing the key value pairs that will be displayed.
     * @return the max length for the key field
     */
    protected int getMaxKeyLength (Set aSet, CompositeType compType) {
        Iterator itr = aSet.iterator();
        int maxSize = 0;
        while (itr.hasNext()) {
            String keyName = (String) itr.next();
            OpenType openType = compType.getType(keyName);
            if (!(openType.isArray())) {
                if (keyName.length() > maxSize) {
                    maxSize = keyName.length();
                }
            }
        }
        return maxSize;
    }


    /**
     * Utility method that will add the token seperator and the necessary
     * alignment spaces to the output string.
     * @param key The Key string (value displayed to the left of the seperator)
     * @param seperator The seperator to display between the key and the value
     * @param maxLength The length of the left column (where in string to place value str)
     * @return the formatted output string
     */
    private String addTokenSeperator (String key,
                                      String seperator,
                                      int maxLength)
    {
        int keyLength = key.length();
        int numSpaces = maxLength - keyLength;
        String keyString = key + createFillString(' ',numSpaces) + seperator;
        return keyString;
    }


    /**
     * Will parse and print the Report about the composite data.
     * @param compData The CompositeData
     * @param indention The amount to indent the string
     * @param tabsize The tabsize
     */
    protected void printCompositeData (TabularData statsReport, 
                                    int indention, 
                                    int tabsize)
    {
        Set names = statsReport.keySet();
        for ( Object name : names )
        {
            Object[] key = ( (java.util.List) name).toArray();
            CompositeData compData = statsReport.get(key);
            printCompositeData(compData,1,indention,tabsize);
        }
    }        


    /**
     * Will parse and print the Report about the composite data.
     * @param compData The CompositeData
     * @param recursiveLevel Level of recursion
     * @param indention The amount to indent the string
     * @param tabsize The tabsize (indention for each resursion level)
     */
    protected void printCompositeData (CompositeData compData, 
                                       int recursiveLevel, 
                                       int indention, 
                                       int tabsize)
    {

        int indentAmount = indention + (tabsize * recursiveLevel);
        recursiveLevel++;

        Hashtable arrayHashTable_ = new Hashtable();

        CompositeType compType = compData.getCompositeType();
        Set compItemSet = compType.keySet();
        Iterator itr = compItemSet.iterator();
        int maxKeyLength = getMaxKeyLength(compItemSet,compType);

        while (itr.hasNext())
        {
            String keyName = (String) itr.next();
            OpenType openType = compType.getType(keyName);
            Object itemValue = compData.get(keyName);
            String className = null;
            if (itemValue != null)
            {
                className = itemValue.getClass().getName();
            }
            
            if (openType.isArray())
            {
                if ((openType.getTypeName() != null) &&
                    (openType.getTypeName().compareTo("[Ljava.lang.String;") == 0))
                {
                    // Save the array info so all the arrays will be display at the
                    // end and will be displayed in a list format.
                    String [] strArray = (String []) compData.get(keyName);
                    ArrayList arrayList = new ArrayList();
                    for (int i=0; i < strArray.length; ++i)
                    {   arrayList.add(strArray[i]);
                    }
                    arrayHashTable_.put(keyName,arrayList);

                    // The following is the old way of displaying the output.  This way displayed
                    // the arrays like arrayName[#]: value, where as the new way above, lists
                    // the array name as a header and each entey is listed below the header.
                    // for (int i=0; i < strArray.length; ++i)
                    // {
                    //     String msg = keyName + "[" + i + "]: " + strArray[i];
                    //     displayMessage (msg,indentAmount);
                    // }
                }
                else if ((openType.getTypeName() != null) &&       
                        (openType.getTypeName().compareTo("[Ljavax.management.openmbean.CompositeData;") == 0))
                {
                    CompositeData [] compDataArray = (CompositeData []) compData.get(keyName);
                    for (int i=0; i < compDataArray.length; ++i)
                    {
                        printCompositeData(compDataArray[i], recursiveLevel, indention, tabsize);
                    }
                }
            }

            else
            {
                if (className != null &&
                    (className.equals("javax.management.openmbean.CompositeDataSupport") || 
                    className.equals("javax.management.openmbean.CompositeData")))
                {
                    printCompositeData((CompositeData) compData.get(keyName), recursiveLevel, indention, tabsize);
                }
                else if (className != null &&
                    (className.equals("javax.management.openmbean.TabularDataSupport") || 
                    className.equals("javax.management.openmbean.TabularData")))
                {
                    printCompositeData((TabularData)compData.get(keyName),indention,tabsize);
                }
                else
                {
                    String msg = addTokenSeperator(keyName,": ",maxKeyLength) + compData.get(keyName);
                    displayMessage (msg,indentAmount);
                }
            }


        }

        for (Enumeration e = arrayHashTable_.keys(); e.hasMoreElements();) {
            String arrayListName = (String)e.nextElement();
            displayHeader (arrayListName,indentAmount+4);
            ArrayList list = (ArrayList)arrayHashTable_.get(arrayListName);
            for (int i=0; i<list.size(); i++) {
                String arrayValue = (String)list.get(i);
                String msg = i + ": " + arrayValue;
                displayMessage (msg,indentAmount+4);
            }
        }
    }        
    

    protected void displayHeader (String headerString, int indention) {
        displayMessage (" ",0);
        displayMessage (headerString,indention);
        String underline = createFillString('-',headerString.length());
        displayMessage (underline,indention);
    }


    /**
     * Will extract the header using the given bundleKey and display it to the
     * screen.
     * @param bundleKey The property key value
     * @param indention The amount to indent the string
     */
    protected void displayOptionHeader (String bundleKey, int indention)
    {
        String msg = getLocalizedString (bundleKey);
        displayHeader (msg,indention);
    }

    
    private void saveOptionHeaderInfo (String headerKey, int indention)
    {
        Hashtable headerInfo = new Hashtable();
        headerInfo.put ("headerKey",headerKey);
        headerInfo.put ("indention",new Integer(indention));
        mOptionHeaderInfo.put (headerKey,headerInfo);
    }


    private void removeOptionHeaderInfo (String key)
    {
        mOptionHeaderInfo.remove(key);
    }


    private Hashtable getOptionHeaderInfo (String key)
    {
        Hashtable headerInfo = (Hashtable)mOptionHeaderInfo.get(key);
        return headerInfo;
    }
        

    /**
     * Will extract the header using the given bundleKey and display it using
     * the given value String.
     * @param bundleKey The property key value
     * @param valaueStr The string containing the values to use in the output string
     * @param indention The amount to indent the string
     */
    protected void displayOptionHeader (String bundleKey, String valueStr, int indention)
    {
        displayMessage (" ",0);
        String msg = getLocalizedString (bundleKey, new Object[] {valueStr});
        displayMessage (msg,indention);
        String underline = createFillString('-',msg.length());
        displayMessage (underline,indention);
    }


    /**
     * Will extract the object from the given CompositeData using itemKey.  Then
     * it will display the object using it toString() method.
     * @param compData The composite data
     * @param itemKey The key to extract the object from the composite data
     * @param bundleKey The property key value
     * @param indention The amount to indent the string
     */
    protected void displayCompositeItem (CompositeData compData,
                                       String itemKey, 
                                       String bundleKey,
                                       int indention)
    {
        CompositeType compType = compData.getCompositeType();
        Set compItemSet = compType.keySet();
        if (compItemSet.contains(itemKey))
        {
            Object value = (Object) compData.get(itemKey);
            if (value != null)
            {
                String valueStr = value.toString();
                String msg = getLocalizedString (bundleKey, new Object[] {valueStr});
                displayMessage (msg,indention);
            }
        }
    }


    /**
     * Will extract the object from the given CompositeData using itemKey.  Then
     * it will display the object using it toString() method.
     * @param compData The composite data
     * @param itemKey The key to extract the object from the composite data
     * @param bundleKey The property key value
     * @param indention The amount to indent the string
     */
    protected void displayCompositeItemLocalizeValue (CompositeData compData,
                                       String itemKey, 
                                       String bundleKey,
                                       int indention)

    {
        CompositeType compType = compData.getCompositeType();
        Set compItemSet = compType.keySet();
        if (compItemSet.contains(itemKey))
        {
            Object value = (Object) compData.get(itemKey);
            if (value != null)
            {
                String valueStr = value.toString();
                valueStr = getLocalizedString (valueStr);
                String msg = getLocalizedString (bundleKey, new Object[] {valueStr});
                displayMessage (msg,indention);
            }
        }
    }
        

    /**
     * Will extract the object from the given CompositeData using itemKey.  Then
     * it will display the object using it toString() method.
     * @param headerKey The key used to retrieve the header text
     * @param compData The composite data
     * @param itemKey The key to extract the object from the composite data
     * @param bundleKey The property key value
     * @param indention The amount to indent the string
     */
    protected void displayCompositeItem (String headerKey,
                                         CompositeData compData,
                                         String itemKey, 
                                         String bundleKey,
                                         int indention)
    {
        // Check to see if we need to display a header
        Hashtable headerInfo = getOptionHeaderInfo (headerKey);

        CompositeType compType = compData.getCompositeType();
        Set compItemSet = compType.keySet();
        if (compItemSet.contains(itemKey))
        {
            Object value = (Object) compData.get(itemKey);
            if (value != null)
            {
                if (headerInfo != null) {
                    String key = (String)headerInfo.get("headerKey");
                    Integer indent= (Integer)headerInfo.get("indention");
                    displayOptionHeader (key,indent.intValue());
                    removeOptionHeaderInfo (headerKey);
                }

                String valueStr = value.toString();
                String msg = getLocalizedString (bundleKey, new Object[] {valueStr});
                displayMessage (msg,indention);
            }
        }
    }
      

    /**
     * Will extract the object from the given CompositeData using itemKey.
     * @param compData The composite data
     * @param itemKey The key to extract the object from the composite data
     * @return the object retrieved from the Composite Data.
     */
    protected Object getCompositeItem (CompositeData compData,
                                       String itemKey)
    {
        Object value = null;
        CompositeType compType = compData.getCompositeType();
        Set compItemSet = compType.keySet();
        if (compItemSet.contains(itemKey))
        {
            value = (Object) compData.get(itemKey);
        }
        return value;
    }

    /**
     * Will extract the timestamp from the given CompositeData using itemKey.  Then
     * it will display the time stamp using the bundleKey to retrieve the string
     * from the LocalString property file.
     * @param compData The composite data
     * @param itemKey The key to extract the object from the composite data
     * @param bundleKey The property key value
     * @param indention The amount to indent the string
     */
    protected void displayCompositeUpTime (CompositeData compData,
                                           String itemKey, 
                                           String bundleKey,
                                           int indention)
    {
        CompositeType compType = compData.getCompositeType();
        Set compItemSet = compType.keySet();
        if (compItemSet.contains(itemKey))
        {
            Long upTime = (Long) compData.get(itemKey);
            if (upTime != null)
            {
                JBITimeUtil elapsedTime = new JBITimeUtil (upTime.longValue());
                String msg = getLocalizedString (bundleKey,new Object[] {
                    elapsedTime.getDays(),
                    elapsedTime.getHours(),
                    elapsedTime.getMinutes(),
                    elapsedTime.getSeconds()});
                displayMessage (msg,indention);
            }
        }
    }


    /**
     * Will extract the timestamp from the given CompositeData using itemKey.  Then
     * it will display the time stamp using the bundleKey to retrieve the string
     * from the LocalString property file.
     * @param headerKey The key used to retrieve the header text
     * @param compData The composite data
     * @param itemKey The key to extract the object from the composite data
     * @param bundleKey The property key value
     * @param indention The amount to indent the string
     */
    protected void displayCompositeUpTime (String headerKey,
                                           CompositeData compData,
                                           String itemKey, 
                                           String bundleKey,
                                           int indention)
    {
        // Check to see if we need to display a header
        Hashtable headerInfo = getOptionHeaderInfo (headerKey);
        
        CompositeType compType = compData.getCompositeType();
        Set compItemSet = compType.keySet();
        if (compItemSet.contains(itemKey))
        {
            Long upTime = (Long) compData.get(itemKey);
            if (upTime != null)
            {
                if (headerInfo != null) {
                    String key = (String)headerInfo.get("headerKey");
                    Integer indent= (Integer)headerInfo.get("indention");
                    displayOptionHeader (key,indent.intValue());
                    removeOptionHeaderInfo (headerKey);
                }

                JBITimeUtil elapsedTime = new JBITimeUtil (upTime.longValue());
                String msg = getLocalizedString (bundleKey,new Object[] {
                    elapsedTime.getDays(),
                    elapsedTime.getHours(),
                    elapsedTime.getMinutes(),
                    elapsedTime.getSeconds()});
                displayMessage (msg,indention);
            }
        }
    }

    /**
     * Will display the provided message to the screen.
     * @param msg The message to display
     * @param indentAmount The amount to indent the message by
     */
    protected void displayMessage (String msg, int indentAmount)
    {
        if (msg != null)
        {
            String outputString = createFillString(' ',indentAmount) + msg;
            CLILogger.getInstance().printMessage (outputString);
        }
    }


    /**
    * Will display the content of a Map sorted by the keys.  The display
    * format will be key = value, one entry per line.
    * @param map The map to display
    */
    protected void displayMap (Map map, int indentAmount)
    {
        Set <String> keys = map.keySet();
        TreeSet <String> sortedKeys = new TreeSet <String>(keys);
        for (String key : sortedKeys) {
            String outputString = createFillString(' ',indentAmount) + 
                                     key + " = " + map.get(key);
            CLILogger.getInstance().printMessage (outputString);
        }
    }


    /**
    * Will display the content of a Map sorted by the keys.  The display
    * format will be key = value, one entry per line.
    * @param map The map to display
    */
    protected void displayProperties (Properties properties, int indentAmount)
    {
        Set keys = properties.keySet();
        TreeSet <String> sortedKeys = new TreeSet <String>(keys);
        for (String key : sortedKeys) {
            String outputString = createFillString(' ',indentAmount) + 
                                     key + " = " + properties.get(key);
            CLILogger.getInstance().printMessage (outputString);

        }
    }


    /**
     * Will format the description text that is displayed in the show commands.  The
     * formatting will consist of removing all new lines and extra white space, then
     * adding back in line breaks at the first avaliable location that is less then
     * or equal to the given max line length.
     * @param msgText the message string to format
     * @param maxLength the maximum line length size.
     * @param indentAmout the amount to indent row 2 - n
     */
    protected String multiLineFormat (String msgText, int maxLength, int indentAmount)
    {
        // Strip out the leading white spaces in each row, and remove any "\n"
        int endIndex = 0;
        int startIndex = 0;
        String finalString = "";
        String rowString = "";
        String space = "";
        endIndex = msgText.indexOf("\n",startIndex);
        while (endIndex != -1)
        {   
            rowString = msgText.substring(startIndex,endIndex).trim();
            finalString += space + rowString;
            startIndex = endIndex + 1;
            endIndex = msgText.indexOf("\n",startIndex);
            space = " ";
        }
        rowString = msgText.substring(startIndex).trim();
        finalString += space + rowString;

        // Format the string by adding the line breaks in the correct location and adding
        // the indention amount at that beginning of each row.
        endIndex = 0;
        startIndex = 0;
        int spaceIndex = 0;
        int indentSize = 0;
        String newString = "";
        boolean done = false;
        int totalLength = finalString.length();
        while (!(done))
        {
            endIndex = ((startIndex + maxLength) > totalLength) ? totalLength : (startIndex + maxLength);
            rowString = finalString.substring(startIndex,endIndex);
            spaceIndex = startIndex + rowString.lastIndexOf(" ");
            if (endIndex < totalLength)
            {
                spaceIndex = startIndex + rowString.lastIndexOf(" ");
                if (spaceIndex != -1)
                {
                    endIndex = spaceIndex;
                }
            }
            rowString = finalString.substring(startIndex,endIndex) + "\n";
            startIndex = endIndex + 1;
            newString += createFillString(' ',indentSize) + rowString;
            indentSize = indentAmount;
            if (startIndex >= totalLength)
            {
                done = true;
            }
        }
        finalString = newString;
        return finalString;
    }


    /**
     * Will create a string of the size specified filled with the fillChar.
     * @param fillChar the character to create the string with
     * @param the size of the string
     */
    protected String createFillString (char fillChar, int size)
    {
        String fillString = "";
        for (int i=0; i<size; i++)
        {
            fillString += fillChar;
        }
        return fillString;
    }


    /**
     * Will format and display the component (binding or engine) show results.
     * @param result the xml string containing the result information
     * @param aName the name of the component
     */
    protected void processShowComponentResult (String result, String aName) throws CommandException
    {
        List list = JBIComponentInfo.readFromXmlText(result);
        if (list.size() == 0)
        {
            throw new CommandException(
                getLocalizedString ("JBINoComponentToShow",new Object[] {aName}));
        }
        else
        {
            CLILogger.getInstance().printMessage ("");
            String header = getLocalizedString ("JBIComponentShowHeader");
            CLILogger.getInstance().printMessage (header);
            CLILogger.getInstance().printMessage (createFillString('-',header.length()));

            Iterator it = list.iterator();
            mInfo = ((JBIComponentInfo)it.next());
            String componentName        = mInfo.getName();
            String componentState       = mInfo.getLocalizedState();
            String componentVersion     = mInfo.getComponentVersion();
            String componentBuildNumber = mInfo.getBuildNumber();
            String componentDescription = mInfo.getDescription();
            String formattedDescription = multiLineFormat(componentDescription,70,14);
            formattedDescription = formattedDescription.trim();
            CLILogger.getInstance().printMessage (
                    getLocalizedString ("JBIComponentName",new Object[] {componentName}));
            CLILogger.getInstance().printMessage (
                    getLocalizedString ("JBIComponentState",new Object[] {componentState}));
            if ((componentVersion != null) && (componentVersion.compareTo("") != 0)) {
                CLILogger.getInstance().printMessage (
                        getLocalizedString ("JBIComponentVersion",new Object[] {componentVersion}));
            }
            if ((componentBuildNumber != null) && (componentBuildNumber.compareTo("") != 0)) {
                CLILogger.getInstance().printMessage (
                        getLocalizedString ("JBIComponentBuildNumber",new Object[] {componentBuildNumber}));
            }
            CLILogger.getInstance().printMessage (
                    getLocalizedString ("JBIComponentDescription",new Object[] {formattedDescription}));




       }
    }


    /**
     * Will format and display the Shared Library show results.
     * @param result the xml string containing the result information
     * @param aName the name of the shared library
     */
    protected void processShowLibraryResult (String result, String aName) throws CommandException
    {
        List list = JBIComponentInfo.readFromXmlText(result);
        if (list.size() == 0)
        {
            throw new CommandException(
                getLocalizedString ("JBINoLibraryToShow",new Object[] {aName}));
        }
        else
        {
            CLILogger.getInstance().printMessage ("");
            String header = getLocalizedString ("JBISharedLibraryShowHeader");
            CLILogger.getInstance().printMessage (header);
            CLILogger.getInstance().printMessage (createFillString('-',header.length()));

            Iterator it = list.iterator();
            JBIComponentInfo info = ((JBIComponentInfo)it.next());
            String libraryName          = info.getName();
            String libraryDescription   = info.getDescription();
            String libraryVersion       = info.getComponentVersion();
            String libraryBuildNumber   = info.getBuildNumber();
            String formattedDescription = multiLineFormat(libraryDescription,50,14);
            CLILogger.getInstance().printMessage (
                    getLocalizedString ("JBISharedLibraryName",new Object[] {libraryName}));
            if ((libraryVersion != null) && (libraryVersion.compareTo("") != 0)) {
                CLILogger.getInstance().printMessage (
                        getLocalizedString ("JBISharedLibraryVersion",new Object[] {libraryVersion}));
            }
            if ((libraryBuildNumber != null) && (libraryBuildNumber.compareTo("") != 0)) {
                CLILogger.getInstance().printMessage (
                        getLocalizedString ("JBISharedLibraryBuildNumber",new Object[] {libraryBuildNumber}));
            }
            CLILogger.getInstance().printMessage (
                    getLocalizedString ("JBISharedLibraryDescription",new Object[] {formattedDescription}));
        }
    }


    /**
     * Will format and display the Service Assembly show results.
     * @param result the xml string containing the result information
     * @param aName the name of the service assembly
     */
    protected void processShowAssemblyResult (String result, String aName) throws CommandException
    {
        List list = ServiceAssemblyInfo.readFromXmlTextWithProlog(result);
        
        if (list.size() <= 0)
        {
            throw new CommandException(
                getLocalizedString ("JBINoServiceAssemblyToShow",new Object[] {aName}));
        }
        else
        {
           Iterator itr = list.iterator();
           ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) itr.next();
           List suInfoList = saInfo.getServiceUnitInfoList();

           String saName = saInfo.getName();
           String saState = saInfo.getLocalizedState();
           String saDescription = saInfo.getDescription();
           String saSize = Integer.toString(suInfoList.size());
           String formattedSADescription = multiLineFormat(saDescription,50,16);
           
           CLILogger.getInstance().printMessage ("");
           String SAHeader = getLocalizedString ("JBIServiceAssemblyShowHeader");
           CLILogger.getInstance().printMessage (SAHeader);
           CLILogger.getInstance().printMessage (createFillString('-',SAHeader.length()));
           CLILogger.getInstance().printMessage (
                   getLocalizedString ("JBIServiceAssemblyName",new Object[] {saName}));
           CLILogger.getInstance().printMessage (
                   getLocalizedString ("JBIServiceAssemblyState",new Object[] {saState}));
           CLILogger.getInstance().printMessage (
                   getLocalizedString ("JBIServiceAssemblyServiceUnits",new Object[] {saSize}));
           CLILogger.getInstance().printMessage (
                   getLocalizedString ("JBIServiceAssemblyDescription",new Object[] {formattedSADescription}));

           String indentString = "    ";
           CLILogger.getInstance().printMessage ("");
           String SUHeader = getLocalizedString ("JBIServiceUnitShowHeader");
           CLILogger.getInstance().printMessage (indentString + SUHeader);
           CLILogger.getInstance().printMessage (indentString + createFillString('-',SUHeader.length()));
           boolean firstTime = true;
           for (Iterator suItr = suInfoList.iterator(); suItr.hasNext();)
           {
               ServiceUnitInfo suInfo = (ServiceUnitInfo ) suItr.next();
               String suState = suInfo.getLocalizedState();
               String suDepoyedOn = suInfo.getDeployedOn();
               String suName = suInfo.getName();
               String suDescription = suInfo.getDescription();
               String formattedSUDescription = multiLineFormat(suDescription,50,18);
               if (!(firstTime))
               {
                   CLILogger.getInstance().printMessage ("");
               }
               CLILogger.getInstance().printMessage (indentString +
                       getLocalizedString ("JBIServiceUnitName",new Object[] {suName}));
               CLILogger.getInstance().printMessage (indentString +
                       getLocalizedString ("JBIServiceUnitState",new Object[] {suState}));
               CLILogger.getInstance().printMessage (indentString +
                       getLocalizedString ("JBIServiceUnitDeployedOn",new Object[] {suDepoyedOn}));
               CLILogger.getInstance().printMessage (indentString +
                       getLocalizedString ("JBIServiceUnitDescription",new Object[] {formattedSUDescription}));
               firstTime = false;
           }
        }
    }

    /**
     * Will process the list results for the components (Service Engines, 
     * Binding Components and Shared Libraries).
     * was specified, the default value will be returned.
     * @param result The result xml string
     */
    protected void processJBIAdminComponentListResult (String result) 
    {
        List list = JBIComponentInfo.readFromXmlText(result);
        if (list.size() == 0)
        {
            CLILogger.getInstance().printDetailMessage (
                    getLocalizedString ("NoElementsToList",new Object[] {result}));
        }
        else
        {
            Iterator it = list.iterator();
            String listBreak = "";
            int count = 0;
            while (it.hasNext())
            {
                JBIComponentInfo info = ((JBIComponentInfo)it.next());
                String componentName = info.getName();
                CLILogger.getInstance().printMessage (componentName);

                // TBD append global state info to end of name
                //String componentName = info.getName();
                //String state = info.getState();
                //String outputString = componentName + " : " + state;
                //CLILogger.getInstance().printDetailMessage (outputString);
            }
        }
    }


    /**
     * Will process the list results for the Service Assemblies
     * @param result The result xml string
     */
    protected void processJBIAdminAsseblyListResult (String result) 
    {
        List list = ServiceAssemblyInfo.readFromXmlTextWithProlog(result);
        if (list.size() == 0)
        {
            CLILogger.getInstance().printDetailMessage (
                    getLocalizedString ("NoElementsToList",new Object[] {result}));
        }
        else
        {
            Iterator it = list.iterator();
            String listBreak = "";
            int count = 0;
            while (it.hasNext())
            {
                ServiceAssemblyInfo info = ((ServiceAssemblyInfo)it.next());
                String assemblyName = info.getName();
                CLILogger.getInstance().printMessage (assemblyName);

                // TBD append global state info to end of name
                //String assemblyName = info.getName();
                //String state = info.getState();
                //String outputString = assemblyName + " : " + state;
                //CLILogger.getInstance().printDetailMessage (outputString);
            }
        }
    }

    
    /**
     * Will scan through the connection properties file and replace the ant property
     * keys with ones that are used by asadmin.
     * @param fileName the connection properties file
     * @throws IOException
     */
    protected void updateConectionPropertiesFile (String fileName) throws IOException
    {
        // Make sure the file specified is the absolute path.
        File inputFile = new File(fileName);
        String absolutePath = inputFile.getAbsolutePath();

        // Create a tempory file that will be renamed later
        String outputFileName = absolutePath + ".tmp";

        // Create FileReader Object
        FileReader inputFileReader   = new FileReader(fileName);
        FileWriter outputFileReader  = new FileWriter(outputFileName);

        // Create Buffered/PrintWriter Objects
        BufferedReader inputStream   = new BufferedReader(inputFileReader);
        PrintWriter    outputStream  = new PrintWriter(outputFileReader);

        // Scan throuch the .properties and replace the ant properties keys
        // with asadmin properties keys.
        String inLine = null;
        Set keySet = mConnectionConversionTbl.keySet();
        while ((inLine = inputStream.readLine()) != null)
        {
            String output = inLine;
            Iterator it = keySet.iterator();
            while (it.hasNext())
            {
                String patternStr = (String)it.next();
                String replacementStr = (String)mConnectionConversionTbl.get(patternStr);
                Pattern pattern = Pattern.compile(patternStr);
                Matcher matcher = pattern.matcher(inLine);
                if (matcher.find())
                {
                    output = matcher.replaceAll(replacementStr);
                    break;
                }
            }
            outputStream.println(output);
        }

        // Close the files
        outputStream.close();
        inputStream.close();

        // Replace the old file with the new updated one.
        inputFile.delete();
        File newFile = new File(absolutePath);
        File outputFile = new File(outputFileName);
        outputFile.renameTo(newFile);
    }


    /**
     * Will convert the template file from an ant syntax style to a asadmin syntax style.
     * @param fileDir The directory containing the files to convert.
     * @throws CommandException
     */
    protected void convertTemplateFilesToAsadmin (String fileDir) throws CommandException
    {
        try {
        
            // First we need to convert the properties file to contain asadmin keys
            String propertiesFile = fileDir + "/" + CONFIG_PROPERTIES_FILENAME;
            updateConectionPropertiesFile (propertiesFile);

            // Parse the xml file and create a hash table containing command info
            String xmlConfigFile = fileDir + "/" + CONFIG_COMMAND_FILENAME_ANT;
            Hashtable commandHashTable = constructCommandHashtable (xmlConfigFile);

            // Create the asadmin configuration file
            String asadminConfigFile = fileDir + "/" + CONFIG_COMMAND_FILENAME_ASADMIN;
            createAsadminConfigFile (commandHashTable,
                                     propertiesFile,
                                     asadminConfigFile);

            // Delete the ant xml file
            File deleteFile = new File(xmlConfigFile);
            deleteFile.delete();

            // Delete the properties file
            File propFile = new File(propertiesFile);
            propFile.delete();
        }

        catch (Exception e) {
            throw new CommandException(getLocalizedString("JBIErrorConfigFileConversion",
                                                    new Object[]{fileDir}));
        }
    }


    /**
     * Utility method that will replace a pattern in a string.
     * @param originalStr The original input string
     * @param patternStr The pattern to replace in the string.
     * @param replacementStr The string to replace the patternStr with.
     */
    protected String replaceString (String originalStr, String patternStr, String replacementStr)
    {
        String outString = originalStr;
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(originalStr);
        if (matcher.find()) {
            String lString = matcher.quoteReplacement(replacementStr);
            outString = matcher.replaceAll(lString);
        }
        return outString;
    }


    /**
     * Using the provided commandHashTable, this method will create the asadmin
     * command file.  This is the file that will contain the asadmin commands.
     * @param commandHashTable The hashtable the contains the commands.
     * @param propertyFile FilePath name of the property file containing the connection info.
     * @param asadminConfigFile Name of the asadmin config file to create.
     * @throws IOException
     */
    protected void createAsadminConfigFile (Hashtable commandHashTable, 
                                         String propertyFile,
                                         String asadminConfigFile) throws IOException
    {        
        // Create the Properties object
        File inputFile = new File(propertyFile);
        String absolutePath = inputFile.getAbsolutePath();
        Properties connectionProps = new Properties();
        FileInputStream in = new FileInputStream(absolutePath);
        connectionProps.load(in);
        in.close();

        // Create the file that will hold the asadmin commands
        String outputFileName = asadminConfigFile;
        FileWriter outputFileReader  = new FileWriter(outputFileName);
        PrintWriter outputStream  = new PrintWriter(outputFileReader);

        // For each item in the connection.properties, create an export statement in
        // the asadmin config file.
        outputStream.println(getLocalizedString ("JBIConfigureFileTextLine1"));
        outputStream.println(getLocalizedString ("JBIConfigureFileTextLine2",new Object[] {asadminConfigFile}));
        outputStream.println(getLocalizedString ("JBIConfigureFileCommentLine"));
        outputStream.println(getLocalizedString ("JBIConfigureFileTextLine3"));
        outputStream.println(getLocalizedString ("JBIConfigureFileCommentLine"));
        Enumeration connectionPropsEnum = connectionProps.propertyNames();
        while (connectionPropsEnum.hasMoreElements ())
        {
            String key  = (String) connectionPropsEnum.nextElement();
            String value= connectionProps.getProperty(key);
            String exportStatement = "export " + key + "=" + value;
            outputStream.println(exportStatement);
        }
        outputStream.println(getLocalizedString ("JBIConfigureFileCommentLine"));
        outputStream.println(getLocalizedString ("JBIConfigureFileCommentLine"));
        outputStream.println(getLocalizedString ("JBIConfigureFileTextLine4"));
        outputStream.println(getLocalizedString ("JBIConfigureFileCommentLine"));

        // Loop through all the commands that are defined in the hashtable
        // Enumeration en = connectionProps.propertyNames ();
        Enumeration en = commandHashTable.keys();
        while (en.hasMoreElements ())
        {
            String antCommand   = (String) en.nextElement();
            String adminCommand = (String) mCommandConversionTbl.get(antCommand);
            ArrayList list = (ArrayList)commandHashTable.get(antCommand);
            for (int j=0; j<list.size(); j++)
            {
                Hashtable cmd = (Hashtable)list.get(j);
                Enumeration e = cmd.keys ();
                while (e.hasMoreElements ())
                {
                    String key = (String) e.nextElement();
                    String value = (String) cmd.get(key);
                    adminCommand = replaceString (adminCommand, key, value);
                }
                outputStream.println(adminCommand);
            }
        }
        outputStream.close();
    }


    /**
     * Using the ant file specified, this method will construct a hashtable that
     * will contain all the commands defined in the ant file.
     * @param filePath The file path to the ant command file.
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    protected Hashtable constructCommandHashtable (String filePath) throws SAXException,
                                                                        IOException,
                                                                        ParserConfigurationException
    {
        Hashtable commandsHashtable = new Hashtable();

        // Retrieve the set of commands to parse from the xml file
        Set commandSet = mCommandConversionTbl.keySet();

        // Create a hashtable entry for each command in the commandSet.  Each
        // entry in the hashtable will hold a list of commands.
        Iterator it = commandSet.iterator();
        while (it.hasNext())
        {
            ArrayList commandArray = new ArrayList();
            commandsHashtable.put(it.next(),commandArray);
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document document = parser.parse(filePath); 

        Node node = document.getDocumentElement();
        NodeList list = node.getChildNodes();

        ArrayList nodeListArray = new ArrayList();
        ArrayList nodeListIndexArray = new ArrayList();
        nodeListArray.add(list);
        nodeListIndexArray.add(new Integer(0));
        node = ((NodeList)nodeListArray.get(0)).item(0);

        int depth = 0;
        int i = 0;
        String commandName = "";
        String key = "";
        String value = "";
        Hashtable commandHash = null;

        while (depth >= 0)
        {
            while (node != null)
            {
                if (node.getNodeType() == node.ELEMENT_NODE)
                {
                    if (node.getNodeName().equalsIgnoreCase("antcall"))
                    {
                        if (commandHash != null)
                        {
                            if (commandHash.size() > 0)
                            {
                                ArrayList cmdArray = (ArrayList)commandsHashtable.get(commandName);
                                cmdArray.add(commandHash);
                            }
                        }
                        commandHash = new Hashtable();
                    }

                    NamedNodeMap nodeMap = node.getAttributes();
                    for (int j=0; j<nodeMap.getLength(); j++)
                    {
                        Node elementNode = nodeMap.item(j);
                        if (elementNode.getNodeName().equalsIgnoreCase("target"))
                        {
                            commandName = elementNode.getNodeValue();
                        } else if (elementNode.getNodeName().equalsIgnoreCase("name"))
                        {
                            key = elementNode.getNodeValue();
                        } else if (elementNode.getNodeName().equalsIgnoreCase("value"))
                        {
                            value = elementNode.getNodeValue();
                            commandHash.put(key,value);
                        }
                    }

                    if (node.hasChildNodes())
                    {
                        list = node.getChildNodes();
                        nodeListArray.add(list);
                        nodeListIndexArray.add(new Integer(0));
                        depth++;
                    }
                }

                i = ((Integer)(nodeListIndexArray.get(depth))).intValue();
                i++;
                nodeListIndexArray.set(depth,i);
                node = ((NodeList)nodeListArray.get(depth)).item(i);
            }
            nodeListIndexArray.remove(depth);
            nodeListArray.remove(depth);
            depth--;
            if (depth >= 0)
            {
                i = ((Integer)(nodeListIndexArray.get(depth))).intValue();
                i++;
                nodeListIndexArray.set(depth,i);
                node = ((NodeList)nodeListArray.get(depth)).item(i);
            }
        }
        if (commandHash != null)
        {
            if (commandHash.size() > 0)
            {
                ArrayList cmdArray = (ArrayList)commandsHashtable.get(commandName);
                cmdArray.add(commandHash);
            }
        }
        return commandsHashtable;
    }


    /**
     * Will first check the properties options for either a property file or 
     * properties specified on the command line.  If no properties option is
     * found, an empty properties object will be returned.
     * @return properties object, will be empty if not properties are found
     * @throws CommandException
     * @throws CommandValidationException
     */
    protected Properties getAnyProperties() throws CommandException, 
                                                   CommandValidationException
    {
        String compProperties = getOption(JBICommandConstants.PROPERTIES_OPTION);
        Properties properties = new Properties();
        if (compProperties != "")
        {
            try
            {
                properties.load(new FileInputStream(compProperties));
            } catch (IOException e)
            {
                properties = createPropertiesParam(compProperties);
            }
        }
        return properties;
    }

    /**
     * Will process the task exception to display the error message.
     * @param ex the exception to process
     * @throws CommandException
     */
    protected void processTaskException (Exception ex) throws CommandException
    {
        JBIManagementMessage mgmtMsg = extractJBIManagementMessage(ex);
        if (mgmtMsg == null)
        {
            displayExceptionMessage(ex);
        }
        else
        {
            // Display the exception message which was returned from the runtime.
            String msg = mgmtMsg.getMessage();
            CLILogger.getInstance().printDetailMessage (msg);
            
            // If the message starts with "WARNING" then display a success message
            if (msg.trim().startsWith("WARNING")) {
                CLILogger.getInstance().printDetailMessage (
                    getLocalizedString ("CommandSuccessful",new Object[] {name}));
            }

            // Else throw a new CommandException so the framework will handle the error
            else {
                throw new CommandException(getLocalizedString("CommandUnSuccessful",
                                                        new Object[]{name}));
            }
        }
    }
    

    /**
     * Will extract the JBIManagementMessgae from the Remote exception.
     * @param ex the exception to process
     */
    protected JBIManagementMessage extractJBIManagementMessage (Exception ex )
    {
        JBIManagementMessage mgmtMsg = null;
        if (ex instanceof JBIRemoteException)
        {
            JBIRemoteException rEx = (JBIRemoteException)ex;
            mgmtMsg = rEx.extractJBIManagementMessage();
        }
        else
        {
            String exMessage = ex.getMessage();
            mgmtMsg = JBIManagementMessage.createJBIManagementMessage(exMessage);
        }
        return mgmtMsg;
    }
    

    /**
     * Will process the list results for the Service Assemblies
     * @param result The result xml string
     */
    protected void processJBIAdminResult (String result, String successKey) throws CommandException
    {
        JBIManagementMessage mgmtMsg = JBIManagementMessage.createJBIManagementMessage(result);
        if (mgmtMsg == null)
        {
            CLILogger.getInstance().printDetailMessage (
                    getLocalizedString (successKey, new Object[] {result}));
        }
        else
        {
            if (mgmtMsg.isSuccessMsg())
            {
                String msg = mgmtMsg.getMessage().trim();
                int index = msg.indexOf(")");
                if (index != -1)
                {
                    msg = msg.substring(index+1);
                }
                CLILogger.getInstance().printDetailMessage (msg);
            }
            else
            {
                String msg = mgmtMsg.getMessage();
                throw new CommandException(msg);
            }
        }
    }


    /**
     * Will process the list results for the Service Assemblies
     * @param result The result xml string
     */
    protected void processJBIAdminResult (String result) throws CommandException
    {
        JBIManagementMessage mgmtMsg = JBIManagementMessage.createJBIManagementMessage(result);
        if (mgmtMsg == null)
        {
            CLILogger.getInstance().printDetailMessage (
                getLocalizedString ("CommandSuccessful",new Object[] {name}));
        }
        else
        {
            if (mgmtMsg.isSuccessMsg())
            {
                String msg = mgmtMsg.getMessage().trim();
                if (msg.length() > 0) {
                    int index = msg.indexOf(")");
                    if (index != -1)
                    {
                        msg = msg.substring(index+1);
                    }
                    CLILogger.getInstance().printDetailMessage (msg);
                }
            }
            else
            {
                String msg = mgmtMsg.getMessage();
                throw new CommandException(msg);
            }
        }
    }


    /**
     * Retrieve the configuration runtime value.
     * @param tgtName
     * @param configName
     * @return String the configuration string.
     */
    private String getRuntimeConfigurationValue (String tgtName, String configName) throws JBIRemoteException
    {
        String configValue = "";
        Properties result = mJBICmds.getRuntimeConfiguration(tgtName);
        configValue = result.getProperty(configName,"");
        return configValue;
    }


    /**
     * Method that will validate that the given file path exists and is not
     * a directory.
     * @param filePath The file path
     * @throws CommandException
     * @return String the absolute file path
     */
    protected String validateFilePath (String filePath) throws CommandException
    {
        return validateFilePath ("FileDoesNotExist",filePath);
    }


    /**
     * Method that will validate that the given file path exists and is not
     * a directory.
     * @param errorKey The property key value used to retrieve the error message text
     * @param filePath The file path
     * @return String the absolute file path
     * @throws CommandException
     */
    protected String validateFilePath (String errorKey, String filePath) throws CommandException
    {
        File file = new File(filePath);
        String absolutePath = file.getAbsolutePath();
        if ((!file.exists()) || (file.isDirectory()))
        {
            throw new CommandException(getLocalizedString(errorKey,
                                                    new Object[]{filePath}));
        }
        return absolutePath;
    }


    /**
     * Perform the pre run initialization.  This will validate the options as
     * well as retrieve the MBean Server Connection.
     * @throws CommandValidationException
     * @throws CommandException
     * @throws JBIRemoteException
     */
    protected boolean preRunInit() throws CommandValidationException, 
                                          CommandException, 
                                          JBIRemoteException
    {
        boolean uploadFlag = false;
        return preRunInit(uploadFlag);
    }


    /**
     * Perform the pre run initialization.  This will validate the options as
     * well as retrieve the MBean Server Connection.
     * @param uploadFlag The upload flag
     * @throws CommandValidationException
     * @throws CommandException
     * @throws JBIRemoteException
     */
    protected boolean preRunInit(boolean uploadFlag) throws CommandValidationException, 
                                                            CommandException, 
                                                            JBIRemoteException
    {
        // Validate the options and opeands
        validateOptions();

        // Retrieve the MBean Server Connection and the JBIAdminCommands object.
        mbsc = getMBeanServerConnection(getHost(), 
                                        getPort(), 
                                        getUser(), 
                                        getPassword());
        
        // Retrieve the JBI Admin Command object
        try {
            mJbiAdminCommands = JBIAdminCommandsClientFactory.getInstance(mbsc,uploadFlag);
        }
        catch (Exception e) {
            displayExceptionMessage(e);
        }

        // Make sure we have a valid command object
        if (mJbiAdminCommands == null)
        {
            throw new CommandException(getLocalizedString("CouldNotInvokeCommand",
                                                    new Object[]{name}));
        }
        return true;
    }

    /**
     * Method that will return the option value or if no option value
     * was specified, the default value will be returned.
     * @param optionName The option name use to retrieve the value
     * @param validOptions An array containing a list of valid options
     * @param defaultValue The default value returned if no option value exists
     * @return The option value
     * @throws CommandValidationException
     */
    protected String getOption(String optionName, String[] validOptions, String defaultValue) throws CommandValidationException
    {
        boolean found = false;
        String option = getOption(optionName);
        if (option == null)
        {
            option = defaultValue;
            found = true;
        }
        else
        {
            for (int i=0; i<validOptions.length; i++)
            {
                if (option.equalsIgnoreCase(validOptions[i]))
                {
                    found = true;
                    break;
                }
            }
        }
        if (found) {
            return option;
        }
        else {
            throw new CommandValidationException(getLocalizedString("InvalidValueInOption",
                                                    new Object[]{optionName}));
        }
    }


    /**
     * Method that will return the option value or if no option value
     * was specified, the default value will be returned.
     * @param optionName The option name use to retrieve the value
     * @param validOptions An array containing a list of valid options
     * @return The option value
     * @throws CommandValidationException
     */
    protected String getOption(String optionName, String[] validOptions) throws CommandValidationException
    {
        String defaultValue = null;
        return getOption(optionName, validOptions, defaultValue);
    }


    /**
     * Retrieve the configuration runtime value.
     * @param inputStr
     * @param patternStr
     * @param replacementStr
     * @return Updated string
     */
    protected String stringReplace (String inputStr, String patternStr, String replacementStr)
    {
        String outputStr = inputStr;
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.find())
        {
            outputStr = matcher.replaceAll(replacementStr);
        }
        return outputStr;
    }


    /**
     * Retrieve the configuration runtime value.
     * @param tgtName
     * @param configName
     * @return String the configuration string.
     */
    protected JBIAdminCommands getJBIAdminCommands()
    {
        return mJbiAdminCommands;
    }

    /**
     * Formulate and Returns Properties from the given string
     * @return Properties
     */
    protected Properties createPropertiesParam(String propertyVal)
        throws CommandException, CommandValidationException
    {
        final String propertyStr = replacePattern(propertyVal);
        if (propertyStr == null) return null;
        Properties properties = new Properties();
        final CLITokenizer propertyTok = new CLITokenizer(propertyStr, ",");
        while (propertyTok.hasMoreTokens()) {
            final String nameAndvalue = propertyTok.nextToken();
            final CLITokenizer nameTok = new CLITokenizer(nameAndvalue, "=");
            if (nameTok.countTokens() == 2)
            {
                properties.setProperty(nameTok.nextTokenWithoutEscapeAndQuoteChars(),
                                       nameTok.nextTokenWithoutEscapeAndQuoteChars());
            }
            else
            {
                throw new CommandValidationException(getLocalizedString("InvalidPropertySyntax"));
            }
        }
        return properties;
    }

}
