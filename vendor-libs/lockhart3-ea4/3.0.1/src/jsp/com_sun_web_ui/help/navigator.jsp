<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@taglib uri="http://www.sun.com/web/ui" prefix="ui" %>

<f:view>
  <ui:page>
    <ui:head title="Help Window Navigator" />      
    <ui:body>
      <ui:form id="helpNavigatorForm">
        <div class="HlpStpTab">
          <ui:tabSet id="javaHelpTabSet" mini="true">
            <ui:tab id="contentsTab" text="#{JavaHelpBean.contentsText}" action="#{JavaHelpBean.contentsTabClicked}" />
            <ui:tab id="indexTab" text="#{JavaHelpBean.indexText}" action="#{JavaHelpBean.indexTabClicked}" />
            <ui:tab id="searchTab" text="#{JavaHelpBean.searchText}" action="#{JavaHelpBean.searchTabClicked}" />
          </ui:tabSet>
        </div><div style="margin-left: 10px;">
        <ui:tree binding="#{JavaHelpBean.contentsTree}" />        
        <ui:tree binding="#{JavaHelpBean.indexTree}" />        
        </div>
        <ui:panelGroup binding="#{JavaHelpBean.searchPanel}">
          <f:verbatim><div class="HlpSchDiv"></f:verbatim>
          <ui:textField id="searchText" />
          <ui:button id="searchButton" text="Search" action="#{JavaHelpBean.doSearch}" />
          <f:verbatim><div class="HlpFldTxt"></f:verbatim>
          <ui:hyperlink binding="#{JavaHelpBean.tipsLink}" />
          <f:verbatim>
            </div><table border="0" cellspacing="0" cellpadding="0" width="98%">
            <tr><td>
          </f:verbatim>
          <ui:image icon="DOT" alt="" border="0" height="5" width="1" />
          <f:verbatim>
            </td></tr><tr><td class="TtlLin">
          </f:verbatim>
          <ui:image icon="DOT" alt="" border="0" height="1" width="1" />
          <f:verbatim>
            </td></tr><tr><td>
          </f:verbatim>
          <ui:image icon="DOT" alt="" border="0" height="5" width="1" />
          <f:verbatim>
            </td></tr></table>
          </f:verbatim>
          <ui:panelGroup binding="#{JavaHelpBean.searchResultsPanel}" />
          <f:verbatim></div></f:verbatim>
        </ui:panelGroup>
      </ui:form>      
    </ui:body> 
  </ui:page>
</f:view>
