<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@taglib uri="http://www.sun.com/web/ui" prefix="ui" %>

<f:view>
  <ui:page>
    <ui:head title="#{JavaHelpBean.mastheadHeadTitle}" />      
    <ui:body styleClass="HlpMstTtlBdy">
      <ui:form id="helpMastheadForm">
        <!-- HelpWindow Secondary Masthead -->
        <ui:masthead id="helpWindowMasthead" secondary="true"
           productImageURL="#{param['mastheadUrl']}"
           productImageHeight="#{JavaHelpBean.mastheadImageHeight}"
           productImageWidth="#{JavaHelpBean.mastheadImageWidth}"
           productImageDescription="#{param['mastheadDescription']}" />
        <!-- HelpWindow ContentPageTitle -->
        <ui:contentPageTitle id="helpWindowPageTitle" title="#{param['pageTitle']}" separator="true">
          <f:facet name="pageButtonsTop">
            <ui:button text="#{JavaHelpBean.closeLabel}" onClick="javascript: parent.close(); return false"/>
          </f:facet>
        </ui:contentPageTitle>
      </ui:form>      
    </ui:body> 
  </ui:page>
</f:view>
