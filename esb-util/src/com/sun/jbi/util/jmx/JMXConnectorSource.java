/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMXConnectorSource.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  JMXConnectorSource.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on June 13, 2005, 12:02 PM
 */

package com.sun.jbi.util.jmx;

import java.io.File;
import java.util.Map;

/**
 * This interface is to be used by jbi components / services to get a JMX Client 
 * Connector. This is a wrapper around the JMXConnectorFactory and allows one to connect 
 * to a Connector Server, which may have proprietary extensions for Security. The 
 * corresponding client Connector Source would be made available through this interface.
 *
 * @author Sun Microsystems, Inc.
 */
public interface JMXConnectorSource
{
    /**
     * @param username is the username to be used for the Connection.
     * @param password is the user password.
     */
    void setCredentials(String username, String password);
    
    /**
     * @param secureFlag indicates whether the connection is to be secured (ex. use SSL)
     */
    void setIsSecure(boolean secureFlag);
    
    /**
     * @param truststore path to the JKS truststore file.
     * @param type is the type of the Keystore ( JKS, JCEKS etc)
     * @param passwd - the truststore password
     */
    void setTrustStore(File truststore, String type, char[] passwd);
    
    /**
     * If the JMXServiceURL is setup using this method, the host, protocol and port
     * set independently are overridden.
     *
     * @param jmxServiceURL - JMX Service URL
     */
    void setJMXServiceURL(javax.management.remote.JMXServiceURL jmxServiceURL);
    
    /**
     * If the JMX Connector Server's host and port cannot be identified from the 
     * JMX Service URL or if the JMXServiceURL is not set, then this is the process
     * of indicating this information.
     *
     * @param host - hostname
     * @param port - port
     */
    void setHostAndPort(String host, int port);
    
    /**
     * If the connection has already been created, return the existing 
     * JMXConnector unless 'forceNew' is true or the connection is currently null.
     *
     * @param forceNew - create a new connection
     * @param environment - a set of attributes to determine how the connection is made. 
     * This parameter can be null. Keys in this map must be Strings. The appropriate type 
     * of each associated value depends on the attribute. The contents of environment are 
     * not changed by this call
     * @return the JMX Connector.
     * @throws java.io.IOException if a connection cannot be established.
     */
    javax.management.remote.JMXConnector getJMXConnector(boolean forceNew,
        Map environment)
        throws java.io.IOException;
    
}
