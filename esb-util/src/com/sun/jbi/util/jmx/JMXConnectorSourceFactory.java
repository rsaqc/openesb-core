/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMXConnectorSourceFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  JMXConnectorSourceFactory.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on June 13, 2005, 3:16 PM
 */

package com.sun.jbi.util.jmx;

import com.sun.jbi.util.StringTranslator;
 
/**
 *
 * @author Sun Microsystems, Inc.
 */
public class JMXConnectorSourceFactory
{
    /** Default JMX Connector Source */
    private static final String DEF_CONNECTOR_SRC = 
        "com.sun.jbi.util.jmx.JMXConnectorSourceImpl";
    
    /** The Connector Source Class System property. */
    private static final String CONNECTOR_SRC_CLASS = 
        "com.sun.jbi.util.jmx.JMXConnectorSource";
    
    /** Invalid Impl. */
    private static final String INVALID_INTERFACE_IMPL = "INVALID_INTERFACE_IMPL";
    
    /** 
     * Create a new Connector Source. If the System property 
     * "com.sun.jbi.util.JMXConnectorSource" is set to a valid Connector Source class,
     * then a new instance of this class is returned, else an instance of the default
     * Connector Source is returned.
     *
     * @return a  ConnectorSource Instance.
     * @throws Exception if a Connector Source instance cannot be created.
     */
    public static JMXConnectorSource createConnectorSource()
        throws Exception
    {
        String className = System.getProperty(CONNECTOR_SRC_CLASS, DEF_CONNECTOR_SRC);
                    
        Object obj = Class.forName (className).newInstance();
        
        if ( !( obj instanceof JMXConnectorSource ) )
        {
            // throw an ex.
            StringTranslator translator = new StringTranslator("com.sun.jbi.util", null);
            
            String errMsg = translator.getString(INVALID_INTERFACE_IMPL,
                className, "com.sun.jbi.util.jmx.JMXConnectorSource");
            throw new Exception(errMsg);
        }
        else
        {
            System.out.println("Creating a Connector Source : " + className );
            return (JMXConnectorSource) obj;
        }

    }
    
}
