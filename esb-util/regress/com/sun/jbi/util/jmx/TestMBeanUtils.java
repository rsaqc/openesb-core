/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMBeanUtils.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util.jmx;

import javax.management.MBeanServer;
import javax.management.ObjectName;

/**
 * Tests for the MBeanUtils class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestMBeanUtils
    extends junit.framework.TestCase
{
    /**
     * MBean instance.
     */
    private AnMBean mAnMBean;

    /**
     * MBean object name.
     */
    private ObjectName mAnMBeanName;

    /**
     * MBean server.
     */
    private MBeanServer mMBeanServer;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestMBeanUtils(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the data items need to create
     * the MBeanUtils in the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

        // Create an MBean server and a logger
        mMBeanServer = javax.management.MBeanServerFactory.createMBeanServer();

        // Do static initialization of MBeanUtils
        MBeanUtils.init(mMBeanServer);

        // Create an instance of an MBean
        mAnMBean = new AnMBeanImpl();

        // Create an MBean ObjectName
        mAnMBeanName = new ObjectName("jbi:type=test,name=anMBean");
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =========================  test static methods =============================

    /**
     * Test the init method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInit()
        throws Exception
    {
        MBeanUtils.init(mMBeanServer);
    }

    /**
     * Test the init method with null arguments. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitNullArguments()
        throws Exception
    {
        try
        {
            MBeanUtils.init(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIUT0000")));
        }
    }

    /**
     * Test the registerStandardMBean method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterStandardMBean()
        throws Exception
    {
        // First register the MBean with replace=false. This should work
        // because the MBean is not already registered.

        MBeanUtils.registerStandardMBean(AnMBean.class, mAnMBean,
            mAnMBeanName, false);
        assertTrue("Failure on registerStandardMBean(): ",
            mMBeanServer.isRegistered(mAnMBeanName));

        // Now try to register the MBean with replace=true. This should also
        // work because the MBean will be replaced.

        MBeanUtils.registerStandardMBean(AnMBean.class, mAnMBean,
            mAnMBeanName, true);
        assertTrue("Failure on registerStandardMBean(): ",
            mMBeanServer.isRegistered(mAnMBeanName));
    }

    /**
     * Test the registerStandardMBean method with an already-registered MBean.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterStandardMBeanDuplicate()
        throws Exception
    {
        // First register the MBean with replace=false. This should work
        // because the MBean is not already registered.

        MBeanUtils.registerStandardMBean(AnMBean.class, mAnMBean,
            mAnMBeanName, false);
 
        // Now try to register the MBean again with replace=false. This should
        // fail because the MBean is already registered.

        try
        {
            MBeanUtils.registerStandardMBean(AnMBean.class, mAnMBean,
                mAnMBeanName, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIUT0011")));
        }
    }

    /**
     * Test the registerStandardMBean method with a non-compliant MBean.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterStandardMBeanNonCompliant()
        throws Exception
    {
        try
        {
            MBeanUtils.registerStandardMBean(AnMBean.class, this,
                mAnMBeanName, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIUT0010")));
        }
    }

    /**
     * Test the unregisterMBean method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterMBean()
        throws Exception
    {
        // First register an MBean and make sure it succeeds
        MBeanUtils.registerStandardMBean(AnMBean.class, mAnMBean,
            mAnMBeanName, false);
        assertTrue("Failure on registerStandardMBean(): ",
            mMBeanServer.isRegistered(mAnMBeanName));

        // Now unregister the MBean and make sure it succeeds
        MBeanUtils.unregisterMBean(mAnMBeanName);
        assertFalse("Failure on unregisterMBean(): ",
            mMBeanServer.isRegistered(mAnMBeanName));
    }

}
