/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestStreamDataSource.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.util;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Tests for the ExchangeIdImpl class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestStreamDataSource extends junit.framework.TestCase
{
    
	final static String CONTENT_TYPE = "text/plain";
	final static String	CONTENT_NAME = "Name";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestStreamDataSource(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
   }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * testNextId 
     * @throws Exception if an unexpected error occurs
     */
    public void testBasic()
           throws Exception
    {
		StreamDataSource		sds;
        OutputStream			os;
        InputStream				is;

		sds = new StreamDataSource("Name", "text/plain");
		assertEquals(CONTENT_TYPE, sds.getContentType());
		assertEquals(CONTENT_NAME, sds.getName());

		//
		//	Shouldn't be able to read yet since there is no output.
		//
		try
		{
			sds.getInputStream();
			fail("Expected IOException");
		}
		catch (java.io.IOException ioEx)
		{
		}

		//
		//	Fill up the Stream.
		//
		os = sds.getOutputStream();
		os.write(1);
		os.write(2);
		os.write(3);
		os.close();

		//
		// Now we should be able to get the InputStream.
		//
		is = sds.getInputStream();
		assertEquals(is.read(), 1);
		assertEquals(is.read(), 2);
		assertEquals(is.read(), 3);
		assertEquals(is.read(), -1);

		//
		//	Shouldn't be able to read yet since the output has been consumed.
		//
		try
		{
			sds.getInputStream();
			fail("Expected IOException");
		}
		catch (java.io.IOException ioEx)
		{
		}    
	}
    
}
