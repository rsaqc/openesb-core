/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 * Tests for the InstanceEntry class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServiceInfo extends junit.framework.TestCase
{    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestServiceInfo(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
   }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * testBasics
     * @throws Exception if an unexpected error occurs
     */
    public void testBasics()
           throws Exception
    {
        QName               sn;
        ServiceEndpoint     se = new ServiceEndpointImpl(sn = new QName("service"), "endpoint");
        RegistrationInfo    ri;
        RegistrationInfo    ri2;
        ServiceInfo         si;
        String              instance;
        String              instance2;
        
        si = new ServiceInfo(se);
        assertEquals(si.getServiceEndpoint(), se);
        assertEquals(si.getInstanceCount(), 0);        
        ri = new RegistrationInfo(se, "i", null, "add");
        ri2 = new RegistrationInfo(se, "j", null, "add");
        si.addInstance(ri);
        assertEquals(si.getInstanceCount(), 1); 
        assertEquals(si.mDepth, 0);
        instance = si.getInstance();
        assertEquals(instance, "i");
        assertEquals(si.mDepth, 1);
        instance = si.getInstance();
        assertEquals(instance, "i");
        assertEquals(si.mDepth, 2);
        si.returnInstance("i");
        assertEquals(si.mDepth, 1);
        si.returnInstance("i");
        assertEquals(si.mDepth, 0);
        si.addInstance(ri2);
        assertEquals(si.getInstanceCount(), 2); 
        instance = si.getInstance();
        instance2 = si.getInstance();
        assertEquals(si.mDepth, 2);
        assertTrue(!instance.equals(instance2));
        si.returnInstance(instance2);
        si.returnInstance(instance);
        assertEquals(si.mDepth, 0);
        assertEquals(si.removeInstance("i"), false);
        assertEquals(si.getInstanceCount(), 1);
        assertEquals(si.removeInstance("j"), true);
        assertEquals(si.getInstanceCount(), 0);
   }
}
