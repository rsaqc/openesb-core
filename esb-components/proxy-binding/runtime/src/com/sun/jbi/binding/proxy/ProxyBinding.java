/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ProxyBinding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import com.sun.jbi.binding.proxy.connection.ConnectionManager;
import com.sun.jbi.binding.proxy.connection.ConnectionManagerFactory;
import com.sun.jbi.binding.proxy.connection.ClientConnection;

import com.sun.jbi.binding.proxy.stats.Aggregate;
import com.sun.jbi.binding.proxy.stats.Calculate;
import com.sun.jbi.binding.proxy.stats.StatBase;

import com.sun.jbi.binding.proxy.util.ExchangeIdImpl;
import com.sun.jbi.binding.proxy.util.MEPInputStream;
import com.sun.jbi.binding.proxy.util.MEPOutputStream;
import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.component.ComponentContext;

import com.sun.jbi.messaging.DeliveryChannel;
import com.sun.jbi.messaging.Link;
import com.sun.jbi.messaging.MessageExchange;

import javax.management.ObjectName;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.support.MBeanNamesImpl;
import com.sun.jbi.util.jmx.MBeanUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import java.text.SimpleDateFormat;

import java.util.logging.Logger;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

/**
 * Performs BC-related work for ProxyBinding.
 * @author Sun Microsystems, Inc
 */
public class ProxyBinding 
{
    /**
     * Out LifeCycle
     */
    private ProxyBindingLifeCycle   mLifeCycle;
    
    /**
     * Managers the connection between ProxyBinding instances.
     */
    private ConnectionManager       mCM;
    
    /**
     * The connection to the local NMR.
     */
    private DeliveryChannel         mChannel;
    
    /**
     * Our endpoint for ProxyBinding exposed services.
     */
    private ServiceEndpoint         mService;
    
    /**
     * Thread that is the primary interface to the NMR.
     */
    private Thread                  mNMRThread;
    private NMRProcessor            mNMRProcessor;
    
    /**
     * Thread that is the primary interface to remote communications.
     */
    private Thread                  mRemoteThread;
    private RemoteProcessor         mRemoteProcessor;
    
    /**
     * Thread that is the primary interface to remote events.
     */
    private Thread                  mEventThread;
    private EventProcessor          mEventProcessor;
    
    /**
     * Set of active MessageExchanges.
     */
    private HashMap                 mExchanges;
    
    /**
     * Our logger.
     */
    private Logger                  mLog;
    
    /**
     * Remote endpoints that we proxy, and local endpoints that we can service.
     */
    private HashMap<RegistrationInfo, ServiceInfo>              mRemoteEndpoints;
    private HashMap<RegistrationInfo, RegistrationInfo>         mLocalEndpoints;
    private HashMap<ConnectionsInfo, ConnectionsInfo>           mLocalConnections;
    private HashMap<ConnectionsInfo, ConnectionsInfo>           mRemoteConnections;
    
    /**
     * Our unique instance identifier.
     */
    private String                  mId;
    
    /**
     * Our current run state.
     */
    private boolean                 mRunning;
    private boolean                 mStarted;
    
    /**
     * The name of our service.
     */
    static final String             SUNPROXYBINDINGSERVICE = "SunProxyBindingService";
    
    // internal stats tree
    private StatBase                mStatistics;
    
    /**
     * Basic statistics collection counters.
     */
    private long                    mExchangesSent;
    private long                    mExchangesReceived;
    private long                    mBytesSent;
    private long                    mBytesReceived;
    private long                    mMessagesSent;
    private long                    mMessagesReceived;
    private long                    mFaults;
    private long                    mError;
    private long                    mDone;
   
    /**
     * Constructor for a ProxyBinding.
     * @param channel that should be used.
     * @param instanceId to be assigned.
     * @throws javax.jbi.JBIException if there is a problem.
     */
    ProxyBinding(ProxyBindingLifeCycle lifeCycle, String instanceId)
        throws javax.jbi.JBIException
    {
        mLifeCycle = lifeCycle;
        mLog = getLogger("base");   
        mChannel = (DeliveryChannel)mLifeCycle.getComponentContext().getDeliveryChannel();
        mExchanges = new HashMap();
        mRemoteEndpoints = new HashMap();
        mLocalEndpoints = new HashMap();
        mLocalConnections = new HashMap();
        mRemoteConnections = new HashMap();
        mCM = ConnectionManagerFactory.getInstance(mLifeCycle.getComponentContext().getNamingContext()).getConnectionManager(instanceId);
        mId = instanceId;
        mRunning = true;
        mService = mLifeCycle.getComponentContext().activateEndpoint(new QName(SUNPROXYBINDINGSERVICE), "InternalEndpoint");  
    }
    
    /**
     * Start the ProxyBinding machinery. This involves starting the two threads that 
     * listen for NMR traffic and Communications traffic.
     */
    void start()
    {
        StatBase            sb;
        StatBase            sb2;
        
        //
        //  Setup statistics collection aggregated by Service and Operation.
        //
        mStatistics = new Aggregate(ExchangeEntry.FIELD_SERVICE);
        mStatistics.addChild(sb2 = sb = new Aggregate(ExchangeEntry.FIELD_OPERATION));
        sb.addChild(sb = new Calculate(ExchangeEntry.FIELD_BYTESSENT));
        sb.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESRECEIVED));
        sb.addPeer(new Calculate(ExchangeEntry.FIELD_DURATION, Calculate.MIN | Calculate.MAX | Calculate.AVG | Calculate.STD));
        sb2.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESSENT));
        sb.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESRECEIVED));
        sb.addPeer(new Calculate(ExchangeEntry.FIELD_DURATION, Calculate.MIN | Calculate.MAX | Calculate.AVG | Calculate.STD));
        mStatistics.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESSENT));
        sb.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESRECEIVED));
        sb.addPeer(new Calculate(ExchangeEntry.FIELD_DURATION, Calculate.MIN | Calculate.MAX | Calculate.AVG | Calculate.STD));
        try
        {
            mCM.start(this);
            mEventProcessor = new EventProcessor(this);
            mNMRProcessor = new NMRProcessor(this);
            mRemoteProcessor = new RemoteProcessor(this);
        }
        catch (com.sun.jbi.binding.proxy.connection.ConnectionException cEx)
        {

            mLog.warning(Translator.translate(LocalStringKeys.NMR_MESSAGING_EXCEPTION,cEx));
            return;            
        }
        
        mEventThread = new Thread (mEventProcessor);
        mEventThread.setName("JBI-ProxyBinding-EventProcessor");
        mEventThread.setDaemon(true);
        mEventThread.start();
        mNMRThread = new Thread (mNMRProcessor);
        mNMRThread.setName("JBI-ProxyBinding-NMRProcessor");
        mNMRThread.setDaemon(true);
        mNMRThread.start();
        mRemoteThread = new Thread (mRemoteProcessor);
        mRemoteThread.setName("JBI-ProxyBinding-RemoteReceiver");
        mRemoteThread.setDaemon(true);
        mRemoteThread.start();
        
        //
        //  Wait for the event processor to resolve membership.
        //
        synchronized (this)
        {
            if (!mStarted)
            {
                try
                {
                    this.wait();
                }
                catch (java.lang.InterruptedException iEx)
                {

                }
            }
        }
	
    }
    
    //
    //  Used to signal start() that it can complete.
    //
    synchronized void startComplete()
    {
        mStarted = true;
        this.notify();
    }
    
    /**
     * Stop the ProxyBinding machinery. This involves getting the NMR and Communications threads
     * to terminate.
     */
    synchronized void stop()
    {
        if (mRunning)
        {
            mLog.info(this.toString());
            mRunning = false;
            mChannel.setEndpointListener(null);
            mNMRProcessor.stop();
            mNMRThread.interrupt();
            mRemoteProcessor.stop();
            mRemoteThread.interrupt();
            mEventProcessor.stop();
            mEventThread.interrupt();
            mChannel.setTimeoutListener(null);
            mCM.closeServerConnection();
            mCM.closeClientConnection();
            mCM.closeEventConnection();
            mCM.stop();
            mLog.info(this.toString());
        }
    }
 
//
// ------------------------ Accessors for interesting info  -----------------------
//   
    
    ConnectionManager getConnectionManager()
    {
        return (mCM);
    }
    
    public DeliveryChannel getDeliveryChannel()
    {
        return (mChannel);
    }

    ServiceEndpoint getService()
    {
        return (mService);
    }
    
    Document getServiceDescription(ServiceEndpoint endpoint)
    {
        return (mNMRProcessor.getDescription(endpoint));
    }
    
    ServiceEndpoint resolveEndpointReference(DocumentFragment document)
    {
        //
        //  We may be entered via recursion involving multiple instances. To avoid
        //  this we check to see if the source of the request is the NMRProcessor thread.
        //  This would only be true if the NMRProcessor if currently handling a remote 
        //  resolveEndpointReference.
        //
        if (!Thread.currentThread().equals(mNMRThread))
        {
            return (mNMRProcessor.resolveEndpointReference(document));
        }
        return (null);
            
    }
    
    boolean isExchangeWithConsumerOkay(ServiceEndpoint endpoint, javax.jbi.messaging.MessageExchange exchange)
    {
        return (mNMRProcessor.isExchangeOkay(endpoint, exchange));
    }
    
    ComponentContext getComponentContext()
    {
        return (mLifeCycle.getComponentContext());
    }
    
    public Logger getLogger(String suffix)
    {
        try
        {
            return (mLifeCycle.getComponentContext().getLogger(suffix, null));
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            return (Logger.getLogger("com.sun.jbi.component.SunProxyBinding." + suffix));
        }
    }
    
    String  getInstanceId()
    {
        return (mId);
    }
    
    HashMap getInstances()
    
    {
        return (mEventProcessor.getInstances());
    }
    
//
// ------------------------ Methods used for tracking state of Exchanges -----------------------
//   

    synchronized public ExchangeEntry trackExchange(String id, MessageExchange me, boolean consumer)
    {
        ExchangeEntry   ee;
        
        mExchanges.put(id, ee = new ExchangeEntry(id, me, consumer));        
        return (ee);
    }

    synchronized public ExchangeEntry getExchangeEntry(String id)
    {       
        return ((ExchangeEntry)mExchanges.get(id));
    }
        
    void purgeExchange(String id)
    {
        ExchangeEntry   ee;
        boolean         isLast = false;
        
        synchronized (this)
        {
            ee = (ExchangeEntry)mExchanges.get(id);
            if (ee != null)
            {
                ServiceEndpoint     se = ee.getMessageExchange().getEndpoint();
                
                if (se != null)
                {
                    returnInstanceForEndpoint(se, ee.getClientConnection().getInstanceId());
                }
                ee.setState(ExchangeEntry.STATE_LAST);
                mExchanges.remove(id);
                isLast = true;
            }
        }
        
        if (isLast)
        {
            updateStatistics(ee);
        }
    }
    
//
// ------------------------ Methods for tracking remote RegistrationInfo ----------------
//      
   
    synchronized void addRemoteEndpoint(RegistrationInfo info)
        throws javax.jbi.JBIException
    {
        ServiceInfo         si = (ServiceInfo)mRemoteEndpoints.get(info);
        
        if (si == null)
        {
            si = new ServiceInfo(mLifeCycle.getComponentContext().activateEndpoint(info.getServiceName(), info.getEndpointName()));  
            mRemoteEndpoints.put(info, si);
        }
        si.addInstance(info);
    }
    
    synchronized void removeRemoteEndpoint(RegistrationInfo info)
        throws javax.jbi.JBIException
    {
        ServiceInfo         si = (ServiceInfo)mRemoteEndpoints.get(info);
        
        if (si != null)
        {
            if (si.removeInstance(info.getInstanceId()))
            {
                mLifeCycle.getComponentContext().deactivateEndpoint(si.getServiceEndpoint());
                mRemoteEndpoints.remove(info);
            }
        }
    }
    
    synchronized void purgeRemoteEndpointsForInstance(String instanceId)
    {
        for (Iterator i = mRemoteEndpoints.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry      m = (Map.Entry)i.next();
            ServiceInfo    si = (ServiceInfo)m.getValue();
            
            if (si.removeInstance(instanceId))
            {
                try
                {
                    mLifeCycle.getComponentContext().deactivateEndpoint(si.getServiceEndpoint());
                }
                catch (javax.jbi.JBIException jEx)
                {
                    
                }
                i.remove();                
            }
        }
    }
        
    synchronized String getInstanceForEndpoint(ServiceEndpoint endpoint)
    {
        RegistrationInfo        info = new RegistrationInfo(endpoint, null, null, null);
        ServiceInfo             si;
        String                  id = null;
        
        si = (ServiceInfo)mRemoteEndpoints.get(info);
        if (si != null)
        {
            id = si.getInstance();
        }
        return (id);
    }

    synchronized void returnInstanceForEndpoint(ServiceEndpoint endpoint, String instance)
    {
        RegistrationInfo        info = new RegistrationInfo(endpoint, null, null, null);
        ServiceInfo             si;
        
        si = (ServiceInfo)mRemoteEndpoints.get(info);
        if (si != null)
        {
            si.returnInstance(instance);
        }       
    }
    
    synchronized void addRemoteConnection(ConnectionsInfo info)
        throws javax.jbi.JBIException
    {
        mChannel.addServiceConnection(info.getServiceLinkName(), info.getEndpointLinkName(),
            info.getServiceName(), info.getEndpointName(), info.getLink());
        mRemoteConnections.put(info, info);
    }
    
    synchronized void removeRemoteConnection(ConnectionsInfo info)
        throws javax.jbi.JBIException
    {
        mChannel.removeServiceConnection(info.getServiceLinkName(), info.getEndpointLinkName(),
            info.getServiceName(), info.getEndpointName());
        mRemoteConnections.remove(info);
    }
    
    synchronized void purgeRemoteConnectionsForInstance(String instanceId)
    {
        for (Iterator i = mRemoteConnections.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry           m = (Map.Entry)i.next();
            ConnectionsInfo     ci = (ConnectionsInfo)m.getValue();
            
            if (ci.getInstanceId().equals(instanceId))
            {
                mChannel.removeServiceConnection(ci.getServiceLinkName(), ci.getEndpointLinkName(),
                    ci.getServiceName(), ci.getEndpointName());
                i.remove();                
            }
        }
    }
//
// ------------------------ Methods operating on local RegistrationInfo -----------------------
//
    /** 
     * Add a registered endpoint to the registry. 
     * @param endpoint to be added to Registry.
     * @return RegistrationInfo created.
     * @throws javax.jbi.JBIException for any JMS problems.
     */
    public void addLocalEndpoint(ServiceEndpoint endpoint, ClassLoader classLoader)
        throws javax.jbi.JBIException
    {        
        RegistrationInfo ri;
        
        ri = new RegistrationInfo(endpoint, mCM.getInstanceId(), classLoader, RegistrationInfo.ACTION_ADD);   
        mLocalEndpoints.put(ri, ri);
        mLog.fine("PB:send registration (" + ri.getAction() + "," + ri.getServiceName() + "," + ri.getEndpointName() + "," + ri.getInstanceId() + ")");
        mEventProcessor.sendEvent(ri);
    }
    
    /** 
     * Remove a registered endpoint to the registry. 
     * @param endpoint to be removed from Registry.
     * @return RegistrationInfo created.
     * @throws javax.jbi.JBIException for any JMS problems.
     */
    public void removeLocalEndpoint(ServiceEndpoint endpoint)
        throws javax.jbi.JBIException
    {        
        RegistrationInfo ri;
        
        ri = new RegistrationInfo(endpoint, mCM.getInstanceId(), null, RegistrationInfo.ACTION_REMOVE);
        mLocalEndpoints.remove(ri);
        mLog.fine("PB:send deregistration (" + ri.getAction() + "," + ri.getServiceName() + "," + ri.getEndpointName() + "," + ri.getInstanceId() + ")");
        mEventProcessor.sendEvent(ri);
    }
    
    LinkedList<RegistrationInfo> getLocalEndpoints()
    {
        LinkedList<RegistrationInfo>          ll = new LinkedList();
        
        for (RegistrationInfo ri : mLocalEndpoints.values())
        {
            ll.add(ri);
        }
        return (ll);
    }
    
    /** 
     * Add a registered endpoint to the registry. 
     * @param endpoint to be added to Registry.
     * @return RegistrationInfo created.
     * @throws javax.jbi.JBIException for any JMS problems.
     */
    public void addServiceConnection(QName serviceLink, String endpointLink, QName service, String endpoint, String link, String instance)
        throws javax.jbi.JBIException
    {        
        ConnectionsInfo ci;
        
        ci = new ConnectionsInfo(serviceLink, endpointLink, service, endpoint, link, ConnectionsInfo.ACTION_ADD, instance);   
        mLocalConnections.put(ci, ci);
        mLog.fine("PB:send add service connection (" + serviceLink + "," +
            endpointLink + "," + service + "," + endpoint + "," + link + ")");
        mEventProcessor.sendEvent(ci);
    }
    
    /** 
     * Remove a registered endpoint to the registry. 
     * @param endpoint to be removed from Registry.
     * @return RegistrationInfo created.
     * @throws javax.jbi.JBIException for any JMS problems.
     */
    public void removeServiceConnection(QName serviceLink, String endpointLink, QName service, String endpoint, String instanceId)
        throws javax.jbi.JBIException
    {        
        ConnectionsInfo ci;
        
        ci = new ConnectionsInfo(serviceLink, endpointLink, service, endpoint, "", ConnectionsInfo.ACTION_REMOVE, instanceId);   
        mLocalConnections.remove(ci);
        mLog.fine("PB:send remove service connection (" + serviceLink + "," +
            endpointLink + "," + service + "," + endpoint + ")");
        mEventProcessor.sendEvent(ci);
    }
    
    void postNotification(String name, String value)
    {
        mLifeCycle.postNotification(name, value);
    }
    
//
// ------------------- Methods for statistics collection/display  -------------------
//
    
    private void updateStatistics(ExchangeEntry ee)
    {
        mBytesSent += ee.getBytesSent();
        mBytesReceived += ee.getBytesReceived();
        mMessagesSent +=  ee.getMessagesSent();
        mMessagesReceived += ee.getMessagesReceived();
        if (ee.isConsumer())
        {
            mExchangesSent++;
        }
        else
        {
            mExchangesReceived++;
        }
        if (ee.isFaulted())
        {
            mFaults++;
	}
        if (ee.isDone())
        {
            mDone++;
	}
        if (ee.isError())
        {
            mError++;
	}

        // update internal stats
        if (mStatistics != null)
        {
            mStatistics.apply(ee);
        }
    }


//
// ------------------------ Methods for debugging/information  -----------------------
//
    
    
    public String toString()
    {
        StringBuffer        sb = new StringBuffer();
        
        sb.append("ProxyBinding state at ");
        sb.append(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date(System.currentTimeMillis())));
        sb.append("\n");
        sb.append(mEventProcessor.toString());
        sb.append("  Exchanges          Sent(");
        sb.append(mExchangesSent);
        sb.append(")  Received(");
        sb.append(mExchangesReceived);
        sb.append(")\n");
        sb.append("  Messages           Sent(");
        sb.append(mMessagesSent);
        sb.append(")  Received(");
        sb.append(mMessagesReceived);
        sb.append(")\n");
        sb.append("  Bytes              Sent(");
        sb.append(mBytesSent);
        sb.append(")  Received(");
        sb.append(mBytesReceived);
        sb.append(")\n");
        sb.append("  Result             DONE(");
        sb.append(mDone);
        sb.append(")  ERROR(");
        sb.append(mError);
        sb.append(")  Faulted(");
        sb.append(mFaults);
        sb.append(")\n  Internal Exchanges Sent(");
        sb.append(mNMRProcessor.mOperationsSent);
        sb.append(")  Received(");
        sb.append(mNMRProcessor.mOperationsReceived);
        sb.append(")\n");
        sb.append("  Active Exchanges   Count(");
        sb.append(mExchanges.size());
        sb.append(")\n");
        for (Iterator i = mExchanges.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("  Remote Services Count(");
        sb.append(mRemoteEndpoints.size());
        sb.append(")\n");
        for (Iterator i = mRemoteEndpoints.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("  Local Services Count(");
        sb.append(mLocalEndpoints.size());
        sb.append(")\n");
        for (Iterator i = mLocalEndpoints.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("  Local Service Connections Count(");
        sb.append(mLocalConnections.size());
        sb.append(")\n");
        for (Iterator i = mLocalConnections.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("  Remote Service Connections Count(");
        sb.append(mRemoteConnections.size());
        sb.append(")\n");
        for (Iterator i = mRemoteConnections.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        if (mStatistics != null)
        {
            sb.append("  Active Statistics:\n");
            sb.append(mStatistics.report(2));
        }
        return (sb.toString());
    }

}
