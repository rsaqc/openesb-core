/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSEvent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.jms;

import com.sun.jbi.binding.proxy.LocalStringKeys;

import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.binding.proxy.connection.Event;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;

import javax.jms.StreamMessage;
import javax.jms.Queue;
import javax.jms.QueueSender;
import javax.jms.QueueSession;

/**
 * Connection as a JMS Client.
 * @author Sun Microsystems, Inc
 */
public class JMSEvent
        implements Event
{
    StreamMessage           mMessage;
    String                  mEventName;
    String                  mSender;
    static final String     EVENTNAME = "EventName";
    
    JMSEvent(StreamMessage    m)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        mMessage = m;
        try
        {
            mSender = m.getJMSType();
            mEventName = m.getStringProperty(EVENTNAME);
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);                        
        }
    }
    
    JMSEvent(StreamMessage m, String eventName)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        mMessage = m;
        mEventName = eventName;
        try
        {
            mSender = m.getJMSType();
            m.setStringProperty(EVENTNAME, eventName);
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);                        
        }
    }
    
    public String getEventName()
    {
        return (mEventName);
    }
    
    public String getString()
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        try
        {
            return (mMessage.readString());
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);
        }
    }
    
    public long getLong()
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        try
        {
            return (mMessage.readLong());
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);            
        }
    }
        
    public void putString(String value)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        try
        {
            mMessage.writeString(value);
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);            
        }
    }
    
    public void putLong(long value)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        try
        {
            mMessage.writeLong(value);
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);            
            
        }
    }

    public Object getObject()
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        try
        {
            ByteArrayInputStream    bais;
            ObjectInputStream       ois;
            int                     size;
            byte[]                  bytes;
            Object                  object;
            
            size = mMessage.readInt();
            bytes = new byte[size];
            mMessage.readBytes(bytes);
            ois = new ObjectInputStream(bais = new ByteArrayInputStream(bytes));
            object = ois.readObject();
            ois.close();
            return (object);
        }
        catch (java.lang.ClassNotFoundException cnfEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(cnfEx);                                             
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(ioEx);                                  
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);                       
        }
    }
    
    public void putObject(Object value)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        try
        {
            ByteArrayOutputStream   baos;
            ObjectOutputStream      oos;
            byte[]                  bytes;
            
            oos = new ObjectOutputStream(baos = new ByteArrayOutputStream());
            oos.writeObject(value);
            oos.close();
            bytes = baos.toByteArray();
            mMessage.writeInt(bytes.length);
            mMessage.writeBytes(bytes);
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(ioEx);                                
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);                        
        }
    }
    
    StreamMessage getMessage()
    {
        return (mMessage);
    }
    
    public String getSender()
    {
        return (mSender);
    }
}
