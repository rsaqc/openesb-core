/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSEventConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.gms;

import com.sun.enterprise.ee.cms.core.MessageSignal;

import com.sun.jbi.binding.proxy.LocalStringKeys;
import com.sun.jbi.binding.proxy.RegistrationInfo;

import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.binding.proxy.connection.EventInfo;
import com.sun.jbi.binding.proxy.connection.EventInfoFactory;

import com.sun.jbi.binding.proxy.connection.EventConnection;
import com.sun.jbi.binding.proxy.connection.Event;

import java.util.concurrent.LinkedBlockingQueue;

import java.util.logging.Logger;

import javax.xml.namespace.QName;

/**
 * Encapsulates the logic of a Event connection. The event connection is assumed to be
 * called from a single thread using pull-style processing. The producer and consumer are 
 * attached to the same session which allows messages sent by this producer to not be seen
 * by this consumer.
 *
 * @author Sun Microsystems, Inc
 */
public class GMSEventConnection 
        implements EventConnection
{
    private GMSConnectionManager                mGMS;
    private LinkedBlockingQueue<EventInfo>      mEventQueue;
    private Logger                              mLog;
    
    GMSEventConnection(GMSConnectionManager cm)
    {
        mGMS = cm;
        mLog = cm.getLogger();
        mEventQueue = new LinkedBlockingQueue();
    }        

    public void sendEvent(EventInfo info)
        throws com.sun.jbi.binding.proxy.connection.EventException            
    {
            GMSEvent            e = new GMSEvent(info.getEventName());
            
            info.encodeEvent(e);
            mGMS.sendEvent(e);
    }

    public void sendEventTo(EventInfo info, String id)
        throws com.sun.jbi.binding.proxy.connection.EventException            
    {
            GMSEvent            e = new GMSEvent(info.getEventName());
            
            info.encodeEvent(e);
            mGMS.sendEventTo(e, id);
    }

    public EventInfo receiveEvent(long timeout)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        try
        {
            return (mEventQueue.take());
        }
        catch (java.lang.InterruptedException ieEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(ieEx);
        }
    }
   
    void queueEvent(EventInfo ei)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        try
        {
            mEventQueue.put(ei);
        }
        catch (java.lang.InterruptedException ieEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(ieEx);            
        }
    }
    
    void postEvent(MessageSignal    ms)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        GMSEvent            e = new GMSEvent(ms);
        EventInfo           ei = EventInfoFactory.getInstance().newInstance(e);

        try
        {
            if (ei != null)
            {
		mLog.info("PB-GMS:Event type (" + e.getEventName() + ") received from (" + ms.getMemberToken() + ")");
                mEventQueue.put(ei);
            }
            else
            {                   
                mLog.info("PB-GMS:Received unknown event type (" + e.getEventName() + ")");
            }        
        }
        catch (java.lang.InterruptedException ieEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(ieEx);            
        }
    }
}
