/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import com.sun.jbi.binding.proxy.RegistrationInfo;

import com.sun.jbi.binding.proxy.connection.Event;
import com.sun.jbi.binding.proxy.connection.EventInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * Represents information about a service. A service may be exported from mulitple instances.
 *
 * @author Sun Microsystems, Inc
 */
public class ServiceInfo
{
    class Entry
    {
        RegistrationInfo        mRegInfo;
        int                     mActiveCount;
        int                     mTotalCount;
    }
    
    private ServiceEndpoint     mEndpoint;
    private ArrayList           mInstances;
    private HashMap             mMap;
    private int                 mNext;
    int                         mTotalCount;
    int                         mDepth;
    
    /** Creates a new instance of ServiceInfo */
    public ServiceInfo(ServiceEndpoint endpoint) 
    {
        mEndpoint = endpoint;
        mMap = new HashMap();
        mInstances = new ArrayList();
        mNext = -1;
    }
    
    public ServiceEndpoint getServiceEndpoint()
    {
        return (mEndpoint);
    }
    
    public void addInstance(RegistrationInfo ri)
    {
        Entry   e = new Entry();
        e.mRegInfo = ri;
        e.mActiveCount = 0;
        e.mTotalCount = 0;        
        mInstances.add(e);
        mMap.put(ri.getInstanceId(), e);
    }
    
    public int getInstanceCount()
    {
        return (mInstances.size());
    }
    
    public boolean removeInstance(String instance)
    {
        for (Iterator i = mInstances.iterator(); i.hasNext(); )
        {
            Entry        e = (Entry)i.next();
            
            if (e.mRegInfo.getInstanceId().equals(instance))
            {
                i.remove();
                mMap.remove(instance);
                break;
            }
        }
        return (mInstances.isEmpty());
    }
    
    public String getInstance()
    {
        Entry       e;
        
        if (++mNext >= mInstances.size())
        {
            mNext = 0;
        }
        e = (Entry)mInstances.get(mNext);
        e.mActiveCount++;
        e.mTotalCount++;
        mDepth++;
        mTotalCount++;
        
        return (e.mRegInfo.getInstanceId());        
    }
    
    public void returnInstance(String instanceId)
    {
        Entry e;
        
        e = (Entry)mMap.get(instanceId);
        if (e != null)
        {
            e.mActiveCount--;
            mDepth--;
        }
    }
    
    public String toString()
    {
        StringBuffer    sb = new StringBuffer();
        
        sb.append("    ServiceInfo Service(");
        sb.append(mEndpoint.getServiceName().toString());
        sb.append(")\n      Endpoint        (");
        sb.append(mEndpoint.getEndpointName());
        sb.append(")\n      Instances Count (");
        sb.append(mInstances.size());
        sb.append(") Next(");
        sb.append(mNext);
        sb.append(") TotalCount(");
        sb.append(mTotalCount);
        sb.append(") Depth(");
        sb.append(mDepth);
        sb.append(")\n");
        for (ListIterator i = mInstances.listIterator(); i.hasNext();)
        {
            Entry   e = (Entry)i.next();
            sb.append(e.mRegInfo.toString());
            sb.append("          TotalCount(");
            sb.append(e.mTotalCount);
            sb.append(") ActiveCount(");
            sb.append(e.mActiveCount);
            sb.append(")\n");
        }
        return (sb.toString());
    }
}
