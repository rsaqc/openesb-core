/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import com.sun.jbi.binding.proxy.connection.ConnectionManager;
import com.sun.jbi.binding.proxy.connection.ConnectionManagerFactory;
import com.sun.jbi.binding.proxy.connection.ClientConnection;
import com.sun.jbi.binding.proxy.connection.EventConnection;
import com.sun.jbi.binding.proxy.connection.ServerConnection;
import com.sun.jbi.binding.proxy.connection.EventInfo;
import com.sun.jbi.binding.proxy.connection.EventInfoFactory;

import com.sun.jbi.binding.proxy.util.MEPInputStream;
import com.sun.jbi.binding.proxy.util.MEPOutputStream;
import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.messaging.DeliveryChannel;
import com.sun.jbi.messaging.EndpointListener;
import com.sun.jbi.messaging.MessageExchange;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Logger;

import java.text.SimpleDateFormat;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 * Performs BC-related work for ProxyBinding.
 * @author Sun Microsystems, Inc
 */
class EventProcessor
        implements  java.lang.Runnable
{
    private HashMap             mInstances;
    private HashMap             mWaitInstances;
    private String              mId;
    private String              mMasterId;
    
    /**
     * Our logger.
     */
    private Logger              mLog;
    
    private ProxyBinding        mPB;
    private ConnectionManager   mCM;
    private ServiceEndpoint     mService;
    private EventConnection     mEC;
    private boolean             mRunning = true;
    int                         mEventsReceived;
    int                         mEventsSent;
    
    /**
     * Name of our ACTIVATE endpoint operation.
     */
    static final String             ACTIVATE = "Activate";
    
    /**
     * Name of our DEACTIVATE endpoint operation.
     */
    static final String             DEACTIVATE = "Deactivate";

    /**
     * Constructor for the EventProcessor.
     * @param proxyBinding that we are running under
     */
    EventProcessor(ProxyBinding proxyBinding)
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        EventInfoFactory            efi;
        
        mPB = proxyBinding;
        mLog = mPB.getLogger("event");        
        mService = proxyBinding.getService();
        mCM = proxyBinding.getConnectionManager();
        mEC = mCM.getEventConnection();
        mInstances = new HashMap();
        mWaitInstances = new HashMap();
       
        //
        //  Register the events that we can accept.
        //
        efi = EventInfoFactory.getInstance();
        efi.registerEventInfo(RegistrationInfo.class, RegistrationInfo.EVENTNAME);
        efi.registerEventInfo(JoinInfo.class, JoinInfo.EVENTNAME);
        efi.registerEventInfo(LeaveInfo.class, LeaveInfo.EVENTNAME);
        efi.registerEventInfo(ConnectionsInfo.class, ConnectionsInfo.EVENTNAME);
        
    }
    
    private long getProperty(String prop, String defaultValue)
    {
        long    value;
        
        try
        {
            value = new Integer(System.getProperty(prop, defaultValue)).longValue();
        }
        catch (java.lang.NumberFormatException nfEx)
        {
            value = new Integer(defaultValue).longValue();
        }
        return (value);
    }
    
    /**
     * Stop the event processor on the next event or timeout.
     */
    void stop()
    {
        mRunning = false;
    }
    
    HashMap getInstances()
    {
        return ((HashMap)mInstances.clone());
    }
    
    /**
     * Main processing loop
     * Basic flow:
     *      Read next write event or timeout.
     *      Process event (if available)
     *      Handle timeout (if happened)
     */
    public void run() 
    {
        mLog.fine("PB:EventProcessor starting.");        

        //
        // Basic processing loop.
        // Prime the machinery with a Hello event.
        //
        mPB.startComplete();
        for (;mRunning;)
        {                
            try
            {
                EventInfo               ei = null;        
                
                //
                //  Wait for next event or timeout.
                //
                ei = mEC.receiveEvent(0);
                
                //
                //  Process event if we got one.
                //
                if (ei != null)
                {   
                    mEventsReceived++;
                    processEvent(ei);
                }
                
            }
            catch (com.sun.jbi.binding.proxy.connection.EventException eEx)
            {
                mLog.warning(Translator.translate(LocalStringKeys.EP_EVENT_EXCEPTION, eEx));
                mPB.stop();
            }
            catch (Exception ex)
            {
                mLog.warning(Translator.translate(LocalStringKeys.EP_EXCEPTION, ex));
            }
        }
    }    
    
//
// ------------------- Methods handling event generation and processing  ------------------
//    
       
    void processEvent(EventInfo ei)
    {
        String      sender = ei.getSender();
       
        //
        //  Skip any events that we sent.
        //
        if (sender == null || !sender.equals(mId))
        {
            if (ei instanceof RegistrationInfo)
            {
                handleRegistrationEvent((RegistrationInfo)ei);
            }
            else if (ei instanceof ConnectionsInfo)
            {
                handleConnectionsEvent((ConnectionsInfo)ei);
            }
            else if (ei instanceof HelloInfo)
            {
                handleHelloEvent((HelloInfo)ei);
            }
            else if (ei instanceof JoinInfo)
            {
                handleJoinEvent((JoinInfo)ei);
            }
            else if (ei instanceof LeaveInfo)
            {
                handleLeaveEvent((LeaveInfo)ei);
            }
        }
    }
    
   
    void handleRegistrationEvent(RegistrationInfo ri)
    {
        try
        {
            if (ri.getAction().equals(RegistrationInfo.ACTION_ADD))
            {
                mLog.fine("PB:Add registration (" + ri.getServiceName() + "," + ri.getEndpointName() + "," + ri.getInstanceId() + ")");
                mPB.addRemoteEndpoint(ri);
            }
            else if (ri.getAction().equals(RegistrationInfo.ACTION_REMOVE))
            {
                mLog.fine("PB:Remove registration (" + ri.getServiceName() + "," + ri.getEndpointName() + "," + ri.getInstanceId() + ")");
                mPB.removeRemoteEndpoint(ri);
            }
            else
            {
                mLog.fine("PB:Unknown registration event action (" + ri.getAction() + ")");
            }
        }
        catch (javax.jbi.JBIException mEx)
        {
            mLog.warning(Translator.translate(LocalStringKeys.EP_REGISTER_EXCEPTION, mEx));
        }
    }
    
    void handleConnectionsEvent(ConnectionsInfo ci)
    {
        try
        {
            if (ci.getAction().equals(ConnectionsInfo.ACTION_ADD))
            {
                mLog.fine("PB:Add service connection (" + ci.getServiceLinkName() + "," +
                    ci.getEndpointLinkName() + "," + ci.getServiceName() + "," + ci.getEndpointName() +
                    ci.getServiceName() + "," + ci.getLink());
                mPB.addRemoteConnection(ci);
            }
            else if (ci.getAction().equals(ConnectionsInfo.ACTION_REMOVE))
            {
                mLog.fine("PB:Remove service connection (" + ci.getServiceLinkName() + "," +
                    ci.getEndpointLinkName() + "," + ci.getServiceName() + "," + ci.getEndpointName() +
                    ci.getServiceName());
                mPB.removeRemoteConnection(ci);
            }
            else
            {
                mLog.fine("PB:Unknown connection event action (" + ci.getAction() + ")");
            }
        }
        catch (javax.jbi.JBIException mEx)
        {
            mLog.warning(Translator.translate(LocalStringKeys.EP_CONNECTION_EXCEPTION, mEx));
        }
    }
    
    void handleHelloEvent(HelloInfo hi)
    {
        mLog.fine("PB:Hello from " + hi.getInstanceId() + "\n");

        JoinInfo   ji = new JoinInfo(mPB.getInstanceId(), mPB.getLocalEndpoints());
        sendEventTo(ji, hi.getInstanceId());
    }
    
    void handleJoinEvent(JoinInfo ji)
    {
        try
        {
            mLog.fine("PB:Join from " + ji.getInstanceId() + "\n");

            LinkedList<RegistrationInfo> eps = ji.getEndpoints();

            for (RegistrationInfo ri : eps)
            {
                mPB.addRemoteEndpoint(ri);
            }                
        }
        catch (javax.jbi.JBIException mEx)
        {
            mLog.warning(Translator.translate(LocalStringKeys.EP_JOIN_EXCEPTION, mEx));
        }
    }
    
    void handleLeaveEvent(LeaveInfo li)
    {
        mLog.fine("PB:Leave from " + li.getInstanceId() + "\n");
        mPB.purgeRemoteEndpointsForInstance(li.getInstanceId());
        mPB.purgeRemoteConnectionsForInstance(li.getInstanceId());
    }
    
    void sendEvent(EventInfo ei)
    {
        try
        {
            mLog.fine("PB:Send event " + ei.getEventName() + "\n");

            mEventsSent++;
            mEC.sendEvent(ei);
        }
        catch (com.sun.jbi.binding.proxy.connection.EventException eEx)
        {
            mLog.warning(Translator.translate(LocalStringKeys.EP_SEND_EXCEPTION, eEx));
        }
    }
    
    void sendEventTo(EventInfo ei, String instance)
    {
        try
        {
            mLog.fine("PB:Send event " + ei.getEventName() + " To " + instance + "\n");
            mEventsSent++;
            mEC.sendEventTo(ei, instance);
        }
        catch (com.sun.jbi.binding.proxy.connection.EventException eEx)
        {
            mLog.warning(Translator.translate(LocalStringKeys.EP_SENDTO_EXCEPTION, eEx));
        }
    }
    
    public String toString()
    {
        StringBuffer        sb = new StringBuffer();
        SimpleDateFormat    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        
        sb.append("  Event Processor\n");
        sb.append("    Events         Sent(");
        sb.append(mEventsSent);
        sb.append(")  Received(");
        sb.append(mEventsReceived);
        sb.append(")\n    Instances Count(");
        sb.append(mInstances.size());
        sb.append(")\n");
        for (Iterator i = mInstances.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("    Wait Instances Count(");
        sb.append(mWaitInstances.size());
        sb.append(")\n");
        if (mWaitInstances.size() > 0)
        {
            for (Iterator i = mWaitInstances.values().iterator(); i.hasNext();)
            {
                sb.append(i.next().toString());
            }
        }
        return (sb.toString());
    }
}
