/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSBindingResolver.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import java.util.Hashtable;

import javax.jbi.servicedesc.ServiceEndpoint;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Resolver implementation for the JMS binding.
 *
 * @author Sun Microsystems Inc.
 */
public class JMSBindingResolver
{
    /**
     *  Description cache.
     */
    private Hashtable mDescriptorCache;

    /**
     * Creates a new JMSBindingResolver object.
     */
    public JMSBindingResolver()
    {
        mDescriptorCache = new Hashtable();
    }

    /**
     * Returns the service description.
     *
     * @param ef enpoint reference.
     *
     * @return document object.
     */
    public Document getServiceDescription(ServiceEndpoint ef)
    {
        try
        {
            Document df =
                (Document) mDescriptorCache.get(ef.getServiceName().toString()
                    + ef.getEndpointName());

            /*org.w3c.dom.Document d =
               DocumentBuilderFactory.newInstance().newDocumentBuilder()
                                     .newDocument();
               Node n = d.importNode(df, true);
                d.appendChild(n);
             */
            return df;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /**
     * Adds an endpoint document.
     *
     * @param epname endpoint name.
     * @param filename wsdl file.
     */
    public void addEndpointDoc(
        String epname,
        String filename)
    {
        if ((epname != null) && (filename != null))
        {
            try
            {
               org.w3c.dom.Document d =
                    DocumentBuilderFactory.newInstance().
                            newDocumentBuilder().parse(filename);
                mDescriptorCache.put(epname, d);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }    
    
    /**
     * Adds an endpoint document.
     *
     * @param epname endpoint name.
     * @param df document fragment.
     */
    public void addEndpointDoc(
        String epname,
        Document df)
    {
        if ((epname != null) && (df != null))
        {
            try
            {
                mDescriptorCache.put(epname, df);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Clears the cache.
     */
    public void clearCache()
    {
        mDescriptorCache = new Hashtable();
    }
}
