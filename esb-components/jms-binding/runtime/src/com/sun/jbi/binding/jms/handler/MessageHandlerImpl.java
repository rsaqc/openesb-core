/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageHandlerImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;
import com.sun.jbi.binding.jms.MessageRegistry;

import com.sun.jbi.binding.jms.mq.MQDestination;
import com.sun.jbi.binding.jms.mq.MQManager;
import com.sun.jbi.binding.jms.mq.MQSession;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import com.sun.jbi.binding.jms.util.UtilBase;

import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.jms.Message;

import javax.xml.namespace.QName;

/**
 * Implementation for a message handler.
 *
 * @author Sun Microsystems Inc.
 */
class MessageHandlerImpl
    extends UtilBase
    implements MessageHandler, JMSBindingResources
{
    /**
     *  Logger.
     */
    protected Logger mLogger;
    /**
     *  Translator.
     */
    protected StringTranslator mStringTranslator;
    /**
     *  Bean.
     */
    private EndpointBean mBean;
    /**
     * Message.
     */
    private Message mMessage;
    /**
     *  Message exchange.
     */
    private MessageExchange mExchange;
    /**
     * Current operation.
     */
    private QName mCurrentOperation;
    /**
     * Creates a new MessageHandlerImpl object.
     */
    public MessageHandlerImpl()
    {
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Gets the artifact.
     *
     * @param id id.
     *
     * @return artifact object.
     */
    public Object getArtifact(String id)
    {
        MessageRegistry reg =
            JMSBindingContext.getInstance().getMessageRegistry();

        return reg.getObject(id);
    }

    /**
     * Setter for property mBean.
     *
     * @param mBean New value of property mBean.
     */
    public void setBean(EndpointBean mBean)
    {
        this.mBean = mBean;
    }

    /**
     * Getter for property mBean.
     *
     * @return Value of property mBean.
     */
    public EndpointBean getBean()
    {
        return mBean;
    }

    /**
     * Sets the current operation.
     *
     * @param op operation q name.
     */
    public void setCurrentOperation(QName op)
    {
        mCurrentOperation = op;
    }

    /**
     * Gets the current operation.
     *
     * @return current operation.
     */
    public QName getCurrentOperation()
    {
        return mCurrentOperation;
    }

    /**
     * Sets the error.
     *
     * @param err error string.
     */
    public void setError(String err)
    {
        super.setError(err);
    }

    /**
     * Gets the error.
     *
     * @return error string.
     */
    public String getError()
    {
        return super.getError();
    }

    /**
     * Sets the exception.
     *
     * @param ex exception.
     */
    public void setException(Exception ex)
    {
        super.setException(ex);
    }

    /**
     * Gets the exception.
     *
     * @return exception.
     */
    public Exception getException()
    {
        return super.getException();
    }

    /**
     * Sets the JMS message.
     *
     * @param msg JMSmesage.
     */
    public void setJMSMessage(Message msg)
    {
        mMessage = msg;
    }

    /**
     * Gets the JMS message.
     *
     * @return JMS message.
     */
    public Message getJMSMessage()
    {
        return mMessage;
    }

    /**
     *Sets the NMR headers.
     */
    public void setNMSHeaders()
    {
        QName servicename =
            new QName(mBean.getValue(ConfigConstants.SERVICE_NAMESPACE),
                mBean.getValue(ConfigConstants.SERVICENAME));
        mLogger.fine("Namespace "
            + mBean.getValue(ConfigConstants.SERVICE_NAMESPACE));

        mLogger.fine("Service :" + mBean.getValue(ConfigConstants.SERVICENAME));

        mLogger.fine("Endpoint :"
            + mBean.getValue(ConfigConstants.ENDPOINTNAME));

        String endpointname = mBean.getValue(ConfigConstants.ENDPOINTNAME);
        ServiceEndpoint ref = null;

        try
        {
            ref = mBean.getServiceEndpoint();

            if (ref == null)
            {
                setServiceEndpoint();
                ref = mBean.getServiceEndpoint();

                if (ref == null)
                {
                    mExchange = null;
                    mLogger.severe(mStringTranslator.getString(
                            JMS_CANNOT_GET_SERVICE, servicename.toString()));
                    setError(mStringTranslator.getString(
                            JMS_CANNOT_GET_SERVICE, servicename.toString()));

                    return;
                }
            }
            
            QName iqname = new QName(mBean.getValue(ConfigConstants.INTERFACE_NAMESPACE),
                mBean.getValue(ConfigConstants.INTERFACE_LOCALNAME));
            mExchange.setService(mBean.getServiceEndpoint().getServiceName());
            mExchange.setEndpoint(ref);
            mExchange.setInterfaceName(iqname);
            mExchange.setOperation(getCurrentOperation());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setException(e);
        }
    }

    /**
     * Sets the NMR message.
     *
     * @param exch NMR msg.
     */
    public void setNMSMessage(MessageExchange exch)
    {
        mExchange = exch;
    }

    /**
     * Gets the nMR message.
     *
     * @return NMR msg.
     */
    public MessageExchange getNMSMessage()
    {
        return mExchange;
    }

    /**
     * Get session.
     *
     * @return MQ session.
     */
    public MQSession getSession()
    {
        MQSession session = null;

        while (session == null)
        {
            //this sshud be wait / notify not buys waiting. 
            // will change it soon
            session =
                JMSBindingContext.getInstance().getMQManager().getSession(false,
                    mBean);
        }

        try
        {
            session.init();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setException(e);
            JMSBindingContext.getInstance().getMQManager().releaseSession(session);
            session = null;
        }

        return session;
    }

    /**
     * Checks if the ME can be terminated.
     *
     * @return true if can terminate.
     */
    public boolean canTerminateConsumer()
    {
        if (mExchange.getPattern().toString().equals(ConfigConstants.IN_ONLY))
        {
            if (mExchange.getStatus() == ExchangeStatus.DONE)
            {
                mLogger.fine(mStringTranslator.getString(JMS_INONLY_DONE,
                        mExchange.getExchangeId()));
            }
            else
            {
                mLogger.fine(mStringTranslator.getString(JMS_INONLY_ERROR,
                        mExchange.getExchangeId()));
            }

            //Update message history here         
            return true;
        }

        if (mExchange.getPattern().toString().equals(ConfigConstants.ROBUST_IN_ONLY))
        {
            if (mExchange.getStatus() == ExchangeStatus.DONE)
            {
                mLogger.fine(mStringTranslator.getString(
                        JMS_ROBUSTINONLY_DONE, mExchange.getExchangeId()));

                //Update message history here
                return true;
            }

            else
            {
                mLogger.fine(mStringTranslator.getString(
                        JMS_ROBUSTINONLY_DONE, mExchange.getExchangeId()));

                return false;
            }
        }

        if (mExchange.getPattern().toString().equals(ConfigConstants.IN_OPTIONAL_OUT))
        {
            mLogger.info(mStringTranslator.getString(JMS_INOPTOUT_SUPPORT,
                    mExchange.getExchangeId()));

            return true;
        }

        return false;
    }

    /**
     * Can or cannot end a exchange.
     *
     * @return true if can terminate.
     */
    public boolean canTerminateProvider()
    {
        if (mExchange.getPattern().toString().equals(ConfigConstants.ROBUST_IN_ONLY))
        {
            if (mExchange.getStatus() == ExchangeStatus.DONE)
            {
                mLogger.fine(mStringTranslator.getString(
                        JMS_ROBUSTINONLY_DONE, mExchange.getExchangeId()));

                //Update message history here
                return true;
            }
            else if (mExchange.getStatus() == ExchangeStatus.ERROR)
            {
                mLogger.fine(mStringTranslator.getString(
                        JMS_ROBUSTINONLY_ERROR, mExchange.getExchangeId()));

                //Update message history here
                return true;
            }
            else
            {
                return false;
            }
        }

        if (mExchange.getPattern().toString().equals(ConfigConstants.IN_OUT))
        {
            if (mExchange.getStatus() == ExchangeStatus.DONE)
            {
                mLogger.fine(mStringTranslator.getString(JMS_INOUT_DONE,
                        mExchange.getExchangeId()));

                //Update message history here
                return true;
            }
            else if (mExchange.getStatus() == ExchangeStatus.ERROR)
            {
                mLogger.fine(mStringTranslator.getString(JMS_INOUT_ERROR,
                        mExchange.getExchangeId()));

                //Update message history here
                return true;
            }
            else
            {
                return false;
            }
        }

        if (mExchange.getPattern().toString().equals(ConfigConstants.IN_OPTIONAL_OUT))
        {
            mLogger.info(mStringTranslator.getString(JMS_INOPTOUT_SUPPORT,
                    mExchange.getExchangeId()));

            return true;
        }

        return false;
    }

    /**
     * Clear.
     */
    public void clear()
    {
        super.clear();
    }

    /**
     * Deregisters the Message.
     *
     * @param id message id.
     */
    public void deRegisterMessage(String id)
    {
        MessageRegistry reg =
            JMSBindingContext.getInstance().getMessageRegistry();
        reg.deregisterExchange(id);
    }

    /**
     *
     */
    public void execute()
    {
        mLogger.fine(mStringTranslator.getString(JMS_HANDLER_BASE));

        /* do sanity first
         */
    }

    /**
     * 
     *
     * @param id 
     * @param ob 
     */
    public void registerMessage(
        String id,
        Object ob)
    {
        MessageRegistry reg =
            JMSBindingContext.getInstance().getMessageRegistry();
        reg.registerExchange(id, ob);
    }

    /**
     * Method that sends any message exchnage on the channel.
     *
     * @param msg exchange to be sent
     *
     * @return true if success else false
     */
    public boolean send(MessageExchange msg)
    {
        /*
         * Send message mSendMessage
         * register in message registry
         * start timer -1 indicates no timer
         */
        DeliveryChannel channel = JMSBindingContext.getInstance().getChannel();

        if (msg == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_NULL_EXCHANGE));
            setError(getError() + "\n"
                + mStringTranslator.getString(JMS_NULL_EXCHANGE));

            return false;
        }

        try
        {
            channel.send(msg);
        }
        catch (MessagingException me)
        {
            me.printStackTrace();
            setException(me);

            return false;
        }

        return true;
    }

    /**
     * 
     *
     * @param msg 
     * @param dest 
     * @param session 
     *
     * @return 
     */
    public boolean send(
        Message msg,
        Object dest,
        MQSession session)
    {
        if (dest == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_NO_DESTINATION));
            setError(mStringTranslator.getString(JMS_NO_DESTINATION));
            JMSBindingContext.getInstance().getMQManager().releaseSession(session);

            return false;
        }
        MQDestination destination = null;
        destination =
                JMSBindingContext.getInstance().getMQManager().getDestination(dest);
 
        if (!session.sendMessage(msg, destination))
        {
            mLogger.severe(mStringTranslator.getString(JMS_CANNOT_SEND)
                + session.getError());
            setError(mStringTranslator.getString(JMS_CANNOT_SEND)
                + session.getError());
            JMSBindingContext.getInstance().getMQManager().releaseSession(session);

            return false;
        }

        JMSBindingContext.getInstance().getMQManager().releaseSession(session);
        destination = null;
        return true;
    }

    /**
     * 
     *
     * @return 
     */
    public boolean sendJMSMessage()
    {
        return false;
    }

    /**
     * 
     *
     * @return 
     */
    public boolean sendNMSMessage()
    {
        return false;
    }

    /**
     *
     */
    public void sendNMSStatus()
    {
        Exception exp = getException();

        try
        {
            if (exp != null)
            {
                mLogger.fine("**ERROR STATUS**");
                mExchange.setError(exp);
            }
            else
            {
                mLogger.fine("**DONE STATUS** " + mExchange.getExchangeId());
                mExchange.setStatus(ExchangeStatus.DONE);
            }

            send(mExchange);
        }

        catch (Exception e)
        {
            mLogger.severe(mStringTranslator.getString(JMS_CANNOT_SEND_STATUS,
                    mExchange.getExchangeId()));
        }
    }

    /**
     *
     */
    private void setServiceEndpoint()
    {
        /**
         * Always try to get an endpoint only before sending this gives the
         * maximum probability of finding an endpoint, also we give an engine
         * enough time to activate the  endpoint
         */
        try
        {
            DeliveryChannel chnl = JMSBindingContext.getInstance().getChannel();
            QName qname =
                new QName(mBean.getValue(ConfigConstants.SERVICE_NAMESPACE),
                    mBean.getValue(ConfigConstants.SERVICENAME));
            QName iname =
                new QName(mBean.getValue(ConfigConstants.INTERFACE_NAMESPACE),
                    mBean.getValue(ConfigConstants.INTERFACE_LOCALNAME));
            ServiceEndpoint [] ref =
                JMSBindingContext.getInstance().getContext()
                                 .getEndpointsForService(qname);
            ServiceEndpoint tmp = null;

            if ((ref == null) || (ref.length == 0))
            {
                ref = JMSBindingContext.getInstance().getContext().getEndpoints(iname);
            }

            if ((ref == null) || (ref.length == 0))
            {
                throw new Exception("Cannot locate endpoint " + qname);
            }
            else
            {
                tmp = ref[0];

                for (int k = 0; k < ref.length; k++)
                {
                    if (ref[k].getEndpointName().trim().equals(mBean.getValue(
                                    ConfigConstants.ENDPOINTNAME)))
                    {
                        tmp = ref[k];
                    }
                }
            }

            mBean.setServiceEndpoint(tmp);
        }
        catch (Exception me)
        {
            me.printStackTrace();
        }
    }
}
