/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDLFileReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResolver;
import com.sun.jbi.binding.jms.JMSBindingResources;


import com.sun.jbi.binding.jms.config.ConfigConstants;
import com.sun.jbi.wsdl2.Binding;
import com.sun.jbi.wsdl2.BindingOperation;
import com.sun.jbi.wsdl2.Description;

import com.sun.jbi.wsdl2.Endpoint;
import com.sun.jbi.wsdl2.Interface;
import com.sun.jbi.wsdl2.InterfaceOperation;


import java.util.LinkedList;
import java.util.List;

import java.util.logging.Logger;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Reads the configuration file file.xml and loads the data. into the
 * EndpointBean objects.
 *
 * @author Sun Microsystems Inc.
 */
public class WSDLFileReader
    extends com.sun.jbi.binding.jms.util.UtilBase
    implements JMSBindingResources
{
    /**
     * Description document.
     */
    private Description mDefinition;

    /**
     * Document object of the XML config file.
     */
    private Document mDoc;
    /**
     * Resolver.
     */
    private JMSBindingResolver mResolver;
    /**
     * Logger Object.
     */
    private Logger mLogger;
    /**
     * Translator.
     */
    private StringTranslator mStringTranslator;
    /**
     * The list of endpoints as configured in the config file. This list
     * contains all the encpoints and their attibutes.
     */
    private EndpointBean [] mEndpointInfoList = null;

    /**
     * The total number of end points in the config file.
     */
    private int mTotalEndpoints = 0;

    /**
     * Creates a new WSDLFileReader object.
     *
     * @param rslv resolver object
     */
    public WSDLFileReader(JMSBindingResolver rslv)
    {
        setValid(true);
        mResolver = rslv;
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Returns the Bean object corresponding to the endpoint.
     *
     * @param endpoint endpoint name.
     *
     * @return endpoint Bean object.
     */
    public EndpointBean getBean(String endpoint)
    {
        /*Search for the bean corresponding to the service and endpoint name
         */
        for (int j = 0; j < mEndpointInfoList.length; j++)
        {
            String tmp = mEndpointInfoList[j].getUniqueName();

            if (tmp.trim().equals(endpoint))
            {
                return mEndpointInfoList[j];
            }
        }

        return null;
    }

    /**
     * Gets the endpoint list corresponding to a config file.
     *
     * @return End point Bean list
     */
    public EndpointBean [] getEndpoint()
    {
        return mEndpointInfoList;
    }

    /**
     * Returns the total number of endopoints in the config file.
     *
     * @return int number of endpoints.
     */
    public int getEndpointCount()
    {
        return mTotalEndpoints;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Description doc)
    {
        mDefinition = doc;
        loadServicesList();
    }

    /**
     * Gets the value for an attribute.
     *
     * @param map map object.
     * @param attr attribute.
     *
     * @return value.
     */
    private String getAttrValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Sets the con factory.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setConnectionFactory(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.CONNECTION_FACTORY);
        eb.setValue(ConfigConstants.CONNECTION_FACTORY, val);
    }

    /**
     * Sets the connection password.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setConnectionPassword(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.CONNECTION_PASSWORD);
        eb.setValue(ConfigConstants.CONNECTION_PASSWORD, val);
    }

    /**
     * Sets connection user name.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setConnectionUsername(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.CONNECTION_USER_ID);
        mLogger.fine("User ID " + val);
        eb.setValue(ConfigConstants.CONNECTION_USER_ID, val);
    }

    /**
     * Sets destination name.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setDestinationName(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.DESTINATION_NAME);

        if ((val == null) || (val.trim().equals("")))
        {
            setError(mStringTranslator.getString(JMS_DESTINATION_NAME_NULL));
        }

        eb.setValue(ConfigConstants.DESTINATION_NAME, val);
    }

    /**
     * Sets the destination style.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setDestinationStyle(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.DESTINATION_STYLE);
        eb.setValue(ConfigConstants.DESTINATION_STYLE, val);

        if (val.trim().equalsIgnoreCase(ConfigConstants.QUEUE_STRING))
        {
            eb.setStyle(ConfigConstants.QUEUE);
        }
        else
        {
            eb.setStyle(ConfigConstants.TOPIC);
        }
    }

    /**
     * Sets durability.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setDurability(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.DURABILITY);

        if (val == null)
        {
            val = ConfigConstants.DEFAULT_DURABILITY;
        }

        eb.setValue(ConfigConstants.DURABILITY, val);
    }

    /**
     * Gets the node array.
     *
     * @param nodeList node list.
     * @param name element name.
     *
     * @return node array.
     */
    private Node [] getElementNodeArrayByName(
        NodeList nodeList,
        String name)
    {
        Node [] retNode = null;
        List nodeArr = new LinkedList();

        //nodeList will have one element for binding. Get all child elements for binding
        //loop thorugh them to find operation elements
        NodeList tmpNodeList = nodeList.item(0).getChildNodes();

        for (int g = 0; g < tmpNodeList.getLength(); g++)
        {
            Node n = tmpNodeList.item(g);

            if ((n != null) && (n.getNodeType() == Node.ELEMENT_NODE))
            {
                if ((n.getLocalName().equals(name)))
                {
                    nodeArr.add(n);
                }
            }
        }

        // for loop
        retNode = new Node[nodeArr.size()];

        for (int k = 0; k < nodeArr.size(); k++)
        {
            retNode[k] = (Node) nodeArr.get(k);
        }

        return retNode;
    }

    /**
     * Mehotd to determin if the binding information in the Document fragment
     * is a file binding or not.
     *
     * @param df document fragment.
     *
     * @return true if JMS binding.
     */
    private boolean isJMSBinding(DocumentFragment df)
    {
        NamedNodeMap n = df.getFirstChild().getAttributes();

        /* can replace below code once the "types" attribute of binding
         *is in place
         */
        for (int g = 0; g < n.getLength(); g++)
        {
            if (n.item(g).getLocalName().equals("type"))
            {
                if (n.item(g).getNodeValue().equals(ConfigConstants.JMS_NAMESPACE))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Sets the message selector.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setMessageSelector(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.MESSAGE_SELECTOR);

        if (val == null)
        {
            val = "";
        }

        eb.setValue(ConfigConstants.MESSAGE_SELECTOR, val);
    }

    /**
     * Sets the operation.
     *
     * @param eb endpoint bean.
     * @param b binding.
     */
    private void setOperations(
        EndpointBean eb,
        Binding b)
    {
        Interface bindingif = b.getInterface();
        Node [] allOpNodes =
            getElementNodeArrayByName(b.toXmlDocumentFragment().getChildNodes(),
                "operation");

        for (int oper = 0; oper < b.getOperationsLength(); oper++)
        {
            BindingOperation boper = b.getOperation(oper);
            QName bindingopername = boper.getInterfaceOperation();

            String input = null;
            String output = null;
            String mep = null;

            for (int k = 0; k < allOpNodes.length; k++)
            {
                NamedNodeMap nm = allOpNodes[k].getAttributes();

                if (nm != null)
                {
                    String val = getAttrValue(nm, "ref");

                    if ((val != null)
                            && val.equals(bindingopername.getLocalPart().trim()))
                    {
                        input =
                            getValue(nm, ConfigConstants.INPUT_MESSAGE_TYPE);
                        output =
                            getValue(nm, ConfigConstants.OUTPUT_MESSAGE_TYPE);

                        break;
                    }
                }
            }

            if ((input == null) || (input.trim().equals("")))
            {
                input = ConfigConstants.DEFAULT_INPUT_MESSAGE_TYPE;
            }

            if ((output == null) || (output.trim().equals("")))
            {
                output = ConfigConstants.DEFAULT_OUTPUT_MESSAGE_TYPE;
            }

            int ifoperlength = bindingif.getOperationsLength();

            for (int j = 0; j < ifoperlength; j++)
            {
                InterfaceOperation op = bindingif.getOperation(j);
                QName ifopername = op.getQualifiedName();

                if (bindingopername.getLocalPart().trim().equals(ifopername.getLocalPart()
                                                                               .trim()))
                {
                    mep = op.getPattern();                    
                    checkMEP(mep);
                    if (mep != null)
                    {
                        if (operationAllowed(eb, mep))
                        {
                            eb.addOperation(ifopername.getNamespaceURI(),
                                ifopername.getLocalPart(), mep, input, output);
                        }
                        else
                        {
                            mLogger.info("operation inconsistent with style");
                            setWarning(
                                "Operation inconsistent with Destination style"
                                + ifopername.toString() + " MEP " + mep);
                        }
                    }
                }
            }
        }
    }

    /**
     * Sets the reply to.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setReplyTo(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.REPLY_TO);

        if (val == null)
        {
            val = "";
        }

        eb.setValue(ConfigConstants.REPLY_TO, val);
    }

    /**
     * Sets the role.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setRole(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String role = getValue(map, ConfigConstants.ENDPOINT_TYPE);

        if (role == null)
        {
            eb.setRole(ConfigConstants.CONSUMER);
        }
        else
        {
            if (role.trim().equalsIgnoreCase(ConfigConstants.PROVIDER_STRING))
            {
                eb.setRole(ConfigConstants.PROVIDER);
            }
            else if (role.trim().equalsIgnoreCase(ConfigConstants.CONSUMER_STRING))
            {
                eb.setRole(ConfigConstants.CONSUMER);
            }
            else
            {
                setError("Endpoint should be Provider or Consumer");

                return;
            }
        }
        eb.setValue(ConfigConstants.ENDPOINT_TYPE, role);
    }

    /**
     * Sets the time to live.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setTimetoLive(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.TIME_TO_LIVE);
        long l;

        try
        {
            l = Long.parseLong(val);
        }
        catch (Exception e)
        {
            setError(mStringTranslator.getString(JMS_INVALID_TIMEOUT));

            return;
        }

        eb.setValue(ConfigConstants.TIME_TO_LIVE, val);
    }

    /**
     * Gets value of an attribute form the map.
     *
     * @param map map.
     * @param attr attribute.
     *
     * @return attribute value.
     */
    private String getValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItemNS(ConfigConstants.JMS_NAMESPACE, attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Loads the endpoint bean.
     *
     * @param ep endpoint reference.
     * @param eb endpoint bean.
     */
    private void loadEndpointBean(
        Endpoint ep,
        EndpointBean eb)
    {
        DocumentFragment docfrag = ep.toXmlDocumentFragment();
        Node epnode = docfrag.getFirstChild();
        NamedNodeMap epattributes = epnode.getAttributes();
        setRole(epattributes, eb);
        setDestinationName(epattributes, eb);
        setDestinationStyle(epattributes, eb);
        setDurability(epattributes, eb);
        setConnectionFactory(epattributes, eb);
        setConnectionUsername(epattributes, eb);
        setConnectionPassword(epattributes, eb);
        setMessageSelector(epattributes, eb);
        setTimetoLive(epattributes, eb);
        setReplyTo(epattributes, eb);

        if (!isValid())
        {
            setError("\n\tEndpoint : " + ep.getName() + getError());
        }
    }

    /**
     * Parses the config files and loads them into bean objects.
     */
    private void loadServicesList()
    {
        int services = mDefinition.getServicesLength();
        List eplist = new LinkedList();

        for (int i = 0; i < services; i++)
        {
            try
            {
                com.sun.jbi.wsdl2.Service ser = mDefinition.getService(i);
                int endpoints = ser.getEndpointsLength();

                for (int k = 0; k < endpoints; k++)
                {
                    Endpoint ep = ser.getEndpoint(k);
                    Binding b = ep.getBinding();

                    if (!isJMSBinding(b.toXmlDocumentFragment()))
                    {
                        continue;
                    }

                    EndpointBean eb = new EndpointBean();
                    eb.setWsdlDefinition(mDefinition);
                    eb.setDeploymentType("WSDL20");
                  
                    loadEndpointBean(ep, eb);

                    eb.setValue(ConfigConstants.SERVICE_NAMESPACE,
                        ser.getQName().getNamespaceURI());
                    eb.setValue(ConfigConstants.SERVICENAME,
                        ser.getQName().getLocalPart());
                    eb.setValue(ConfigConstants.ENDPOINTNAME, ep.getName());
                    eb.setValue(ConfigConstants.INTERFACE_LOCALNAME,
                        ep.getBinding().getInterface().getQName().getLocalPart());
                    eb.setValue(ConfigConstants.INTERFACE_NAMESPACE,
                        ep.getBinding().getInterface().getQName()
                        .getNamespaceURI());
                    setOperations(eb, b);
                    if (!isValid())
                    {
                        setError(("\n\t" + "Service : "
                            + ser.getQName().toString()));
                        eplist.clear();

                        return;
                    }

                    eplist.add(eb);
                    mResolver.addEndpointDoc(ser.getQName().toString()
                        + ep.getName(), mDefinition.toXmlDocument());
                }
            }
            catch (Throwable e)
            {
                setError(mStringTranslator.getString(
                        JMS_ENDPOINT_VALIDATION_ERROR, e.getMessage()));

                //setException(e);
                e.printStackTrace();

                return;
            }
        }

        mEndpointInfoList = new EndpointBean[eplist.size()];

        for (int x = 0; x < eplist.size(); x++)
        {
            mEndpointInfoList[x] = (EndpointBean) eplist.get(x);
        }

        mTotalEndpoints = eplist.size();
    }

    /**
     * Checks if the mep is valid for the style.
     *
     * @param eb endpoint bean.
     * @param mep mep.
     *
     * @return true if valid.
     */
    private boolean operationAllowed(
        EndpointBean eb,
        String mep)
    {
        int style = eb.getStyle();
        boolean allowed = true;

        if (style == ConfigConstants.TOPIC)
        {
            if (mep.trim().equals(ConfigConstants.IN_OUT)
                    || mep.trim().equals(ConfigConstants.IN_OPTIONAL_OUT)
                    || mep.trim().equals(ConfigConstants.OUT_IN)
                    || mep.trim().equals(ConfigConstants.OUT_OPTIONAL_IN)
                    || mep.trim().equals(ConfigConstants.ROBUST_OUT_ONLY))
            {
                allowed = false;
            }
        }

        return allowed;
    }
    
    /**
     * Checks the MEP.
     *
     * @param pattern.
     */
    
    private void checkMEP(String pattern)
    {
        if (pattern != null)
        {
            if (pattern.trim().equals(ConfigConstants.IN_OUT)
             || pattern.trim().equals(ConfigConstants.IN_OPTIONAL_OUT)
             || pattern.trim().equals(ConfigConstants.OUT_IN)
             || pattern.trim().equals(ConfigConstants.OUT_OPTIONAL_IN)
             || pattern.trim().equals(ConfigConstants.ROBUST_OUT_ONLY)
             || pattern.trim().equals(ConfigConstants.IN_ONLY)
             || pattern.trim().equals(ConfigConstants.OUT_ONLY)
             || pattern.trim().equals(ConfigConstants.ROBUST_IN_ONLY))
            {
                ;
            }
            else
            {
                setError("MEP " + pattern + 
                    " is not a valid WSDL message exchange pattern");
            }
        }

    }
}
