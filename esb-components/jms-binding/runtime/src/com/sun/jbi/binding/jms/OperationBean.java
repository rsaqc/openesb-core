/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OperationBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import javax.xml.namespace.QName;

/**
 * Stores the data related to an operation.
 *
 * @author Sun Microsystems Inc.
 */
public final class OperationBean
{
    /**
     * Input type.
     */
    private String mInputType;
    /**
     * MEP.
     */
    private String mMep;
    /**
     * Operation name.
     */
    private String mName;

    /**
     * Operation namespace.
     */
    private String mNamespace;
    /**
     * Output type.
     */
    private String mOutputType;

    /**
     * Creates a new OperationBean object.
     */
    public OperationBean()
    {
    }

    /**
     * Creates a new OperationBean object.
     *
     * @param namespace namespace.
     * @param name name.
     * @param mep mep.
     * @param input input type.
     * @param output putput type.
     */
    public OperationBean(String namespace, String name, String mep, 
                         String input, String output)
    {
        this.mName = name;
        this.mMep = mep;
        this.mInputType = input;
        this.mOutputType = output;
        this.mNamespace = namespace;
    }

    /**
     * Setter for property mInputType.
     *
     * @param mInputType New value of property mInputType.
     */
    public void setInputType(java.lang.String mInputType)
    {
        this.mInputType = mInputType;
    }

    /**
     * Getter for property mInputType.
     *
     * @return Value of property mInputType.
     */
    public java.lang.String getInputType()
    {
        return mInputType;
    }

    /**
     * Setter for property mMep.
     *
     * @param mMep New value of property mMep.
     */
    public void setMep(java.lang.String mMep)
    {
        this.mMep = mMep;
    }

    /**
     * Getter for property mMep.
     *
     * @return Value of property mMep.
     */
    public java.lang.String getMep()
    {
        return mMep;
    }

    /**
     * Setter for property mName.
     *
     * @param mName New value of property mName.
     */
    public void setName(java.lang.String mName)
    {
        this.mName = mName;
    }

    /**
     * Getter for property mName.
     *
     * @return Value of property mName.
     */
    public java.lang.String getName()
    {
        return mName;
    }

    /**
     * Sets the namespace.
     *
     * @param namespace namespace.
     */
    public void setNamespace(String namespace)
    {
        this.mNamespace = namespace;
    }

    /**
     * Gets the operation namespace.
     *
     * @return  namespace.
     */
    public String getNamespace()
    {
        return this.mNamespace;
    }

    /**
     * Setter for property mOutputType.
     *
     * @param mOutputType New value of property mOutputType.
     */
    public void setOutputType(java.lang.String mOutputType)
    {
        this.mOutputType = mOutputType;
    }

    /**
     * Getter for property mOutputType.
     *
     * @return Value of property mOutputType.
     */
    public java.lang.String getOutputType()
    {
        return mOutputType;
    }

    /**
     * Gets the QName.
     *
     * @return  returns the operation Qname.
     */
    public String getQName()
    {
        QName qname = new QName(mNamespace, mName);

        return qname.toString();
    }
}
