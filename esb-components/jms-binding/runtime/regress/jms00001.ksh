#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jms00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#create a JBI test installation and start it up
echo Starting framework for tests
. ./regress_defs.ksh

#remove our old test installations BEFORE we restart the appserver:
rm -rf $JBI_DOMAIN_ROOT/jbi/bindings/SunJMSBinding
rm -rf $JBI_DOMAIN_ROOT/jbi/tempdeploy
#remove "pre-loaded" scaffolded installations:
rm -f $JBI_DOMAIN_ROOT/config/ComponentList.dat
touch $JBI_DOMAIN_ROOT/config/ComponentList.dat

#remove destinations if they exists already

#export JMS_ADMIN_USER
#export JMS_ADMIN_PASSWORD
export JMS_ADMIN_HOST
#export JMS_ADMIN_PORT
#JMS_ADMIN_USER="admin"
#JMS_ADMIN_PASSWORD="admin"
JMS_ADMIN_HOST="localhost"
#JMS_ADMIN_PORT="10176"
echo "Removing destinations"
$AS8BASE/imq/bin/imqcmd destroy dst -t q -n send_queue -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd destroy dst -t q -n receive_queue -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd destroy dst -t q -n temporary_queue -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd destroy dst -t q -n external_queue -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd destroy dst -t t -n send_topic  -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd destroy dst -t t -n receive_topic -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
# create physical destination queue factories and topics here
echo "Destinations destroyed"

echo "Creating destinations"
$AS8BASE/imq/bin/imqcmd create dst -t q -n send_queue -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT

$AS8BASE/imq/bin/imqcmd create dst -t q -n receive_queue -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd create dst -t q -n temporary_queue -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd create dst -t q -n external_queue -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd create dst -t t -n send_topic -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT
$AS8BASE/imq/bin/imqcmd create dst -t t -n receive_topic -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -f -s -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT

echo "Destinations created"

echo "Listing destinations"
$AS8BASE/imq/bin/imqcmd list dst -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT | grep send_queue
$AS8BASE/imq/bin/imqcmd list dst -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT | grep receive_queue
$AS8BASE/imq/bin/imqcmd list dst -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT | grep temporary_queue
$AS8BASE/imq/bin/imqcmd list dst -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT | grep external_queue
$AS8BASE/imq/bin/imqcmd list dst -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT | grep send_topic
$AS8BASE/imq/bin/imqcmd list dst -u $JMS_ADMIN_USER -passfile $JV_AS8BASE/imq-passwords -b $JMS_ADMIN_HOST:$JMS_ADMIN_PORT | grep receive_topic
echo "List destinations DONE"
rm -rf $JV_SVC_TEST_CLASSES/imqobjectstore
mkdir $JV_SVC_TEST_CLASSES/imqobjectstore

export PROVIDER_URL
PROVIDER_URL="file:///$JV_SVC_TEST_CLASSES/imqobjectstore"

# create object stores now

echo "Creating Lookup names in object store"
$AS8BASE/imq/bin/imqobjmgr add -t q -f -s -l "send_queue" -o "imqDestinationName=send_queue" -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"
$AS8BASE/imq/bin/imqobjmgr add -t q -f -s -l "receive_queue" -o "imqDestinationName=receive_queue" -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"
$AS8BASE/imq/bin/imqobjmgr add -t q -f -s -l "temporary_queue" -o "imqDestinationName=temporary_queue" -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"
$AS8BASE/imq/bin/imqobjmgr add -t q -f -s -l "external_queue" -o "imqDestinationName=external_queue" -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"
$AS8BASE/imq/bin/imqobjmgr add -t t -f -s -l "send_topic" -o "imqDestinationName=send_topic" -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"
$AS8BASE/imq/bin/imqobjmgr add -t t -f -s -l "receive_topic" -o "imqDestinationName=receive_topic" -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"
#connection factories , we need 2 one for q one for tpic

$AS8BASE/imq/bin/imqobjmgr add -t qf -f -s -l "MyQueueConnectionFactory" -r true -o "imqBrokerHostName=$JMS_ADMIN_HOST" -o "imqBrokerHostPort=$JMS_ADMIN_PORT" -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"
$AS8BASE/imq/bin/imqobjmgr add -t tf -f -s -l "MyTopicConnectionFactory" -r true -o "imqBrokerHostName=$JMS_ADMIN_HOST" -o "imqBrokerHostPort=$JMS_ADMIN_PORT" -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"

echo "Created lookup names"

echo "Listing lookup names"


$AS8BASE/imq/bin/imqobjmgr list -t q -f -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"| grep send_queue
$AS8BASE/imq/bin/imqobjmgr list -t q -f   -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"| grep receive_queue
$AS8BASE/imq/bin/imqobjmgr list -t q -f   -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"| grep temporary_queue
$AS8BASE/imq/bin/imqobjmgr list -t q -f   -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"| grep external_queue
$AS8BASE/imq/bin/imqobjmgr list -t t -f   -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"| grep send_topic
$AS8BASE/imq/bin/imqobjmgr list -t t -f    -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL"| grep receive_topic

$AS8BASE/imq/bin/imqobjmgr list -t qf -f   -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL" | grep MyQueueConnectionFactory
$AS8BASE/imq/bin/imqobjmgr list -t tf -f   -j "java.naming.factory.initial=com.sun.jndi.fscontext.RefFSContextFactory" -j "java.naming.provider.url=$PROVIDER_URL" | grep MyTopicConnectionFactory

echo "Listed lookup names"

# sleep 5

echo Started the Test framework
