<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)endpoints.xsd
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
     xmlns="http://www.sun.com/ns/jbi/bindings/jmsbinding/deploy/endpoint-config" 
     targetNamespace="http://www.sun.com/ns/jbi/bindings/jmsbinding/deploy/endpoint-config" 
     elementFormDefault="qualified">

<!-- Container for all the endpoints-->
<xs:element name="endpoint-list">
  <xs:complexType>
    <xs:sequence>      
    
    <!-- Holds information corresponding to each endpoint -->
    
      <xs:element name="endpoint" minOccurs="1" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
	   
          <!-- Service namespace is namespace this service corresponds to. This maps
           to the targetNamespace for a service in WSDL -->            
           
      	    <xs:element name="service" type="qnameType" minOccurs="1" maxOccurs="1"/>
            
            
      	    <xs:element name="interface" type="qnameType" minOccurs="1" maxOccurs="1"/>
            <!-- The name of this endpoint , in case of a provider endpoint it is
            a user defined value in case of a provider endpoint , for a consumer endpoint
            it could be one of the valid endpoints that are activated by Service engines.
            For a consumer , In case this endpoint name does not macth any of the 
            available endpoints, then a default endpoint for that service will be picked-->
            <!-- It is compulsory to give a name, and it is used by the binding as above-->
            
            <xs:element name="endpoint-name" type="xs:NCName" minOccurs="1" maxOccurs="1"/>
            
            <!-- This should be either "provider" or "consumer" and denotes the role 
            of this endpoint in a message exchange scenario -->             
            
            <xs:element name="endpoint-role" minOccurs="1" maxOccurs="1">
                <xs:simpleType>
			<xs:restriction base="xs:string">
				<xs:enumeration value="provider"/>
				<xs:enumeration value="consumer"/>							
  			</xs:restriction>
		</xs:simpleType>  
            </xs:element>
            
            <!-- Name of the queue or topic for this endpoint , should be created. Messages
            are received from this destination or sent to this destination depending on 
            the role of the endpoint-->
          
		    <!-- This is just a lookup name and the actual destination is created and configured
             by a JMS admin and stored as administered objects in the object store.
             This binding just does a lookup of this object in the store -->
            
            <xs:element name="destination-name" type="xs:string" minOccurs="1" maxOccurs="1"/>
            
            <!-- Should be Topic or Queue -->
	    	<xs:element name="destination-style" minOccurs="1" maxOccurs="1">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:enumeration value="Topic"/>
					<xs:enumeration value="Queue"/>							
  				</xs:restriction>
			</xs:simpleType>  
			</xs:element>

             <!-- In case of topics this indicates if the subscriber has to be durable -->
                
	    	<xs:element name="durabilty" minOccurs="0" maxOccurs="1">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:enumeration value="durable"/>
					<xs:enumeration value="non-durable"/>							
  				</xs:restriction>
			</xs:simpleType> 
		</xs:element>                    
                
           <!-- Lookup Name   of the connection factory using which a connection will
              be established with the provider for this endpoint. This has to be created and
              configured by a JMS admin and stored as administered objects in an Object store.
              What should be provided here is the lookup name, user id and password.
              -->
            <xs:element name="connection-params" type="connectionType" minOccurs="1" maxOccurs="1"/>	                
            
            <!-- SQl92 based message filter to filter incoming JMS messages.Please
            refer to JMS specification for details -->                        
            <xs:element name="message-selector" type="xs:string" minOccurs="0" maxOccurs="1"/>
	
            <!-- time to live for the JMS reply message or a JMS outbound message. This
            tim will be set in JMS header of outbound messages-->
            
            <xs:element name="time-to-live" type="xs:long" minOccurs="0" maxOccurs="1"/>
            
            <!-- reply to will be used for replying to JMS oubtound messages. This could
            be the same destination or differnet one according to the users choice-->
            
            <xs:element name="reply-to" type="xs:string" minOccurs="0" maxOccurs="1"/>
	
            <!-- WSDL operations that are supported by this service -->
            
            <xs:element name="operation" type="operationType" minOccurs="1" maxOccurs="unbounded"/>
        </xs:sequence>
       </xs:complexType>
     </xs:element>
   </xs:sequence>
 </xs:complexType>
</xs:element>


<xs:complexType name="connectionType">
	<xs:sequence minOccurs="1" maxOccurs="1">        
          <!-- Lookup Name   of the connection factory using which a connection will
              be established with the provider for this endpoint. This has to be created and
              configured by a JMS admin and stored as administered objects in an Object store.
              What should be provided here is the lookup name.
           -->
        <xs:element name="connection-factory-name" type="xs:string"/>
        
        <!-- User name for establishing a connection using the above conneciton factory
        This should be the same user name as configured by the JMS admin for this factory
        -->
        <xs:element name="connection-user-name" type="xs:string" minOccurs="0" maxOccurs="1"/>	                
        
        <!-- Password for establishing a connection using the above conneciton factory
        This should be the same user name as configured by the JMS admin for this factory
        -->
        <xs:element name="connection-password" type="xs:string" minOccurs="0" maxOccurs="1"/>	        
        
        </xs:sequence>
</xs:complexType>
        

<!-- Aggregate type for operations information -->
<xs:complexType name="operationType">
	<xs:sequence minOccurs="1" maxOccurs="1">
        
        
        <xs:element name = "name" type="qnameType" minOccurs="1" maxOccurs="1"/>
        
        
        <!-- WSDL Message Exchange pattern for this operation. Should be a valid 
        URI according to WSDL specification -->
			
        <xs:element name="mep" minOccurs="1" maxOccurs="1">	
		<xs:simpleType>
			<xs:restriction base="xs:anyURI">
				<xs:enumeration value="http://www.w3.org/ns/wsdl/in-only"/>
				<xs:enumeration value="http://www.w3.org/ns/wsdl/in-out"/>							
				<xs:enumeration value="http://www.w3.org/ns/wsdl/in-opt-out"/>
				<xs:enumeration value="http://www.w3.org/ns/wsdl/robust-in-only"/>	
  			</xs:restriction>
		</xs:simpleType>
	</xs:element>
        
        <!-- One of the six types of messages suppoerted by JMS. Ths binding expects
        any message received for this endpoint to be of this type -->
        
        <xs:element name="input-message-type" type="messageType" minOccurs="0" maxOccurs="1"/>	        
        
        <!-- One of the six types of messages suppoerted by JMS. Ths binding converts 
        all messages that are sent out to this type.-->
        
		<xs:element name="output-message-type" type="messageType" minOccurs="0" maxOccurs="1"/>
        
        </xs:sequence>
        </xs:complexType>
        <!-- Possible message types in JMS, Refer specification -->
        
        <xs:simpleType name="messageType">
          	<xs:restriction base="xs:string">
			<xs:enumeration value="TextMessage"/>
			<xs:enumeration value="StreamMessage"/>
			<xs:enumeration value="MapMessage"/>
			<xs:enumeration value="BytesMessage"/>
			<xs:enumeration value="ObjectMessage"/>
			<xs:enumeration value="Message"/>
                 </xs:restriction>
				
</xs:simpleType>

<xs:complexType name="qnameType">
 	 <xs:sequence minOccurs="1" maxOccurs="1">
          <!-- The namespace for this Qualified name-->     

     <xs:element name="namespace-uri" type="xs:anyURI"/>
          
      <!-- Local Part of the qualified name-->      

     <xs:element name="local-part" type="xs:NCName"/>
     </xs:sequence>
</xs:complexType> 
</xs:schema>
