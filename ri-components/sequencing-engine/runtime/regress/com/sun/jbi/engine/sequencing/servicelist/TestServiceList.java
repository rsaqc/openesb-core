/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceList.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import com.sun.jbi.engine.sequencing.MessageProcessor;
import com.sun.jbi.engine.sequencing.exception.ServicelistException;
import com.sun.jbi.engine.sequencing.framework.threads.Command;
import com.sun.jbi.engine.sequencing.servicelist.ServiceBean;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistReader;

import junit.framework.*;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchange;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServiceList extends TestCase
{
    /**
     * Creates a new TestServiceList object.
     *
     * @param testName
     */
    public TestServiceList(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestServiceList.class);

        return suite;
    }

    /**
     * Test of cancelExchange method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testCancelExchange()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of execute method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testExecute()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getCurrentService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testGetCurrentService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getDeploymentId method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testGetDeploymentId()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getListName method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testGetListName()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServiceListBean method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testGetServiceListBean()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getState method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testGetState()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setChannel method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testSetChannel()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setCurrentService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testSetCurrentService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setDeploymentId method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testSetDeploymentId()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setMessageExchange method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testSetMessageExchange()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setServiceListBean method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testSetServiceListBean()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of terminate method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceList.
     */
    public void testTerminate()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
