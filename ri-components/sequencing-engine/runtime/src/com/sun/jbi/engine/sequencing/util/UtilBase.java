/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UtilBase.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.util;

/**
 * Base class for error/status reporting.
 *
 * @author Sun Microsystems, Inc.
 */
public class UtilBase
{
    /**
     *    Exception.
     */
    private Exception mException;

    /**
     * Error.
     */
    private StringBuffer mError;

    /**
     *  Warning.
     */
    private StringBuffer mWarning;

    /**
     *  Validity.
     */
    private boolean mValid = true;

    /**
     * Creates a new UtilBase object.
     */
    public UtilBase()
    {
        mError = new StringBuffer();
        mWarning = new StringBuffer();
    }

    /**
     * Sets the error.
     *
     * @param err error string.
     */
    public void setError(String err)
    {
        mValid = false;

        if (err != null)
        {
            if (!err.trim().equals(""))
            {
                mError.append("\nError : " + "Reason : " + err);
            }
        }
    }

    /**
     * Gets the error.
     *
     * @return error.
     */
    public String getError()
    {
        return mError.toString();
    }

    /**
     * Sets the excpetion.
     *
     * @param ex exception.
     */
    public void setException(Exception ex)
    {
        mValid = false;
        mException = ex;
        mError.append(ex.getMessage());
    }

    /**
     * Gets the exception.
     *
     * @return exception.
     */
    public Exception getException()
    {
        if (!mError.toString().trim().equals(""))
        {
            mException = new Exception(mError.toString());
        }

        return mException;
    }

    /**
     * Validity.
     *
     * @return true if valid.
     */
    public boolean isValid()
    {
        return mValid;
    }

    /**
     * Returns the warning.
     *
     * @return warning.
     */
    public String getWarning()
    {
        return mWarning.toString();
    }

    /**
     *
     */
    public void clear()
    {
        mException = null;
        mValid = true;
        mError = new StringBuffer();
        mWarning = new StringBuffer();
    }

    /**
     * Sets the validity.
     *
     * @param valid true if valid.
     */
    protected void setValid(boolean valid)
    {
        mValid = valid;
    }

    /**
     * Sets the warning.
     *
     * @param warn warning string.
     */
    protected void setWarning(String warn)
    {
        if (warn != null)
        {
            if (!warn.trim().equals(""))
            {
                mWarning.append("\nWarning : " + "Reason : " + warn);
            }
        }
    }
}
