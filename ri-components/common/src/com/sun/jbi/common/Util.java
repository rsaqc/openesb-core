/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Util.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.common;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.common.management.ManagementMessageBuilder;
import com.sun.jbi.common.management.ManagementMessageBuilderImpl;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This is an utility class which provides an interface to access the string tran
 *
 * @author Sun Microsystems, Inc.
 */
public class Util
{
    /**
     * Table of handles to StringTranslators.
     */
    private static HashMap sStringTranslators;

    /**
     * StringTranslator for the common utility.
     */
    private static StringTranslator sTranslator;

    /**
     * Handle to the Logger instance.
     */
    private static Logger sLogger;

    static
    {        
        sStringTranslators = new HashMap();
        sLogger = Logger.getLogger("com.sun.jbi.common");
        sTranslator =  new StringTranslatorImpl("com.sun.jbi.common", null);
        sStringTranslators.put("com.sun.jbi.common", sTranslator);
    }

    /**
     * A helper method to efficiently log an internationalized message. The method uses
     * the StringTranslator to get the internationalized only if the configured log
     * level is greater than or equal to the prescribed log level. It is recommended
     * that clients use
     * <pre><code>
     *      Util.log( stringTranslator, logger, Level.FINE, key);
     *  </code></pre>
     * instead of
     * <pre> <code>
     *      logger.fine(stringTranslator.getString(key));
     * </code></pre>
     *
     * @param stringTranslator the stringTranslator implementation to be used
     * @param logger the logger instance to be used
     * @param level the prescribed log level
     * @param key the key to the localized string in the resource bundle
     */
    public static void log(
        StringTranslator stringTranslator, Logger logger, Level level, String key)
    {
        if (logger.isLoggable(level))
        {
            logger.log(level, stringTranslator.getString(key));
        }
    }

    /**
     * A helper method to efficiently log an internationalized message. The method uses
     * the StringTranslator to get the internationalized only if the configured log
     * level is greater than or equal to the prescribed log level. This is a helper
     * method which can be used when the inserts length is 1.
     *
     * @param stringTranslator the stringTranslator implementation to be used
     * @param logger the logger instance to be used
     * @param level the prescribed log level
     * @param key the key to the localized string in the resource bundle
     * @param insert the object value to be inserted.
     */
    public static void log(
        StringTranslator stringTranslator, Logger logger, Level level, String key,
        Object insert)
    {
        if (logger.isLoggable(level))
        {
            Object[] inserts = new Object[1];
            inserts[0] = insert;
            logger.log(level, stringTranslator.getString(key, inserts));
        }
    }

    /**
     * A helper method to efficiently log an internationalized message. The method uses
     * the StringTranslator to get the internationalized only if the configured log
     * level is greater than or equal to the prescribed log level. This is a helper
     * method which can be used when the inserts length is 2.
     *
     * @param stringTranslator the stringTranslator implementation to be used
     * @param logger the logger instance to be used
     * @param level the prescribed log level
     * @param key the key to the localized string in the resource bundle
     * @param insert1 the first object value to be inserted.
     * @param insert2 the second object value to be inserted.
     */
    public static void log(
        StringTranslator stringTranslator, Logger logger, Level level, String key,
        Object insert1, Object insert2)
    {
        if (logger.isLoggable(level))
        {
            Object[] inserts = new Object[2];
            inserts[0] = insert1;
            inserts[1] = insert2;
            logger.log(level, stringTranslator.getString(key, inserts));
        }
    }

    /**
     * A helper method to efficiently log an internationalized message. The method uses
     * the StringTranslator to get the internationalized only if the configured log
     * level is greater than or equal to the prescribed log level. It is recommended
     * that clients use
     * <pre><code>
     *      Util.log( stringTranslator, logger, Level.FINE, key, inserts);
     *  </code></pre>
     * instead of
     * <pre> <code>
     *      logger.fine(stringTranslator.getString(key, inserts));
     * </code></pre>
     *
     * @param stringTranslator the stringTranslator implementation to be used
     * @param logger the logger instance to be used
     * @param level the prescribed log level
     * @param key the key to the localized string in the resource bundle
     * @param inserts the array of message inserts.
     */
    public static void log(
        StringTranslator stringTranslator, Logger logger, Level level, String key,
        Object[] inserts)
    {
        if (logger.isLoggable(level))
        {
            logger.log(level, stringTranslator.getString(key, inserts));
        }
    }
    
    /**
     * Get the StringTranslator for a specified package name.
     *
     * @param packageName - the name of the package containing the resource bundle to be
     *        used by this StringTranslator.    
     *
     * @return The StringTranslator instance.
     */
    public static synchronized StringTranslator getStringTranslator(String packageName)
    {        
        sLogger.fine(
            sTranslator.getString("EC_STRING_TRANSLATOR_REQUESTED", packageName));

        StringTranslator translator =
            (StringTranslator) sStringTranslators.get(packageName);

        if (null == translator)
        {
            translator = new StringTranslatorImpl(packageName, null);
            sStringTranslators.put(packageName, translator);
            sLogger.finer(
                sTranslator.getString("EC_STRING_TRANSLATOR_CREATED", packageName));
        }

        return translator;
    }

    /**
     * Get the StringTranslator for a specified object.
     *
     * @param object - an object in the package that contains the resource bundle to be
     *        used for this StringTranslator.
     *
     * @return The StringTranslator instance.
     */
    public static StringTranslator getStringTranslatorFor(Object object)
    {
        return getStringTranslator(object.getClass().getPackage().getName());
    }

    /**
     * Logs the exception stack trace using the defined logger instance at the prescribed
     * level.
     *
     * @param exception exception instance thrown by the application.
     * @param logger logger instance to be used.
     * @param level prescribed level.
     */
    public static void logExceptionTrace(Exception exception, Logger logger, Level level)
    {
        if (logger.isLoggable(level))
        {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            exception.printStackTrace(printWriter);
            logger.log(level, stringWriter.toString());
            printWriter.close();
        }
    }
    
    /**
     * Logs the exception stack trace using the defined logger. It logs the
     * exception trace using a default level of SEVERE.
     *
     * @param exception exception instance thrown by the application.
     * @param logger logger instance to be used
     */
    public static void logExceptionTrace(Exception exception, Logger logger)
    {
        logExceptionTrace(exception, logger, Level.SEVERE);
    }
    
    /**
     * Creates a new management message builder instance.
     *
     * @return a management message builder implementation instance.
     */
    public static ManagementMessageBuilder createManagementMessageBuilder()
    {
        return new ManagementMessageBuilderImpl();
    }
    
}
