/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UtilBase.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.common;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class UtilBase
{
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private Exception mException;

    /**
     *
     */

    /**
     *    
     */
    private StringBuffer mError;

    /**
     *
     */

    /**
     *    
     */
    private StringBuffer mWarning;

    /**
     *
     */

    /**
     *    
     */
    private boolean mValid = true;

    /**
     * Creates a new UtilBase object.
     */
    public UtilBase()
    {
        mError = new StringBuffer();
        mWarning = new StringBuffer();
    }

    /**
     * DOCUMENT ME!
     *
     * @param err NOT YET DOCUMENTED
     */
    public void setError(String err)
    {
        mValid = false;
        if (err != null)
        {
            if (!err.trim().equals(""))
            {
                mError.append("\nError : " + "Reason : " + err);        
            }
        }      
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getError()
    {
        return mError.toString();
    }

    /**
     * DOCUMENT ME!
     *
     * @param ex NOT YET DOCUMENTED
     */
    public void setException(Exception ex)
    {
        mValid = false;
        mException = ex;
        mError.append(ex.getMessage());
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public Exception getException()
    {
        if (!mError.toString().trim().equals(""))
        {
            mException = new Exception(mError.toString());
        }

        return mException;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public boolean isValid()
    {
        return mValid;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getWarning()
    {
        return mWarning.toString();
    }

    /**
     *
     */
    public void clear()
    {
        mException = null;
        mValid = true;
        mError = new StringBuffer();
        mWarning = new StringBuffer();
    }

    /**
     * DOCUMENT ME!
     *
     * @param valid NOT YET DOCUMENTED
     */
    protected void setValid(boolean valid)
    {
        mValid = valid;
    }

    /**
     * DOCUMENT ME!
     *
     * @param warn NOT YET DOCUMENTED
     */
    protected void setWarning(String warn)
    {
        if (warn != null)
        {
            if (!warn.trim().equals(""))
            {
                mWarning.append("\nWarning : " + "Reason : " + warn);
            }
        }
    }
}
