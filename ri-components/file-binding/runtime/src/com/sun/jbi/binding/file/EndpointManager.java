/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.util.ConfigData;
import com.sun.jbi.binding.file.util.StringTranslator;
import com.sun.jbi.binding.file.util.UtilBase;

import java.io.File;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.jbi.JBIException;
import javax.jbi.component.ComponentContext;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * Manages the stopping and starting of endpoints.
 *
 * @author Sun Microsystems, Inc.
 */
public class EndpointManager
    extends UtilBase
    implements FileBindingResources
{
    /**
     * Sleep time for thread.
     */
    private static final long SLEEP_TIME = 50;

    /**
     * Component Context.
     */
    private ComponentContext mContext;

    /**
     * Deployment Registry.
     */
    private DeploymentRegistry mRegistry;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * Helper for i18n.
     */
    private StringTranslator mStringTranslator;

    /**
     * Creates a new EndpointManager object.
     */
    public EndpointManager()
    {
        mRegistry = DeploymentRegistry.getInstance();
        mLog = FileBindingContext.getInstance().getLogger();
        mStringTranslator = new StringTranslator();
        mContext = FileBindingContext.getInstance().getContext();
    }

    /**
     * Checks the endpoint attributes.
     *
     * @param eb bean
     */
    public void docheck(EndpointBean eb)
    {
        if ((eb.getValue(ConfigData.OUTPUTDIR).trim().equals("")))
        {
            mLog.severe(mStringTranslator.getString(FBC_OUTPUTFOLDER_NOTEXIST,
                    eb.getValue(ConfigData.OUTPUTDIR)));
            mLog.severe(mStringTranslator.getString(
                    FBC_CANNOT_ACTIVATE_ENDPOINT, eb.getUniqueName()));
            setError(mStringTranslator.getString(FBC_CANNOT_ACTIVATE_ENDPOINT,
                    eb.getUniqueName()));
            setError(mStringTranslator.getString(FBC_OUTPUTFOLDER_NOTEXIST,
                    eb.getValue(ConfigData.OUTPUTDIR)));

            return;
        }

        if (eb.getRole() == ConfigData.CONSUMER)
        {
            if ((eb.getValue(ConfigData.PROCESSEDDIR).trim().equals("")))
            {
                mLog.severe(mStringTranslator.getString(
                        FBC_PROCESSEDFOLDER_NOTEXIST,
                        eb.getValue(ConfigData.PROCESSEDDIR)));
                mLog.severe(mStringTranslator.getString(
                        FBC_CANNOT_ACTIVATE_ENDPOINT, eb.getUniqueName()));
                setError(mStringTranslator.getString(
                        FBC_PROCESSEDFOLDER_NOTEXIST,
                        eb.getValue(ConfigData.PROCESSEDDIR)));
                setError(mStringTranslator.getString(
                        FBC_CANNOT_ACTIVATE_ENDPOINT, eb.getUniqueName()));

                return;
            }

            if ((eb.getValue(ConfigData.INPUTDIR).trim().equals("")))
            {
                mLog.severe(mStringTranslator.getString(
                        FBC_INPUTFOLDER_NOTEXIST,
                        eb.getValue(ConfigData.INPUTDIR)));
                mLog.severe(mStringTranslator.getString(
                        FBC_CANNOT_ACTIVATE_ENDPOINT, eb.getUniqueName()));
                setError(mStringTranslator.getString(FBC_INPUTFOLDER_NOTEXIST,
                        eb.getValue(ConfigData.INPUTDIR)));
                setError(mStringTranslator.getString(
                        FBC_CANNOT_ACTIVATE_ENDPOINT, eb.getUniqueName()));

                return;
            }

            File inputdir = new File(eb.getValue(ConfigData.INPUTDIR));

            if (!inputdir.exists())
            {
                mLog.info(mStringTranslator.getString(
                        FBC_INPUTFOLDER_NOTEXIST, inputdir.getAbsolutePath()));

                if (inputdir.mkdirs())
                {
                    mLog.info(mStringTranslator.getString(FBC_CREATED_FOLDER,
                            inputdir.getAbsolutePath()));
                    setWarning(mStringTranslator.getString(FBC_CREATED_FOLDER,
                            inputdir.getAbsolutePath()));
                }
                else
                {
                    mLog.info(mStringTranslator.getString(
                            FBC_CANNOT_CREATE_FOLDER, inputdir.getAbsolutePath()));
                    setError(mStringTranslator.getString(
                            FBC_CANNOT_CREATE_FOLDER, inputdir.getAbsolutePath()));

                    return;
                }
            }

            if ((!inputdir.isDirectory()) || (!inputdir.canRead()))
            {
                mLog.severe(mStringTranslator.getString(
                        FBC_INPUTFOLDER_NOTEXIST, inputdir.getAbsolutePath()));
                mLog.severe(mStringTranslator.getString(
                        FBC_CANNOT_ACTIVATE_ENDPOINT, eb.getUniqueName()));
                setError(mStringTranslator.getString(FBC_INPUTFOLDER_NOTEXIST,
                        inputdir.getAbsolutePath()));
                setError(mStringTranslator.getString(
                        FBC_CANNOT_ACTIVATE_ENDPOINT, eb.getUniqueName()));

                return;
            }

            for (int i = 0; i < eb.getOperationsCount(); i++)
            {
                QName oper = eb.getOperationQName(i);
                File f =
                    new File(inputdir.getAbsolutePath() + File.separatorChar
                        + oper.getLocalPart());

                if (!f.exists())
                {
                    mLog.info(mStringTranslator.getString(
                            FBC_INPUTFOLDER_NOTEXIST, inputdir.getAbsolutePath()));

                    if (f.mkdirs())
                    {
                        mLog.info(mStringTranslator.getString(
                                FBC_CREATED_FOLDER, inputdir.getAbsolutePath()));
                        setWarning(mStringTranslator.getString(
                                FBC_CREATED_FOLDER, inputdir.getAbsolutePath()));
                    }
                    else
                    {
                        mLog.info(mStringTranslator.getString(
                                FBC_CANNOT_CREATE_FOLDER,
                                inputdir.getAbsolutePath()));
                        setWarning(mStringTranslator.getString(
                                FBC_CANNOT_CREATE_FOLDER,
                                inputdir.getAbsolutePath()));

                        return;
                    }
                }
            }

            File processedFolder =
                new File(eb.getValue(ConfigData.PROCESSEDDIR));

            if (!processedFolder.exists())
            {
                mLog.info(mStringTranslator.getString(
                        FBC_PROCESSEDFOLDER_NOTEXIST,
                        processedFolder.getAbsolutePath()));

                if (processedFolder.mkdirs())
                {
                    mLog.info(mStringTranslator.getString(FBC_CREATED_FOLDER,
                            processedFolder.getAbsolutePath()));
                    setWarning(mStringTranslator.getString(FBC_CREATED_FOLDER,
                            processedFolder.getAbsolutePath()));
                }
                else
                {
                    mLog.info(mStringTranslator.getString(
                            FBC_CANNOT_CREATE_FOLDER,
                            processedFolder.getAbsolutePath()));
                    setError(mStringTranslator.getString(
                            FBC_CANNOT_CREATE_FOLDER,
                            processedFolder.getAbsolutePath()));

                    return;
                }
            }

            if (!processedFolder.canWrite())
            {
                mLog.warning(mStringTranslator.getString(
                        FBC_NO_WRITE_PERMISSION,
                        processedFolder.getAbsolutePath()));
                setError(mStringTranslator.getString(FBC_NO_WRITE_PERMISSION,
                        processedFolder.getAbsolutePath()));
            }
        }
    }

    /**
     * Checks the endpoint attributes.
     *
     * @param eb bean
     */
    public void docheck2(EndpointBean eb)
    {
        File writeFolder = new File(eb.getValue(ConfigData.OUTPUTDIR));

        if (!writeFolder.exists())
        {
            mLog.info(mStringTranslator.getString(
                    FBC_OUTPUTFOLDER_NOTEXIST, writeFolder.getAbsolutePath()));

            if (writeFolder.mkdirs())
            {
                mLog.info(mStringTranslator.getString(FBC_CREATED_FOLDER,
                        writeFolder.getAbsolutePath()));
                setWarning(mStringTranslator.getString(FBC_CREATED_FOLDER,
                        writeFolder.getAbsolutePath()));
            }
            else
            {
                mLog.severe(mStringTranslator.getString(
                        FBC_CANNOT_CREATE_FOLDER, writeFolder.getAbsolutePath()));
                setError(mStringTranslator.getString(FBC_CANNOT_CREATE_FOLDER,
                        writeFolder.getAbsolutePath()));

                return;
            }
        }

        if (!writeFolder.canWrite())
        {
            mLog.warning(mStringTranslator.getString(FBC_NO_WRITE_PERMISSION,
                    writeFolder.getAbsolutePath()));
        }

        if (eb.getRole() == ConfigData.CONSUMER)
        {
            if ((eb.getValue(ConfigData.SERVICE_LOCALNAME).trim().equals(""))
                    && (
                        eb.getValue(ConfigData.ENDPOINTNAME).trim().equals("")
                    )
                    && (
                        eb.getValue(ConfigData.INTERFACE_LOCALNAME).trim()
                              .equals("")
                    ))
            {
                mLog.severe(mStringTranslator.getString(
                        FBC_INVALID_ENDPOINT_INFO, eb.getDeploymentId()));
                setError(mStringTranslator.getString(
                        FBC_INVALID_ENDPOINT_INFO, eb.getDeploymentId()));

                return;
            }
        }
        else if (eb.getRole() == ConfigData.PROVIDER)
        {
            if ((eb.getValue(ConfigData.SERVICE_LOCALNAME).trim().equals(""))
                    || (
                        eb.getValue(ConfigData.ENDPOINTNAME).trim().equals("")
                    )
                    || (
                        eb.getValue(ConfigData.INTERFACE_LOCALNAME).trim()
                              .equals("")
                    ))
            {
                mLog.severe(mStringTranslator.getString(
                        FBC_INVALID_PROVIDER_ENDPOINT_INFO, eb.getDeploymentId()));
                setError(mStringTranslator.getString(
                        FBC_INVALID_PROVIDER_ENDPOINT_INFO, eb.getDeploymentId()));

                return;
            }
        }
    }

    /**
     * Starts a deployment.
     *
     * @param suid service unit id.
     */
    public void startDeployment(String suid)
    {
        mLog.info(mStringTranslator.getString(FBC_START_DEPLOYMENT, suid));
        super.clear();

        List l = mRegistry.getEndpoints(suid);
        Iterator iter = l.iterator();

        while (iter.hasNext())
        {
            EndpointBean eb = (EndpointBean) iter.next();

            if (eb == null)
            {
                mLog.severe(mStringTranslator.getString(
                        FBC_START_DEPLOYMENT_FAILED, suid));
                mLog.severe(mStringTranslator.getString(
                        FBC_START_DEPLOYMENT_FAILED_BEANNULL, suid));
                setWarning(mStringTranslator.getString(
                        FBC_START_DEPLOYMENT_FAILED_BEANNULL, suid));

                continue;
            }

            docheck(eb);
            docheck2(eb);

            if (!isValid())
            {
                setWarning(mStringTranslator.getString(
                        FBC_START_ENDPOINT_FAILED, eb.getUniqueName(), suid));

                return;
            }

            if (eb.getRole() == ConfigData.PROVIDER)
            {
                if (!startOutboundEndpoint(eb))
                {
                    setWarning(mStringTranslator.getString(
                            FBC_START_ENDPOINT_FAILED, eb.getUniqueName(), suid));

                    continue;
                }
            }
            else if (eb.getRole() == ConfigData.CONSUMER)
            {
                if (!startInboundEndpoint(eb))
                {
                    setWarning(mStringTranslator.getString(
                            FBC_START_ENDPOINT_FAILED, eb.getUniqueName(), suid));
                    stopInboundEndpoint(eb);

                    continue;
                }
            }
        }

        mLog.info(mStringTranslator.getString(FBC_START_DEPLOYMENT_SUCCESS, suid));
    }

    /**
     * Starts a consumer endpoint.
     *
     * @param eb endpoint bean
     *
     * @return true if started successfuly.
     */
    public boolean startInboundEndpoint(EndpointBean eb)
    {
        try
        {
            FileThreads ft = new FileThreads(eb);
            Thread t = new Thread(ft);
            eb.setFileThread(ft);
            eb.setReceiverThread(t);
            t.start();
        }
        catch (Exception e)
        {
            mLog.severe(mStringTranslator.getString(FBC_START_ENDPOINT_FAILED,
                    eb.getUniqueName()));
            mLog.severe(e.getMessage());
            setWarning(e.getMessage());

            return false;
        }

        return true;
    }

    /**
     * Starts a provider endpoint.
     *
     * @param eb bean.
     *
     * @return true if success.
     */
    public boolean startOutboundEndpoint(EndpointBean eb)
    {
        ServiceEndpoint ref = null;

        try
        {
            ref = mContext.activateEndpoint(new QName(eb.getValue(
                            ConfigData.SERVICE_NAMESPACE),
                        eb.getValue(ConfigData.SERVICE_LOCALNAME)),
                    eb.getValue(ConfigData.ENDPOINTNAME));
            mLog.info(mStringTranslator.getString(
                    FBC_ACTIVATE_OUTBOUND_SUCCESS, eb.getUniqueName()));
        }
        catch (JBIException me)
        {
            mLog.severe(mStringTranslator.getString(
                    FBC_ACTIVATE_OUTBOUND_FAILED, eb.getUniqueName()));
            mLog.severe(me.getMessage());
            setWarning(me.getMessage());

            return false;
        }

        eb.setServiceEndpoint(ref);

        return true;
    }

    /**
     * Stops all running endpoints. This inclides deactiavting all the
     * endpoints with the NMS and also stopping all running threads.
     */
    public void stopAllEndpoints()
    {
        Iterator iter = mRegistry.listAllEndpoints();

        while (iter.hasNext())
        {
            String ebname = (String) (iter.next());

            EndpointBean eb = mRegistry.findEndpoint(ebname);

            try
            {
                if (eb.getRole() == ConfigData.CONSUMER)
                {
                    stopInboundEndpoint(eb);
                }
                else if (eb.getRole() == ConfigData.CONSUMER)
                {
                    stopOutboundEndpoint(eb);
                }
            }
            catch (Exception e)
            {
                setWarning(e.getMessage());

                continue;
            }
        }
    }

    /**
     * Stops a deployment.
     *
     * @param suid Service unit ID.
     */
    public void stopDeployment(String suid)
    {
        mLog.info(mStringTranslator.getString(FBC_STOP_DEPLOYMENT, suid));
        super.clear();

        List l = mRegistry.getEndpoints(suid);
        Iterator iter = l.iterator();

        while (iter.hasNext())
        {
            EndpointBean eb = (EndpointBean) iter.next();

            if (eb == null)
            {
                mLog.severe(mStringTranslator.getString(
                        FBC_STOP_DEPLOYMENT_FAILED, suid));
                setWarning(mStringTranslator.getString(
                        FBC_STOP_DEPLOYMENT_FAILED, suid));

                continue;
            }

            if (eb.getRole() == ConfigData.PROVIDER)
            {
                if (!stopOutboundEndpoint(eb))
                {
                    setWarning(mStringTranslator.getString(
                            FBC_STOP_ENDPOINT_FAILED, eb.getUniqueName(), suid));

                    continue;
                }
            }
            else if (eb.getRole() == ConfigData.CONSUMER)
            {
                if (!stopInboundEndpoint(eb))
                {
                    setWarning(mStringTranslator.getString(
                            FBC_STOP_ENDPOINT_FAILED, eb.getUniqueName(), suid));

                    continue;
                }
            }
        }

        mLog.info(mStringTranslator.getString(FBC_STOP_DEPLOYMENT_SUCCESS, suid));
    }

    /**
     * Stops a consumer endpoint.
     *
     * @param eb endpoint bean.
     *
     * @return true if success.
     */
    public boolean stopInboundEndpoint(EndpointBean eb)
    {
        try
        {
            FileThreads ft = (FileThreads) (eb.getFileThread());

            if (ft.isRunning())
            {
                ft.stopAll();
            }

            Thread t = (Thread) eb.getReceiverThread();

            try
            {
                t.interrupt();
                t.join();
            }
            catch (Exception ie)
            {
                ;
            }
        }
        catch (Exception e)
        {
            mLog.severe(mStringTranslator.getString(FBC_STOP_ENDPOINT_FAILED,
                    eb.getUniqueName()));
            mLog.severe(e.getMessage());
            setWarning(e.getMessage());

            return false;
        }

        return true;
    }

    /**
     * Stops a provider endpoint.
     *
     * @param eb bean object.
     *
     * @return true if success.
     */
    public boolean stopOutboundEndpoint(EndpointBean eb)
    {
        ServiceEndpoint ref = eb.getServiceEndpoint();

        try
        {
            mContext.deactivateEndpoint(ref);
        }
        catch (Exception me)
        {
            mLog.severe(mStringTranslator.getString(
                    FBC_DEACTIVATE_OUTBOUND_FAILED, eb.getUniqueName()));
            mLog.severe(me.getMessage());
            setWarning(me.getMessage());

            return false;
        }

        return true;
    }
}
