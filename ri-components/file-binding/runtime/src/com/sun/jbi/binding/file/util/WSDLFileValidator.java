/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDLFileValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.FileBindingResources;
import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.wsdl2.Binding;
import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Endpoint;
import com.sun.jbi.wsdl2.WsdlFactory;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.logging.Logger;


/**
 * This class is uses to validate the configuration file supplied during
 * deployment conforms to the schema.
 *
 * @author Sun Microsystems, Inc.
 */
public final class WSDLFileValidator
    extends UtilBase
    implements FileBindingResources
{
    /**
     * Description object
     */
    private static Description sDefinition;

    /**
     * Inbound / Outbound
     */
    private String mCurValue = "";

    /**
     * Name of the file to parse.
     */
    private String mFileName;

    /**
     * Translator for i18n messages.
     */
    private StringTranslator mTranslator;

    /**
     * Creates a new WSDLFileValidator object.
     *
     * @param wsdlfile schema file name
     */
    public WSDLFileValidator(String wsdlfile)
    {
        mFileName = wsdlfile;
        mTranslator = new StringTranslator();
    }

    /**
     * Returns the document object obtained as a result of parsing.
     *
     * @return document object
     */
    public Description getDocument()
    {
        return sDefinition;
    }

    /**
     * This method has to be invoked to check the validity of the input
     * document.
     */
    public void validate()
    {
        parse();

        if (isValid())
        {
            validateEndpoints();
        }
    }

    /**
     * Checks if a binging is file binding
     *
     * @param df Document fragment
     *
     * @return true if file binding
     */
    private boolean isFileBinding(DocumentFragment df)
    {
        NamedNodeMap n = df.getFirstChild().getAttributes();

        for (int g = 0; g < n.getLength(); g++)
        {
            if (n.item(g).getLocalName().equals(ConfigData.WSDL_BINDING_TYPE))
            {
                if (n.item(g).getNodeValue().equals(ConfigData.FILENAMESPACE))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks the Endpoint.
     *
     * @param ep endpoint
     */
    private void checkEndpoint(Endpoint ep)
    {
        DocumentFragment docfrag = ep.toXmlDocumentFragment();

        Node epnode = docfrag.getFirstChild();
        NamedNodeMap epattributes = epnode.getAttributes();
        checkRequired(epattributes, ConfigData.ENDPOINT_TYPE);

        if (mCurValue.trim().equalsIgnoreCase(ConfigData.CONSUMER_STRING))
        {
            checkRequired(epattributes, ConfigData.WSDL_INPUT_LOCATION);
            checkRequired(epattributes, ConfigData.WSDL_PROCESSED_LOCATION);
        }

        checkRequired(epattributes, ConfigData.WSDL_OUTPUT_LOCATION);

        if (!isValid())
        {
            setError("\n\tEndpoint : " + ep.getName() + getError());
        }
    }

    /**
     * Checks the required attributes.
     *
     * @param map Node map
     * @param attr attributes that has to be checked.
     */
    private void checkRequired(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItemNS(ConfigData.FILENAMESPACE, attr);

        if ((attrnode == null))
        {
            setValid(false);
            setError(getError() + "\n\t" + "Required attribute \"" + attr
                + "\" not found");

            return;
        }

        try
        {
            mCurValue = attrnode.getNodeValue().trim();

            if (mCurValue.equals(""))
            {
                setValid(false);
                setError(getError() + "\n\t" + "Required attribute \"" + attr
                    + "\" has to have a value, cannot be null");
            }
        }
        catch (Exception e)
        {
            setValid(false);
            setError(getError() + "\n\t" + "Cannot get attribute value of "
                + attr);
            setException(e);
        }
    }

    /**
     * Parses the input XML file.
     */
    private void parse()
    {
        try
        {
	    com.sun.jbi.component.ComponentContext ctx = 
		 (com.sun.jbi.component.ComponentContext)
			FileBindingContext.getInstance().getContext();
	    WsdlFactory wsdlFactory = ctx.getWsdlFactory ();
            com.sun.jbi.wsdl2.WsdlReader wsdlRdr = wsdlFactory.newWsdlReader();
            sDefinition = wsdlRdr.readDescription(mFileName);
        }
        catch (Exception e)
        {
            setError("WSDL file " + mFileName + " parse error, reason "
                + e.getMessage());
            setException(e);
            setValid(false);
            e.printStackTrace();
        }
    }

    /**
     * Validates the endpoints.
     */
    private void validateEndpoints()
    {
        int services = sDefinition.getServicesLength();
        boolean found = false;

        for (int i = 0; i < services; i++)
        {
            try
            {
                com.sun.jbi.wsdl2.Service ser = sDefinition.getService(i);
                int endpoints = ser.getEndpointsLength();

                for (int k = 0; k < endpoints; k++)
                {
                    Endpoint ep = ser.getEndpoint(k);
                    Binding b = ep.getBinding();

                    if (!isFileBinding(b.toXmlDocumentFragment()))
                    {
                        continue;
                    }

                    found = true;
                    checkEndpoint(ep);

                    if (!isValid())
                    {
                        setError("\n\tBinding " + " : " + b.getName()
                            + getError());

                        return;
                    }
                }
            }
            catch (Exception e)
            {
                setError(mTranslator.getString(FBC_WSDL_ENDPOINT_ERROR,
                        e.getMessage()));
                setException(e);
                setValid(false);
                e.printStackTrace();

                return;
            }
        }

        if (!found)
        {
            setError(getError() + mTranslator.getString(FBC_WSDL_NO_ENDPOINTS));
            setValid(false);
        }
    }
}
