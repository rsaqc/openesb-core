/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileCommand.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.framework.Command;
import com.sun.jbi.binding.file.util.ConfigData;
import com.sun.jbi.binding.file.util.FileBindingUtil;
import com.sun.jbi.binding.file.util.StringTranslator;

import org.w3c.dom.Text;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;

import com.sun.jbi.wsdl11wrapper.Wsdl11WrapperHelper;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import java.util.logging.Level;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.dom.DOMSource;


/**
 * The FileCommand class reads a file and sends assembles the Normalized
 * MEssage and sends to to the NMS.
 *
 * @author Sun Microsystems, Inc.
 */
public class FileCommand
    implements Command, FileBindingResources
{
    /**
     * Input stream for reading the file.
     */
    private FileInputStream mFis;
    
    /**
     * Component context.
     */
    private ComponentContext mContext;

    /**
     * Delivery channel
     */
    private DeliveryChannel mChannel;

    /**
     * Endpoint bean that stores the endpoint attributes.
     */
    private EndpointBean mBean;

    /**
     * Exception
     */
    private Exception mException;

    /**
     * Destination folder to put the processed file.
     */
    private File mDestFolder = null;

    /**
     * The source file to be sent to NMS
     */
    private File mFile = null;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * Exchange factory
     */
    private MessageExchangeFactory mFactory;

    /**
     * Current operation.
     */
    private QName mCurrentOperation;

    /**
     * Endpoint reference.
     */
    private ServiceEndpoint mServiceEndpoint;

    /**
     * Tracking id.
     */
    private String mTrkId;

    /**
     * Helper for i18n.
     */
    private StringTranslator mTranslator;
    
    /**
     * True if a file is locked.
     */
    private boolean mFileLocked = false;
    
    /**
     *
     * Flag to enable input validation
     */
    private boolean mInputValidation = true;
  

    /**
     * Creates a new FileCommand object.
     *
     * @param file file to be read
     * @param eb folder to move the processed files.
     * @param oper operation name.
     * @param trkid trk id.
     */
    FileCommand(
        File file,
        EndpointBean eb,
        QName oper,
        String trkid)
    {
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        mFile = file;
        mContext = FileBindingContext.getInstance().getContext();
        mBean = eb;
        mCurrentOperation = oper;
        mFileLocked = false;
        mTrkId = trkid;
    }

    /**
     * Returns the name of the folder.
     *
     * @return folder name.
     */
    public String getName()
    {
        if (mFile != null)
        {
            return mFile.getAbsolutePath();
        }
        else
        {
            return "";
        }
    }

    /**
     * This determines if the input XML document will be validated
     * for XML well-formedness.
     */
    public void setValidating(boolean valid)
    {
        mInputValidation = valid;
    }
    
    /**
     * The run method which sends the normalized message to the respective
     * service channel. This class is an implementation of the COmmand
     * interface This method is called when this object is excetuted in a
     * separate thread.
     */
    public void execute()
    {
        InOut inout = null;
        InOnly inonly = null;
        RobustInOnly robustinonly = null;
        NormalizedMessage inmsg = null;

        try
        {
            mChannel = mContext.getDeliveryChannel();
            mFactory = mChannel.createExchangeFactory();

            Source src = null;

                src = getSource();
                
                dump(src);

            if (src == null)
            {
                mLog.severe(mTranslator.getString(FBC_INVALID_INPUT_FILE));
                throw mException;
            }

            if (mCurrentOperation == null)
            {
                mLog.severe(mTranslator.getString(FBC_INVALID_OPERATION_FILE));
                throw new Exception(mTranslator.getString(
                        FBC_INVALID_OPERATION_FILE));
            }

            String mep = mBean.getMEP(mCurrentOperation.getLocalPart());

            if (mep == null)
            {
                mLog.severe(mTranslator.getString(FBC_INVALID_OPERATION_FILE));
                throw new Exception(mTranslator.getString(
                        FBC_INVALID_OPERATION_FILE));
            }

            if (mep.trim().equalsIgnoreCase(MessageProcessor.IN_OUT.trim()))
            {
                inout = mFactory.createInOutExchange();
                inmsg = inout.createMessage();
                inmsg.setContent(src);
                sendInOut(inout, inmsg);
            }
            else if (mep.trim().equalsIgnoreCase(MessageProcessor.IN_ONLY.trim()))
            {
                inonly = mFactory.createInOnlyExchange();
                inmsg = inonly.createMessage();
                inmsg.setContent(src);
                sendInOnly(inonly, inmsg);
            }
            else if (mep.trim().equalsIgnoreCase(MessageProcessor.ROBUST_IN_ONLY
                        .trim()))
            {
                robustinonly = mFactory.createRobustInOnlyExchange();
                inmsg = robustinonly.createMessage();
                inmsg.setContent(src);
                sendRobustInOnly(robustinonly, inmsg);
            }
            else if (mep.trim().equalsIgnoreCase(MessageProcessor.IN_OPTIONAL_OUT
                        .trim()))
            {
                mLog.info(mTranslator.getString(FBC_INOPTIONALOUT_NOT_SUPPORTED));
                throw new Exception(mTranslator.getString(
                        FBC_INOPTIONALOUT_NOT_SUPPORTED));
            }
            else
            {
                mLog.severe(mTranslator.getString(FBC_INVALID_PATTERN,
                        mBean.getValue(ConfigData.MEP), mBean.getUniqueName()));
                throw new Exception(mTranslator.getString(FBC_INVALID_PATTERN,
                        mBean.getValue(ConfigData.MEP), mBean.getUniqueName()));
            }
        }
        catch (Exception e)
        {
            FileBindingUtil.removeEntry(mTrkId);
            if (!mFileLocked)
            {
                mLog.severe(mTranslator.getString(FBC_SEND_FAILED));
                e.printStackTrace();
                writeErrorMessage(e);
                String procfolder =
                    (String) mBean.getValue(ConfigData.PROCESSEDDIR);

                if (procfolder != null)
                {
                    mDestFolder = new File(procfolder);
                }
                if (FileBindingUtil.moveFile(mTrkId, mDestFolder, mFile) == null)
                {
                    mLog.severe(mTranslator.getString(FBC_MOVE_FAILED,
                            mFile.getName(), mDestFolder.getAbsolutePath()));
                }
            }
        }
    }

    /**
     * Closes a file being read.
     */
    public void terminate()
    {
        try
        {
            mFis.close();
        }
        catch (Exception e)
        {
            ;
        }
    }

    /**
     * Writes a error message into file.
     *
     * @param ex exception message.
     */
    public void writeErrorMessage(Exception ex)
    {
        StringBuffer sb = new StringBuffer();
        StackTraceElement [] stckTrElem = null;
        FileOutputStream out = null;

        try
        {
            out = new FileOutputStream(mBean.getValue(ConfigData.OUTPUTDIR)
                    + File.separatorChar + FileBindingUtil.getBaseName(mFile)
                    + ConfigData.SEPARATOR + mTrkId + "."
                    + ConfigData.ERROR_EXTENSION);

            if (ex == null)
            {
                sb.append(mTranslator.getString(FBC_UNKNOWN_ERROR));
            }
            else
            {
                sb.append(ex.getMessage());
                stckTrElem = ex.getStackTrace();

                if (stckTrElem != null)
                {
                    for (int i = 0; i < stckTrElem.length; i++)
                    {
                        String stckTrace = stckTrElem[i].toString();
                        sb.append(stckTrace);
                        sb.append("\n");
                    }
                }
            }

            out.write(sb.toString().getBytes());
            out.close();
        }
        catch (Exception e)
        {
            mLog.info(mTranslator.getString(FBC_FILE_WRITE_FAILED,
                    mFile.getAbsolutePath()));
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (Exception e1)
            {
                mLog.severe(mTranslator.getString(FBC_CLOSE_FAILED,
                        e1.getMessage()));
            }
        }
    }

    /**
     * Sets the endpoint reference.
     *
     * @throws Exception EXCEPTION.
     */
    private void setServiceEndpoint()
        throws Exception
    {
        /**
         * Always try to get an endpoint only before sending this gives the
         * maximum probability of finding an endpoint, also we give an engine
         * enoughs time to activate the  endpoint
         */
        try
        {
            QName qname =
                new QName(mBean.getValue(ConfigData.SERVICE_NAMESPACE),
                    mBean.getValue(ConfigData.SERVICE_LOCALNAME));
            QName iqname =
                new QName(mBean.getValue(ConfigData.INTERFACE_NAMESPACE),
                    mBean.getValue(ConfigData.INTERFACE_LOCALNAME));

            ServiceEndpoint [] ref = mContext.getEndpointsForService(qname);

            if ((ref == null) || (ref.length == 0))
            {
                ref = mContext.getEndpoints(iqname);
            }

            if ((ref == null) || (ref.length == 0))
            {
                throw new Exception("Cannot locate endpoint for " + qname
                    + " or " + iqname);
            }
            else
            {
                mServiceEndpoint = ref[0];

                for (int k = 0; k < ref.length; k++)
                {
                    if (ref[k].getEndpointName().trim().equals(mBean.getValue(
                                    ConfigData.ENDPOINTNAME)))
                    {
                        mServiceEndpoint = ref[k];
                    }
                }
            }

            mBean.setServiceEndpoint(mServiceEndpoint);
        }
        catch (Exception me)
        {
            me.printStackTrace();
            throw me;
        }
    }

    /**
     * Creates a source from the file contents.
     *
     * @return source
     */
    private Source getSource()
    {
        org.w3c.dom.Document doc = null;
        org.w3c.dom.Document doc1 = null;
        Text txt = null;
        FileLock fileLock = null;
        try
        {
            FileOutputStream fos = null;
            try
            {
                /* try to get  a lock on this file this way
                 * if u get one, its good
                 */
                fos = new FileOutputStream(mFile, true);
                if (fos != null)
                {
                    fos.close();
                }
            }
            catch (Exception e)
            {               
                mLog.info(mTranslator.getString(FBC_NO_LOCK,
                    mFile.getAbsolutePath()));
                mFileLocked = true;
                return null;
            }
            mFis= new FileInputStream(mFile);        
           if (mBean.getDeploymentType().equals("WSDL11"))
           {               
                DocumentBuilderFactory docbuilderfactory = 
                        DocumentBuilderFactory.newInstance();                
                docbuilderfactory.setValidating(false);
                docbuilderfactory.setNamespaceAware(true);                            
                DocumentBuilder docbuilder = docbuilderfactory.newDocumentBuilder();                
                Document d = docbuilder.parse(mFis);   
                Wsdl11WrapperHelper helper = new Wsdl11WrapperHelper(mBean.getWsdlDefinition());   
                QName qname =
                    new QName(mBean.getValue(ConfigData.SERVICE_NAMESPACE),
                        mBean.getValue(ConfigData.SERVICE_LOCALNAME));
                String epname = mBean.getValue(ConfigData.ENDPOINTNAME);            
                Document wd = helper.wrapMessage(d, qname, epname,          
                        mCurrentOperation.getLocalPart(), true);

                if (mFis != null)
                {
                       mFis.close();
                }   
                if (wd == null)
                {
                    return null;                
                }
                else
                {
                    return (new DOMSource(wd));
                }
           }               
           else
           {
                if (mInputValidation)           
                {  
                    SAXParserFactory saxParserFactory =
                        SAXParserFactory.newInstance();
                    saxParserFactory.setValidating(false);
                    saxParserFactory.setNamespaceAware(true);            
                    SAXParser saxParser = saxParserFactory.newSAXParser();
                    saxParser.parse(mFis, new DefaultHandler());   
                    
                    if (mFis != null)
                    {
                        mFis.close();
                    }                   
                }  
                return (new StreamSource(mFile));
           }

        }
        catch (SAXParseException spe)
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_XML,
                    mFile.getAbsolutePath()));
            mLog.severe(spe.getMessage());
            mException = spe;
            spe.printStackTrace();
        }
        catch (ParserConfigurationException pce)
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_XML,
                    mFile.getAbsolutePath()));
            mLog.severe(pce.getMessage());
            mException = pce;
            pce.printStackTrace();
        }
        catch (SAXException sxe)
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_XML,
                    mFile.getAbsolutePath()));
            mLog.severe(sxe.getMessage());
            mException = sxe;
            sxe.printStackTrace();
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_XML,
                    mFile.getAbsolutePath()));
            mLog.severe(e.getMessage());
            mException = e;
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (mFis != null)
                {
                    mFis.close();
                }
            }
            catch (Exception e)
            {
                ;
            }
        }

        return null;
    }

    
    
    /**
     * Creates a source from the file contents.
     *
     * @return source
     */
    private Source getWsdl11Source()
    {
        org.w3c.dom.Document doc = null;
        org.w3c.dom.Document doc1 = null;
        Text txt = null;
        FileLock fileLock = null;
        try
        {
            FileOutputStream fos = null;
            try
            {
                /* try to get  a lock on this file this way
                 * if u get one, its good
                 */
                fos = new FileOutputStream(mFile, true);
                if (fos != null)
                {
                    fos.close();
                }
            }
            catch (Exception e)
            {               
                mLog.info(mTranslator.getString(FBC_NO_LOCK,
                    mFile.getAbsolutePath()));
                mFileLocked = true;
                return null;
            }
            mFis= new FileInputStream(mFile);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_XML,
                    mFile.getAbsolutePath()));
            mLog.severe(e.getMessage());
            mException = e;
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (mFis != null)
                {
                    mFis.close();
                }
            }
            catch (Exception e)
            {
                ;
            }
        }

        return null;
    }
    
    /**
     * Sends an in only message exchange.
     *
     * @param inonly inonly message exchange
     * @param inmsg message to be set in exchange.
     *
     * @throws Exception any exception
     */
    private void sendInOnly(
        InOnly inonly,
        NormalizedMessage inmsg)
        throws Exception
    {
        /**
         * Whatever is implemented here is a best effort delivery  We dont try
         * to store messages and redelierv is an endpoint is temporarily
         * unavailable
         */
        try
        {
            setServiceEndpoint();
            inonly.setService(mServiceEndpoint.getServiceName());            
            QName iqname =
                new QName(mBean.getValue(ConfigData.INTERFACE_NAMESPACE),
                    mBean.getValue(ConfigData.INTERFACE_LOCALNAME));
            inonly.setInterfaceName(iqname);
            inonly.setEndpoint(mServiceEndpoint);
            inonly.setOperation(mCurrentOperation);

            inonly.setProperty(ConfigData.FILENAME_PROPERTY,
                FileBindingUtil.getBaseName(mFile));
            inonly.setProperty(ConfigData.TRACKINGID_PROPERTY, mTrkId);
            inonly.setProperty(ConfigData.EXTENSION_PROPERTY,
                FileBindingUtil.getExtension(mFile));
            inonly.setInMessage(inmsg);
            mChannel.send(inonly);
            mLog.info(mTranslator.getString(FBC_SENDING_MESSAGE,
                    inonly.getExchangeId()));
        }
        catch (Exception me)
        {
            mLog.severe(mTranslator.getString(FBC_TRY_LATER,
                    mFile.getAbsolutePath()));
            mLog.severe(me.getMessage());
            mException = me;
            throw mException;
        }
    }

    /**
     * Sends an in-out message exchange.
     *
     * @param inout message exchange.
     * @param inmsg message to be set in exchange.
     *
     * @throws Exception any exception.
     */
    private void sendInOut(
        InOut inout,
        NormalizedMessage inmsg)
        throws Exception
    {
        /**
         * Whatever is implemented here is a best effort delivery  We dont try
         * to store messages and redelierv is an endpoint is temporarily
         * unavailable
         */
        try
        {
            setServiceEndpoint();
            inout.setService(mServiceEndpoint.getServiceName());                     
            QName iqname =
                new QName(mBean.getValue(ConfigData.INTERFACE_NAMESPACE),
                    mBean.getValue(ConfigData.INTERFACE_LOCALNAME));
            inout.setInterfaceName(iqname);
            inout.setOperation(mCurrentOperation);
            inout.setEndpoint(mServiceEndpoint);
            inout.setProperty(ConfigData.FILENAME_PROPERTY,
                FileBindingUtil.getBaseName(mFile));
            inout.setProperty(ConfigData.TRACKINGID_PROPERTY, mTrkId);
            inout.setProperty(ConfigData.EXTENSION_PROPERTY,
                FileBindingUtil.getExtension(mFile));
            inout.setInMessage(inmsg);
            mChannel.send(inout);
            mLog.info(mTranslator.getString(FBC_SENDING_MESSAGE,
                    inout.getExchangeId()));
        }
        catch (Exception me)
        {
            mLog.severe(mTranslator.getString(FBC_TRY_LATER,
                    mFile.getAbsolutePath()));
            mLog.severe(me.getMessage());
            mException = me;
            throw mException;
        }
    }

    /**
     * Sends a robust in only message exchange.
     *
     * @param robustinonly inonly message exchange
     * @param inmsg message to be set in exchange.
     *
     * @throws Exception any exception
     */
    private void sendRobustInOnly(
        RobustInOnly robustinonly,
        NormalizedMessage inmsg)
        throws Exception
    {
        /**
         * Whatever is implemented here is a best effort delivery  We dont try
         * to store messages and redelierv is an endpoint is temporarily
         * unavailable
         */
        try
        {
            setServiceEndpoint();
            robustinonly.setService(mServiceEndpoint.getServiceName());                     
            QName iqname =
                new QName(mBean.getValue(ConfigData.INTERFACE_NAMESPACE),
                    mBean.getValue(ConfigData.INTERFACE_LOCALNAME));
            robustinonly.setInterfaceName(iqname);
            robustinonly.setOperation(mCurrentOperation);
            robustinonly.setEndpoint(mServiceEndpoint);
            robustinonly.setProperty(ConfigData.FILENAME_PROPERTY,
                FileBindingUtil.getBaseName(mFile));
            robustinonly.setProperty(ConfigData.TRACKINGID_PROPERTY, mTrkId);
            robustinonly.setProperty(ConfigData.EXTENSION_PROPERTY,
                FileBindingUtil.getExtension(mFile));
            robustinonly.setInMessage(inmsg);
            mChannel.send(robustinonly);
            mLog.info(mTranslator.getString(FBC_SENDING_MESSAGE,
                    robustinonly.getExchangeId()));
        }
        catch (Exception me)
        {
            mLog.severe(mTranslator.getString(FBC_TRY_LATER,
                    mFile.getAbsolutePath()));
            mLog.severe(me.getMessage());
            mException = me;
            throw me;
        }
    }    
    
    private void dump(Source src)
    {
        try
        {
            if (mLog.getLevel() == Level.FINEST)
            {
                TransformerFactory tFactory = TransformerFactory.newInstance();
                Transformer trans = tFactory.newTransformer();
                StreamResult result = new StreamResult(System.out);
                trans.transform(src, result);  
            }               
            
        }
        catch (Throwable t)
        {
            ;
        }
    }
}
