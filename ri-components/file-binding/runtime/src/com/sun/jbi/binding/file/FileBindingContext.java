/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileBindingContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;
import java.util.logging.Logger;

/**
 * Context object to store sequencing engine specific informaiton.
 *
 * @author Sun Microsystems, Inc.
 */
public final class FileBindingContext
{
    /**
     * This object
     */
    private static FileBindingContext sFileContext;

    /**
     * ENgine context
     */
    private ComponentContext mContext;
    
    /**
     * Logger object.
     */
    private Logger mLogger;
    
    /**
     * DD schema.
     */
    private String mDDSchema;

    /**
     * Creates a new FileBindingContext object.
     */
    private FileBindingContext()
    {
    }

    /**
     * Gets the engine channel.
     *
     * @return engine channel
     */
    public DeliveryChannel getChannel()
    {
        DeliveryChannel chnl = null;

        if (mContext != null)
        {
            try
            {
                chnl = mContext.getDeliveryChannel();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return chnl;
    }

    /**
     * Sets the engine context.
     *
     * @param ctx engine context.
     */
    public void setContext(ComponentContext ctx)
    {
        mContext = ctx;
    }

    /**
     * Returns the context.
     *
     * @return engine context
     */
    public ComponentContext getContext()
    {
        return mContext;
    }
    

    /**
     * Sets the logger.
     *
     * @param logger Logger.
     */
    public void setLogger(Logger logger)
    {
        mLogger = logger;
    }

    /**
     * Returns the logger.
     *
     * @return engine context
     */
    public Logger  getLogger()
    {
        if (mLogger == null)
        {
            mLogger = Logger.getLogger("com.sun.jbi.binding.file");
        }
        return mLogger;
    }    

    /**
     * Used to grab a reference of this object.
     *
     * @return an initialized FileBindingContext reference
     */
    public static synchronized FileBindingContext getInstance()
    {
        if (sFileContext == null)
        {
            sFileContext = new FileBindingContext();              
        }

        return sFileContext;
    }
    
    /**
     * Gets the deployment descriptor schema file path.
     *
     * @return Full path of the DD schema file.
     */
    public String getDDSchema()
    {
        return mDDSchema;
    }
    
    /**
     * Sets the deployment descriptor schema file path.
     *
     * @ return full path of the DD schema.
     */
    public void setDDSchema(String sch)
    {
        mDDSchema = sch;
    }
}
