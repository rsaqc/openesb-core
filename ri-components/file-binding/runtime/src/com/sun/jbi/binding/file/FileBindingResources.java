/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileBindingResources.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

/**
 * Interface to store the keys for localization.
 *
 * @author Sun Microsystems, Inc.
  */
public interface FileBindingResources
{
    /**
     * Denotes completion of endpoint activation.
     */
    String FBC_ACTIVATE_ENDPOINTS_DONE = "FBC_ACTIVATE_ENDPOINTS_DONE";

    /**
     * Denotes failure of outbound endpoint actiavtion.
     */
    String FBC_ACTIVATE_OUTBOUND_FAILED = "FBC_ACTIVATE_OUTBOUND_FAILED";

    /**
     * Denotes success of outbound endpoint activation.
     */
    String FBC_ACTIVATE_OUTBOUND_SUCCESS = "FBC_ACTIVATE_OUTBOUND_SUCCESS";

    /**
     *
     */

    /**
     *    
     */
    String FBC_ACTIVATING_ENDPOINTS = "FBC_ACTIVATING_ENDPOINTS";

    /**
     *
     */

    /**
     *    
     */
    String FBC_ADMIN_MBEAN_FAILED = "FBC_ADMIN_MBEAN_FAILED";

    /**
     *
     */

    /**
     *    
     */
    String FBC_BINDINGCHANNEL_FAILED = "FBC_BINDINGCHANNEL_FAILED";

    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_ACTIVATE = "FBC_CANNOT_ACTIVATE";

    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_ACTIVATE_ENDPOINT = "FBC_CANNOT_ACTIVATE_ENDPOINT";

    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_CREATE_FOLDER = "FBC_CANNOT_CREATE_FOLDER";

    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_DEACTIVATE_ENDPOINT = "FBC_CANNOT_DEACTIVATE_ENDPOINT";

    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_PARSE = "FBC_CANNOT_PARSE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_PROCESS1 = "FBC_CANNOT_PROCESS1";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_PROCESSRESPONSE = "FBC_CANNOT_PROCESSRESPONSE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_PROCESS_FILE = "FBC_CANNOT_PROCESS_FILE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CANNOT_SEND = "FBC_CANNOT_SEND";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CLEANUP = "FBC_CLEANUP";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CLEAN_FAILED = "FBC_CLEAN_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CLOSE_FAILED = "FBC_CLOSE_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CONFIGDATA_ERROR = "FBC_CONFIGDATA_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CONFIGFILE_PARSE_ERROR = "FBC_CONFIGFILE_PARSE_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CONFIGFILE_UNAVAILABLE = "FBC_CONFIGFILE_UNAVAILABLE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CONFIGFILE_VALID = "FBC_CONFIGFILE_VALID";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CONFIGFILE_WARNING = "FBC_CONFIGFILE_WARNING";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CONFIG_FILE_CHECK = "FBC_CONFIG_FILE_CHECK";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CONTENT_NULL = "FBC_CONTENT_NULL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_CREATED_FOLDER = "FBC_CREATED_FOLDER";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_DEACTIVATE_OUTBOUND_FAILED = "FBC_DEACTIVATE_OUTBOUND_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_DEPLOY_FAILED = "FBC_DEPLOY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_DEREGISTER_DEPLOYERMBEAN_FAILED =
        "FBC_DEREGISTER_DEPLOYERMBEAN_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_DEREGISTER_ENDPOINT = "FBC_DEREGISTER_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_DEREGISTER_ENDPOINT_FAILED = "FBC_DEREGISTER_ENDPOINT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_DONE_MESSAGE = "FBC_DONE_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_DUPLICATE_DEPLOYMENT = "FBC_DUPLICATE_DEPLOYMENT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_DUPLICATE_ENDPOINT = "FBC_DUPLICATE_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_EXCHANGE_DONE = "FBC_EXCHANGE_DONE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_EXCHANGE_ERROR = "FBC_EXCHANGE_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FAILED_WRITE = "FBC_FAILED_WRITE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FILE_WRITE_FAILED = "FBC_FILE_WRITE_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FRAMEWORK_BUSYPOOL_SIZE = "FBC_FRAMEWORK_BUSYPOOL_SIZE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FRAMEWORK_FREEPOOL_SIZE = "FBC_FRAMEWORK_FREEPOOL_SIZE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FRAMEWORK_INVALID_SERVICENAME =
        "FBC_FRAMEWORK_INVALID_SERVICENAME";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FRAMEWORK_STARTPOOL = "FBC_FRAMEWORK_STARTPOOL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FRAMEWORK_THREAD_PROCESS = "FBC_FRAMEWORK_THREAD_PROCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FRAMWEORK_RUNNING_THREAD = "FBC_FRAMWEORK_RUNNING_THREAD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_FRAMWEORK_SHUTDOWN_THREAD = "FBC_FRAMWEORK_SHUTDOWN_THREAD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_IGNORE_ENDPOINT = "FBC_IGNORE_ENDPOINT";

    /**
     *    
     */
    String FBC_INCONSISTENT_CONSUMER_DATA = "FBC_INCONSISTENT_CONSUMER_DATA";

    /**
     *    
     */
    String FBC_INCONSISTENT_DATA = "FBC_INCONSISTENT_DATA";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INCONSISTENT_OPERATION = "FBC_INCONSISTENT_OPERATION";

    /**
     *    
     */
    String FBC_INCONSISTENT_PROVIDER_DATA = "FBC_INCONSISTENT_PROVIDER_DATA";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INIT_FAILED = "FBC_INIT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INIT_START = "FBC_INIT_START";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INOPTIONALOUT_NOT_SUPPORTED = "FBC_INOPTIONALOUT_NOT_SUPPORTED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INPUTFOLDER_NOTEXIST = "FBC_INPUTFOLDER_NOTEXIST";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INPUT_TYPE_NAME_NULL = "FBC_INPUT_TYPE_NAME_NULL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_BINDINGCONTEXT = "FBC_INVALID_BINDINGCONTEXT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_COMPROOT = "FBC_INVALID_COMPROOT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_CONFIG_FILE = "FBC_INVALID_CONFIG_FILE";

    /**
     *    
     */
    String FBC_INVALID_DD = "FBC_INVALID_DD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_ENDPOINT = "FBC_INVALID_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_ENDPOINTTYPE = "FBC_INVALID_ENDPOINTTYPE";

    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_ENDPOINT_INFO = "FBC_INVALID_ENDPOINT_INFO";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_ENDPOINT_TYPE = "FBC_INVALID_ENDPOINT_TYPE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_INPUTFOLDER = "FBC_INVALID_INPUTFOLDER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_INPUT_FILE = "FBC_INVALID_INPUT_FILE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_MEP = "FBC_INVALID_MEP";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_MESSAGE_STATE = "FBC_INVALID_MESSAGE_STATE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_OPERATION = "FBC_INVALID_OPERATION";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_OPERATION_FILE = "FBC_INVALID_OPERATION_FILE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_OUTPUTFOLDER = "FBC_INVALID_OUTPUTFOLDER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_PATTERN = "FBC_INVALID_PATTERN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_PROCESSEDFOLDER = "FBC_INVALID_PROCESSEDFOLDER";

    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_PROVIDER_ENDPOINT_INFO =
        "FBC_INVALID_PROVIDER_ENDPOINT_INFO";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_SERVICE = "FBC_INVALID_SERVICE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_WSDL_FILE = "FBC_INVALID_WSDL_FILE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_INVALID_XML = "FBC_INVALID_XML";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_LOAD_CONFIG_FAILED = "FBC_LOAD_CONFIG_FAILED";

    /**
     *    
     */
    String FBC_LOAD_DD_FAILED = "FBC_LOAD_DD_FAILED";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_MEP_NULL = "FBC_MEP_NULL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_MESSAGE_CREATION_FAILED = "FBC_MESSAGE_CREATION_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_MESSAGE_RECEIVED = "FBC_MESSAGE_RECEIVED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_MOVE_FAILED = "FBC_MOVE_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NOT_VALID = "FBC_NOT_VALID";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_ARTIFACT = "FBC_NO_ARTIFACT";

    /**
     *    
     */
    String FBC_NO_DD = "FBC_NO_DD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_DEPLOYMENTS = "FBC_NO_DEPLOYMENTS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_ENDPOINTS = "FBC_NO_ENDPOINTS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_FREE_THREAD = "FBC_NO_FREE_THREAD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_NAMESPACE = "FBC_NO_NAMESPACE";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_OPERATIONS = "FBC_NO_OPERATIONS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_SERVICE = "FBC_NO_SERVICE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_TARGET_NAMESPACE = "FBC_NO_TARGET_NAMESPACE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_NO_WRITE_PERMISSION = "FBC_NO_WRITE_PERMISSION";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_OPERATION_NAMESPACE_NULL = "FBC_OPERATION_NAMESPACE_NULL";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_OPERATION_NAME_NULL = "FBC_OPERATION_NAME_NULL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_OUTPUTFOLDER_NOTEXIST = "FBC_OUTPUTFOLDER_NOTEXIST";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_OUTPUT_TYPE_NAME_NULL = "FBC_OUTPUT_TYPE_NAME_NULL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_PROCESSEDFOLDER_NOTEXIST = "FBC_PROCESSEDFOLDER_NOTEXIST";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_PROCESSED_FAILED = "FBC_PROCESSED_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_PROCESSED_SUCCESS = "FBC_PROCESSED_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_PROCESS_INONLY_FAILED = "FBC_PROCESS_INONLY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_PROCESS_INOUT_FAILED = "FBC_PROCESS_INOUT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_PROCESS_OUTONLY_FAILED = "FBC_PROCESS_OUTONLY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_PROCESS_ROBUSTINONLY_FAILED = "FBC_PROCESS_ROBUSTINONLY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_PROCESS_ROBUSTOUTONLY_FAILED =
        "FBC_PROCESS_ROBUSTOUTONLY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_REASON1 = "FBC_REASON1";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_INONLY = "FBC_RECEIVED_INONLY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_INOPTIONALOUT = "FBC_RECEIVED_INOPTIONALOUT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_INOUT = "FBC_RECEIVED_INOUT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_INVALID_MESSAGE = "FBC_RECEIVED_INVALID_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_OUTIN = "FBC_RECEIVED_OUTIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_OUTONLY = "FBC_RECEIVED_OUTONLY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_OUTOPTIONALIN = "FBC_RECEIVED_OUTOPTIONALIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_ROBUSTINONLY = "FBC_RECEIVED_ROBUSTINONLY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_ROBUSTOUTONLY = "FBC_RECEIVED_ROBUSTOUTONLY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVED_UNSUPPORTED_MEP = "FBC_RECEIVED_UNSUPPORTED_MEP";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVER_ERROR = "FBC_RECEIVER_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVER_START = "FBC_RECEIVER_START";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RECEIVER_STOP = "FBC_RECEIVER_STOP";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_REGISTER_DEPLOYERMBEAN_FAILED =
        "FBC_REGISTER_DEPLOYERMBEAN_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_REGISTER_ENDPOINT = "FBC_REGISTER_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_ROBUSTINONLY_NOTSUPPORTED = "FBC_ROBUSTINONLY_NOTSUPPORTED";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_RUNNING_THREAD = "FBC_RUNNING_THREAD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_SENDING_MESSAGE = "FBC_SENDING_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_SEND_FAILED = "FBC_SEND_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_SHUTDOWN_BEGIN = "FBC_SHUTDOWN_BEGIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_SHUTDOWN_END = "FBC_SHUTDOWN_END";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_START_BEGIN = "FBC_START_BEGIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_START_DEPLOYMENT = "FBC_START_DEPLOYMENT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_START_DEPLOYMENT_FAILED = "FBC_START_DEPLOYMENT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_START_DEPLOYMENT_FAILED_BEANNULL =
        "FBC_START_DEPLOYMENT_FAILED_BEANNULL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_START_DEPLOYMENT_SUCCESS = "FBC_START_DEPLOYMENT_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_START_END = "FBC_START_END";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_START_ENDPOINT_FAILED = "FBC_START_ENDPOINT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_BEGIN = "FBC_STOP_BEGIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_DEPLOYMENT = "FBC_STOP_DEPLOYMENT";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_DEPLOYMENT_FAILED = "FBC_STOP_DEPLOYMENT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_DEPLOYMENT_SUCCESS = "FBC_STOP_DEPLOYMENT_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_END = "FBC_STOP_END";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_ENDPOINTTHREAD = "FBC_STOP_ENDPOINTTHREAD";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_ENDPOINT_FAILED = "FBC_STOP_ENDPOINT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_POLLING = "FBC_STOP_POLLING";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_STOP_WARNING = "FBC_STOP_WARNING";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_TASK_FAILED = "FBC_TASK_FAILED";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_BUSYTHREADS = "FBC_THREADS_BUSYTHREADS";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_CANNOT_INTERRUPT = "FBC_THREADS_CANNOT_INTERRUPT";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_COMMAND_FAILED = "FBC_THREADS_COMMAND_FAILED";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_INTERUPPTED = "FBC_THREADS_INTERUPPTED";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_POOL_STOP = "FBC_THREADS_POOL_STOP";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_SHUTDOWN = "FBC_THREADS_SHUTDOWN";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_STOPPED = "FBC_THREADS_STOPPED";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_WM_FAILED = "FBC_THREADS_WM_FAILED";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_WM_NOFREETHREAD = "FBC_THREADS_WM_NOFREETHREAD";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_WM_PROCESS = "FBC_THREADS_WM_PROCESS";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_THREADS_WM_THREADPOOL_FAILED =
        "FBC_THREADS_WM_THREADPOOL_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_TRY_LATER = "FBC_TRY_LATER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_UNJAR_FAILED = "FBC_UNJAR_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_UNKNOWN_ERROR = "FBC_UNKNOWN_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_WRITE_ERROR_FAILED = "FBC_WRITE_ERROR_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_WRITE_ERROR_SUCCESS = "FBC_WRITE_ERROR_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_WSDLFILE_UNAVAILABLE = "FBC_WSDLFILE_UNAVAILABLE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_WSDL_ENDPOINT_ERROR = "FBC_WSDL_ENDPOINT_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_WSDL_INVALID_DIRECTION = "FBC_WSDL_INVALID_DIRECTION";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_WSDL_INVALID_OPERATION = "FBC_WSDL_INVALID_OPERATION";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_WSDL_INVALID_URI = "FBC_WSDL_INVALID_URI";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_WSDL_NO_ENDPOINTS = "FBC_WSDL_NO_ENDPOINTS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String FBC_XML_STRING_CREATION_FAILED = "FBC_XML_STRING_CREATION_FAILED";
    
    /**
     *
     **/
    String FBC_NOT_PRESENT_IN_DD = "FBC_NOT_PRESENT_IN_DD";
    
    String FBC_ARTIFACT_NOT_FOUND = "FBC_ARTIFACT_NOT_FOUND";
    
    String FBC_DD_DATA_NOT_FOUND = "FBC_DD_DATA_NOT_FOUND";
    
    String FBC_NO_LOCK = "FBC_NO_LOCK";
    
    String FBC_RESET_SOURCE_FAILED = "FBC_RESET_SOURCE_FAILED";
    
    String FBC_DESCRIPTOR_NOT_FOUND = "FBC_DESCRIPTOR_NOT_FOUND";
}
