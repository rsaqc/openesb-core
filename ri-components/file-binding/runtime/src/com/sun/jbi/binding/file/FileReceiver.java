/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileReceiver.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.framework.WorkManager;
import com.sun.jbi.binding.file.util.StringTranslator;

import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchange;


/**
 * The File Reciever class receives the Normalized Message from the  NMR and
 * writes the contents to the directory specified in file.xml
 *
 * @author Sun Microsystems, Inc.
 */
class FileReceiver
    implements Runnable, FileBindingResources
{
    /**
     * Time out for receive
     */
    private static final long TIME_OUT = 500;

    /**
     * Wait time for thread.
     */
    private static final long WAIT_TIME = 100;

    /**
     * Receiver.
     */
    private static final String RECEIVER = "RECEIVER";

    /**
     * Binding channel.
     */
    private DeliveryChannel mChannel;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * Normalized Message
     */
    private MessageExchange mExchange;

    /**
     * Monitor Object to stop the thread.
     */
    private Object mMonitor;

    /**
     * Helper for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * Thread framework object.
     */
    private WorkManager mWorkManager;

    /**
     * Creates the FileReceiver Thread.
     *
     * @param bc Thread group for this receiver
     */
    public FileReceiver(DeliveryChannel bc)
    {
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        mChannel = bc;
        mWorkManager = WorkManager.getWorkManager(RECEIVER);
        mMonitor = new Object();
    }

    /**
     * Blocking call on the service channel to receive the message.
     */
    public void run()
    {
        mLog.info(mTranslator.getString(FBC_RECEIVER_START));
        mWorkManager.start();

        while (mMonitor != null)
        {
            try
            {
                mExchange = mChannel.accept(TIME_OUT);

                if (mExchange != null)
                {
                    MessageProcessor proc =
                        new MessageProcessor(mChannel, mExchange);
                    mLog.info(mTranslator.getString(FBC_MESSAGE_RECEIVED));

                    if (!mWorkManager.processCommand(proc))
                    {
                        mLog.info(mTranslator.getString(FBC_NO_FREE_THREAD));
                    }
                }
            }
            catch (Exception e)
            {
                mLog.info(mTranslator.getString(FBC_RECEIVER_ERROR));
                mLog.info(e.getMessage());
                mWorkManager.cease();

                return;
            }
        }
    }

    /**
     * Stops the receiving thread.
     */
    public void stopReceiving()
    {
        mLog.fine(mTranslator.getString(FBC_RECEIVER_STOP));
        mMonitor = null;
    }
}
