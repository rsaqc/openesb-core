/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestInputFileFilter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestInputFileFilter extends TestCase
{
    /**
     * Creates a new TestInputFileFilter object.
     *
     * @param testName DOCUMENT ME!
     */
    public TestInputFileFilter(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestInputFileFilter.class);

        return suite;
    }

    /**
     * Test of accept method, of class
     * com.sun.jbi.binding.file.util.InputFileFilter.
     */
    public void testAccept()
    {
        System.out.println("testAccept");
    }

    /**
     * Test of getFilterexpression method, of class
     * com.sun.jbi.binding.file.util.InputFileFilter.
     */
    public void testGetFilterexpression()
    {
        System.out.println("testGetFilterexpression");
    }

    /**
     * Test of setFilterexpression method, of class
     * com.sun.jbi.binding.file.util.InputFileFilter.
     */
    public void testSetFilterexpression()
    {
        System.out.println("testSetFilterexpression");
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
