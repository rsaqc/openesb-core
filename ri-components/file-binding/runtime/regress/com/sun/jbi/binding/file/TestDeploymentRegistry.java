/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDeploymentRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestDeploymentRegistry extends TestCase
{
    /**
     *    
     */
    private DeploymentRegistry mRegistry;

    /**
     *    
     */
    private EndpointBean mEndpointBean;

    /**
     * Creates a new TestDeploymentRegistry object.
     *
     * @param testName DOCUMENT ME!
     */
    public TestDeploymentRegistry(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestDeploymentRegistry.class);

        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        mRegistry = DeploymentRegistry.getInstance();
        mEndpointBean = new EndpointBean();
    }

    /**
     * Test of dump method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testDump()
    {
        System.out.println("testDump");
    }

    /**
     * Test of findEndpoint method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testFindEndpoint()
    {
        System.out.println("testFindEndpoint");
        mRegistry.registerEndpoint("testendpoint", mEndpointBean);

        EndpointBean eb = mRegistry.findEndpoint("testendpoint");
        assertNotNull("Endpoint Bean is Null ", eb);
    }

    /**
     * Test of getDeploymentStatus method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testGetDeploymentStatus()
    {
        System.out.println("testGetDeploymentStatus");
        mEndpointBean.setDeploymentId("ASA1");
        mRegistry.registerEndpoint("testendpoint", mEndpointBean);
        assertEquals(true, mRegistry.getDeploymentStatus("ASA1"));
    }

    /**
     * Test of getEndpoints method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testGetEndpoints()
    {
        System.out.println("testGetEndpoints");
        mEndpointBean.setDeploymentId("ASA1");
        mRegistry.registerEndpoint("testendpoint", mEndpointBean);
        assertNotNull(mRegistry.getEndpoints("ASA1"));
    }

    /**
     * Test of getInstance method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testGetInstance()
    {
        System.out.println("testGetInstance");
    }

    /**
     * Test of isDeployed method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testIsDeployed()
    {
        System.out.println("testIsDeployed");
        mEndpointBean.setDeploymentId("ASA1");
        mRegistry.registerEndpoint("testendpoint", mEndpointBean);
        assertEquals(true, mRegistry.isDeployed("testendpoint"));
    }

    /**
     * Test of listAllServiceLists method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testListAllServiceLists()
    {
        System.out.println("testListAllServiceLists");
    }

    /**
     * Test of registerEndpoint method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testRegisterEndpoint()
    {
        System.out.println("testRegisterEndpoint");
        mEndpointBean.setDeploymentId("ASA1");
        mRegistry.registerEndpoint("testendpoint", mEndpointBean);
        assertNotNull("Endpoint Bean is Null ",
            mRegistry.getEndpoints("testendpoint"));
    }

    /**
     * Test of setComproot method, of class
     * com.sun.jbi.binding.file.DeploymentRegistry.
     */
    public void testSetComproot()
    {
        System.out.println("testSetComproot");
    }
}
