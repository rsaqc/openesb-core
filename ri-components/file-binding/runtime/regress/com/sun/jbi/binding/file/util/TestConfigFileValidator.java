/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestConfigFileValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestConfigFileValidator extends TestCase
{
    /**
     * Creates a new TestConfigFileValidator object.
     */
    private ConfigFileValidator mValidator;

    /**
     * Creates a new TestConfigFileValidator object.
     *
     * @param testName DOCUMENT ME!
     */
    public TestConfigFileValidator(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestConfigFileValidator.class);

        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        String srcroot = System.getProperty("junit.srcroot");
        mValidator =
            new ConfigFileValidator(srcroot
                + "/binding/file/schema/endpoints.xsd",
                srcroot + "/binding/file/config/endpoints.xml");
    }

    /**
     * Test of getDocument method, of class
     * com.sun.jbi.binding.file.util.ConfigFileValidator.
     */
    public void testGetDocument()
    {
        System.out.println("testGetDocument");
        mValidator.validate();
        assertTrue("Failed validating XML config file", mValidator.isValid());
        assertNotNull("Document object is null", mValidator.getDocument());
    }

    /**
     * Test of validate method, of class
     * com.sun.jbi.binding.file.util.ConfigFileValidator.
     */
    public void testValidate()
    {
        System.out.println("testValidate");
        mValidator.validate();
        assertTrue("Failed validating XML config file", mValidator.isValid());
    }
}
