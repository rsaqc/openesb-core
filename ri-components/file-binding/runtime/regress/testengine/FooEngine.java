/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FooEngine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package testengine;

import java.io.File;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Logger;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.RobustInOnly;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

/**
 * This class implements EngineLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Sun Microsystems, Inc.
 */
public class FooEngine implements ComponentLifeCycle, Component
{
    /**
     * In Only MEP.
     */
    public static final String IN_ONLY =
        "http://www.w3.org/ns/wsdl/in-only";

    /**
     * In Out MEP.
     */
    public static final String IN_OUT = "http://www.w3.org/ns/wsdl/in-out";

    /**
     * In Optional Out MEP.
     */
    public static final String IN_OPTIONAL_OUT =
        "http://www.w3.org/ns/wsdl/in-opt-out";

    /**
     * Robust In Only MEP.
     */
    public static final String ROBUST_IN_ONLY =
        "http://www.w3.org/ns/wsdl/robust-in-only";

    /**
     *    
     */
    private static final String SERVICE_NAMESPACE =
        "http://sun.com/transform-file.wsdl";

    /**
     *    
     */
    private static final String INBOUND_SERVICE_NAME = "transformService";
    
    private static final String INBOUND_ENDPOINT_NAME = "filetxEP";

    /**
     *    
     */
    private static final String OUTBOUND_SERVICE_NAME = "outboundService";


    /**
     *    
     */
    public Thread mRecv;

    /**
     *    
     */
    private ServiceEndpoint mEndpoint;

    /**
     *
     */

    /**
     *    
     */
    private DeliveryChannel mChannel;
    
    /**
     *    
     */
    private MessageExchangeFactory mFactory;

    /**
     * Environment conext passed down from framework to this stockquote engine.
     */
    private ComponentContext mContext = null;

    /**
     * Refernce to logger.
     */
    private Logger mLog = null;

    /**
     *    
     */
    private MessageExchange mReq;

    /**
     *    
     */
    private ServiceEndpoint mInboundReference;

    /**
     *    
     */
    private QName mOutbound;

    /**
     *    
     */
    private boolean mRunFlag = true;

    /**
     * Get the JMX ObjectName for the DeploymentMBean for this BPE. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the DeploymentMBean or null if
     *         there is no DeploymentMBean.
     */
    public javax.management.ObjectName getDeploymentMBeanName()
    {
        return null;
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Initialize the test engine. This performs initialization required
     *
     * @param jbiContext the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the stockquote engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext jbiContext)
        throws javax.jbi.JBIException
    {
        mLog = Logger.getLogger(this.getClass().getPackage().getName());
        mLog.info("test engine INIT invoked");

        if (jbiContext == null)
        {
            throw (new javax.jbi.JBIException("Context is null",
                new NullPointerException()));
        }

        /* Assign values */
        mContext = jbiContext;
        Payload.mDefaultPath =
            mContext.getInstallRoot() + File.separatorChar + "testengine"
            + File.separatorChar + "payload.xml";
                Payload.mDefaultOutPath =
            mContext.getInstallRoot() + File.separatorChar + "testengine"
            + File.separatorChar + "response.xml";
        mLog.info(" INIT METHOD FINISHED ");
    }

    /**
     * Shutdown the SE. This performs cleanup before the SE is terminated.
     * Once this has been called, init() must be called before the SE can be
     * started again with a call to start().
     *
     * @throws javax.jbi.JBIException if the SE is unable to shut down.
     */
    public void shutDown() throws javax.jbi.JBIException
    {
        mLog.info("test Engine Shutdown-Start");
        mLog.info("test Engine Shutdown-End");
    }

    /**
     */
    public void start() throws javax.jbi.JBIException
    {
        mLog.info(" Test  ENGINE INVOKED ");

        try
        {
            mChannel = mContext.getDeliveryChannel();
            mFactory = mChannel.createExchangeFactory();

            mOutbound =
                new QName(SERVICE_NAMESPACE, OUTBOUND_SERVICE_NAME);

            QName inbound = new QName(SERVICE_NAMESPACE, INBOUND_SERVICE_NAME);
            mInboundReference = mContext.activateEndpoint(inbound, INBOUND_ENDPOINT_NAME);
            mLog.info("Activated endpoint " + mInboundReference.toString());
        }
        catch (MessagingException me)
        {
            me.printStackTrace();
        }

        if (mChannel == null)
        {
            mLog.severe("Cannot get Engine Channel from context ");

            return;
        }

        doTestGoodOutOnly();

        doTestBadOutOnly();

        doTestGoodRobustOutOnly();

        doTestBadRobustOutOnly();

        mRecv = new Thread(new Receiver());
        mRecv.start();

        String compRoot = mContext.getInstallRoot();

        mLog.info(" START FINISHED  ");
    }

    /**
     */
    public void stop() throws javax.jbi.JBIException
    {
        mLog.info("STOP INVOKED ");
        mRunFlag = false;

        try
        {            
            mContext.deactivateEndpoint(mInboundReference);
            mRecv.interrupt();
            mRecv.join();
        }
        catch (Exception e)
        {
            ;
        }

        mLog.info(" STOP FINISHED ");
    }

    /**
     *
     */
    private void doTestBadOutOnly()
    {
        mLog.info("Test engine sending bad out-only message to binding");

        NormalizedMessage outMsg;
        InOnly inOnly;

        try
        {
            // create the exchange
            inOnly = mFactory.createInOnlyExchange();
            outMsg = inOnly.createMessage();

            // set the stuff we know
            inOnly.setService(mOutbound);
            inOnly.setOperation(new QName(SERVICE_NAMESPACE,"transforminonly"));

            // lookup the endpoint reference and set on exchange
            mEndpoint = mContext.getEndpointsForService(mOutbound)[0];
            inOnly.setEndpoint(mEndpoint);

            // set the payload
            Payload.setBadPayload(outMsg);

            // set the message on the exchange
            inOnly.setInMessage(outMsg);

            // send the exchange
            mChannel.send(inOnly);

            // receive the response
            inOnly = (InOnly) mChannel.accept();
            mLog.info("Received status " + inOnly.getStatus().toString());

            if (inOnly.getStatus() == ExchangeStatus.ERROR)
            {
                mLog.info("Test-BADOUTONLY-Passed");
            }
            else
            {
                mLog.info("Test-BADOUTONLY-Failed");
            }
        }
        catch (Exception e)
        {
            mLog.severe("Test-BADOUTONLY-threw-Exception");
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestBadRobustOutOnly()
    {
        NormalizedMessage outMsg;
        Exception error;
        RobustInOnly robInOnly;

        try
        {
            // create the exchange
            robInOnly = mFactory.createRobustInOnlyExchange();
            outMsg = robInOnly.createMessage();

            // set the stuff we know
            robInOnly.setService(mOutbound);
            robInOnly.setOperation(new QName(SERVICE_NAMESPACE,"transformrobustinonly"));

            // lookup the endpoint reference and set on exchange
            mEndpoint = mContext.getEndpointsForService(mOutbound)[0];
            robInOnly.setEndpoint(mEndpoint);

            // set the payload
            Payload.setBadPayload(outMsg);
            robInOnly.setInMessage(outMsg);

            // Now we can proceed normally            
            mChannel.send(robInOnly);

            // receive the response
            robInOnly = (RobustInOnly) mChannel.accept();

            // check for a fault or error
            error = robInOnly.getError();

            if (error != null)
            {
                mLog.info("Test-BADROBUSTOUTONLY-Passed");
            }
        }
        catch (Exception e)
        {
            mLog.severe("Test-BADROBUSTOUTONLY-threw-Exception");
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestGoodInOnly()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            InOnly inonly = (InOnly) mReq;

            if (inonly.getInMessage() == null)
            {
                mLog.info("Test-GOODINONLY-Failed");

                return;
            }

            if (inonly.getInMessage().getContent() == null)
            {
                mLog.info("Test-GOODINONLY-Failed");

                return;
            }

            inonly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inonly);
            mLog.info("Test-GOODINONLY-Passed");
        }
        catch (Exception e)
        {
            mLog.severe("Test-GOODINONLY-threw-Exception" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestGoodInOut()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            InOut inout = (InOut) mReq;

            if (inout.getStatus() == ExchangeStatus.DONE)
            {
                mLog.info("Test-GOODINOUT-Passed");

                return;
            }

            if (inout.getStatus() == ExchangeStatus.ERROR)
            {
                mLog.info("Test-GOODINOUT-Failed");

                return;
            }

            if (inout.getInMessage() == null)
            {
                mLog.info("Test-GOODINOUT-Failed");

                return;
            }
            NormalizedMessage outmsg = inout.createMessage();
            Payload.setResponse(outmsg);
            inout.setOutMessage(outmsg);
            mChannel.send(inout);
        }
        catch (Exception e)
        {
            mLog.severe("Test-GOODINOUT-threw-Exception" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestGoodOutOnly()
    {
        mLog.info("Test engine sending good out-only message to binding");

        NormalizedMessage outMsg;
        InOnly inOnly;

        try
        {
            // create the exchange
            inOnly = mFactory.createInOnlyExchange();
            outMsg = inOnly.createMessage();

            // set the stuff we know
            inOnly.setService(mOutbound);
            inOnly.setOperation(new QName(SERVICE_NAMESPACE,"transforminonly"));

            ServiceEndpoint mEndpoint;

            // lookup the endpoint reference and set on exchange
            mEndpoint = mContext.getEndpointsForService(mOutbound)[0];
            inOnly.setEndpoint(mEndpoint);

            // set the payload
            Payload.setPayload(outMsg);

            // set the message on the exchange
            inOnly.setInMessage(outMsg);

            // send the exchange
            mChannel.send(inOnly);

            // receive the response
            inOnly = (InOnly) mChannel.accept();

            if (inOnly.getStatus() == ExchangeStatus.DONE)
            {
                mLog.info("Test-GOODOUTONLY-Passed");
            }
            else
            {
                mLog.info("Test-GOODOUTONLY-Failed");
            }
        }
        catch (Exception e)
        {
            mLog.severe("Test-GOODOUTONLY-threw-Exception");
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestGoodRobustInOnly()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            RobustInOnly robinonly = (RobustInOnly) mReq;

            if (robinonly.getInMessage() == null)
            {
                mLog.info("Test-GOODROBUSTINONLY-Failed");
                robinonly.setError(new Exception("Message is null"));
                mChannel.send(robinonly);

                return;
            }

            if (robinonly.getInMessage().getContent() == null)
            {
                mLog.info("Test-GOODROBUSTINONLY-Failed");

                return;
            }

            robinonly.setStatus(ExchangeStatus.DONE);
            mChannel.send(robinonly);
            mLog.info("Test-GOODROBUSTINONLY-Passed");
        }
        catch (Exception e)
        {
            mLog.severe("Test-GOODROBUSTINONLY-threw-Exception"
                + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestGoodRobustOutOnly()
    {
        NormalizedMessage outMsg;
        Fault fault;
        RobustInOnly robInOnly;

        try
        {
            // create the exchange
            robInOnly = mFactory.createRobustInOnlyExchange();
            outMsg = robInOnly.createMessage();

            // set the stuff we know
            robInOnly.setService(mOutbound);
            robInOnly.setOperation(new QName(SERVICE_NAMESPACE,"transformrobustinonly"));

            // lookup the endpoint reference and set on exchange
            mEndpoint = mContext.getEndpointsForService(mOutbound)[0];
            robInOnly.setEndpoint(mEndpoint);

            // set the payload
            Payload.setPayload(outMsg);
            robInOnly.setInMessage(outMsg);

            // Now we can proceed normally            
            mChannel.send(robInOnly);

            // receive the response
            robInOnly = (RobustInOnly) mChannel.accept();

            // check for a fault
            fault = robInOnly.getFault();

            if ((fault == null)
                    && (robInOnly.getStatus() == ExchangeStatus.DONE))
            {
                mLog.info("Test-GOODROBUSTOUTONLY-Passed");
            }
        }
        catch (Exception e)
        {
            mLog.severe("Test-GOODROBUSTOUTONLY-threw-Exception");
            e.printStackTrace();
        }
    }

    /**
     *   
     *
     * @author Sun Microsystems, Inc.
     * 
     */
    public class Receiver implements Runnable
    {
        /**
         *
         */
        public void run()
        {
            mLog.info("Test engine receiver started");

            try
            {
                while (mRunFlag)
                {
                    mReq = mChannel.accept();
                    mLog.info("Got message i test ebgine");

                    /* start test for resolver               
                    Descriptor d =
                        mChannel.getEndpointDescriptor(mReq.getEndpoint());

                    mLog.info("Got a descriptor with "
                        + d.getDescription().getDocumentElement().getLocalName());
                     */

                    if (mReq instanceof InOut)
                    {
                        doTestGoodInOut();
                    }
                    else if (mReq instanceof InOnly)
                    {
                        doTestGoodInOnly();
                    }
                    else if (mReq instanceof RobustInOnly)
                    {
                        doTestGoodRobustInOnly();
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace(System.out);
                mLog.severe("Exception at receiver");
                mLog.info("Test engine receiver ended");
            }
        }
    }
    
    
    public javax.jbi.component.ComponentLifeCycle getLifeCycle() 
    {
        return this;
    }
    
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager() 
    {
        return null;
    }
    
    public Document getServiceDescription(
            javax.jbi.servicedesc.ServiceEndpoint ServiceEndpoint) 
    {
        return null;
    }
    
    
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
}
