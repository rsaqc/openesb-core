/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TransformationEngineComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import java.util.logging.Logger;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.servicedesc.ServiceEndpoint;

import com.sun.jbi.engine.xslt.TransformationEngineContext;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

/**
 * This class implements javax.jbi.component.Component interface. 
 *
 *
 * @author Sun Microsystems, Inc.
 */
public class TransformationEngineComponent
    implements Component, TEResources
{
    /**
     * TransformationEngineLifeCyle object that implements
     * javax.jbi.component.ComponentLifeCycle interface.
     */
    private TransformationEngineLifeCycle mLifeCycleObj;
    
    public TransformationEngineComponent()
    {
        mLifeCycleObj = new TransformationEngineLifeCycle();
    }
    // javax.jbi.component.Component methods

    /**
     * Returns an object of TransformationEngineLifeCycle.
     *
     * @return ComponentLifeCycle Object implementing
     *         javax.jbi.component.ComponentLifeCycle interface.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return mLifeCycleObj;
    }

    /**
     * Returns an object implementing javax.jbi.component.ServiceUnitManager 
     * interface.
     *
     * @return ServiceUnitManager Object implementing
     *         javax.jbi.component.ServiceUnitManager interface.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        return mLifeCycleObj.getSUManager();
    }
    
    /**
     *
     * @param ref ServiceEndpoint object
     *
     * @return Descriptor Object implementing javax.jbi.servicedesc.Descriptor
     *         interface.
     */
    public Document getServiceDescription(ServiceEndpoint ref)
    {
	org.w3c.dom.Document desc = null;

	try
	{
    		desc  = TransformationEngineContext.getInstance().getServiceDescription(ref);
	}
	catch (Exception e)
	{
    		e.printStackTrace();
	}
	return desc;

    }

    
    /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. The consumer is described by the given 
     *  capabilities, and JBI has already ensured that a fit exists between the 
     *  set of required capabilities of the provider and the available 
     *  capabilities of the consumer, and vice versa. This matching consists of
     *  simple set matching based on capability names only. <br><br>
     *  Note that JBI assures matches on capability names only; it is the 
     *  responsibility of this method to examine capability values to ensure a 
     *  match with the consumer.
     *  @param endpoint the endpoint to be used by the consumer
     *  @param exchange the proposed message exchange to be performed
     *  @param consumerCapabilities the consumer�s capabilities and requirements
     *  @return true if this provider component can perform the the given 
     *   exchange with the described consumer
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. Ths provider is described 
     *  by the given capabilities, and JBI has already ensure that a fit exists 
     *  between the set of required capabilities of the consumer and the 
     *  available capabilities of the provider, and vice versa. This matching 
     *  consists of simple set matching based on capability names only. <br><br>
     *  Note that JBI assures matches on capability names only; it is the 
     *  responsibility of this method to examine capability values to ensure a 
     *  match with the provider.
     *  @param exchange the proposed message exchange to be performed
     *  @param providerCapabilities the provider�s capabilities and requirements
     *  @return true if this consurer component can interact with the described
     *   provider to perform the given exchange
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
}
