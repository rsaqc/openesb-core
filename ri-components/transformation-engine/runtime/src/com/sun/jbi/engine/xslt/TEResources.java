/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TEResources.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

/**
 * Interface for Localization Keys.
 *
 * @author Sun Microsystems, Inc.
  */
public interface TEResources
{
    /**
     *
     */
    String ACTIVE = "ACTIVE";

    /**
     *
     */
    String ADD_NEW_THREAD = "ADD_NEW_THREAD";

    /**
     *
     */
    String ADMINISTRATION_SERVICE = "ADMINISTRATION_SERVICE";

    /**
     *
     */
    String ADMIN_MBEAN = "ADMIN_MBEAN";

    /**
     *
     */
    String ADMIN_SERVICE = "ADMIN_SERVICE";

    /**
     *
     */
    String ALL_SERVICES_STOPPED = "ALL_SERVICES_STOPPED";

    /**
     *
     */
    String ALL_SERVICE_BEING_STOPPED = "ALL_SERVICE_BEING_STOPPED";

    /**
     *
     */
    String ALL_SERVICE_STOPPED = "ALL_SERVICE_STOPPED";

    /**
     *
     */
    String ALREADY_DEPLOYED_ON_TE = "ALREADY_DEPLOYED_ON_TE";

    /**
     *
     */
    String APPLICATIONS = "APPLICATIONS";

    /**
     *
     */
    String BEAN_UPLOADED = "BEAN_UPLOADED";

    /**
     *
     */
    String BECAUSE = "BECAUSE";

    /**
     *
     */
    String BEING_STOPPED = "BEING_STOPPED";

    /**
     *
     */
    String BLD_MGMT_MSG_OBJ = "BLD_MGMT_MSG_OBJ";

    /**
     *
     */
    String BOOTSTRAP_ONINSTALL = "BOOTSTRAP_ONINSTALL";

    /**
     *
     */
    String BOOTSTRAP_ONUNINSTALL = "BOOTSTRAP_ONUNINSTALL";

    /**
     *
     */
    String BUSY_THREAD_POOL_SIZE = "BUSY_THREAD_POOL_SIZE";

    /**
     *
     */
    String CACHED_TRANSFORMERFACTORY_NOT_PRESENT =
        "CACHED_TRANSFORMERFACTORY_NOT_PRESENT";

    /**
     *
     */
    String CANNOT_BE_FOUND = "CANNOT_BE_FOUND";

    /**
     *
     */
    String CANNOT_CREATE_MANAGEMENT_BUILDER = "CANNOT_CREATE_MANAGEMENT_BUILDER";

    /**
     *
     */
    String CANNOT_CREATE_MGMT_XML_MSG = "CANNOT_CREATE_MGMT_XML_MSG";

    /**
     *
     */
    String CANNOT_CREATE_XML_MESSAGE = "CANNOT_CREATE_XML_MESSAGE";

    /**
     *
     */
    String CAN_NOT_START_DEPLOYMENT = "CAN_NOT_START_DEPLOYMENT";

    /**
     *
     */
    String CHANNEL_NULL = "CHANNEL_NULL";

    /**
     *
     */
    String CHECK_FAILED = "CHECK_FAILED";

    /**
     *
     */
    String COLUMN_HEADER = "COLUMN_HEADERR";

    /**
     *
     */
    String COLUMN_HEADERS = "COLUMN_HEADERS";

    /**
     *
     */
    String COLUMN_SEPARATOR = "COLUMN_SEPARATOR";

    /**
     *
     */
    String COL_HEADERS_NOT_PRESENT = "COL_HEADERS_NOT_PRESENT";

    /**
     *
     */
    String COL_HEADER_AND_FIRST_ROW = "COL_HEADER_AND_FIRST_ROW";

    /**
     *
     */
    String COL_HEADER_NOT_PRESENT = "COL_HEADER_NOT_PRESENT";

    /**
     *
     */
    String COMPILATION_OF_XSLT = "COMPILATION_OF_XSLT";

    /**
     *
     */
    String COMPONENTEXLOCMESSAGE = "COMPONENTEXLOCMESSAGE";

    /**
     *
     */
    String COMPONENTEXLOCPARAM = "COMPONENTEXLOCPARAM";

    /**
     *
     */
    String COMPONENTEXLOCTOKEN = "COMPONENTEXLOCTOKEN";

    /**
     *
     */
    String COMPONENTEXSTACKTRACE = "COMPONENTEXSTACKTRACE";

    /**
     *
     */
    String COMPONENT_ID = "COMPONENT_ID";

    /**
     *
     */
    String CONTEXT_IS_NULL = "CONTEXT_IS_NULL";
    
    /**
     *
     */
    String SUMANAGER_IS_NULL = "SUMANAGER_IS_NULL";

    /**
     *
     */
    String COULD_NOT_DEREGISTER = "COULD_NOT_DEREGISTER";

    /**
     *
     */
    String COULD_NOT_SEND_MESSAGE = "COULD_NOT_SEND_MESSAGE";

    /**
     *
     */
    String COULD_NOT_START_DEPLOYMENT = "COULD_NOT_START_DEPLOYMENT";

    /**
     *
     */
    String CREATEFAULTMESSAGE_START = "CREATEFAULTMESSAGE_START";

    /**
     *
     */
    String CREATE_CACHE = "CREATE_CACHE";

    /**
     *
     */
    String CREATING_NEW_WORKER_THREAD = "CREATING_NEW_WORKER_THREAD";

    /**
     *
     */
    String DELETE_FAILED = "DELETE_FAILED";

    /**
     *
     */
    String DEPLOYER = "DEPLOYER";

    /**
     *
     */
    String DEPLOYMENT = "DEPLOYMENT";

    /**
     *
     */
    String DEPLOYMENT_ID = "DEPLOYMENT_ID";

    /**
     *
     */
    String DEPLOYMENT_LISTER_DIR_NULL = "DEPLOYMENT_LISTER_DIR_NULL";

    /**
     *
     */
    String ENGINE_CHANNEL_IS_NULL = "ENGINE_CHANNEL_IS_NULL";

    /**
     *
     */
    String ENGINE_STARTED = "ENGINE_STARTED";

    /**
     *
     */
    String ERR_CASE_SERVIVCE_COULD_NOT_BE_CREATED =
        "ERR_CASE_SERVIVCE_COULD_NOT_BE_CREATED";

    /**
     *
     */
    String FAILED_TO_DEACTIVATE = "FAILED_TO_DEACTIVATE";

    /**
     *
     */
    String FAILED_TO_REGISTER = "FAILED_TO_REGISTER";

    /**
     *
     */
    String FAILED_TO_REGISTER_DEPLOYER_BEAN =
        "FAILED_TO_REGISTER_DEPLOYER_BEAN";

    /**
     *
     */
    String FAULT = "FAULT";

    /**
     *
     */
    String FINALLY = "FINALLY";

    /**
     *
     */
    String FIRST_ROW_COL_HEADERS = "FIRST_ROW_COL_HEADERS";

    /**
     *
     */
    String FOR_DEPLOYMENT = "FOR_DEPLOYMENT";

    /**
     *
     */
    String FREE_POOL_SIZE = "FREE_POOL_SIZE";

    /**
     *
     */
    String GEN_OUT_FILE = "GEN_OUT_FILE";

    /**
     *
     */
    String GETTING_SVC_BEAN = "GETTING_SVC_BEAN";

    /**
     *
     */
    String GET_TRANSFORMER_FACTORY = "GET_TRANSFORMER_FACTORY";

    /**
     *
     */
    String GOT_MESSAGEEXCHANGE = "GOT_MESSAGEEXCHANGE";

    /**
     *
     */
    String GOT_NULLPOINTEREXCEPTION = "GOT_NULLPOINTEREXCEPTION";

    /**
     *
     */
    String GOT_SERVICE = "GOT_SERVICE";

    /**
     *
     */
    String IGNORING_STATUS = "IGNORING_STATUS";

    /**
     *
     */
    String INIT_END = "INIT_END";

    /**
     *
     */
    String INIT_FAILED = "INIT_FAILED";

    /**
     *
     */
    String INIT_START = "INIT_START";

    /**
     *
     */
    String INPUTXML = "INPUTXML";

    /**
     *
     */
    String INPUT_FILE = "INPUT_FILE";

    /**
     *
     */
    String INVALID_DEPLOY_ID = "INVALID_DEPLOY_ID";

    /**
     *
     */
    String INVALID_INPUT = "INVALID_INPUT";

    /**
     *
     */
    String INVALID_REGISTRY_STATE = "INVALID_REGISTRY_STATE";

    /**
     *
     */
    String INVALID_SERVICE_LOCATION = "INVALID_SERVICE_LOCATION";

    /**
     *
     */
    String IO_ERROR = "IO_ERROR";

    /**
     *
     */
    String IS = "IS";

    /**
     *
     */
    String IS_ACTIVATED = "IS_ACTIVATED";

    /**
     *
     */
    String MAX_THREADS = "MAX_THREADS";

    /**
     *
     */
    String MESSAGE = "MESSAGE";

    /**
     *
     */
    String MESSAGEEXCHANGE_NULL = "MESSAGEEXCHANGE_NULL";

    /**
     *
     */
    String MGMT_MSG_OBJECT = "MGMT_MSG_OBJECT";

    /**
     *
     */
    String MIN_THREADS = "MIN_THREADS";

    /**
     *
     */
    String NON_ACTIVE = "NON_ACTIVE";

    /**
     *
     */
    String NON_XML = "NON_XML";

    /**
     *
     */
    String NOT_PROCESSED = "NOT_PROCESSED";

    /**
     *
     */
    String NO_DEPLOYMENTS = "NO_DEPLOYMENTS";

    /**
     *
     */
    String NO_DEPLOYMENTS_FOUND = "NO_DEPLOYMENTS_FOUND";

    /**
     *
     */
    String NO_ENGINE_CHANNEL = "NO_ENGINE_CHANNEL";

    /**
     *
     */
    String NO_FREE_THREAD_IN_POOL = "NO_FREE_THREAD_IN_POOL";

    /**
     *
     */
    String NO_OF_SERVICE = "NO_OF_SERVICE";

    /**
     *
     */
    String PROBLEM_SENDING_MESSAGE = "PROBLEM_SENDING_MESSAGE";

    /**
     *
     */
    String NO_PROCESSING_DONE = "NO_PROCESSING_DONE";

    /**
     *
     */
    String NO_TRANSFORMATIONIMPL = "NO_TRANSFORMATIONIMPL";

    /**
     *
     */
    String OTHER = "OTHER";

    /**
     *
     */
    String OUTPUT_FILE = "OUTPUT_FILE";

    /**
     *
     */
    String OUT_FILE_2 = "OUT_FILE_2";

    /**
     *
     */
    String PACKAGE = "PACKAGE";

    /**
     *
     */
    String PARENT_DIR = "PARENT_DIR";

    /**
     *
     */
    String PARENT_DIR_FAILED = "PARENT_DIR_FAILED";

    /**
     *
     */
    String PARSER_CONFIG_EX = "PARSER_CONFIG_EX";

    /**
     *
     */
    String PARSING_ERROR = "PARSING_ERROR";

    /**
     *
     */
    String PATTERN_EXCG_ID = "PATTERN_EXCG_ID";

    /**
     *
     */
    String PUSHING_TRANSFORMATIONIMPL_FAILED =
        "PUSHING_TRANSFORMATIONIMPL_FAILED";

    /**
     *
     */
    String RCVD_IN_OUT = "RCVD_IN_OUT";

    /**
     *
     */
    String RCVD_IN_OUT_MSG_EX = "RCVD_IN_OUT_MSG_EX";

    /**
     *
     */
    String RECECIVER_FINISHED = "RECECIVER_FINISHED";

    /**
     *
     */
    String RECEIVED_INVALID_PATTERN = "RECEIVED_INVALID_PATTERN";

    /**
     *
     */
    String RECEIVER_CONSTRUCTOR = "RECEIVER_CONSTRUCTOR";

    /**
     *
     */
    String RECEIVER_EXIT = "RECEIVER_EXIT";

    /**
     *
     */
    String RECEIVER_INIT = "RECEIVER_INIT";

    /**
     *
     */
    String RECEIVER_INIT_END = "RECEIVER_INIT_END";

    /**
     *
     */
    String RECEIVER_START = "RECEIVER_START";

    /**
     *
     */
    String RECEIVER_START_END = "RECEIVER_START_END";

    /**
     *
     */
    String RECEIVER_TERMINATED = "RECEIVER_TERMINATED";

    /**
     *
     */
    String RECEVIER_CEASE = "RECEVIER_CEASE";

    /**
     *
     */
    String RECEVIER_CEASE_END = "RECEVIER_CEASE_END";

    /**
     *
     */
    String RECEVIER_EXEC = "RECEVIER_EXEC";

    /**
     *
     */
    String RECORD = "RECORD";

    /**
     *
     */
    String REQ_STREAMSOURCE = "REQ_STREAMSOURCE";

    /**
     *
     */
    String REQ_TYPE = "REQ_TYPE";

    /**
     *
     */
    String RESPONSE = "RESPONSE";

    /**
     *
     */
    String RESULT_NULL = "RESULT_NULL";

    /**
     *
     */
    String RETRIEVED_INPUT = "RETRIEVED_INPUT";

    /**
     *
     */
    String RETRIVED_SERVICE_NULL = "RETRIVED_SERVICE_NULL";

    /**
     *
     */
    String RUNNING_THREAD = "RUNNING_THREAD";

    /**
     *
     */
    String SENDING_MESSAGE_EXCHANGE = "SENDING_MESSAGE_EXCHANGE";

    /**
     *
     */
    String SEQUENCING_LOG = "SEQUENCING_LOG";

    /**
     *
     */
    String SERVICE = "SERVICE";

    /**
     *
     */
    String SERVICEEXECUTOR_END = "SERVICEEXECUTOR_END";

    /**
     *
     */
    String SERVICEEXECUTOR_EXECUTE_END = "SERVICEEXECUTOR_EXECUTE_END";

    /**
     *
     */
    String SERVICEEXECUTOR_START = "SERVICEEXECUTOR_START";

    /**
     *
     */
    String SERVICENAME_NULL = "SERVICENAME_NULL";

    /**
     *
     */
    String SERVICENAME_NULL_CANT_DESTROY = "SERVICENAME_NULL_CANT_DESTROY";

    /**
     *
     */
    String SERVICEXML_NOT_FOUND = "SERVICEXML_NOT_FOUND";

    /**
     *
     */
    String SERVICE_CONFIG_NOT_FOUND = "SERVICE_CONFIG_NOT_FOUND";

    /**
     *
     */
    String SERVICE_COULD_NOT_BE_ACTIVATED = "SERVICE_COULD_NOT_BE_ACTIVATED";

    /**
     *
     */
    String SERVICE_CREATED = "SERVICE_CREATED";

    /**
     *
     */
    String SERVICE_DESC = "SERVICE_DESC";

    /**
     *
     */
    String SERVICE_DESTROYED = "SERVICE_DESTROYED";

    /**
     *
     */
    String SERVICE_ID = "SERVICE_ID";

    /**
     *
     */
    String SERVICE_IS = "SERVICE_IS";

    /**
     *
     */
    String SERVICE_IS_BEING_STOPPED = "SERVICE_IS_BEING_STOPPED";

    /**
     *
     */
    String SERVICE_IS_NULL = "SERVICE_IS_NULL";

    /**
     *
     */
    String SERVICE_IS_NULL_CANT_DESTROY = "SERVICE_IS_NULL_CANT_DESTROY";

    /**
     *
     */
    String SERVICE_IS_STOPPED = "SERVICE_IS_STOPPED";

    /**
     *
     */
    String SERVICE_NAME = "SERVICE_NAME";

    /**
     *
     */
    String SERVICE_NOT_DEPLOYED = "SERVICE_NOT_DEPLOYED";

    /**
     *
     */
    String SERVICE_NOT_RUNNING = "SERVICE_NOT_RUNNING";

    /**
     *
     */
    String SERVICE_NOT_STARTED = "SERVICE_NOT_STARTED";

    /**
     *
     */
    String SERVICE_NULL = "SERVICE_NULL";

    /**
     *
     */
    String SERVICE_PROCESSING_NOT_AVAILABLE = 
            "SERVICE_PROCESSING_NOT_AVAILABLE";
    /**
     *
     */
    String SERVICE_NULL_CANNOT_GET_WORKMANAGER =
        "SERVICE_NULL_CANNOT_GET_WORKMANAGER";

    /**
     *
     */
    String SERVICE_NULL_NOT_PROCESSED = "SERVICE_NULL_NOT_PROCESSED";

    /**
     *
     */
    String SERVICE_RETREVAL_FAILED_STOP_FAILED =
        "SERVICE_RETREVAL_FAILED_STOP_FAILED";

    /**
     *
     */
    String SERVICE_RETREVAL_FAILED =
        "SERVICE_RETREVAL_FAILED";
    
    /**
     *
     */
    String SERVICE_RETREVAL_FAIL_CANT_STOP = "SERVICE_RETREVAL_FAIL_CANT_STOP";

    /**
     *
     */
    String SERVICE_XML = "SERVICE_XML";

    /**
     *
     */
    String SERVIVE_COULD_NOT_BE_ACTIVATED = "SERVIVE_COULD_NOT_BE_ACTIVATED";

    /**
     *
     */
    String SERVIVE_PROCESSING_NOT_AVAILABLE =
        "SERVIVE_PROCESSING_NOT_AVAILABLE";

    /**
     *
     */
    String SERVIVE_RETREVAL_FAILED = "SERVIVE_RETREVAL_FAILED";

    /**
     *
     */
    String SHUTTINGDOWN_THREAD = "SHUTTINGDOWN_THREAD";

    /**
     *
     */
    String STARTALLSERVICES_START = "STARTALLSERVICES_START";

    /**
     *
     */
    String STARTALLSERVICE_END = "STARTALLSERVICE_END";

    /**
     *
     */
    String START_ALL_SERVICE = "START_ALL_SERVICE";

    /**
     *
     */
    String START_THREAD_POOL = "START_THREAD_POOL";

    /**
     *
     */
    String STATUS_IS = "STATUS_IS";

    /**
     *
     */
    String STYLESHEET_FOUND = "STYLESHEET_FOUND";

    /**
     *
     */
    String STYLESHEET_NOT_FOUND = "STYLESHEET_NOT_FOUND";

    /**
     *
     */
    String SUCCESS = "SUCCESS";

    /**
     *
     */
    String SYSTEM = "SYSTEM";

    /**
     *
     */
    String TASKIDENTIFIER = "TASKIDENTIFIER";

    /**
     *
     */
    String TASKRSLTSTATUS = "TASKRSLTSTATUS";

    /**
     *
     */
    String TEMPLATECOMMAND_CONSTRUCTOR_END = "TEMPLATECOMMAND_CONSTRUCTOR_END";

    /**
     *
     */
    String TEMPLATECOMMAND_CONSTRUCTOR_START =
        "TEMPLATECOMMAND_CONSTRUCTOR_START";

    /**
     *
     */
    String TEMPLATECOMMAND_EXECUTE_END = "TEMPLATECOMMAND_EXECUTE_END";

    /**
     *
     */
    String TEMPLATECOMMAND_EXECUTE_START = "TEMPLATECOMMAND_EXECUTE_START";

    /**
     *
     */
    String TEMPLATECOMMAND_WORKMANAGER_INVOKE =
        "TEMPLATECOMMAND_WORKMANAGER_INVOKE";

    /**
     *
     */
    String TEMPLATEREGISTRY_CONSTRUCTOR_END =
        "TEMPLATEREGISTRY_CONSTRUCTOR_END";

    /**
     *
     */
    String TEMPLATEREGISTRY_CONSTRUCTOR_START =
        "TEMPLATEREGISTRY_CONSTRUCTOR_START";

    /**
     *
     */
    String TEMP_FILES_NOT_DELETED = "TEMP_FILES_NOT_DELETED";

    /**
     *
     */
    String TE_CANNOT_CREATE_XML_MESSAGE = "TE_CANNOT_CREATE_XML_MESSAGE";

    /**
     *
     */
    String TE_ERR_MESSEXC_RCVD_NULL = "TE_ERR_MESSEXC_RCVD_NULL";

    /**
     *
     */
    String TE_NO_ARTIFACT = "TE_NO_ARTIFACT";

    /**
     *
     */
    String TE_SERVICE = "TE_SERVICE";

    /**
     *
     */
    String TE_SHUTDOWN_END = "TE_SHUTDOWN_END";

    /**
     *
     */
    String TE_SHUTDOWN_START = "TE_SHUTDOWN_START";

    /**
     *
     */
    String TE_START_END = "TE_START_END";

    /**
     *
     */
    String TE_STOP_END = "TE_STOP_END";

    /**
     *
     */
    String TE_STOP_START = "TE_STOP_START";

    /**
     *
     */
    String THREAD = "THREAD";

    /**
     *
     */
    String THREAD_RETRIVED_IS_ALIVE = "THREAD_RETRIVED_IS_ALIVE";

    /**
     *
     */
    String TOTAL_SERVICE = "TOTAL_SERVICE";

    /**
     *
     */
    String TRANSFORM = "TRANSFORM";

    /**
     *
     */
    String TRANSFORMATION = "TRANSFORMATION";

    /**
     *
     */
    String TRANSFORMATIONIMPL_CREATION_FAILED =
        "TRANSFORMATIONIMPL_CREATION_FAILED";

    /**
     *
     */
    String TRANSFORMATION_FAILED = "TRANSFORMATION_FAILED";

    /**
     *
     */
    String TRANSFORMATION_FAILED_NODE_NULL = "TRANSFORMATION_FAILED_NODE_NULL";

    /**
     *
     */
    String TRANSFORMATION_SUCCESS = "TRANSFORMATION_SUCCESS";

    /**
     *
     */
    String TRANSFORMATION_SUCCESS_OTHER = "TRANSFORMATION_SUCCESS_OTHER";

    /**
     *
     */
    String UNABLE_DELETE_SERVICES = "UNABLE_DELETE_SERVICES";

    /**
     *
     */
    String UNABLE_TO_DELETE_ASA_DIR = "UNABLE_TO_DELETE_ASA_DIR";

    /**
     *
     */
    String VALID = "VALID : ";

    /**
     *
     */
    String WARNING = "WARNING";

    /**
     *
     */
    String WARNING_STOP = "WARNING_STOP";

    /**
     *
     */
    String WORKERMANAGER_PASSING_REQ_TO_WORKERTHREAD =
        "WORKERMANAGER_PASSING_REQ_TO_WORKERTHREAD";

    /**
     *
     */
    String WORKMANAGER_PASSING_REQ_TO_WORKERTHREAD =
        "WORKMANAGER_PASSING_REQ_TO_WORKERTHREAD";

    /**
     *
     */
    String WRITTING_INPUT_REQUEST_FAILED = "WRITTING_INPUT_REQUEST_FAILED";

    /**
     *
     */
    String WRITTING_REQ_FAIL = "WRITTING_REQ_FAIL";

    /**
     *
     */
    String XML = "XML";

    /**
     *
     */
    String XML_PROLOG = "XML_PROLOG";

    /**
     *
     */
    String XSD = "XSD";

    /**
     *
     */
    String XSLT = "XSLT";

    /**
     *
     */
    String XSLTFILE = "XSLTFILE";

    /**
     *
     */
    String XSLT_FILE_FOUND_IS = "XSLT_FILE_FOUND_IS";

    /**
     *
     */
    String XSLT_FILE_SET_IS = "XSLT_FILE_SET_IS";
    
    /**
     *
     */
    String TE_LOAD_DD_FAILED = "TE_LOAD_DD_FAILED";

    /**
     *
     */
    String TE_NO_DD = "TE_NO_DD";
    
    /**
     *
     */
    String TE_INVALID_DD = "TE_INVALID_DD";
    
    /**
     *
     */
    String TE_SU_DD_PARSE_FAILURE = "TE_SU_DD_PARSE_FAILURE";
    
    /**
     *
     */
    String TE_NO_ARTIFACT_EXTENSION_ELEMENT = "TE_NO_ARTIFACT_EXTENSION_ELEMENT";
    
    /**
     *
     */
    String TE_INVALID_ARTIFACT_EXTENSION_ELEMENT_ATTRIBUTES = "TE_INVALID_ARTIFACT_EXTENSION_ELEMENT_ATTRIBUTES";
    
    /**
     *
     */
    String TE_INCONSISTENT_CONSUMER_DATA = "TE_INCONSISTENT_CONSUMER_DATA";
    
    /**
     *
     */
    String TE_INCONSISTENT_PROVIDER_DATA = "TE_INCONSISTENT_PROVIDER_DATA";
    
    /**
     *
     */
    String TE_INCONSISTENT_DATA = "TE_INCONSISTENT_DATA";
            
}
