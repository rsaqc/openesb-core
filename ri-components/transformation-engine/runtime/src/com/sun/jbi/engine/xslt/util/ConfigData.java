/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

/**
 * Configuration information, that stores the tag names.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConfigData
{
    /**
     * DOCUMENT ME!
     */
    public static final String LOG_FILE = "sequencing.log";

    /**
     * DOCUMENT ME!
     */
    public static final String SCHEMA_FILE_NAME = "transformation.xsd";

    /**
     * DOCUMENT ME!
     */
    public static final String APPLICATIONS_FOLDER = "applications";

    /**
     * DOCUMENT ME!
     */
    public static final String NAME = "service-name";

    /**
     * DOCUMENT ME!
     */
    public static final String ENDPOINT = "endpoint-name";

    /**
     * DOCUMENT ME!
     */
    public static final String NONACTIVE = "nonactive";

    /**
     * DOCUMENT ME!
     */
    public static final String ACTIVE = "active";

    /**
     * DOCUMENT ME!
     */
    public static final String START = "start";

    /**
     * DOCUMENT ME!
     */
    public static final String STOP = "stop";

    /**
     * DOCUMENT ME!
     */
    public static final String SERVICE = "service";

    /**
     * DOCUMENT ME!
     */
    public static final String TARGET_NAMESPACE = "targetNamespace";

    /**
     * DOCUMENT ME!
     */
    public static final String SERVICEID = "service-id";

    /**
     * DOCUMENT ME!
     */
    public static final String SERVICE_DESCRIPTION = "service-description";

    /**
     * DOCUMENT ME!
     */
    public static final String CREATE_CACHE = "create-cache";

    /**
     * DOCUMENT ME!
     */
    public static final String MINIMUM_NUMBER_OF_THREADS = "min-no-of-threads";

    /**
     *    
     */
    public static final String MAXIMUM_NUMBER_OF_THREADS = "max-no-of-threads";

    /**
     * Used for SERVICE only.
     */
    public static final String STYLE_SHEET_NAME = "style-sheet-name";

    /**
     * Used for SERVICE only.
     */
    public static final String STYLE_SHEET_LOCATION = "style-sheet-location";

    /**
     * DOCUMENT ME!
     */
    public static final String SOURCE_TYPE = "source-type";

    /**
     * DOCUMENT ME!
     */
    public static final String CONFIG_FILE_NAME = "service.xml";

    /**
     * DOCUMENT ME!
     */
    public static final String WSDL_FILE_NAME = "service.wsdl";

    /**
     * DOCUMENT ME!
     */
    public static final String SAX_SOURCE = "saxsource";

    /**
     * DOCUMENT ME!
     */
    public static final String DOM_SOURCE = "domsource";

    /**
     * DOCUMENT ME!
     */
    public static final String FIRST_ROW_COL_HEADERS = "first-row-col-headers";

    /**
     * DOCUMENT ME!
     */
    public static final String COLUMN_HEADERS = "column-headers";

    /**
     * DOCUMENT ME!
     */
    public static final String COLUMN_HEADER = "column-header";

    /**
     * DOCUMENT ME!
     */
    public static final String COLUMN_SEPARATOR = "column-separator";

    /**
     * DOCUMENT ME!
     */
    public static final String SERVICE_NAMESPACE = "namespace-uri";

    /**
     * DOCUMENT ME!
     */
    public static final String SERVICE_NAME = "service-name";

    /**
     * DOCUMENT ME!
     */
    public static final String SERVICENAME_LOCALPART = "local-part";

    /**
     * DOCUMENT ME!
     */
    public static final String SERVICE_ENGINE_TYPE =
        "http://java.sun.com/xml/ns/jbi/binding/service+engine";

    /**
     * DOCUMENT ME!
     */
    public static final String TE_NAMESPACE = "http://sun.com/jbi/wsdl/te10";

    /**
     * Direction.
     */
    public static final String DIRECTION = "direction";

    /**
     * In Only MEP.
     */
    public static final String IN_ONLY =
        "http://www.w3.org/ns/wsdl/in-only";

    /**
     * In Out MEP.
     */
    public static final String IN_OUT = "http://www.w3.org/ns/wsdl/in-out";

    /**
     * In Optional Out MEP.
     */
    public static final String IN_OPTIONAL_OUT =
        "http://www.w3.org/ns/wsdl/in-opt-out";

    /**
     * Robust In Only MEP.
     */
    public static final String ROBUST_IN_ONLY =
        "http://www.w3.org/ns/wsdl/robust-in-only";
    
    /**
     * The name of the jbi schema file for jbi.xml in an SU.
     */
    public static final String JBI_SCHEMA_FILE = "jbi.xsd";
    
    
    /**
     * Service Unit descriptor file
     */
    public static final String SU_DESCRIPTOR = "jbi.xml";
    
    /**
     * Service Unit descriptor file
     */
    public static final String DEPLOYMENT_TYPE = "deployType";
}
