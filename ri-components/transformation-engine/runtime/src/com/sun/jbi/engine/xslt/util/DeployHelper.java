/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.engine.xslt.TEResources;
import com.sun.jbi.engine.xslt.TransformationEngineContext;
import com.sun.jbi.engine.xslt.util.ServiceListValidator;

import java.io.File;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import com.sun.jbi.common.management.ComponentMessageHolder;
import javax.jbi.management.DeploymentException;
import com.sun.jbi.common.management.ManagementMessageBuilder;
import com.sun.jbi.common.Util;

import org.w3c.dom.Document;
import javax.xml.namespace.QName;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;        
/**
 * This class is a Helper clas to do deployment.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeployHelper
    extends UtilBase
    implements TEResources
{
    /**
     * Binding Context object.
     */
    private ComponentContext mContext;

    /**
     * Logger object.
     */
    private Logger mLogger;

    /**
     *    
     */
    private ManagementMessageBuilder mBuildManagementMessage;

    /**
     *    
     */
    private ServiceManager mServiceManager;

    /**
     * Application Sub assembly of deployment.
     */
    private String mServiceUnitID;

    /**
     * Path of the sub assembly.
     */
    private String mServiceUnitPath;

    /**
     * Status.
     */
    private String mStatus = "";

    /**
     * Name of the Stylesheet to be used.
     */
    private String mStyleSheet;

    /**
     * Wsdl file name.
     */
    private String mWsdlFile;

    /**
     * Name of the Service config XMl file.
     */
    private String mXmlConfigFile;

    /**
     * Helper class for i18n.
     */
    private StringTranslator mTranslator;
    
    /**
     * Service Unit Descriptor file
     */
    private String mSUDescriptor;
    
    /**
     * Service Unit Descriptor schema file
     */
    private String mSUDSchema;
    
    /**
     * Service Unit Descriptor reader
     */
    private SUDescriptorReader mSUDescriptorReader;

    /**
     * Array of ConfigBean holding the services defined in artifact
     */
    private ConfigBean[] mServicelist;
    
    /**
     * Deployment File Name
     */
    private String mDeployFileName;
    
    /**
     * Deployment Type (specified in SU jbi.xml "wsdl_20", "wsdl_11", "xml"
     */
    private String mDeploymentType;
    
    /**
     * Creates a new DeployHelper object.
     *
     * @param suID application sub assembly Id.
     * @param suPath RESOLVER
     * @param context binding context.
     */
    public DeployHelper(
        String suID,
        String suPath,
        ComponentContext context)
    {
        String xsltFile;
        setValid(true);
        mServiceUnitID = suID;
        mContext = context;
        mServiceUnitPath = suPath;

        mSUDescriptor = suPath + File.separatorChar + "META-INF" +
                             File.separatorChar + ConfigData.SU_DESCRIPTOR;
        mSUDSchema = 
            TransformationEngineContext.getInstance().getContext().getInstallRoot() 
            + File.separatorChar + ConfigData.JBI_SCHEMA_FILE;
        
        mLogger = TransformationEngineContext.getInstance().getLogger("");

        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());

        /*mXmlConfigFile =
            mServiceUnitPath + File.separatorChar + ConfigData.CONFIG_FILE_NAME;

        mWsdlFile =
            mServiceUnitPath + File.separatorChar + ConfigData.WSDL_FILE_NAME;*/

        
        xsltFile = getStyleSheetName();

        if (xsltFile != null)
        {
            mStyleSheet = mServiceUnitPath + File.separatorChar + xsltFile;
            mLogger.finer("Stylesheet used: " + mStyleSheet);
        }
        else
        {
            mLogger.finer("No Stylesheet found in : " + mServiceUnitPath
                + " Fatal Error.");
            setValid(false);
            setError("No Stylesheet found in : " + mServiceUnitPath
                + " Fatal Error.");
        }

        mServiceManager = ServiceManager.getInstance();

        try
        {
            mBuildManagementMessage = Util.createManagementMessageBuilder();
                
            mLogger.finer(mTranslator.getString(TEResources.BLD_MGMT_MSG_OBJ)
                + mBuildManagementMessage);
        }
        catch (Exception e)
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.CANNOT_CREATE_MANAGEMENT_BUILDER));

            //throw e;
            setValid(false);
        }
    }

    /**
     * Gets the status object.
     *
     * @return status string.
     */
    public String getStatusMessage()
    {
        return mStatus;
    }

    /**
     * Method which does the actual deployment.
     */
    public void doDeploy() throws DeploymentException
    {
        String retMsg = null;

        // Read the SU jbi.xml and read the XSLT specific service
        // extensions described under "TEServiceExtns" element
        // type can be "xml" or "wsdl_20" or "wsdl_11"
        // If the SU jbi.xml is not proper or missing, fail the deployment

        if ((mSUDescriptor != null) && 
            (new File(mSUDescriptor)).exists())
        {
            doSUDescriptorParsing();
        }

        if(!isValid())
        {
            mLogger.severe("Error parsing Service Unit Descriptor.");
            
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "TESE_L224", mServiceUnitID, 
                    mTranslator.getString("TE_SU_DD_PARSE_FAILURE") + ": "
                    + getError(),
                    null) );
        }
        
        
        // doSUDescriptorParsing will validate the SU DD (jbi.xml) and 
        // load the extensions into memory.
        // Get all TE specific service extensions from SUDescriptorReader
        
        if(mSUDescriptorReader == null)
        {
            mLogger.severe("SUDecriptorReader Null.");
        }
        mDeploymentType = mSUDescriptorReader.getExtensionAttribute("type");
        if ( mDeploymentType.equals("xml") )
        {
            mLogger.finer("Doing XML deployment.");
            retMsg = doXMLDeploy();

            if (retMsg != null)
            {
                mLogger.finer("doXMLDeploy returned: " + retMsg);
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "TESE_L233", mServiceUnitID, retMsg, null));
            }
        }
        else if (mDeploymentType.equals("wsdl_20") )
        {
            mDeployFileName = mSUDescriptorReader.getExtensionAttribute("file-name");
            // throw a deployment exception if any of these extension elements are null.
            if ( ( mDeployFileName == null) || ( mDeploymentType == null ) )
            {
                throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "TESE_L253", mServiceUnitID, 
                    mTranslator.getString("TE_INVALID_ARTIFACT_EXTENSION_ELEMENT_ATTRIBUTES"),
                    null));
            }

            // Check if the deployFile exists for this type
            mDeployFileName = mServiceUnitPath + File.separatorChar + mDeployFileName;

            if (!new File(mDeployFileName).exists())
            {
                throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "TESE_L253", mServiceUnitID, 
                    mTranslator.getString("TE_NO_ARTIFACT"),
                    null));
            }
            
            mLogger.finer("Doing WSDL 2.0 deployment.");
            retMsg = doWSDL20Deploy();

            if (retMsg != null)
            {
                mLogger.finer("doWSDL20Deploy returned: " + retMsg);
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "TESE_L218", mServiceUnitID, retMsg, null));
            }
        }
        else if ( mDeploymentType.equals("wsdl_11") )
        {
            mDeployFileName = mSUDescriptorReader.getExtensionAttribute("file-name");
            // throw a deployment exception if any of these extension elements are null.
            if ( ( mDeployFileName == null) || ( mDeploymentType == null ) )
            {
                throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "TESE_L309", mServiceUnitID, 
                    mTranslator.getString("TE_INVALID_ARTIFACT_EXTENSION_ELEMENT_ATTRIBUTES"),
                    null));
            }

            // Check if the deployFile exists for this type
            mDeployFileName = mServiceUnitPath + File.separatorChar + mDeployFileName;

            if (!new File(mDeployFileName).exists())
            {
                throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "TESE_L321", mServiceUnitID, 
                    mTranslator.getString("TE_NO_ARTIFACT"),
                    null));
            }
            
            mLogger.info("Doing WSDL 1.1 deployment.");
            retMsg = doWSDL11Deploy();
            if (retMsg != null)
            {
                mLogger.finer("doWSDL11Deploy returned: " + retMsg);
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "TESE_L334", mServiceUnitID, retMsg, null));
            }
        }        
        else
        {
            setError(mTranslator.getString(TEResources.TE_NO_ARTIFACT));
            setValid(false);
            throw new DeploymentException(createExceptionMessage(
                mContext.getComponentName(), "deploy", "FAILED",
                "TESE_L309", mServiceUnitID, 
                mTranslator.getString("TE_NO_ARTIFACT"),
                null));
        }
    }

    /**
     * Sets the status message.
     *
     * @param status status message.
     */
    private void setStatusMessage(String status)
    {
        mStatus = status;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    private String getStyleSheetName()
    {
        String [] xsltFileList;
        String xsltFileName = null;

        try
        {
            mLogger.finer("Getting Listing for directory: "
                + mServiceUnitPath);
            xsltFileList = (new File(mServiceUnitPath)).list();

            for (int i = 0; i < xsltFileList.length; i++)
            {
                mLogger.finer("File List Entry: " + xsltFileList[i]);

                if (xsltFileList[i].endsWith("xsl")
                        || xsltFileList[i].endsWith("xslt"))
                {
                    xsltFileName = xsltFileList[i];

                    break;
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

        return xsltFileName;
    }

    /**
     * helper method to create XML exception string.
     *
     * @param compid Component id.
     * @param oper operation like deploy or undeploy
     * @param status success r failure
     * @param loctoken some failure string token like SEQ_300001
     * @param locparam parameters for error message.
     * @param locmessage error message.
     * @param exObj stack trace of exception.
     *
     * @return XML string.
     */
    private String createExceptionMessage(
        String compid,
        String oper,
        String status,
        String loctoken,
        String locparam,
        String locmessage,
        Throwable exObj)
    {
        String retMsg = null;

        String [] locParams = new String[1];
        locParams[0] = locparam;

        ComponentMessageHolder msgMap =
            new ComponentMessageHolder("EXCEPTION_MSG");

        msgMap.setComponentName(compid);
        msgMap.setTaskName(oper);
        msgMap.setTaskResult(status);
        msgMap.setLocToken(1, loctoken);
        msgMap.setLocParam(1, locParams);
        msgMap.setLocMessage(1, locmessage);
        msgMap.setExceptionObject(exObj);

        try
        {
            retMsg = mBuildManagementMessage.buildComponentMessage(msgMap);
        }
        catch (Exception e)
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.CANNOT_CREATE_MGMT_XML_MSG) + e.getMessage());
        }

        return retMsg;
    }

    private String doWSDL11Deploy()
    {
        String retMsg = null;
        
        try
        {
            WSDL11FileReader wsdl11Reader = new WSDL11FileReader(mDeployFileName);
            if(!wsdl11Reader.isValid())
            {
                retMsg = createExceptionMessage(mContext.getComponentName(),
                        "deploy", "FAILED", "", "", wsdl11Reader.getError(), null);
                return retMsg;
            }

            ConfigBean[] cbList = mSUDescriptorReader.loadServices();
            if(cbList!=null)
            {
                QName sname = new QName ((String)cbList[0].getValue(ConfigData.SERVICE_NAMESPACE).elementAt(0), 
                                (String)cbList[0].getValue(ConfigData.SERVICENAME_LOCALPART).elementAt(0) );
                if(wsdl11Reader.isServiceDefined(sname))
                {
                    mServicelist = wsdl11Reader.extractEndpoints(sname);
                    if(mServicelist == null)
                    {
                        mLogger.severe(wsdl11Reader.getError());
                        retMsg =
                            createExceptionMessage(mContext.getComponentName(),
                                "deploy", "FAILED", "", "",
                                "Check the Deployment wsdl file."
                                + " No service specified.", null);

                        return retMsg;
                    }

                    //Read SU jbi.xml and populate mServiceList with Service extensions
                    mSUDescriptorReader.populateServiceInformation(mServicelist[0]);
                    
                    // Set the deployment type in the ConfigBean 
                    mServicelist[0].setValue(ConfigData.DEPLOYMENT_TYPE, mDeploymentType);

                    printServiceList();

                    if ((retMsg = nonxml(mServicelist[0])) != null)
                    {
                        mLogger.finer("nonxml returned: " + retMsg);

                        return retMsg;
                    }

                    for (int i = 0; i < mServicelist.length; i++)
                    {
                        mServicelist[i].setValue(mTranslator.getString(
                                TEResources.XSLTFILE), mStyleSheet);
                    }

                    mServiceManager.createServices(mServiceUnitID, mServicelist);
                }
                else
                {
                    retMsg = createExceptionMessage(mContext.getComponentName(),
                        "deploy", "FAILED", "", "", mTranslator.getString(TE_INCONSISTENT_PROVIDER_DATA), null);
                    return retMsg;
                }
            }
            else
            {
                retMsg = createExceptionMessage(mContext.getComponentName(),
                        "deploy", "FAILED", "", "", mSUDescriptorReader.getError(), null);
                return retMsg;
            }
        }
        catch(Exception e)
        {
            mLogger.severe(mTranslator.getString(
                        TEResources.TE_INCONSISTENT_PROVIDER_DATA) + mServiceUnitID);

            retMsg =
                createExceptionMessage(mContext.getComponentName(),
                    "deploy", "FAILED", "", "",
                    mTranslator.getString(
                        TEResources.TE_INCONSISTENT_PROVIDER_DATA)
                    + mServiceUnitID, e);

            return retMsg;
        }
        
        return retMsg;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    private String doWSDL20Deploy()
    {
        String retMsg = null;

        WSDLFileValidator validator = new WSDLFileValidator(mServiceUnitPath, mDeployFileName);
        validator.validate();

        if (!validator.isValid())
        {
            Exception e = validator.getException();

            if (e != null)
            {
                retMsg =
                    createExceptionMessage(mContext.getComponentName(),
                        "deploy", "FAILED", "", "", e.getMessage(), e);

                return retMsg;

                //e.printStackTrace();
            }
        }

        if (validator.isValid())
        {
            // This WSDL 2.0 document will not have XSLT specific extension
            // attributes like service-id etc. They will be mentioned in
            // the SU jbi.xml (private to TE)
            WSDLFileReader reader = new WSDLFileReader();
            reader.init(validator.getDocument());

            if (!reader.isValid())
            {
                Exception e = reader.getException();

                if (e != null)
                {
                    retMsg =
                        createExceptionMessage(mContext.getComponentName(),
                            "deploy", "FAILED", "", "", e.getMessage(), e);

                    return retMsg;

                    //e.printStackTrace();
                }
            }

            try
            {
                mServicelist = reader.getServices();

                if (mServicelist == null)
                {
                    mLogger.severe(reader.getError());
                    retMsg =
                        createExceptionMessage(mContext.getComponentName(),
                            "deploy", "FAILED", "", "",
                            "Check the Deployment config file (service.wsdl)."
                            + " No service specified.", null);

                    return retMsg;
                }

                //Read SU jbi.xml and populate mServiceList with Service extensions
                mSUDescriptorReader.populateServiceInformation(mServicelist[0]);
                
                // Set the deployment type in the ConfigBean 
                mServicelist[0].setValue(ConfigData.DEPLOYMENT_TYPE, mDeploymentType);
                
                printServiceList();

                if ((retMsg = nonxml(mServicelist[0])) != null)
                {
                    mLogger.finer("nonxml returned: " + retMsg);

                    return retMsg;
                }

                for (int i = 0; i < mServicelist.length; i++)
                {
                    mServicelist[i].setValue(mTranslator.getString(
                            TEResources.XSLTFILE), mStyleSheet);
                }
                
                
                mServiceManager.createServices(mServiceUnitID, mServicelist);
            }
            catch (Exception de)
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.CAN_NOT_START_DEPLOYMENT) + mServiceUnitID);

                retMsg =
                    createExceptionMessage(mContext.getComponentName(),
                        "deploy", "FAILED", "", "",
                        mTranslator.getString(
                            TEResources.CAN_NOT_START_DEPLOYMENT)
                        + mServiceUnitID, de);

                return retMsg;
            }
        }

        //end if (validator.isValid()
        return retMsg;
    }

    /**
     * Perform deployment of configuration XML file.
     *
     * @return NOT YET DOCUMENTED
     */
    private String doXMLDeploy()
    {
        String retMsg = null;

        try
        {
            mServicelist = mSUDescriptorReader.loadServices();

            if (mServicelist == null)
            {
                mLogger.severe(mSUDescriptorReader.getError());
                retMsg =
                    createExceptionMessage(mContext.getComponentName(),
                        "deploy", "FAILED", "", "",
                        "Check the Deployment artifact file."
                        + " No service specified.", null);

                return retMsg;
            }

            if ((retMsg = nonxml(mServicelist[0])) != null)
            {
                return retMsg;
            }

            for (int i = 0; i < mServicelist.length; i++)
            {
                mServicelist[i].setValue(mTranslator.getString(
                        TEResources.XSLTFILE), mStyleSheet);
            }

            mSUDescriptorReader.populateServiceInformation(mServicelist[0]);
            // Set the deployment type in the ConfigBean 
            mServicelist[0].setValue(ConfigData.DEPLOYMENT_TYPE, mDeploymentType);
                    
            mServiceManager.createServices(mServiceUnitID, mServicelist);
        }
        catch (Exception de)
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.CAN_NOT_START_DEPLOYMENT) + mServiceUnitID);
            retMsg =
                createExceptionMessage(mContext.getComponentName(), "deploy",
                    "FAILED", "", "",
                    mTranslator.getString(TEResources.CAN_NOT_START_DEPLOYMENT)
                    + mServiceUnitID, null);

            return retMsg;
        }

        return retMsg;
    }

    /**
     * DOCUMENT ME!
     *
     * @param bean NOT YET DOCUMENTED
     *
     * @return NOT YET DOCUMENTED
     */
    private String nonxml(ConfigBean bean)
    {
        String retMsg = null;

        //Check for non-xml case
        if ((bean.getValue(ConfigData.COLUMN_HEADERS) != null)
                && (bean.getValue(ConfigData.COLUMN_HEADER) == null))
        {
            retMsg =
                "COLUMN_HEADERS specified but COLUMN_HEADER elements are missing.";

            return retMsg;
        }

        if ((bean.getValue(ConfigData.COLUMN_HEADERS) == null)
                && (bean.getValue(ConfigData.COLUMN_HEADER) != null))
        {
            retMsg =
                "COLUMN_HEADERS Not Found But COLUMN_HEADER elements "
                + "are specified. Error ... Check.";

            return retMsg;
        }

        if ((bean.getValue(ConfigData.COLUMN_HEADER) != null)
                && (bean.getValue(ConfigData.FIRST_ROW_COL_HEADERS) != null))
        {
            retMsg =
                "Both COLUMN_HEADER and FIRST_ROW_COL_HEADERS elements "
                + "are specified. Only one should be present.";

            return retMsg;
        }

        mLogger.finer("nonxml returning: " + retMsg);

        return retMsg;
    }

    /**
     * DOCUMENT ME!
     *
     */
    private void printServiceList()
    {
        try
        {
            for (int i = 0; i < mServicelist.length; i++)
            {
                mLogger.finer(ConfigData.SERVICE_NAMESPACE + " = "
                    + mServicelist[i].getValue(ConfigData.SERVICE_NAMESPACE)
                                     .elementAt(0));
                mLogger.finer(ConfigData.ENDPOINT + " = "
                    + mServicelist[i].getValue(ConfigData.ENDPOINT).elementAt(0));
                mLogger.finer(ConfigData.SERVICEID + " = "
                    + mServicelist[i].getValue(ConfigData.SERVICEID).elementAt(0));
                mLogger.finer(ConfigData.CREATE_CACHE + " = "
                    + mServicelist[i].getValue(ConfigData.CREATE_CACHE)
                                     .elementAt(0));

                if (mServicelist[i].getValue(ConfigData.COLUMN_SEPARATOR)
                                       .elementAt(0) != null)
                {
                    mLogger.finer(ConfigData.COLUMN_SEPARATOR + " = "
                        + mServicelist[i].getValue(ConfigData.COLUMN_SEPARATOR)
                                         .elementAt(0));
                }

                if (mServicelist[i].getValue(ConfigData.COLUMN_HEADER) != null)
                {
                    for (int k = 0;
                            k < (mServicelist[i].getValue(
                                ConfigData.COLUMN_HEADER)).size(); k++)
                    {
                        mLogger.finer(ConfigData.COLUMN_HEADER + " = "
                            + mServicelist[i].getValue(ConfigData.COLUMN_HEADER)
                                             .elementAt(k));
                    }
                }
            }

            //end for
        }
        catch (Exception e)
        {
            ;
        }
    }
    
   /**
    * Parses the deployment descriptor jbi.xml.
    */
    private void doSUDescriptorParsing()
    {
        mLogger.finer("mSUDSchema: "+mSUDSchema);
        mLogger.finer("mSUDescriptor: "+mSUDescriptor);
        
        ServiceListValidator validator =
            new ServiceListValidator(mSUDSchema, mSUDescriptor);
        validator.validate();
        //getDocument will return null if validate is not called before.
        Document d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLogger.severe(mTranslator.getString(TE_INVALID_DD,
                    mSUDescriptorReader));
            setError(mTranslator.getString(TE_INVALID_DD,
                    mSUDescriptorReader) + " " + validator.getError());
            setValid(false);
        }   
        
        /**
         * Match service names in artifacts file ( endpoints.xml)
         * and the DD. Every endpoint in the the DD file should
         * be present in the artifacts file.
         * We dont care if the artifacts file has more endpoints. Those
         * will be ignored.
         * The condition is the DD XML file should be a sub-set
         * of artifacts file. The spec does not govern this logic so it is upto
         * the component to decide how to veryify consistence between 
         * DD and its artifacts.
         */

        /**
         * A valid question that arises here is why are we having 2 XML
         * files to decorate an endpoint.
         * The artifacts XML file is the older form which was used when the 
         * spec did not accomodate any DD for an SU.
         * So that is still around. 
         * Any component specific decoration for the endpoints can be done 
         * through the DD jbi.xml also. And that would be the correct way to
         * do it.
         * We dont do it that way because there are other modules that might          
         * depend on the endpoints.xml file, so to maintain their functionality 
         * the older endpoints.xml file is still used to decorate endpoints.
         */ 
        
        
        mSUDescriptorReader = new SUDescriptorReader();
        mLogger.finer("Created mSUDescriptorReader: ");

        mSUDescriptorReader.init(d);
        if (!mSUDescriptorReader.isValid())
        {
            mLogger.severe(mTranslator.getString(TE_INVALID_DD,
                    ConfigData.SU_DESCRIPTOR));
            setError(mTranslator.getString(TE_INVALID_DD,
                     ConfigData.SU_DESCRIPTOR) + " " + 
                        mSUDescriptorReader.getError());
            setValid(false);
        }
        
        mSUDescriptorReader.check();
        if (!mSUDescriptorReader.isValid())
        {
            mLogger.severe(mTranslator.getString(TE_INVALID_DD,
                    ConfigData.SU_DESCRIPTOR));
            setError(mTranslator.getString(TE_INVALID_DD,
                     ConfigData.SU_DESCRIPTOR) + " " + 
                        mSUDescriptorReader.getError());
            setValid(false);
        }
                
        /*if (mSUDescriptorReader.getConsumerCount() != 0)
        {
            setError(mTranslator.getString(TE_INCONSISTENT_CONSUMER_DATA));
        }       
        
        if (mSUDescriptorReader.getProviderCount() != 1)
        {
            setError(mTranslator.getString(TE_INCONSISTENT_PROVIDER_DATA));
        } */  
        
        /*for(int ii=0; ii<mServicelist.length; ii++)
        {
            QName sername = new QName ((String)mServicelist[ii].getValue(ConfigData.SERVICE_NAMESPACE).elementAt(0), 
                            (String)mServicelist[ii].getValue(ConfigData.SERVICENAME_LOCALPART).elementAt(0) );
            
            String epname = (String)mServicelist[ii].getValue(ConfigData.ENDPOINT).elementAt(0);
            if (!mSUDescriptorReader.isPresent(sername, epname))             
            {
                setError(mTranslator.getString(TE_INCONSISTENT_DATA));
                setValid(false);
                break;
            }
        }*/
     }
}
