/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TransformationImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import com.sun.jbi.engine.xslt.framework.WorkManager;
import com.sun.jbi.engine.xslt.util.StringTranslator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.EmptyStackException;
import java.util.Properties;
import java.util.Stack;
import java.util.logging.Logger;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;


/**
 * This class provides interfaces to process the messages received by the
 * QuoteService implementations. It also contains interfaces to create the
 * outbound response and fault message.
 *
 * @author Sun Microsystems, Inc.
 */
public final class TransformationImpl
    implements TransformationProcessor, TEResources
{
    /**
     *    
     */
    private static StringTranslator sTranslator =
        new StringTranslator("com.sun.jbi.engine.xslt", null);

    /**
     *    
     */
    private static Stack sPool = new Stack();

    /**
     *    
     */
    private static TemplateRegistry sTemplateRegistry =
        TemplateRegistry.getRegistry();

    /**
     *    
     */
    private static Logger sLogger =
        TransformationEngineContext.getInstance().getLogger("");

    /**
     *    
     */
    private Properties mCache = null;

    /**
     *    
     */
    private String mTemplateKey = null;

    /**
     *    
     */
    private WorkManager mWorkManager = null;

    /**
     *    
     */
    private int mCount = 0;

    /**
     * Creates a new instance of TransformationImpl.
     *
     * @param count number of Factory instances
     */
    public TransformationImpl(int count)
    {
        init(count);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static synchronized TransformerFactory getTransformerFactory()
    {
        TransformerFactory tFactory = null;

        try
        {
            tFactory = (TransformerFactory) sPool.pop();
        }
        catch (EmptyStackException ex)
        {
            sLogger.warning(sTranslator.getString(
                    TEResources.NO_TRANSFORMATIONIMPL));
        }

        return tFactory;
    }

    /**
     * DOCUMENT ME!
     *
     * @param tfactory NOT YET DOCUMENTED
     */
    public static synchronized void putTransformerFactory(
        TransformerFactory tfactory)
    {
        Object obj = sPool.push(tfactory);

        if (obj == null)
        {
            sLogger.warning(sTranslator.getString(
                    TEResources.PUSHING_TRANSFORMATIONIMPL_FAILED));
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param template NOT YET DOCUMENTED
     */
    public void setTemplateName(String template)
    {
        mTemplateKey = template;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getTemplateName()
    {
        return mTemplateKey;
    }

    /**
     * Generates a fault message.
     *
     * @param document - a DOM object which will contain the fault message.
     * @param input - the stock symbol name.
     * @param message - error message.
     */
    public void createFaultMessage(
        Document document,
        Node input,
        String message)
    {
        sLogger.info(sTranslator.getString(
                TEResources.CREATEFAULTMESSAGE_START));

        Element transformElement =
            document.createElement(sTranslator.getString(TEResources.TRANSFORM));
        Element faultElement =
            document.createElement(sTranslator.getString(TEResources.FAULT));
        Element messageElement =
            document.createElement(sTranslator.getString(TEResources.MESSAGE));
        messageElement.appendChild(document.createTextNode(message));
        faultElement.appendChild(messageElement);
        transformElement.appendChild(faultElement);
        document.appendChild(transformElement);
    }

    /**
     * Generates a response message.
     *
     * @param document - a DOM object which will contain the response message.
     * @param response - the stock symbol name.
     *
     * @throws Exception exception
     */
    public void createResponseMesage(
        Document document,
        Node response)
        throws Exception
    {
        Element transformElement =
            document.createElement(sTranslator.getString(TEResources.TRANSFORM));
        Element responseElement =
            document.createElement(sTranslator.getString(TEResources.RESPONSE));
        responseElement.appendChild(response);
        transformElement.appendChild(responseElement);
        document.appendChild(transformElement);
    }

    /**
     * Does transformation on the input DOM object using the stylesheet.
     *
     * @param input - a DOM object containing the message request.
     * @param xsltfile - XSLT Tranformation stylesheet
     * @param result - Result of the XSLT transformation
     *
     * @return the symbol if the message conforms to the format; otherwise
     *         null.
     *
     * @throws Exception exception
     */
    public boolean doTransform(
        Source input,
        String xsltfile,
        Result result)
        throws Exception
    {
        boolean status = false;
        TransformerFactory tFactory = null;
        Transformer transformer = null;

        //Check template registry for the xsltfile
        Templates tpl = (Templates) sTemplateRegistry.get(mTemplateKey);
        int mSomeNumber = 19;

        try
        {
            if (tpl != null)
            {
                transformer = tpl.newTransformer();
            }
            else
            {
                try
                {
                    int attempt = 0;

                    while ((tFactory == null) && (attempt < mSomeNumber))
                    {
                        tFactory = getTransformerFactory();

                        if (tFactory == null)
                        {
                            sLogger.severe(sTranslator.getString(
                                    TEResources.CACHED_TRANSFORMERFACTORY_NOT_PRESENT));
                            attempt++;

                            // wait for some time
                        }
                    }

                    // Temproary fix  
                    if (tFactory == null)
                    {
                        tFactory = TransformerFactory.newInstance();
                    }

                    if (xsltfile != null)
                    {
                        StreamSource stylesheet = new StreamSource(xsltfile);
                        transformer = tFactory.newTransformer(stylesheet);
                        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
                            "yes");
                    }
                }
                finally
                {
                    //Returning the tFactory to pool
                    if (sPool.size() < mCount)
                    {
                        putTransformerFactory(tFactory);
                    }
                }

                TemplateCommand command =
                    new TemplateCommand(mTemplateKey, new StreamSource(xsltfile));

                while (!mWorkManager.processCommand(command))
                {
                    //block till template is done
                }
            }

            transformer.transform(input, result);
            status = true;
        }
        catch (Exception ex)
        {
            sLogger.severe(sTranslator.getString(
                    TEResources.TRANSFORMATION_FAILED));
            throw ex;
        }

        return status;
    }

    /**
     * Does transformation on the input DOM object using the stylesheet.
     *
     * @param input - a DOM object containing the message request.
     * @param result - Result of Transformation
     *
     * @return the symbol if the message conforms to the format; otherwise
     *         null.
     *
     * @throws Exception exception
     */
    public boolean transform(
        Source input,
        Result result)
        throws Exception
    {
        return doTransform(input, "/temp.xsl", result);
    }

    /**
     * Initializes the instance.
     *
     * @param num - number of Factory instances
     */
    private void init(int num)
    {
        mWorkManager =
            WorkManager.getWorkManager(sTranslator.getString(TEResources.XSLT));
        mCount = num;
       
        /* Commenting out the following as JAXP will be able to detect
           the provider for the underlying platform 

        String key = "javax.xml.transform.TransformerFactory";

        //String value = "org.apache.xalan.xsltc.trax.SmartTransformerFactoryImpl";
        //String value = "org.apache.xalan.processor.TransformerFactoryImpl";
        String value = "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl";
        mCache = System.getProperties();
        mCache.put(key, value);
        System.setProperties(mCache); */

        for (int i = 0; i < mCount; i++)
        {
            TransformerFactory tFactory = TransformerFactory.newInstance();
            putTransformerFactory(tFactory);
        }
    }
}
