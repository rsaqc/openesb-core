/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CSVFeederEngineLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

import java.io.File;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Logger;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.MessageExchangeFactory;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * This class implements ComponentLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Nilesh
 */
public class CSVFeederEngineLifeCycle implements ComponentLifeCycle, Component
{
    /**
     * In Only MEP.
     */
    public static final String IN_ONLY =
        "http://www.w3.org/2004/08/wsdl/in-only";

    /**
     * In Out MEP.
     */
    public static final String IN_OUT = "http://www.w3.org/2004/08/wsdl/in-out";

    /**
     * In Optional Out MEP.
     */
    public static final String IN_OPTIONAL_OUT =
        "http://www.w3.org/2004/08/wsdl/in-opt-out";

    /**
     * Robust In Only MEP.
     */
    public static final String ROBUST_IN_ONLY =
        "http://www.w3.org/2004/08/wsdl/robust-in-only";

    /**
     *    
     */
    private MessageExchangeFactory mFactory;
    
    /**
     *    
     */
    private static final String INOUT_SERVICE_NAME = "transformService2";
    
    /**
     *    
     */
    private static final String CSV_INOUT_SERVICE_NAME = "csvTransformService";
    
    /**
     *    
     */
    private static final String INOUT_SERVICE_NAMESPACE = "http://www.sun.com/ns/jbi/binding/engines/transformation/deploy/service-config";
    
        /**
     *    
     */
    private static final String CSV_INOUT_SERVICE_NAMESPACE = "http://www.sun.com/ns/jbi/engines/transformation/deploy/service-config";

    /**
     *    
     */
    private static final String ENDPOINT = "JBI";
    
    /**
     *    
     */
    public Thread mRecv;

    /**
     *    
     */
    private ServiceEndpoint mEndpoint;

    /**
     *    
     */
    private DeliveryChannel mChannel;

    /**
     * Environment conext passed down from framework to this stockquote engine.
     */
    private ComponentContext mContext = null;

    /**
     * Refernce to logger.
     */
    private Logger mLog = null;

    /**
     *    
     */
    private MessageExchange mReq;

    /**
     *    
     */
    private ServiceEndpoint mInonlyReference;

    /**
     *    
     */
    private ServiceEndpoint mInoutReference;

    /**
     *    
     */
    private ServiceEndpoint mOutboundReference;

    /**
     *    
     */
    private ServiceEndpoint mRobustinonlyReference;

    /**
     *    
     */
    private boolean mRunFlag = true;

    /**
     * Get the JMX ObjectName for the DeploymentMBean for this BPE. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the DeploymentMBean or null if
     *         there is no DeploymentMBean.
     */
    public javax.management.ObjectName getDeploymentMBeanName()
    {
        return null;
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Initialize the Data Feeder Engine. This performs initialization required
     *
     * @param jbiContext the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the stockquote engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext jbiContext)
        throws javax.jbi.JBIException
    {
        mLog = Logger.getLogger("DataFeederEngine");
        mLog.info("Data Feeder Engine INIT invoked");

        if (jbiContext == null)
        {
            throw (new javax.jbi.JBIException("Context is null",
                new NullPointerException()));
        }

        /* Assign values */
        mContext = jbiContext;
        Payload.mXMLFile =
            mContext.getInstallRoot() + File.separatorChar + "te_input.xml";
        
        Payload.mCSVFileName = 
            mContext.getInstallRoot() + File.separatorChar + "csvinput.txt";
        
        mLog.info(" INIT METHOD FINISHED ");
    }

    /**
     * Shutdown the SE. This performs cleanup before the SE is terminated.
     * Once this has been called, init() must be called before the SE can be
     * started again with a call to start().
     *
     * @throws javax.jbi.JBIException if the SE is unable to shut down.
     */
    public void shutDown() throws javax.jbi.JBIException
    {
        mLog.info("Data Feeder Engine Shutdown-Start");
        mLog.info("Data Feeder Engine Shutdown-End");
    }

    /**
     */
    public void start() throws javax.jbi.JBIException
    {
        mLog.info(" Data Feeder Engine INVOKED ");

        try
        {
            mChannel = mContext.getDeliveryChannel();
            mFactory = mChannel.createExchangeFactory();
        }
        catch (MessagingException me)
        {
            me.printStackTrace();
        }

        if (mChannel == null)
        {
            mLog.severe("Cannot get Engine Channel from context ");

            return;
        }

        /*mRecv = new Thread(new Receiver());
        mRecv.start();*/

        testXMLTrasnformation();
        
        testCSVTrasnformation();
        
        mLog.info(" START FINISHED  ");
    }

    /**
     */
    public void stop() throws javax.jbi.JBIException
    {
        mLog.info("STOP INVOKED ");
        mRunFlag = false;

        try
        {
            if (mChannel != null)
            {
                mChannel.close();
            }
            mRecv.interrupt();
            mRecv.join();
        }
        catch (Exception e)
        {
            ;
        }

        mLog.info(" STOP FINISHED ");
    }


    /**
     *   
     *
     */
    public void testCSVTrasnformation()
    {
        mLog.info("Data Feeder engine sending CSV Input to TE");

        NormalizedMessage aMsg;
        InOut inout;

        try
        {
            // create the exchange
            inout = mFactory.createInOutExchange();
            aMsg = inout.createMessage();

            // set the stuff we know
            inout.setOperation(new QName(CSV_INOUT_SERVICE_NAMESPACE,"transform"));

            //ServiceEndpoint [] mEndpoint;
            ServiceEndpoint  mEndpoint;

            // lookup the endpoint reference and set on exchange
            mEndpoint = mContext.getEndpointsForService(
                    new QName(CSV_INOUT_SERVICE_NAMESPACE, CSV_INOUT_SERVICE_NAME))[0];

            // set the payload
            Payload.setCSVPayload(aMsg);
            inout.setService(new QName(CSV_INOUT_SERVICE_NAMESPACE, CSV_INOUT_SERVICE_NAME));
            inout.setEndpoint(mEndpoint);
            // set the message on the exchange
            inout.setInMessage(aMsg);

            // send the exchange
            mChannel.send(inout);

            // receive the response
            inout = (InOut) mChannel.accept();

            if (inout.getStatus() == ExchangeStatus.ERROR)
            {
                mLog.info("CSV to XML Trasnformation request Failed");

                return;
            }
            else if (inout.getStatus() == ExchangeStatus.ACTIVE)
            {
                if (inout.getOutMessage() != null)
                {
                    mLog.info("XML Transformation SUCCESS");
                }
                else
                {
                    mLog.info("XML Transformation FAILED");
                }
            }

            inout.setStatus(ExchangeStatus.DONE);
            mChannel.send(inout);
        }
        catch (Exception e)
        {
            mLog.severe("XML Transformation threw-Exception");
            e.printStackTrace();
        }
    }
    
    /**
     *   
     *
     */
    public void testXMLTrasnformation()
    {
        mLog.info("Data Feeder engine sending XML Input to TE");

        NormalizedMessage aMsg;
        InOut inout;

        try
        {
            // create the exchange
            inout = mFactory.createInOutExchange();
            aMsg = inout.createMessage();

            // set the stuff we know
            //outIn.setService(mOutboundReference);
            inout.setOperation(new QName(INOUT_SERVICE_NAMESPACE,"transform"));

            //ServiceEndpoint [] mEndpoint;
            ServiceEndpoint  mEndpoint;

            // lookup the endpoint reference and set on exchange
            mEndpoint = mContext.getEndpointsForService(
                    new QName(INOUT_SERVICE_NAMESPACE, INOUT_SERVICE_NAME))[0];

            // set the payload
            Payload.setXMLPayload(aMsg);
            inout.setService(new QName(INOUT_SERVICE_NAMESPACE, INOUT_SERVICE_NAME));
            inout.setEndpoint(mEndpoint);
            // set the message on the exchange
            inout.setInMessage(aMsg);

            // send the exchange
            mChannel.send(inout);

            // receive the response
            inout = (InOut) mChannel.accept();

            if (inout.getStatus() == ExchangeStatus.ERROR)
            {
                mLog.info("XML Trasnformation request Failed");

                return;
            }
            else if (inout.getStatus() == ExchangeStatus.ACTIVE)
            {
                if (inout.getOutMessage() != null)
                {
                    mLog.info("XML Transformation SUCCESS");
                }
                else
                {
                    mLog.info("XML Transformation FAILED");
                }
            }

            inout.setStatus(ExchangeStatus.DONE);
            mChannel.send(inout);
        }
        catch (Exception e)
        {
            mLog.severe("XML Transformation threw-Exception");
            e.printStackTrace();
        }
    }
    
    /**
     *   
     *
     */
    public class Receiver implements Runnable
    {
        /**
         *
         */
        public void run()
        {
            mLog.info("Data Feeder Engine receiver started");

            try
            {
                while (mRunFlag)
                {
                    mReq = mChannel.accept();
                    
                    mLog.info("Got message in feeder engine");
                    if(mReq != null)
                    {
                        mLog.info("Message ID: "+mReq.getExchangeId());
                        mLog.info("Message Pattern: "+mReq.getPattern());
                        mLog.info("Status: "+mReq.getStatus());
                        mLog.info("Role: "+mReq.getRole());

                        /*if (mReq.getPattern().toString().equals(IN_OUT))
                        {
                            doTestGoodInOut();
                        }
                        else if (mReq.getPattern().toString().equals(IN_ONLY))
                        {
                            doTestGoodInOnly();
                        }
                        else if (mReq.getPattern().toString().equals(ROBUST_IN_ONLY))
                        {
                            doTestGoodRobustInOnly();
                        }*/
                    } //end if
                }
            }
            catch (Exception e)
            {
                mLog.severe("Exception at receiver");
                mLog.info("Data Feeder Engine receiver ended");
            }
        }
    }
    
    
    
    public javax.jbi.component.ComponentLifeCycle getLifeCycle() 
    {
        return this;
    }
    
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager() 
    {
        return null;
    }
    
    public org.w3c.dom.Document getServiceDescription(
            javax.jbi.servicedesc.ServiceEndpoint ServiceEndpoint) 
    {
        return null;
    }    
    
     /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired.
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component). 
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
}
