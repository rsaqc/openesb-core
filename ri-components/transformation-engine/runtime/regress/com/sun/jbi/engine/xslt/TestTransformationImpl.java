/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestTransformationImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import junit.framework.*;
import com.sun.jbi.engine.xslt.framework.*;
import java.io.File;
import java.util.logging.Logger;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Result;
import javax.xml.transform.TransformerFactory;


/**
 * DOCUMENT ME!
 *
 * @author root
 */
public class TestTransformationImpl extends TestCase
{

    /**
     * DOCUMENT ME!
     */
    private WorkManager mWorkManager;
    /**
     * DOCUMENT ME!
     */
    private TransformationImpl mTransformationImpl;
    /**
     * DOCUMENT ME!
     */
    private String mXslt;
    /**
     * DOCUMENT ME!
     */
    private String mBadXslt;
    /**
     * DOCUMENT ME!
     */
    private File mInput;
    /**
     * DOCUMENT ME!
     */
    private File mBadInput;
    /**
     * DOCUMENT ME!
     */
    private File mOutput;

    /**
     * Creates a new TestTransformationImpl object.
     *
     * @param testName DOCUMENT ME!
     */
    public TestTransformationImpl(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestTransformationImpl.class);
        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        /*
	mTransformationImpl = new TransformationImpl(6);
        String srcroot = System.getProperty("junit.srcroot");

        try
        {
            mXslt = srcroot + "/engine/xslt/regress/data/test.xslt";
            mBadXslt = srcroot + "/engine/xslt/regress/data/bad.xslt";
            mInput= new File(srcroot + "/engine/xslt/regress/data/input.xml");
            mBadInput= new File(srcroot + "/engine/xslt/regress/data/badinput.xml");
            mOutput= new File (srcroot + "/engine/xslt/regress/data/output.xml");
            mWorkManager = WorkManager.getWorkManager("xslt");
        }
        catch (Exception jbiException)
        {
            jbiException.printStackTrace();
            fail("setUp failed");
        }
        try
        {
            mWorkManager.start();         
        }
        catch (Exception ex)
        {
            //ex.printStackTrace();
        } 
	*/
    }

    /**
     * Test of getBean method, of class com.sun.jbi.engine.xslt.util.TransformationImpl.
     */
    public void testGetTransformerFactory()
    {
        /*
	System.out.println("testGetTransformerFactory");
        TransformerFactory factory = TransformerFactory.newInstance();
        TransformationImpl.putTransformerFactory(factory); 
        TransformerFactory factory2=TransformationImpl.getTransformerFactory();
        assertNotNull("Retrieved factory is not Null", factory2);
        assertEquals("putTransformerfactory failed", factory, factory2);
	*/
    }

    /**
     * Test of getEndpoint method, of class com.sun.jbi.engine.xslt.util.TransformationImpl.
     */
    public void testTransform()
    {
        /*
	System.out.println("testTransform");
        Source input  = (Source)new StreamSource(mInput);
        Result output = (Result)new StreamResult(mOutput);
        boolean result = false; 
        try
        {
            result = mTransformationImpl.doTransform(input, mXslt, output);
        }
        catch(Exception ex)
        {
           ex.printStackTrace();
        } 
        assertTrue( "Transformation failed", result );
    	*/
    }

    /**
     * Test of bad transformation for class com.sun.jbi.engine.xslt.util.TransformationImpl.
     */
    public void testBadInput()
    {
        /*
	System.out.println("testBadInput");
        Source input  = (Source)new StreamSource(mBadInput);
        Result output = (Result)new StreamResult(mOutput);
        boolean result = true;
        try
        { 
            result = mTransformationImpl.doTransform(input, mXslt ,output );
            System.out.println("Test bad input" + result);
        }
        catch(Exception ex)
        {
           return; 
        }
        assertFalse("Expected Transformation Failure" , result);
        */
     }

    /**
     * Test of bad transformation for class com.sun.jbi.engine.xslt.util.TransformationImpl.
     */
    public void testBadXSLT()
    {
        /*
	System.out.println("testBadXSLT");
        Source input  = (Source)new StreamSource(mBadInput);
        Result output = (Result)new StreamResult(mOutput);
        boolean result = true;
        try
        { 
            result = mTransformationImpl.doTransform(input, mBadXslt ,output );
            System.out.println("Test bad xslt" + result);
        }
        catch(Exception ex)
        {
           return; 
        }
        assertFalse("Expected Transformation Failure" , result);
        */
    }

    /**
     * Test of init method, of class com.sun.jbi.engine.xslt.util.TransformationImpl.
     */
    public void testInit()
    {
        System.out.println("testInit");
    }

}
