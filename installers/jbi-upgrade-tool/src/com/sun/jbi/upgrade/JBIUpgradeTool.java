/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIUpgradeTool.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.upgrade;

/**
 * JBIUpgradeTool is used to upgrade JBI artifacts in a domain when the 
 * underlying domain is upgraded. This class contains the contract 
 * between the Sun Java System Application Installer, Appserver
 * upgrade tool and JBI upgrade tool. 
 */

public interface JBIUpgradeTool 
{

    /**
     * This method is the contract between SJSAS installer and 
     * JBI upgrade tool. When the user perfoms a binary upgrade SJSAS 
     * installer will update the JBI binaries in AS_INSTALL/jbi.
     * After that, for each of the domains in the default domain dir, 
     * the installer will invoke this method to upgrade 
     * the JBI system components in that domain.
     * 
     * @param appserverInstallRoot appserver installation root
     * @param domainRoot the root dir for the domain
     * 
     * @throws JBIUpgradeException if the upgrade tool encountered an error
     */  
    public void upgradeJBISystemComponentsInDomain(
        String appserverInstallRoot, 
        String domainRoot)
    throws JBIUpgradeException;
    
    

    /**
     * This method is the contract between appserver upgrade tool and
     * JBI upgrade tool. 
     * When the user performs an in-place upgrade or a side-by-side 
     * upgrade upgrade tool will invoke the JBIUpgrade tool to
     * migrate the existing JBI artifacts (components and applications)
     * to the newly created domain. 
     * 
     * @param sourceDomainRoot the root dir for the source domain
     * @param destinationDomainRoot the root dir for the destination domain
     * @throws JBIUpgradeException if JBIUpgradetool encountered an error condition
     */
    public void migrateJBIArtifacts(
            String sourceDomainRoot, 
            String destinationDomainRoot)
    throws JBIUpgradeException;            

}
