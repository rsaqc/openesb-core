#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)core00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#core00001 - core installer and JBI Runtime Test

. $SRCROOT/antbld/regress/common_defs.ksh
rm -rf $SRCROOT/installers/core/bld/test
mkdir -p $SRCROOT/installers/core/bld/test/lib/addons/jbi

cp $SRCROOT/installers/core/bld/jbi-core-installer.jar $SRCROOT/installers/core/bld/test 2>&1
cp $AS8BASE/lib/addons/jbi/appserv-jbise.jar        $SRCROOT/installers/core/bld/test/lib/addons/jbi 2>&1

cp -r $AS8BASE/lib/ant             $SRCROOT/installers/core/bld/test/lib 2>&1
cp -r $AS8BASE/config $AS8BASE/bin $SRCROOT/installers/core/bld/test 2>&1

export AS_INSTALL
AS_INSTALL=$JV_SVC_BLD/test
as_install=$SVC_BLD/test

cd $as_install
java -jar jbi-core-installer.jar $SRCROOT/installers/core/bld/test install 2>&1
grep 'BUILD FAILED' jbi/jbi-core-installer.log
if [ $? -eq 0 ]; then
    bldmsg -error "java -jar jbi-core-installer.jar $SRCROOT/installers/core/bld/test install FAILED in `pwd`"
    bldmsg "######## LOG FOLLOWS:"
    cat jbi/jbi-core-installer.log
    bldmsg "######## EOF"
fi

ls jbi
cd jbi 2>&1
ls lib
ls components
ls shared-libraries
