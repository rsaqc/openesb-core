<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)pom.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project>
    <parent>
        <artifactId>jbi-common</artifactId>
        <groupId>open-esb</groupId>
        <version>1.0</version>
        <relativePath>build-common/jbi-common</relativePath>
    </parent>
    <modelVersion>4.0.0</modelVersion>
    <groupId>open-esb</groupId>
    <artifactId>core-top</artifactId>
    <packaging>pom</packaging>
    <name>core-top</name>
    <version>${openesb.currentVersion}</version>
    <description>Top-level project for OpenESB core</description>
    <build>
        <defaultGoal>install</defaultGoal>
        <plugins>
            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generatejbiver</id>
                        <phase>initialize</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <echo file="${project.build.directory}/jbi.ver" append="false">
FULL_PRODUCT_NAME="Open Enterprise Service Bus"
SHORT_PRODUCT_NAME="Open_ESB"
FULL_VERSION="${openesb.currentVersion}"
MAJOR_VERSION="${parsedVersion.majorVersion}"
MINOR_VERSION="${parsedVersion.minorVersion}.${parsedVersion.incrementalVersion}-${parsedVersion.qualifier}"
BUILD_NUMBER="${timestamp}"
CODELINE="${env.CODELINE}"
TIMESTAMP="${env.UCVSUPDATETIMEDOT}"
                                </echo>
                            </target>
                        </configuration>
                    </execution>
                    <execution>
                        <id>installjbigf</id>
                        <phase>install</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <ant antfile="${project.parent.relativePath}/../m2.ant" dir="${basedir}" target="openesb:do_make_kenai_jbiroot">
                                    <property name="maven.repo.local" value="${maven.repo.local}" />
                                    <property name="openesb.currentVersion" value="${openesb.currentVersion}" />
                                </ant>
                            </target>
                        </configuration>
                    </execution>
                    <execution>
                        <id>junitreport</id>
                        <phase>test</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <ant antfile="${project.parent.relativePath}/../inc/junit_report.ant" dir="${basedir}">
                                    <property name="SRCROOT" value="${SRCROOT}"/>
                                    <property name="maven.repo.local" value="${maven.repo.local}" />
                                    <property name="maven.test.skip" value="${maven.test.skip}"/>
                                </ant>
                            </target>
                        </configuration>
                    </execution>
                    <execution>
                        <id>installjbise</id>
                        <phase>install</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <ant antfile="${project.parent.relativePath}/../m2.ant" dir="${basedir}" target="openesb:do_make_jbise_jbiroot">
                                    <property name="maven.repo.local" value="${maven.repo.local}" />
                                    <property name="openesb.currentVersion" value="${openesb.currentVersion}" />
                                </ant>
                            </target>
                        </configuration>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>org.apache.ant</groupId>
                        <artifactId>ant-nodeps</artifactId>
                        <version>1.8.1</version>
                    </dependency>
                </dependencies>
            </plugin>
            
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.4</version>
                <executions>
                    <execution>
                        <id>dist-assembly</id>
                        <phase>install</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <!--<finalName>openesb-${project.version}</finalName>-->
                            <descriptors>
                                <descriptor>src/assemble/distribution.xml</descriptor>
                            </descriptors>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    <profiles>
        <profile>
            <id>default-profile</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <modules>
                <!-- common project: -->
                <module>build-common/jbi-common</module>
                <module>build-common/package-common</module>
                <module>build-common</module>
                <!-- compile configuration -->
                <module>platform-config</module>
                <!-- jbi: -->
                <module>jbi</module>
                <!-- runtime: -->
                <module>runtime</module>
                <!-- components: util, -->
                <module>esb-util</module>
                <!-- common, file binding, sequencing engine, xslt engine -->
                <module>ri-components</module>
                <!-- JMS binding and proxy binding: -->
                <module>esb-components</module>
                <!-- client projects: -->
                <module>ri-clients</module>
                <!-- module>esb-clients</module -->
                <!-- packaging projects: -->
                <module>ri-packages</module>
                <module>esb-packages</module>
                <module>installers</module>
            </modules>
        </profile>

        <profile>
            <id>junit-profile</id>
            <activation>
                <property>
                    <name>maven.test.skip</name>
                    <value>false</value>
                </property>
            </activation>
            <modules>
                <!-- list only modules that have junit tests: -->
                <module>runtime</module>
                <module>esb-util</module>
                <module>ri-components</module>
                <module>esb-components</module>
                <module>ri-clients</module>
                <module>installers</module>
            </modules>
        </profile>

        <!-- run-system-tests profile: -->
        <profile>
            <id>run-system-tests</id>
            <activation>
                <property> 
                    <name>jregress</name>
                </property>
            </activation>
            <build>
                <defaultGoal>integration-test</defaultGoal>
            </build>
            <modules>
                <!-- list only modules that have jregress tests: -->
                <module>runtime</module>
                <module>ri-components</module>
                <module>esb-components</module>
                <module>ri-clients</module>
                <module>installers</module>
            </modules>
        </profile>

        <profile>
            <activation>
                <property>
                    <name>aggregateReports</name>
                </property>
            </activation>
            <reporting>
                <outputDirectory>target/site</outputDirectory>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-jxr-plugin</artifactId>
                        <configuration>
                            <!-- aggregate all sub-project source into one report.  -->
                            <aggregate>true</aggregate>
                            <bottom>Copyright (c) 2004-2007 Sun Microsystems Inc.  All Rights Reserved.</bottom>
                            <windowTitle>Open-ESB ${project.version} Reference</windowTitle>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <configuration>
                            <!-- aggregate all sub-project javadoc into one report.  -->
                            <aggregate>true</aggregate>
                            <minmemory>128m</minmemory>
                            <maxmemory>512m</maxmemory>
                        </configuration>
                    </plugin>
                </plugins>
            </reporting>
        </profile>
    </profiles>
    <dependencies>
        <!--dependency>     
            <groupId>ant-contrib</groupId>  
            <artifactId>ant-contrib</artifactId>
            <version>1.0b3</version>
            <exclusions>
                <exclusion>
                    <groupId>ant</groupId>
                    <artifactId>ant</artifactId>
                </exclusion>
            </exclusions>  
        </dependency-->
        <dependency>
            <groupId>org.apache.ant</groupId>      
            <artifactId>ant-nodeps</artifactId>
            <version>1.8.1</version>
        </dependency>
    </dependencies>
</project>
