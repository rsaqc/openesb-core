/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Browser.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package jbidemo;

import java.net.URL;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class Browser
{
    private String mBrowserPath;
    /**
     * Creates a new instance of Browser
     */
    public Browser()
    {
    }
    
    public void setBrowserPath(String browserPath)
    {
        if ( browserPath != null && browserPath.trim().length() > 0 )
        {
            this.mBrowserPath = browserPath.trim();
        }
        else
        {
            this.mBrowserPath = "mozilla";
        }
    }
    
    public String getBrowserPath() {
        return this.mBrowserPath;
    }
    
    public void launchBrowserUnix(URL target)
    {
        try
        {
            Runtime.getRuntime().exec(getBrowserPath() + " " +
                target.toString());
        }
        catch ( Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void launchBrowserWin32(URL target)
    {
        try
        {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " +
                target.toString());
        }
        catch ( Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void launchBrowser(URL target)
    {
        System.out.println("OS " + System.getProperty("os.name"));
        String osName = System.getProperty("os.name");
        
        if (osName.startsWith("Windows"))
        {
            launchBrowserWin32(target);
        }
        else
        {
            launchBrowserUnix(target);
        }
    }
    
    public final static void main(String[] args)
    {
        String urlStr = "";
        String browserPath = "mozilla";
        
        if (args.length <= 0 )
        {
            System.out.println("No Url is specified");
        }
        else
        {
            urlStr = args[0];
            if ( urlStr == null )
            {
                urlStr = "";
            }
            
            if ( args.length > 1 )
            {
                browserPath = args[1];
            }
        }
        try
        {
            Browser browser = new Browser();
            browser.setBrowserPath(browserPath);
            URL url = new URL(urlStr);
            browser.launchBrowser(url);
        }
        catch (Exception ex)
        {
            // ex.printStackTrace();
            System.out.println("Can not Open Browser" + ex.getMessage());
        }
    }
}
