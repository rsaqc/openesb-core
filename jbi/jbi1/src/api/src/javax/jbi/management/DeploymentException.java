/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package javax.jbi.management;

/**
 * DeploymentException is an exception thrown by the Deployment Service and
 * the Service Unit Manager.
 *
 * @author JSR208 Expert Group
 */

public class DeploymentException extends javax.jbi.JBIException
{
    /**
     * Creates a new instance of DeploymentException with an exception detail
     * message.
     *
     * @param message the detail message for this exception.
     */
    public DeploymentException(String message)
    {
        super(message);
    }

    /**
     * Creates a new instance of DeploymentException with and exception detail
     * message and a cause.
     *
     * @param message the detail message for this exception.
     * @param cause <code>Error</code> or <code>Exception</code> which
     *        represents the cause of the problem (<code>null</code> if none,
     *        or if the cause is not known).
     */
    public DeploymentException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Creates a new instance of DeploymentException with the specified cause.
     *
     * @param cause <code>Error</code> or <code>Exception</code> which
     *        represents the cause of the problem (<code>null</code> if none,
     *        or if the cause is not known).
     */
    public DeploymentException(Throwable cause)
    {
        super(cause);
    }
}
