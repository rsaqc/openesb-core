/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLifeCycleMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package javax.jbi.management;

import javax.jbi.management.LifeCycleMBean;

import javax.management.ObjectName;

/**
 * ComponentLifeCycleMBean defines the standard life cycle controls for
 * JBI Installable Components.
 * <ul>
 *   <li>Initialize the component, preparing it to receive service requests.</li>
 *   <li>Start the component, allowing it to initiate service requests.</li>
 *   <li>Stop the component from initiating any more service requests.</li>
 *   <li>Shut down the component, returning it to the uninitialized state
 *       where it cannot receive service requests.</li>
 *   <li>Query the JMX object name of the extension MBean for the component.</li>
 * </ul>
 *
 * @author JSR208 Expert Group
 */
public interface ComponentLifeCycleMBean extends LifeCycleMBean
{
    /**
     * Get the JMX ObjectName for the life cycle extension MBean for this 
     * component. If there is none, return <code>null</code>.
     * Note that this MBean may serve as a container for multiple MBeans,
     * as required by the component implementation.
     * 
     * @return ObjectName the JMX object name of the additional MBean 
     *         or <code>null</code> if there is no additional MBean.
     * @exception javax.jbi.JBIException if there is a failure getting component
     *            information for the component to which this life cycle 
     *            applies.
     */
    ObjectName getExtensionMBeanName() throws javax.jbi.JBIException;
}
