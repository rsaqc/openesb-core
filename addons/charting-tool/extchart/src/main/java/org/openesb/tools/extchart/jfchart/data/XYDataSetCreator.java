/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XYDataSetCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.jfchart.data;


import org.openesb.tools.extchart.exception.ChartException;
import org.openesb.tools.extchart.property.ChartConstants;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.List;

import java.util.logging.Logger;
import org.jfree.data.general.Dataset;
import org.jfree.data.xy.CategoryTableXYDataset;

/**
 *
 * @author rdwivedi
 */
public class XYDataSetCreator extends DataSetCreator{
     private static Logger mLogger =Logger.getLogger(XYDataSetCreator.class.getName());
     private CategoryTableXYDataset dataset = null;
    
    /** Creates a new instance of XYDataSetCreator */
    public XYDataSetCreator() {
       
    }
     public Dataset getDefaultDataSet() {
        return dataset;
    }
    public void processResultSet( ResultSet resultSet) throws ChartException {
         dataset = new CategoryTableXYDataset();
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            if (numberOfColumns != 3) {
                throw new ChartException(
                    "There must be 3 columns.");
            }
            while (resultSet.next()) {
                String series = resultSet.getString(1);
                double x=0,y = 0;
                boolean dataOK = true;
                    int columnType = metaData.getColumnType(2);
                    switch (columnType) {
                        case Types.NUMERIC:
                        case Types.REAL:
                        case Types.INTEGER:
                        case Types.DOUBLE:
                        case Types.FLOAT:
                        case Types.DECIMAL:
                        case Types.BIGINT:
                        case Types.SMALLINT:
                            x = resultSet.getDouble(2);
                            break;

                        case Types.NULL:
                            break;
                        default:
                            System.err.println("Unknown data");
                            dataOK = false;
                            break;
                    }
                    
                    columnType = metaData.getColumnType(3);
                    switch (columnType) {
                        case Types.NUMERIC:
                        case Types.REAL:
                        case Types.INTEGER:
                        case Types.DOUBLE:
                        case Types.FLOAT:
                        case Types.DECIMAL:
                        case Types.BIGINT:
                        case Types.SMALLINT:
                            y = resultSet.getDouble(3);
                            break;

                        case Types.NULL:
                            break;
                        default:
                            System.err.println("Unknown data");
                            dataOK = false;
                            break;
                    }
                
            if(dataOK) {
                dataset.add(x,y,series);
            }
            }
        } catch(SQLException e) {
            throw new ChartException(e);
        }
        
    }
    
    public  String getQuery() throws ChartException {
        return null;
    }
    
}
