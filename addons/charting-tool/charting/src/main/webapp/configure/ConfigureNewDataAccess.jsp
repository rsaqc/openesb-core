<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:ui="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <jsp:directive.page import="org.openesb.tools.charting.persist.ChartApplicationBeansContainer"/>
    <jsp:scriptlet>
                    
                    String id = request.getParameter("_daUID"); 
                    boolean idNull = false; 
                        if(id == null || id.length()&lt;1) { 
                        idNull = true;
                    } else {
                    
                    ChartApplicationBeansContainer container = (ChartApplicationBeansContainer)session.getAttribute("ChartApplicationBeansContainer");
                    container.setCurrentDBABeanOnEditorID(id); 
                    }
                    if(!idNull) {
                </jsp:scriptlet>
    <f:view>
        <ui:page binding="#{configure$ConfigureNewDataAccess.page1}" id="page1">
            <ui:html binding="#{configure$ConfigureNewDataAccess.html1}" id="html1">
                <ui:head binding="#{configure$ConfigureNewDataAccess.head1}" id="head1">
                    <ui:link binding="#{configure$ConfigureNewDataAccess.link1}" id="link1" url="/resources/stylesheet.css"/>
                </ui:head>
                <ui:body binding="#{configure$ConfigureNewDataAccess.body1}" id="body1" style="-rave-layout: grid">
                    <ui:form binding="#{configure$ConfigureNewDataAccess.form1}" id="form1">
                        <ui:label binding="#{configure$ConfigureNewDataAccess.label1}" id="label1" style="left: 120px; top: 192px; position: absolute" text="DB JNDI "/>
                        <ui:textField binding="#{configure$ConfigureNewDataAccess.textField1}" id="textField1" style="left: 240px; top: 192px; position: absolute; width: 240px"/>
                        <ui:label binding="#{configure$ConfigureNewDataAccess.label2}" id="label2" style="left: 120px; top: 240px; position: absolute" text="Data set Type"/>
                        <ui:dropDown binding="#{configure$ConfigureNewDataAccess.dropDown1}" id="dropDown1"
                            items="#{configure$ConfigureNewDataAccess.options.options}" style="left: 240px; top: 240px; position: absolute; width: 240px"/>
                        <ui:label binding="#{configure$ConfigureNewDataAccess.label3}" id="label3"
                            style="left: 120px; top: 288px; position: absolute; width: 336px" text="SQL Query"/>
                        <ui:textArea binding="#{configure$ConfigureNewDataAccess.textArea1}" id="textArea1" style="height: 216px; left: 120px; top: 312px; position: absolute; width: 504px"/>
                        <ui:button action="#{configure$ConfigureNewDataAccess.button1_action}" binding="#{configure$ConfigureNewDataAccess.button1}"
                            id="button1" style="left: 119px; top: 552px; position: absolute; width: 96px" text="Save"/>
                        <ui:hiddenField binding="#{configure$ConfigureNewDataAccess.hiddenField1}" id="hiddenField1" />
                    </ui:form>
                </ui:body>
            </ui:html>
        </ui:page>
    </f:view>
    <jsp:scriptlet> }  </jsp:scriptlet>
    
</jsp:root>
