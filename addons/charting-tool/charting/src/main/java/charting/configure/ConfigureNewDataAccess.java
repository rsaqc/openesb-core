/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigureNewDataAccess.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package charting.configure;

import org.openesb.tools.charting.persist.ChartApplicationBeansContainer;
import org.openesb.tools.charting.persist.DataBean;
import org.openesb.tools.extchart.jfchart.data.SampleDemoDataAccess;
import org.openesb.tools.extchart.property.JFChartConstants;
import com.sun.rave.web.ui.appbase.AbstractPageBean;
import com.sun.web.ui.component.Body;
import com.sun.web.ui.component.Button;
import com.sun.web.ui.component.DropDown;
import com.sun.web.ui.component.Form;
import com.sun.web.ui.component.Head;
import com.sun.web.ui.component.HiddenField;
import com.sun.web.ui.component.Html;
import com.sun.web.ui.component.Label;
import com.sun.web.ui.component.Link;
import com.sun.web.ui.component.Page;
import com.sun.web.ui.component.TextArea;
import com.sun.web.ui.component.TextField;
import com.sun.web.ui.model.Option;
import com.sun.web.ui.model.SingleSelectOptionsList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;
import javax.faces.FacesException;

/**
 * <p>Page bean that corresponds to a similarly named JSP page.  This
 * class contains component definitions (and initialization code) for
 * all components that you have defined on this page, as well as
 * lifecycle methods and event handlers where you may add behavior
 * to respond to incoming events.</p>
 */
public class ConfigureNewDataAccess extends AbstractPageBean {
    private static Logger mLogger = Logger.getLogger("/configure/ConfigureNewDataAccess.jsp");
    // <editor-fold defaultstate="collapsed" desc="Managed Component Definition">
    private int __placeholder;

    /**
     * <p>Automatically managed component initialization.  <strong>WARNING:</strong>
     * This method is automatically generated, so any user-specified code inserted
     * here is subject to being replaced.</p>
     */
    private void _init() throws Exception {
    }

    private Page page1 = new Page();
    
    public Page getPage1() {
        return page1;
    }
    
    public void setPage1(Page p) {
        this.page1 = p;
    }
    
    private Html html1 = new Html();
    
    public Html getHtml1() {
        return html1;
    }
    
    public void setHtml1(Html h) {
        this.html1 = h;
    }
    
    private Head head1 = new Head();
    
    public Head getHead1() {
        return head1;
    }
    
    public void setHead1(Head h) {
        this.head1 = h;
    }
    
    private Link link1 = new Link();
    
    public Link getLink1() {
        return link1;
    }
    
    public void setLink1(Link l) {
        this.link1 = l;
    }
    
    private Body body1 = new Body();
    
    public Body getBody1() {
        return body1;
    }
    
    public void setBody1(Body b) {
        this.body1 = b;
    }
    
    private Form form1 = new Form();
    
    public Form getForm1() {
        return form1;
    }
    
    public void setForm1(Form f) {
        this.form1 = f;
    }
    private Label label1 = new Label();

    public Label getLabel1() {
        return label1;
    }

    public void setLabel1(Label l) {
        this.label1 = l;
    }
    private TextField textField1 = new TextField();

    public TextField getTextField1() {
        return textField1;
    }

    public void setTextField1(TextField tf) {
        this.textField1 = tf;
    }
    private Label label2 = new Label();

    public Label getLabel2() {
        return label2;
    }

    public void setLabel2(Label l) {
        this.label2 = l;
    }
    private DropDown dropDown1 = new DropDown();

    public DropDown getDropDown1() {
        return dropDown1;
    }

    public void setDropDown1(DropDown dd) {
        this.dropDown1 = dd;
    }
    private SingleSelectOptionsList dropDown1DefaultOptions = new SingleSelectOptionsList();

    public SingleSelectOptionsList getDropDown1DefaultOptions() {
        return dropDown1DefaultOptions;
    }

    public void setDropDown1DefaultOptions(SingleSelectOptionsList ssol) {
        this.dropDown1DefaultOptions = ssol;
    }
    private Label label3 = new Label();

    public Label getLabel3() {
        return label3;
    }

    public void setLabel3(Label l) {
        this.label3 = l;
    }
    private TextArea textArea1 = new TextArea();

    public TextArea getTextArea1() {
        return textArea1;
    }

    public void setTextArea1(TextArea ta) {
        this.textArea1 = ta;
    }
    private Button button1 = new Button();

    public Button getButton1() {
        return button1;
    }

    public void setButton1(Button b) {
        this.button1 = b;
    }
    private HiddenField hiddenField1 = new HiddenField();

    public HiddenField getHiddenField1() {
        return hiddenField1;
    }

    public void setHiddenField1(HiddenField hf) {
        this.hiddenField1 = hf;
    }

    // </editor-fold>

    /**
     * <p>Construct a new Page bean instance.</p>
     */
    public ConfigureNewDataAccess() {
    }

    /**
     * <p>Callback method that is called whenever a page is navigated to,
     * either directly via a URL, or indirectly via page navigation.
     * Customize this method to acquire resources that will be needed
     * for event handlers and lifecycle methods, whether or not this
     * page is performing post back processing.</p>
     * 
     * <p>Note that, if the current request is a postback, the property
     * values of the components do <strong>not</strong> represent any
     * values submitted with this request.  Instead, they represent the
     * property values that were saved for this view when it was rendered.</p>
     */
    public void init() {
        // Perform initializations inherited from our superclass
        super.init();
        // Perform application initialization that must complete
        // *before* managed components are initialized
        // TODO - add your own initialiation code here
       /* mLogger.info("The request map is " + getRequestMap());
        Map m =  getRequestMap();
        Iterator iter = m.keySet().iterator();
        while (iter.hasNext()) {
            String k = (String)iter.next();
            mLogger.info("Key = " + k + "value= "+ m.get(k)) ;
        }
        */
        String id = getChartApplicationBeansContainer().getCurrentDBABeanOnEditorID();
        
        DataBean  dBean = 
                getChartApplicationBeansContainer().getDataBeanByID(id);
        //mLogger.info("init is called. The id = " + id +" and DB Bean is " + dBean);
        if(dBean != null){
            String jndi = dBean.getJNDIName();
            getTextField1().setText(jndi);
            String q = dBean.getQuery();
                    getTextArea1().setText(q);
             String dsType = dBean.getDataSetType();    
             getDropDown1().setSelected(dsType);
            
        }
        
        
        // <editor-fold defaultstate="collapsed" desc="Managed Component Initialization">
        // Initialize automatically managed components
        // *Note* - this logic should NOT be modified
        try {
            _init();
        } catch (Exception e) {
            log("ConfigureNewDataAccess Initialization Failure", e);
            throw e instanceof FacesException ? (FacesException) e: new FacesException(e);
        }
        
        // </editor-fold>
        // Perform application initialization that must complete
        // *after* managed components are initialized
        // TODO - add your own initialization code here
    }

    /**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a postback request that
     * is processing a form submit.  Customize this method to allocate
     * resources that will be required in your event handlers.</p>
     */
    public void preprocess() {
    }

    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a postback and then navigated to a different page).  Customize
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     */
    public void prerender() {
        
    }

    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called (regardless of whether
     * or not this was the page that was actually rendered).  Customize this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     */
    public void destroy() {
    }

    
    private SingleSelectOptionsList l = new SingleSelectOptionsList();
    public SingleSelectOptionsList getOptions() {
        ArrayList al = new ArrayList();
        //l = new SingleSelectOptionsList();
        Option[] options = new Option[4];
        Option opt1 = new Option();
        opt1.setLabel("Category Based");
        opt1.setValue(JFChartConstants.CATEGORY_DATASET);
        options[0] = opt1;

        Option opt2 = new Option();
        opt2.setLabel("XYSeries Based");
        opt2.setValue(JFChartConstants.XY_DATASET);
        options[1] = opt2;

        
        Option opt3 = new Option();
        opt3.setLabel("Pie Based");
        opt3.setValue(JFChartConstants.PIE_DATASET);
        options[2] = opt3;

        
        Option opt4 = new Option();
        opt4.setLabel("Single Value Based");
        opt4.setValue(JFChartConstants.VALUE_DATASET);
        options[3] = opt4;
        
        l.setOptions(options);
        //getDropDown1().setSelected(opt4.getValue());
        return l;
         
    }
    
     private ChartApplicationBeansContainer getChartApplicationBeansContainer() {
        return (ChartApplicationBeansContainer)getBean("ChartApplicationBeansContainer");
        
    }
     
     private Object getRequestParameter(String key) {
         return getRequestMap().get(key);
     }
     
    
    public String button1_action() {
        // TODO: Process the button click action. Return value is a navigation
        // case name where null will return to the same page.
        String dsType = (String)this.getDropDown1().getSelected();
        String jndiName = (String)getTextField1().getText();
        String q = (String)this.getTextArea1().getText();
        String id = getChartApplicationBeansContainer().getCurrentDBABeanOnEditorID();
        //SQLDataAccess sqlDA = new SQLDataAccess(dsType,q);
        mLogger.info("The ds is = " + dsType);
        mLogger.info("The id = " + id);
        DataBean  dBean = 
                getChartApplicationBeansContainer().getDataBeanByID(id);
        mLogger.info("The bean is = " + dBean);
        dBean.setQuery(q);
        dBean.setJNDIName(jndiName);
        dBean.setDataSetType(dsType);
        try {
            getChartApplicationBeansContainer().persistDataBean(id);
        }catch(Exception e) {
            mLogger.severe("Error while saving data bean. " + e.getMessage());
            e.printStackTrace();
            return "error";
        }
        return "finished";

    }
}
