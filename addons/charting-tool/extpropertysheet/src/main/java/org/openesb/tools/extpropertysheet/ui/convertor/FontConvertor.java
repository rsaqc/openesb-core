/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FontConvertor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.ui.convertor;

 
import java.awt.Font;

/**
 *
 * @author rdwivedi
 */
public class FontConvertor implements Convertor {
    
     
    public FontConvertor() {
    }
    
    public String getStringValue(Object obj) {
        
        Font font = (Font)obj;
        String s =  font.getName() + ";" + font.getStyle() + ";" + font.getSize();
        return s;
    }
    
    public Object getObjectFromString(String obj,short type){
       String[] str =  obj.split(";");
       if(str.length == 3) {
       Font font = new Font(str[0],Integer.parseInt(str[1]),Integer.parseInt(str[2]) );
       return font;
       } else return null;
        
    }
    
}
