/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtPropertySheetTag.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.ui;

import javax.faces.component.UIComponent;
import javax.faces.el.ValueBinding;
import javax.faces.webapp.UIComponentTag;
import java.util.logging.Logger;

/**
 *
 * @author rdwivedi
 */
public class ExtPropertySheetTag extends UIComponentTag {
    
    private static Logger mLogger = Logger.getLogger(ExtPropertySheetTag.class.getName());
    /** Creates a new instance of ExtPropertySheetTag */
    private String mPropGroups = null;
    private String mResBundle = null;
    public ExtPropertySheetTag() {
    }

    public String getComponentType() {
        return ExtPropertySheetComponent.COMPONENT_TYPE;
    }

    public String getRendererType() {
        return null;
    }
    protected void setProperties(UIComponent component) {
		
        super.setProperties(component);
        mLogger.info("set" + component.getClass().getName());
        if (mPropGroups != null) {
            if (isValueReference(mPropGroups)) {
                ValueBinding vb = getFacesContext().getApplication()
                        .createValueBinding(mPropGroups);
                component.setValueBinding("propertyGroups", vb);
            } else {
                component.getAttributes().put("propertyGroups", mPropGroups);
            }
        }
        
        if (mResBundle != null) {
            if (isValueReference(mResBundle)) {
                ValueBinding vb = getFacesContext().getApplication()
                        .createValueBinding(mResBundle);
                component.setValueBinding("resourceBundle", vb);
            } else {
                component.getAttributes().put("resourceBundle", mResBundle);
            }
        }
        ((ExtPropertySheetComponent)component).createSubComponent(getFacesContext());
    }
    
    public void setPropertyGroups(String b) {
       mPropGroups = b;
        
    }
    public String getPropertyGroups() {
        return mPropGroups;
    }
    
    public void setResourceBundle(String b) {
       mResBundle = b;
        
    }
    public String getResourceBundle() {
        return mResBundle;
    }
    
    
}
