<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>

<wiki:TabbedSection >
  <wiki:Tab id="conflict" title="Oops">
    <div class="error">
      <b>Oops!  Someone modified the page while you were editing it!</b>
      <p>
      Since I am stupid and can't figure out what the difference
      between those pages is, you will need to do that for me. I've
      printed here the text (in Wiki) of the new page, and the
      modifications you made. You'll now need to copy the text onto a
      scratch pad (Notepad or emacs will do just fine), and then edit
      the page again.
      </p>
      <p>Note that when you go back into the editing mode, someone might have
      changed the page again. So be quick.</p>
    </div>
  </wiki:Tab>
  <wiki:Tab id="conflictOther" title="Modified by someone else">
    <tt><%=pageContext.getAttribute("conflicttext",PageContext.REQUEST_SCOPE)%></tt>      
  </wiki:Tab>
  <wiki:Tab id="conflictown" title="Here is your text">
    <tt><%=pageContext.getAttribute("usertext",PageContext.REQUEST_SCOPE)%></tt>
  </wiki:Tab>
</wiki:TabbedSection>