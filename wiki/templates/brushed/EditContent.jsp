<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>

<%
  /* see commonheader.jsp */
  String prefEditorType     = (String) session.getAttribute("prefEditorType");
  //prefEditorType = "plain"; //"FCK";

  WikiContext wikiContext = WikiContext.findContext(pageContext);
  int attCount = wikiContext.getEngine().getAttachmentManager()
                            .listAttachments( wikiContext.getPage() ).size();
  String attTitle = "Attach";
  if( attCount != 0 ) attTitle += " (" + attCount + ")";
  boolean showAttachTab = (attCount > 0 );
%>
<wiki:Permission permission="upload">
<% showAttachTab = true; %>
</wiki:Permission>

<wiki:TabbedSection>
<wiki:Tab id="editcontent" title="Edit" accesskey="e">

<wiki:CheckLock mode="locked" id="lock" >
    <p class="error">User '<%=lock.getLocker()%>' has started to edit this page, but has 
    not yet saved.  I won't stop you from editing this page anyway, BUT be aware that
    the other person might be quite annoyed.  It would be courteous to wait for his lock
    to expire or until he stops editing the page.  The lock expires in 
    <%=lock.getTimeLeft()%> minutes.
    </p>
</wiki:CheckLock>
<wiki:CheckVersion mode="notlatest">
  <p class="warning">You are about to restore version <wiki:PageVersion/>.
  Click on "Save" to restore.  You may also edit the page before restoring it.
  </p>
</wiki:CheckVersion>

<%-- <wiki:Editor /> --%>
<% if( prefEditorType.equals( "FCK" ) ) { %>
  <wiki:Include page="editors/FCK.jsp" />  
<% } else { %>
  <wiki:Include page="editors/plain.jsp" />
<% } %>

</wiki:Tab>

<wiki:PageExists>
<% if( showAttachTab ) { %>
<wiki:Tab id="attachments" title="<%= attTitle %>" accesskey="a">
  <wiki:Include page="AttachmentTab.jsp"/>
</wiki:Tab>
<% } %>

<wiki:Tab id="pageinfo" title="Info" accesskey="i" >
  <wiki:Include page="InfoTab.jsp"/>
</wiki:Tab>
</wiki:PageExists>

<% if( prefEditorType.equals( "plain" ) ) { %>
<wiki:Tab id="edithelp" title="Help" accesskey="h">
  <wiki:Include page="HelpTab.jsp"/>
</wiki:Tab>
<% } %>

</wiki:TabbedSection>