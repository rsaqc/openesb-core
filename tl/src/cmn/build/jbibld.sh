#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#
#
# @(#)jbibld.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#

# jbiBuild - production build script for jbi product
#
# Conventions:  Lowercase names are generally local vars (not exported).
#               Uppercase names are generally exported vars.
#


############################### USAGE ROUTINES ################################

usage()
{
    status=$1

    cat << EOF

Usage:  $p [options...] [var=def...] [-Dprop=val] [target(s)]

 Master build script for JBI products.

Options:
 -help           Display this message.
 -helpflow       Display the build stages.
 -test           Run all scripts in non-destructive test mode.
 -keepon         Keep running, even if compile fails. RELEASE DEFAULT
 -nokeepon       Don't run tests if it doesn't compile.  DEVELOPER DEFAULT.
 -clean          Do a clean build (call ant with clean options). DEFAULT.
 -noclean        Do not do a clean build.
 -cleanmavenrepo Remove maven local repository, and require build to redownload all maven dependencies
 -fixcvsroot     Update CVS working directory sources to use the current \$CVSROOT setting.
 -update         Update (or create) working directory. DEFAULT FOR RELEASE BUILD.
 -cleansrc       Clean out the source code before building.
                 Implies "-update -updateas" for development builds unless explicitly
                 turned off.
 -ee             install appserver EE.  Implies -updateas.
 -pe             install appserver PE.  Implies -updateas.
 -updateas|-updateappserver|-updateas8
                 Update the appserver installation.  DEFAULT IF -update
 -noupdateas|-noupdateappserver|-noupdateas8
                 Do not update the appserver installation.  DEVELOPER DEFAULT.
 -updatemaven    Update external dependencies in local maven repository.  DEFAULT IF -update
 -noupdatemaven  Do not update external dependencies in local maven repository.
                 (pass --offline option to all mvn commands.)
 -noupdate       Do not checkout or update source code from CVS. DEVELOPER DEFAULT.
 -fast|-fastupdate
                 Short-hand for:  $p -update -noupdateappserver -noregress -nojavadoc
 -integreport    Create integration report. DEFAULT FOR RELEASE BUILD.
 -nointegreport  Do not create integration report. DEFAULT FOR DEVELOPER BUILD.
 -taskreport     Create project task report. DEFAULT FOR PRIMARY RELEASE BUILD.
 -notaskreport   Do not create project task report. DEVELOPER DEFAULT.
 -nobuild        Do not build.
 -product <prod> Build and test against <prod>, which can be  "shasta"
                 or "whitney".  Default is "shasta".
 -javadoc        Create javadoc. DEFAULT FOR RELEASE BUILD.
 -nojavadoc      Do not create javadoc. DEVELOPER DEFAULT.
 -site           Run maven site reports. DEVELOPER DEFAULT.
 -nosite         Do not run maven site reports.
 -xref           Create cross-referenced sources. DEFAULT FOR RELEASE BUILD.
 -noxref         Do not create cross-referenced sources. DEVELOPER DEFAULT.
 -archive_doc    Archive javadoc and xref builds. DEFAULT FOR RELEASE BUILD.
 -noarchive_doc  Do not archive javadoc and xref builds. DEVELOPER DEFAULT.
 -archive_logs   Save test output with permanant log. DEFAULT.
 -noarchive_logs Do not save test output and servers logs.
 -release        Build all release kits.  DEFAULT FOR RELEASE BUILD.
 -norelease      Do not build any release kits.  DEVELOPER DEFAULT.
 -repackgf       Repackage glassfish distribution with latest open-esb runtime.
 -norepackgf     Do no repackage glassfish distribution. DEVELOPER DEFAULT.
 -install        Build the installer.  DEFAULT FOR RELEASE BUILD.
 -noinstall      Do not build the installer.  DEVELOPER DEFAULT.
 -installtests   Run installer tests.  DEFAULT IF BUILDING INSTALLER.
 -noinstalltests Do not run the installer tests.
 -pushkit        Push kits to a location other than the usual kitroot.
 -nopushkit      Do not push kits to a location other than the usual kitroot (DEFAULT)
 -alaska         Build with files in KITROOT/seebeyond/alaska/alaska_components.zip DEFAULT FOR RELEASE BUILD.
 -noalaska       Do not build with files in KITROOT/seebeyond/alaska/alaska_components.zip DEVELOPER DEFAULT.
 -noreview       Do not run checkStyle.
 -regress        Run all junit and jregress tests (default).
 -jregress       Run the jregress tests (DEFAULT)
 -junit          Run the junit tests (DEFAULT)
 -noregress      Do not run junit or jregress tests.

 -nojregress     Do not run the jregress tests.
 -nojunit        Do not run the junit tests.
 -ant_output     Send the ANT build output to the terminal as well as to
                 the log file.
 -verbose        Tell ant to be verbose.
 -bldnum num     use <num> for BLDNUM instead of generating a new one.
                 useful for re-releasing kits without a new build.
 var=def...      Substitute <def> for each <var>, and export to the environment.
 -Dprop=val...   Pass the given propery/value to ant.
 target(s)...    Build the given targets in the main build.xml file.

Environment:
 LOGDIR          Where to put all log files.
 LOCKDIR         Where to look for and write files used to communicate
                 completion of various build steps.

 KITROOT         The root of all product kits.
 KIT_DISTROOT    Where to distribute kits.  Normally set to \$KITROOT/\$PRODUCT

 DEVELOPER_BUILD Set this to 1 if you are building for development.

 RELEASE_BUILD   Set this to 1 if for official release builds only.
                 DO NOT SET THIS ENVIRONMENT VARIABLE IN A DEVELOPMENT PATH.

 INTEG_REPORT    Set to the name of script to create the integration report.

Developer Build Example:
 $p -update

Release Build Example:
 $p -bldnum \$BLDNUM

EOF

    exit $status
}

help_flow()
{
    cat << EOF
 Build flow is:
    1. clean
    2. update_src
    3. build_$PRODUCT
    4  BACKGROUND TASKS (in parallel with tests):
        4a. code_review
        4b. javadoc
        4c. make_release
        4d. make_installer
        4e. xref
        4f. archive_doc
    5. run_tests
    6. endbuild

EOF
}

parse_args()
{
    ### set option defaults according to builder profile.
    bld_set_builder_profile

    DOHELP=0
    TESTMODE=0; testarg=
    CALLEDFROMWRAPPER=0
    BLDNUM_ARG=NULL
    ANT_PROPS=
    VERBOSE_ARG=
    had_explicit_noupdate=0
    REGRESS_PRODUCT=openesb
    DOFIXCVSROOT=0
    DOAPPSERVER_INSTALL=0
    DOMAVEN_UPDATE=1
    DOTASK_REPORT=0
    DEBUG=0
    DOREGRESS=1
    DOJUNIT=1
    DOJREGRESS=1
    DOPUSHKIT=0
    DOPUSHMAVEN=0
    INSTALLAS_ARGS=
    DOSITE=1
    DOINSTALL=0
    DOINSTALL_TESTS=0
    REPACKAGE_GF=0

    I_AM_PRIMARY=0
    if [ "$FORTE_PORT" = "$PRIMARY_PORT" ]; then
        I_AM_PRIMARY=1
    fi

    if [ $RELEASE_BUILD -eq 1 ]; then
        DOARCHIVELOGS=0
        DOBUILD=1
        #always clean object files:
        DOCLEAN=1
        DOCLEANSRC=0
        DOMAVENCLEAN=0
        DOUPDATE=1
        DOAPPSERVER_INSTALL=1
        DOMAVEN_UPDATE=1
        DOCODEREVIEW=0
        DOJAVADOC=1
        #do not ignore build failures, even for release,
        #as this leaves appserver instances running.  RT 3/2/06
        KEEPON=0
        if [ $I_AM_PRIMARY -eq 1 ]; then
            DOINTEGREPORT=1
            DORELEASE=1
            #default is to only do the task report on the primary release build:
            #disabled; unused now.  RT 7/19/06
            DOTASK_REPORT=0
            DOINSTALL=1
            DOINSTALL_TESTS=1
            DOXREF=1
            DOARCHIVEDOC=1
            DOALASKA=1
            REPACKAGE_GF=1
        else
            DOINTEGREPORT=0
            DORELEASE=0
            DOXREF=0
            DOARCHIVEDOC=0
            DOINSTALL=0
            DOALASKA=0
        fi
        if [ "$BUILD_JES" = "YES" ]; then
            DOJES=1
        else
            DOJES=0
        fi
        if [ "$BUILD_JBOSS" = "YES" ]; then
            DOJBOSS=1
        else
            DOJBOSS=0
        fi
    else
        DOCLEAN=1
        DOCLEANSRC=0
        DOMAVENCLEAN=0
        DOUPDATE=0
        DOAPPSERVER_INSTALL=0
        DOMAVEN_UPDATE=1
        DOINTEGREPORT=0
        DORELEASE=0
        DOINSTALL=0
        DOBUILD=1
        DOCODEREVIEW=0
        DOJAVADOC=1
        DOARCHIVEDOC=0
        DOXREF=0
        KEEPON=0
        DOARCHIVELOGS=0
        DOJES=0
        DOJBOSS=0
        DOALASKA=0
    fi

    while [ $# -gt 0 -a "$1" != "" ]
    do
        arg=$1; shift

        case $arg in
        -helpf* )
            help_flow
            exit 0
            ;;
        -h* )
            usage 0
            ;;
        -debug )
            DEBUG=1
            ;;
        -t* )
            TESTMODE=1; testarg="-test"
            ;;
        -calledFromWrapper )
            #true if called from runjbiBuild wrapper:
            CALLEDFROMWRAPPER=1
            ;;
        -clean )
            DOCLEAN=1
            ;;
        -cleanmavenrepo )
            DOMAVENCLEAN=1
            DOMAVEN_UPDATE=1
            ;;
        -cleansrc )
            DOCLEANSRC=1
            DOUPDATE=1
            DOAPPSERVER_INSTALL=1
            ;;
        -bldnum )
            if [ $# -gt 0 ]; then
                BLDNUM_ARG=$1; shift
            else
                echo "${p}: -bldnum requires an argument"
                usage 1
            fi
            ;;

        -noclean )
            DOCLEAN=0
            ;;
        -pushmaven )
            DOPUSHMAVEN=1
            ;;
         -nopushmaven )
            DOPUSHMAVEN=0
            ;;
        -pushkit )
            DOPUSHKIT=1
            ;;
         -nopushkit )
            DOPUSHKIT=0
            ;;
        -keepon )
            KEEPON=1
            ;;
        -nokeepon )
            KEEPON=0
            ;;
        -regress )
            #useful with -fast option.
            DOREGRESS=1
            DOJUNIT=1
            DOJREGRESS=1
            ;;
        -noregress )
            DOREGRESS=0
            DOJUNIT=0
            DOJREGRESS=0
            ;;
        -jregress )
            DOJREGRESS=1
            ;;
        -nojregress )
            DOJREGRESS=0
            ;;
        -junit )
            DOJUNIT=1
            ;;
        -nojunit )
            DOJUNIT=0
            ;;
        -javadoc )
            DOJAVADOC=1
            ;;
        -nojavadoc )
            DOJAVADOC=0
            ;;
        -archive_doc )
            DOARCHIVEDOC=1
            ;;
        -noarchive_doc )
            DOARCHIVEDOC=0
            ;;
        -archive_logs )
            DOARCHIVELOGS=1
            ;;
        -noarchive_logs )
            DOARCHIVELOGS=0
            ;;
        -site )
            DOSITE=1
            ;;
        -nosite )
            DOSITE=0
            ;;
        -xref )
            DOXREF=1
            ;;
        -noxref )
            DOXREF=0
            ;;
        -release )
            DORELEASE=1
            ;;
        -norelease )
            DORELEASE=0
            ;;
        -repackgf )
            REPACKAGE_GF=1
            ;;
        -norepackgf )
            REPACKAGE_GF=0
            ;;
        -alaska )
            DOALASKA=1
            ;;
        -noalaska )
            DOALASKA=0
            ;;
        -jes )
            DOJES=1
            ;;
        -nojes )
            DOJES=0
            ;;
        -jboss )
            DOJBOSS=1
            ;;
        -nojboss )
            DOJBOSS=0
            ;;
        -taskreport )
            DOTASK_REPORT=1
            ;;
        -notaskreport )
            DOTASK_REPORT=0
            ;;
        -installtests )
            DOINSTALL_TESTS=1
            ;;
        -noinstalltests )
            DOINSTALL_TESTS=0
            ;;
        -install )
            #default is to run installer tests if we are building the installer.
            #specify -install -noinstalltests if you don't want to run them.
            DOINSTALL_TESTS=1
            DOINSTALL=1
            ;;
        -noinstall )
            DOINSTALL_TESTS=0
            DOINSTALL=0
            ;;
        -nocodereview|-noreview )
            DOCODEREVIEW=0
            ;;
        -nobuild )
            DOBUILD=0
            #don't clean if we're not building
            DOCLEAN=0
            ;;
        -fixcvsroot )
            #run fixcvsroot utility to update $CVSROOT settings for product sources:
            DOFIXCVSROOT=1
            ;;
        -update )
            #update product sources:
            DOUPDATE=1
            DOMAVEN_UPDATE=1
            DOAPPSERVER_INSTALL=1
            ;;
        -noupdate )
            DOUPDATE=0
            DOAPPSERVER_INSTALL=0
            had_explicit_noupdate=1
            ;;
        -updatemaven )
            DOMAVEN_UPDATE=1
            ;;
        -noupdatemaven )
            DOMAVEN_UPDATE=0
            ;;
        -updateas|-updateappserver|-updateas8 )
            DOAPPSERVER_INSTALL=1
            ;;
        -noupdateas|-noupdateappserver|-noupdateas8 )
            DOAPPSERVER_INSTALL=0
            ;;
        -pe )
            INSTALLAS_ARGS="-pe"
            DOAPPSERVER_INSTALL=1
            ;;
        -ee )
            INSTALLAS_ARGS="-ee"
            DOAPPSERVER_INSTALL=1
            ;;
        -fast|-fastupdate )
            DOUPDATE=1
            DOAPPSERVER_INSTALL=0
            #only run junit tests (this will force use to compile tests):
            DOREGRESS=1
            DOJUNIT=1
            DOJREGRESS=0
            DOJAVADOC=0
            DOSITE=0
            ;;
        -integreport )
            DOINTEGREPORT=1
            ;;
        -nointegreport )
            DOINTEGREPORT=0
            ;;
        -verbose )
            VERBOSE_ARG="-verbose"
            ;;
        -product )
            if [ $# -gt 0 ]; then
                REGRESS_PRODUCT=$1; shift
            else
                echo "${p}: -product requires the name of the top-level bom (shasta/whitney)"
                usage 1
            fi
            ;;

        -D*=* )
            if [ -z "$ANT_PROPS" ]; then
                ANT_PROPS="$arg"
            else
                ANT_PROPS="$ANT_PROPS $arg"
            fi
            ;;
        -D* )
            echo "${p}: illegal option, $arg"
            usage 1
            ;;
        *=* )
            tmp=`echo $arg|sed -e 's/"/\\\\"/g'`
            #echo A arg=.$arg. tmp is .$tmp.
            tmp=`echo $tmp|sed -e 's/^\([^=][^=]*\)=\(.*\)/\1="\2"; export \1/'`
            #echo B tmp is .$tmp.
            eval $tmp
            ;;
        -* )
            echo "${p}: unknown option, $arg"
            usage 1
            ;;
        esac
    done

    # reset DOCLEAN if it is not needed.
    if [ $DOCLEAN -eq 1 ]; then
        if [ $DOCLEANSRC -eq 1 ]; then
            #we don't need to run the clean step if we are removing the src tree:
            DOCLEAN=0
            bldmsg -p $p -warn Ignoring -clean because -cleansrc is also specified.
        elif [ ! -d "$SRCROOT/build-common" ]; then
            #we don't need to run the clean step because source has not been checked out yet:
            DOCLEAN=0
            bldmsg -p $p -warn Ignoring -clean because build-common is missing - assume first build.
        fi
    fi

    #!junit and !jregress => !regress
    if [ $DOJUNIT -eq 0 -a $DOJREGRESS -eq 0 ]; then
        DOREGRESS=0
    fi

    #converse is also true:
    if [ $DOREGRESS -eq 0 ]; then
        DOJUNIT=0
        DOJREGRESS=0
    fi

    ### NOTE: more option setup in set_global_vars.
}

############################### INFO ROUTINES #################################

show_initial_options()
{
    bldmsg -mark Running $p $saveargs
    show_options INITIAL
}

show_final_options()
{
    show_options FINAL
}

show_options()
{
    cat << EOF
$1 OPTION SETTINGS FOR $p -
    DOHELP is          $DOHELP
    TESTMODE is        $TESTMODE
    DOCLEAN is         $DOCLEAN
    DOMAVENCLEAN is    $DOMAVENCLEAN
    DOMAVEN_UPDATE is  $DOMAVEN_UPDATE
    DOCLEANSRC is      $DOCLEANSRC

    DOUPDATE is        $DOUPDATE
    DOFIXCVSROOT is    $DOFIXCVSROOT

    DOBUILD is         $DOBUILD
    DOCODEREVIEW is    $DOCODEREVIEW
    DOREGRESS is       $DOREGRESS
    DOJREGRESS is      $DOJREGRESS
    DOJUNIT is         $DOJUNIT

    DOJAVADOC is       $DOJAVADOC
    DOSITE is          $DOSITE
    DOXREF is          $DOXREF
    DOARCHIVEDOC is    $DOARCHIVEDOC
    DOARCHIVELOGS is   $DOARCHIVELOGS

    DORELEASE is       $DORELEASE

    DOINSTALL is       $DOINSTALL
    DOALASKA is        $DOALASKA
    DOINSTALL_TESTS is $DOINSTALL_TESTS
    REPACKAGE_GF is    $REPACKAGE_GF

    DOJES is           $DOJES
    DOJBOSS is         $DOJBOSS

    DOINTEGREPORT is   $DOINTEGREPORT
    DOTASK_REPORT is   $DOTASK_REPORT

    REGRESS_PRODUCT is $REGRESS_PRODUCT

EOF

}

show_build_environment()
{
    #show general environment:
    bld_show_env

    #show local additions:
    cat << EOF

    I_AM_PRIMARY is $I_AM_PRIMARY

    UPDATELOG is    $UPDATELOG
    CLEANLOG is     $CLEANLOG
    BUILDLOG is     $BUILDLOG
    JAVADOCLOG is   $JAVADOCLOG
    SITELOG is      $SITELOG
    XREFLOG is      $XREFLOG
    RELEASELOG is   $RELEASELOG
    PUSHKITLOG is   $PUSHKITLOG
    UNITTESTLOG is  $UNITTESTLOG
    REGRESSLOG is   $REGRESSLOG

    INSTALLOG is         $INSTALLOG
    INSTALLER_TESTLOG is $INSTALLER_TESTLOG is
    QECOMPONENTSLOG is   $QECOMPONENTSLOG
    QECOMPONENTSLOG is   $QECOMPONENTSLOG

    REPACK_GF_LOG is       $REPACK_GF_LOG
    GLASSFISH_VERSION is   $GLASSFISH_VERSION
    GLASSFISH_BUILD is   $GLASSFISH_BUILD
    GLASSFISH_KIT_URL is   $GLASSFISH_KIT_URL
    GLASSFISH_PLATFORMS is $GLASSFISH_PLATFORMS
    GLASSFISH_STAGEDIR is  $GLASSFISH_STAGEDIR

    FIXCVSROOTLOG is     $FIXCVSROOTLOG
    INTEGRATIONLOG is    $INTEGRATIONLOG

    JBI_CVSROOT is    $JBI_CVSROOT
    CVS_OPTS is        $CVS_OPTS

    HTML_SRCROOT is     $HTML_SRCROOT

    BLDNUM is   $BLDNUM
    KIT_REV is  $KIT_REV

    DEVELOPER_BUILD is $DEVELOPER_BUILD
    RELEASE_BUILD is   $RELEASE_BUILD

    JBI_BRANCH_NAME is $JBI_BRANCH_NAME

EOF
}

################################## UTILITIES ##################################

require()
#import external shell routines - fatal error if we can't find it.
{
    libname=$1

    if [ x$libname = x ]; then
        echo "BUILD_ERROR: ${p}:require:  missing file name - ABORT"
        exit 1
    fi

    #look in a couple of familiar places:
    if [ -f "$TOOLROOT/lib/cmn/$libname" ]; then
        libname=$TOOLROOT/lib/cmn/$libname
    elif [ -f "./$libname" ]; then
        #we assume this is a test env!
        echo "$p - BUILD_WARNING: loading $libname from current directory."
        libname=./$libname
    fi

    . $libname
    if [ $? -ne 0 ]; then
        echo "BUILD_ERROR: ${p}:require: errors sourcing $libname - ABORT"
        exit 1
    fi
}

has_ant_errors()
#search for ant task errors:
#   [javac] 60 errors
#   [javadoc] 1 error
#   [junit] TEST com.sun.iis.ebxml.internal.support.logger.TestGlobalLogger FAILED
{
    if [ "$1" = "" ]; then
        bldmsg -error -p $p "Usage:  has_ant_errors LOGFILE"
        return 0
    fi

    #check for javac/javadoc errors:
    grep '\] [0-9]* error'  $1 > /dev/null
    if [ $? -eq 0 ]; then
        return 1
    fi

    #check for junit errors:
    grep 'Tests run:' $1 | grep -v 'Failures: 0, Errors: 0,' > /dev/null
    if [ $? -eq 0 ]; then
        #we found something matching, which means we had failures:
        return 1
    fi

    #check for jregress errors:
    egrep '\*TIMED OUT\*|\*FAILED\*| Connection refused' $1 > /dev/null
    if [ $? -eq 0 ]; then
        return 1
    fi

    return 0
}

filter_maven_log()
#funtion to reduce the maven download progress strings to just the final download total.
#INPUT:  stdin
#OUTPUT:  stdout
{
    perl -n -a -e 's|^.*\r([0-9][0-9Kb]* downloaded)$|$1|g; print;'
}

filter_maven_log_in_place()
#Usage:  filter_maven_log_in_place fn
#replace contents of <fn> with results of filter_maven_log
{
    if [ "$1" != "" ]; then
        filter_maven_log < "$1" > "$TMPA"
        if [ $? -eq 0 ]; then
            cmp -s "$1" "$TMPA"
            if [ $? -ne 0 ]; then
                #files are different:
                mv "$TMPA" "$1"
                return $?
            fi

            ## files are the same
            rm -f "$TMPA"
            return 0
        fi
        rm -f "$TMPA"
        return 1
    fi

    #FAILED:
    return 1
}

############################### INITIALIZATION ################################

setup_wrapper_env()
#setup up variables that are normally set by wrapper.
{
    #we require a LINKROOT separate from SRCROOT:
    if [ x$FORTE_LINKROOT = x ]; then
        if [ "$FORTE_PORT" != "nt" ]; then
            bld_fatal_error "FORTE_LINKROOT must be set to build JBI - ABORT"
        fi
    fi

    bld_setup_logdir
    if [ $? -ne 0 ]; then
        bld_fatal_error "failed to set up log directory - ABORT"
    fi

    bld_setup_lockdir

    #set up port-class vars:
    export IS_UNIX IS_NT

    IS_NT=0; bldhost -is_nt $FORTE_PORT
    if [ $? -eq 0 ]; then
        IS_NT=1
    fi

    IS_UNIX=0; bldhost -is_unix $FORTE_PORT
    if [ $? -eq 0 ]; then
        IS_UNIX=1
    fi
}

set_global_vars()
{
    p=`basename $0`
    TMPA=/tmp/${p}A.$$

    ###############
    # BUILD CONTROL
    ###############

    export MAVEN_OPTIONS MAVEN_OFFLINE KITROOT_URL KITROOT_SMB GFESB_KITROOT

    #some build steps can be run offline:
    MAVEN_OFFLINE="-o -npu"

    if [ $DOMAVEN_UPDATE -eq 0 ]; then
      MAVEN_OPTIONS="$MAVEN_OFFLINE"
      #if we incorporte offline options into MAVEN_OPTIONS,
      #then don't repeat for offline #build steps:
      MAVEN_OFFLINE=
    fi

    if [ "$GFESB_KITROOT" = "" ]; then
       GFESB_KITROOT="NEED_TO_SET_VAR"
       bldmsg -warn -p $p "defaulting GFESB_KITROOT for gfesb pickup to '$GFESB_KITROOT'"
    fi

    if [ $DOPUSHMAVEN -eq 1 ]; then
        #Make sure required environment is defined:
        if [ x$PUSHMAVEN_REMOTE_REPOS = x ]; then
            DOPUSHKIT=0
            bldmsg -error -p $p "turning off -pushkit because PUSHMAVEN_REMOTE_REPOS is not set"
        fi
        if [ x$PUSHMAVEN_VERSION = x ]; then
            DOPUSHKIT=0
            bldmsg -error -p $p "turning off -pushkit because PUSHMAVEN_VERSION is not set"
        fi
    fi

    if [ $DOPUSHKIT -eq 1 ]; then
        #Make sure required environment is defined:
        if [ x$PUSHKIT_IDENTITY = x ]; then
            DOPUSHKIT=0
            bldmsg -error -p $p "turning off -pushkit because PUSHKIT_IDENTITY is not set"
        fi
        if [ x$PUSHKIT_DEST = x ]; then
            DOPUSHKIT=0
            bldmsg -error -p $p "turning off -pushkit because PUSHKIT_DEST is not set"
        fi
        if [ x$PUSHKIT_SRC = x ]; then
            DOPUSHKIT=0
            bldmsg -error -p $p "turning off -pushkit because PUSHKIT_SRC is not set"
        fi
    fi

    #if we passed the -pushkit sainty tests:
    if [ $DOPUSHKIT -eq 1 ]; then
        #make sure we do the release step:
        if [ $DORELEASE -eq 0 ]; then
            DORELEASE=1
            bldmsg -warn -p $p "turning on -release because -pushkit requires it."
        fi
        if [ $DOINSTALL -eq 0 ]; then
            DOINSTALL=1
            bldmsg -warn -p $p "turning on -install because -pushkit requires it."
        fi
    fi

    #if we passed the -pushmaven sainty tests:
    if [ $DOPUSHMAVEN -eq 1 ]; then
        if [ $DORELEASE -eq 0 ]; then
            DORELEASE=1
            bldmsg -warn -p $p "turning on -release because -pushmaven requires it."
        fi
        if [ $DOINSTALL -eq 0 ]; then
            DOINSTALL=1
            bldmsg -warn -p $p "turning on -install because -pushmaven requires it."
        fi
    fi


    if [ $DOINSTALL -eq 1 ]; then
        #make sure we do the release step:
        if [ $DORELEASE -eq 0 ]; then
            DORELEASE=1
            bldmsg -warn -p $p "turning on -release because -install requires it."
        fi
        if [ $DOALASKA -eq 0 ]; then
            #installer build fails unless -alaska.   RT 8/22/07
            bldmsg -p $p -warn "the -install option requires -alaska - turning on -alaska."
            DOALASKA=1
        fi
    fi

    if [ $REPACKAGE_GF -eq 1 ]; then
        if [ $DORELEASE -eq 0 ]; then
            DORELEASE=1
            DOALASKA=1
            bldmsg -warn -p $p "turning on -release -alaska because -repackgf requires it."
        fi

        #default repackage variables if undefined:
        if [ "$GLASSFISH_VERSION" = "" ]; then
            GLASSFISH_VERSION=9.1_01
            bldmsg -warn -p $p "defaulting GLASSFISH_VERSION for -repackgf to '$GLASSFISH_VERSION'"
        fi

        if [ "$GLASSFISH_BUILD" = "" ]; then
            GLASSFISH_BUILD=v2ur1-b09d
            bldmsg -warn -p $p "defaulting GLASSFISH_BUILD for -repackgf to '$GLASSFISH_BUILD'"
        fi

        if [ "$GLASSFISH_KIT_URL" = "" ]; then
            GLASSFISH_KIT_URL="http://download.java.net/javaee5/v2_branch/promoted"
            bldmsg -warn -p $p "defaulting GLASSFISH_KIT_URL for -repackgf to '$GLASSFISH_KIT_URL'"
        fi

        if [ "$GLASSFISH_PLATFORMS" = "" ]; then
            GLASSFISH_PLATFORMS="AIX Darwin Linux SunOS SunOS_X86 WINNT"
            bldmsg -warn -p $p "defaulting GLASSFISH_PLATFORMS for -repackgf to '$GLASSFISH_PLATFORMS'"
        fi

        if [ "$GLASSFISH_STAGEDIR" = "" ]; then
            GLASSFISH_STAGEDIR="$SRCROOT/install/downloads/glassfish"
            bldmsg -warn -p $p "defaulting GLASSFISH_STAGEDIR for -repackgf to '$GLASSFISH_STAGEDIR'"
        fi

        if [ "$KITROOT_URL" = "" ]; then
            KITROOT_URL="NEED_TO_SET_THIS_VAR"
            bldmsg -warn -p $p "defaulting KITROOT_URL for -repackgf to '$KITROOT_URL'"
        fi

        if [ "$KITROOT_SMB" = "" ]; then
            KITROOT_SMB="NEED_TO_SET_THIS_VAR"
            bldmsg -warn -p $p "defaulting KITROOT_SMB for -repackgf to '$KITROOT_SMB'"
        fi
    fi

    #note - I_AM_PRIMARY is set by parse_args.
    if [ $I_AM_PRIMARY -eq 0 ]; then
        if [ $RELEASE_BUILD -eq 1 ]; then
            #we don't build & release on non-primary machines during RE builds:
            if [ $DORELEASE -eq 1 ]; then
                #user specified -release on non-primary - issue usage warning and unset
                bldmsg -warn -p $p "-release has no effect on non-primary RE build machines."
                DORELEASE=0
            fi
        fi
    fi

    if [ $DORELEASE -eq 1 -a $DOBUILD -eq 1 -a $DOJAVADOC -eq 0 ]; then
        #we must build javadoc if we are building release:
        bldmsg -warn -p $p "turning on -javadoc because -release requires it."
        DOJAVADOC=1
    fi

    ##############
    # BRANCH NAMES
    ##############
    export JBI_BRANCH_NAME
    if [ "$JBI_BRANCH_NAME" = "trunk" ]; then
        JBI_BRANCH_NAME="main"
        bldmsg -warn -p $p "JBI_BRANCH_NAME 'trunk' is deprecated; changing to '$JBI_BRANCH_NAME'"
    elif [ "$JBI_BRANCH_NAME" = "" ]; then
        JBI_BRANCH_NAME="main"
        bldmsg -warn -p $p "defaulting JBI_BRANCH_NAME to '$JBI_BRANCH_NAME'"
    fi

    #########
    # JAVADOC
    # Warning:  if you change $JAVADOC_BASE here, change it in common_defs.bom and antbld/inc/prolog.ant
    #########
    export JAVADOC_BASE LASSEN_JAVADOC WHITNEY_JAVADOC SHASTA_JAVADOC ARCHIVE_JAVADOC ALASKA_KITDIR
    ARCHIVE_JAVADOC=$SRCROOT/$JBI_BRANCH_NAME/javadoc_stable
    JAVADOC_BASE=$SRCROOT/antbld/bld/doc
    LASSEN_JAVADOC=$JAVADOC_BASE/lassen
    WHITNEY_JAVADOC=$JAVADOC_BASE/whitney
    SHASTA_JAVADOC=$JAVADOC_BASE/shasta
    ALASKA_KITDIR=$SRCROOT/antbld/bld/alaska

    if [ $DOXREF -eq 1 ]; then
        #make sure we do the javadoc step:
        if [ $DOJAVADOC -eq 0 ]; then
            DOJAVADOC=1
            bldmsg -warn -p $p "turning on -javadoc because -xref requires it."
        fi
    fi

    if [ $DOARCHIVEDOC -eq 1 -a $DOXREF -eq 0 -a $DOJAVADOC -eq 0 ]; then
        #possible that user *only* wants to run the archive step, but unlikely.
        bldmsg -warn -p $p "-archive_doc usually requires -javadoc or -xref options."
    fi

    ###########
    # X-DISPLAY
    ###########
    export DISPLAY
    DISPLAY=$REGRESS_DISPLAY

    ###########
    # LOG FILES
    ###########
    export ANTLOGDIR
    ANTLOGDIR=$LOGDIR/antlogs

    export CLEANLOG BUILDLOG RELEASELOG INSTALLOG QECOMPONENTSLOG JESLOG JESWAITLOG JBOSSLOG JBOSSWAITLOG
    export UNITTESTLOG REGRESSLOG UPDATELOG INTEGRATIONLOG FIXCVSROOTLOG PUSHKITLOG PUSHMAVENLOG
    export JAVADOCLOG SITELOG INSTALLER_TESTLOG REPACK_GF_LOG

    CLEANLOG=$LOGDIR/javaClean.log
    BUILDLOG=$LOGDIR/javaBuild.log
    RELEASELOG=$LOGDIR/makeRelease.log
    REPACK_GF_LOG=$LOGDIR/repackageGlassfish.log
    INSTALLOG=$LOGDIR/makeInstall.log
    INSTALLER_TESTLOG=$LOGDIR/installerTest.log
    QECOMPONENTSLOG=$LOGDIR/makeQEComponents.log
    JESLOG=$LOGDIR/makeJes.log
    JESWAITLOG=$LOGDIR/makeJesWait.log
    PUSHKITLOG=$LOGDIR/pushkit.log
    PUSHMAVENLOG=$LOGDIR/pushmaven.log
    JBOSSLOG=$LOGDIR/makeJboss.log
    JBOSSWAITLOG=$LOGDIR/makeJbossWait.log
    UNITTESTLOG=$LOGDIR/runUnitTests.log
    REGRESSLOG=$LOGDIR/runSystemTests.log
    UPDATELOG=$LOGDIR/cvs_update.log
    FIXCVSROOTLOG=$LOGDIR/fixcvsroot.log
    INTEGRATIONLOG=$LOGDIR/integ_${PRODUCT}.txt
    JAVADOCLOG=$LOGDIR/javadoc.log
    SITELOG=$LOGDIR/siteBuild.log
    XREFLOG=$LOGDIR/xref.log

    #init vars for build_summary() routine.
    BUILD_SUMMARY_REPORT=$LOGDIR/runRegress.rpt

    ### setup file to tell testResults which suite we are testing:
    REGSTAT=$LOGDIR/../regstat.ok
    rm -f $REGSTAT

    if [ "$PRI_BLD_LOCATION" = "" ]; then
      PRI_BLD_LOCATION=$SRCROOT
    fi

    ############
    # LOCK FILES
    ############
    KITSREADY=$LOCKDIR/kits.rdy
    SETRELEASEREADY=$LOCKDIR/release.rdy
    TESTRELEASEREADY=$LOCK_READDIR/release.rdy
    READYFORJES=$PRI_BLD_LOCATION/bldlock/jes.rdy
    READYFORJBOSS=$PRI_BLD_LOCATION/bldlock/jboss.rdy

    export MAX_KIT_WAIT
    if [ "$MAX_KIT_WAIT" = "" ]; then
        #wait up to 30 minutes (1800 seconds) for primary build:
        MAX_KIT_WAIT=1800
    fi

    #if we have a -bldnum arg, then reset BLDNUM (set by bld_setup_logdir).
    if [ $BLDNUM_ARG != NULL ]; then
        BLDNUM=$BLDNUM_ARG
    fi

    ###############
    # JBI_CVSROOT - this is the product CVSROOT used by this script.
    #                 You can override it if you are calling this script from
    #                 a special env. where CVSROOT means something else.
    ###############
    export JBI_CVSROOT
    if [ "$JBI_CVSROOT" = "" ]; then
        JBI_CVSROOT=$CVSROOT
    else
        bldmsg -warn -p $p "Setting JBI_CVSROOT from environment."
    fi

    #############
    # CVS OPTIONS
    #############
    export CVS_OPTS CVSIGNORE
    if [ "$CVS_OPTS" = "" ]; then
        #default is quiet, ignore ~/.cvsrc, read-only, compression:
        CVS_OPTS="-q -f -r -z6"
    else
        bldmsg -warn -p $p "Setting CVS_OPTS from environment."
    fi
    if [ "$CVSIGNORE" = "" ]; then
        CVSIGNORE='bld Makefile'
    else
        bldmsg -warn -p $p "Setting CVSIGNORE from environment."
    fi

    #####################
    # CVS CHECKOUT TARGET
    #####################
    # Use module lists from the environment, if specified. #
    if [ ! -z "$JBI_MODULES" ]; then
        JBI_CO_TARGET="$JBI_MODULES"
    else
        if [ "$JBI_BRANCH_NAME" = "main" ]; then
            JBI_CO_TARGET="trunk_build"
        else
            JBI_CO_TARGET="${JBI_BRANCH_NAME}_build"
        fi
    fi

    ####################
    # INTEGRATION REPORT
    ####################
    if [ $DOINTEGREPORT -eq 1 ]; then
        export INTEG_REPORT
        if [ "$INTEG_REPORT" = "" ]; then
            INTEG_REPORT=integrationReport
            bldmsg -warn -p $p "defaulting INTEG_REPORT to '$INTEG_REPORT'"
        fi
    fi

    ##########
    # JXTOHTML
    ##########
    export HTML_SRCROOT EXCEPTION_INDEX
    if [ "$HTML_SRCROOT" = "" ]; then
        if [ "$HTTP_LOGROOT" != "" ]; then
            HTML_SRCROOT="$HTTP_LOGROOT/$CODELINE/javadoc/regress_javadoc/src-html"
        else
            HTML_SRCROOT="http://iis.sfbay/jbi/main/javadoc/regress_javadoc/src-html"
        fi

        bldmsg -warn -p $p "defaulted HTML_SRCROOT to '$HTML_SRCROOT'"
    fi

    EXCEPTION_INDEX=$LOGDIR/_EXCEPTIONS.html

    #########
    # REGRESS PRODUCT TARGET
    #########
    case $REGRESS_PRODUCT in
    shasta )
        ;;
    whitney )
        ;;
    openesb )
        REGRESS_PRODUCT=shasta
        ;;
    * )
        bldmsg -error -p $p "Unrecognized -product arg, '$REGRESS_PRODUCT' - defaulting to 'shasta'"
        REGRESS_PRODUCT=shasta
        ;;
    esac

    #########
    # ASADMIN TIMEOUT:
    #########
    #how long to wait for asadmin commands:
    export MAX_ASADMIN_WAIT
    if [ "$MAX_ASADMIN_WAIT" = "" ]; then
        #max wait, in seconds:
        MAX_ASADMIN_WAIT=90
    fi

    return 0
}

check_local_vars()
#This routine is largely obsolete.  however you can
#include checks for variables that must be set in the
#environment and are not checked earlier in set_global_vars()
{
    localvarerrs=0

    if [ $localvarerrs -ne 0 ]; then
        return 1
    fi

    return 0
}

clear_local_locks()
{
    #place-holder for now.
    echo ""
}

################################# HOUSEKEEPING #################################

cleanup()
{
    if [ x$TMPA != x ]; then
        rm -f $TMPA
    fi

    #remove the pid file if we were not called from wrapper:
    if [ $CALLEDFROMWRAPPER -eq 0 ]; then
        rm -f $RUNBLD_PIDFILE
    fi

}

rec_signal()
{
    cleanup
    bldmsg -error -p $p Interrupted
    bldmsg -markend -status 2 $p

    exit 2
}

################################### INTERNAL ###################################

update_repo()
# Usage:  eval update_repo [-D date_tag] [-r alpha_tag] cvsroot dest_dir repos_name co_target
# you must quote date arg if it has spaces.
{
    _datearg=""
    _tagarg=""
    _prunearg=""

    while [ $# -gt 0 -a "$1" != "" ]
    do
        arg=$1; shift

        case $arg in
        -D )
            _datearg="-D '$1'"
            shift
            ;;
        -r )
            _tagarg="-r '$1'"
            shift
            ;;
        -* )
            echo "update_repo: unknown option, $arg"
            return 1
            ;;
        * )
            break
            ;;
        esac
    done

    cvsroot=$arg        #already did shift in loop
    _dest_dir=$1; shift
    repo_name="$1"; shift
    co_target="$1"

#echo update_repos: cvsroot=$cvsroot _dest_dir=$_dest_dir repo_name=$repo_name co_target=$co_target _datearg=$_datearg _tagarg=$_tagarg

    installdir $SRCROOT/$_dest_dir
    ret=$?
    if [ $ret -ne 0 ]; then
        bldmsg -error -p $p/update_repo "cannot create $SRCROOT/$_dest_dir - aborting checkout of $_dest_dir."
        return $ret
    fi

    bldmsg -markbeg -p $p update $repo_name source

    if [ "$_datearg" = "" ]; then
        _prunearg="-P"
    else
        #Note that -D <date> implies -P, so we don't need it:
        _prunearg=""
    fi

    #bldmsg -warn "turning off CVS timestamp and prune args to update until jbi source tree is populated"
    #_prunearg=""
    #_datearg=""

    #hack to install top-level CVS dir
    if [ ! -r $SRCROOT/CVS/Root ]; then
        rm -rf  $SRCROOT/CVS
        rm -rf $SRCROOT/tmp/$CVS_SRCROOT_PREFIX

        mkdir -p $SRCROOT/tmp
        cd $SRCROOT/tmp


        bldmsg -mark "Create top-level CVS dir: `echo $cmd`"
        cmd="cvs $CVS_OPTS -d $cvsroot checkout -l -A $_prunearg $_tagarg $_datearg $CVS_SRCROOT_PREFIX"
        eval $cmd
        ret=$?

        if [ $ret -ne 0 ]; then
            bldmsg -error -p $p Could not checkout top-level CVS directory, $CVS_SRCROOT_PREFIX/CVS
            bldmsg -markend -p $p -status $ret update $repo_name source
            return $ret
        else
            mv $SRCROOT/tmp/$CVS_SRCROOT_PREFIX/CVS $SRCROOT
            rm -rf $SRCROOT/tmp/$CVS_SRCROOT_PREFIX
        fi
    fi

    cd $SRCROOT/$_dest_dir
    cwd=`pwd`

    # Use 'eval' to delay interpretation of embedded quotes:
    cmd="cvs $CVS_OPTS -d $cvsroot checkout -A $_prunearg $_tagarg $_datearg $co_target"
    bldmsg -mark "In $cwd: `echo $cmd`"
    eval $cmd
    ret=$?

    bldmsg -markend -p $p -status $ret update $repo_name source

    return $ret
}

get_product_services()
#return the list of services used in the product build (exclude tools services)
{
    if [ "$JBI_BRANCH_NAME" = "main" ]; then
        _branch=trunk
    else
        _branch="$JBI_BRANCH_NAME"
    fi

    #warning:  this routine will not expand module aliases
    echo `bld_get_cvs_module_defs $JBI_CVSROOT ${_branch}_build`
    return $?
}

set_update_time()
{
    export BLDTIME UCVSUPDATETIME UCVSUPDATETIMEDOT JBI_SNAPSHOT_TIME
    BLDTIME=""
    touch $SRCROOT/bldlock/bldenv.sh

    UCVSUPDATETIME=`bld_gmttime`

    if [ $RELEASE_BUILD -eq 1 ]; then
        if [ $I_AM_PRIMARY -eq 1 ]; then
            #always set the build time for primary
            if [ $DOUPDATE -eq 1 ]; then
                shprops -set $SRCROOT/bldlock/bldenv.sh LASTUPDATETIME=$UCVSUPDATETIME
            else
                tmpPRODUCT=jbi
                eval `shprops -get $LASTBLDPARMS ${tmpPRODUCT}_last_update`
                cmd="echo \$${tmpPRODUCT}_last_update"
                LOCAL_LASTUPDATETIME=`eval $cmd`
                shprops -set $SRCROOT/bldlock/bldenv.sh LASTUPDATETIME=$LOCAL_LASTUPDATETIME
            fi
        else
            if [ ! -f $PRIPATHREF/bldlock/bldenv.sh ]; then
                bldmsg -mark -error -p $p "RELEASE BUILD:  Cannot access \$PRIPATHREF/bldlock/bldenv.sh - has primary build fired? PRIPATHREF='$PRIPATHREF'"
                return 1
            fi

            eval `shprops -get $PRIPATHREF/bldlock/bldenv.sh LASTUPDATETIME`
            UCVSUPDATETIME=$LASTUPDATETIME
            echo "LASTUPDATETIME=${LASTUPDATETIME}" > $SRCROOT/bldlock/bldenv.sh
        fi
    fi

    UCVSUPDATETIMEDOT=`echo $UCVSUPDATETIME | sed "s/\(....\)\(..\)\(..\)\(..\)\(..\)\(..\)/\1.\2.\3.\4.\5.\6/"`
    BLDTIME=`bld_tocvstime $UCVSUPDATETIME`
    JBI_SNAPSHOT_TIME=`echo $UCVSUPDATETIME | perl -n -e '$xx=$_; printf "%s.%s-1", substr($xx,0,8), substr($xx,8,6);'`
    bldmsg "JBI_SNAPSHOT_TIME=$JBI_SNAPSHOT_TIME"
    return 0
}

update_src()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE=cvsupdate"

    # Print the time in both formats. *
    bldmsg -mark -p $p/update_src BLDTIME in CVS format: \"$BLDTIME\"

    #######
    # setup the source update:
    #######
    #default branch to env name if it is not mainline:
    if [ "$JBI_BRANCH_NAME" != "main" ]; then
        jbi_revarg="-r '$JBI_BRANCH_NAME'";
    else
        jbi_revarg="";
    fi

    #setup the date arg. default, which is BLDTIME for RE builds:
    if [ $RELEASE_BUILD -eq 1 ]; then
        jbi_datearg="-D '$BLDTIME'"
    else
        jbi_datearg=""
    fi

    srcdirs="`get_product_services`"

    #######
    # start the source update:
    #######
    if [ $DOCLEANSRC -eq 1 ]; then
        #also remove local bdb and release files:
        cleandirs="$srcdirs $SRCROOT/bdb $RELEASE_DISTROOT"

        bldmsg -mark -p $p/update_src "Removing source: $cleandirs"
        cd $SRCROOT/..
        bld_killall -bg $cleandirs
        if [ $? -ne 0 ]; then
            bldmsg -warn -p $p/update_src "had trouble removing source, continuing"
        fi
        cd $SRCROOT
    fi

    update_src_errs=0

    #note that all module checkouts for open-esb are relative to $SRCROOT/..
    eval update_repo $jbi_datearg $jbi_revarg $JBI_CVSROOT .. product "'$JBI_CO_TARGET'"
    c_status=$?

    if [ $c_status -eq 0 ]; then
        bldmsg -p $p/update_src -mark $PRODUCT source update SUCCESSFUL.

        bldmsg -mark -p $p "Setting jbi_last_update to '$UCVSUPDATETIME' in '$BLDPARMS'"
        shprops -set $BLDPARMS jbi_last_update=$UCVSUPDATETIME
    else
        update_src_errs=1
        bldmsg -p $p/update_src -error $PRODUCT FAILED with status $c_status - check log for errors.
        bldmsg -warn -p $p "Not setting jbi_last_update in '$BLDPARMS' because the CVS update failed"
    fi

    return $update_src_errs
}

do_fixcvsroot()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    srcdirs="`get_product_services`"

    bldmsg -mark -p $p/do_fixcvsroot "fixcvsroot -f $srcdirs CVSROOT"
    cd $SRCROOT
    fixcvsroot -f $srcdirs CVSROOT
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/do_fixcvsroot "FAILURE during fixcvsroot"
        return 1
    fi

    return 0
}

build_product()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE = ant_generic"

    cd $SRCROOT

    if [ $DOAPPSERVER_INSTALL -eq 1 ]; then
        #refresh the app-server installation, which we compile and test against:
        bldmsg -markbeg -p $p/build_product                  Install appserver
        installas8 $INSTALLAS_ARGS
        _status=$?
        bldmsg -markend -p $p/build_product -status $_status Install appserver
        if [ $_status -ne 0 ]; then
            bldmsg -error -p $p/build_product Appserver installation FAILED
            bld_fatal_error "CANNOT run installas8 successfully - ABORT"
        fi
    fi

    bldmsg "BUILDRESULTS_TYPE = jbi_javabuild"

    #build the jbi_compileconf jar:
    MAVEN_GOALS="-f platform-config/jbi-compileconf/pom.xml clean"
    cmd="mvn $MAVEN_OPTIONS -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -Dmaven.test.skip=true -DBUILD_NUMBER=$BLDNUM $MAVEN_GOALS"
    bldmsg -mark -p $p/build_product `echo $cmd`

    eval $cmd
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/build_product FAILED while building jbi_compileconf jar
        bld_fatal_error "CANNOT build jbi_compileconf jar - ABORT"
    fi

    MAVEN_GOALS="install"
    cmd="mvn $MAVEN_OPTIONS -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -Dmaven.test.skip=true -DBUILD_NUMBER=$BLDNUM $MAVEN_GOALS"
    bldmsg -mark -p $p/build_product `echo $cmd`

    eval $cmd
    return $?
}

build_xref()
#run xrefactory xref.  see also build_site().
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE=ant_generic"

    cd $SRCROOT/antbld

    _save_antopts="$ANT_OPTS"

    #this job is running out of memory on solsparc.
    export ANT_OPTS
    ANT_OPTS="-Xms512m -Xmx512m"

    #note - xref_only target assumes that javadoc is already built.
    cmd="ant -emacs $VERBOSE_ARG -Dbuildnumber=$BLDNUM -Dbranch=$JBI_BRANCH_NAME $ANT_PROPS -f xref.ant xref archive_doc"
    bldmsg -mark -p $p/build_xref `echo $cmd`

    eval $cmd
    _status=$?

    ANT_OPTS="$_save_antopts"

    return $_status
}

build_javadoc()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE = jbi_javabuild"

    cd $SRCROOT/build-common
    cmd="ant -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -f m2.ant openesb-javadoc"

    eval $cmd
    return $?
}

build_site()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE = jbi_javabuild"

    cd $SRCROOT
    cmd="mvn -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -DBUILD_NUMBER=$BLDNUM site:site"
    bldmsg -mark -p $p/build_site `echo $cmd`

    eval $cmd
    return $?
}

archive_doc()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE=general"

    bldmsg -mark -p $p/archive_doc creating $ARCHIVE_JAVADOC

    TMPDIR=$SRCROOT/javadoc.$$
    cp -rp $JAVADOC_BASE $TMPDIR
    status=$?

    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/archive_doc "failed to copy $JAVADOC_BASE -> $SRCROOT/javadoc.$$"
        return 1
    fi

    if [ -d $ARCHIVE_JAVADOC ]; then
        rm -rf ${ARCHIVE_JAVADOC}.old
        mv $ARCHIVE_JAVADOC ${ARCHIVE_JAVADOC}.old
        mv $TMPDIR $ARCHIVE_JAVADOC
        rm -rf ${ARCHIVE_JAVADOC}.old
    else
        #first time, make sure parent dir exists:
        installdir $ARCHIVE_JAVADOC
        rm -rf $ARCHIVE_JAVADOC
        mv $TMPDIR $ARCHIVE_JAVADOC
    fi

    return 0
}

archive_logs()
{
    _status=0

    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE=ant_generic"

    cd $SRCROOT
    installdir $ANTLOGDIR

    cmd="ant $VERBOSE_ARG -Dregress_product=$REGRESS_PRODUCT -Dlog.dir.name=$ANTLOGDIR -Dfailonerror=false -Dbuildnumber=$BLDNUM -Dbranch=$JBI_BRANCH_NAME $ANT_PROPS archive_logs"
    cmd="echo archive_logs"
    bldmsg -mark -p $p/archive_logs `echo $cmd`

    eval $cmd
    [ $? -ne 0 ] && _status=1

    #now scan the logs for java exception traces:
    cmd="ant $VERBOSE_ARG -Dregress_product=$REGRESS_PRODUCT -Dlog.dir.name=$ANTLOGDIR -Dfailonerror=false -Dbuildnumber=$BLDNUM -Dbranch=$JBI_BRANCH_NAME $ANT_PROPS jxtohtml"
    cmd="echo jxtohtml"
    bldmsg -mark -p $p/archive_logs `echo $cmd`
    eval $cmd

    [ $? -ne 0 ] && _status=1

    #if we created any _X* files (html logs with hyperlinked exception traces),
    #then find them and collect them in a top-level index file:

    make_exception_index

    return $_status
}

make_exception_index()
{
    bldmsg -mark -p $p "Scanning log files for Java exceptions..."
    rm -f $EXCEPTION_INDEX

    #mks shell has bug - redirect of stdout doesn't work inside paren's:
    (cd $LOGDIR; find . -name '_X*' -print) > $TMPA

    #if find command produced content...
    if [ -s $TMPA ]; then
        #found exceptions - create index:
        bldmsg -warn -p $p "Java exceptions found in logs.  See $EXCEPTION_INDEX."
        cat << EOF > $EXCEPTION_INDEX
<h3>
THE FOLLOWING LOG FILES CONTAIN JAVA EXCEPTION TRACES:
<h4>
EOF
        #OUT:   <A HREF="blah"> blah</A>
        sed -e 's|\(.*\)_X\(.*\)\.html$|<br>\&nbsp; \&nbsp; <A HREF="&">\1\2</A>|' $TMPA >> $EXCEPTION_INDEX
    else
        #no exceptions - good!
        bldmsg -mark -p $p "No Java exceptions found in logs."
    fi

    return 0
}

clean_maven()
{
  rm -rf "$SRCROOT/m2"
  return $?
}


clean_build_tree()
#NOTE:  maven always loads dependencies prior to running a target.
#Therefore, we have to do a clean:build stepwise for each sub-project.
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE = ant_generic"

    cd $SRCROOT

    bldmsg "BUILDRESULTS_TYPE = jbi_javabuild"

    MAVEN_GOALS="clean"

    cmd="mvn -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -Dmaven.test.skip=true -DBUILD_NUMBER=$BLDNUM $MAVEN_GOALS"
    bldmsg -mark -p $p/clean_build_tree `echo $cmd`

    eval $cmd
    return $?
}

wait_for_remote_kit()
#wait for the primary build to finish creating the
#make-release kits.
{
    #reset the watchdog timer before/after ($RUNBLD_TIMEOUT is the watchdog timeout in seconds).
    bld_reset_watchdog

    #wait for jar files to be ready...
    bldwait -max $MAX_KIT_WAIT -p $p -marktime $TESTRELEASEREADY
    wait_status=$?

    bld_reset_watchdog

    if [ $wait_status -eq 2 ]; then
        #exceeded max time out:
        bldmsg -error -p $p/wait_for_remote_kit "timed out waiting for $TESTRELEASEREADY"
        return 1
    fi

    return 0   #success
}

map_regress_port()
#
# Usage: realport=`map_regress_port $kitport`
#
#NOTE:  this is to make sure <FORTE_PORT> macros in the boms are analyzed
#       correctly.
{
    if [ "$1" = "" ]; then
        bldmsg -error -p $p/map_regress_port "Usage: map_regress_port regress_port_name" 1>&2
        echo "NULL_PORT"
        return 1
    fi

    case $1 in
    redhat )
        echo linux
        ;;
    sollassen )
        echo solsparc
        ;;
    nt5 )
        echo nt
        ;;
    xp )
        echo nt
        ;;
    * )
        echo $1
        ;;
    esac

    return 0
}

map_caps_port()
#
# Usage: kitport=`map_caps_port $gfport`
#
{
    if [ "$1" = "" ]; then
        bldmsg -error -p $p/map_caps_port "Usage: map_caps_port gf_port" 1>&2
        echo "NULL_PORT"
        return 1
    fi

    case $1 in
    AIX )
        echo aix
        ;;
    Darwin )
        echo macos
        ;;
    SunOS_X86 )
        echo solaris-x86
        ;;
    Linux )
        echo linux
        ;;
    SunOS )
        echo solaris-sparc
        ;;
    WINNT )
        echo windows
        ;;
    Windows )
        echo windows
        ;;
    * )
        echo $1
        ;;
    esac

    return 0
}

map_glassfish_port()
#
# Usage: kitport=`map_glassfish_port $gfport`
#
{
    if [ "$1" = "" ]; then
        bldmsg -error -p $p/map_glassfish_port "Usage: map_glassfish_port gf_port" 1>&2
        echo "NULL_PORT"
        return 1
    fi

    case $1 in
    AIX )
        echo rs6000
        ;;
    Darwin )
        echo macosx
        ;;
    SunOS_X86 )
        echo solx86
        ;;
    Linux )
        echo linux
        ;;
    SunOS )
        echo solsparc
        ;;
    WINNT )
        echo nt5
        ;;
    Windows )
        echo nt5
        ;;
    * )
        echo $1
        ;;
    esac

    return 0
}

generate_boms()
#generate boms for various components.
#boms are saved to $SRCROOT/rl/bld, which is where they need to
#be referenced from in other boms.
{

    _genbomerrs=0

    bldmsg -mark -p $p/generate_boms Generating boms for javadoc

    #########
    #Gen boms
    #########

    #Commented out due to changes in the locations of the javadoc. KH 11/11/05

    installdir $LASSEN_JAVADOC/public
    cd $LASSEN_JAVADOC/public
    bldmsg "makebom  -prefixSrc <LASSEN_JAVADOC>/public -prefixDst doc/ * > $LASSEN_JAVADOC/public_javadoc.bom"
    makebom  -prefixSrc "<LASSEN_JAVADOC>/public" -prefixDst doc/ * > $LASSEN_JAVADOC/public_javadoc.bom
    [ $? -ne 0 ] && _genbomerrs=1

    installdir $LASSEN_JAVADOC/private
    cd $LASSEN_JAVADOC/private
    bldmsg "makebom  -prefixSrc <LASSEN_JAVADOC>/private -prefixDst doc/ * > $LASSEN_JAVADOC/private_javadoc.bom"
    makebom  -prefixSrc "<LASSEN_JAVADOC>/private" -prefixDst doc/ * > $LASSEN_JAVADOC/private_javadoc.bom
    [ $? -ne 0 ] && _genbomerrs=1

    #make sure dir exist; otherwise shell will bomb:
    installdir $WHITNEY_JAVADOC/private
    cd $WHITNEY_JAVADOC/private
    bldmsg "makebom  -prefixSrc <WHITNEY_JAVADOC>/private -prefixDst doc/ * > $WHITNEY_JAVADOC/private_javadoc.bom"
    makebom  -prefixSrc "<WHITNEY_JAVADOC>/private" -prefixDst doc/ * > $WHITNEY_JAVADOC/private_javadoc.bom
    [ $? -ne 0 ] && _genbomerrs=1

    installdir $SHASTA_JAVADOC/private
    cd $SHASTA_JAVADOC/private
    bldmsg "makebom  -prefixSrc <SHASTA_JAVADOC>/private -prefixDst doc/ * > $SHASTA_JAVADOC/private_javadoc.bom"
    makebom  -prefixSrc "<SHASTA_JAVADOC>/private" -prefixDst doc/ * > $SHASTA_JAVADOC/private_javadoc.bom
    [ $? -ne 0 ] && _genbomerrs=1

    installdir $SHASTA_JAVADOC/public
    cd $SHASTA_JAVADOC/public
    bldmsg "makebom  -prefixSrc <SHASTA_JAVADOC>/public -prefixDst doc/ * > $SHASTA_JAVADOC/public_javadoc.bom"
    makebom  -prefixSrc "<SHASTA_JAVADOC>/public" -prefixDst doc/ * > $SHASTA_JAVADOC/public_javadoc.bom
    [ $? -ne 0 ] && _genbomerrs=1

    return $_genbomerrs
}

make_release()
#Example settings:
# KITREV=main
# BLDNUM=001114
#this routine is normally only called on primary port
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    kiterrs=0

    bldmsg "BUILDRESULTS_TYPE=general"

    ######
    #build release dir:
    ######
    makedrv -auto -q -c bdb
    makedrv -auto -q -b jbiroot.cmn -c mmf

    #########
    #generate boms:
    #########
    generate_boms
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_release "generate boms step FAILED."
        kiterrs=1
    fi

    kitbase=$KIT_DISTROOT/$KIT_REV
    bldnum=Build$BLDNUM

    if [ ! -d $kitbase ]; then
        installdir -m 0775 $kitbase
    fi


    #######
    #release each port on primary build machines...
    #######
    if [ $I_AM_PRIMARY -eq 1 ]; then
        kitports="`bldhost -a -kitports -port`"
    else
        kitports=$REGRESS_FORTE_PORT
    fi

    #if we are a shasta build, we release all installers:
    akrelarg=
    if [ $REGRESS_PRODUCT = shasta ]; then
       if [ $DOALASKA -eq 1 ]; then
          products="shasta kenai jbise"
          akrelarg="INCLUDE_ALASKA_COMPS=1"
       else
          products="shasta jbise"
       fi
    fi

    for kitport in $kitports
    do
        #we need <KIT_PORT> defined in boms in order to pick up kits from related products.
        #RT 6/11/07.
        export KIT_PORT
        KIT_PORT="$kitport"

        realport=`map_regress_port $kitport`

        for prod in $products
        do
            kitdir=$kitbase/$bldnum/$kitport/$prod
            bom=$prod.bom

            bldmsg -mark -p $p/make_release Release $bom to $kitdir

            release $akrelarg -bomloc $SRCROOT/rl/src/cmn/bom -ver $BLDNUM -nolog -checksum -w $kitdir -port $realport $bom
            status=$?

            if [ $status -ne 0 ]; then
                bldmsg -error -p $p/make_release Release of $bom to $KIT_DISTROOT FAILED
                kiterrs=1
            fi
        done
    done


    if [ $kiterrs -eq 0 ]; then
        #put the bldnum in the release.ready file
        bldmsg -mark "Creating $SETRELEASEREADY file."
        echo $bldnum > $SETRELEASEREADY
    fi


    ######
    # copy cvs_update.log to under SRCROOT/bld
    ######
    relstage=$SRCROOT/bld/relstage
    rm -rf $relstage
    mkdir -p $relstage
    if [ -r $UPDATELOG ]; then
        cmd="cp $UPDATELOG ${relstage}"
        bldmsg -mark -p $p/make_release $cmd
        eval $cmd
        if [ $? -ne 0 ]; then
            bldmsg -error -p $p/make_release "'$cmd' FAILED"
            kiterrs=1
        fi
    else
        echo "cvs -update was not run" > ${relstage}/cvs_update.log
        if [ $? -ne 0 ]; then
           bldmsg -error -p $p/make_release "fake cvs update creation FAILED"
           kiterrs=1
        fi
    fi

    ##### 
    # make gfesb_download here
    ##### 
    _bld="$SRCROOT/bld/relstage"
    export FULL_VERSION SHORT_PRODUCT_NAME
    eval `bldhost -product shasta -versionproperties | egrep '^FULL_VERSION=|^SHORT_PRODUCT_NAME=' `

    bldmsg -mark -p $p/make_release create gfesb_download file
    gfesbfile="${_bld}/gfesb_download"
    rm -f "$gfesbfile"; touch -f "$gfesbfile"

    #output gfesbfile
    cat << EOF_GFESB >> "$gfesbfile"
OPENESB_BUILD_NUMBER="${BLDNUM}"
OPENESB_CODELINE="${CODELINE}"
OPENESB_TIMESTAMP="${UCVSUPDATETIMEDOT}"
OPENESB_FULL_VERSION="${FULL_VERSION}"
OPENESB_FULL_PRODUCT_NAME="Open Enterprise Service Bus"
jbi-core-installer.jar=${GFESB_KITROOT}/Build${BLDNUM}/CORE/jbi-core-installer.jar
jbi-javase.jar=${GFESB_KITROOT}/Build${BLDNUM}/CORE/jbise/jbi-javase.jar
EOF_GFESB


    ######
    #generate index.html file
    ######
    exstagedir=$SRCROOT/antbld/bld/external
    installdir $exstagedir
    indexfile="${exstagedir}/index.html"
    rm -f "$indexfile"; touch -f "$indexfile"

cat << INDEX_EOF >> "$indexfile"
<html>
<head>
<title>OESB Build : ${bldnum}</title>
</head>
<body>
<h3>OESB Build ${bldnum}</h3>
Before installing this build of open-esb, you need to make sure that you have Project Glassfish $GLASSFISH_BUILD installed on your machine.
<br>
Project Glassfish $GLASSFISH_BUILD  can be downloaded here: <a href="$GLASSFISH_KIT_URL">$GLASSFISH_KIT_URL</a>
<br><br>
Open ESB downloads from ${bldnum}:
<ul>
<li><a href="CORE/jbi-core-installer.jar">jbi-core-installer.jar</a>
<li><a href="../../common/install-jbi-runtime-jar.html">view the JBI core runtime installation instructions</a>
<li><a href="jbise/jbi-javase.jar">jbi-javase.jar</a>
<li><a href="../../common/3rdPartyLicenses.html">view the 3rd party license agreeement</a>
</ul>
<br>
<ul>
<li><a href="appservers/websphere/openesb-websphere.war">Websphere web app</a>
<li><a href="appservers/websphere/openesb-websphere-client.jar">Websphere client jar</a>
<li><a href="appservers/jboss/openesb-jboss-1.1-distribution.zip">JBoss zip </a>
<li><a href="appservers/jboss/openesb-jboss-1.1-distribution.tar.gz">JBoss tar file</a>
</ul>
<br>
<br>
<a href="cvs_update.log">CVS update log for this build</a><br>
<a href="jbi.ver">version file for this build</a><br>
<a href="gfesb_download">gfesb_download file for this build</a><br>
</body>
</html>
INDEX_EOF

    # release index.html here
    bom=external.bom
    blddir=$kitbase/$bldnum/external
    bldmsg -mark -p $p/make_release Release $bom to $blddir
    release -bomloc $SRCROOT/rl/src/cmn/bom -nolog -checksum -w $blddir $bom
    status=$?
    if [ $status -ne 0 ]; then
      bldmsg -error -p $p/make_release:  Release of $bom to $blddir FAILED
      kiterrs=1
    fi


    return $kiterrs
}

repackage_glassfish()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    repackage_glassfish_errs=0

    _image="glassfish-image-$GLASSFISH_BUILD.jar"
    _bld="$SRCROOT/bld/glassfish_repackage"
    _kitbase="$KIT_DISTROOT/$KIT_REV/Build$BLDNUM"

    # This command sets version variables loaded from codeline.pl that are needed to write GF property file
    export FULL_VERSION_UL SHORT_PRODUCT_NAME
    eval `bldhost -product shasta -versionproperties | egrep '^FULL_VERSION_UL=|^SHORT_PRODUCT_NAME=' `

    rm -rf "$_bld"
    mkdir -p "$_bld"

    bldmsg -markbeg staging of glassfish $GLASSFISH_BUILD
    for _gfplatform in $GLASSFISH_PLATFORMS
    do
        _jbiplatform=`map_glassfish_port $_gfplatform`

        if [ -r "$GLASSFISH_STAGEDIR/$_gfplatform/$_image" ]; then
            bldmsg -mark using cached copy of $GLASSFISH_STAGEDIR/$_gfplatform/$_image
        else
            mkdir -p "$GLASSFISH_STAGEDIR/$_gfplatform"
            cd "$GLASSFISH_STAGEDIR/$_gfplatform"
            if [ $? -ne 0 ]; then
                bldmsg -error -p $p/repackage_glassfish cannot cd to $GLASSFISH_STAGEDIR/$_gfplatform
                repackage_glassfish_errs=1
                continue
            fi

            bldmsg -mark wget -nv "$GLASSFISH_KIT_URL/$_gfplatform/$_image"
            wget -nv "$GLASSFISH_KIT_URL/$_gfplatform/$_image"
            if [ $? -ne 0 ]; then
                bldmsg -error -p $p/repackage_glassfish wget FAILED for "$GLASSFISH_KIT_URL/$_gfplatform/$_image"
                repackage_glassfish_errs=1
                continue
            fi
        fi

        mkdir -p "$_bld/$_jbiplatform/orig"
        if [ $? -ne 0 ]; then
            bldmsg -error -p $p/repackage_glassfish mkdir FAILED for "$_bld/$_jbiplatform/orig"
            repackage_glassfish_errs=1
            continue
        fi

        cd "$_bld/$_jbiplatform/orig"
        bldmsg -mark unzip -X -q "$GLASSFISH_STAGEDIR/$_gfplatform/$_image"
        unzip -X -q "$GLASSFISH_STAGEDIR/$_gfplatform/$_image"
        if [ $? -ne 0 ]; then
            bldmsg -error -p $p/repackage_glassfish FAILED: unzip -X -q "$GLASSFISH_STAGEDIR/$_gfplatform/$_image"
            repackage_glassfish_errs=1
            continue
        fi

        #kitdir:  $KIT_DISTROOT/rt*/latest/solx86/kenai
        mv jbi jbi.orig
        _kitsrc="$_kitbase/$_jbiplatform/kenai"
        cp -rp "$_kitsrc" jbi
        if [ $? -ne 0 ]; then
            bldmsg -error -p $p/repackage_glassfish FAILED: cp -rp "$_kitsrc" jbi
            repackage_glassfish_errs=1
            continue
        fi

        cp -rp  jbi.orig/components/sun-javaee-engine jbi/components
        rm -rf jbi.orig

        #patch admin gui:
        bldmsg -mark -p $p/repackage_glassfish patch_admin_gui
        patch_admin_gui "$_bld/$_jbiplatform/orig"
        if [ $? -ne 0 ]; then
            bldmsg -error -p $p/repackage_glassfish patch_admin_gui FAILED
            repackage_glassfish_errs=1
            continue
        fi

        #zip up the new kit:
        bldmsg -mark -p $p/repackage_glassfish repackage kit
        zip -rq ../glassfish.zip *
        if [ $? -ne 0 ]; then
            bldmsg -error -p $p/repackage_glassfish 'FAILED: zip -rq ../glassfish.zip *'
            repackage_glassfish_errs=1
            continue
        fi
    done

    #create repackage.properties file
    if [ $repackage_glassfish_errs -eq 0 ]; then
        bldmsg -mark -p $p/repackage_glassfish create repackage.properties file
        gfpropfile="${_bld}/repackage.properties"
        rm -f "$gfpropfile"; touch -f "$gfpropfile"

        echo "GLASSFISH_BUILD=${GLASSFISH_BUILD}" >> "$gfpropfile"
        echo "GLASSFISH_VERSION=${GLASSFISH_VERSION}" >> "$gfpropfile"
        echo "OPENESB_BUILD=${BLDNUM}" >> "$gfpropfile"
        echo "OPENESB_VERSION=${FULL_VERSION_UL}" >> "$gfpropfile"

        for _gfplatform in $GLASSFISH_PLATFORMS
        do
            _jbiplatform=`map_glassfish_port $_gfplatform`
            _capsplatform=`map_caps_port $_gfplatform`
            echo "glassfish-image-SNAPSHOT-${_capsplatform}.jar=${KITROOT_SMB}/openesb/${CODELINE}/${bldnum}/glassfish/${_jbiplatform}/glassfish.zip" >> "$gfpropfile"
        done
    fi

    #release the new packages if no errors:
    bldmsg -mark -p $p/repackage_glassfish release new packages
    if [ $repackage_glassfish_errs -eq 0 ]; then
        release -w "$_kitbase" -bomloc $SRCROOT/rl/src/cmn/bom glassfish_repackage.bom
        if [ $? -ne 0 ]; then
            bldmsg -error -p $p/repackage_glassfish release of glassfish_repackage.bom FAILED
            repackage_glassfish_errs=1
        fi
    else
        bldmsg -error -p $p/repackage_glassfish skipping release step because there were errors during repackaging
    fi

    bldmsg -markend staging of glassfish $GLASSFISH_BUILD
    return $repackage_glassfish_errs
}

patch_admin_gui()
#run script to update glassfish admin application
#usage:  patch_admin_gui <gf_install_base>
{
    patch_admin_gui_errs=0

    #patch is no longer necessary as of v2ur1-b06.  RT 10/27/07
    return 0

    if [ "$1" = "" ]; then
        bldmsg -error -p $p/patch_admin_gui "Usage: patch_admin_gui gf_install_root" 1>&2
        return 1
    fi

    _savecwd=`pwd`
    _saveas8base="$AS8BASE"
    export AS8BASE
    AS8BASE="$1"

    cd $SRCROOT/ri-clients/jbi-admin-gui/regress
    2>&1 sh jbi-admin-gui00001.ksh | grep -v JBITest/config/domain.xml

    #restore AS8BASE:
    AS8BASE="$_saveas8base"
    cd "$_savecwd"

    return $patch_admin_gui_errs
}

make_qecomponents()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    make_qecomponents_errs=0

    stagedir=$SRCROOT/antbld/bld/qecomponents
    installdir $stagedir

    kitbase=$KIT_DISTROOT/$KIT_REV
    bldnum=Build$BLDNUM
    if [ $I_AM_PRIMARY -eq 1 ]; then
        kitport=$PRIMARY_PORT
    else
        kitport=$REGRESS_FORTE_PORT
    fi

    ####
    #create components.zip
    ####
    bldmsg -mark -p $p/make_qecomponents create components.zip
    qesourcekitdir=$kitbase/$bldnum/$kitport/shasta/components
    if [ -r $qesourcekitdir ]; then
      cd $qesourcekitdir/..
      jar -Mcf $stagedir/components.zip components
      status=?
      if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_qecomponents creation of components.zip FAILED
        make_qecomponents_errs=1
      fi
    else
        bldmsg -error -p $p/make_qecomponents creation of components.zip FAILED
        bldmsg -error -p $p/make_qecomponents this directory does not exist : $qesourcekitdir
        make_qecomponents_errs=1
    fi


    ########
    #release components.zip
    ########
    bom=qecomponents.bom
    qecomponentsdir=$kitbase/$bldnum/INTERNAL
    bldmsg -mark -p $p/make_qecomponents Release $bom to $qecomponentsdir
    release -bomloc $SRCROOT/rl/src/cmn/bom -ver $image_num -nolog -checksum -w $qecomponentsdir -port $kitport $bom
    status=$?
    if [ $status -ne 0 ]; then
      bldmsg -error -p $p/make_sdk_image Release of $bom to $qecomponentsdir FAILED
      make_qecomponents_errs=1
    fi

    return $make_qecomponents_errs
}


make_installer()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    make_installer_errs=0

    kitbase=$KIT_DISTROOT/$KIT_REV
    if [ $I_AM_PRIMARY -eq 1 ]; then
        kitports="`bldhost -a -kitports -port`"
        kitdistport=$PRIMARY_PORT
    else
        kitdistport=$REGRESS_FORTE_PORT
        kitports="`bldhost -a -kitports -port`"
    fi

    #primary port builds all installers:
    if [ $I_AM_PRIMARY -eq 1 ]; then
        kitports="`bldhost -a -kitports -port`"
        kitport=$PRIMARY_PORT
    else
        kitports=$REGRESS_FORTE_PORT
    fi

    products=$REGRESS_PRODUCT

    #remove the installer build area:
    bldmsg -p $p/make_installer -mark removing shasta installer build area in $SRCROOT/installers/gui/bld
    rm -rf $SRCROOT/installers/gui/bld

    bundle_status=0

    # CURRENTLY NOT RELEASING IN CDROM or BUNDLE FORMAT
    #for kitport in $kitports
    #do
    #    for prod in $products
    #    do
    #        kitdir=$kitbase/$bldnum/$kitport/$prod
    #        cdromdir=$kitbase/$bldnum/CDROM/$kitport/$prod
    #        bundledir=$kitbase/$bldnum/BUNDLES/$kitport/$prod
    #        packagedir=$kitbase/$bldnum/NATIVE-PACKAGES/$kitport/$prod
    #
    #        make_gui_installer "$prod" "$kitport" "$kitdir" "$cdromdir"
    #        if [ $? -ne 0 ]; then
    #            bldmsg -error -p $p/make_installer make_gui_installer FAILED for $kitport
    #            make_installer_errs=1
    #        fi
    #
    #        make_bundle "$prod" "$kitport" "$kitdir" "$cdromdir" "$bundledir"
    #        if [ $? -ne 0 ]; then
    #            bldmsg -error -p $p/make_installer make_bundle FAILED for $kitport
    #            make_installer_errs=1
    #        fi
    #
    #        if [ "$kitport" = "solsparc"  -o "$kitports" = "$REGRESS_FORTE_PORT" ]; then
    #          #make_mavenarchive "$prod" "$kitport" "$kitdir" "$cdromdir" "$bundledir"
    #          make_mavenarchive "$prod" "$kitport" "$kitdir"
    #          if [ $? -ne 0 ]; then
    #            bldmsg -error -p $p/make_installer make_mavenarchive FAILED
    #            make_installer_errs=1
    #          fi
    #        fi
    #    done
    #done

    # Jar the kenai make release kit
    kenaikitdir=$kitbase/$bldnum/$REGRESS_FORTE_PORT/kenai
    kenaidir=$kitbase/$bldnum/KENAI
    make_kenai "kenai" "$kitdistport" "$kenaikitdir" "$kenaidir"
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/make_installer make_kenai FAILED
        make_installer_errs=1
    fi

    # Jar the jbi standalone release kit
    bldmsg -p $p/make_installer -mark creating jbi-javase.jar
    jbisekitdir=$kitbase/$bldnum/$REGRESS_FORTE_PORT/jbise
    jbisedir=$kitbase/$bldnum/jbise

    installdir $jbisedir
    cd $jbisekitdir/..
    jar -Mcf $jbisedir/jbi-javase.jar jbise
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/make_installer make jbise FAILED
        make_installer_errs=1
    fi

    # make and release the jbi-core-installer.jar and jcaps_download
    # make jbi-core-installer.jar here
    cd $SRCROOT/installers/core
    cmd="mvn $MAVEN_OPTIONS -Dmaven.test.skip=true -Dmaven.repo.local=$JV_SRCROOT/m2/repository"
    bldmsg -mark -p $p/make_installer `echo $cmd`
    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
      bldmsg -error -p $p/make_installer make jbi-core-installer.jar FAILED
      make_installer_errs=1
    fi

    # make jcaps_download here
    if [ $make_installer_errs -eq 0 ]; then
      _bld="$SRCROOT/bld/jcaps_download"
      export FULL_VERSION SHORT_PRODUCT_NAME
      eval `bldhost -product shasta -versionproperties | egrep '^FULL_VERSION=|^SHORT_PRODUCT_NAME=' `
      rm -rf "$_bld"
      mkdir -p "$_bld"

        bldmsg -mark -p $p/make_installer create jcaps_download file
        jcapspropfile="${_bld}/jcaps_download"
        rm -f "$jcapspropfile"; touch -f "$jcapspropfile"

        echo "component.build.save.dir=${KITROOT_SMB}/openesb/${CODELINE}/${bldnum}/CORE" >> "$jcapspropfile"
        echo "OPENESB_BUILD=${BLDNUM}" >> "$jcapspropfile"
        echo "OPENESB_VERSION=${FULL_VERSION}" >> "$jcapspropfile"
        echo "jbi-core-installer.jar=${KITROOT_SMB}/openesb/${CODELINE}/${bldnum}/CORE/jbi-core-installer.jar" >> "$jcapspropfile"
    fi



    # release them here
    bom=core.bom
    coredir=$kitbase/$bldnum/CORE
    bldmsg -mark -p $p/make_installer Release $bom to $coredir
    release -bomloc $SRCROOT/rl/src/cmn/bom -nolog -checksum -w $coredir $bom
    status=$?
    if [ $status -ne 0 ]; then
      bldmsg -error -p $p/make_installer Release of $bom to $coredir FAILED
      make_installer_errs=1
    fi

    # release the websphere components
    bom=appservers.bom
    appserversdir=$kitbase/$bldnum/appservers
    bldmsg -mark -p $p/make_installer Release $bom to $appserversdir
    release -bomloc $SRCROOT/rl/src/cmn/bom -nolog -checksum -w $appserversdir $bom
    status=$?
    if [ $status -ne 0 ]; then
      bldmsg -error -p $p/make_installer Release of $bom to $appserversdir FAILED
      make_installer_errs=1
    fi

    # save off the maven components used in the build
    kitdir=$kitbase/$bldnum/$kitdistport/shasta
    bundledir=$kitbase/$bldnum/BUNDLES/$kitdistport/shasta
    make_mavenarchive "shasta" "$kitdistport" "$kitdir"
    if [ $? -ne 0 ]; then
       bldmsg -error -p $p/make_installer make_mavenarchive FAILED
       make_installer_errs=1
    fi

    # create the JAVA_EE5_SDK_INSTALLER directory for QE to test with
    sdkinstalldir=$kitbase/$bldnum/JAVA_EE5_SDK_INSTALLER
    cmd="mkdir -p $sdkinstalldir"
    bldmsg -mark -p $p/make_installer `echo $cmd`
    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
      bldmsg -error -p $p/make_installer FAILED during the mkdir $sdkinstalldir command
      make_installer_errs=1
    fi

    #write the bldnumfile
    bldnumfile=$kitbase/.bldnum
    if [ ! -f $bldnumfile ]; then
        touch $bldnumfile
    fi
    ##note the build number, status, etc in top level dir:
    touch -f "$bldnumfile" && chmod +w "$bldnumfile"
    if [ $? -eq 0 ]; then
        tmp1=`date '+%Y%m%d%H%M%S'`
        tmp2=`logname`
        echo $bldnum $make_installer_errs $tmp1 $tmp2 >> "$bldnumfile"
        bldmsg -mark Updated bldnum file, $bldnumfile
    else
        bldmsg -error -p $p/make_installer "Failed to update bldnum file, '$bldnumfile'."
        make_installer_errs=1
    fi

    # create trigger files for JES and JBOSS creation to begin
    bldmsg -mark "Creating $READYFORJES file."
    echo $bldnum > $READYFORJES
    bldmsg -mark "Creating $READYFORJBOSS file."
    echo $bldnum > $READYFORJBOSS

    # Create latest symlink in kits directory
    cmd="rm ${kitbase}/.previouslatest"
    bldmsg -mark removing .previouslatest symlink - `echo $cmd`
    eval $cmd
    cmd="mv ${kitbase}/latest ${kitbase}/.previouslatest"
    bldmsg -mark moving latest symlink to .previouslatest symlink - `echo $cmd`
    eval $cmd
    cmd="ln -s ${kitbase}/${bldnum} ${kitbase}/latest"
    bldmsg -mark adding new latest symlink - `echo $cmd`
    eval $cmd

    return $make_installer_errs
}


make_kenai()
#make_kenai "$prod" "$kitdistport" "$kitdir" "$kenaidir"
#note - $kitdistport is the port we are getting the cannonical kit from.
{
    product="$1"  #kenai
    kitport="$2"  #$kitdistport
    kitdir="$3"   #/kits/.../Build.../$kitdistport/kenai
    kenaidir="$4" #/kits/.../Build.../KENAI/$kitdistport

    make_kenai_errs=0

    bldmsg -mark -p $p/make_kenai creating stagedir : $stagedir
    stagedir=$SRCROOT/antbld/bld/$product
    rm -rf $stagedir
    installdir ${stagedir}
    cd $stagedir

    bldmsg -mark -p $p/make_kenai copying conents of $kitdir to $stagedir/jbi
    cp -rp ${kitdir} $stagedir/jbi
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/make_kenai cp -rp ${kitdir} $stagedir/jbi FAILED in `pwd`
        make_kenai_errs=1
        return $make_kenai_errs
    fi

    bldmsg -mark -p $p/make_kenai creating jbi-install.jar
    jar -Mcf $stagedir/jbi-install.jar jbi
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/make_kenai jar -Mcf $stagedir/jbi-install.jar jbi FAILED in `pwd`
        make_kenai_errs=1
        return $make_kenai_errs
    fi

    bldmsg -mark -p $p/make_kenai creating jbi-install-${BLDNUM}.jar
    jar -Mcf $stagedir/jbi-install-${BLDNUM}.jar jbi-install.jar
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/make_kenai jar -Mcf $stagedir/jbi-install-${BLDNUM}.jar jbi-install.jar FAILED in `pwd`
        make_kenai_errs=1
        return $make_kenai_errs
    fi

    bldmsg -mark -p $p/make_kenai Release $bom to $kenaidir

    bom=${product}_bundle.bom
    release -bomloc $SRCROOT/rl/src/cmn/bom -ver $BLDNUM -nolog -checksum -w $kenaidir -port $kitport $bom
    status=$?
    if [ $status -ne 0 ]; then
      bldmsg -error -p $p/make_kenai release -bomloc $SRCROOT/rl/src/cmn/bom -ver $BLDNUM -nolog -checksum -w $kenaidir -port $kitport $bom FAILED.
      make_kenai_errs=1
    fi

    return $make_kenai_errs
}


push_maven()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    push_maven_errs=0

    cd $SRCROOT

    deploy_defs="deploy:deploy-file -Durl=file://${PUSHMAVEN_REMOTE_REPOS} -Dpackaging=jar -DuniqueVersion=false" 

    maven_groupId="open-esb"
    maven_artifactId="esb-jbi-ext"
    maven_version="${PUSHMAVEN_VERSION}"
    maven_file="${JV_SRCROOT}/esb-packages/jbi-ext/bld/esb-jbi-ext-1.1.jar"
    maven_pom="${JV_SRCROOT}/esb-packages/jbi-ext/src/resource/deploy/release_pom.xml"

    
    cmd2="mvn $deploy_defs -DSRCROOT=$JV_SRCROOT -Dmaven.repo.local=$JV_SRCROOT/m2/repository -Dmaven.test.skip=true -DBUILD_NUMBER=$BLDNUM -Dfile=$maven_file -DgroupId=$maven_groupId -DartifactId=$maven_artifactId -Dversion=$maven_version -DpomFile=$maven_pom"

    bldmsg -mark -p $p/push_maven `echo $cmd2`

    eval $cmd2
    return $?
}



push_kit()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    push_kit_errs=0

    pushkitsrc="$PUSHKIT_SRC/$bldnum"
    pushkitlist="$pushkitsrc/external/gfesb_download $pushkitsrc/external/index.html $pushkitsrc/external/jbi.ver $pushkitsrc/external/cvs_update.log $pushkitsrc/CORE $pushkitsrc/jbise $pushkitsrc/appservers"

    #Make sure source location is reachable
    if [ ! -d "$pushkitsrc" ]; then
        bldmsg -error -p $p/pushkit "can not access source dir '$pushkitsrc' - ABORT"
        push_kit_errs=1
        return $push_kit_errs
    fi

    #####
    #test the ssh connection, by copying a file from local /tmp to remote /tmp:
    #####
    _pushkit_test=/tmp/pushkit_sshtest.$$
    touch -f $_pushkit_test
    scp -B $_pushkit_test ${PUSHKIT_IDENTITY}:$_pushkit_test
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/push_kit "Cannot copy test file '$_pushkit_test' using ssh identity '$PUSHKIT_IDENTITY' - ABORT"
        push_kit_errs=1
        rm -f $_pushkit_test
        return $push_kit_errs
    fi

    ###########
    #connection okay - remove local & remote test files, create destination directory:
    ###########
    rm -f $_pushkit_test
    cmd="ssh $PUSHKIT_IDENTITY 'rm -f $_pushkit_test && mkdir -p $PUSHKIT_DEST/$bldnum'"
    bldmsg -mark -p $p/pushkit "$cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/push_kit "$cmd FAILED"
        push_kit_errs=1
        return $push_kit_errs
    fi

     #copy kits.  note -B option is for "batch", and prevents looping for password entry:
     cmd="scp -B -pr $pushkitlist ${PUSHKIT_IDENTITY}:$PUSHKIT_DEST/$bldnum"
     bldmsg -mark -p $p/pushkit "$cmd"
     eval $cmd
     status=$?
     if [ $status -ne 0 ]; then
         bldmsg -error -p $p/push_kit "$cmd FAILED"
         push_kit_errs=1
         return $push_kit_errs
     else
         bldmsg -p $p/pushkit "copy to pushkit location succeeded"
     fi

     ####
     # Create latest symlink in PUSHKIT_DEST directory
     ####
     cmd="ssh $PUSHKIT_IDENTITY 'cd $PUSHKIT_DEST && rm -f .previouslatest && ((test -d latest &&  mv latest .previouslatest) || rm -f latest) && ln -s $bldnum latest'"
     bldmsg -mark -p $p/pushkit "$cmd"
     eval $cmd
     status=$?
     if [ $status -ne 0 ]; then
         bldmsg -error -p $p/push_kit "$cmd FAILED"
         push_kit_errs=1
         return $push_kit_errs
     fi

     return $push_kit_errs
}



make_jboss_packages()
{
    #make_jboss_packages "$jboss_product" "$jboss_port" "$jboss_pri_bld" >>$JBOSSLOG 2>&1

    product="$1"
    kitport="$2"
    jboss_num="$3"

    kitbase=$KIT_DISTROOT/$KIT_REV

    kitdir=$kitbase/Build$jboss_num/$kitport/$product
    cdromdir=$kitbase/Build$jboss_num/CDROM/$kitport/$product
    bundledir=$kitbase/Build$jboss_num/BUNDLES/$kitport/$product
    packagedir=$kitbase/Build$jboss_num/NATIVE-PACKAGES/$kitport/$product
    jbossdir=$kitbase/Build$jboss_num/JBOSS/$kitport/$product
    cdrompackagedir="${cdromdir}/packages"

    bldmsg -mark "product is $product"
    bldmsg -mark "kitport is $kitport"
    bldmsg -mark "jboss_num is $jboss_num"
    bldmsg -mark "kitdir is $kitdir"
    bldmsg -mark "cdromdir is $cdromdir"
    bldmsg -mark "bundledir is $bundledir"
    bldmsg -mark "packagedir is $packagedir"
    bldmsg -mark "jbossdir is $jbossdir"
    bldmsg -mark "cdrompackagedir is $cdrompackagedir"
    bldmsg -mark "kitbase is $kitbase"


    if [ "$product" = "" -o "$kitport" = ""  -o "$jboss_num" = "" ]; then
        bldmsg -error -p $p/make_jboss_packages "Usage: make_jboss_packages product port primary_build_number"
        return 1
    fi

    make_jboss_packages_errs=0
    jboss_packager_ant_target="createkit"


    if [ "$jboss_packager_ant_target" != "NONE" ]; then

      cd $SRCROOT/shasta/jboss-packager
      cmd="ant createkit"
      bldmsg -mark -p $p/make_jboss_packages `echo $cmd`
      eval $cmd
      status=$?

      if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_jboss_packages Building jboss packages for $product on $kitport FAILED
        make_jboss_packages_errs=1
      fi

      ########
      #release jboss packages to packages directory:
      ########
      bom=${product}_${kitport}_jboss_packages.bom
      bldmsg -mark -p $p/make_jboss_packages Release $bom to $jbossdir
      release -bomloc $SRCROOT/rl/src/cmn/bom -ver $jes_num -nolog -checksum -w $jbossdir -port $kitport $bom
      status=$?
      if [ $status -ne 0 ]; then
          bldmsg -error -p $p/make_jboss_packages Release of $bom to $jbossdir FAILED
          make_jboss_packages_errs=1
      fi

    fi

    return $make_jboss_packages_errs
}

#Usage:  make_native_packages product kit_port jes_num
make_native_packages()
{
    #make_native_packages "$jes_product" "$jes_port" "$jes_pri_bld" >>$JESLOG 2>&1

    product="$1"
    kitport="$2"
    jes_num="$3"

    kitbase=$KIT_DISTROOT/$KIT_REV

    kitdir=$kitbase/Build$jes_num/$kitport/$product
    cdromdir=$kitbase/Build$jes_num/CDROM/$kitport/$product
    bundledir=$kitbase/Build$jes_num/BUNDLES/$kitport/$product
    packagedir=$kitbase/Build$jes_num/NATIVE-PACKAGES/$kitport/$product
    cdrompackagedir="${cdromdir}/packages"

    bldmsg -mark "product is $product"
    bldmsg -mark "kitport is $kitport"
    bldmsg -mark "jes_num is $jes_num"
    bldmsg -mark "kitdir is $kitdir"
    bldmsg -mark "cdromdir is $cdromdir"
    bldmsg -mark "bundledir is $bundledir"
    bldmsg -mark "packagedir is $packagedir"
    bldmsg -mark "cdrompackagedir is $cdrompackagedir"
    bldmsg -mark "kitbase is $kitbase"


    if [ "$product" = "" -o "$kitport" = ""  -o "$jes_num" = "" ]; then
        bldmsg -error -p $p/make_native_packages "Usage: make_native_packages product port primary_build_number"
        return 1
    fi

    make_native_packages_errs=0
    native_packager_ant_target="NONE"

    if [ "$kitport" = "solsparc" -o "$kitport" = "solx86" ]; then
        native_packager_ant_target="build-solaris-packages"
    fi

    if [ "$kitport" = "linux" ]; then
        native_packager_ant_target="build-linux-rpms"
    fi

    if [ "$native_packager_ant_target" != "NONE" ]; then

      cd $SRCROOT/shasta/native-packager
      cmd="ant $VERBOSE_ARG -Dregress_product=$product -Dkitport=$kitport -Dkitlocation=$kitdir -Dzip_kit_location=$cdrompackagedir $native_packager_ant_target"
      bldmsg -mark -p $p/make_native_packages `echo $cmd`
      eval $cmd
      status=$?

      if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_native_packages Building native packages for $product FAILED
        make_native_packages_errs=1
      fi

      ########
      #release native packages to packages directory:
      ########
      bom=${product}_${kitport}_native_packages.bom
      bldmsg -mark -p $p/make_native_packages Release $bom to $packagedir
      release -bomloc $SRCROOT/rl/src/cmn/bom -ver $jes_num -nolog -checksum -w $packagedir -port $kitport $bom
      status=$?
      if [ $status -ne 0 ]; then
          bldmsg -error -p $p/make_native_packages Release of $bom to $packagedir FAILED
          make_native_packages_errs=1
      fi

    fi

    return $make_native_packages_errs
}


make_gui_installer()
#Usage:  make_gui_installer kit_port kitdir cdromdir
{
    product="$1"
    kitport="$2"
    kitdir="$3"
    cdromdir="$4"
    if [ "$product" = "" -o "$kitport" = "" -o "$kitdir" = "" -o "$cdromdir" = "" ]; then
        bldmsg -error -p $p/make_gui_installer "Usage: make_gui_installer product kit_port kitdir cdromdir"
        return 1
    fi

    make_gui_installer_errs=0

    cd $SRCROOT/installers/gui
    cmd="ant $VERBOSE_ARG -Dregress_product=$prod -Dkitport=$kitport -Dkitlocation=$kitdir build-kit"
    # cmd="echo build-kit"
    bldmsg -mark -p $p/make_gui_installer `echo $cmd`

    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_gui_installer Building installer for $prod FAILED
        make_gui_installer_errs=1
    fi

    ########
    #release installer to cdrom directory:
    ########
    bom=${product}_installer.bom

    bldmsg -mark -p $p/make_gui_installer Release $bom to $cdromdir

    release -bomloc $SRCROOT/rl/src/cmn/bom -ver $BLDNUM -nolog -checksum -w $cdromdir -port $kitport $bom
    status=$?

    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_gui_installer Release of $bom to $cdromdir FAILED
        make_gui_installer_errs=1
    fi

    return $make_gui_installer_errs
}

make_mavenarchive()
#Usage:  make_mavenarchive  "$prod" "$kitport" "$kitdir"
{
    product="$1"
    kitport="$2"
    kitdir="$3"
    if [ -z "$product" -o -z "$kitport" -o -z "$kitdir" ]; then
        bldmsg -error -p $p/make_mavenarchive "Usage: make_mavenarchive product kit_port kitdir "
        return 1
    fi

    if [ $DEBUG -ne 0 ]; then
        cat << EOF
IN make_mavenarchive:
    product=>$product<
    kitport=>$kitport<
    kitdir=>$kitdir<
EOF
    fi

    # This command sets version variables loaded from codeline.pl that are needed
    export FULL_VERSION_UL SHORT_PRODUCT_NAME
    eval `bldhost -product $product -versionproperties | egrep '^FULL_VERSION_UL=|^SHORT_PRODUCT_NAME=' `

    make_mavenarchive_errs=0

    maven_kitbase=$kitdir/../../maven

    bldmsg -mark -p $p/make_mavenarchive Archiving m2 to bld/m2.zip using maven_archive.bom
    cd $SRCROOT
    rm -rf $SRCROOT/bld/m2.zip
    zip -r $SRCROOT/bld/m2.zip m2
    bldmsg -mark -p $p/make_mavenarchive Release maven_archive.bom to $maven_kitbase
    installdir $maven_kitbase
    release -bomloc $SRCROOT/rl/src/cmn/bom -ver $BLDNUM -nolog -checksum -w $maven_kitbase -port $realport maven_archive.bom
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_mavenarchive releasing m2.zip via maven_archive.bom FAILED
        make_mavenarchive_errs=1
    fi


    bldmsg -mark -p $p/make_mavenarchive releasing maven components to kits server via maven_publish.bom
    release -bomloc $SRCROOT/rl/src/cmn/bom -ver $BLDNUM -nolog -checksum -w $maven_kitbase maven_publish.bom
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_mavenarchive releasing maven components via maven_publish.bom FAILED
        make_mavenarchive_errs=1
    fi


    return $make_mavenarchive_errs
}


make_bundle()
#Usage:  make_bundle  "$prod" "$kitport" "$kitdir" "$cdromdir" "$bundledir"
{
    product="$1"
    kitport="$2"
    kitdir="$3"
    cdromdir="$4"
    bundledir="$5"
    if [ -z "$product" -o -z "$kitport" -o -z "$kitdir" -o -z "$cdromdir" -o -z "$bundledir" ]; then
        bldmsg -error -p $p/make_bundle "Usage: make_bundle product kit_port kitdir cdromdir bundledir"
        return 1
    fi

    if [ $DEBUG -ne 0 ]; then
        cat << EOF
IN make_bundle:
    product=>$product<
    kitport=>$kitport<
    kitdir=>$kitdir<
    cdromdir=>$cdromdir<
    bundledir=>$bundledir<
EOF
    fi

    # This command sets version variables loaded from codeline.pl that are needed
    export FULL_VERSION_UL SHORT_PRODUCT_NAME
    eval `bldhost -product $product -versionproperties | egrep '^FULL_VERSION_UL=|^SHORT_PRODUCT_NAME=' `

    make_bundle_errs=0

    #clean output directory:
    stagedir=$SRCROOT/installers/gui/bld/installer/$product
    installdir $stagedir

    realport=`map_regress_port $kitport`
    bundleport=$realport

    cd $cdromdir
    if [ $realport = 'nt' ]; then
        rm -f $stagedir/${SHORT_PRODUCT_NAME}_${FULL_VERSION_UL}_windows.zip
        jar -Mcf $stagedir/${SHORT_PRODUCT_NAME}_${FULL_VERSION_UL}_windows.zip *
    else
        [ "$realport" = "solsparc" ] && bundleport="solaris-sparc"
        [ "$realport" = "solx86" ] && bundleport="solaris-x86"

        rm -f $stagedir/${SHORT_PRODUCT_NAME}_${FULL_VERSION_UL}_${bundleport}.tar\
            ${SHORT_PRODUCT_NAME}_${FULL_VERSION_UL}_${bundleport}.tar.zip

        if [ "$realport" = "solsparc" -o "$realport" = "solx86" ]; then
            #use solaris tar:
            if [ "$FORTE_PORT" != "solsparc" -a "$FORTE_PORT" != "solsparc" ]; then
                bldmsg -warn -p $p/make_bundle "creating solaris tar on non-solaris primary build host.  Output is invalid.  We assume you are testing."
            fi
            /bin/tar -cf $stagedir/${SHORT_PRODUCT_NAME}_${FULL_VERSION_UL}_${bundleport}.tar *
        else
            #use gnu tar ($TOOLROOT version):
            tar -cf $stagedir/${SHORT_PRODUCT_NAME}_${FULL_VERSION_UL}_${bundleport}.tar *
        fi

        cd $stagedir
        jar -Mcf ${SHORT_PRODUCT_NAME}_${FULL_VERSION_UL}_${bundleport}.tar.zip ${SHORT_PRODUCT_NAME}_${FULL_VERSION_UL}_${bundleport}.tar
    fi

    bom=${product}_bundle.bom
    bldmsg -mark -p $p/make_bundle Release $bom to $bundledir

    release -bomloc $SRCROOT/rl/src/cmn/bom -ver $BLDNUM -nolog -checksum -w $bundledir -port $realport $bom
    status=$?

    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/make_bundle Release of $bom to $KIT_DISTROOT FAILED
        make_bundle_errs=1
    fi

    #maven_kitbase=$bundledir/../../..

    #if [ "$realport" = "solsparc" ]; then
    #  bldmsg -mark -p $p/make_bundle Archiving m2 to bld/m2.zip
    #  cd $SRCROOT
    #  rm -rf $SRCROOT/bld/m2.zip
    #  jar -Mcf $SRCROOT/bld/m2.zip m2
    #  bldmsg -mark -p $p/make_bundle Release maven_archive.bom to $maven_kitbase
    #  installdir $maven_kitbase
    #  release -bomloc $SRCROOT/rl/src/cmn/bom -ver $BLDNUM -log -checksum -w $maven_kitbase -port $realport maven_archive.bom
    #fi


    return $make_bundle_errs
}

run_unit_tests()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE=jbi_regress"

    cd $SRCROOT
    run_tests_status=0

    ######
    #junit (and junitreport happens automatically here)
    ######
    bldmsg -markbeg ${p}:junit
    #NOTE:  changing target to install to work-around maven lifecycle bug that evokes
    #errors in the maven-dependency plugin in packaging projects when running "test" or "test-compile".
    #RT 2/2/07
    #
    #Changing target to package, to see if we can avoid writing on local repos again while
    #potentially other tasks are running.  RT 2/13/07
    #
    cmd="mvn $MAVEN_OPTIONS -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -Dmaven.test.skip=false -DBUILD_NUMBER=$BLDNUM package"
    bldmsg -mark -p $p/run_tests `echo $cmd`
    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        run_tests_status=1
        bldmsg -error -p $p/run_tests Junit test step FAILED
    fi
    bldmsg -markend -status $status ${p}:junit
    bld_reset_watchdog




    ############
    #junitreport (archival; report is done above, in "test" step)
    ############

    #archive junit report:
    if [ -d "$SRCROOT/bld/junit" ]; then
        bldmsg -mark -p $p/run_tests Archive junit test results
        cp -rp $SRCROOT/bld/junit $LOGDIR
        if [ $? -ne 0 ]; then
            run_tests_status=1
            bldmsg -error -p $p/run_tests Archive junit test results FAILED
        fi
    fi
    bld_reset_watchdog

    return $run_tests_status
}

run_system_tests()
{
    if [ $TESTMODE -eq 1 ]; then
        return 0
    fi

    bldmsg "BUILDRESULTS_TYPE=jbi_regress"

    run_tests_status=0

    #########
    #jregress
    #########
    bldmsg -markbeg ${p}:jregress

    ### Run jbi standalone regress tests first because they are quick. can be run offline:
    cd $SRCROOT/esb-test
    cmd="mvn $MAVEN_OFFLINE $MAVEN_OPTIONS -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -DMAX_ASADMIN_WAIT="$MAX_ASADMIN_WAIT" -DBUILD_NUMBER=$BLDNUM -Dtest_target=openesb:jbise_jregress_no_report"
    bldmsg -mark -p $p/run_tests `echo $cmd`
    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        run_tests_status=1
        bldmsg -error -p $p/run_tests jregress step FAILED
    fi
    bld_reset_watchdog

    ### Run standard jregress tests.  can be run offline:
    cd $SRCROOT/esb-test
    cmd="mvn $MAVEN_OFFLINE $MAVEN_OPTIONS -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -DMAX_ASADMIN_WAIT="$MAX_ASADMIN_WAIT" -DBUILD_NUMBER=$BLDNUM -Dtest_target=openesb:jregress_no_report"
    bldmsg -mark -p $p/run_tests `echo $cmd`
    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        run_tests_status=1
        bldmsg -error -p $p/run_tests jregress step FAILED
    fi

    bld_reset_watchdog

    bldmsg -markend -status $status ${p}:jregress

    bld_reset_watchdog

    ###############
    #jregressreport
    ###############
    #run jregress report from maven harness.  can be run offline:
    cd $SRCROOT/esb-test
    cmd="mvn $MAVEN_OFFLINE $MAVEN_OPTIONS -Das8base=$JV_AS8BASE -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -DBUILD_NUMBER=$BLDNUM -Dtest_target=openesb:jregress_report"
    eval $cmd
    if [ $? -ne 0 ]; then
        run_tests_status=1
        bldmsg -error -p $p/run_tests jregress report step FAILED
    fi

    #archive jregress summary log & server logs:
    if [ -d "$SRCROOT/bld/server-logs" ]; then
        bldmsg -mark -p $p/run_tests Archive server logs
        cp -rp $SRCROOT/bld/server-logs $LOGDIR
        if [ $? -ne 0 ]; then
            run_tests_status=1
            bldmsg -error -p $p/run_tests Archive junit test results FAILED
        fi
    fi
    bld_reset_watchdog

    cd $SRCROOT
    return $run_tests_status
}

build_summary()
#create a summary status of the test step, if tests were run
{
    rm -f "$BUILD_SUMMARY_REPORT"
    touch "$BUILD_SUMMARY_REPORT"

    jdk_version=`2>&1 java -version | grep 'Java(TM)'`
    appserver_version=`2>&1 asadmin version --verbose=true --terse=true`
    date=`date`
    host=`uname -n`
    oesb_version=`2>&1 grep BUILD_NUMBER $AS8BASE/jbi/jbi.ver`
    ojc_version=`2>&1 grep BUILD_NUMBER $AS8BASE/jbi.gf/ojc.ver`

    #write the summary header:
    cat >> "$BUILD_SUMMARY_REPORT" << EOF
TEST RESULTS $date ($host/$FORTE_PORT/${BLDNUM}):
    $jdk_version
    Appserver $appserver_version
    Regression Testing Open-ESB $oesb_version
    Regression Testing Open-JBI-Components $ojc_version
    JBI Codeline = open-esb{$CVS_BRANCH_NAME}

EOF

    # if no tests were run ...
    if [ $DOREGRESS -eq 0 ]; then
        cat >> "$BUILD_SUMMARY_REPORT" << EOF
NO TESTS RUN

EOF
    ##
    else
        #... we ran some tests:
        junit_summary="NO JUNIT TEST RESULTS"
        if [ -r "$UNITTESTLOG"  ]; then
            junit_summary=`sed -n -e "/${PRODUCT}:junit_report/,/junit.failure.list/p" "$UNITTESTLOG" | grep 'junit[\._]'`
            [ "$junit_summary" = "" ] && junit_summary="NO JUNIT TEST RESULTS"
        fi

        jregress_summary="NO JREGRESS TEST RESULTS"
        if [ -r "$REGRESSLOG" ]; then
            jregress_summary=`sed -n -e "/${PRODUCT}:jregress_report/,/timed out/p" "$REGRESSLOG"`
            [ "$jregress_summary" = "" ] && jregress_summary="NO JREGRESS TEST RESULTS"
        fi

        cat >> "$BUILD_SUMMARY_REPORT" << EOF
$junit_summary

$jregress_summary

EOF
    ##
    fi
}

installer_tests()
#run the installer regression suite.
#this step must run after the installer is built.
{
    installer_tests_status=0
    cd $SRCROOT/esb-test

    #run installer tests.  can be run offline:
    cmd="mvn $MAVEN_OFFLINE $MAVEN_OPTIONS -Das8base=$JV_AS8BASE -DSRCROOT='$JV_SRCROOT' -Dmaven.repo.local='$JV_SRCROOT/m2/repository' -DMAX_ASADMIN_WAIT="$MAX_ASADMIN_WAIT" -DBUILD_NUMBER=$BLDNUM -Dtest_target=openesb:installer_jregress"
    bldmsg -mark -p $p/installer_tests `echo $cmd`
    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        installer_tests_status=1
        bldmsg -error -p $p/installer_tests installer tests FAILED
    fi

    cd $SRCROOT
    return $installer_tests_status
}

run_background_tasks()
#start a thread to run supplemental build tasks in parallel with regression
#WARNING:  we cannot set any vars in parent process, so we use shprops to
#communicate results.
{
    #note - we add the begin/end marks here
    #so we know the actual time the background tasks take,
    #(otherwise, get the run_tests times).
    bldmsg -markbeg ${p}:background tasks

    BG_BUILD_STATUS=0
    shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS

    if [ $DOSITE -eq 1 ]; then
        bldmsg -mark Building site - Log is $SITELOG
        bldmsg -markbeg ${p}:site
        build_site >> $SITELOG 2>&1
        status=$?
        if [ $status -ne 0 ]; then
            bldmsg -error -p $p build_site failed. Check $SITELOG for errors.
            BG_BUILD_STATUS=1
            shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
        fi
        bldmsg -markend -status $status ${p}:site
        bld_reset_watchdog
    fi

    if [ $DOJAVADOC -eq 1 ]; then
        bldmsg -mark Building javadoc - Log is $JAVADOCLOG
        bldmsg -markbeg ${p}:javadoc
        build_javadoc >> $JAVADOCLOG 2>&1
        status=$?
        if [ $status -ne 0 ]; then
            bldmsg -error -p $p build_javadoc failed. Check $JAVADOCLOG for errors.
            BG_BUILD_STATUS=1
            shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
        fi
        bldmsg -markend -status $status ${p}:javadoc
        bld_reset_watchdog
    fi

    release_failed=0
    if [ $DORELEASE -eq 1 -a $BUILD_FAILED -eq 0 ]; then
        bldmsg -mark Releasing $PRODUCT - Log is $RELEASELOG
        bldmsg -markbeg ${p}:make_release
        make_release >> $RELEASELOG 2>&1
        status=$?
        if [ $status -ne 0 ]; then
            bldmsg -error -p $p make_release failed. Check $RELEASELOG for errors.
            release_failed=1
            BG_BUILD_STATUS=1
            shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
        fi
        bldmsg -markend -status $status ${p}:make_release
        bld_reset_watchdog
    elif [ $DORELEASE -eq 1 -a $BUILD_FAILED -ne 0 ]; then
        bldmsg -error Skipping make_release because build step failed
    fi

    if [ $REPACKAGE_GF -eq 1 -a $release_failed -eq 0 ]; then
        bldmsg -mark Repackaging glassfish - Log is $REPACK_GF_LOG
        bldmsg -markbeg ${p}:repackage_glassfish
        repackage_glassfish >> $REPACK_GF_LOG 2>&1
        status=$?
        if [ $status -ne 0 ]; then
            bldmsg -error -p $p repackage_glassfish failed. Check $REPACK_GF_LOG for errors.
            BG_BUILD_STATUS=1
            shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
        fi
        bldmsg -markend -status $status ${p}:repackage_glassfish
        bld_reset_watchdog
    elif [ $REPACKAGE_GF -eq 1 -a $release_failed -ne 0 ]; then
        bldmsg -error Skipping glassfish repackaging step because release step failed
    fi

    qecomponents_failed=0
    if [ $DOINSTALL -eq 1 -a $DOALASKA -eq 1 ]; then
        if [ $release_failed -eq 0 -a $BUILD_FAILED -eq 0 ]; then
            bldmsg -mark - Creating components.zip for QE  - Log is $QECOMPONENTSLOG
            bldmsg -markbeg ${p}:make_qecomponents
            make_qecomponents >> $QECOMPONENTSLOG 2>&1
            status=$?
            if [ $status -ne 0 ]; then
                bldmsg -error -p $p make_qecomponents failed. Check $QECOMPONENTSLOG for errors.
                qecomponents_failed=1
                BG_BUILD_STATUS=1
                shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
            fi
            bldmsg -markend -status $status ${p}:make_qecomponents
        else
             bldmsg -error Skipping make_qecomponents because release or build step failed
        fi
        bld_reset_watchdog
    fi


    install_failed=0
    if [ $DOINSTALL -eq 1 ]; then
        if [ $release_failed -eq 0 -a $BUILD_FAILED -eq 0 ]; then
            bldmsg -mark Kitting Installer of $PRODUCT - Log is $INSTALLOG
            bldmsg -markbeg ${p}:make_installer
            make_installer >> $INSTALLOG 2>&1
            status=$?
            if [ $status -ne 0 ]; then
                bldmsg -error -p $p make_installer failed. Check $INSTALLOG for errors.
                install_failed=1
                BG_BUILD_STATUS=1
                shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
            fi
            bldmsg -markend -status $status ${p}:make_installer
        else
             bldmsg -error Skipping make_installer because release or build step failed
        fi
        bld_reset_watchdog
    fi

    ### Run the INSTALLER JREGRESS TEST after we build the installer:
    installer_tests_failed=0
    if [ $DOINSTALL_TESTS -eq 1 ]; then
        if [ $release_failed -eq 0 -a $BUILD_FAILED -eq 0 -a $install_failed -eq 0 ]; then
            bldmsg -markbeg ${p}:installer_tests
            installer_tests >> $INSTALLER_TESTLOG 2>&1
            status=$?
            if [ $status -ne 0 ]; then
                bldmsg -error -p $p installer_tests failed. Check $INSTALLER_TESTLOG for errors.
                installer_tests_failed=1
                BG_BUILD_STATUS=1
                shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
            fi
            bldmsg -markend -status $status ${p}:installer_tests
        else
            bldmsg -error Skipping installer tests because release or build step failed
        fi
        bld_reset_watchdog
    fi

    pushkit_failed=0
    if [ $DOPUSHKIT -eq 1 -a $release_failed -eq 0 -a $install_failed -eq 0 -a $BUILD_FAILED -eq 0 ]; then
        bldmsg -mark Pushing kits of $PRODUCT to external site - Log is $PUSHKITLOG
        bldmsg -markbeg ${p}:push_kit
        push_kit >> $PUSHKITLOG 2>&1
        status=$?
        if [ $status -ne 0 ]; then
            bldmsg -error -p $p push_kit failed. Check $PUSHKITLOG for errors.
            pushkit_failed=1
            BG_BUILD_STATUS=1
            shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
        fi
        bldmsg -markend -status $status ${p}:push_kit
        bld_reset_watchdog
    elif [ $DOPUSHKIT -eq 1 -a $DORELEASE -eq 1 -a $BUILD_FAILED -ne 0 ]; then
        bldmsg -error Skipping push_kit because build step failed
    fi

    pushmaven_failed=0
    if [ $DOPUSHMAVEN -eq 1 -a $release_failed -eq 0 -a $install_failed -eq 0 -a $BUILD_FAILED -eq 0 ]; then
        bldmsg -mark Pushing maven artifacts of $PRODUCT to external site - Log is $PUSHMAVENLOG
        bldmsg -markbeg ${p}:push_maven
        push_maven >> $PUSHMAVENLOG 2>&1
        status=$?
        if [ $status -ne 0 ]; then
            bldmsg -error -p $p push_maven failed. Check $PUSHMAVENLOG for errors.
            pushmaven_failed=1
            BG_BUILD_STATUS=1
            shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
        fi
        bldmsg -markend -status $status ${p}:push_maven
        bld_reset_watchdog
    elif [ $DOPUSHMAVEN -eq 1 -a $DORELEASE -eq 1 -a $BUILD_FAILED -ne 0 ]; then
        bldmsg -error Skipping push_maven because build step failed
    fi

    if [ $DOJES -eq 1 ]; then
        if [ $install_failed -eq 0 -a $BUILD_FAILED -eq 0 ]; then
            bldmsg -mark "Begin waiting for $READYFORJES"
            bldwait -max $MAX_KIT_WAIT -p $p -marktime $READYFORJES >>$JESWAITLOG 2>&1
            status=$?
            bldmsg -mark "Finished waiting for $READYFORJES :  status is $status"
            if [ $status -eq 0 ]; then
               #Get variables
               jes_product=shasta
               jes_port=$FORTE_PORT
               jes_pri_bld="`cat $PRI_BLD_LOCATION/regress/.date`"

                bldmsg -mark Making JES kits of $PRODUCT - Log is $JESLOG
                bldmsg -markbeg ${p}:make_native_packages
                make_native_packages "$jes_product" "$jes_port" "$jes_pri_bld" >>$JESLOG 2>&1
                status=$?
                if [ $status -ne 0 ]; then
                  bldmsg -error -p $p make_native_packages failed. Check $JESLOG for errors.
                  BG_BUILD_STATUS=1
                  shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
                fi
                bldmsg -markend -status $status ${p}:make_native_packages
            else
              bldmsg -error Failed during make_native_packages due to timing out waiting for $READYFORJES
            fi
        else
           bldmsg -error Skipping make_native_packages because release or build step failed
        fi

        bld_reset_watchdog
    fi


    if [ $DOJBOSS -eq 1 ]; then
        if [ $install_failed -eq 0 -a $BUILD_FAILED -eq 0 ]; then
            bldmsg -mark "Begin waiting for $READYFORJBOSS"
            bldwait -max $MAX_KIT_WAIT -p $p -marktime $READYFORJBOSS >>$JBOSSWAITLOG 2>&1
            status=$?
            bldmsg -mark "Finished waiting for $READYFORJBOSS :  status is $status"
            if [ $status -eq 0 ]; then
               #Get variables
               jboss_product=shasta
               jboss_port=$FORTE_PORT
               if [ $FORTE_PORT = "nt" ]; then
                 jboss_port="nt5"
               fi

               jboss_pri_bld="`cat $PRI_BLD_LOCATION/regress/.date`"

                bldmsg -mark Making JBOSS kits of $PRODUCT - Log is $JBOSSLOG
                bldmsg -markbeg ${p}:make_jboss_packages
                make_jboss_packages "$jboss_product" "$jboss_port" "$jboss_pri_bld" >>$JBOSSLOG 2>&1
                status=$?
                if [ $status -ne 0 ]; then
                  bldmsg -error -p $p make_jboss_packages failed. Check $JBOSSLOG for errors.
                  BG_BUILD_STATUS=1
                  shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
                fi
                bldmsg -markend -status $status ${p}:make_jboss_packages
            else
              bldmsg -error Failed during make_jboss_packages due to timing out waiting for $READYFORJBOSS
            fi
        else
           bldmsg -error Skipping make_jboss_packages because release or build step failed
        fi

        bld_reset_watchdog
    fi


    # note - do this AFTER the installer build.  We may want to package javadoc,
    # but xref is for internal use only.
    if [ $DOXREF -eq 1 ]; then
        bldmsg -mark Creating cross-referenced source trees - Log is $XREFLOG
        bldmsg -markbeg ${p}:xref
        build_xref >> $XREFLOG 2>&1
        status=$?
        if [ $status -ne 0 ]; then
            bldmsg -error -p $p build_xref failed. Check $XREFLOG for errors.
            BG_BUILD_STATUS=1
            shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
        fi
        bldmsg -markend -status $status ${p}:xref
        bld_reset_watchdog
    fi

    if [ $DOARCHIVEDOC -eq 1 ]; then
        bldmsg -markbeg ${p}:archive_doc
        #not much output - so just let it go to main log:
        archive_doc
        status=$?
        if [ $status -ne 0 ]; then
            bldmsg -error -p $p archive_doc failed.
            BG_BUILD_STATUS=1
            shprops -set $BG_RESULTS BG_BUILD_STATUS=$BG_BUILD_STATUS
        fi
        bldmsg -markend -status $status ${p}:archive_doc
        bld_reset_watchdog
    fi

    bldmsg -markend -status $BG_BUILD_STATUS ${p}:background tasks
}

#################################### MAIN #####################################

p=`basename $0`
saveargs="$@"
BUILD_STATUS=0

#source common build routines or die.
#NOTE - all "bld_<name>" routines come from this module:
require bldcmn.sh

parse_args "$@"
if [ $? -ne 0 ]; then
    echo
fi

#if asking for help, display help of all scripts called by this one.
if [ $DOHELP -eq 1 ]; then
    #none written yet...
    exit 0
fi

show_initial_options

#make sure we have a /tmp dir on nt:
bld_check_create_tmp

bld_check_usepath
if [ $? -ne 0 ]; then
    bld_fatal_error "one or more usePathDef variables not set - ABORT"
fi

if [ $CALLEDFROMWRAPPER -eq 0 ]; then
    setup_wrapper_env
fi

set_global_vars
if [ $? -ne 0 ]; then
    bld_fatal_error "Error setting global variables.  Message should have been provided. - ABORT"
fi

check_local_vars
if [ $? -ne 0 ]; then
    bld_fatal_error "one or more required environment variables not set - ABORT"
fi

show_final_options
show_build_environment

#this will get updated at the end of the build.
#if it does not, then program was interrupted or crashed:
shprops -set $BLDPARMS BUILD_STATUS=1

bldmsg -markbeg $p
#don't trap interrupts until initial start message.
trap rec_signal 2 15

clear_local_locks

#this updates the runBuild wrapper watchdog timer if it exists:
bld_reset_watchdog

if [ $DOFIXCVSROOT -eq 1 ]; then
    bldmsg -mark Fixing CVSROOT setting- log is $FIXCVSROOTLOG
    bldmsg -markbeg ${p}:fixcvsroot
    do_fixcvsroot >> $FIXCVSROOTLOG 2>&1
    status=$?
    if [ $status -ne 0 ]; then
        ### abort entire build if fixcvsroot fails, since
        ### we cannot build from a corrupted working directory:
        bldmsg -error -p $p "fixcvsroot failed - ABORT BUILD. Check $FIX_CVSROOTLOG for errors."
        bldmsg -markend -status $status ${p}:fixcvsroot
        bldmsg -markend -status $BUILD_STATUS $p
        exit $BUILD_STATUS
    fi
    bldmsg -markend -status $status ${p}:fixcvsroot
fi

if [ $DOMAVENCLEAN -eq 1 ]; then
    bldmsg -mark Removing maven local repository in $SRCROOT/m2
    bldmsg -markbeg ${p}:clean_maven
    clean_maven
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p clean_maven failed.
        BUILD_STATUS=1
    fi
    bldmsg -markend -status $status ${p}:clean_maven
    bld_reset_watchdog
fi

if [ $DOCLEAN -eq 1 ]; then
    bldmsg -mark Cleaning build areas - log is $CLEANLOG
    bldmsg -markbeg ${p}:clean_build_tree
    clean_build_tree >> $CLEANLOG 2>&1
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p clean_build_tree failed. Check $CLEANLOG for errors.
        BUILD_STATUS=1
    fi

    #reduce download noise:
    filter_maven_log_in_place $CLEANLOG

    bldmsg -markend -status $status ${p}:clean_build_tree
    bld_reset_watchdog
fi

#set the update time, which is used by the update task and the integration report task
set_update_time
status=$?

if [ $status -ne 0 ]; then
    bld_fatal_error "CANNOT set CVS update time - ABORT"
fi

if [ $DOINTEGREPORT -eq 1 ]; then
    #run integration report:
    bldmsg -mark Creating integration report - output is $INTEGRATIONLOG
    bldmsg -markbeg ${p}:$INTEG_REPORT
    $INTEG_REPORT -o $INTEGRATIONLOG $UCVSUPDATETIME
    if [ $? -ne 0 ]; then
        bldmsg -error ${p}:$INTEG_REPORT failed for $PRODUCT repostiorty.
        status=1
    fi

    $INTEG_REPORT -tools -o $TMPA $UCVSUPDATETIME
    if [ $? -ne 0 ]; then
        bldmsg -error ${p}:$INTEG_REPORT failed for devtools repostiorty.
        status=2
    else
        cat $TMPA >> $INTEGRATIONLOG
        rm -f $TMPA
    fi

    bldmsg -markend -status $status ${p}:$INTEG_REPORT


    bld_reset_watchdog
fi

if [ $DOUPDATE -eq 1 ]; then
    bldmsg -mark Updating source code - log is $UPDATELOG
    bldmsg -markbeg ${p}:update_src
    update_src >> $UPDATELOG 2>&1
    status=$?
    bldmsg -markend -status $status ${p}:update_src
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p update_src failed. Check $UPDATELOG for errors.
        BUILD_STATUS=1

        ### abort entire build if RE build, since we cannot issue kits
        ### built from a corrupted working directory:
        if [ $RELEASE_BUILD -eq 1 ]; then
            bldmsg -error -p $p BUILD ABORTED DUE TO CVS UPDATE ERRORS.
            bldmsg -markend -status $BUILD_STATUS $p
            exit $BUILD_STATUS
        fi
    else
        #temporarily ignore m2 files, so we can get meaningful buildResults.  RT 7/24/06
        mv $UPDATELOG ${UPDATELOG}.tmp
        egrep -v '/pom.xml$|/settings.xml$|/m2.ant$' ${UPDATELOG}.tmp > $UPDATELOG
        rm -f ${UPDATELOG}.tmp
    fi
    bld_reset_watchdog
fi

BUILD_FAILED=0
if [ $DOBUILD -eq 1 ]; then
    bldmsg -mark Building $PRODUCT - Log is $BUILDLOG
    bldmsg -markbeg ${p}:build_product
    build_product >> $BUILDLOG 2>&1
    status=$?

    #reduce download noise:
    filter_maven_log_in_place $BUILDLOG

    #this additional check is necessary because we tell ant to ignore various errors,
    #so ant always returns a zero status.
    if [ $status -eq 0 ]; then
        has_ant_errors $BUILDLOG
        status=$?
    fi

    if [ $status -ne 0 ]; then
        bldmsg -error -p $p build_product failed. Check $BUILDLOG for errors.
        BUILD_STATUS=1
        BUILD_FAILED=1

        #if -keepon specified ...
        if [ $KEEPON -eq 1 ]; then
            #.. then pretend the compile didn't fail:
            BUILD_FAILED=0
        fi
    fi
    bldmsg -markend -status $status ${p}:build_product
    bld_reset_watchdog
fi

#track total time for all tests:
if [ $DOREGRESS -eq 1 -a $BUILD_FAILED -eq 0 ]; then
    bldmsg -mark Testing $PRODUCT - Log is $REGRESSLOG
    bldmsg -markbeg ${p}:run_tests
fi

######
#junit
######
if [ $DOJUNIT -eq 1 -a $BUILD_FAILED -eq 0 ]; then
    run_unit_tests >> $UNITTESTLOG 2>&1
    status=$?
    bld_reset_watchdog

    #scan for ignored ant task errors:
    if [ $status -eq 0 ]; then
        has_ant_errors $UNITTESTLOG
        status=$?
    fi

    if [ $status -ne 0 ]; then
        bldmsg -error -p $p One or more unit tests failed. Check $UNITTESTLOG for errors.
        BUILD_STATUS=1
    fi
elif [ $DOJUNIT -eq 1  -a $BUILD_FAILED -ne 0 ]; then
    bldmsg -error Skipping unit tests because build step failed
fi

#####
#FORK background tasks:
#NOTE:  because both site and junit tasks run the maven harness, we cannot run them
#       in parallel.  RT 7/24/08
#####
export BG_LOG BG_RESULTS
BG_LOG=$LOGDIR/background_tasks.log
BG_RESULTS=$LOGDIR/background_results.sh

run_background_tasks > $BG_LOG &
bgpid=$!

#########
#jregress
#########
if [ $DOJREGRESS -eq 1 -a $BUILD_FAILED -eq 0 ]; then
    run_system_tests >> $REGRESSLOG 2>&1
    status=$?
    bld_reset_watchdog

    #scan for ignored ant task errors:
    if [ $status -eq 0 ]; then
        has_ant_errors $REGRESSLOG
        status=$?
    fi

    if [ $status -ne 0 ]; then
        bldmsg -error -p $p One or more system tests failed. Check $REGRESSLOG for errors.
        BUILD_STATUS=1
    fi
elif [ $DOJREGRESS -eq 1  -a $BUILD_FAILED -ne 0 ]; then
    bldmsg -error Skipping system tests because build step failed
fi

if [ $DOREGRESS -eq 1 -a $BUILD_FAILED -eq 0 ]; then
    bldmsg -markend -status $status ${p}:run_tests
fi

if [ $DOARCHIVELOGS -eq 1 -a $DOREGRESS -eq 1 -a $BUILD_FAILED -eq 0 ]; then
    bldmsg -markbeg ${p}:archive_logs
    #not much output - so just let it go to main log:
    archive_logs
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -error -p $p archive_logs failed.
        BUILD_STATUS=1
    fi
    bldmsg -markend -status $status ${p}:archive_logs
    bld_reset_watchdog
fi

#########
#WAIT FOR background tasks if necessary:
#########
wait $bgpid
eval `shprops -get $BG_RESULTS BG_BUILD_STATUS`

bldmsg -mark Results of run_background_tasks tasks follows:
cat $BG_LOG
bldmsg -mark "##### EOF (run_background_tasks)"

#rm -f $BG_LOG $BG_RESULTS

if [ $BG_BUILD_STATUS -ne 0 ]; then
    BUILD_STATUS=1
fi

if [ $BUILD_STATUS -eq 0 ]; then
    #update the last good build time in the build parameters file:
    shprops -set $BLDPARMS ULASTGOODBLDTIME=$UBLDSTARTTIME
fi

shprops -set $BLDPARMS BUILD_STATUS=$BUILD_STATUS

#collect logs and run ant summary report:
build_summary

bldmsg -markend -status $BUILD_STATUS $p

cleanup

exit $BUILD_STATUS
