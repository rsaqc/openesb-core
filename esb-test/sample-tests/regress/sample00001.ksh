#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)sample00001.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#
# This test installs and runs the JBI component examples that are posted on
# the open-esb web site:
# https://open-esb.dev.java.net/public/jbi-comp-examples/Developing_JBI_Components.html
# This is required to ensure that any changes to the JBI runtime that affect
# the examples are caught in time to update the examples on the web site so
# that users will be able to run the examples.
# The source for the examples is in the ri-examples/jbi-comp directory. The
# build script for the examples produces the jbi-comp-examples.zip file that
# is picked up in this test.
#
# @author mwhite
#

echo testname is sample00001

#
# Initial setup
#

. ./regress_defs.ksh
my_test_domain=JBITest
. $SRCROOT/antbld/regress/common_defs.ksh

export SAMPLE_HOME
SAMPLE_HOME=$JV_SVC_BLD/jbi-comp-examples

rm -f $JV_SVC_BLD/sample00001.log 1>&2

#
# Unzip the example file into the test work directory
#

rm -rf $SAMPLE_HOME 1>&2
SAVE_PWD=`pwd`
cp jbi-comp-examples.zip $JV_SVC_BLD 1>&2
cd $JV_SVC_BLD
jar -xf jbi-comp-examples.zip 1>&2
rm -f $JV_SVC_BLD/jbi-comp-example.zip 1&2
cd $SAVE_PWD

#
# Set the GlassFish install root and admin settings in the build.properties
# files for both examples (MyEngine and MyBinding)
#

echo "s#glassfish.home=.*#glassfish.home=$JV_AS8BASE#g" > $JV_SVC_BLD/buildproperties.sed
echo "s#jbi.port=.*#jbi.port=$ASADMIN_PORT#g" >> $JV_SVC_BLD/buildproperties.sed

sed -f $JV_SVC_BLD/buildproperties.sed $SAMPLE_HOME/MyEngine/build.properties > MyEngine.properties
mv MyEngine.properties $SAMPLE_HOME/MyEngine/build.properties

sed -f $JV_SVC_BLD/buildproperties.sed $SAMPLE_HOME/MyBinding/build.properties > MyBinding.properties
mv MyBinding.properties $SAMPLE_HOME/MyBinding/build.properties

#
# Set the GlassFish JMX RMI provider URL in the test.properties for the
# MyBinding example
#

echo "s#jmx.binding.inbound.endpoint.address.url=.*#jmx.binding.inbound.endpoint.address.url=$AS_JMX_REMOTE_URL#g" > $JV_SVC_BLD/testproperties.sed

sed -f $JV_SVC_BLD/testproperties.sed $SAMPLE_HOME/MyBinding/test/java/bindingtest/test.properties > bindingtest.properties
mv bindingtest.properties $SAMPLE_HOME/MyBinding/test/java/bindingtest/test.properties

#
# Start the HTTP BC for the test
#

$JBI_ANT -Djbi.component.name=sun-http-binding start-component > $JV_SVC_BLD/sun-http-binding-start.log

#
# Create the artifacts, install/deploy them, and run the MyEngine test, then
# do the same for the MyBinding test
#

cd $SAMPLE_HOME/MyEngine
$ANT_HOME/bin/ant package > $JV_SVC_BLD/MyEngine.package.log
$ANT_HOME/bin/ant install > $JV_SVC_BLD/MyEngine.install.log
$ANT_HOME/bin/ant test > $JV_SVC_BLD/MyEngine.test.log

cd $SAMPLE_HOME/MyBinding
$ANT_HOME/bin/ant package > $JV_SVC_BLD/MyBinding.package.log
$ANT_HOME/bin/ant install > $JV_SVC_BLD/MyBinding.install.log
$ANT_HOME/bin/ant test > $JV_SVC_BLD/MyBinding.test.log

#
# Shut down and uninstall the test components
#

cd $SAMPLE_HOME/MyBinding
$ANT_HOME/bin/ant uninstall > $JV_SVC_BLD/MyBinding.uninstall.log

cd $SAMPLE_HOME/MyEngine
$ANT_HOME/bin/ant uninstall > $JV_SVC_BLD/MyEngine.uninstall.log

#
# Shut down the HTTP BC
#

$JBI_ANT -Djbi.component.name=sun-http-binding shut-down-component > $JV_SVC_BLD/sun-http-binding-shutdown.log

#
# Check for success of both tests
#
echo "Checking MyEngine test result"
echo " "
grep "EchoResult: from httpsoap binding - hello new service engine!!!" $JV_SVC_BLD/MyEngine.test.log 2>&1
echo " "
echo "Checking MyBinding test result"
echo " "
grep "EchoResult: from jmx binding component - hello service engine!!!" $JV_SVC_BLD/MyBinding.test.log 2>&1

