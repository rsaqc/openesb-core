#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)packaging00003.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#get a copy of m2 repository for the compare
. ./regress_defs.ksh

cd $SVC_BLD

mkdir trash.$$
[ -r m2-new ] && mv m2-new trash.$$
rm -rf trash.$$ &

echo Copying m2 repository ...
cp -rp $MAVEN_REPOSITORY/open-esb m2-new
[ $? -ne 0 ] && bldmsg -error cp -rp $MAVEN_REPOSITORY/open-esb m2-new FAILED

#remove installer projects, since we only build it on primary platform:
rm -rf m2-new/core

#remove maven-metadata-*.xml files
rm -f `walkdir -f m2-new | grep 'maven-metadata-.*'`

#remove snapshot deployments, which are duplicates.  we get the null version
#when run outside of the jbiBuild harness because $JBI_SNAPSHOT_TIME is undefined:
rm -rf m2-new/*/1.1-SNAPSHOT m2-new/*/1.1-null

#this file is only created after junit. pom needs work.
#remote deployment needs work - should not be over-writing artifacts
#in local repository for deployment.  RT 2/12/07
rm -f `walkdir -f m2-new | grep '\.sha1$'`

#ignore source jars for now.  RT 1/28/08.
rm -rf m2-new/ri-sources
rm -f `walkdir -f m2-new | grep 'sources.jar$'`

#remove empty directories:
rm -rf `walkdir -d -e m2-new`

echo Expanding m2 repository jars...
walkdir -qq -unjar m2-new
walkdir -qq -unjar m2-new
walkdir -qq -unjar m2-new
walkdir -qq -unjar m2-new

wait
exit $?
