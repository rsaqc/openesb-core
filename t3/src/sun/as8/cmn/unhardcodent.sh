#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)unhardcodent.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#this script will parameterize a "virgin" gf nt installation.
#a virgin install has been unpacked but never started.
#
#USAGE:  unhardcodent install_loc
#
#HINT:  to setup a new version, install it and tar it up.
#  then in the install location, run "walkdir -ftxt > ../list".
#  then grep for your path name, eg., "grep rtshasta `cat ../list`".
#  from this, you can create a list of files that need to be patched.
#  read this list into the EDIT_LIST variable.

p=`basename $0`
if [ "$1" = "" -o ! -d "$1" ]; then
    echo USAGE:  $p virgin_install_location
    exit 1
fi

exit_status=0

TMPA=/tmp/${p}_tmpA.$$
TMPB=/tmp/${p}_tmpB.$$
INSTALL_PORT=nt

cd "$1"
conffn=config/asenv.bat
if [ ! -r $conffn ]; then
    echo $p: cannot open $1/$conffn
    exit 1
fi

#convert .bat conf to sh conf:
grep '^set ' $conffn | sed -e 's/^set //' | sed -e 's#\\#&&#g' | sed -e 's#/#\\/#g' | sed -e "s/=\(.*\)/='\1'/" > $TMPA
localtxt -os unix $TMPA
. $TMPA

#echo SOURCED SHELL DEFINITIONS FROM:
#echo -----------------
#cat $TMPA
#echo -----------------

#sanity check:
if [ "$AS_INSTALL" = "" ]; then
    echo $p: invalid file: $1/$conffn - AS_INSTALL is not set
    exit 1
else
    rm -rf appserv_uninstall.class domains/domain1 samples uninstall 
    mv jbi jbi.gf
fi

echo In `pwd`, AS_INSTALL=$AS_INSTALL

#######
#EE KIT?
#######
if [ -r LICENSE_EE.txt ]; then
    echo $p: detected EE kit
    IS_EE_INSTALL=1
else
    echo $p: detected PE kit
    IS_EE_INSTALL=0
fi

AS_INSTALL_PAT=$AS_INSTALL
echo AS_INSTALL_PAT=$AS_INSTALL_PAT

AS_JAVA_PAT=$AS_JAVA
echo AS_JAVA_PAT=$AS_JAVA_PAT

if [ $IS_EE_INSTALL -eq 1 ]; then
    #EE kit:
    EDIT_LIST="bin/appclient.bat
bin/asadmin.bat
bin/asant.bat
bin/asupgrade.bat
bin/capture-schema.bat
bin/deploytool.bat
bin/jspc.bat
bin/package-appclient.bat
bin/verifier.bat
bin/wscompile.bat
bin/wsdeploy.bat
config/asenv.bat
hadb/4.4.1-7/bin/ma.cfg
imq/etc/imqenv.conf
lib/asadmin-pause.bat
lib/certutil.bat
lib/pk12util.bat
lib/start-appserv.bat
pointbase/tools/serveroption/startcommander.bat
pointbase/tools/serveroption/startconsole.bat
pointbase/tools/serveroption/startserver.bat
pointbase/tools/serveroption/stopserver.bat
"
else
    #PE kit:
    EDIT_LIST="bin/appclient.bat
bin/asadmin.bat
bin/asant.bat
bin/asapt.bat
bin/asupgrade.bat
bin/capture-schema.bat
bin/jspc.bat
bin/package-appclient.bat
bin/schemagen.bat
bin/verifier.bat
bin/wscompile.bat
bin/wsdeploy.bat
bin/wsgen.bat
bin/wsimport.bat
bin/xjc.bat
config/asenv.bat
imq/etc/imqenv.conf
"
fi

#echo EDIT_LIST=$EDIT_LIST

rm -rf patch
mkdir patch

echo creating parameterized files ...
for fn in $EDIT_LIST
do
    newfn=patch/$fn
    mkdir -p `dirname $newfn`
    sed -e "s/$AS_INSTALL_PAT/{=AS8BASE=}/g;s/$AS_JAVA_PAT/{=JAVA_HOME=}/g" $fn > $newfn
done

echo generating codegen script to patch installation ...
(cd patch; walkdir -f | sed -e 's#./##' > $TMPB)
sed -e 's#.*#patch/&	/&#' $TMPB > patch/as8patch.cg

cat >> patch/as8patch.cg << EOF
#add back jbi files now needed at create-domain time as of glassfish b26:
#copy these file without interpolating codegen macros.  RT 8/31/07
%pragma copy 1
jbi.gf/lib/install/templates/jbi-registry.xml.template   jbi/lib/install/templates/jbi-registry.xml.template
jbi.gf/lib/install/templates/config.properties           jbi/lib/install/templates/config.properties
jbi.gf/components/sun-http-binding/httpbc.jar        jbi/components/sun-http-binding/httpbc.jar
jbi.gf/components/sun-javaee-engine/appserv-jbise.jar    jbi/components/sun-javaee-engine/appserv-jbise.jar
jbi.gf/shared-libraries/sun-wsdl-library/wsdlsl.jar      jbi/shared-libraries/sun-wsdl-library/wsdlsl.jar
%pragma copy 0

EOF

if [ $IS_EE_INSTALL -eq 1 ]; then
    tarfile=$SRCROOT/t3/src/sun/as8/$INSTALL_PORT/gfee.tgz
else
    #tarfile=$SRCROOT/t3/src/sun/as8/$INSTALL_PORT/gfpe.tgz
    tarfile=$SRCROOT/t3/src/sun/as8/$INSTALL_PORT/gf${GF_VERSION}.tgz
fi

if [ ! -f $tarfile -o -w $tarfile ]; then
    echo creating parameterized tar file in $tarfile
    rm -f $tarfile
    #create the tar file in compatible format or it won't work on mks/nt.  RT 11/10/06
    if [ "$FORTE_PORT" = "nt" ]; then
        #note:  mks/nt tar auto-switches to ustar format if paths are too long:
        tar czf $tarfile *
    else
        tar --help | grep ustar > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            bldmsg -error -p $p tar does not support --format=ustar.  `fwhich tar`:  `tar --version | head -1`
            exit_status=1
        else
            tar --format=ustar -czf $tarfile *
        fi
    fi
else
    echo $tarfile is not writable, skipping distribution
fi

rm -f $TMPA $TMPB

exit $exit_status
