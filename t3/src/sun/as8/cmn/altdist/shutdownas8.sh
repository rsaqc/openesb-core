#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)shutdownas8.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


shutdown_domains()
#attempt to shutdown any installed domains
{
    _return_status=0
    shutdown_status=0 

    asadmin=$AS8BASE/bin/asadmin
    if [ $FORTE_PORT = "cygwin" ]; then
       asadmin=$AS8BASE/bin/asadmin.bat
    fi

    $asadmin list-domains -t > $TMPA

    #sleep a bit, just in case a the start command was just issued:
    sleep 5

    #re-run command:
    $asadmin list-domains -t > $TMPA
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p Command "'$asadmin list-domains'" FAILED
        return 1
    fi

    grep -v ' not running' $TMPA
    if [ $? -eq 0 ]; then
        for domain in `egrep ' starting| stopping' $TMPA | awk '{ print $1}'`
        do
            bldmsg -markbeg -p $p waiting for $domain to finish starting ...
            sleep 30
            bldmsg -markend -p $p waiting for $domain to finish starting ...
            #re-run command:
            $asadmin list-domains -t > $TMPA
        done

        for domain in `grep -v ' not running' $TMPA | awk '{ print $1}'`
        do
            bldmsg -markbeg -p $p shutdown domain $domain
            $asadmin stop-domain $domain
            if [ $? -ne 0 ]; then
              shutdown_status=1
              bldmsg -mark -p $p there was an error shuting down domain $domain
            fi  
            bldmsg -markend -p $p shutdown domain $domain
        done
        #we need to chill out a bit to wait for domains to really finish
        sleep 30
    else
        bldmsg -mark -p $p No domains are currently running.
        cat $TMPA
    fi
 
    return $shutdown_status
}

shutdown_instances()
{

    is_CAS_running=1

    asadmin=$AS8BASE/bin/asadmin
    pwdfile=$AS8BASE/passwords
    
    $asadmin list-domains -t > $TMPA
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p Command "'$asadmin list-domains'" FAILED
        return 1
    fi

    #Only if CAS is running, instances, node-agents need to be shutdown
    for domain in `grep -v ' not running' $TMPA | awk '{ print $1}'`
    do
        if [ $domain -eq "CAS" ]; then
            is_CAS_running=0
        fi
    done

    if [ $is_CAS_running -ne 0 ]; then
        return 0
    fi
    
   $asadmin list-instances --port $CAS_admin_port --user admin --passwordfile $pwdfile -t > $TMPA
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p Command "'$asadmin list-instances --port $CAS_admin_port'" FAILED
        return 1
    fi

    grep -v ' not running' $TMPA
    if [ $? -eq 0 ]; then
        for instance in `egrep ' starting| stopping' $TMPA | awk '{ print $1}'`
        do
            bldmsg -markbeg -p $p waiting for $instance to finish starting ...
            sleep 30
            bldmsg -markend -p $p waiting for $instance to finish starting ...
            #re-run command:
            $asadmin list-instances --port $CAS_admin_port --user admin --passwordfile $pwdfile -t > $TMPA
        done

        for instance in `grep -v ' not running' $TMPA | awk '{ print $1}'`
        do
            bldmsg -markbeg -p $p shutdown instance $instance
            $asadmin stop-instance --port $CAS_admin_port --user admin --passwordfile $pwdfile $instance
            if [ $? -ne 0 ]; then
              shutdown_status=1
              bldmsg -mark -p $p there was an error shuting down instance $instance
            fi  
            bldmsg -markend -p $p shutdown instance $instance
        done
        #we need to chill out a bit to wait for instances to really finish
        sleep 30
    else
        bldmsg -mark -p $p No instances are currently running.
        cat $TMPA
    fi

}

shutdown_node_agents()
{

    is_CAS_running=1
   
    asadmin=$AS8BASE/bin/asadmin
    pwdfile=$AS8BASE/passwords

    #Only if CAS is running, instances, node-agents need to be shutdown
    for domain in `grep -v ' not running' $TMPA | awk '{ print $1}'`
    do
        if [ $domain -eq "CAS" ]; then
            is_CAS_running=0
        fi
    done

    if [ $is_CAS_running -ne 0 ]; then
        return 0
    fi
   
    $asadmin list-node-agents --port $CAS_admin_port --user admin --passwordfile $pwdfile -t > $TMPA
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p Command "'$asadmin list-node-agents --port $CAS_admin_port --user admin --passwordfile $pwdfile -t '" FAILED
        return 1
    fi

    grep -v ' not running' $TMPA
    if [ $? -eq 0 ]; then
        for agent in `egrep ' starting| stopping' $TMPA | awk '{ print $1}'`
        do
            bldmsg -markbeg -p $p waiting for $agent to finish starting ...
            sleep 30
            bldmsg -markend -p $p waiting for $agent to finish starting ...
            #re-run command:
            $asadmin list-node-agents --port $CAS_admin_port --user admin --passwordfile $pwdfile -t > $TMPA
        done

        for agent in `grep -v ' not running' $TMPA | awk '{ print $1}'`
        do
            bldmsg -markbeg -p $p shutdown agent $agent
            $asadmin stop-node-agent $agent
            if [ $? -ne 0 ]; then
              shutdown_status=1
              bldmsg -mark -p $p there was an error shuting down instance $instance
            fi  
            bldmsg -markend -p $p shutdown agent $agent
        done
        #we need to chill out a bit to wait for instances to really finish
        sleep 30
    else
        bldmsg -mark -p $p No agents are currently running.
        cat $TMPA
    fi

}

######## MAIN

status=0
call_shutdown_instances=1
status_instances=0
status_nodeagents=0
status_domains=0
p=`basename $0`
TMPA=/tmp/${p}_A.$$

if [ x$AS8BASE = x ]; then
    bldmsg -p $p -error AS8BASE is not defined - halt.
    exit 1
fi

if [ -d "$AS8BASE/bin" ]; then
    IS_INSTALLED=1
else
    IS_INSTALLED=0
fi


#attempt to shutdown instances in CAS, if CAS_port.sh is present.
if  test -f $AS8BASE/CAS_port.sh ; then
    call_shutdown_instances=0
    . $AS8BASE/CAS_port.sh    
else
    echo "Port definiton file not found. Instances may not be shut down"
fi


if [ $IS_INSTALLED -eq 1 ]; then

    if [ $call_shutdown_instances -eq 0 ]; then
    
        status_instances=1
        status_nodeagents=1
    
        shutdown_instances
        status_instances=$? 
    
        shutdown_node_agents
        status_nodeagents=$?
    
    fi

    #shutdown the domains
    shutdown_domains
    status_domains=$?

    if [ $status_instances -ne 0 -o $status_nodeagents -ne 0 -o $status_domains -ne 0 ]; then
      status=1
    fi
else
    bldmsg -warn -p $p no appserver installation found in $AS8BASE
fi

rm -f $TMPA
exit $status
